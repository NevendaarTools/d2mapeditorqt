#include "mainwindow.h"
#include "Engine/MapView/MapView.h"

#include <QHBoxLayout>
#include <QSplitter>
#include <QQuickView>
#include "Engine/MapConverter.h"
#include "Events/MapEvents.h"
#include <QtMath>
#include <QElapsedTimer>
#include <QFileDialog>
#include <QApplication>
#include "toolsqt/DBFModel/GameClases.h"
#include "Common/MapJsonIO.h"
#include "Dialogs/ValidationResultYesNoDialog.h"
#include "toolsqt/MapUtils/D2SagaModel.h"
#include "Generation/GeneratorHandle.h"
#include "Generation/GeneratorWrapper.h"
#include "Dialogs/SelectMapInSagaDialog.h"
#include "Common/DataStorageVIew/DataStorageWidget.h"
#include "QMLHelpers/Common/OverlayWrapper.h"
#include "Engine/GameInstance.h"
#include "Engine/Map/MapStateHolder.h"
#include "Engine/SettingsManager.h"
#include "Dialogs/settingseditordialog.h"

MainWindow::MainWindow(QWidget *parent)
    : QWidget(parent)
{
    m_stackedWidget = new QStackedWidget();
    m_view = new View();

    QHBoxLayout *layout = new QHBoxLayout;
    layout->setContentsMargins(0,0,0,0);
    layout->addWidget(m_stackedWidget);

    setLayout(layout);
    m_mainMenuWidget = new QQuickWidget();
//    m_mainMenuWidget->setAttribute(Qt::WA_TranslucentBackground, true);
//    m_mainMenuWidget->setAttribute(Qt::WA_AlwaysStackOnTop, true);
//    m_mainMenuWidget->setAttribute(Qt::WA_TransparentForMouseEvents, true);
//    m_mainMenuWidget->setClearColor(Qt::transparent);
//    m_mainMenuWidget->rootContext()->setContextProperty("infoProvider", m_infoProvider);
    auto overlay = new OverlayWrapper(this);
    m_mainMenuWidget->rootContext()->setContextProperty("overlay", overlay);
    m_mainMenuWidget->rootContext()->setContextProperty("translate", overlay);
    m_mainMenuWidget->engine()->addImageProvider(QLatin1String("provider"), new ImageProvider());
    m_mainMenuWidget->setSource(QUrl::fromLocalFile("QML/MainMenu.qml"));
    m_mainMenuWidget->setResizeMode(QQuickWidget::SizeRootObjectToView);
    m_stackedWidget->addWidget(m_mainMenuWidget);
    m_stackedWidget->addWidget(m_view);
    m_stackedWidget->setCurrentIndex(0);
    m_mainMenuWidget->rootContext()->setContextProperty("translate", this);

    setWindowTitle(tr("D2MapEditor"));
    m_overlay = new OverlayWidget(this);
    m_overlay->init();
    QMetaObject::invokeMethod(this, "onPostLoad", Qt::QueuedConnection);
    registerExecutor<SaveMapCommand>
            ([this](const QVariant &command){return executeSaveMapCommand(command);});
    registerExecutor<ExportMapCommand>
            ([this](const QVariant &command){return executeExportMapCommand(command);});
    registerExecutor<OpenMapCommand>
            ([this](const QVariant &command){return executeOpenMapCommand(command);});
    registerExecutor<PopulateMapCommand>
            ([this](const QVariant &command){return executePopulateMapCommand(command);});
    registerExecutor<OpenSettingsCommand>
            ([this](const QVariant &command){return executeOpenSettingsCommand(command);});
    registerExecutor<CloseMapCommand>
            ([this](const QVariant &command){return executeCloseCommand(command);});
    registerExecutor<OpenDataStorageViewCommand>
            ([this](const QVariant &command){return executeOpenDataStorageViewCommand(command);});

    subscribe<MapLoadedEvent>([this](const QVariant &event){onMapChanged(event);});
}

MainWindow::~MainWindow()
{
    RESOLVE(SettingsManager)->save("settings.json");
    //m_quickWidget2->close();
}

void MainWindow::keyPressEvent(QKeyEvent *event)
{
    if (event->key() == Qt::Key_F11)
    {
        if (windowState() != Qt::WindowFullScreen)
            setWindowState(Qt::WindowFullScreen);
        else
            setWindowState(Qt::WindowMaximized);
    }
    QWidget::keyPressEvent(event);
}

CommandResult MainWindow::executeSaveMapCommand(const QVariant &commandVar)
{
    Q_UNUSED(commandVar);
    QString name= m_mapPath;
    name = name.replace(".sg",".nme");//TODO: save
    MapJsonIO::save(RESOLVE(MapStateHolder)->map(), name);
    return CommandResult::success();

    m_map = MapConverter::exportMap(RESOLVE(MapStateHolder)->map());
    name = m_mapPath;
    while (name.endsWith("[nme]"))
        name.remove(name.count() - 5, 5);
    name += "[nme].sg";
    m_map.save(name);
    return CommandResult::success();
}

CommandResult MainWindow::executeExportMapCommand(const QVariant &commandVar)
{
    ExportMapCommand command = commandVar.value<ExportMapCommand>();
    m_map = MapConverter::exportMap(RESOLVE(MapStateHolder)->map());
    QString fileName = QFileDialog::getSaveFileName(this,  // Предполагаем, что нет родительского виджета
        "Save map",  // Заголовок диалога
        m_mapPath,  // Начальная директория
        "D2 map (*.sg)");  // Фильтр файлов

    if (!fileName.isEmpty()) {
         m_map.save(fileName);
        return CommandResult::success();
    } else {
        return CommandResult::fail("");
    }

//    name = m_mapPath;
//    while (name.endsWith("[nme]"))
//        name.remove(name.count() - 5, 5);
//    name += "[nme].sg";
//    m_map.save(name);
//    return CommandResult::success();
}

CommandResult MainWindow::executeOpenMapCommand(const QVariant &commandVar)
{
    OpenMapCommand command = commandVar.value<OpenMapCommand>();
    if (command.path.endsWith(".csg"))
    {
        D2SagaModel saga;
        if (!saga.load(command.path))
            return CommandResult::fail("Can't open csg file!");;

        SelectMapInSagaDialog * dlg = new SelectMapInSagaDialog(this);
        dlg->init(saga);
        if (dlg->exec() != QDialog::Accepted)
            return CommandResult::success();
        int mapIndex = dlg->selectedMap();
        D2MapModel map = saga.getMap(mapIndex);
        ValidationResult result  = MapConverter::validateMap(map);
        if (!result.valid)
        {
            ValidationResultYesNoDialog * dlg = new ValidationResultYesNoDialog(this, result, "Import errors! Open anyway?");
            if (QDialog::Rejected == dlg->exec())
                return CommandResult::fail("Failed to import map.");
        }
        GameMap resMap;
        MapConverter::importMap(&resMap, map);
        RESOLVE(MapStateHolder)->setMap(resMap);
        populateScene();
        m_map = map;
        m_mapPath = command.path + "_" +QString::number(mapIndex) + ".sg";
    }
    else if (command.path.endsWith(".sg"))
    {
        QString importedPath = command.path;
        importedPath.replace(".sg", ".nme/mapInfo.json");
        QFileInfo importedFile(importedPath);
        if (importedFile.exists())
        {
            GameMap::MapInfo info;
            bool infoRecived = MapJsonIO::readMapInfo(info, importedFile.absoluteDir().absolutePath());
            qDebug()<<"Map version = "<<info.version;
            if (infoRecived && info.version > 0.8 && false)
            {
                GameMap resMap;
                MapJsonIO::load(resMap, importedFile.absoluteDir().absolutePath());
                RESOLVE(MapStateHolder)->setMap(resMap);
                m_mapPath = command.path;
            }
            else {
                D2MapModel map;
                map.open(command.path);
                ValidationResult result  = MapConverter::validateMap(map);
                if (!result.valid)
                {
                    ValidationResultYesNoDialog * dlg = new ValidationResultYesNoDialog(this, result, "Import errors! Open anyway?");
                    if (QDialog::Rejected == dlg->exec())
                        return CommandResult::fail("Failed to import map.");
                }
                GameMap resMap;
                MapConverter::importMap(&resMap, map);
                RESOLVE(MapStateHolder)->setMap(resMap);
                //populateScene();
                m_map = map;
                m_mapPath = command.path;
            }
        }
        else
        {
            D2MapModel map;
            map.open(command.path);
            ValidationResult result  = MapConverter::validateMap(map);
            if (!result.valid)
            {
                ValidationResultYesNoDialog * dlg = new ValidationResultYesNoDialog(this, result, "Import errors! Open anyway?");
                if (QDialog::Rejected == dlg->exec())
                    return CommandResult::fail("Failed to import map.");
            }
            GameMap resMap;
            MapConverter::importMap(&resMap, map);
            RESOLVE(MapStateHolder)->setMap(resMap);
            //populateScene();
            m_map = map;
            m_mapPath = command.path;
        }
    }
    else if (command.path.endsWith(".json"))
    {
        QFileInfo info(command.path);
        GameMap resMap;
        MapJsonIO::load(resMap, info.absoluteDir().absolutePath());
        RESOLVE(MapStateHolder)->setMap(resMap);
        m_mapPath = command.path;
    }
    m_stackedWidget->setCurrentIndex(1);
    return CommandResult::success();
}

CommandResult MainWindow::executePopulateMapCommand(const QVariant &commandVar)
{
    PopulateMapCommand command = commandVar.value<PopulateMapCommand>();
    populateScene();
    m_stackedWidget->setCurrentIndex(1);
    m_mapPath = command.path;
    qDebug()<<Q_FUNC_INFO<<m_mapPath;
    return CommandResult::success();
}

CommandResult MainWindow::executeOpenSettingsCommand(const QVariant &commandVar)
{
    SettingsEditorDialog dlg(this);
    dlg.exec();
    return CommandResult::success();
    OpenSettingsCommand command = commandVar.value<OpenSettingsCommand>();
    QString path;
    path = QFileDialog::getExistingDirectory(0, ("Select Game Folder"), QDir::currentPath());
    if (path == "")
        return CommandResult::success();
    RESOLVE(SettingsManager)->setValue("GamePath", path);
    m_view->closeMap();
    GameInstance::init(path);
    return CommandResult::success();
}

CommandResult MainWindow::executeOpenDataStorageViewCommand(const QVariant &command)
{
    DataStorageWidget * wgt = new DataStorageWidget();
    wgt->initStorage("ExtraDeps/Worlds/Editor");
    wgt->setGeometry(200,200,1220, 600);
    wgt->show();
    return CommandResult::success();
}

CommandResult MainWindow::executeCloseCommand(const QVariant &command)
{
    m_stackedWidget->setCurrentIndex(0);
    m_view->closeMap();
    return CommandResult::success();
}

void MainWindow::onMapChanged(const QVariant &event)
{
    m_view->closeMap();
    populateScene();
}

void MainWindow::onPostLoad()
{
    updateOverlayGeometry();
    QString path;
    if (RESOLVE(SettingsManager)->get("GamePath") == "")
    {
        path = QFileDialog::getExistingDirectory(0, ("Select Game Folder"), QDir::currentPath());
        if (path == "")
            QApplication::quit();
        RESOLVE(SettingsManager)->setValue("GamePath", path);
    }
    else
    {
        path = RESOLVE(SettingsManager)->get("GamePath");
    }
    GameInstance::init(path);
}

void MainWindow::updateOverlayGeometry()
{
    if (m_stackedWidget && m_overlay)
    {
        m_overlay->setGeometry(m_stackedWidget->geometry());
    }
}

QRectF getVisibleRect( QGraphicsView * view )
{
    QPointF A = view->mapToScene( QPoint(0, 0) );
    QPointF B = view->mapToScene( QPoint(
                                      view->viewport()->width(),
        view->viewport()->height() ));
    return QRectF( A, B );
}

D2MapModel MainWindow::exportMap()
{
    return D2MapModel();
}

void MainWindow::populateScene()
{
    m_view->setMap(RESOLVE(MapStateHolder)->map());
}

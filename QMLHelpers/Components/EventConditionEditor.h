#ifndef EVENTCONDITIONEDITOR_H
#define EVENTCONDITIONEDITOR_H

#include <QObject>
#include <QString>
#include <QVariant>
#include <QVector>
#include "toolsqt/MapUtils/DataBlocks/D2Event.h"

class EventConditionEditor : public QObject
{
    Q_OBJECT
public:
    explicit EventConditionEditor(QObject *parent = nullptr);

    Q_PROPERTY(int conditionType READ conditionType WRITE setConditionType NOTIFY conditionTypeChanged)
    Q_PROPERTY(QString uid READ uid WRITE setUid NOTIFY uidChanged)

    Q_PROPERTY(QString bank READ bank WRITE setBank NOTIFY conditionChanged)
    Q_PROPERTY(bool greaterOrEqual READ greaterOrEqual WRITE setGreaterOrEqual NOTIFY conditionChanged)
    Q_PROPERTY(int frequency READ frequency WRITE setFrequency NOTIFY conditionChanged)
    Q_PROPERTY(QString locId READ locId WRITE setLocId NOTIFY conditionChanged)
    Q_PROPERTY(QString villageId READ villageId WRITE setVillageId NOTIFY conditionChanged)
    Q_PROPERTY(QString stackId READ stackId WRITE setStackId NOTIFY conditionChanged)
    Q_PROPERTY(QString itemType READ itemType WRITE setItemType NOTIFY conditionChanged)
    Q_PROPERTY(QString player1 READ player1 WRITE setPlayer1 NOTIFY conditionChanged)
    Q_PROPERTY(QString player2 READ player2 WRITE setPlayer2 NOTIFY conditionChanged)
    Q_PROPERTY(int diplomacy READ diplomacy WRITE setDiplomacy NOTIFY conditionChanged)
    Q_PROPERTY(QString ruinId READ ruinId WRITE setRuinId NOTIFY conditionChanged)
    Q_PROPERTY(int pctLand READ pctLand WRITE setPctLand NOTIFY conditionChanged)
    Q_PROPERTY(QString siteId READ siteId WRITE setSiteId NOTIFY conditionChanged)
    Q_PROPERTY(int miscInt READ miscInt WRITE setMiscInt NOTIFY conditionChanged)
    Q_PROPERTY(int miscInt2 READ miscInt2 WRITE setMiscInt2 NOTIFY conditionChanged)
    Q_PROPERTY(int miscInt3 READ miscInt3 WRITE setMiscInt3 NOTIFY conditionChanged)
    Q_PROPERTY(int miscInt4 READ miscInt4 WRITE setMiscInt4 NOTIFY conditionChanged)
    Q_PROPERTY(int miscInt5 READ miscInt5 WRITE setMiscInt5 NOTIFY conditionChanged)
    Q_PROPERTY(int miscInt6 READ miscInt6 WRITE setMiscInt6 NOTIFY conditionChanged)
    Q_PROPERTY(int miscInt7 READ miscInt7 WRITE setMiscInt7 NOTIFY conditionChanged)
    Q_PROPERTY(QString var1 READ var1 WRITE setVar1 NOTIFY conditionChanged)
    Q_PROPERTY(QString var2 READ var2 WRITE setVar2 NOTIFY conditionChanged)
    Q_PROPERTY(int cmp READ cmp WRITE setCmp NOTIFY conditionChanged)
    Q_PROPERTY(QString code READ code WRITE setCode NOTIFY conditionChanged)
    Q_PROPERTY(QString descr READ descr WRITE setDescr NOTIFY conditionChanged)
    Q_PROPERTY(int mode READ mode WRITE setMode NOTIFY conditionChanged)
    Q_PROPERTY(bool ai READ ai WRITE setAI NOTIFY conditionChanged)

    Q_INVOKABLE QString d2Id(const QString & uid) const;

    Q_INVOKABLE void saveEvent();
    void setConditionType(int newConditionType);
    QString uid() const;
    void setUid(const QString &newUid);
    int conditionType() const;

    QString bank() const { return _bank; }
    void setBank(QString bank) {if (_bank != bank){_bank = bank; emit conditionChanged();} }

    bool greaterOrEqual() const { return _greaterOrEqual; }
    void setGreaterOrEqual(bool greaterOrEqual) { _greaterOrEqual = greaterOrEqual; emit conditionChanged(); }

    int frequency() const { return _frequency; }
    void setFrequency(int frequency) { _frequency = frequency; emit conditionChanged(); }

    QString locId() const { return m_locId; }
    void setLocId(const QString& locId) { m_locId = locId; emit conditionChanged(); }

    QString villageId() const { return m_villageId; }
    void setVillageId(const QString& villageId) { m_villageId = villageId; emit conditionChanged(); }

    QString stackId() const { return m_stackId; }
    void setStackId(const QString& stackId) { m_stackId = stackId; emit conditionChanged(); }

    QString itemType() const { return _itemType; }
    void setItemType(const QString& itemType) { _itemType = itemType; emit conditionChanged(); }

    QString player1() const { return _player1; }
    void setPlayer1(const QString& player1) { _player1 = player1; emit conditionChanged(); }

    QString player2() const { return _player2; }
    void setPlayer2(const QString& player2) { _player2 = player2; emit conditionChanged(); }

    int diplomacy() const { return _diplomacy; }
    void setDiplomacy(int diplomacy) { _diplomacy = diplomacy; emit conditionChanged(); }

    QString ruinId() const { return _ruinId; }
    void setRuinId(const QString& ruinId) { _ruinId = ruinId; emit conditionChanged(); }

    int pctLand() const { return _pctLand; }
    void setPctLand(int pctLand) { if (_pctLand != pctLand) {_pctLand = pctLand; emit conditionChanged();} }

    QString siteId() const { return _siteId; }
    void setSiteId(const QString& siteId) { _siteId = siteId; emit conditionChanged(); }

    int miscInt() const { return _miscInt; }
    void setMiscInt(int miscInt) { _miscInt = miscInt; emit conditionChanged(); }

    int miscInt2() const { return _miscInt2; }
    void setMiscInt2(int miscInt2) { _miscInt2 = miscInt2; emit conditionChanged(); }

    int miscInt3() const { return _miscInt3; }
    void setMiscInt3(int miscInt3) { _miscInt3 = miscInt3; emit conditionChanged(); }

    int miscInt4() const { return _miscInt4; }
    void setMiscInt4(int miscInt4) { _miscInt4 = miscInt4; emit conditionChanged(); }

    int miscInt5() const { return _miscInt5; }
    void setMiscInt5(int miscInt5) { _miscInt5 = miscInt5; emit conditionChanged(); }

    int miscInt6() const { return _miscInt6; }
    void setMiscInt6(int miscInt6) { _miscInt6 = miscInt6; emit conditionChanged(); }

    int miscInt7() const { return _miscInt7; }
    void setMiscInt7(int miscInt7) { _miscInt7 = miscInt7; emit conditionChanged(); }

    QString var1() const { return _var1; }
    void setVar1(QString var1) { _var1 = var1; emit conditionChanged(); }

    QString var2() const { return _var2; }
    void setVar2(QString var2) { _var2 = var2; emit conditionChanged(); }

    int cmp() const { return _cmp; }
    void setCmp(int cmp) { _cmp = cmp; emit conditionChanged(); }

    QString code() const { return _code; }
    void setCode(const QString& code) { _code = code; emit conditionChanged(); }

    QString descr() const { return _descr; }
    void setDescr(const QString& descr) { _descr = descr; emit conditionChanged(); }

    int mode() const { return _mode; }
    void setMode(int mode) { _mode = mode; emit conditionChanged(); }

    bool ai() const { return _ai; }
    void setAI(bool ai) { _ai = ai; emit conditionChanged(); }
signals:
    void uidChanged();
    void conditionTypeChanged();
    void conditionChanged();
private:
    void fillFromCondition();
    void updateEffect();
private:
    QString _bank;
    bool _greaterOrEqual;
    int _frequency;
    QString m_locId;
    QString m_villageId;
    QString m_stackId;
    QString _itemType;
    QString _player1;
    QString _player2;
    int _diplomacy;
    QString _ruinId;
    int _pctLand;
    QString _siteId;
    int _miscInt;
    int _miscInt2;
    int _miscInt3;
    int _miscInt4;
    int _miscInt5;
    int _miscInt6;
    int _miscInt7;
    QString _var1;
    QString _var2;
    int _cmp;
    QString _code;
    QString _descr;
    int _mode;
    bool _ai;

    D2Event::EventCondition m_condition;
    QPair<int, int> eventUId;
    int m_conditionIndex = 0;
    int m_conditionType;
};


#endif // EVENTCONDITIONEDITOR_H

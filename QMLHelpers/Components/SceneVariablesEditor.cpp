#include "SceneVariablesEditor.h"
#include <QStringList>
#include "MapObjects/ScenVariableObject.h"
#include "Engine/Components/TranslationHelper.h"

SceneVariablesEditor::SceneVariablesEditor(QObject *parent) : QObject(parent)
{
    m_variables = new SelectMapObjectModel(this);
    RESOLVE(MapStateHolder)->startEdit();
}

int SceneVariablesEditor::value(const QString &id) const
{
    auto uid = IdFromString(id);
    auto variable = RESOLVE(MapStateHolder)->objectByIdT<ScenVariableObject>(uid);
    return variable.value;
}

void SceneVariablesEditor::setValue(const QString &id, int value) const
{
    auto uid = IdFromString(id);
    auto variable = RESOLVE(MapStateHolder)->objectByIdT<ScenVariableObject>(uid);
    variable.value = value;
    RESOLVE(MapStateHolder)->replaceObject(variable);
    m_variables->updateWithId(IdFromString(id));
}

void SceneVariablesEditor::setName(const QString &id, const QString & value) const
{
    auto uid = IdFromString(id);
    auto variable = RESOLVE(MapStateHolder)->objectByIdT<ScenVariableObject>(uid);
    variable.name = value;
    RESOLVE(MapStateHolder)->replaceObject(variable);
    m_variables->updateWithId(IdFromString(id));
}

QString SceneVariablesEditor::desc(const QString &id) const
{
    QString result = "";
    auto uid = IdFromString(id);
    auto variable = RESOLVE(MapStateHolder)->objectByIdT<ScenVariableObject>(uid);
    auto translated = TranslationHelper::tr(variable.name);
    if (translated != variable.name)
    {
        result += translated + ". ";
    }
    result += TranslationHelper::tr("Linked event cout: ");
    result += "not implemented";
    return result;
}

SelectMapObjectModel *SceneVariablesEditor::variables() const
{
    return m_variables;
}

QString SceneVariablesEditor::createVariable(const QString &name, int value)
{
    ScenVariableObject obj;
    obj.name = name;
    obj.value = value;
    auto res = RESOLVE(MapStateHolder)->addObject<>(obj);
    auto result =  IdToString(res);
    init();
    return result;
}

void SceneVariablesEditor::init()
{
    auto getText = [](const QSharedPointer<MapObject>& item)
    {
        ScenVariableObject * variable = static_cast<ScenVariableObject*>(item.data());
        return variable->name;
    };
    auto getId = [](const QSharedPointer<MapObject>& item)
    {
        ScenVariableObject * variable = static_cast<ScenVariableObject*>(item.data());
        return variable->uid;
    };

    m_variables->init<ScenVariableObject>(getText, getId);
}

void SceneVariablesEditor::save()
{
    RESOLVE(MapStateHolder)->acceptEdit();
}

void SceneVariablesEditor::cancel()
{
    RESOLVE(MapStateHolder)->rejectEdit();
    init();
}

#include "GroupUnitsEditor.h"
#include "MapObjects/UnitObject.h"
#include "Engine/GameInstance.h"
#include "toolsqt/DBFModel/DBFModel.h"
#include "Engine/Map/MapStateHolder.h"

GroupUnitsEditor::GroupUnitsEditor(QObject *parent) : QObject(parent)
{
    resetDrag();
}

QString GroupUnitsEditor::unitId(int x, int y)
{
    return m_editor.unitId(x, y);
}

QString GroupUnitsEditor::unitUid(int x, int y)
{
    return m_editor.unitUid(x, y);
}

QString GroupUnitsEditor::icon(int x, int y)
{
    QString unit = unitId(x, y);
    if (unit == QString())
        return unit;
    auto gunit = RESOLVE(DBFModel)->get<Gunit>(unit);
    QString unitIconId = gunit->base_unit.getKey() + "FACE";
    if (gunit->base_unit.value == nullptr)
        unitIconId = gunit->unit_id + "FACE";
    return "Faces-" + unitIconId;
}

int GroupUnitsEditor::unitBaseLvl(int x, int y)
{
    return m_editor.unitBaseLvl(x, y);
}

int GroupUnitsEditor::unitMaxHp(int x, int y)
{
    return m_editor.unitMaxHp(x, y);
}

int GroupUnitsEditor::unitCurrentHp(int x, int y)
{
    return m_editor.unitCurrentHp(x, y);
}

int GroupUnitsEditor::unitMaxLvl()
{
    return m_editor.unitMaxLvl();
}

int GroupUnitsEditor::expForKill()
{
    return m_editor.expForKill();
}

int GroupUnitsEditor::unitLvl(int x, int y)
{
    return m_editor.unitLvl(x, y);
}

void GroupUnitsEditor::setUnitLvl(int x, int y, int level)
{
    if (unitLvl(x, y) != level)
    {
        m_editor.setUnitLvl(x, y, level);
        emit unitsChanged();
    }
}

bool GroupUnitsEditor::isLeader(int x, int y)
{
    return m_editor.isLeader(x, y);
}

bool GroupUnitsEditor::isSmall(int x, int y)
{
    return m_editor.isSmall(x, y);
}

bool GroupUnitsEditor::hasModifiers(int x, int y)
{
    return m_editor.hasModifiers(x, y);
}

void GroupUnitsEditor::remove(int x, int y)
{
    m_editor.remove(x, y);
    emit unitsChanged();
}

void GroupUnitsEditor::setDragSource(int x, int y)
{
    m_dragSource = QPoint(x, y);
}

void GroupUnitsEditor::setDragTarget(int x, int y)
{
    m_dragTarget = QPoint(x, y);
}

void GroupUnitsEditor::finishDrag()
{
    emit dragFinished();
}

bool GroupUnitsEditor::tryHire(int x, int y, const QString &id, int level)
{
    if (m_editor.tryHire(x, y, id, level))
    {
        emit unitsChanged();
        return true;
    }
    return false;
}

bool GroupUnitsEditor::isDragTarget() const
{
    if (m_dragTarget.x() == -1)
        return false;
    return true;
}

bool GroupUnitsEditor::isDragSource() const
{
    if (m_dragSource.x() == -1)
        return false;
    return true;
}

void GroupUnitsEditor::resetDrag()
{
    m_dragTarget = QPoint(-1, -1);
    m_dragSource = QPoint(-1, -1);
}

void GroupUnitsEditor::setGroup(const GroupData &newGroup)
{
    m_editor.setGroup(newGroup);
    emit unitsChanged();
}

const GroupData &GroupUnitsEditor::group() const
{
    return m_editor.group();
}

void GroupUnitsEditor::clearGroup()
{
    m_editor.clearGroup();
    emit unitsChanged();
}

void GroupUnitsEditor::print(const GroupData &group)
{
    m_editor.print(group);
}

void GroupUnitsEditor::moveUnit(const QPoint &from, GroupUnitsEditor *target, const QPoint &to)
{
    m_editor.moveUnit(from, &(target->m_editor), to);
}

QString GroupEditor::unitId(int x, int y)
{
    return m_group.unitByPos(QPoint(x, y)).id;
}

QString GroupEditor::unitUid(int x, int y)
{
    auto result = m_group.unitByPos(QPoint(x, y));
    return QString("%1:%2").arg(result.uid.first).arg(result.uid.second);
}

int GroupEditor::unitBaseLvl(int x, int y)
{
    QString unit = unitId(x, y);
    if (unit == QString())
        return 0;
    auto gunit = RESOLVE(DBFModel)->get<Gunit>(unit.toUpper());
    if (gunit.isNull())
        return 0;
    return gunit->level;
}

int GroupEditor::unitMaxHp(int x, int y)
{
    QString unit = unitId(x, y);
    if (unit == QString())
        return 0;
    auto gunit = RESOLVE(DBFModel)->get<Gunit>(unit.toUpper());
    if (gunit.isNull())
        return 0;
    return calcLeveledValue(gunit->hit_point, gunit->level, unitLvl(x, y),
                            gunit->dyn_upg_lv, gunit->dyn_upg1->hit_point,
                            gunit->dyn_upg2->hit_point);
}

int GroupEditor::unitCurrentHp(int x, int y)
{
    auto result = m_group.unitByPos(QPoint(x, y));
    return result.currentHp;
}

int GroupEditor::unitMaxLvl()
{
    return 65536 / RESOLVE(DBFModel)->getKeysList<Gunit>().count();
}

int GroupEditor::expForKill()
{
    return groupExpForKill(m_group);
}

int GroupEditor::unitLvl(int x, int y)
{
    auto result = m_group.unitByPos(QPoint(x, y));
    return result.level;
}

void GroupEditor::setUnitLvl(int x, int y, int level)
{
    // return;
    int index = m_group.unitIndexByPos(QPoint(x, y));
    if (index < 0)
        return;
    auto unit = RESOLVE(MapStateHolder)->objectByIdT<UnitObject>(m_group.units[index].first);
    unit.level = level;
    auto gunit = RESOLVE(DBFModel)->get<Gunit>(unit.id);
    if (!gunit.isNull())
        unit.currentHp = calcLeveledValue(gunit->hit_point, gunit->level, level,
                                          gunit->dyn_upg_lv, gunit->dyn_upg1->hit_point,
                                          gunit->dyn_upg2->hit_point);
    RESOLVE(MapStateHolder)->replaceObject(QSharedPointer<MapObject>(new UnitObject(unit)));
}

bool GroupEditor::isLeader(int x, int y)
{
    return m_group.isLeader(x, y);
}

bool GroupEditor::isSmall(int x, int y)
{
    if (!m_group.hasUnit(QPoint(x, y)))
        return true;
    auto result = m_group.unitByPos(QPoint(x, y));
    auto unit = RESOLVE(DBFModel)->get<Gunit>(result.id);
    return unit->size_small;
}

bool GroupEditor::hasModifiers(int x, int y)
{
    if (!m_group.hasUnit(QPoint(x, y)))
        return false;
    auto result = m_group.unitByPos(QPoint(x, y));
    return result.modifiers.count() > 0;
}

void GroupEditor::remove(int x, int y)
{
    m_group.remove(x, y);
}

bool GroupEditor::tryHire(int x, int y, const QString &id, int level)
{
    if (m_group.hasUnit(QPoint(x, y)))
        return false;
    auto gunit = RESOLVE(DBFModel)->get<Gunit>(id);
    if (!gunit->size_small && (m_group.hasUnit(QPoint(0, y)) || m_group.hasUnit(QPoint(1, y))))
        return false;
    UnitObject unit(0,0);
    unit.id = id;
    unit.leader = gunit->unit_cat->text != "L_SOLDIER";
    qDebug()<<unit.id << unit.leader<<gunit->unit_cat->text;
    unit.currentHp = calcLeveledValue(gunit->hit_point, gunit->level, level,
                                      gunit->dyn_upg_lv, gunit->dyn_upg1->hit_point,
                                      gunit->dyn_upg2->hit_point);
    unit.level = level;
    if (unit.level < gunit->level)
        unit.level = gunit->level;

    RESOLVE(MapStateHolder)->addObject<UnitObject>(unit);
    m_group.addUnit(unit, QPoint(x, y));
}

void GroupEditor::setGroup(const GroupData &newGroup)
{
    m_group = newGroup;
}

const GroupData &GroupEditor::group() const
{
    return m_group;
}

void GroupEditor::clearGroup()
{
    for (int i = 0; i < 2; ++i) {
        for (int k = 0; k < 3; ++k) {
            remove(i, k);
        }
    }
}

void GroupEditor::swapRows(int y1, int y2)
{
    if (y1 == y2)
        return;
    for(int i = 0; i< m_group.units.count(); ++i)
    {
        if (m_group.units[i].second.y() == y1)
            m_group.units[i].second.setY(y2);
        else if (m_group.units[i].second.y() == y2)
            m_group.units[i].second.setY(y1);
    }
}

void GroupEditor::swapUnits(const QPoint &from, GroupEditor *target, const QPoint &to)
{
    if (target == this && to == from)
        return;
    if (!m_group.hasUnit(from))
        return;
    int fromIndex = m_group.unitIndexByPos(from);
    if (!target->m_group.hasUnit(to))
    {
        auto unitObj = RESOLVE(MapStateHolder)->objectByIdT<UnitObject>(m_group.units[fromIndex].first);
        target->m_group.addUnit(unitObj, to);
        m_group.units.removeAt(fromIndex);
        return;
    }

    auto fromUnit = RESOLVE(MapStateHolder)->objectByIdT<UnitObject>(m_group.units[fromIndex].first);
    m_group.units.removeAt(fromIndex);
    int toIndex = target->m_group.unitIndexByPos(to);
    target->m_group.addUnit(fromUnit, to);
    m_group.addUnit(RESOLVE(MapStateHolder)->objectByIdT<UnitObject>(target->m_group.units[toIndex].first), from);
    target->m_group.units.removeAt(toIndex);
}

void GroupEditor::print(const GroupData &group)
{
    for(int i = 0; i< group.units.count(); ++i)
    {
        auto unitObj =  RESOLVE(MapStateHolder)->objectByIdT<UnitObject>(group.units[i].first);
        QString unit = unitObj.id;
        auto gunit = RESOLVE(DBFModel)->get<Gunit>(unit);
        if (!gunit.isNull())
            unit = gunit->name_txt.value->text;
        qDebug()<< QString("%1 - %2_%3").arg(unit.trimmed())
                        .arg(group.units[i].second.x())
                        .arg(group.units[i].second.y());
    }
}

void GroupEditor::moveUnit(const QPoint &from, GroupEditor *target, const QPoint &to)
{
    qDebug()<<"Before:" << from << " "<<to;
    print(m_group);
    if (this != target)
    {
        //don't swap leaders between stacks
        if (isLeader(from) || target->isLeader(to))
            return;
        if (!isSmall(from) || !target->isSmall(to))
        {
            if (isLeader(0, from.y()) || isLeader(1, from.y()))
                return;
            if (target->isLeader(0, to.y()) || target->isLeader(1, to.y()))
                return;
        }
    }

    if (!isSmall(from) || !target->isSmall(to))
    {
        if (this == target)
            swapRows(from.y(), to.y());
        else {
            qDebug()<<"big units";
            if (!isSmall(from) && !target->isSmall(to))//both big
                swapUnits(from, target, to);
            else
            {
                if (!target->isSmall(to))//target big - from small
                {
                    int toIndex = target->m_group.unitIndexByPos(to);
                    auto toUnit =  RESOLVE(MapStateHolder)->objectByIdT<UnitObject>(target->m_group.units[toIndex].first);
                    target->m_group.units.removeAt(toIndex);
                    swapUnits(QPoint(0, from.y()), target, QPoint(0, to.y()));
                    swapUnits(QPoint(1, from.y()), target, QPoint(1, to.y()));
                    m_group.addUnit(toUnit, QPoint(0, from.y()));
                }
                else//from big to small or empty
                {
                    target->moveUnit(to, this, from);
                }
            }
        }
    }
    else//both small(or target is empty)
        swapUnits(from, target, to);

    qDebug()<<"After:";
    print(m_group);
}

bool GroupEditor::hasUnit(int pos) const
{
    int x = pos % 2;
    int y = pos / 2;
    return hasUnit(x, y);
}


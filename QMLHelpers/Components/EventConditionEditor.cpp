#include "EventConditionEditor.h"
#include "Engine/GameInstance.h"
#include "MapObjects/EventObject.h"
#include <QDebug>
#include "Engine/Map/MapStateHolder.h"
#include "toolsqt/MapUtils/DataBlocks/D2Location.h"
#include "toolsqt/MapUtils/DataBlocks/D2Village.h"
#include "toolsqt/MapUtils/DataBlocks/D2Merchant.h"
#include "toolsqt/MapUtils/DataBlocks/D2Ruin.h"
#include "toolsqt/MapUtils/DataBlocks/D2Player.h"
#include "EventConvertionHelper.h"

void EventConditionEditor::saveEvent()
{
    updateEffect();
    auto event = RESOLVE(MapStateHolder)->objectByIdT<EventObject>(eventUId);
    event.conditions[m_conditionIndex].condition = m_condition;
    RESOLVE(MapStateHolder)->replaceObject(event);
}

EventConditionEditor::EventConditionEditor(QObject *parent) : QObject(parent)
{
    m_condition.category = (D2Event::EventCondition::ConditionType)999;
}

QString EventConditionEditor::d2Id(const QString &uid) const
{
    auto intUid = IdFromString(uid);
    return "S143" + TypeHolder::ShortByType((IDataBlock::Type)(intUid.first)) +
                    QString::number(intUid.second, 16).rightJustified(4, '0').toLower();
}

QString EventConditionEditor::uid() const
{
    return QString("%1:%2:%3").arg(eventUId.first).arg(eventUId.second).arg(m_conditionIndex);
}

void EventConditionEditor::setUid(const QString &newUid)
{
    qDebug()<<Q_FUNC_INFO;
    QStringList data = newUid.split(":");
    if (data.count() < 3)
        return;

    eventUId = QPair<int, int>{data[0].toInt(), data[1].toInt()};
    m_conditionIndex = data[2].toInt();
    auto event = RESOLVE(MapStateHolder)->objectByIdT<EventObject>(QPair<int,int>(data[0].toInt(), data[1].toInt()));
    m_condition = event.conditions[m_conditionIndex].condition;
    qDebug()<<m_condition.toString();
    fillFromCondition();
    emit uidChanged();
    emit conditionTypeChanged();
}

int EventConditionEditor::conditionType() const
{
    return m_condition.category;
}

void EventConditionEditor::fillFromCondition()
{
    switch (m_condition.category) {
    case D2Event::EventCondition::RESOURCE_AMOUNT:
    {
        auto& cond = std::get<D2Event::EventCondition::ConditionResourceAmount>(m_condition.condition);
        _bank = cond.bank;
        _greaterOrEqual = cond.greaterOrEqual;
        break;
    }
    case D2Event::EventCondition::FREQUENCY:
    {
        auto& cond = std::get<D2Event::EventCondition::ConditionFrequency>(m_condition.condition);
        _frequency = cond.frequency;
        break;
    }
    case D2Event::EventCondition::ENTERING_A_PREDEFINED_ZONE:
    {
        auto& cond = std::get<D2Event::EventCondition::ConditionEnterZone>(m_condition.condition);
        m_locId = stringIdFromD2<D2Location>(cond.locId);
        break;
    }
    case D2Event::EventCondition::ENTERING_A_CITY:
    {
        auto& cond = std::get<D2Event::EventCondition::ConditionEnterCity>(m_condition.condition);
        m_villageId = stringIdFromD2<D2Village>(cond.villageId);
        break;
    }
    case D2Event::EventCondition::OWNING_A_CITY:
    {
        auto& cond = std::get<D2Event::EventCondition::ConditionOwningCity>(m_condition.condition);
        m_villageId = stringIdFromD2<D2Village>(cond.villageId);
        break;
    }
    case D2Event::EventCondition::DESTROY_STACK:
    {
        auto& cond = std::get<D2Event::EventCondition::ConditionDestroyStack>(m_condition.condition);
        m_stackId = stackIdFromD2(cond.stackId);
        break;
    }
    case D2Event::EventCondition::OWNING_AN_ITEM:
    {
        auto& cond = std::get<D2Event::EventCondition::ConditionOwningItem>(m_condition.condition);
        _itemType = cond.itemType;
        break;
    }
    case D2Event::EventCondition::SPECIFIC_LEADER_OWNING_AN_ITEM:
    {
        auto& cond = std::get<D2Event::EventCondition::ConditionLeaderOwningItem>(m_condition.condition);
        _itemType = cond.itemType;
        m_stackId = stackIdFromD2(cond.stackId);
        break;
    }
    case D2Event::EventCondition::DIPLOMACY_RELATIONS:
    {
        auto& cond = std::get<D2Event::EventCondition::ConditionDiplomacyRelation>(m_condition.condition);
        _player1 = stringIdFromD2<D2Player>(cond.player1);
        _player2 = stringIdFromD2<D2Player>(cond.player2);
        _diplomacy = cond.diplomacy;
        break;
    }
    case D2Event::EventCondition::ALLIANCE:
    {
        auto& cond = std::get<D2Event::EventCondition::ConditionAlliance>(m_condition.condition);
        _player1 = stringIdFromD2<D2Player>(cond.player1);
        _player2 = stringIdFromD2<D2Player>(cond.player2);
        break;
    }
    case D2Event::EventCondition::LOOTING_A_RUIN:
    {
        auto& cond = std::get<D2Event::EventCondition::ConditionLootingRuin>(m_condition.condition);
        _ruinId = stringIdFromD2<D2Ruin>(cond.ruinId);
        break;
    }
    case D2Event::EventCondition::TRANSFORMING_LAND:
    {
        auto& cond = std::get<D2Event::EventCondition::ConditionTransformLand>(m_condition.condition);
        _pctLand = cond.value;
        break;
    }
    case D2Event::EventCondition::VISITING_A_SITE:
    {
        auto& cond = std::get<D2Event::EventCondition::ConditionVisitingSite>(m_condition.condition);
        _siteId = stringIdFromD2<D2Merchant>(cond.siteId);
        break;
    }
    case D2Event::EventCondition::STACK_IN_LOCATION:
    {
        auto& cond = std::get<D2Event::EventCondition::ConditionStackInLocation>(m_condition.condition);
        m_stackId = stackIdFromD2(cond.stackId);
        m_locId = stringIdFromD2<D2Location>(cond.locId);
        break;
    }
    case D2Event::EventCondition::STACK_IN_CITY:
    {
        const auto& cond = std::get<D2Event::EventCondition::ConditionStackInCity>(m_condition.condition);
        m_stackId = stackIdFromD2(cond.stackId);
        m_villageId = stringIdFromD2<D2Village>(cond.villageId);
        break;
    }
    case D2Event::EventCondition::ITEM_TO_LOCATION:
    {
        const auto& cond = std::get<D2Event::EventCondition::ConditionItemToLocation>(m_condition.condition);
        _itemType = cond.itemType;
        m_locId = stringIdFromD2<D2Location>(cond.locId);
        break;
    }
    case D2Event::EventCondition::STACK_EXISTANCE:
    {
        const auto& cond = std::get<D2Event::EventCondition::ConditionStackExist>(m_condition.condition);
        m_stackId = stackIdFromD2(cond.stackId);
        _miscInt = cond.exist ? 0 : 1;
        break;
    }
    case D2Event::EventCondition::VARIABLE_IS_IN_RANGE:
    {
        const auto& cond = std::get<D2Event::EventCondition::ConditionVarInRange>(m_condition.condition);
        _var1 = IdToString(QPair<int,int>(MapObject::ScenVariable, cond.var1));
        _miscInt2 = cond.min_1;
        _miscInt3 = cond.max_1;
        _var2 = IdToString(QPair<int,int>(MapObject::ScenVariable, cond.var2));
        _miscInt5 = cond.min_2;
        _miscInt6 = cond.max_2;
        _miscInt7 = cond.relation;
        break;
    }
    case D2Event::EventCondition::COMPARE_VAR:
    {
        const auto& cond = std::get<D2Event::EventCondition::ConditionCompareVar>(m_condition.condition);
        _var1 = IdToString(QPair<int,int>(MapObject::ScenVariable, cond.var1));
        _var2 = IdToString(QPair<int,int>(MapObject::ScenVariable, cond.var1));
        _cmp = static_cast<int>(cond.relation);
        break;
    }
    case D2Event::EventCondition::CUSTOM_SCRIPT:
    {
        const auto& cond = std::get<D2Event::EventCondition::ConditionCustomScript>(m_condition.condition);
        _code = cond.code;
        _descr = cond.desc;
        break;
    }
    case D2Event::EventCondition::CHECK_GAMEMODE:
    {
        const auto& cond = std::get<D2Event::EventCondition::ConditionGameMode>(m_condition.condition);
        _mode = static_cast<int>(cond.mode);
        break;
    }
    case D2Event::EventCondition::CHECK_FOR_HUMAN:
    {
        const auto& cond = std::get<D2Event::EventCondition::ConditionPlayerType>(m_condition.condition);
        _ai = cond.isAI;
        break;
    }
    }
}

void EventConditionEditor::updateEffect()
{
    switch (m_condition.category) {
    case D2Event::EventCondition::RESOURCE_AMOUNT:
    {
        D2Event::EventCondition::ConditionResourceAmount cond;
        cond.bank = _bank;
        cond.greaterOrEqual = _greaterOrEqual;
        m_condition.condition = cond;
        break;
    }
    case D2Event::EventCondition::FREQUENCY:
    {
        D2Event::EventCondition::ConditionFrequency cond;
        cond.frequency = _frequency;
        m_condition.condition = cond;
        break;
    }
    case D2Event::EventCondition::ENTERING_A_PREDEFINED_ZONE:
    {
        D2Event::EventCondition::ConditionEnterZone cond;
        cond.locId = stringIdToD2<D2Location>(m_locId);
        m_condition.condition = cond;
        break;
    }
    case D2Event::EventCondition::ENTERING_A_CITY:
    {
        D2Event::EventCondition::ConditionOwningCity cond;
        cond.villageId = stringIdToD2<D2Village>(m_villageId);
        m_condition.condition = cond;
        break;
    }
    case D2Event::EventCondition::OWNING_A_CITY:
    {
        D2Event::EventCondition::ConditionOwningCity cond;
        cond.villageId = stringIdToD2<D2Village>(m_villageId);
        m_condition.condition = cond;
        break;
    }
    case D2Event::EventCondition::DESTROY_STACK:
    {
        D2Event::EventCondition::ConditionDestroyStack cond;
        cond.stackId = stackIdToD2(m_stackId);
        m_condition.condition = cond;
        break;
    }
    case D2Event::EventCondition::OWNING_AN_ITEM:
    {
        D2Event::EventCondition::ConditionOwningItem cond;
        cond.itemType = _itemType;
        m_condition.condition = cond;
        break;
    }
    case D2Event::EventCondition::SPECIFIC_LEADER_OWNING_AN_ITEM:
    {
        D2Event::EventCondition::ConditionLeaderOwningItem cond;
        cond.itemType = _itemType;
        cond.stackId = stackIdToD2(m_stackId);
        m_condition.condition = cond;
        break;
    }
    case D2Event::EventCondition::DIPLOMACY_RELATIONS:
    {
        D2Event::EventCondition::ConditionDiplomacyRelation cond;
        cond.player1 = stringIdToD2<D2Player>(_player1);
        cond.player2 = stringIdToD2<D2Player>(_player2);
        cond.diplomacy = _diplomacy;
        m_condition.condition = cond;
        break;
    }
    case D2Event::EventCondition::ALLIANCE:
    {
        D2Event::EventCondition::ConditionAlliance cond;
        cond.player1 = stringIdToD2<D2Player>(_player1);
        cond.player2 = stringIdToD2<D2Player>(_player2);
        m_condition.condition = cond;
        break;
    }
    case D2Event::EventCondition::LOOTING_A_RUIN:
    {
        D2Event::EventCondition::ConditionLootingRuin cond;
        cond.ruinId = stringIdToD2<D2Ruin>(_ruinId);
        m_condition.condition = cond;
        break;
    }
    case D2Event::EventCondition::TRANSFORMING_LAND:
    {
        D2Event::EventCondition::ConditionTransformLand cond;
        cond.value = _pctLand;
        m_condition.condition = cond;
        break;
    }
    case D2Event::EventCondition::VISITING_A_SITE:
    {
        D2Event::EventCondition::ConditionVisitingSite cond;
        cond.siteId = stringIdToD2<D2Merchant>(_siteId);
        m_condition.condition = cond;
        break;
    }
    case D2Event::EventCondition::STACK_IN_LOCATION:
    {
        D2Event::EventCondition::ConditionStackInLocation cond;
        cond.stackId = stackIdToD2(m_stackId);
        cond.locId = stringIdToD2<D2Location>(m_locId);
        m_condition.condition = cond;
        break;
    }
    case D2Event::EventCondition::STACK_IN_CITY:
    {
        D2Event::EventCondition::ConditionStackInCity cond;
        cond.stackId = stackIdToD2(m_stackId);
        cond.villageId = stringIdToD2<D2Village>(m_villageId);
        m_condition.condition = cond;
        break;
    }
    case D2Event::EventCondition::ITEM_TO_LOCATION:
    {
        D2Event::EventCondition::ConditionItemToLocation cond;
        cond.itemType = _itemType;
        cond.locId = stringIdToD2<D2Location>(m_locId);
        m_condition.condition = cond;
        break;
    }
    case D2Event::EventCondition::STACK_EXISTANCE:
    {
        D2Event::EventCondition::ConditionStackExist cond;
        cond.stackId = stackIdToD2(m_stackId);
        cond.exist = _miscInt == 0;
        m_condition.condition = cond;
        break;
    }
    case D2Event::EventCondition::VARIABLE_IS_IN_RANGE:
    {
        D2Event::EventCondition::ConditionVarInRange cond;
        cond.var1 = IdFromString(_var1).second;
        cond.min_1 = _miscInt2;
        cond.max_1 = _miscInt3;
        cond.var2 = IdFromString(_var2).second;
        cond.min_2 = _miscInt5;
        cond.max_2 = _miscInt6;
        cond.relation = _miscInt7;
        m_condition.condition = cond;
        break;
    }
    case D2Event::EventCondition::COMPARE_VAR:
    {
        D2Event::EventCondition::ConditionCompareVar cond;
        cond.var1 = IdFromString(_var1).second;
        cond.var2 = IdFromString(_var2).second;
        cond.relation = static_cast<D2Event::EventCondition::ConditionCompareVar::CompareType>(_cmp);
        m_condition.condition = cond;
        break;
    }
    case D2Event::EventCondition::CUSTOM_SCRIPT:
    {
        D2Event::EventCondition::ConditionCustomScript cond;
        cond.code = _code;
        cond.desc = _descr;
        m_condition.condition = cond;
        break;
    }
    case D2Event::EventCondition::CHECK_GAMEMODE:
    {
        D2Event::EventCondition::ConditionGameMode cond;
        cond.mode = static_cast<D2Event::EventCondition::ConditionGameMode::GameMode>(_mode);
        m_condition.condition = cond;
        break;
    }
    case D2Event::EventCondition::CHECK_FOR_HUMAN:
    {
        D2Event::EventCondition::ConditionPlayerType cond;
        cond.isAI = _ai;
        m_condition.condition = cond;
        break;
    }
    }
}

void EventConditionEditor::setConditionType(int newConditionType)
{
    if (m_conditionType == newConditionType)
        return;
    m_conditionType = newConditionType;
    emit conditionTypeChanged();
}

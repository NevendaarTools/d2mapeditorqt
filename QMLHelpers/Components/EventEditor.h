#ifndef EVENTEDITOR_H
#define EVENTEDITOR_H
#include <QObject>
#include "MapObjects/EventObject.h"
#include "Commands/CommandBus.h"
#include <QSharedPointer>
#include <QQuickWidget>
#include "QMLHelpers/Models/StringModel.h"

class EventEditor : public QObject
{
    Q_OBJECT
public:
    explicit EventEditor(QObject *parent = nullptr);
    Q_PROPERTY(QString name READ name WRITE setName NOTIFY changed)
    Q_PROPERTY(int chance READ chance WRITE setChance NOTIFY changed)
    Q_PROPERTY(bool enabledFromStart READ enabledFromStart WRITE setEnabledFromStart NOTIFY changed)
    Q_PROPERTY(bool occurInfinite READ occurInfinite WRITE setOccurInfinite NOTIFY changed)
    Q_PROPERTY(StringModel * conditions READ conditions NOTIFY changed)
    Q_PROPERTY(StringModel * effects READ effects NOTIFY changed)

    Q_INVOKABLE bool activatedByRace(int index) const;
    Q_INVOKABLE void setActivatedByRace(int index);
    Q_INVOKABLE void setUID(const QString &uid);
    Q_INVOKABLE QString getUID();

    Q_INVOKABLE void moveEffect(int index, int value);
    Q_INVOKABLE void removeEffect(int index);
    Q_INVOKABLE void cloneEffect(int index);
    Q_INVOKABLE void moveCondition(int index, int value);
    Q_INVOKABLE void removeCondition(int index);
    Q_INVOKABLE void cloneCondition(int index);

    Q_PROPERTY(StringModel* availableConditions READ availableConditions NOTIFY changed)
    Q_PROPERTY(StringModel* availableEffects READ availableEffects NOTIFY changed)

    Q_INVOKABLE void save();
    Q_INVOKABLE void cancel();


    StringModel *effects() const;
    StringModel *conditions() const;

    bool occurInfinite() const;
    void setOccurInfinite(bool newOccurInfinite);

    bool enabledFromStart() const;
    void setEnabledFromStart(bool newEnabledFromStart);

    int chance() const;
    void setChance(int newChance);

    const QString name() const;
    void setName(const QString &newName);

    StringModel *availableConditions() const;
    StringModel *availableEffects() const;

    Q_INVOKABLE void addCondition(int code);
    Q_INVOKABLE void addEffect(int code);

    void updateAvailable() const;
signals:
    void changed();
private:
    void updateEffects();
    void updateConditions();
private:
    EventObject m_event;
    EventObject m_origEvent;
    StringModel *m_effects;
    StringModel *m_conditions;
    StringModel *m_availableConditions = nullptr;
    StringModel *m_availableEffects = nullptr;
};

#endif // EVENTEDITOR_H

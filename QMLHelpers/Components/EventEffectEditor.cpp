#include "EventEffectEditor.h"
#include "Engine/GameInstance.h"
#include "MapObjects/EventObject.h"
#include "Engine/Map/MapStateHolder.h"
#include "toolsqt/MapUtils/DataBlocks/D2Location.h"
#include "toolsqt/MapUtils/DataBlocks/D2LandMark.h"
#include <QDebug>
#include "EventConvertionHelper.h"
#include "toolsqt/MapUtils/DataBlocks/D2Player.h"
#include "toolsqt/DBFModel/DBFModel.h"

void EventEffectEditor::saveEvent()
{
    updateEffect();
    auto event = RESOLVE(MapStateHolder)->objectByIdT<EventObject>(eventUId);
    event.effects[m_effectIndex].effect = m_effect;
    RESOLVE(MapStateHolder)->replaceObject(event);
}

EventEffectEditor::EventEffectEditor(QObject *parent) : QObject(parent)
{
    m_effect.category = (D2Event::EventEffect::EffectType)999;
}

QString EventEffectEditor::uid() const
{
    return QString("%1:%2:%3").arg(eventUId.first).arg(eventUId.second).arg(m_effectIndex);
}

void EventEffectEditor::setUid(const QString &newUid)
{
    qDebug()<<Q_FUNC_INFO;
    QStringList data = newUid.split(":");
    if (data.count() < 3)
        return;

    eventUId = QPair<int, int>{data[0].toInt(), data[1].toInt()};
    m_effectIndex = data[2].toInt();
    auto event = RESOLVE(MapStateHolder)->objectByIdT<EventObject>(QPair<int,int>(data[0].toInt(), data[1].toInt()));
    m_effect = event.effects[m_effectIndex].effect;
    qDebug()<<m_effect.toString();
    fillFromEffect();
        qDebug()<<"###"<<m_stackId;
    emit uidChanged();
    emit effectTypeChanged();
}

int EventEffectEditor::effectType() const
{
    return m_effect.category;
}

void EventEffectEditor::setEffectType(int newEffectType)
{
    if (m_effect.category == newEffectType)
        return;
    m_effect.category = (D2Event::EventEffect::EffectType)newEffectType;
    emit effectTypeChanged();
}

void EventEffectEditor::fillFromEffect()
{
    switch (m_effect.category) {
        case D2Event::EventEffect::WIN_OR_LOSE_SCENARIO:
        {
            auto& effect = std::get<D2Event::EventEffect::WinLooseEffect>(m_effect.effect);
            m_num = effect.num;
            m_win = effect.win;
            m_playerId1 = stringIdFromD2<D2Player>(effect.playerId);
            break;
        }
        case D2Event::EventEffect::CREATE_NEW_STACK:
        {
            auto& effect = std::get<D2Event::EventEffect::CreateStackEffect>(m_effect.effect);
            m_num = effect.num;
            m_templateId = stringIdFromD2<D2StackTemplate>(effect.templateId);
            m_locId = stringIdFromD2<D2Location>(effect.locId);
            break;
        }
        case D2Event::EventEffect::CAST_SPELL_ON_TRIGGERER:
        {
            auto& effect = std::get<D2Event::EventEffect::CastSpellOnTriggerEffect>(m_effect.effect);
            m_num = effect.num;
            m_spellType = effect.spellType;
            m_playerId1 = stringIdFromD2<D2Player>(effect.playerId1);
            break;
        }
        case D2Event::EventEffect::CAST_SPELL_AT_SPECIFIC_LOCATION:
        {
            auto& effect = std::get<D2Event::EventEffect::CastSpellOnLocationEffect>(m_effect.effect);
            m_num = effect.num;
            m_spellType = effect.spellType;
            m_locId = stringIdFromD2<D2Location>(effect.locId);
            qDebug()<<"$$$$$$$$$$$ player 1 id = "<<effect.playerId1;
            m_playerId1 = stringIdFromD2<D2Player>(effect.playerId1);
            break;
        }
        case D2Event::EventEffect::CHANGE_STACK_OWNER:
        {
            auto& effect = std::get<D2Event::EventEffect::ChangeStackOwnerEffect>(m_effect.effect);
            m_num = effect.num;
            m_stackId = stackIdFromD2(effect.stackId);
            m_playerId1 = stringIdFromD2<D2Player>(effect.playerId1);
            m_firstOnly = effect.firstOnly;
            m_anim = effect.anim;
            break;
        }
        case D2Event::EventEffect::MOVE_STACK_NEXT_TO_TRIGGERER:
        {
            auto& effect = std::get<D2Event::EventEffect::MoveStackToTriggererEffect>(m_effect.effect);
            m_num = effect.num;
            m_stackId = stackIdFromD2(effect.stackId);
            break;
        }
        case D2Event::EventEffect::GO_INTO_BATTLE:
        {
            auto& effect = std::get<D2Event::EventEffect::GoBattleEffect>(m_effect.effect);
            m_num = effect.num;
            m_stackId = stackIdFromD2(effect.stackId);
            m_firstOnly = effect.firstOnly;
            break;
        }
        case D2Event::EventEffect::ENABLE_DISABLE_ANOTHER_EVENT:
        {
            auto& effect = std::get<D2Event::EventEffect::DisableEventEffect>(m_effect.effect);
            m_num = effect.num;
            m_eventId = effect.eventId;
            m_enabled = effect.enable;
            break;
        }
        case D2Event::EventEffect::GIVE_SPELL:
        {
            auto& effect = std::get<D2Event::EventEffect::GiveSpellEffect>(m_effect.effect);
            m_num = effect.num;
            m_spellType = effect.spellType;
            break;
        }
        case D2Event::EventEffect::GIVE_ITEM:
        {
            auto& effect = std::get<D2Event::EventEffect::GiveItemEffect>(m_effect.effect);
            m_num = effect.num;
            m_giveTo = effect.giveTo;
            m_itemType = effect.itemType;
            break;
        }
        case D2Event::EventEffect::MOVE_STACK_TO_SPECIFIC_LOCATION:
        {
            auto& effect = std::get<D2Event::EventEffect::MoveStackToLocationEffect>(m_effect.effect);
            m_num = effect.num;
            m_stackId = stackIdFromD2(effect.stackTmpId);
            m_locId = stringIdFromD2<D2Location>(effect.locId);
            m_boolValue = effect.boolVal;
            break;
        }
        case D2Event::EventEffect::ALLY_TWO_AI_PLAYERS:
        {
            auto& effect = std::get<D2Event::EventEffect::AllyAIPlayersEffect>(m_effect.effect);
            m_num = effect.num;
            m_playerId1 = stringIdFromD2<D2Player>(effect.playerId1);
            m_playerId2 = stringIdFromD2<D2Player>(effect.playerId2);
            m_permally = effect.permally;
            break;
        }
        case D2Event::EventEffect::CHANGE_PLAYER_DIPLOMACY_METER:
        {
            auto& effect = std::get<D2Event::EventEffect::ChangeDiplomacyEffect>(m_effect.effect);
            m_num = effect.num;
            m_playerId1 = stringIdFromD2<D2Player>(effect.playerId1);
            m_playerId2 = stringIdFromD2<D2Player>(effect.playerId2);
            m_diplomacy = effect.diplomacy + 10000 * (int)effect.enabled;
            m_enabled = effect.enabled;
            break;
        }
        case D2Event::EventEffect::UNFOG_OR_FOG_AN_AREA_ON_THE_MAP:
        {
            auto& effect = std::get<D2Event::EventEffect::ChangeFogEffect>(m_effect.effect);
            m_num = effect.num;
            m_locId = stringIdFromD2<D2Location>(effect.locId);
            if (effect.events.count() > 0) {
                m_playerId1 = stringIdFromD2<D2Player>(effect.events[0].player);
            }
            m_enabled = effect.enable;
            m_value = effect.value;
            break;
        }
        case D2Event::EventEffect::REMOVE_MOUNTAINS_AROUND_A_LOCATION:
        {
            auto& effect = std::get<D2Event::EventEffect::RemoveMountainsEffect>(m_effect.effect);
            m_num = effect.num;
            m_locId = stringIdFromD2<D2Location>(effect.locId);
            break;
        }
        case D2Event::EventEffect::REMOVE_LANDMARK:
        {
            auto& effect = std::get<D2Event::EventEffect::RemoveLandMarkEffect>(m_effect.effect);
            m_num = effect.num;
            m_lmarkId = stringIdFromD2<D2LandMark>(effect.lmarkId);
            m_boolValue = effect.boolVal;
            break;
        }
        case D2Event::EventEffect::CHANGE_SCENARIO_OBJECTIVE_TEXT:
        {
            auto& effect = std::get<D2Event::EventEffect::ChangeScenarioTextEffect>(m_effect.effect);
            m_num = effect.num;
            m_text = effect.text;
            break;
        }
        case D2Event::EventEffect::DISPLAY_POPUP_MESSAGE:
        {
            auto& effect = std::get<D2Event::EventEffect::DisplayPopupEffect>(m_effect.effect);
            m_num = effect.num;
            m_text = effect.text;
            m_music = effect.music;
            m_sound = effect.sound;
            m_image = effect.image;
            m_image2 = effect.image2;
            m_leftSide = effect.leftSide;
            m_popupShow = effect.popupShow;
            m_boolValue = effect.boolValue;
            break;
        }
        case D2Event::EventEffect::CHANGE_STACK_ORDER:
        {
            auto& effect = std::get<D2Event::EventEffect::ChangeStackOrderEffect>(m_effect.effect);
            m_num = effect.num;
            m_stackId = stackIdFromD2(effect.stackId);
            m_orderTarget = effect.orderTarget;
            m_firstOnly = effect.firstOnly;
            m_order = effect.order;
            break;
        }
        case D2Event::EventEffect::DESTROY_ITEM:
        {
            auto& effect = std::get<D2Event::EventEffect::DestroyItemEffect>(m_effect.effect);
            m_num = effect.num;
            m_itemType = effect.itemType;
            m_triggerOnly = effect.triggerOnly;
            break;
        }
        case D2Event::EventEffect::REMOVE_STACK:
        {
            auto& effect = std::get<D2Event::EventEffect::RemoveStackEffect>(m_effect.effect);
            m_num = effect.num;
            m_stackId = stackIdFromD2(effect.stackId);
            m_firstOnly = effect.firstOnly;
            break;
        }
        case D2Event::EventEffect::CHANGE_LANDMARK:
        {
            auto& effect = std::get<D2Event::EventEffect::ChangeLandmarkEffect>(m_effect.effect);
            m_num = effect.num;
            m_lmarkId = stringIdFromD2<D2LandMark>(effect.lmarkId);
            m_lmarkType = effect.lmarkType;
            QSharedPointer<DBFModel>  data = RESOLVE(DBFModel);
            auto mark = data->get<GLmark>(effect.lmarkType);
            m_value = mark->cx;
            break;
        }
        case D2Event::EventEffect::CHANGE_TERRAIN:
        {
            auto& effect = std::get<D2Event::EventEffect::ChangeTerrainEffect>(m_effect.effect);
            m_num = effect.num;
            m_locId = stringIdFromD2<D2Location>(effect.locId);
            m_lookup = effect.lookup;
            m_value = effect.value;
            break;
        }
        case D2Event::EventEffect::MODIFY_VARIABLE:
        {
            auto& effect = std::get<D2Event::EventEffect::ModifyVarEffect>(m_effect.effect);
            m_num = effect.num;
            m_lookup = effect.lookup;
            m_val1 = effect.val1;
            m_val2 = effect.val2;
            break;
        }
        }
}

void EventEffectEditor::updateEffect()
{
    switch (m_effect.category) {
    case D2Event::EventEffect::WIN_OR_LOSE_SCENARIO:
    {
        D2Event::EventEffect::WinLooseEffect effect;
        effect.num = m_num;
        effect.win = m_win;
        effect.playerId = stringIdToD2<D2Player>(m_playerId1);
        m_effect.effect = effect;
        break;
    }
    case D2Event::EventEffect::EventEffect::CREATE_NEW_STACK:
    {
        D2Event::EventEffect::CreateStackEffect effect;
        effect.num = m_num;
        effect.templateId = stringIdToD2<D2StackTemplate>(m_templateId);
        effect.locId = m_locId;
        m_effect.effect = effect;
        break;
    }
    case D2Event::EventEffect::EventEffect::CAST_SPELL_ON_TRIGGERER:
    {
        D2Event::EventEffect::CastSpellOnTriggerEffect effect;
        effect.num = m_num;
        effect.spellType = m_spellType;
        effect.playerId1 = stringIdToD2<D2Player>(m_playerId1);
        m_effect.effect = effect;
        break;
    }
    case D2Event::EventEffect::EventEffect::CAST_SPELL_AT_SPECIFIC_LOCATION:
    {
        D2Event::EventEffect::CastSpellOnLocationEffect effect;
        effect.num = m_num;
        effect.spellType = m_spellType;
        effect.locId = stringIdToD2<D2Location>(m_locId);
        effect.playerId1 = stringIdToD2<D2Player>(m_playerId1);
        m_effect.effect = effect;
        break;
    }
    case D2Event::EventEffect::EventEffect::CHANGE_STACK_OWNER:
    {
        D2Event::EventEffect::ChangeStackOwnerEffect effect;
        effect.num = m_num;
        effect.stackId = stackIdToD2(m_stackId);
        effect.playerId1 = stringIdToD2<D2Player>(m_playerId1);
        effect.firstOnly = m_firstOnly;
        effect.anim = m_anim;
        m_effect.effect = effect;
        break;
    }
    case D2Event::EventEffect::EventEffect::MOVE_STACK_NEXT_TO_TRIGGERER:
    {
        D2Event::EventEffect::MoveStackToTriggererEffect effect;
        effect.num = m_num;
        effect.stackId = stackIdToD2(m_stackId);
        m_effect.effect = effect;
        break;
    }
    case D2Event::EventEffect::EventEffect::GO_INTO_BATTLE:
    {
        D2Event::EventEffect::GoBattleEffect effect;
        effect.num = m_num;
        effect.stackId = stackIdToD2(m_stackId);
        effect.firstOnly = m_firstOnly;
        m_effect.effect = effect;
        break;
    }
    case D2Event::EventEffect::EventEffect::ENABLE_DISABLE_ANOTHER_EVENT:
    {
        D2Event::EventEffect::DisableEventEffect effect;
        effect.num = m_num;
        effect.eventId = m_eventId;
        effect.enable = m_enabled;
        m_effect.effect = effect;
        break;
    }
    case D2Event::EventEffect::EventEffect::GIVE_SPELL:
    {
        D2Event::EventEffect::GiveSpellEffect effect;
        effect.num = m_num;
        effect.spellType = m_spellType;
        m_effect.effect = effect;
        break;
    }
    case D2Event::EventEffect::EventEffect::GIVE_ITEM:
    {
        D2Event::EventEffect::GiveItemEffect effect;
        effect.num = m_num;
        effect.giveTo = m_giveTo;
        effect.itemType = m_itemType;
        m_effect.effect = effect;
        break;
    }
    case D2Event::EventEffect::EventEffect::MOVE_STACK_TO_SPECIFIC_LOCATION:
    {
        D2Event::EventEffect::MoveStackToLocationEffect effect;
        effect.num = m_num;
        effect.stackTmpId = stackIdToD2(m_stackId);
        effect.locId = stringIdToD2<D2Location>(m_locId);
        effect.boolVal = m_boolValue;
        m_effect.effect = effect;
        break;
    }
    case D2Event::EventEffect::EventEffect::ALLY_TWO_AI_PLAYERS:
    {
        D2Event::EventEffect::AllyAIPlayersEffect effect;
        effect.num = m_num;
        effect.playerId1 = stringIdToD2<D2Player>(m_playerId1);
        effect.playerId2 = stringIdToD2<D2Player>(m_playerId2);
        effect.permally = m_permally;
        m_effect.effect = effect;
        break;
    }
    case D2Event::EventEffect::EventEffect::CHANGE_PLAYER_DIPLOMACY_METER:
    {
        D2Event::EventEffect::ChangeDiplomacyEffect effect;
        effect.num = m_num;
        effect.playerId1 = stringIdToD2<D2Player>(m_playerId1);
        effect.playerId2 = stringIdToD2<D2Player>(m_playerId2);
        effect.diplomacy = m_diplomacy;
        if (effect.diplomacy > 10000)
            effect.diplomacy-=10000;
        effect.enabled = m_diplomacy > 10000;
        m_effect.effect = effect;
        break;
    }
    case D2Event::EventEffect::EventEffect::UNFOG_OR_FOG_AN_AREA_ON_THE_MAP:
    {
        D2Event::EventEffect::ChangeFogEffect effect;
        effect.num = m_num;
        effect.locId = stringIdToD2<D2Location>(m_locId);//TODO: fix fog entries
        effect.events.clear();
        effect.events << D2Event::EventEffect::ChangeFogEffect::FogEntry{"", stringIdToD2<D2Player>(m_playerId1)};
        effect.enable = m_enabled;
        effect.value = m_value;
        m_effect.effect = effect;
        break;
    }
    case D2Event::EventEffect::EventEffect::REMOVE_MOUNTAINS_AROUND_A_LOCATION:
    {
        D2Event::EventEffect::RemoveMountainsEffect effect;
        effect.num = m_num;
        effect.locId = stringIdToD2<D2Location>(m_locId);
        m_effect.effect = effect;
        break;
    }
    case D2Event::EventEffect::EventEffect::REMOVE_LANDMARK:
    {
        D2Event::EventEffect::RemoveLandMarkEffect effect;
        effect.num = m_num;
        effect.lmarkId = stringIdToD2<D2LandMark>(m_lmarkId);
        effect.boolVal = m_boolValue;
        m_effect.effect = effect;
        break;
    }
    case D2Event::EventEffect::EventEffect::CHANGE_SCENARIO_OBJECTIVE_TEXT:
    {
        D2Event::EventEffect::ChangeScenarioTextEffect effect;
        effect.num = m_num;
        effect.text = m_text;
        m_effect.effect = effect;
        break;
    }
    case D2Event::EventEffect::EventEffect::DISPLAY_POPUP_MESSAGE:
    {
        D2Event::EventEffect::DisplayPopupEffect effect;
        effect.num = m_num;
        effect.text = m_text;
        effect.music = m_music;
        effect.sound = m_sound;
        effect.image = m_image;
        effect.image2 = m_image2;
        effect.leftSide = m_leftSide;
        effect.popupShow = m_popupShow;
        effect.boolValue = m_boolValue;
        m_effect.effect = effect;
        break;
    }
    case D2Event::EventEffect::EventEffect::CHANGE_STACK_ORDER:
    {
        D2Event::EventEffect::ChangeStackOrderEffect effect;
        effect.num = m_num;
        effect.stackId = stackIdToD2(m_stackId);
        effect.orderTarget = m_orderTarget;
        effect.firstOnly = m_firstOnly;
        effect.order = m_order;
        m_effect.effect = effect;
        break;
    }
    case D2Event::EventEffect::EventEffect::DESTROY_ITEM:
    {
        D2Event::EventEffect::DestroyItemEffect effect;
        effect.num = m_num;
        effect.itemType = m_itemType;
        effect.triggerOnly = m_triggerOnly;
        m_effect.effect = effect;
        break;
    }
    case D2Event::EventEffect::EventEffect::REMOVE_STACK:
    {
        D2Event::EventEffect::RemoveStackEffect effect;
        effect.num = m_num;
        effect.stackId = stackIdToD2(m_stackId);
        effect.firstOnly = m_firstOnly;
        m_effect.effect = effect;
        break;
    }
    case D2Event::EventEffect::EventEffect::CHANGE_LANDMARK:
    {
        D2Event::EventEffect::ChangeLandmarkEffect effect;
        effect.num = m_num;
        effect.lmarkId = stringIdToD2<D2LandMark>(m_lmarkId);
        effect.lmarkType = m_lmarkType;
        m_effect.effect = effect;
        break;
    }
    case D2Event::EventEffect::EventEffect::CHANGE_TERRAIN:
    {
        D2Event::EventEffect::ChangeTerrainEffect effect;
        effect.num = m_num;
        effect.locId = stringIdToD2<D2Location>(m_locId);
        effect.lookup = m_lookup;
        effect.value = m_value;
        m_effect.effect = effect;
        break;
    }
    case D2Event::EventEffect::EventEffect::MODIFY_VARIABLE:
    {
        D2Event::EventEffect::ModifyVarEffect effect;
        effect.num = m_num;
        effect.lookup = m_lookup;
        effect.val1 = m_val1;
        effect.val2 = m_val2;
        m_effect.effect = effect;
        break;
    }
    }
}

bool EventEffectEditor::firstOnly() const
{
    return m_firstOnly;
}

void EventEffectEditor::setFirstOnly(bool newFirstOnly)
{
    if (m_firstOnly == newFirstOnly)
        return;
    m_firstOnly = newFirstOnly;
    emit firstOnlyChanged();
}

bool EventEffectEditor::anim() const
{
    return m_anim;
}

void EventEffectEditor::setAnim(bool newAnim)
{
    if (m_anim == newAnim)
        return;
    m_anim = newAnim;
    emit animChanged();
}

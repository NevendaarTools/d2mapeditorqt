#ifndef COSTEDITOR_H
#define COSTEDITOR_H

#include <QObject>
//#include "AppInstance.h"
#include <QAbstractListModel>
//#include "MapObjects/BaseClases.h"

class CostEditor : public QAbstractListModel
{
    Q_OBJECT
public:
    struct CostValue
    {
        int value;
        QString icon;
        QString letter;
        CostValue(){}
        CostValue(const QString & totalValue, const QString & icon, const QString & letter) : icon(icon), letter(letter)
        {
            QString tmp = totalValue.toUpper();
            value = tmp.mid(tmp.indexOf(letter) + 1, 4).toInt();
        }
        static bool hasValue(const QString & totalValue, const QString & letter)
        {
            QString tmp = totalValue.toUpper();
            return tmp.mid(tmp.indexOf(letter) + 1, 4).toInt() > 0;
        }
    };
    explicit CostEditor(QObject *parent = nullptr);
    enum Roles
    {
        ROLE_COUNT = Qt::UserRole + 1,
        ROLE_ICON
    };

    int rowCount(const QModelIndex & parent = QModelIndex()) const;
    virtual QVariant data(const QModelIndex & index, int role = Qt::DisplayRole) const;
    QHash<int, QByteArray> roleNames() const Q_DECL_OVERRIDE;
    Q_PROPERTY(QString value READ value WRITE setValue NOTIFY valueChanged)
    Q_PROPERTY(bool editable READ editable WRITE setEditable NOTIFY valueChanged)

    QString value() const;
    void setValue(const QString &newValue);
    Q_INVOKABLE void setValue(int index, const QString &newValue);

    bool editable() const;
    void setEditable(bool newEditable);

signals:
    void valueChanged();

private:
    QHash<int, QByteArray> m_roles;
    QString m_value;
    QVector<CostValue> m_data;
    bool m_editable;
};

#endif // COSTEDITOR_H

#ifndef STACKTEMPLATEEDITOR_H
#define STACKTEMPLATEEDITOR_H

#include <QObject>
#include <QObject>
#include "MapObjects/StackTemplateObject.h"
#include "Commands/CommandBus.h"
#include <QSharedPointer>
#include <QQuickWidget>
#include "QMLHelpers/Models/InventoryModel.h"
#include <QQmlContext>
#include "QMLHelpers/Components/GroupUnitsEditor.h"
#include "QMLHelpers/Components/StackOrderEditor.h"
#include "QMLHelpers/Models/EventsListModel.h"
#include "QMLHelpers/Models/OwnerSelectModel.h"

class StackTemplateEditor : public QObject
{
    Q_OBJECT
public:
    explicit StackTemplateEditor(QObject *parent = nullptr);
    Q_PROPERTY(GroupUnitsEditor* stack READ stack NOTIFY dataChanged)
    Q_PROPERTY(int rotation READ rotation WRITE setRotation NOTIFY dataChanged)
    Q_PROPERTY(int priority READ priority WRITE setPriority NOTIFY dataChanged)
    Q_PROPERTY(QString name READ name WRITE setName NOTIFY dataChanged)
    Q_PROPERTY(QString icon READ icon NOTIFY dataChanged)
    Q_PROPERTY(bool ignore READ ignore WRITE setIgnore NOTIFY dataChanged)
    Q_PROPERTY(StackOrderEditor* orderEditor READ orderEditor NOTIFY dataChanged)
    Q_PROPERTY(EventsListModel* events READ events NOTIFY dataChanged)
    Q_PROPERTY(OwnerSelectModel *  ownerModel READ ownerModel NOTIFY changed)
    Q_PROPERTY(QString uid READ uid NOTIFY dataChanged)

    void setUID(QPair<int, int> uid);
    Q_INVOKABLE void setUID(const QString& uid);
    Q_INVOKABLE QString validate();
    QString uid();
    void setStack(StackTemplateObject stack);
    GroupUnitsEditor *stack() const;

    int rotation() const;
    void setRotation(int newRotation);
    QString name() const;
    void setName(const QString &newName);

    QString icon() const;

    bool ignore() const;
    void setIgnore(bool newIgnore);
    int priority() const;
    void setPriority(int newPriority);

    StackOrderEditor *orderEditor() const;

    EventsListModel *events() const;

    OwnerSelectModel *ownerModel() const;

public slots:
    void save();
    void saveEdit();
    void cancel();
    void startEdit();
    void onUnitDrag();
signals:
    void dataChanged();
    void loadFinished();
    void changed();
private:
private:
    StackTemplateObject m_object;
    GroupUnitsEditor *m_stack;
    QString m_icon;
    bool m_ignore;
    int m_priority;
    StackOrderEditor *m_orderEditor;
    EventsListModel *m_events;
    OwnerSelectModel *m_ownerModel;
    bool m_initState = false;
};

#endif // STACKTEMPLATEEDITOR_H

#ifndef BASEOBJECTPREVIEW_H
#define BASEOBJECTPREVIEW_H
#include <QObject>

class BaseObjectPreview : public QObject
{
    Q_OBJECT
public:
    explicit BaseObjectPreview(QObject *parent = nullptr);
    Q_PROPERTY(QString uid READ uid WRITE setValue NOTIFY changed)
    Q_PROPERTY(QString name READ getName NOTIFY changed)
    Q_PROPERTY(QString desc READ getDesc NOTIFY changed)
    Q_PROPERTY(QString image READ getImage NOTIFY changed)
    Q_PROPERTY(QString position READ getPosition NOTIFY changed)

    const QString &uid() const;
    void setValue(const QString &newUid);

    const QString &getName() const;
    const QString &getDesc() const;
    const QString &getImage() const;
    const QString &getPosition() const;

signals:

    void changed();
private:
    QString m_uid;
    QString m_name;
    QString m_desc;
    QString m_image;
    QString m_position;
};

#endif // BASEOBJECTPREVIEW_H

#include "StackTemplateEditor.h"
#include <QStringList>
#include "Engine/Map/MapStateHolder.h"
#include "Engine/GameInstance.h"

StackTemplateEditor::StackTemplateEditor(QObject *parent) : QObject(parent)
{
    m_stack = new GroupUnitsEditor(this);
    m_orderEditor = new StackOrderEditor(this);
    m_events = new EventsListModel(this);
    m_ownerModel = new OwnerSelectModel(this);
    m_object.uid = EMPTY_ID;
    connect(m_stack, SIGNAL(unitsChanged()), this, SIGNAL(dataChanged()));
    connect(m_stack, SIGNAL(dragFinished()), this, SLOT(onUnitDrag()));
    connect(m_ownerModel, SIGNAL(changed()), SLOT(save()));
    connect(m_orderEditor, SIGNAL(targetChanged()), SLOT(save()));
    connect(m_stack, SIGNAL(unitsChanged()), SLOT(save()));
    connect(this, SIGNAL(dataChanged()), this, SLOT(save()));
    RESOLVE(MapStateHolder)->startEdit();
}

void StackTemplateEditor::setUID(QPair<int, int> uid)
{
    setStack(RESOLVE(MapStateHolder)->objectByIdT<StackTemplateObject>(uid));
}

QString StackTemplateEditor::validate()
{
    if (!m_stack->hasLeader())
    {
        return "There is no leader in the squad, add him to save";
    }
    return QString();
}

QString StackTemplateEditor::uid()
{
    return QString("%1:%2").arg(m_object.uid.first).arg(m_object.uid.second);
}

void StackTemplateEditor::setUID(const QString &uid)
{
    QStringList data = uid.split(":");
    if (data.count() < 2)
        return;
    setUID(QPair<int,int>(data[0].toInt(), data[1].toInt()));
}

void StackTemplateEditor::setStack(StackTemplateObject stack)
{
    m_initState = true;
    m_object = stack;
    m_stack->setGroup(m_object.stack);
    m_orderEditor->setOrder(m_object.order);
    m_orderEditor->setOrderTarget(m_object.orderTarget);
    m_events->init(m_object.uid, EventsListModel::Both);
    m_ownerModel->init();
    m_ownerModel->setOwnerIndexByUid(m_object.subrace);
    emit dataChanged();
    emit loadFinished();
    m_initState = false;
}

GroupUnitsEditor *StackTemplateEditor::stack() const
{
    return m_stack;
}

int StackTemplateEditor::rotation() const
{
    return m_object.rotation;
}

void StackTemplateEditor::setRotation(int newRotation)
{
    if (m_object.rotation == newRotation)
        return;
    m_object.rotation = newRotation;
    save();
    emit dataChanged();
}

void StackTemplateEditor::onUnitDrag()
{
    m_stack->moveUnit(m_stack->dragSource(), m_stack, m_stack->dragTarget());
    m_stack->resetDrag();
    save();
    emit dataChanged();
}

void StackTemplateEditor::save()
{
    if (m_object.uid == EMPTY_ID || m_initState)
        return;
    m_object.stack = m_stack->group();
    m_object.order = m_orderEditor->order();
    m_object.orderTarget = m_orderEditor->orderTargetNative();
    m_object.subrace = m_ownerModel->currentSubrace();
    RESOLVE(MapStateHolder)->replaceObject(m_object);
}

void StackTemplateEditor::saveEdit()
{
    RESOLVE(MapStateHolder)->acceptEdit();
}

void StackTemplateEditor::cancel()
{
    RESOLVE(MapStateHolder)->rejectEdit();
    setUID(m_object.uid);
}

void StackTemplateEditor::startEdit()
{
    RESOLVE(MapStateHolder)->startEdit();
}

QString StackTemplateEditor::name() const
{
    return m_stack->group().name;
}

void StackTemplateEditor::setName(const QString &newName)
{
    if (m_stack->group().name == newName)
        return;
    m_stack->setName(newName);
    save();
    emit dataChanged();
}

QString StackTemplateEditor::icon() const
{
    QString leaderId = m_stack->group().leaderId();
    return "IsoAnim-IsoUnit-" + leaderId.toUpper() + "STOP" + QString::number(rotation());
}

bool StackTemplateEditor::ignore() const
{
    return m_object.ignoreAI;
}

void StackTemplateEditor::setIgnore(bool newIgnore)
{
    if (m_object.ignoreAI == newIgnore)
        return;
    m_object.ignoreAI = newIgnore;
    save();
    emit dataChanged();
}

int StackTemplateEditor::priority() const
{
    return m_object.priority;
}

void StackTemplateEditor::setPriority(int newPriority)
{
    if (m_object.priority == newPriority)
        return;
    m_object.priority = newPriority;
    save();
    emit dataChanged();
}

StackOrderEditor *StackTemplateEditor::orderEditor() const
{
    return m_orderEditor;
}

EventsListModel *StackTemplateEditor::events() const
{
    return m_events;
}

OwnerSelectModel *StackTemplateEditor::ownerModel() const
{
    return m_ownerModel;
}

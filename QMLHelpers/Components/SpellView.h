#ifndef SPELLVIEW_H
#define SPELLVIEW_H

#include <QObject>
#include "toolsqt/DBFModel/GameClases.h"

class SpellView : public QObject
{
    Q_OBJECT
public:
    explicit SpellView(QObject *parent = nullptr);
    Q_PROPERTY(QString spellId READ spellId WRITE setSpellId NOTIFY dataChanged)
    Q_PROPERTY(QString name READ name NOTIFY dataChanged)
    Q_PROPERTY(QString desc READ desc NOTIFY dataChanged)
    Q_PROPERTY(QString icon READ icon NOTIFY dataChanged)
    Q_PROPERTY(QString cost READ cost NOTIFY dataChanged)

    QString spellId() const;
    void setSpellId(const QString &newSpellId);

    QString name() const;
    QString desc() const;
    QString icon() const;
    QString cost() const;
signals:
    void dataChanged();
private:
    QSharedPointer<GSpell> m_spell;
};

#endif // SPELLVIEW_H

#ifndef QUESTSTAGEEDITOR_H
#define QUESTSTAGEEDITOR_H
#include <QObject>
#include "QMLHelpers/Editors/EditorWidget.h"
#include "QMLHelpers/Models/StringModel.h"
#include "QMLHelpers/Models/SelectMapObjectModel.h"
#include "Events/EventBus.h"

class QuestStageEditor : public QObject, public EventSubscriber
{
    Q_OBJECT
public:
    explicit QuestStageEditor(QObject *parent = nullptr);
    Q_PROPERTY(SelectMapObjectModel *  stageQuests READ stageQuests NOTIFY dataChanged)

    Q_PROPERTY(QString selectedId READ selectedId WRITE setSelectedId NOTIFY dataChanged)
    Q_PROPERTY(int stage READ stage WRITE setStage NOTIFY dataChanged)

    Q_PROPERTY(QString stageName READ stageName WRITE setStageName NOTIFY dataChanged)
    Q_PROPERTY(QString stageDesc READ stageDesc WRITE setStageDesc NOTIFY dataChanged)

    const QString selectedId() const;
    void setSelectedId(const QString &newSelectedId);

    int stage() const;
    void setStage(int newStage);

    SelectMapObjectModel *stageQuests() const;

    const QString &stageName() const;
    void setStageName(const QString &newStageName);

    const QString &stageDesc() const;
    void setStageDesc(const QString &newStageDesc);
    Q_INVOKABLE QString eventDesc(const QString& id);
    Q_INVOKABLE QString createEvent();

    void onObjectUpdated(const QVariant &event);
    void onObjectRemoved(const QVariant &event);
signals:
    void dataChanged();
private:

private:
    SelectMapObjectModel *m_stageQuests;
    QPair<int, int> m_selectedUid;
    int m_stage = 0;
    QString m_stageName;
    QString m_stageDesc;
};

#endif // QUESTSTAGEEDITOR_H

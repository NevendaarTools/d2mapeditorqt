#include "CostEditor.h"
#include <QDebug>

CostEditor::CostEditor(QObject *parent) : QAbstractListModel(parent)
{
    m_roles[ROLE_ICON] = "Icon";
    m_roles[ROLE_COUNT] = "Count";
    m_editable = false;
}

int CostEditor::rowCount(const QModelIndex &parent) const
{
    if (parent.isValid())
        return 0;
    return m_data.count();
}

QVariant CostEditor::data(const QModelIndex &index, int role) const
{
    if (role == ROLE_ICON)
    {
        return m_data[index.row()].icon;
    }
    if (role == ROLE_COUNT)
    {
        return m_data[index.row()].value;
    }
    return QVariant();
}

QHash<int, QByteArray> CostEditor::roleNames() const
{
    return m_roles;
}

QString CostEditor::value() const
{
    QString result;
    for(int i = 0; i < m_data.count(); ++i)
    {
        result += m_data[i].letter + QString::number(m_data[i].value).rightJustified(4, '0');
    }
    return result;
}

void CostEditor::setValue(const QString &newValue)
{
    if (m_value.toUpper() == newValue.toUpper())
        return;
    beginResetModel();
    m_data.clear();
    m_value = newValue.toUpper();
    if (m_editable || CostValue::hasValue(newValue, "G"))
        m_data.append(CostValue(newValue, "IntfScen-_RESOURCES_GOLD", "G"));
    if (m_editable || CostValue::hasValue(newValue, "R"))
        m_data.append(CostValue(newValue, "IntfScen-_RESOURCES_REDM_B", "R"));
    if (m_editable || CostValue::hasValue(newValue, "Y"))
        m_data.append(CostValue(newValue, "IntfScen-_RESOURCES_BLUEM_B", "Y"));
    if (m_editable || CostValue::hasValue(newValue, "E"))
        m_data.append(CostValue(newValue, "IntfScen-_RESOURCES_BLACKM_B", "E"));
    if (m_editable || CostValue::hasValue(newValue, "W"))
        m_data.append(CostValue(newValue, "IntfScen-_RESOURCES_WHITEM_B", "W"));
    if (m_editable || CostValue::hasValue(newValue, "B"))
        m_data.append(CostValue(newValue, "IntfScen-_RESOURCES_GREENM_B", "B"));
    endResetModel();
    emit valueChanged();
}

void CostEditor::setValue(int index, const QString &newValue)
{
    if (m_data[index].value != newValue.toUInt())
    {
        m_data[index].value = newValue.toUInt();
        emit valueChanged();
    }
}

bool CostEditor::editable() const
{
    return m_editable;
}

void CostEditor::setEditable(bool newEditable)
{
    if (m_editable == newEditable)
        return;
    m_editable = newEditable;
    setValue(m_value);
}

#ifndef EVENTCONVERTIONHELPER_H
#define EVENTCONVERTIONHELPER_H
#include "toolsqt/MapUtils/DataBlocks/D2Stack.h"
#include "toolsqt/MapUtils/DataBlocks/D2StackTemplate.h"
#include "MapObjects/MapObject.h"


template <class classT>
static QString stringIdToD2(const QString& editorUid)
{
    auto uid = IdFromString(editorUid);
    if (uid.first == IDataBlock::Fort_capital || uid.first == IDataBlock::Fort_village)
        uid.first = IDataBlock::Fort;
    if (uid.first == IDataBlock::Merchant_items  || uid.first == IDataBlock::Merchant_mercs ||
        uid.first == IDataBlock::Merchant_spells || uid.first == IDataBlock::Merchant_train)
        uid.first = IDataBlock::Merchant;
    return classT::shortType() + QString::number(uid.second, 16).rightJustified(4, '0').toLower();
}
template <class classT>
static QString stringIdFromD2(const QString& d2id)
{
    auto intId = IDataBlock::getIntId(d2id);
    int type = classT::intType();
    if (type == IDataBlock::Fort_capital || type == IDataBlock::Fort_village)
        type = IDataBlock::Fort;
    if (type == IDataBlock::Merchant_items  || type == IDataBlock::Merchant_mercs ||
        type == IDataBlock::Merchant_spells || type == IDataBlock::Merchant_train)
        type = IDataBlock::Merchant;
    return IdToString(QPair<int, int> (type, intId));
}

static QString stackIdFromD2(const QString& d2id)
{
    if (d2id.startsWith(D2StackTemplate::shortType()))
        return stringIdFromD2<D2StackTemplate>(d2id);
    return stringIdFromD2<D2Stack>(d2id);
}
static QString stackIdToD2(const QString& editorUid)
{
    if (editorUid.startsWith(QString::number(IDataBlock::StackTemplate)))
        return stringIdToD2<D2StackTemplate>(editorUid);
    return stringIdToD2<D2Stack>(editorUid);
}



#endif // EVENTCONVERTIONHELPER_H

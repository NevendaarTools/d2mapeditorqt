#ifndef EVENTEFFECTEDITOR_H
#define EVENTEFFECTEDITOR_H

#include <QObject>
#include <QString>
#include <QVariant>
#include <QVector>
#include "toolsqt/MapUtils/DataBlocks/D2Event.h"

class EventEffectEditor : public QObject {
    Q_OBJECT

    Q_PROPERTY(int effectType READ effectType WRITE setEffectType NOTIFY effectTypeChanged)
    Q_PROPERTY(QString uid READ uid WRITE setUid NOTIFY uidChanged)

    Q_PROPERTY(int num READ num WRITE setNum NOTIFY numChanged)
    Q_PROPERTY(QString locId READ locId WRITE setLocId NOTIFY locIdChanged)
    Q_PROPERTY(int lookup READ lookup WRITE setLookup NOTIFY lookupChanged)
    Q_PROPERTY(int value READ value WRITE setValue NOTIFY valueChanged)
    Q_PROPERTY(QString playerId1 READ playerId1 WRITE setPlayerId1 NOTIFY playerId1Changed)
    Q_PROPERTY(QString playerId2 READ playerId2 WRITE setPlayerId2 NOTIFY playerId2Changed)
    Q_PROPERTY(bool permally READ permally WRITE setPermally NOTIFY permallyChanged)
    Q_PROPERTY(QString spellType READ spellType WRITE setSpellType NOTIFY spellTypeChanged)
    Q_PROPERTY(QString lmarkId READ lmarkId WRITE setLmarkId NOTIFY lmarkIdChanged)
    Q_PROPERTY(QString lmarkType READ lmarkType WRITE setLmarkType NOTIFY lmarkTypeChanged)
    Q_PROPERTY(int diplomacy READ diplomacy WRITE setDiplomacy NOTIFY diplomacyChanged)
    Q_PROPERTY(bool firstOnly READ firstOnly WRITE setFirstOnly NOTIFY firstOnlyChanged)
    Q_PROPERTY(bool enabled READ enabled WRITE setEnabled NOTIFY enabledChanged)
    Q_PROPERTY(bool anim READ anim WRITE setAnim NOTIFY animChanged)
    Q_PROPERTY(QString text READ text WRITE setText NOTIFY textChanged)
    Q_PROPERTY(QString stackId READ stackId WRITE setStackId NOTIFY stackIdChanged)
    Q_PROPERTY(QString orderTarget READ orderTarget WRITE setOrderTarget NOTIFY orderTargetChanged)
    Q_PROPERTY(int order READ order WRITE setOrder NOTIFY orderChanged)
    Q_PROPERTY(QString templateId READ templateId WRITE setTemplateId NOTIFY templateIdChanged)
    Q_PROPERTY(QString itemType READ itemType WRITE setItemType NOTIFY itemTypeChanged)
    Q_PROPERTY(bool triggerOnly READ triggerOnly WRITE setTriggerOnly NOTIFY triggerOnlyChanged)
    Q_PROPERTY(QString music READ music WRITE setMusic NOTIFY musicChanged)
    Q_PROPERTY(QString sound READ sound WRITE setSound NOTIFY soundChanged)
    Q_PROPERTY(QString image READ image WRITE setImage NOTIFY imageChanged)
    Q_PROPERTY(QString image2 READ image2 WRITE setImage2 NOTIFY image2Changed)
    Q_PROPERTY(bool leftSide READ leftSide WRITE setLeftSide NOTIFY leftSideChanged)
    Q_PROPERTY(QString popupShow READ popupShow WRITE setPopupShow NOTIFY popupShowChanged)
    Q_PROPERTY(bool boolValue READ boolValue WRITE setBoolValue NOTIFY boolValueChanged)
    Q_PROPERTY(QString eventId READ eventId WRITE setEventId NOTIFY eventIdChanged)
    Q_PROPERTY(int giveTo READ giveTo WRITE setGiveTo NOTIFY giveToChanged)
    Q_PROPERTY(int val1 READ val1 WRITE setVal1 NOTIFY val1Changed)
    Q_PROPERTY(int val2 READ val2 WRITE setVal2 NOTIFY val2Changed)
    Q_PROPERTY(bool win READ win WRITE setWin NOTIFY winChanged)
    Q_PROPERTY(QVector<QString> events READ events WRITE setEvents NOTIFY eventsChanged)

public:
    explicit EventEffectEditor(QObject *parent = nullptr);

    Q_INVOKABLE void saveEvent();
    // Геттеры и сеттеры для всех свойств
    int num() const { return m_num; }
    void setNum(int num) {
        if (m_num != num) {
            m_num = num;
            emit numChanged();
        }
    }

    QString locId() const { return m_locId; }
    void setLocId(const QString &locId) {
        if (m_locId != locId) {
            m_locId = locId;
            emit locIdChanged();
        }
    }

    int lookup() const { return m_lookup; }
    void setLookup(int lookup) {
        if (m_lookup != lookup) {
            m_lookup = lookup;
            emit lookupChanged();
        }
    }

    int value() const { return m_value; }
    void setValue(int value) {
        if (m_value != value) {
            m_value = value;
            emit valueChanged();
        }
    }

    QString playerId1() const { return m_playerId1; }
    void setPlayerId1(const QString &playerId1) {
        if (m_playerId1 != playerId1) {
            m_playerId1 = playerId1;
            emit playerId1Changed();
        }
    }

    QString playerId2() const { return m_playerId2; }
    void setPlayerId2(const QString &playerId2) {
        if (m_playerId2 != playerId2) {
            m_playerId2 = playerId2;
            emit playerId2Changed();
        }
    }

    bool permally() const { return m_permally; }
    void setPermally(bool permally) {
        if (m_permally != permally) {
            m_permally = permally;
            emit permallyChanged();
        }
    }

    QString spellType() const { return m_spellType; }
    void setSpellType(const QString &spellType) {
        if (m_spellType != spellType) {
            m_spellType = spellType;
            emit spellTypeChanged();
        }
    }

    QString lmarkId() const { return m_lmarkId; }
    void setLmarkId(const QString &lmarkId) {
        if (m_lmarkId != lmarkId) {
            m_lmarkId = lmarkId;
            emit lmarkIdChanged();
        }
    }

    QString lmarkType() const { return m_lmarkType; }
    void setLmarkType(const QString &lmarkType) {
        if (m_lmarkType != lmarkType) {
            m_lmarkType = lmarkType;
            emit lmarkTypeChanged();
        }
    }

    int diplomacy() const { return m_diplomacy; }
    void setDiplomacy(int diplomacy) {
        if (m_diplomacy != diplomacy) {
            m_diplomacy = diplomacy;
            emit diplomacyChanged();
        }
    }

    bool enabled() const { return m_enabled; }
    void setEnabled(bool enabled) {
        if (m_enabled != enabled) {
            m_enabled = enabled;
            emit enabledChanged();
        }
    }

    QString text() const { return m_text; }
    void setText(const QString &text) {
        if (m_text != text) {
            m_text = text;
            emit textChanged();
        }
    }

    QString stackId() const { return m_stackId; }
    void setStackId(const QString &stackId) {
        if (m_stackId != stackId) {
            m_stackId = stackId;
            emit stackIdChanged();
        }
    }

    QString orderTarget() const { return m_orderTarget; }
    void setOrderTarget(const QString &orderTarget) {
        if (m_orderTarget != orderTarget) {
            m_orderTarget = orderTarget;
            emit orderTargetChanged();
        }
    }

    int order() const { return m_order; }
    void setOrder(int order) {
        if (m_order != order) {
            m_order = order;
            emit orderChanged();
        }
    }

    QString templateId() const { return m_templateId; }
    void setTemplateId(const QString &templateId) {
        if (m_templateId != templateId) {
            m_templateId = templateId;
            emit templateIdChanged();
        }
    }

    QString itemType() const { return m_itemType; }
    void setItemType(const QString &itemType) {
        if (m_itemType != itemType) {
            m_itemType = itemType;
            emit itemTypeChanged();
        }
    }

    bool triggerOnly() const { return m_triggerOnly; }
    void setTriggerOnly(bool triggerOnly) {
        if (m_triggerOnly != triggerOnly) {
            m_triggerOnly = triggerOnly;
            emit triggerOnlyChanged();
        }
    }

    QString music() const { return m_music; }
    void setMusic(const QString &music) {
        if (m_music != music) {
            m_music = music;
            emit musicChanged();
        }
    }

    QString sound() const { return m_sound; }
    void setSound(const QString &sound) {
        if (m_sound != sound) {
            m_sound = sound;
            emit soundChanged();
        }
    }

    QString image() const { return m_image; }
    void setImage(const QString &image) {
        if (m_image != image) {
            m_image = image;
            emit imageChanged();
        }
    }

    QString image2() const { return m_image2; }
    void setImage2(const QString &image2) {
        if (m_image2 != image2) {
            m_image2 = image2;
            emit image2Changed();
        }
    }

    bool leftSide() const { return m_leftSide; }
    void setLeftSide(bool leftSide) {
        if (m_leftSide != leftSide) {
            m_leftSide = leftSide;
            emit leftSideChanged();
        }
    }

    QString popupShow() const { return m_popupShow; }
    void setPopupShow(const QString &popupShow) {
        if (m_popupShow != popupShow) {
            m_popupShow = popupShow;
            emit popupShowChanged();
        }
    }

    bool boolValue() const { return m_boolValue; }
    void setBoolValue(bool boolValue) {
        if (m_boolValue != boolValue) {
            m_boolValue = boolValue;
            emit boolValueChanged();
        }
    }

    QString eventId() const { return m_eventId; }
    void setEventId(const QString &eventId) {
        if (m_eventId != eventId) {
            m_eventId = eventId;
            emit eventIdChanged();
        }
    }

    int giveTo() const { return m_giveTo; }
    void setGiveTo(int giveTo) {
        if (m_giveTo != giveTo) {
            m_giveTo = giveTo;
            emit giveToChanged();
        }
    }

    int val1() const { return m_val1; }
    void setVal1(int val1) {
        if (m_val1 != val1) {
            m_val1 = val1;
            emit val1Changed();
        }
    }

    int val2() const { return m_val2; }
    void setVal2(int val2) {
        if (m_val2 != val2) {
            m_val2 = val2;
            emit val2Changed();
        }
    }

    bool win() const { return m_win; }
    void setWin(bool win) {
        if (m_win != win) {
            m_win = win;
            emit winChanged();
        }
    }

    QVector<QString> events() const { return m_events; }
    void setEvents(const QVector<QString> &events) {
        if (m_events != events) {
            m_events = events;
            emit eventsChanged();
        }
    }

    QString uid() const;
    void setUid(const QString &newUid);

    int effectType() const;
    void setEffectType(int newEffectType);

    bool firstOnly() const;
    void setFirstOnly(bool newFirstOnly);

    bool anim() const;
    void setAnim(bool newAnim);

signals:
    void numChanged();
    void locIdChanged();
    void lookupChanged();
    void valueChanged();
    void playerId1Changed();
    void playerId2Changed();
    void permallyChanged();
    void spellTypeChanged();
    void lmarkIdChanged();
    void lmarkTypeChanged();
    void diplomacyChanged();
    void enabledChanged();
    void textChanged();
    void stackIdChanged();
    void orderTargetChanged();
    void orderChanged();
    void templateIdChanged();
    void itemTypeChanged();
    void triggerOnlyChanged();
    void musicChanged();
    void soundChanged();
    void imageChanged();
    void image2Changed();
    void leftSideChanged();
    void popupShowChanged();
    void boolValueChanged();
    void eventIdChanged();
    void giveToChanged();
    void val1Changed();
    void val2Changed();
    void boolValChanged();
    void winChanged();
    void eventsChanged();

    void uidChanged();
    void effectTypeChanged();
    void firstOnlyChanged();

    void animChanged();

private:
    void fillFromEffect();
    void updateEffect();
private:
    int m_num;
    QString m_locId;
    int m_lookup;
    int m_value;
    QString m_playerId1;
    QString m_playerId2;
    bool m_permally;
    QString m_spellType;
    QString m_lmarkId;
    QString m_lmarkType;
    int m_diplomacy;
    bool m_enabled;
    QString m_text;
    QString m_stackId;
    QString m_orderTarget;
    int m_order;
    QString m_templateId;
    QString m_itemType;
    bool m_triggerOnly;
    QString m_music;
    QString m_sound;
    QString m_image;
    QString m_image2;
    bool m_leftSide;
    QString m_popupShow;
    bool m_boolValue;
    QString m_eventId;
    int m_giveTo;
    int m_val1;
    int m_val2;
    bool m_win;
    QVector<QString> m_events;

    D2Event::EventEffect m_effect;
    QPair<int, int> eventUId;
    int m_effectIndex = 0;
    bool m_firstOnly;
    bool m_anim;
};


#endif // EVENTEFFECTEDITOR_H

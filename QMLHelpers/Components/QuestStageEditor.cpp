#include "QuestStageEditor.h"
#include <QStringList>
#include "MapObjects/QuestLine.h"
#include "Engine/GameInstance.h"
#include "Engine/Components/AccessorHolder.h"
#include "Events/MapEvents.h"

QuestStageEditor::QuestStageEditor(QObject *parent) : QObject(parent)
{
    m_stageQuests = new SelectMapObjectModel(this);
    m_selectedUid = EMPTY_ID;
    subscribe<ObjectChangedEvent>([this](const QVariant &event){onObjectUpdated(event);});
    subscribe<ObjectRemovedEvent>([this](const QVariant &event){onObjectRemoved(event);});
}

const QString QuestStageEditor::selectedId() const
{
    return QString("%1:%2").arg(m_selectedUid.first).arg(m_selectedUid.second);
}

void QuestStageEditor::setSelectedId(const QString &newSelectedId)
{
    m_selectedUid = IdFromString(newSelectedId);
    setStage(0);
    emit dataChanged();
}

int QuestStageEditor::stage() const
{
    return m_stage;
}

void QuestStageEditor::setStage(int newStage)
{
//    if (m_stage == newStage)
//        return;
    m_stage = newStage;

    auto getText = [](const QSharedPointer<MapObject>& item)
    {
        EventObject * quest = static_cast<EventObject*>(item.data());
        return quest->name;
    };
    auto getId = [](const QSharedPointer<MapObject>& item)
    {
        EventObject * quest = static_cast<EventObject*>(item.data());
        return quest->uid;
    };
    QList<QSharedPointer<MapObject>> quests;
    if (m_selectedUid == EMPTY_ID)
        return;
    QuestLine line = RESOLVE(MapStateHolder)->objectByIdT<QuestLine>(m_selectedUid);
    m_stageName = "";
    m_stageDesc = "";
    if (line.stages.count() > m_stage)
    {
        QuestLine::QuestStage stage = line.stages[m_stage];
        for (int i = 0; i < stage.events.count(); ++i)
        {
            auto tmp = RESOLVE(MapStateHolder)->objectById(stage.events[i]);
            quests << tmp;
        }
        m_stageName = stage.name;
        m_stageDesc = stage.stageText;
    }
    m_stageQuests->init<QuestLine>(getText, getId, quests);
    emit dataChanged();
}

SelectMapObjectModel *QuestStageEditor::stageQuests() const
{
    return m_stageQuests;
}

const QString &QuestStageEditor::stageName() const
{
    return m_stageName;
}

void QuestStageEditor::setStageName(const QString &newStageName)
{
    if (m_stageName == newStageName)
        return;
    m_stageName = newStageName;
    if (m_selectedUid == EMPTY_ID)
        return;
    QuestLine line = RESOLVE(MapStateHolder)->objectByIdT<QuestLine>(m_selectedUid);
    while (line.stages.count() <= m_stage)
    {
        line.stages << QuestLine::QuestStage();
    }
    line.stages[m_stage].name = newStageName;
    RESOLVE(MapStateHolder)->replaceObject(line);
    emit dataChanged();
}

const QString &QuestStageEditor::stageDesc() const
{
    return m_stageDesc;
}

void QuestStageEditor::setStageDesc(const QString &newStageDesc)
{
    if (m_stageDesc == newStageDesc)
        return;
    m_stageDesc = newStageDesc;
    if (m_selectedUid == EMPTY_ID)
        return;
    QuestLine line = RESOLVE(MapStateHolder)->objectByIdT<QuestLine>(m_selectedUid);
    while (line.stages.count() <= m_stage)
    {
        line.stages << QuestLine::QuestStage();
    }
    line.stages[m_stage].stageText = newStageDesc;
    RESOLVE(MapStateHolder)->replaceObject(line);
    emit dataChanged();
}

QString QuestStageEditor::eventDesc(const QString &id)
{
    auto uid = IdFromString(id);
    auto event = RESOLVE(MapStateHolder)->objectByIdT<EventObject>(uid);
    QString desc = TranslationHelper::tr("Conditions:");
    for (int k = 0; k < event.conditions.count(); ++k)
    {
        desc += "\n[" + QString::number(k) + "]    " +
                TranslationHelper::tr(event.conditions[k].condition.toString());
        if (event.conditions[k].targets.count() > 0)
        {
            foreach (auto target , event.conditions[k].targets)
            {
                QSharedPointer<MapObject> mapObj =
                        RESOLVE(MapStateHolder)->objectById(target);
                QSharedPointer<IMapObjectAccessor> accessor =
                        RESOLVE(AccessorHolder)->objectAccessor(target.first);
                desc+="[ ";
                if (mapObj.isNull())
                    desc += "null object";
                if (accessor.isNull())
                    desc += "null accessor";
                if (!accessor.isNull() && !mapObj.isNull()){

                    desc += accessor->getName(mapObj) + " ";
                    if (mapObj->populate)
                        desc += QString("[%1:%2] ").arg(mapObj->x).arg(mapObj->y);
                }
                desc += "]";
            }
        }
    }
    desc += "\n" +  TranslationHelper::tr("Effects:");
    for (int k = 0; k < event.effects.count(); ++k)
    {
        desc += "\n";
        desc += EventObject::effectToString(event.effects[k].effect);
        if (event.effects[k].targets.count() > 0)
        {
            foreach (auto target , event.effects[k].targets)
            {
                QSharedPointer<MapObject> mapObj =
                        RESOLVE(MapStateHolder)->objectById(target);
                QSharedPointer<IMapObjectAccessor> accessor =
                        RESOLVE(AccessorHolder)->objectAccessor(target.first);

                desc+="[ ";
                if (mapObj.isNull())
                    desc += "null object";
                if (accessor.isNull())
                    desc += "null accessor";
                if (!accessor.isNull() && !mapObj.isNull()){

                    desc += accessor->getName(mapObj) + " ";
                    if (mapObj->populate)
                        desc += QString("[%1:%2] ").arg(mapObj->x).arg(mapObj->y);
                }
                desc += "]";
            }

        }
    }
    return desc;
}

QString QuestStageEditor::createEvent()
{
    EventObject obj;
    QuestLine line = RESOLVE(MapStateHolder)->objectByIdT<QuestLine>(m_selectedUid);
    obj.lineId = line.uid;
    auto res = RESOLVE(MapStateHolder)->addObject<>(obj);
    while (line.stages.count() <= m_stage)
    {
        line.stages << QuestLine::QuestStage();
    }
    line.stages[m_stage].events << res;
    RESOLVE(MapStateHolder)->replaceObject(line);
    setStage(m_stage);
    return IdToString(res);
}

void QuestStageEditor::onObjectUpdated(const QVariant &event)
{
    ObjectChangedEvent command = event.value<ObjectChangedEvent>();
    if (command.uid.first == MapObject::Type::QuestLine){
        setStage(m_stage);
    }
    if (command.uid.first == MapObject::Type::Event){
        setStage(m_stage);
    }
}

void QuestStageEditor::onObjectRemoved(const QVariant &event)
{
    ObjectRemovedEvent command = event.value<ObjectRemovedEvent>();
    if (command.uid.first == MapObject::Type::Event){
        setStage(m_stage);
    }
}


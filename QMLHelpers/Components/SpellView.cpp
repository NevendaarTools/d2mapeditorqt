#include "SpellView.h"
#include "Engine/GameInstance.h"
#include "toolsqt/DBFModel/DBFModel.h"

SpellView::SpellView(QObject *parent) : QObject(parent)
{

}

QString SpellView::spellId() const
{
    if (m_spell.isNull())
        return "";
    return m_spell->spell_id;
}

void SpellView::setSpellId(const QString & newItemId)
{
    m_spell = RESOLVE(DBFModel)->get<GSpell>(newItemId);
    emit dataChanged();
}

QString SpellView::name() const
{
    if (m_spell.isNull())
        return "";
    return m_spell->name_txt->text;
}

QString SpellView::desc() const
{
    if (m_spell.isNull())
        return "";
    QString resDesc = m_spell->desc_txt->text;
    resDesc.replace("\\n","<br>");
    return resDesc;
}

QString SpellView::icon() const
{
    if (m_spell.isNull())
        return "";
    return "IconSpel-" + m_spell->spell_id.toUpper();
}

QString SpellView::cost() const
{
    if (m_spell.isNull())
        return "";
    return m_spell->casting_c;
}

#include "EventEditor.h"
#include "MapObjects/UnitObject.h"
#include "Engine/GameInstance.h"
#include "Engine/Map/MapStateHolder.h"
#include "Engine/Components/AccessorHolder.h"
#include "Engine/Components/TranslationHelper.h"

EventEditor::EventEditor(QObject *parent) : QObject(parent)
{
    m_conditions = new StringModel(this);
    m_effects = new StringModel(this);
    m_availableConditions = new StringModel(this);
    m_availableEffects = new StringModel(this);
    updateAvailable();
}

bool EventEditor::activatedByRace(int index) const
{
    return m_event.activatedBy[index];
}

void EventEditor::setActivatedByRace(int index)
{
    m_event.activatedBy[index] = true;
}

void EventEditor::moveEffect(int index, int value)
{
    qDebug()<<Q_FUNC_INFO<<index<<value;
    std::swap(m_event.conditions[index], m_event.conditions[index + value]);
    updateEffects();
}

void EventEditor::moveCondition(int index, int value)
{
    qDebug()<<Q_FUNC_INFO<<index<<value;
    std::swap(m_event.conditions[index], m_event.conditions[index + value]);
    updateConditions();
}

void EventEditor::removeEffect(int index)
{
    m_event.effects.removeAt(index);
    updateEffects();
}

void EventEditor::cloneEffect(int index)
{
    auto effect = m_event.effects.at(index);
    m_event.effects << effect;
    updateEffects();
}

void EventEditor::cloneCondition(int index)
{
    auto effect = m_event.conditions.at(index);
    m_event.conditions << effect;
    updateConditions();
}

void EventEditor::removeCondition(int index)
{
    m_event.conditions.removeAt(index);
    updateConditions();
}

void EventEditor::save()
{
    RESOLVE(MapStateHolder)->replaceObject(m_event);
}

void EventEditor::cancel()
{
    qDebug()<<"#######"<<m_origEvent.conditions.count() + m_origEvent.effects.count() << m_origEvent.lineId;
    if ((m_origEvent.conditions.count() + m_origEvent.effects.count()) == 0)
    {
        if (m_origEvent.lineId != EMPTY_ID)
        {
            QuestLine line = RESOLVE(MapStateHolder)->objectByIdT<QuestLine>(m_origEvent.lineId);
            for (int i = 0; i < line.stages.count(); ++i)
            {
                line.stages[i].events.removeAll(m_origEvent.uid);
            }
            RESOLVE(MapStateHolder)->replaceObject(line);
            RESOLVE(MapStateHolder)->removeObject(m_origEvent.uid);
        }
    }
    else
    {
        RESOLVE(MapStateHolder)->replaceObject(m_origEvent);
    }
}

void EventEditor::setUID(const QString &uid)
{
    QStringList data = uid.split(":");
    if (data.count() < 2)
        return;
    m_event = RESOLVE(MapStateHolder)->objectByIdT<EventObject>(QPair<int,int>(data[0].toInt(), data[1].toInt()));
    m_origEvent = RESOLVE(MapStateHolder)->objectByIdT<EventObject>(QPair<int,int>(data[0].toInt(), data[1].toInt()));

    updateConditions();
    updateEffects();
    emit changed();
}

QString EventEditor::getUID()
{
    return IdToString(m_event.uid);
}

StringModel *EventEditor::effects() const
{
    return m_effects;
}

StringModel *EventEditor::conditions() const
{
    return m_conditions;
}

bool EventEditor::occurInfinite() const
{
    return !m_event.occurOnce;
}

void EventEditor::setOccurInfinite(bool newOccurInfinite)
{
    if (m_event.occurOnce == !newOccurInfinite)
        return;
    m_event.occurOnce = !newOccurInfinite;
    emit changed();
}

bool EventEditor::enabledFromStart() const
{
    return m_event.enabled;
}

void EventEditor::setEnabledFromStart(bool newEnabledFromStart)
{
    if (m_event.enabled == newEnabledFromStart)
        return;
    m_event.enabled = newEnabledFromStart;
    emit changed();
}

int EventEditor::chance() const
{
    return m_event.chance;
}

void EventEditor::setChance(int newChance)
{
    if (m_event.chance == newChance)
        return;
    m_event.chance = newChance;
    emit changed();
}

const QString EventEditor::name() const
{
    return m_event.name;
}

void EventEditor::setName(const QString &newName)
{
    if (m_event.name == newName)
        return;
    m_event.name = newName;
    emit changed();
}

void EventEditor::updateEffects()
{
    QSharedPointer<IStringDataProvider> provider = m_effects->provider();
    if (provider.isNull())
        provider = QSharedPointer<IStringDataProvider> (new SimpleStringProvider());
    SimpleStringProvider * prov = dynamic_cast<SimpleStringProvider*>(provider.data());
    prov->clear();
    for (int k = 0; k < m_event.effects.count(); ++k)
    {
        QString name = EventObject::effectToString(m_event.effects[k]);
        prov->addRow(name, name);
    }
    m_effects->setProvider(provider);
}

void EventEditor::updateConditions()
{
    QSharedPointer<IStringDataProvider> provider = m_conditions->provider();
    if (provider.isNull())
        provider = QSharedPointer<IStringDataProvider> (new SimpleStringProvider());
    SimpleStringProvider * prov = dynamic_cast<SimpleStringProvider*>(provider.data());
    prov->clear();
    for (int k = 0; k < m_event.conditions.count(); ++k)
    {
        QString name, desc;

        name = TranslationHelper::tr(m_event.conditions[k].condition.toString());
        if (m_event.conditions[k].targets.count() > 0)
        {
            foreach (auto target , m_event.conditions[k].targets)
            {
                QSharedPointer<MapObject> mapObj =
                        RESOLVE(MapStateHolder)->objectById(target);
                QSharedPointer<IMapObjectAccessor> accessor =
                        RESOLVE(AccessorHolder)->objectAccessor(target.first);
                desc+="[ ";
                if (mapObj.isNull())
                    desc += "null object";
                if (accessor.isNull())
                    desc += "null accessor";
                if (!accessor.isNull() && !mapObj.isNull()){

                    desc += accessor->getName(mapObj) + " ";
                    if (mapObj->populate)
                        desc += QString("[%1:%2] ").arg(mapObj->x).arg(mapObj->y);
                }
                desc += "]";
            }
        }
        prov->addRow(name, desc);
    }
    m_conditions->setProvider(provider);
}

StringModel *EventEditor::availableConditions() const
{
    return m_availableConditions;
}

StringModel *EventEditor::availableEffects() const
{
    return m_availableEffects;
}

void EventEditor::addCondition(int code)
{
    EventObject::EventCondition condition;
    D2Event::EventCondition d2condition;

    switch (code) {
        case D2Event::EventCondition::FREQUENCY:
            d2condition.category = D2Event::EventCondition::FREQUENCY;
            d2condition.condition = D2Event::EventCondition::ConditionFrequency{};
            break;
        case D2Event::EventCondition::ENTERING_A_PREDEFINED_ZONE:
            d2condition.category = D2Event::EventCondition::ENTERING_A_PREDEFINED_ZONE;
            d2condition.condition = D2Event::EventCondition::ConditionEnterZone{};
            break;
        case D2Event::EventCondition::ENTERING_A_CITY:
            d2condition.category = D2Event::EventCondition::ENTERING_A_CITY;
            d2condition.condition = D2Event::EventCondition::ConditionEnterCity{};
            break;
        case D2Event::EventCondition::OWNING_A_CITY:
            d2condition.category = D2Event::EventCondition::OWNING_A_CITY;
            d2condition.condition = D2Event::EventCondition::ConditionOwningCity{};
            break;
        case D2Event::EventCondition::DESTROY_STACK:
            d2condition.category = D2Event::EventCondition::DESTROY_STACK;
            d2condition.condition = D2Event::EventCondition::ConditionDestroyStack{};
            break;
        case D2Event::EventCondition::OWNING_AN_ITEM:
            d2condition.category = D2Event::EventCondition::OWNING_AN_ITEM;
            d2condition.condition = D2Event::EventCondition::ConditionOwningItem{};
            break;
        case D2Event::EventCondition::SPECIFIC_LEADER_OWNING_AN_ITEM:
            d2condition.category = D2Event::EventCondition::SPECIFIC_LEADER_OWNING_AN_ITEM;
            d2condition.condition = D2Event::EventCondition::ConditionLeaderOwningItem{};
            break;
        case D2Event::EventCondition::DIPLOMACY_RELATIONS:
            d2condition.category = D2Event::EventCondition::DIPLOMACY_RELATIONS;
            d2condition.condition = D2Event::EventCondition::ConditionDiplomacyRelation{};
            break;
        case D2Event::EventCondition::ALLIANCE:
            d2condition.category = D2Event::EventCondition::ALLIANCE;
            d2condition.condition = D2Event::EventCondition::ConditionAlliance{};
            break;
        case D2Event::EventCondition::LOOTING_A_RUIN:
            d2condition.category = D2Event::EventCondition::LOOTING_A_RUIN;
            d2condition.condition = D2Event::EventCondition::ConditionLootingRuin{};
            break;
        case D2Event::EventCondition::TRANSFORMING_LAND:
            d2condition.category = D2Event::EventCondition::TRANSFORMING_LAND;
            d2condition.condition = D2Event::EventCondition::ConditionTransformLand{};
            break;
        case D2Event::EventCondition::VISITING_A_SITE:
            d2condition.category = D2Event::EventCondition::VISITING_A_SITE;
            d2condition.condition = D2Event::EventCondition::ConditionVisitingSite{};
            break;
        case D2Event::EventCondition::STACK_IN_LOCATION:
            d2condition.category = D2Event::EventCondition::STACK_IN_LOCATION;
            d2condition.condition = D2Event::EventCondition::ConditionStackInLocation{};
            break;
        case D2Event::EventCondition::STACK_IN_CITY:
            d2condition.category = D2Event::EventCondition::STACK_IN_CITY;
            d2condition.condition = D2Event::EventCondition::ConditionStackInCity{};
            break;
        case D2Event::EventCondition::ITEM_TO_LOCATION:
            d2condition.category = D2Event::EventCondition::ITEM_TO_LOCATION;
            d2condition.condition = D2Event::EventCondition::ConditionItemToLocation{};
            break;
        case D2Event::EventCondition::STACK_EXISTANCE:
            d2condition.category = D2Event::EventCondition::STACK_EXISTANCE;
            d2condition.condition = D2Event::EventCondition::ConditionStackExist{};
            break;
        case D2Event::EventCondition::VARIABLE_IS_IN_RANGE:
            d2condition.category = D2Event::EventCondition::VARIABLE_IS_IN_RANGE;
            d2condition.condition = D2Event::EventCondition::ConditionVarInRange{};
            break;
        case D2Event::EventCondition::RESOURCE_AMOUNT:
            d2condition.category = D2Event::EventCondition::RESOURCE_AMOUNT;
            d2condition.condition = D2Event::EventCondition::ConditionResourceAmount{};
            break;
        case D2Event::EventCondition::CHECK_GAMEMODE:
            d2condition.category = D2Event::EventCondition::CHECK_GAMEMODE;
            d2condition.condition = D2Event::EventCondition::ConditionGameMode{};
            break;
        case D2Event::EventCondition::CHECK_FOR_HUMAN:
            d2condition.category = D2Event::EventCondition::CHECK_FOR_HUMAN;
            d2condition.condition = D2Event::EventCondition::ConditionPlayerType{};
            break;
        case D2Event::EventCondition::COMPARE_VAR:
            d2condition.category = D2Event::EventCondition::COMPARE_VAR;
            d2condition.condition = D2Event::EventCondition::ConditionCompareVar{};
            break;
        case D2Event::EventCondition::CUSTOM_SCRIPT:
            d2condition.category = D2Event::EventCondition::CUSTOM_SCRIPT;
            d2condition.condition = D2Event::EventCondition::ConditionCustomScript{};
            break;
        default:
            qDebug() << "Unknown condition code:" << code;
            return;
    }

    condition.condition = d2condition;
    m_event.conditions << condition;
    RESOLVE(MapStateHolder)->replaceObject(m_event);
    updateConditions();
}

void EventEditor::addEffect(int code)
{
    EventObject::EventEffect effect;
    D2Event::EventEffect d2effect;

    switch (code) {
        case D2Event::EventEffect::WIN_OR_LOSE_SCENARIO:
            d2effect.category = D2Event::EventEffect::WIN_OR_LOSE_SCENARIO;
            d2effect.effect = D2Event::EventEffect::WinLooseEffect{};
            break;
        case D2Event::EventEffect::CREATE_NEW_STACK:
            d2effect.category = D2Event::EventEffect::CREATE_NEW_STACK;
            d2effect.effect = D2Event::EventEffect::CreateStackEffect{};
            break;
        case D2Event::EventEffect::CAST_SPELL_ON_TRIGGERER:
            d2effect.category = D2Event::EventEffect::CAST_SPELL_ON_TRIGGERER;
            d2effect.effect = D2Event::EventEffect::CastSpellOnTriggerEffect{};
            break;
        case D2Event::EventEffect::CAST_SPELL_AT_SPECIFIC_LOCATION:
            d2effect.category = D2Event::EventEffect::CAST_SPELL_AT_SPECIFIC_LOCATION;
            d2effect.effect = D2Event::EventEffect::CastSpellOnLocationEffect{};
            break;
        case D2Event::EventEffect::CHANGE_STACK_OWNER:
            d2effect.category = D2Event::EventEffect::CHANGE_STACK_OWNER;
            d2effect.effect = D2Event::EventEffect::ChangeStackOwnerEffect{};
            break;
        case D2Event::EventEffect::MOVE_STACK_NEXT_TO_TRIGGERER:
            d2effect.category = D2Event::EventEffect::MOVE_STACK_NEXT_TO_TRIGGERER;
            d2effect.effect = D2Event::EventEffect::MoveStackToTriggererEffect{};
            break;
        case D2Event::EventEffect::GO_INTO_BATTLE:
            d2effect.category = D2Event::EventEffect::GO_INTO_BATTLE;
            d2effect.effect = D2Event::EventEffect::GoBattleEffect{};
            break;
        case D2Event::EventEffect::ENABLE_DISABLE_ANOTHER_EVENT:
            d2effect.category = D2Event::EventEffect::ENABLE_DISABLE_ANOTHER_EVENT;
            d2effect.effect = D2Event::EventEffect::DisableEventEffect{};
            break;
        case D2Event::EventEffect::GIVE_SPELL:
            d2effect.category = D2Event::EventEffect::GIVE_SPELL;
            d2effect.effect = D2Event::EventEffect::GiveSpellEffect{};
            break;
        case D2Event::EventEffect::GIVE_ITEM:
            d2effect.category = D2Event::EventEffect::GIVE_ITEM;
            d2effect.effect = D2Event::EventEffect::GiveItemEffect{};
            break;
        case D2Event::EventEffect::MOVE_STACK_TO_SPECIFIC_LOCATION:
            d2effect.category = D2Event::EventEffect::MOVE_STACK_TO_SPECIFIC_LOCATION;
            d2effect.effect = D2Event::EventEffect::MoveStackToLocationEffect{};
            break;
        case D2Event::EventEffect::ALLY_TWO_AI_PLAYERS:
            d2effect.category = D2Event::EventEffect::ALLY_TWO_AI_PLAYERS;
            d2effect.effect = D2Event::EventEffect::AllyAIPlayersEffect{};
            break;
        case D2Event::EventEffect::CHANGE_PLAYER_DIPLOMACY_METER:
            d2effect.category = D2Event::EventEffect::CHANGE_PLAYER_DIPLOMACY_METER;
            d2effect.effect = D2Event::EventEffect::ChangeDiplomacyEffect{};
            break;
        case D2Event::EventEffect::UNFOG_OR_FOG_AN_AREA_ON_THE_MAP:
            d2effect.category = D2Event::EventEffect::UNFOG_OR_FOG_AN_AREA_ON_THE_MAP;
            d2effect.effect = D2Event::EventEffect::ChangeFogEffect{};
            break;
        case D2Event::EventEffect::REMOVE_MOUNTAINS_AROUND_A_LOCATION:
            d2effect.category = D2Event::EventEffect::REMOVE_MOUNTAINS_AROUND_A_LOCATION;
            d2effect.effect = D2Event::EventEffect::RemoveMountainsEffect{};
            break;
        case D2Event::EventEffect::REMOVE_LANDMARK:
            d2effect.category = D2Event::EventEffect::REMOVE_LANDMARK;
            d2effect.effect = D2Event::EventEffect::RemoveLandMarkEffect{};
            break;
        case D2Event::EventEffect::CHANGE_SCENARIO_OBJECTIVE_TEXT:
            d2effect.category = D2Event::EventEffect::CHANGE_SCENARIO_OBJECTIVE_TEXT;
            d2effect.effect = D2Event::EventEffect::ChangeScenarioTextEffect{};
            break;
        case D2Event::EventEffect::DISPLAY_POPUP_MESSAGE:
            d2effect.category = D2Event::EventEffect::DISPLAY_POPUP_MESSAGE;
            d2effect.effect = D2Event::EventEffect::DisplayPopupEffect{};
            break;
        case D2Event::EventEffect::CHANGE_STACK_ORDER:
            d2effect.category = D2Event::EventEffect::CHANGE_STACK_ORDER;
            d2effect.effect = D2Event::EventEffect::ChangeStackOrderEffect{};
            break;
        case D2Event::EventEffect::DESTROY_ITEM:
            d2effect.category = D2Event::EventEffect::DESTROY_ITEM;
            d2effect.effect = D2Event::EventEffect::DestroyItemEffect{};
            break;
        case D2Event::EventEffect::REMOVE_STACK:
            d2effect.category = D2Event::EventEffect::REMOVE_STACK;
            d2effect.effect = D2Event::EventEffect::RemoveStackEffect{};
            break;
        case D2Event::EventEffect::CHANGE_LANDMARK:
            d2effect.category = D2Event::EventEffect::CHANGE_LANDMARK;
            d2effect.effect = D2Event::EventEffect::ChangeLandmarkEffect{};
            break;
        case D2Event::EventEffect::CHANGE_TERRAIN:
            d2effect.category = D2Event::EventEffect::CHANGE_TERRAIN;
            d2effect.effect = D2Event::EventEffect::ChangeTerrainEffect{};
            break;
        case D2Event::EventEffect::MODIFY_VARIABLE:
            d2effect.category = D2Event::EventEffect::MODIFY_VARIABLE;
            d2effect.effect = D2Event::EventEffect::ModifyVarEffect{};
            break;
        default:
            qDebug() << "Unknown effect code:" << code;
            return;
    }

    effect.effect = d2effect;
    m_event.effects << effect;
    RESOLVE(MapStateHolder)->replaceObject(m_event);
    updateEffects();
}

void EventEditor::updateAvailable() const
{
    {
        QSharedPointer<IStringDataProvider> provider = m_availableConditions->provider();
        if (provider.isNull())
            provider = QSharedPointer<IStringDataProvider> (new SimpleStringProvider());
        SimpleStringProvider * prov = dynamic_cast<SimpleStringProvider*>(provider.data());
        prov->clear();

        prov->addRow(TranslationHelper::tr(D2Event::EventCondition::toString(D2Event::EventCondition::FREQUENCY)), QString::number(D2Event::EventCondition::FREQUENCY));
        prov->addRow(TranslationHelper::tr(D2Event::EventCondition::toString(D2Event::EventCondition::ENTERING_A_PREDEFINED_ZONE)), QString::number(D2Event::EventCondition::ENTERING_A_PREDEFINED_ZONE));
        prov->addRow(TranslationHelper::tr(D2Event::EventCondition::toString(D2Event::EventCondition::ENTERING_A_CITY)), QString::number(D2Event::EventCondition::ENTERING_A_CITY));
        prov->addRow(TranslationHelper::tr(D2Event::EventCondition::toString(D2Event::EventCondition::OWNING_A_CITY)), QString::number(D2Event::EventCondition::OWNING_A_CITY));
        prov->addRow(TranslationHelper::tr(D2Event::EventCondition::toString(D2Event::EventCondition::DESTROY_STACK)), QString::number(D2Event::EventCondition::DESTROY_STACK));
        prov->addRow(TranslationHelper::tr(D2Event::EventCondition::toString(D2Event::EventCondition::OWNING_AN_ITEM)), QString::number(D2Event::EventCondition::OWNING_AN_ITEM));
        prov->addRow(TranslationHelper::tr(D2Event::EventCondition::toString(D2Event::EventCondition::SPECIFIC_LEADER_OWNING_AN_ITEM)), QString::number(D2Event::EventCondition::SPECIFIC_LEADER_OWNING_AN_ITEM));
        prov->addRow(TranslationHelper::tr(D2Event::EventCondition::toString(D2Event::EventCondition::DIPLOMACY_RELATIONS)), QString::number(D2Event::EventCondition::DIPLOMACY_RELATIONS));
        prov->addRow(TranslationHelper::tr(D2Event::EventCondition::toString(D2Event::EventCondition::ALLIANCE)), QString::number(D2Event::EventCondition::ALLIANCE));
        prov->addRow(TranslationHelper::tr(D2Event::EventCondition::toString(D2Event::EventCondition::LOOTING_A_RUIN)), QString::number(D2Event::EventCondition::LOOTING_A_RUIN));
        prov->addRow(TranslationHelper::tr(D2Event::EventCondition::toString(D2Event::EventCondition::TRANSFORMING_LAND)), QString::number(D2Event::EventCondition::TRANSFORMING_LAND));
        prov->addRow(TranslationHelper::tr(D2Event::EventCondition::toString(D2Event::EventCondition::VISITING_A_SITE)), QString::number(D2Event::EventCondition::VISITING_A_SITE));
        prov->addRow(TranslationHelper::tr(D2Event::EventCondition::toString(D2Event::EventCondition::STACK_IN_LOCATION)), QString::number(D2Event::EventCondition::STACK_IN_LOCATION));
        prov->addRow(TranslationHelper::tr(D2Event::EventCondition::toString(D2Event::EventCondition::STACK_IN_CITY)), QString::number(D2Event::EventCondition::STACK_IN_CITY));
        prov->addRow(TranslationHelper::tr(D2Event::EventCondition::toString(D2Event::EventCondition::ITEM_TO_LOCATION)), QString::number(D2Event::EventCondition::ITEM_TO_LOCATION));
        prov->addRow(TranslationHelper::tr(D2Event::EventCondition::toString(D2Event::EventCondition::STACK_EXISTANCE)), QString::number(D2Event::EventCondition::STACK_EXISTANCE));
        prov->addRow(TranslationHelper::tr(D2Event::EventCondition::toString(D2Event::EventCondition::VARIABLE_IS_IN_RANGE)), QString::number(D2Event::EventCondition::VARIABLE_IS_IN_RANGE));
        prov->addRow(TranslationHelper::tr(D2Event::EventCondition::toString(D2Event::EventCondition::RESOURCE_AMOUNT)), QString::number(D2Event::EventCondition::RESOURCE_AMOUNT));
        prov->addRow(TranslationHelper::tr(D2Event::EventCondition::toString(D2Event::EventCondition::CHECK_GAMEMODE)), QString::number(D2Event::EventCondition::CHECK_GAMEMODE));
        prov->addRow(TranslationHelper::tr(D2Event::EventCondition::toString(D2Event::EventCondition::CHECK_FOR_HUMAN)), QString::number(D2Event::EventCondition::CHECK_FOR_HUMAN));
        prov->addRow(TranslationHelper::tr(D2Event::EventCondition::toString(D2Event::EventCondition::COMPARE_VAR)), QString::number(D2Event::EventCondition::COMPARE_VAR));
        prov->addRow(TranslationHelper::tr(D2Event::EventCondition::toString(D2Event::EventCondition::CUSTOM_SCRIPT)), QString::number(D2Event::EventCondition::CUSTOM_SCRIPT));

        m_availableConditions->setProvider(provider);
    }
    {
        QSharedPointer<IStringDataProvider> provider = m_availableEffects->provider();
        if (provider.isNull())
            provider = QSharedPointer<IStringDataProvider> (new SimpleStringProvider());
        SimpleStringProvider * prov = dynamic_cast<SimpleStringProvider*>(provider.data());
        prov->clear();

        prov->addRow(TranslationHelper::tr(D2Event::EventEffect::toString(D2Event::EventEffect::WIN_OR_LOSE_SCENARIO)), QString::number(D2Event::EventEffect::WIN_OR_LOSE_SCENARIO));
        prov->addRow(TranslationHelper::tr(D2Event::EventEffect::toString(D2Event::EventEffect::CREATE_NEW_STACK)), QString::number(D2Event::EventEffect::CREATE_NEW_STACK));
        prov->addRow(TranslationHelper::tr(D2Event::EventEffect::toString(D2Event::EventEffect::CAST_SPELL_ON_TRIGGERER)), QString::number(D2Event::EventEffect::CAST_SPELL_ON_TRIGGERER));
        prov->addRow(TranslationHelper::tr(D2Event::EventEffect::toString(D2Event::EventEffect::CAST_SPELL_AT_SPECIFIC_LOCATION)), QString::number(D2Event::EventEffect::CAST_SPELL_AT_SPECIFIC_LOCATION));
        prov->addRow(TranslationHelper::tr(D2Event::EventEffect::toString(D2Event::EventEffect::CHANGE_STACK_OWNER)), QString::number(D2Event::EventEffect::CHANGE_STACK_OWNER));
        prov->addRow(TranslationHelper::tr(D2Event::EventEffect::toString(D2Event::EventEffect::MOVE_STACK_NEXT_TO_TRIGGERER)), QString::number(D2Event::EventEffect::MOVE_STACK_NEXT_TO_TRIGGERER));
        prov->addRow(TranslationHelper::tr(D2Event::EventEffect::toString(D2Event::EventEffect::GO_INTO_BATTLE)), QString::number(D2Event::EventEffect::GO_INTO_BATTLE));
        prov->addRow(TranslationHelper::tr(D2Event::EventEffect::toString(D2Event::EventEffect::ENABLE_DISABLE_ANOTHER_EVENT)), QString::number(D2Event::EventEffect::ENABLE_DISABLE_ANOTHER_EVENT));
        prov->addRow(TranslationHelper::tr(D2Event::EventEffect::toString(D2Event::EventEffect::GIVE_SPELL)), QString::number(D2Event::EventEffect::GIVE_SPELL));
        prov->addRow(TranslationHelper::tr(D2Event::EventEffect::toString(D2Event::EventEffect::GIVE_ITEM)), QString::number(D2Event::EventEffect::GIVE_ITEM));
        prov->addRow(TranslationHelper::tr(D2Event::EventEffect::toString(D2Event::EventEffect::MOVE_STACK_TO_SPECIFIC_LOCATION)), QString::number(D2Event::EventEffect::MOVE_STACK_TO_SPECIFIC_LOCATION));
        prov->addRow(TranslationHelper::tr(D2Event::EventEffect::toString(D2Event::EventEffect::ALLY_TWO_AI_PLAYERS)), QString::number(D2Event::EventEffect::ALLY_TWO_AI_PLAYERS));
        prov->addRow(TranslationHelper::tr(D2Event::EventEffect::toString(D2Event::EventEffect::CHANGE_PLAYER_DIPLOMACY_METER)), QString::number(D2Event::EventEffect::CHANGE_PLAYER_DIPLOMACY_METER));
        prov->addRow(TranslationHelper::tr(D2Event::EventEffect::toString(D2Event::EventEffect::UNFOG_OR_FOG_AN_AREA_ON_THE_MAP)), QString::number(D2Event::EventEffect::UNFOG_OR_FOG_AN_AREA_ON_THE_MAP));
        prov->addRow(TranslationHelper::tr(D2Event::EventEffect::toString(D2Event::EventEffect::REMOVE_MOUNTAINS_AROUND_A_LOCATION)), QString::number(D2Event::EventEffect::REMOVE_MOUNTAINS_AROUND_A_LOCATION));
        prov->addRow(TranslationHelper::tr(D2Event::EventEffect::toString(D2Event::EventEffect::REMOVE_LANDMARK)), QString::number(D2Event::EventEffect::REMOVE_LANDMARK));
        prov->addRow(TranslationHelper::tr(D2Event::EventEffect::toString(D2Event::EventEffect::CHANGE_SCENARIO_OBJECTIVE_TEXT)), QString::number(D2Event::EventEffect::CHANGE_SCENARIO_OBJECTIVE_TEXT));
        prov->addRow(TranslationHelper::tr(D2Event::EventEffect::toString(D2Event::EventEffect::DISPLAY_POPUP_MESSAGE)), QString::number(D2Event::EventEffect::DISPLAY_POPUP_MESSAGE));
        prov->addRow(TranslationHelper::tr(D2Event::EventEffect::toString(D2Event::EventEffect::CHANGE_STACK_ORDER)), QString::number(D2Event::EventEffect::CHANGE_STACK_ORDER));
        prov->addRow(TranslationHelper::tr(D2Event::EventEffect::toString(D2Event::EventEffect::DESTROY_ITEM)), QString::number(D2Event::EventEffect::DESTROY_ITEM));
        prov->addRow(TranslationHelper::tr(D2Event::EventEffect::toString(D2Event::EventEffect::REMOVE_STACK)), QString::number(D2Event::EventEffect::REMOVE_STACK));
        prov->addRow(TranslationHelper::tr(D2Event::EventEffect::toString(D2Event::EventEffect::CHANGE_LANDMARK)), QString::number(D2Event::EventEffect::CHANGE_LANDMARK));
        prov->addRow(TranslationHelper::tr(D2Event::EventEffect::toString(D2Event::EventEffect::CHANGE_TERRAIN)), QString::number(D2Event::EventEffect::CHANGE_TERRAIN));
        prov->addRow(TranslationHelper::tr(D2Event::EventEffect::toString(D2Event::EventEffect::MODIFY_VARIABLE)), QString::number(D2Event::EventEffect::MODIFY_VARIABLE));

        m_availableEffects->setProvider(provider);
    }
}

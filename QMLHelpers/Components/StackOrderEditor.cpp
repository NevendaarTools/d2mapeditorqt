#include "StackOrderEditor.h"
#include "Engine/GameInstance.h"
#include "toolsqt/DBFModel/DBFModel.h"
#include "Engine/Components/AccessorHolder.h"
#include "Engine/Components/TranslationHelper.h"
#include "Engine/Map/MapStateHolder.h"

StackOrderEditor::StackOrderEditor(QObject *parent) : QAbstractListModel(parent)
{
    m_roles[ROLE_ID] = "Id";
    m_roles[ROLE_TEXT] = "orderText";
    m_roles[ROLE_COMBO] = "Name";
    m_orders = RESOLVE(DBFModel)->getList<LOrder>();
    m_targetID.first = StackObject::Stand;
    m_targetID.second = -1;
}

int StackOrderEditor::rowCount(const QModelIndex &parent) const
{
    if (parent.isValid())
        return 0;
    return m_orders.count();
}

QVariant StackOrderEditor::data(const QModelIndex &index, int role) const
{
    int i = index.row();
    if (role == ROLE_ID)
    {
        return m_orders[i]->id;
    }
    if (role == ROLE_TEXT)
    {
        return orderName((StackObject::Order)m_orders[i]->id);
    }
    if (role == ROLE_COMBO)
    {
        return orderName((StackObject::Order)m_orders[i]->id);
    }
    return QVariant();
}

QHash<int, QByteArray> StackOrderEditor::roleNames() const
{
    return m_roles;
}

QString StackOrderEditor::targetName() const
{
    if (currentTargetType() == 0 || m_targetID.second == -1)
        return TranslationHelper::tr("Select target");
    QSharedPointer<IMapObjectAccessor> accessor = RESOLVE(AccessorHolder)->objectAccessor(m_targetID.first);
    if (accessor.isNull())
        return "";
    QSharedPointer<MapObject> obj = RESOLVE(MapStateHolder)->objectById(m_targetID);
    return accessor->getName(obj)  + QString("[%1 : %2]").arg(obj->x).arg(obj->y);;
}

QString StackOrderEditor::orderTarget() const
{
    return QString("%1:%2").arg(m_targetID.first).arg(m_targetID.second);
}

QPair<int, int> StackOrderEditor::orderTargetNative() const
{
    return m_targetID;
}

void StackOrderEditor::setOrderTarget(const QString &newOrderTarget)
{
    QString current = orderTarget();
    if (current == newOrderTarget)
        return;
    QStringList data = newOrderTarget.split(":");
    if (data.count() < 2)
        return;
    m_targetID.first = data[0].toInt();
    m_targetID.second = data[1].toInt();
    emit targetChanged();
}

void StackOrderEditor::setOrderTarget(const QPair<int, int> &newOrderTarget)
{
    if (m_targetID == newOrderTarget)
        return;
    m_targetID = newOrderTarget;
    emit targetChanged();
}

int StackOrderEditor::orderIndex() const
{
    for (int i = 0; i < m_orders.count(); ++i)
    {
        if (m_orders[i]->id == m_currentOrder)
            return i;
    }
    return 0;
}

void StackOrderEditor::setOrderIndex(int newOrderIndex)
{
    if (orderIndex() == newOrderIndex)
        return;
    m_currentOrder = (StackObject::Order) m_orders[newOrderIndex]->id;
    qDebug()<<m_currentOrder<<" "<<targetName()<<" "<<currentTargetType();
    m_currentTargetType = currentTargetType();
    m_targetID.first = m_currentTargetType;
    m_targetID.second = -1;
    m_orderDesc = orderDescription(m_currentOrder);

    emit targetChanged();
}

int StackOrderEditor::currentTargetType() const
{
    switch (m_currentOrder) {
    case StackObject::Normal:
        break;
    case StackObject::Stand:
        break;
    case StackObject::Guard:
        break;
    case StackObject::AttackStack:
        return MapObject::Stack;
        break;
    case StackObject::DefendStack:
        return MapObject::Stack;
        break;
    case StackObject::SecureCity:
        return MapObject::Fort;
        break;
    case StackObject::Roam:
        break;
    case StackObject::MoveToLocation:
        return MapObject::Location;
        break;
    case StackObject::DefendLocation:
        return MapObject::Location;
        break;
    case StackObject::Bezerk:
        break;
    case StackObject::Assist:
        break;
    case StackObject::ExploreUnused:
        break;
    case StackObject::Steal:
        break;
    case StackObject::DefendCity:
        return MapObject::Fort;
        break;
    default:
        break;
    }

    return 0;
}

void StackOrderEditor::setOrder(int newOrder)
{
    if (m_currentOrder == newOrder)
        return;
    m_currentOrder = (StackObject::Order)newOrder;
    m_orderDesc = orderDescription(m_currentOrder);
    emit targetChanged();
}

int StackOrderEditor::order() const
{
    return m_currentOrder;
}

QString StackOrderEditor::orderDescription(StackObject::Order order)
{
    switch (order) {
    case StackObject::Normal:
        return getTAppEditText("x009ta2001") + " " + getTAppEditText("x009ta2002");
        break;
    case StackObject::Stand:
        return getTAppEditText("x009ta2003") + " " + getTAppEditText("x009ta2004");
        break;
    case StackObject::Guard:
        {
            QString result = getTAppEditText("x009ta2005") + " " + getTAppEditText("x009ta2006");
            auto value = RESOLVE(DBFModel)->getList<GVars>()[0];
            result.replace("%QTY%", QString::number(value->gu_range));
            return result;
        }
        break;
    case StackObject::AttackStack:
        return getTAppEditText("x009ta2007") + " " + getTAppEditText("x009ta2008");
        break;
    case StackObject::DefendStack:
        {
            QString result = getTAppEditText("x009ta2009") + " " + getTAppEditText("x009ta2010");
            auto value = RESOLVE(DBFModel)->getList<GVars>()[0];
            result.replace("%QTY%", QString::number(value->pa_range));
            return result;
        }
        break;
    case StackObject::SecureCity:
        return getTAppEditText("x009ta2011") + " " + getTAppEditText("x009ta2012");
        break;
    case StackObject::Roam:
        return getTAppEditText("x009ta2013") + " " + getTAppEditText("x009ta2014");
        break;
    case StackObject::MoveToLocation:
        return getTAppEditText("x009ta2015") + " " + getTAppEditText("x009ta2016");
        break;
    case StackObject::DefendLocation:
        return getTAppEditText("x009ta2017") + " " + getTAppEditText("x009ta2018");
        break;
    case StackObject::Bezerk:
        return getTAppEditText("x009ta2019") + " " + getTAppEditText("x009ta2020");
        break;
    case StackObject::Assist:
        break;
    case StackObject::ExploreUnused:
        break;
    case StackObject::Steal:
        break;
    case StackObject::DefendCity:
        break;
    default:
        break;
    }

    return QString();
}

QString StackOrderEditor::orderName(StackObject::Order order)
{
    QString result;
    result = getTAppEditText(orderTAppName(order).toUpper());
    if (result.isEmpty())
    {
        auto value = RESOLVE(DBFModel)->get<LOrder>(QString::number(order));
        if (value.isNull())
            result = QString();
        else
            result = TranslationHelper::tr(value->text);
    }
    return result;
}

QString StackOrderEditor::orderTAppName(StackObject::Order order)
{
    switch (order) {
    case StackObject::Normal:
        return "x100ta0760";
        break;
    case StackObject::Stand:
        return "x100ta0761";
        break;
    case StackObject::Guard:
        return "x100ta0762";
        break;
    case StackObject::AttackStack:
        return "x100ta0763";
        break;
    case StackObject::DefendStack:
        return "x100ta0764";
        break;
    case StackObject::SecureCity:
        return "x100ta0765";
        break;
    case StackObject::Roam:
        return "x100ta0766";
        break;
    case StackObject::MoveToLocation:
        return "x100ta0767";
        break;
    case StackObject::DefendLocation:
        return "x100ta0768";
        break;
    case StackObject::Bezerk:
        return "x100ta0769";
        break;
    case StackObject::Assist:
        break;
    case StackObject::ExploreUnused:
        break;
    case StackObject::Steal:
        break;
    case StackObject::DefendCity:
        break;
    default:
        break;
    }

    return QString();
}

const QString &StackOrderEditor::orderDesc() const
{
    return m_orderDesc;
}

#ifndef ITEMVIEW_H
#define ITEMVIEW_H

#include <QObject>
#include "toolsqt/DBFModel/GameClases.h"

class ItemView : public QObject
{
    Q_OBJECT
public:
    explicit ItemView(QObject *parent = nullptr);
    Q_PROPERTY(QString itemId READ itemId WRITE setItemId NOTIFY dataChanged)
    Q_PROPERTY(QString name READ name NOTIFY dataChanged)
    Q_PROPERTY(QString desc READ desc NOTIFY dataChanged)
    Q_PROPERTY(QString icon READ icon NOTIFY dataChanged)
    Q_PROPERTY(QString icon2 READ icon2 NOTIFY dataChanged)
    Q_PROPERTY(QString cost READ cost NOTIFY dataChanged)

    QString itemId() const;
    void setItemId(const QString &newItemId);

    QString name() const;
    QString desc() const;
    QString icon() const;
    QString icon2() const;
    QString cost() const;
signals:
    void dataChanged();
private:
    QSharedPointer<GItem> m_item;
};

#endif // ITEMVIEW_H

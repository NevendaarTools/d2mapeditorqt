#include "BaseObjectPreview.h"
#include "Engine/GameInstance.h"
#include "Engine/Map/MapStateHolder.h"
#include "Engine/Components/AccessorHolder.h"

BaseObjectPreview::BaseObjectPreview(QObject *parent)
    : QObject{parent}
{

}

const QString &BaseObjectPreview::uid() const
{
    return m_uid;
}

void BaseObjectPreview::setValue(const QString &newUid)
{
    QStringList data = newUid.split(":");
    if (data.count() < 2)
        return;
    if (m_uid == newUid)
        return;
    m_uid = newUid;

    auto object = RESOLVE(MapStateHolder)->objectById(QPair<int,int>(data[0].toInt(), data[1].toInt()));
    QSharedPointer<IMapObjectAccessor> accessor = RESOLVE(AccessorHolder)->objectAccessor(data[0].toInt());
    m_name = accessor->getName(object).trimmed();
    m_desc = accessor->getDesc(object).trimmed();
    m_position = QString("pos: %1 : %2 (%3 x %4)").arg(object->x).arg(object->y).arg(accessor->getW(object))
                     .arg(accessor->getH(object));

    emit changed();
}

const QString &BaseObjectPreview::getName() const
{
    return m_name;
}

const QString &BaseObjectPreview::getDesc() const
{
    return m_desc;
}

const QString &BaseObjectPreview::getImage() const
{
    return m_image;
}

const QString &BaseObjectPreview::getPosition() const
{
    return m_position;
}

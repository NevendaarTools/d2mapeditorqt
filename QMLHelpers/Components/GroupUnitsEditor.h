#ifndef GROUPUNITSEDITOR_H
#define GROUPUNITSEDITOR_H
#include <QObject>
#include "Commands/CommandBus.h"
#include <QSharedPointer>
#include <QQuickWidget>
#include <QQmlContext>
#include "MapObjects/BaseClases.h"

class GroupEditor
{
public:
    explicit GroupEditor(){}

    QString unitId(int x, int y);
    QString unitUid(int x, int y);
    int unitBaseLvl(int x, int y);
    int unitMaxHp(int x, int y);
    int unitCurrentHp(int x, int y);
    int unitMaxLvl();
    int expForKill();
    int unitLvl(int x, int y);
    void setUnitLvl(int x, int y, int level);
    bool isLeader(int x, int y);
    bool isSmall(int x, int y);
    bool hasModifiers(int x, int y);
    void remove(int x, int y);

    bool tryHire(int x, int y, const QString& id, int level);

    void setGroup(const GroupData &newGroup);
    const GroupData &group() const;

    void clearGroup();
    void swapRows(int y1, int y2);
    void swapUnits(const QPoint & from, GroupEditor * target, const QPoint & to);

    void print(const GroupData & group);
    void moveUnit(const QPoint & from, GroupEditor * target, const QPoint & to);
    bool isSmall(const QPoint& point) {return isSmall(point.x(), point.y());}
    bool isLeader(const QPoint& point) {return isLeader(point.x(), point.y());}
    void setName(const QString& name) {m_group.name = name;}
    bool hasLeader() const {return m_group.leaderIndex() != -1;}
    bool hasUnit(int x, int y) const {return m_group.hasUnit(QPoint(x, y));}
    bool hasUnit(int pos) const;
private:
    GroupData m_group;
};


class GroupUnitsEditor : public QObject
{
    Q_OBJECT
public:
    explicit GroupUnitsEditor(QObject *parent = nullptr);

    Q_INVOKABLE QString unitId(int x, int y);
    Q_INVOKABLE QString unitUid(int x, int y);
    Q_INVOKABLE QString icon(int x, int y);
    Q_INVOKABLE int unitBaseLvl(int x, int y);
    Q_INVOKABLE int unitMaxHp(int x, int y);
    Q_INVOKABLE int unitCurrentHp(int x, int y);
    Q_INVOKABLE int unitMaxLvl();
    Q_INVOKABLE int expForKill();
    Q_INVOKABLE int unitLvl(int x, int y);
    Q_INVOKABLE void setUnitLvl(int x, int y, int level);
    Q_INVOKABLE bool isLeader(int x, int y);
    Q_INVOKABLE bool isSmall(int x, int y);
    Q_INVOKABLE bool hasModifiers(int x, int y);
    Q_INVOKABLE void remove(int x, int y);

    Q_INVOKABLE void setDragSource(int x, int y);
    Q_INVOKABLE void setDragTarget(int x, int y);
    Q_INVOKABLE void finishDrag();

    Q_INVOKABLE bool tryHire(int x, int y, const QString& id, int level);
    Q_PROPERTY(bool hasLeader READ hasLeader NOTIFY unitsChanged)
    bool isDragTarget() const;
    bool isDragSource() const;
    void resetDrag();

    void setGroup(const GroupData &newGroup);
    const GroupData &group() const;

    void clearGroup();
    void print(const GroupData & group);

    QPoint dragSource() const {return m_dragSource;}
    QPoint dragTarget() const {return m_dragTarget;}
    bool isSmall(const QPoint& point) {return isSmall(point.x(), point.y());}
    bool isLeader(const QPoint& point) {return isLeader(point.x(), point.y());}
    void moveUnit(const QPoint & from, GroupUnitsEditor * target, const QPoint & to);
    void setName(const QString& name) {m_editor.setName(name);}
    bool hasLeader() const {return m_editor.hasLeader();}
signals:
    void unitsChanged();
    void dragFinished();

private:
    GroupEditor m_editor;
    QPoint m_dragSource;
    QPoint m_dragTarget;
};//garrison

#endif // GROUPUNITSEDITOR_H

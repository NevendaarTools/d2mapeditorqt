#include "ItemView.h"
#include "Engine/GameInstance.h"
#include "toolsqt/DBFModel/DBFModel.h"

ItemView::ItemView(QObject *parent) : QObject(parent)
{

}

QString ItemView::itemId() const
{
    if (m_item.isNull())
        return "";
    return m_item->item_id;
}

void ItemView::setItemId(const QString & newItemId)
{
    m_item = RESOLVE(DBFModel)->get<GItem>(newItemId);
    emit dataChanged();
}

QString ItemView::name() const
{
    if (m_item.isNull())
        return "";
    return m_item->name_txt->text;
}

QString ItemView::desc() const
{
    if (m_item.isNull())
        return "";\
    QString resDesc = m_item->desc_txt->text;
    resDesc.replace("\\n","<br>");
    return resDesc;
}

QString ItemView::icon() const
{
    if (m_item.isNull())
        return "";
    if (m_item->item_cat == 8)
    {
        if (!m_item->spell_id.isEmpty())
            return "IconSpel-" + m_item->spell_id.toUpper();
    }
    else
        return "IconItem-" + m_item->item_id.toUpper();
    return "";
}

QString ItemView::icon2() const
{
    if (m_item.isNull())
        return "";
    if (m_item->item_cat == 8)
    {
        return "IconItem-SCROLLHU";
    }
    return "";
}

QString ItemView::cost() const
{
    if (m_item.isNull())
        return "";
    return m_item->value;
}

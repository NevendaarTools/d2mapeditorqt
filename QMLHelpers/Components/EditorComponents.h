#ifndef EDITORCOMPONENTS_H
#define EDITORCOMPONENTS_H
#include "BaseObjectPreview.h"
#include "CostEditor.h"
#include "EventEditor.h"
#include "GroupUnitsEditor.h"
#include "ItemView.h"
#include "SpellView.h"
#include "QuestStageEditor.h"
#include "StackOrderEditor.h"
#include "SceneVariablesEditor.h"
#include "UnitEditor.h"
#include "EventEffectEditor.h"
#include "EventConditionEditor.h"
#include "StackTemplateEditor.h"

static void editorComponentsRegister()
{
    qmlRegisterType<BaseObjectPreview>("QMLBaseObjectPreview", 1, 0,"BaseObjectPreview");
    qmlRegisterType<CostEditor>("QMLCostEditor", 1, 0,"CostEditor");
    qmlRegisterType<EventEditor>("QMLEventEditor", 1, 0,"EventEditor");
    qmlRegisterType<GroupUnitsEditor>("QMLGroupUnitsEditor", 1, 0,"GroupUnitsEditor");
    qmlRegisterType<ItemView>("QMLItemView", 1, 0,"ItemView");
    qmlRegisterType<SpellView>("NTComponents", 1, 0,"SpellView");
    qmlRegisterType<QuestStageEditor>("QMLQuestStageEditor", 1, 0,"QuestStageEditor");
    qmlRegisterType<SceneVariablesEditor>("QMLSceneVariablesEditor", 1, 0,"SceneVariablesEditor");
    qmlRegisterType<StackOrderEditor>("NTComponents", 1, 0, "StackOrderEditor");
    qmlRegisterType<UnitEditor>("QMLUnitEditor", 1, 0, "UnitEditor");
    qmlRegisterType<EventEffectEditor>("NevendaarTools", 1, 0, "EventEffectEditor");
    qmlRegisterType<EventConditionEditor>("NevendaarTools", 1, 0, "EventConditionEditor");
    qmlRegisterType<StackTemplateEditor>("NevendaarTools", 1, 0, "StackTemplateEditor");
}
#endif // EDITORCOMPONENTS_H

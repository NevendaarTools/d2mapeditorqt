#include "UnitEditor.h"
#include "Engine/GameInstance.h"
#include "Engine/Map/MapStateHolder.h"
#include "Engine/Components/TranslationHelper.h"

static QString upgrString (int val1, int val2)
{
    QString result = "";
    if (val1 == 0 && val2 == 0)
        result = "";
    else
    {
        if (val1 == val2)
            result = " (" + QString::number(val1) + ")";
        else
        {
            result = " (" + QString::number(val1) + " / " + QString::number(val2) + ")";
        }
    }
    return result;
}

UnitEditor::UnitEditor(QObject *parent): QObject(parent)
{
    m_unit.level = 1;
    m_unitModifiers = new SelectGameObjectModel(this);
    m_modifiers = new SelectGameObjectModel(this);
    m_allModifs = RESOLVE(DBFModel)->getList<Gmodif>();
}

void UnitEditor::addMod(const QString &modId, int count)
{
    qDebug()<<Q_FUNC_INFO<<modId<<count;
    QSharedPointer<Gmodif> modif = RESOLVE(DBFModel)->get<Gmodif>(modId);
    Gmodif * newT = new Gmodif(*modif.data());
    auto pointer = QSharedPointer<GameData>(newT);

    for (int i = 0; i < count; ++i)
    {
        m_unitModifiers->addItem(pointer);
    }
    emit unitChanged();
}

void UnitEditor::removeMod(int index)
{
    m_unitModifiers->removeItemAt(index);
    emit unitChanged();
}

void UnitEditor::save()
{
    auto data = m_unitModifiers->getData();
    m_unit.modifiers.clear();
    for(auto & item: data)
    {
        Gmodif * mod = static_cast<Gmodif*>(item.data());
        m_unit.modifiers.append(mod->modif_id);
    }
    RESOLVE(MapStateHolder)->replaceObject(m_unit);
}

void UnitEditor::cancel()
{

}
QDebug operator<<(QDebug debug, const GmodifL &gmodif) {
    QDebugStateSaver saver(debug);
    debug.nospace() << "GmodifL("
                    << "belongs_to: " << gmodif.belongs_to << ", "
                    << "desc: " << gmodif.desc << ", "
                    << "type: " << gmodif.type << ", "
                    << "percent: " << gmodif.percent << ", "
                    << "number: " << gmodif.number << ", "
                    << "ability: " << gmodif.ability << ", "
                    << "immunity: " << gmodif.immunity << ", "
                    << "immunecat: " << gmodif.immunecat << ", "
                    << "move: " << gmodif.move
                    << ")";
    return debug;
}
void UnitEditor::setUnitUid(const QString &newUid)
{
    QStringList data = newUid.split(":");
    if (data.count() < 2)
        return;

    QPair<int, int> uid(data[0].toInt(), data[1].toInt());
    m_unit = RESOLVE(MapStateHolder)->objectByIdT<UnitObject>(uid);
    m_base = RESOLVE(DBFModel)->get<Gunit>(m_unit.id);

    auto getNameFunc = [](const QSharedPointer<GameData>& item)
    {
        Gmodif * mod = static_cast<Gmodif*>(item.data());
        QString text = "";
        if (!mod->desc_txt.value.isNull())
            text = mod->desc_txt->text;
        return text;
    };
    auto getIdFunc = [](const QSharedPointer<GameData>& item)
    {
        Gmodif * mod = static_cast<Gmodif*>(item.data());
        QString text = "";
        if (!mod->desc_txt.value.isNull())
            text = mod->modif_id;
        return text;
    };
    QList<QSharedPointer<GameData>> posibleModifs;
    bool leader = m_unit.leader;
    for (int i = 0; i < m_allModifs.count(); ++i)
    {
        QSharedPointer<Gmodif> & modif = m_allModifs[i];
        const QString key = modif->key();
        //gxxxum9yyy
        if(key[0] != 'g' || key[4] != 'u' || key[5] != 'm' || key[6] != '9')
            continue;
        if (!leader && (modif->source == 1))//source1 = for leader
            continue;
        Gmodif * newT = new Gmodif(*m_allModifs[i].data());
        posibleModifs << QSharedPointer<GameData>(newT);
    }
    qDebug()<<Q_FUNC_INFO<<"##posible mod count = " << posibleModifs.count();
    m_modifiers->init<Gmodif>(posibleModifs, getNameFunc, getIdFunc);

    QList<QSharedPointer<GameData>> currentModifs;
    for (int i = 0; i < m_unit.modifiers.count(); ++i)
    {
        QString id = m_unit.modifiers[i];
        QSharedPointer<Gmodif> modif = RESOLVE(DBFModel)->get<Gmodif>(id);
        Gmodif * newT = new Gmodif(*modif.data());
        currentModifs << QSharedPointer<GameData>(newT);
    }
    qDebug()<<Q_FUNC_INFO<<"##current mod count = " << currentModifs.count();
    m_unitModifiers->init<Gmodif>(currentModifs, getNameFunc, getIdFunc);
    emit levelChanged();
    emit unitChanged();
}

int UnitEditor::level() const
{
    return m_unit.level;
}

int UnitEditor::baseLevel() const
{
    if (m_base.isNull())
        return 0;
    return m_base->level;
}

int UnitEditor::maxLevel() const
{
    if (m_base.isNull())
        return 0;
    return 65536 / RESOLVE(DBFModel)->getKeysList<Gunit>().count();
}

void UnitEditor::setLevel(int newLevel)
{
    if (m_unit.level != newLevel)
        m_unit.level = newLevel;
    auto gunit = RESOLVE(DBFModel)->get<Gunit>(m_unit.id);
    if (!gunit.isNull())
    {
        m_unit.currentHp = calcLeveledValue(gunit->hit_point, gunit->level, newLevel,
                            gunit->dyn_upg_lv, gunit->dyn_upg1->hit_point,
                            gunit->dyn_upg2->hit_point);
        m_unit.currentExp = 0;
    }
    emit levelChanged();
    emit unitChanged();
}

const QString UnitEditor::unitid() const
{
    return m_unit.id;
}

const QString UnitEditor::icon() const
{
    if (m_base.isNull())
        return "";
    QString unitIconId = m_base->base_unit.getKey() + "FACE";
    if (m_base->base_unit.value == nullptr)
        unitIconId = m_base->unit_id + "FACE";
    return "Faces-" + unitIconId;
}

void UnitEditor::setUnitid(const QString &newUnitid)
{
    if (m_unit.id == newUnitid)
        return;
    m_unit.id = newUnitid;
    m_base = RESOLVE(DBFModel)->get<Gunit>(m_unit.id);
    m_unit.level = m_base->level;
    emit unitChanged();
}

const QString UnitEditor::name() const
{
    if (m_base.isNull())
        return "";
    return m_base->name_txt->text;
}

const QString UnitEditor::desc() const
{
    if (m_base.isNull())
        return "";
    return m_base->desc_txt->text;
}
void applyModifs(int & value, QList<QSharedPointer<GameData> > mods, ModifierElementType type)
{
    for(auto & mod: mods)
    {
        Gmodif * modif = (Gmodif*)mod.data();
        for(const auto & modifl: modif->modifiers.m_value)
        {
            if (modifl->type != (int)type)
                continue;
            if (type == ModifierElementType::Regeneration)
            {
                value = qMax(value, modifl->percent);
                continue;
            }
            bool percent = false;
            switch (type) {
            case ModifierElementType::QtyDamage:
            case ModifierElementType::Power:
            case ModifierElementType::Initiative:
                percent = true;
                break;
            case ModifierElementType::Hp:;
                percent = modifl->percent;
                break;
            }

            if (percent) {
                value += value * modifl->percent/100.0;
            } else {
                value += modifl->number;
            }
        }
    }
}
const QString UnitEditor::hp() const
{
    if (m_base.isNull())
        return "";
    int value = calcLeveledValue(m_base->hit_point, m_base->level, m_unit.level,
                                 m_base->dyn_upg_lv, m_base->dyn_upg1->hit_point,
                                 m_base->dyn_upg2->hit_point);
    auto mods = m_unitModifiers->getData();
    applyModifs(value, mods, ModifierElementType::Hp);
    QString result = QString::number(value);
    if (m_withUpgr)
        result += upgrString(m_base->dyn_upg1->hit_point, m_base->dyn_upg2->hit_point);
    return result;
}

const QString UnitEditor::currentHp() const
{
    return QString::number(m_unit.currentHp);
}

const QString UnitEditor::exp() const
{
    if (m_base.isNull())
        return "";
    int value = calcLeveledValue(m_base->xp_next, m_base->level, m_unit.level,
                                 m_base->dyn_upg_lv, m_base->dyn_upg1->xp_next,
                                 m_base->dyn_upg2->xp_next);
    QString result = QString::number(value);
    if (m_withUpgr)
        result += upgrString(m_base->dyn_upg1->xp_next, m_base->dyn_upg2->xp_next);
    return result;
}

const QString UnitEditor::currentExp() const
{
    return QString::number(m_unit.currentExp);
}

const QString UnitEditor::expKill() const
{
    if (m_base.isNull())
        return "";
    int value = calcLeveledValue(m_base->xp_killed, m_base->level, m_unit.level,
                                 m_base->dyn_upg_lv, m_base->dyn_upg1->xp_killed,
                                 m_base->dyn_upg2->xp_killed);
    QString result = QString::number(value);
    if (m_withUpgr)
        result += upgrString(m_base->dyn_upg1->xp_killed, m_base->dyn_upg2->xp_killed);
    return result;
}

const QString UnitEditor::regen() const
{
    if (m_base.isNull())
        return "";
    int value = calcLeveledValue(m_base->regen, m_base->level, m_unit.level,
                                 m_base->dyn_upg_lv, m_base->dyn_upg1->regen,
                                 m_base->dyn_upg2->regen);
    auto mods = m_unitModifiers->getData();
    applyModifs(value, mods, ModifierElementType::Regeneration);
    QString result = QString::number(value);
    if (m_withUpgr)
        result += upgrString(m_base->dyn_upg1->regen, m_base->dyn_upg2->regen);
    return result;
}

const QString UnitEditor::armor() const
{
    if (m_base.isNull())
        return "";
    int value = calcLeveledValue(m_base->armor, m_base->level, m_unit.level,
                                 m_base->dyn_upg_lv, m_base->dyn_upg1->armor,
                                 m_base->dyn_upg2->armor);
    auto mods = m_unitModifiers->getData();
    applyModifs(value, mods, ModifierElementType::Armor);
    QString result = QString::number(value);
    if (m_withUpgr)
        result += upgrString(m_base->dyn_upg1->armor, m_base->dyn_upg2->armor);
    return result;
}
QString getImmunes(QList<QSharedPointer<GameData> > mods, bool always, int type)
{
    QString result;
    for(auto & mod: mods)
    {
        Gmodif * modif = (Gmodif*)mod.data();
        for(const auto & modifl: modif->modifiers.m_value)
        {
            if (modifl->type != type)
                continue;
            QSharedPointer<LImmune> value = RESOLVE(DBFModel)->get<LImmune>(QString::number(modifl->immunecat));
            if ((value->text == "L_ALWAYS") == always) {
                auto immunity = RESOLVE(DBFModel)->get<LattS>(QString::number(modifl->immunity));
                if (immunity->name_txt.value.isNull())
                    result+= TranslationHelper::tr(immunity->text).trimmed() + ", ";
                else
                    result += immunity->name_txt->text.trimmed() + ", ";
            }
        }
    }
    return result;
}

const QString UnitEditor::immun() const
{
    if (m_base.isNull())
        return "";
    QString result;
    StringLinkList<Gimmu> immun = m_base->immun;
    StringLinkList<GimmuC> immunCategory = m_base->immunCategory;
    auto mods = m_unitModifiers->getData();

    foreach(const QSharedPointer<Gimmu> & value, immun.m_value)
    {
        if (value->immunecat->text == "L_ALWAYS")
        {
            if (value->immunity->name_txt.value.isNull())
                result+= TranslationHelper::tr(value->immunity->text).trimmed() + ", ";
            else
                result += value->immunity->name_txt->text.trimmed() + ", ";
        }
    }
    result += getImmunes(mods, true, (int)ModifierElementType::Immunity);
    foreach(const QSharedPointer<GimmuC> & value, immunCategory.m_value)
    {
        if (value->immunecat->text == "L_ALWAYS")
            result+= TranslationHelper::tr(value->immunity->text).trimmed() + ", ";
    }
    result += getImmunes(mods, true, (int)ModifierElementType::Immunityclass);

    if (result.count() > 3)
        result.remove(result.count() - 2, 2);
    qDebug()<<result;
    return result;
}

const QString UnitEditor::protect() const
{
    if (m_base.isNull())
        return "";
    QString result;
    StringLinkList<Gimmu> immun = m_base->immun;
    StringLinkList<GimmuC> immunCategory = m_base->immunCategory;
    auto mods = m_unitModifiers->getData();

    foreach(const QSharedPointer<Gimmu> value, immun.m_value)
    {
        if (value->immunecat->text == "L_ONCE")
        {
            if (value->immunity->name_txt.value.isNull())
                result+= TranslationHelper::tr(value->immunity->text).trimmed() + ", ";
            else
                result += value->immunity->name_txt->text.trimmed() + ", ";
        }
    }
    result += getImmunes(mods, false, (int)ModifierElementType::Immunity);
    foreach(const QSharedPointer<GimmuC> value, immunCategory.m_value)
    {
        if (value->immunecat->text == "L_ONCE")
            result+= TranslationHelper::tr(value->immunity->text).trimmed() + ", ";
    }
    result += getImmunes(mods, false, (int)ModifierElementType::Immunityclass);

    if (result.count() > 3)
        result.remove(result.count() - 2, 2);
    qDebug()<<result;
    return result;
}

const QString UnitEditor::ini() const
{
    if (m_base.isNull())
        return "";
    auto value = m_base->attack_id->initiative;
    auto mods = m_unitModifiers->getData();
    applyModifs(value, mods, ModifierElementType::Initiative);
    return QString::number(value);
}

const QString UnitEditor::attack1() const
{
    if (m_base.isNull())
        return "";
    QString result;
    if (m_base->atck_twice &&
            m_base->attack_id->clas->text != "L_DOPPELGANGER" &&
            m_base->attack_id->clas->text != "L_TRANSFORM_SELF")
        result = "(2x) ";
    result += m_base->attack_id->name_txt->text;
    if (!m_base->attack2_id.value.isNull() &&
            m_base->attack_id->clas->text != "L_DOPPELGANGER" &&
            m_base->attack_id->clas->text != "L_TRANSFORM_SELF")
        result += " / " + m_base->attack2_id->name_txt->text;
    return result;
}

const QString UnitEditor::attack1Source() const
{
    if (m_base.isNull())
        return "";
    if (m_base->attack_id->clas->text == "L_TRANSFORM_SELF" ||
        m_base->attack_id->clas->text == "L_DOPPELGANGER")
        return "";
    QString result;
    if (m_base->attack_id->source->name_txt.value.isNull())
        result += TranslationHelper::tr(m_base->attack_id->source->text);
    else
        result += m_base->attack_id->source->name_txt->text;
    if (!m_base->attack2_id.value.isNull())
    {
        if (m_base->attack2_id->source->name_txt.value.isNull())
            result += " / " + TranslationHelper::tr(m_base->attack2_id->source->text);
        else
            result += m_base->attack2_id->source->name_txt->text;
    }
    return result;
}

QString UnitEditor::attackDamage(QSharedPointer<Gattack> attack, QString &name, bool withName) const
{
    auto vars = RESOLVE(DBFModel)->getList<GVars>()[0];

    QString result;
    if (attack->clas->text == "L_BOOST_DAMAGE")
    {
        if (withName)
            name = TranslationHelper::tr("Boost:");
        switch(attack->level)
        {
            case 1:
                result += QString::number(vars->batboostd1) + "%";
                break;
            case 2:
                result += QString::number(vars->batboostd2) + "%";
                break;
            case 3:
                result += QString::number(vars->batboostd3) + "%";
                break;
            case 4:
                result += QString::number(vars->batboostd4) + "%";
                break;
        }
        return result;
    }
    if (attack->clas->text == "L_LOWER_INITIATIVE")
    {

        switch (attack->level)
        {
            case 1:
                result += QString::number(vars->batloweri1) + "%";
                break;
        }
        return result;
    }
    if (attack->clas->text == "L_LOWER_DAMAGE")
    {
        switch(attack->level)
        {
            case 1:
                result += QString::number(vars->batlowerd1) + "%";
                break;
            case 2:
                result += QString::number(vars->batlowerd2) + "%";
                break;
        }
        return result;
    }
    if (attack->clas->text == "L_HEAL")
    {
        if (withName)
            name = TranslationHelper::tr("Heal:");
        int value = calcLeveledValue(attack->qty_heal, m_base->level, m_unit.level,
                                     m_base->dyn_upg_lv, m_base->dyn_upg1->heal,
                                     m_base->dyn_upg2->heal);
        result += QString::number(value);
        if (m_withUpgr)
            result += upgrString(m_base->dyn_upg1->heal, m_base->dyn_upg2->heal);
        return result;
    }
    if (    attack->clas->text == "L_DAMAGE" ||
            attack->clas->text == "L_DRAIN" ||
            attack->clas->text == "L_DRAIN_OVERFLOW" ||
            attack->clas->text == "L_POISON" ||
            attack->clas->text == "L_FROSTBITE" ||
            attack->clas->text == "L_BLISTER" ||
            attack->clas->text == "L_SHATTER"
            )
    {
        if (withName)
            name = TranslationHelper::tr("Attack:");
        int value = calcLeveledValue(attack->qty_dam, m_base->level, m_unit.level,
                                     m_base->dyn_upg_lv, m_base->dyn_upg1->damage,
                                     m_base->dyn_upg2->damage);

        auto mods = m_unitModifiers->getData();

        if (    attack->clas->text == "L_DAMAGE" ||
                attack->clas->text == "L_DRAIN" ||
                attack->clas->text == "L_DRAIN_OVERFLOW")
            applyModifs(value, mods, ModifierElementType::QtyDamage);

        result += QString::number(value);
        if (m_withUpgr && withName)
            result += upgrString(m_base->dyn_upg1->damage, m_base->dyn_upg2->damage);
        return result;
    }
    return result;
}
const QString UnitEditor::attack1Damage() const
{
    if (m_base.isNull())
        return "";
    if (m_base->attack_id->clas->text == "L_TRANSFORM_SELF" ||
        m_base->attack_id->clas->text == "L_DOPPELGANGER")
        return "";
    QString name;
    QString result;

    if (!m_base->attack2_id.value.isNull())
    {
        result = attackDamage(m_base->attack_id.value, name);
        QString tmp = attackDamage(m_base->attack2_id.value, name, true);
        if  (!tmp.trimmed().isEmpty())
        {
            if (result.trimmed().isEmpty())
                result = tmp;
            else
                result += " / " +  tmp;
        }
    }
    else
    {
        result = attackDamage(m_base->attack_id.value, name, true);

    }
    return result;
}

const QString UnitEditor::attack1DamageName() const
{
    if (m_base.isNull())
        return "";
    if (m_base->attack_id->clas->text == "L_TRANSFORM_SELF" ||
        m_base->attack_id->clas->text == "L_DOPPELGANGER")
        return "";
    QString result;
    attackDamage(m_base->attack_id.value, result, true);
    if (result.isEmpty() && (!m_base->attack2_id.value.isNull()))
        attackDamage(m_base->attack2_id.value, result, true);
    return result;
}

const QString UnitEditor::attack1Accuracy() const
{
    if (m_base.isNull())
        return "";
    if (m_base->attack_id->clas->text == "L_HEAL" ||
        m_base->attack_id->clas->text == "L_TRANSFORM_SELF" ||
        m_base->attack_id->clas->text == "L_DOPPELGANGER" ||
        m_base->attack_id->clas->text == "L_BOOST_DAMAGE")
        return "";
    QString result;
    int value1 = calcLeveledValue(m_base->attack_id->power, m_base->level, m_unit.level,
                                 m_base->dyn_upg_lv, m_base->dyn_upg1->power,
                                 m_base->dyn_upg2->power);
    auto mods = m_unitModifiers->getData();
    applyModifs(value1, mods, ModifierElementType::Power);

    result = QString::number(value1) + "%";

    if (!m_base->attack2_id.value.isNull())
    {
        int value2 = calcLeveledValue(m_base->attack2_id->power, m_base->level, m_unit.level,
                                     m_base->dyn_upg_lv, m_base->dyn_upg1->power,
                                     m_base->dyn_upg2->power);
        //applyModifs(value2, mods, ModifierElementType::Power);
        result += " / " +  QString::number(value2) + "%";
    }
    if (m_withUpgr)
        result += upgrString(m_base->dyn_upg1->power, m_base->dyn_upg2->power);
    return result;
}

const QString UnitEditor::attack1Target() const
{
    if (m_base.isNull())
        return "";
    if (m_base->attack_id->reach.key == 1)
        return TranslationHelper::tr("(6) All targets");
    if (m_base->attack_id->reach.key == 2)
    {
        if (m_base->attack_id->clas->text == "L_TRANSFORM_SELF" ||
               m_base->attack_id->clas->text == "L_DOPPELGANGER")
            return TranslationHelper::tr("Unit self");
        return TranslationHelper::tr("Any 1 target");
    }
    if (m_base->attack_id->reach.key == 3)
    {
        if (m_base->attack_id->clas->text == "L_TRANSFORM_SELF" ||
               m_base->attack_id->clas->text == "L_DOPPELGANGER")
            return TranslationHelper::tr("Unit self");
        return TranslationHelper::tr("1 Melee target");
    }

    return QString("(%1) ").arg(m_base->attack_id->reach->max_targts) +
            m_base->attack_id->reach->reach_txt->text;
}

const QString UnitEditor::attack2() const
{
    if (m_base.isNull() || m_base->attack_id->alt_attack.value.isNull())
        return "";

    QString result;
    if (m_base->atck_twice &&
            m_base->attack_id->alt_attack->clas->text != "L_DOPPELGANGER" &&
            m_base->attack_id->alt_attack->clas->text != "L_TRANSFORM_SELF")
        result = "(2x) ";
    result += m_base->attack_id->alt_attack->name_txt->text;
    if (!m_base->attack2_id.value.isNull() &&
            m_base->attack_id->alt_attack->clas->text != "L_DOPPELGANGER" &&
            m_base->attack_id->alt_attack->clas->text != "L_TRANSFORM_SELF")
        result += " / " + m_base->attack2_id->name_txt->text;
    return result;
}

const QString UnitEditor::attack2Source() const
{
    if (m_base.isNull() || m_base->attack_id->alt_attack.value.isNull())
        return "";
    if (m_base->attack_id->clas->text == "L_TRANSFORM_SELF" ||
        m_base->attack_id->clas->text == "L_DOPPELGANGER")
        return "";
    QString result;
    if (m_base->attack_id->alt_attack->source->name_txt.value.isNull())
        result += TranslationHelper::tr(m_base->attack_id->alt_attack->source->text);
    else
        result += m_base->attack_id->alt_attack->source->name_txt->text;
    if (!m_base->attack2_id.value.isNull())
    {
        if (m_base->attack2_id->source->name_txt.value.isNull())
            result += " / " + TranslationHelper::tr(m_base->attack2_id->source->text);
        else
            result += m_base->attack2_id->source->name_txt->text;
    }

    return result;
}

const QString UnitEditor::attack2Damage() const
{
    if (m_base.isNull() || m_base->attack_id->alt_attack.value.isNull())
        return "";
    if (m_base->attack_id->alt_attack->clas->text == "L_TRANSFORM_SELF" ||
        m_base->attack_id->alt_attack->clas->text == "L_DOPPELGANGER")
        return "";
    QString name;
    QString result;

    if (!m_base->attack2_id.value.isNull())
    {
        result = attackDamage(m_base->attack_id->alt_attack.value, name);
        QString tmp = attackDamage(m_base->attack2_id.value, name, true);
        if  (!tmp.trimmed().isEmpty())
        {
            if (result.trimmed().isEmpty())
                result = tmp;
            else
                result += " / " +  tmp;
        }
    }
    else
    {
        result = attackDamage(m_base->attack_id->alt_attack.value, name, true);

    }
    return result;
}

const QString UnitEditor::attack2DamageName() const
{
    if (m_base.isNull() || m_base->attack_id->alt_attack.value.isNull())
        return "";
    if (m_base->attack_id->alt_attack->clas->text == "L_TRANSFORM_SELF" ||
        m_base->attack_id->alt_attack->clas->text == "L_DOPPELGANGER")
        return "";
    QString result;
    attackDamage(m_base->attack_id->alt_attack.value, result, true);
    if (result.isEmpty() && (!m_base->attack2_id.value.isNull()))
        attackDamage(m_base->attack2_id.value, result, true);
    return result;
}

const QString UnitEditor::attack2Accuracy() const
{
    if (m_base.isNull() || m_base->attack_id->alt_attack.value.isNull())
        return "";
    if (m_base->attack_id->alt_attack->clas->text == "L_HEAL" ||
        m_base->attack_id->alt_attack->clas->text == "L_TRANSFORM_SELF" ||
        m_base->attack_id->alt_attack->clas->text == "L_DOPPELGANGER" ||
        m_base->attack_id->alt_attack->clas->text == "L_BOOST_DAMAGE")
        return "";
    QString result;
    int value1 = calcLeveledValue(m_base->attack_id->alt_attack->power, m_base->level, m_unit.level,
                                 m_base->dyn_upg_lv, m_base->dyn_upg1->power,
                                 m_base->dyn_upg2->power);
    result = QString::number(value1) + "%";

    if (!m_base->attack2_id.value.isNull())
    {
        int value2 = calcLeveledValue(m_base->attack2_id->power, m_base->level, m_unit.level,
                                     m_base->dyn_upg_lv, m_base->dyn_upg1->power,
                                     m_base->dyn_upg2->power);
        result += " / " +  QString::number(value2) + "%";
    }
    if (m_withUpgr)
        result += upgrString(m_base->dyn_upg1->power, m_base->dyn_upg2->power);
    return result;
}

const QString UnitEditor::attack2Target() const
{
    if (m_base.isNull() || m_base->attack_id->alt_attack.value.isNull())
        return "";
    if (m_base->attack_id->alt_attack->reach.key == 1)
        return TranslationHelper::tr("(6) All targets");
    if (m_base->attack_id->alt_attack->reach.key == 2)
    {
        if (m_base->attack_id->alt_attack->clas->text == "L_TRANSFORM_SELF" ||
               m_base->attack_id->alt_attack->clas->text == "L_DOPPELGANGER")
            return TranslationHelper::tr("Unit self");
        return TranslationHelper::tr("Any 1 target");
    }
    if (m_base->attack_id->alt_attack->reach.key == 3)
    {
        if (m_base->attack_id->alt_attack->clas->text == "L_TRANSFORM_SELF" ||
               m_base->attack_id->alt_attack->clas->text == "L_DOPPELGANGER")
            return TranslationHelper::tr("Unit self");
        return TranslationHelper::tr("1 Melee target");
    }

    return QString("(%1) ").arg(m_base->attack_id->alt_attack->reach->max_targts) +
            m_base->attack_id->alt_attack->reach->reach_txt->text;
}

int UnitEditor::limit() const
{
    if (m_base.isNull())
        return 0;
    return m_base->dyn_upg_lv;
}

const QString UnitEditor::unitUid() const
{
    return QString("%1:%2").arg(m_unit.uid.first).arg(m_unit.uid.second);
}

SelectGameObjectModel *UnitEditor::unitModifiers() const
{
    return m_unitModifiers;
}

SelectGameObjectModel *UnitEditor::modifiers() const
{
    return m_modifiers;
}

#ifndef SCENEVARIABLESEDITOR_H
#define SCENEVARIABLESEDITOR_H
#include <QObject>
#include "QMLHelpers/Editors/EditorWidget.h"
#include "QMLHelpers/Models/StringModel.h"
#include "QMLHelpers/Models/SelectMapObjectModel.h"

class SceneVariablesEditor : public QObject
{
    Q_OBJECT
public:
    explicit SceneVariablesEditor(QObject *parent = nullptr);
    Q_PROPERTY(SelectMapObjectModel *  variables READ variables NOTIFY dataChanged)
    Q_INVOKABLE int value(const QString& id) const;
    Q_INVOKABLE void setValue(const QString& id, int value) const;
    Q_INVOKABLE void setName(const QString& id, const QString &value) const;
    Q_INVOKABLE QString desc(const QString& id) const;
    SelectMapObjectModel *variables() const;


    Q_INVOKABLE QString createVariable(const QString & name, int value);

signals:
    void dataChanged();
public slots:
    void init();
    void save();
    void cancel();
private:
private:
    SelectMapObjectModel *m_variables;
    QPair<int, int> m_selectedUid;
    QHash<QString, QString> descriptions;
    int m_stage = 0;
    QString m_stageName;
    QString m_stageDesc;
};

#endif // SCENEVARIABLESEDITOR_H

#ifndef STACKORDEREDITOR_H
#define STACKORDEREDITOR_H
#include "MapObjects/StackObject.h"
#include <QAbstractListModel>

class StackOrderEditor : public QAbstractListModel
{
    Q_OBJECT
public:
    explicit StackOrderEditor(QObject *parent = nullptr);
    enum Roles
    {
        ROLE_ID = Qt::UserRole + 1,
        ROLE_TEXT,
        ROLE_COMBO
    };

    int rowCount(const QModelIndex & parent = QModelIndex()) const;
    virtual QVariant data(const QModelIndex & index, int role = Qt::DisplayRole) const;
    QHash<int, QByteArray> roleNames() const Q_DECL_OVERRIDE;

    Q_PROPERTY(QString targetName READ targetName NOTIFY targetChanged)
    Q_PROPERTY(QString orderDesc READ orderDesc NOTIFY targetChanged)
    Q_PROPERTY(QString orderTarget READ orderTarget WRITE setOrderTarget NOTIFY targetChanged)
    Q_PROPERTY(int orderIndex READ orderIndex WRITE setOrderIndex NOTIFY targetChanged)
    Q_PROPERTY(int order READ order WRITE setOrder NOTIFY targetChanged)
    Q_PROPERTY(int currentTargetType READ currentTargetType NOTIFY targetChanged)

    QString targetName() const;

    QString orderTarget() const;
    QPair<int, int> orderTargetNative() const;
    void setOrderTarget(const QString &newOrderTarget);
    void setOrderTarget(const QPair<int, int> &newOrderTarget);

    int orderIndex() const;
    void setOrderIndex(int newOrderIndex);

    int currentTargetType() const;

    void setOrder(int newOrder);
    int order() const;

    static QString orderDescription(StackObject::Order order);
    static QString orderName(StackObject::Order order);
    static QString orderTAppName(StackObject::Order order);
    const QString &orderDesc() const;

signals:
    void targetChanged();

private:
    QHash<int, QByteArray> m_roles;
    QVector<QSharedPointer<LOrder>> m_orders;
    StackObject::Order m_currentOrder;
    QPair<int, int> m_targetID;
    QString m_orderTarget;
    int m_currentTargetType;
    QString m_orderDesc;
};

#endif // STACKORDEREDITOR_H

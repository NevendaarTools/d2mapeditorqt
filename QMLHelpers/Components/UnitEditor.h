#ifndef UNITEDITOR_H
#define UNITEDITOR_H
#include <QObject>
#include "MapObjects/StackObject.h"
#include "toolsqt/DBFModel/GameClases.h"
#include "QMLHelpers/Models/SelectGameObjectModel.h"

class UnitEditor : public QObject
{
    Q_OBJECT
public:
    UnitEditor(QObject *parent = nullptr);
    Q_PROPERTY(int level READ level WRITE setLevel NOTIFY levelChanged)
    Q_PROPERTY(int baseLevel READ baseLevel NOTIFY unitChanged)
    Q_PROPERTY(int maxLevel READ maxLevel NOTIFY unitChanged)
    Q_PROPERTY(SelectGameObjectModel * unitModifiers READ unitModifiers NOTIFY unitChanged)
    Q_PROPERTY(SelectGameObjectModel * modifiers READ modifiers NOTIFY unitChanged)
    Q_PROPERTY(QString unitid READ unitid WRITE setUnitid NOTIFY unitChanged)
    Q_PROPERTY(QString unitUid READ unitUid WRITE setUnitUid NOTIFY unitChanged)

    Q_PROPERTY(QString icon READ icon NOTIFY unitChanged)
    Q_PROPERTY(int limit READ limit NOTIFY unitChanged)
    Q_PROPERTY(QString name READ name NOTIFY unitChanged)
    Q_PROPERTY(QString desc READ desc NOTIFY unitChanged)
    Q_PROPERTY(QString hp READ hp NOTIFY unitChanged)
    Q_PROPERTY(QString currentHp READ currentHp NOTIFY unitChanged)
    Q_PROPERTY(QString exp READ exp NOTIFY unitChanged)
    Q_PROPERTY(QString currentExp READ currentExp NOTIFY unitChanged)
    Q_PROPERTY(QString expKill READ expKill NOTIFY unitChanged)
    Q_PROPERTY(QString regen READ regen NOTIFY unitChanged)
    Q_PROPERTY(QString armor READ armor NOTIFY unitChanged)
    Q_PROPERTY(QString immun READ immun NOTIFY unitChanged)
    Q_PROPERTY(QString protect READ protect NOTIFY unitChanged)

    Q_PROPERTY(QString ini READ ini NOTIFY unitChanged)

    Q_PROPERTY(QString attack1 READ attack1 NOTIFY unitChanged)
    Q_PROPERTY(QString attack1Source READ attack1Source NOTIFY unitChanged)
    Q_PROPERTY(QString attack1Damage READ attack1Damage NOTIFY unitChanged)
    Q_PROPERTY(QString attack1DamageName READ attack1DamageName NOTIFY unitChanged)
    Q_PROPERTY(QString attack1Accuracy READ attack1Accuracy NOTIFY unitChanged)
    Q_PROPERTY(QString attack1Target READ attack1Target NOTIFY unitChanged)

    Q_PROPERTY(QString attack2 READ attack2 NOTIFY unitChanged)
    Q_PROPERTY(QString attack2Source READ attack2Source NOTIFY unitChanged)
    Q_PROPERTY(QString attack2Damage READ attack2Damage NOTIFY unitChanged)
    Q_PROPERTY(QString attack2DamageName READ attack1DamageName NOTIFY unitChanged)
    Q_PROPERTY(QString attack2Accuracy READ attack2Accuracy NOTIFY unitChanged)
    Q_PROPERTY(QString attack2Target READ attack2Target NOTIFY unitChanged)

    Q_INVOKABLE void addMod(const QString & modId, int count);
    Q_INVOKABLE void removeMod(int index);
    Q_INVOKABLE void save();
    Q_INVOKABLE void cancel();
    int level() const;
    int baseLevel() const;
    int maxLevel() const;
    void setLevel(int newLevel);

    const QString unitid() const;
    void setUnitid(const QString &newUnitid);
    const QString icon() const;
    const QString unitUid() const;
    void setUnitUid(const QString &newUid);

    const QString name() const;
    const QString desc() const;
    const QString hp() const;
    const QString currentHp() const;
    const QString exp() const;
    const QString currentExp() const;
    const QString expKill() const;
    const QString regen() const;
    const QString armor() const;
    const QString immun() const;
    const QString protect() const;

    const QString ini() const;

    const QString attack1() const;
    const QString attack1Source() const;
    const QString attack1Damage() const;
    const QString attack1DamageName() const;
    const QString attack1Accuracy() const;
    const QString attack1Target() const;

    const QString attack2() const;
    const QString attack2Source() const;
    const QString attack2Damage() const;
    const QString attack2DamageName() const;
    const QString attack2Accuracy() const;
    const QString attack2Target() const;

    int limit() const;
    SelectGameObjectModel *unitModifiers() const;

    SelectGameObjectModel *modifiers() const;

signals:
    void levelChanged();

    void unitChanged();
private:
    QString attackDamage(QSharedPointer<Gattack> attack, QString & name, bool withName = false) const;
private:
    UnitObject m_unit;
    QSharedPointer<Gunit> m_base;
    bool m_withUpgr = true;
    int m_limit;
    SelectGameObjectModel *m_unitModifiers;
    SelectGameObjectModel *m_modifiers;
    QVector<QSharedPointer<Gmodif>> m_allModifs;
};

#endif // UNITEDITOR_H

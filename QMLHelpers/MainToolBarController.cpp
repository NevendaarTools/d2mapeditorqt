#include "MainToolBarController.h"
#include <QDebug>
#include <QApplication>
#include <QFileDialog>
#include "Commands/CommandBus.h"
#include <Commands/MenuCommands.h>
#include "Engine/GameInstance.h"
#include "Engine/SettingsManager.h"
#include "Commands/EditCommands.h"
#include "Events/MapEvents.h"
#include "Engine/Map/MapStateHolder.h"

MainToolBarController::MainToolBarController(QObject *parent) : QObject(parent)
{
    m_provider = new ToolsProvider(this);
    m_mapImage = "mapPreview";
    subscribe<MapLoadedEvent>([this](const QVariant &event){onMapLoaded(event);});
    subscribe<HoverChangedEvent>([this](const QVariant &event){onHoverChanged(event);});
}

void MainToolBarController::save()
{
    RESOLVE(CommandBus)->execute(SaveMapCommand());
    qDebug()<<Q_FUNC_INFO;
}

void MainToolBarController::open()
{
    QString latestPath = RESOLVE(SettingsManager)->get("LatestMapFolder", "");
    QString str = QFileDialog::getOpenFileName(0, "Open map", latestPath, "*.csg *.sg *.json");
    if (str != "")
    {
        RESOLVE(SettingsManager)->setValue("LatestMapFolder", str);
        RESOLVE(CommandBus)->execute(OpenMapCommand(str));
    }
}

void MainToolBarController::exportMap()
{
    RESOLVE(CommandBus)->execute(ExportMapCommand());
}

void MainToolBarController::settings()
{
    RESOLVE(CommandBus)->execute(OpenSettingsCommand());
}

void MainToolBarController::exit()
{
    RESOLVE(CommandBus)->execute(CloseMapCommand());
}

void MainToolBarController::requestDataView()
{
    RESOLVE(CommandBus)->execute(OpenDataStorageViewCommand());
}

void MainToolBarController::openQuestLineEditor()
{
    QPair<int,int> uid((int)MapObject::QuestLine, 0);
    RESOLVE(CommandBus)->execute<>(EditCommand{uid});
}

void MainToolBarController::openMapStatistic()
{
    QPair<int,int> uid(MapStatistics, 0);
    RESOLVE(CommandBus)->execute<>(EditCommand{uid});
}

void MainToolBarController::openMapInfoEditor()
{
    QPair<int,int> uid((int)MapObject::Info, 0);
    RESOLVE(CommandBus)->execute<>(EditCommand{uid});
}

void MainToolBarController::setMode(int mode)
{
    qDebug()<<Q_FUNC_INFO<<" "<<mode;
}

ToolsProvider *MainToolBarController::tools() const
{
    return m_provider;
}

QString MainToolBarController::getMapImage() const
{
    return m_mapImage;
}

void MainToolBarController::onMapLoaded(const QVariant &event)
{
    qDebug()<<Q_FUNC_INFO;
    m_mapImage = "";
    emit selectedMapChanged();
    m_mapImage = "mapPreview";
    emit selectedMapChanged();
}

void MainToolBarController::onHoverChanged(const QVariant &event)
{
    HoverChangedEvent hoverEvent = event.value<HoverChangedEvent>();
    const MapGrid & grid = RESOLVE(MapStateHolder)->getGrid();
    const int size = grid.cells.count();
    m_mapPosition = QPointF(hoverEvent.pos.x() / (float(size)), hoverEvent.pos.y() / (float(size)));
    emit mapPositionChanged();
}

void MainToolBarController::minimapClicked(float x, float y)
{
    const MapGrid & grid = RESOLVE(MapStateHolder)->getGrid();
    const int size = grid.cells.count();
    FocusOnCommand focusCom;
    focusCom.pos = QPoint(size * x, size * y);
    RESOLVE(CommandBus)->execute(focusCom);
}

QPointF MainToolBarController::getMapPosition() const
{
    return m_mapPosition;
}

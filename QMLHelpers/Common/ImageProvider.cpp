#include "ImageProvider.h"
#include <QMap>
#include <QPixmap>
#include <QDir>
#include "Engine/GameInstance.h"
#include "Engine/Components/ResourceManager.h"

ImageProvider::ImageProvider() :
    QQuickImageProvider(QQuickImageProvider::Image)
{

}

QImage ImageProvider::requestImage(const QString &id, QSize *size, const QSize &requestedSize)
{
    QImage im = request(id);
    //QSize resSize = *size;
    if (requestedSize.width() > 0 && requestedSize.height() > 0) {
        *size = requestedSize;
        return im.scaled(requestedSize, Qt::KeepAspectRatio);
    }
    return im;
}

ImageProvider::~ImageProvider()
{

}

QImage ImageProvider::request(const QString &id)
{
    auto manager = GameInstance::instance()->get<ResourceManager>();
    if (manager->hasLoadedImagesData(id))
    {
        return manager->getImageNoCash(id);
    }
    if (id.count("-") == 1)
    {
        QStringList idList = id.split("-");
        return manager->getImageNoCash(idList[0], idList[1]);
    }
    if (id.count("-") > 1)
    {
        QStringList idList = id.split("-");
        QString key = idList.last();
        idList.removeLast();
        return  manager->getImageNoCash(idList, key);
    }
    return manager->getImageNoCash(id);
}



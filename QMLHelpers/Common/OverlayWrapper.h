#ifndef OVERLAYWRAPPER_H
#define OVERLAYWRAPPER_H

#include <QObject>
#include <QQuickItem>

class OverlayWrapper : public QObject
{
    Q_OBJECT
public:
    explicit OverlayWrapper(QObject *parent = nullptr);

    Q_INVOKABLE void showTooltip(QQuickItem* item, const QString & text);
    Q_INVOKABLE void hideTooltip();

    Q_INVOKABLE void showPreview(QQuickItem* item, const QString & source, const QString & uid);
    Q_INVOKABLE void showPreview(QQuickItem* item, const QString & uid);
    Q_INVOKABLE void hidePreview();

    Q_INVOKABLE void showMapOverlay();
    Q_INVOKABLE void hideMapOverlay();

    Q_INVOKABLE QString tr(const QString & text);
signals:

};

#endif // OVERLAYWRAPPER_H

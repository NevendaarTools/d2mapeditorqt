#ifndef ENGINEIMAGEPROVIDER_H
#define ENGINEIMAGEPROVIDER_H
#include <qquickimageprovider.h>
#include <QMap>
#include <qstring.h>
#include <QImage>
#include <QPixmap>
#include <QDir>

class ImageProvider : public QQuickImageProvider
{
public:
    ImageProvider();

    QImage requestImage(const QString &id, QSize *size, const QSize &requestedSize);


    ~ImageProvider();
private:
    QImage request(const QString& id);
};

#endif // ENGINEIMAGEPROVIDER_H

#ifndef DYNAMICIMAGEITEM_H
#define DYNAMICIMAGEITEM_H

#include <QQuickPaintedItem>
#include <QPainter>
#include <QTimer>

class DynamicImageItem : public QQuickPaintedItem
{
    Q_OBJECT
public:
    Q_PROPERTY(QString source READ getSource WRITE setSource NOTIFY sourceChanged)

    DynamicImageItem(QQuickItem *parent = nullptr);

    void paint(QPainter *painter) override;
    const QString &getSource() const;
    void setSource(const QString &newSource);

signals:

    void sourceChanged();
private:
    QString m_source;
    QImage m_image;
};

#endif // DYNAMICIMAGEITEM_H

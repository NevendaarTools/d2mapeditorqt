#include "StyleManager.h"
#include "Engine/SettingsManager.h"
#include "Commands/UIEvents.h"
#include "Engine/GameInstance.h"

static SettingsManager settings;

StyleManager::StyleManager(QObject *parent)
    : QObject{parent}
{
    if (!settings.loaded())
    {
        auto settings  = RESOLVE(SettingsManager);
        if (!load(settings->get("style", "1280_720")))
        {
            qDebug()<<"@@@@@@@@@@ settings not found. Initialise defaults!";
            initDefaults();
            save("1280_720");
        }
    }
    subscribe<SettingsChangedEvent>([this](const QVariant &event){onSettingsUpdate(event);});
}

void StyleManager::initDefaults()
{
    settings.addOption(Option::intOption("editorHeight", 680, 100));
    settings.addOption(Option::intOption("editorWidht", 1240, 100));
    settings.addOption(Option::intOption("buttonHeight", 30, 5));
    settings.addOption(Option::intOption("buttonWidht", 80, 5));
    settings.addOption(Option::stringOption("backColor", "gray",""));
    settings.addOption(Option::stringOption("borderColor", "black", ""));
    settings.addOption(Option::intOption("borderWidht", 2, 20));
    settings.addOption(Option::intOption("fontSize", 12, 4, 96));
    settings.addOption(Option::intOption("titleFontSize", 14, 4, 96));
    settings.addOption(Option::intOption("iconSize", 30, 4, 512));
    settings.addOption(Option::intOption("toolBarPanelWidth", 200, 40));
    settings.setLoaded(true);
}

bool StyleManager::load(const QString &name)
{
    qDebug()<<Q_FUNC_INFO<<name;
    if (name == m_lastLoaded)
        return true;
    bool res = settings.loadSimple("Presets/style_" + name + ".json");
    if (res)
        m_lastLoaded = name;
    return res;
}

void StyleManager::save(const QString &name)
{
    settings.saveSimple("Presets/style_" + name + ".json");
}

int StyleManager::editorHeight() const
{
    return settings.getInt("editorHeight", 340);
}

int StyleManager::editorWidht() const
{
    return settings.getInt("editorWidht", 340);
}

int StyleManager::buttonWidht() const
{
    return settings.getInt("buttonWidht", 70);
}

int StyleManager::buttonHeight() const
{
    return settings.getInt("buttonHeight", 30);
}

const QString StyleManager::backColor() const
{
    return settings.get("backColor", "gray");
}

const QString StyleManager::borderColor() const
{
    return settings.get("borderColor", "black");
}

int StyleManager::borderWidht() const
{
    return settings.getInt("borderWidht", 2);
}

int StyleManager::fontSize() const
{
    return settings.getInt("fontSize", 12);
}

int StyleManager::titleFontSize() const
{
    return settings.getInt("titleFontSize", 14);
}

int StyleManager::iconSize() const
{
    return settings.getInt("iconSize", 30);
}

int StyleManager::toolBarPanelWidth() const
{
    return settings.getInt("toolBarPanelWidth", 200);
}

void StyleManager::onSettingsUpdate(const QVariant &eventVar)
{
    SettingsChangedEvent event = eventVar.value<SettingsChangedEvent>();
    if (event.name == "style")
    {
        auto settings  = RESOLVE(SettingsManager);
        if (load(settings->get("style", "1280_720")))
        {
            emit styleChanged();
            emit resolutionChanged();
        }
    }
}

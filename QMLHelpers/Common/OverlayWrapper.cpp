#include "OverlayWrapper.h"
#include "Engine/GameInstance.h"
#include "Commands/OverlayCommands.h"
#include "Commands/CommandBus.h"
#include "Events/EventBus.h"
#include "MapObjects/MapObject.h"
#include "Engine/Components/TranslationHelper.h"

OverlayWrapper::OverlayWrapper(QObject *parent)
    : QObject{parent}
{}

void OverlayWrapper::showTooltip(QQuickItem *item, const QString &text)
{
    auto screanCoordinates = item->mapToGlobal(QPoint(item->width() / 2, item->height() / 2));
    ShowToolTipCommand command{static_cast<int>(screanCoordinates.x()), static_cast<int>(screanCoordinates.y()), text};
    RESOLVE(CommandBus)->execute(command);
}

void OverlayWrapper::hideTooltip()
{
    RESOLVE(CommandBus)->execute(HideToolTipCommand());
}

void OverlayWrapper::showPreview(QQuickItem *item, const QString &source, const QString &uid)
{
    auto screanCoordinates = item->mapToGlobal(QPoint(item->width() / 2, item->height() / 2));
    ShowPreviewCommand command{static_cast<int>(screanCoordinates.x()), static_cast<int>(screanCoordinates.y()), source, uid};
    RESOLVE(CommandBus)->execute(command);
}

void OverlayWrapper::showPreview(QQuickItem *item, const QString &uid)
{
    auto id = IdFromString(uid);
    QString source = "Preview/" + MapObject::typeName((MapObject::Type)id.first) + ".qml";

    showPreview(item, source, uid);
}

void OverlayWrapper::hidePreview()
{
    RESOLVE(CommandBus)->execute(HidePreviewCommand());
}

void OverlayWrapper::showMapOverlay()
{
    MapOverlaySettingsChanged command(true);
    RESOLVE(EventBus)->notify(command);
}

void OverlayWrapper::hideMapOverlay()
{
    MapOverlaySettingsChanged command(false);
    RESOLVE(EventBus)->notify(command);
}

QString OverlayWrapper::tr(const QString &text)
{
    return TranslationHelper::tr(text);
}


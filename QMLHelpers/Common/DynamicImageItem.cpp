#include "DynamicImageItem.h"
#include "Engine/Components/ResourceManager.h"
#include "Engine/GameInstance.h"

DynamicImageItem::DynamicImageItem(QQuickItem *parent) : QQuickPaintedItem(parent)
{
    m_image = QImage(32,32, QImage::Format_ARGB32);
    m_image.fill(Qt::transparent);
}

void DynamicImageItem::paint(QPainter *painter)
{
    painter->drawImage(boundingRect(), m_image);
}

const QString &DynamicImageItem::getSource() const
{
    return m_source;
}

void DynamicImageItem::setSource(const QString &newSource)
{
    m_source = newSource;
    if (RESOLVE(ResourceManager)->hasImagesData(m_source))
    {
        m_image = RESOLVE(ResourceManager)->getImageNoCash(m_source);
        qDebug()<<Q_FUNC_INFO<<m_image;
        qDebug()<<Q_FUNC_INFO<<newSource;
        update();
    }
    emit sourceChanged();
}

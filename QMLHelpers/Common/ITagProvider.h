#ifndef ITAGPROVIDER_H
#define ITAGPROVIDER_H
#include <QString>
#include <QList>
#include <QSharedPointer>

class ITagProvider
{
public:
    ~ITagProvider(){}
    virtual void init() = 0;
    virtual QList<QString> tagGroups() const = 0;
    virtual QList<QString> availableTags(int groupIndex) const = 0;
    virtual QList<QString> itemTags(const QString & item) const = 0;
    virtual bool groupItemsExclusive(int groupIndex) const = 0;
};

static bool acceptTags(QSharedPointer<ITagProvider> m_tagProvider,
                       const QList<QList<QString>> & tags,
                       const QString &id)
{
    if (m_tagProvider.isNull())
        return true;
    QList<QString> unitTags = m_tagProvider->itemTags(id);
    for (int i = 0; i < tags.count(); ++i)
    {
        bool accepted = false;
        if (tags[i].isEmpty())
            continue;
        if (!m_tagProvider->groupItemsExclusive(i))
        {
            for (int k = 0; k < tags[i].count(); ++k)
            {
                if (unitTags.contains(tags[i][k]))
                {
                    accepted = true;
                    break;
                }
            }
        }
        else
        {
            for (int k = 0; k < tags[i].count(); ++k)
            {
                if (!unitTags.contains(tags[i][k]))
                {
                    return false;
                }
            }
            accepted = true;
        }
        if (!accepted)
            return false;
    }
    return true;
}

#endif // ITAGPROVIDER_H

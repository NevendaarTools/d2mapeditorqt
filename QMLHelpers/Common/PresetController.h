#ifndef PRESETCONTROLLER_H
#define PRESETCONTROLLER_H

#include <QObject>
#include "Engine/PresetManager.h"

class PresetController : public QObject
{
    Q_OBJECT
public:
    explicit PresetController(QObject *parent = nullptr);
    Q_PROPERTY(QStringList  presetList READ presetList NOTIFY presetListChanged)
    Q_PROPERTY(QString  type READ type WRITE setType NOTIFY presetListChanged)
    Q_INVOKABLE QStringList getPreset(int index) const;
    Q_INVOKABLE void savePreset(const QString& name, const QStringList& data);

    QStringList presetList() const;
    const QString &type() const;
    void setType(const QString &newType);

signals:
    void presetListChanged();
private:
    QString m_type;
    QList<PresetManager::Preset> m_presets;
};

#endif // PRESETCONTROLLER_H

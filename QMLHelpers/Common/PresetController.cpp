#include "PresetController.h"
#include "Engine/PresetManager.h"
#include "Engine/GameInstance.h"

PresetController::PresetController(QObject *parent) : QObject(parent)
{

}

QStringList PresetController::getPreset(int index) const
{
    return m_presets[index].data;
}

void PresetController::savePreset(const QString &name, const QStringList &data)
{
    RESOLVE(PresetManager)->addPreset(m_type, name, data);
    setType(m_type);
}

QStringList PresetController::presetList() const
{
    QStringList result;
    foreach (const PresetManager::Preset & preset, m_presets)
    {
        result << preset.name;
    }
    return result;
}

const QString &PresetController::type() const
{
    return m_type;
}

void PresetController::setType(const QString &newType)
{
    m_type = newType;
    m_presets = RESOLVE(PresetManager)->presetList(newType);
    emit presetListChanged();
}

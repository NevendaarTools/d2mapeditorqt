#ifndef STYLEMANAGER_H
#define STYLEMANAGER_H

#include <QObject>
#include "Events/EventBus.h"

class StyleManager : public QObject,  public EventSubscriber
{
    Q_OBJECT
public:
    explicit StyleManager(QObject *parent = nullptr);
    Q_PROPERTY(int editorHeight READ editorHeight NOTIFY resolutionChanged)
    Q_PROPERTY(int editorWidht READ editorWidht NOTIFY resolutionChanged)
    Q_PROPERTY(int buttonWidht READ buttonWidht NOTIFY resolutionChanged)
    Q_PROPERTY(int buttonHeight READ buttonHeight NOTIFY resolutionChanged)
    Q_PROPERTY(int iconSize READ iconSize NOTIFY resolutionChanged)
    Q_PROPERTY(QString backColor READ backColor NOTIFY styleChanged)
    Q_PROPERTY(QString borderColor READ borderColor NOTIFY styleChanged)
    Q_PROPERTY(int borderWidht READ borderWidht NOTIFY styleChanged)
    Q_PROPERTY(int fontSize READ fontSize NOTIFY styleChanged)
    Q_PROPERTY(int titleFontSize READ titleFontSize NOTIFY styleChanged)

    Q_PROPERTY(int toolBarPanelWidth READ toolBarPanelWidth NOTIFY styleChanged)

    void initDefaults();
    Q_INVOKABLE bool load(const QString & name);
    Q_INVOKABLE void save(const QString & name);


    int editorHeight() const;
    int editorWidht() const;
    int buttonWidht() const;
    int buttonHeight() const;
    const QString backColor() const;
    const QString borderColor() const;
    int borderWidht() const;
    int fontSize() const;




    int titleFontSize() const;
    int iconSize() const;

    int toolBarPanelWidth() const;
    void onSettingsUpdate(const QVariant &eventVar);
signals:
    void resolutionChanged();
    void styleChanged();
private:
    QString m_lastLoaded;
};

#endif // STYLEMANAGER_H

#ifndef PLACETREASURETOOL_H
#define PLACETREASURETOOL_H
#include "PlaceObjectTool.h"
#include "QMLHelpers/Models/SelectMapObjectModel.h"
#include "QMLHelpers/Models/OwnerSelectModel.h"

class PlaceTreasureTool : public PlaceObjectTool
{
    Q_OBJECT
public:
    explicit PlaceTreasureTool(QObject *parent = nullptr);
    Q_PROPERTY(int imageIndex READ imageIndex WRITE setImageIndex NOTIFY changed)
    Q_INVOKABLE QString toolName() const{return "Place treasure";}

    void onHoverChanged(const QVariant &event);

    Q_INVOKABLE void init();
    Q_INVOKABLE QString imageName(int imageIndex);
public slots:

signals:
    void changed();
public:
    virtual void activate() override;
    virtual QString editorFile() const override {return "Tool_PlaceTreasure.qml";}

    int imageIndex() const;
    void setImageIndex(int newIndex);

private:
    virtual void placeObject() override;
private:
    int m_imageIndex = -1;
};

#endif // PLACETREASURETOOL_H

#ifndef PlaceLMarkTool_H
#define PlaceLMarkTool_H
#include "PlaceObjectTool.h"
#include "QMLHelpers/Models/SelectGameObjectModel.h"

class LmarkTagsProvider: public ITagProvider
{
public:
    void init()
    {
        m_groupNames.clear();
        m_groupTags.clear();
        m_itemTags.clear();

        m_groupNames <<"Size";
        m_groupNames <<"Category";

        QSharedPointer<DBFModel>  data = RESOLVE(DBFModel);
        QVector<QSharedPointer<GLmark>> list = data->getList<GLmark>();
        QList<QString> sizeNames;
        QList<QString> categoryNames;
        QList<QString> raceNames;

        for (int i = 0; i < list.count(); ++i)
        {
            GLmark * newT = list[i].data();
            QString sizeName = QString("%1x%2").arg(newT->cx).arg(newT->cy);
            if (!sizeNames.contains(sizeName))
                sizeNames << sizeName;
            m_itemTags[newT->lmark_id.toUpper()]<<sizeName;

            QString categoryName = newT->category->text;
            if (!categoryNames.contains(categoryName))
                categoryNames << categoryName;
            m_itemTags[newT->lmark_id.toUpper()]<<categoryName;

            QString raceName = "neutral";
            if (!raceNames.contains(raceName))
                raceNames << raceName;
            m_itemTags[newT->lmark_id.toUpper()]<<raceName;
        }

        m_groupTags.append(sizeNames);
        m_groupTags.append(categoryNames);
        m_groupTags.append(raceNames);
    }
    QList<QString> tagGroups() const
    {
        return m_groupNames;
    }
    QList<QString> availableTags(int groupIndex) const
    {
        return m_groupTags[groupIndex];
    }
    QList<QString> itemTags(const QString &item) const
    {
        return m_itemTags[item];
    }
    bool groupItemsExclusive(int groupIndex) const
    {
        Q_UNUSED(groupIndex);
        return false;
    }
private:

private:
    QHash<QString, QList<QString>> m_itemTags;
    QList<QList<QString>> m_groupTags;
    QList<QString> m_groupNames;
};

class PlaceLMarkTool : public PlaceObjectTool
{
    Q_OBJECT
public:
    explicit PlaceLMarkTool(QObject *parent = nullptr);
    Q_PROPERTY(SelectGameObjectModel* model READ model NOTIFY modelChanged)

    Q_INVOKABLE QString toolName() const{return "Place landmark";}

    void onHoverChanged(const QVariant &event);

    Q_INVOKABLE void init();
    virtual void activate() override;
public slots:
    void onSelectObject();
signals:
    // IEditorTool interface
    void modelChanged();

public:
    virtual QString editorFile() const override {return "Tool_PlaceLMark.qml";}

    SelectGameObjectModel *model() const;
private:
    void reset();
    virtual void placeObject() override;
private:
    SelectGameObjectModel *m_model;
};

#endif // PlaceLMarkTool_H

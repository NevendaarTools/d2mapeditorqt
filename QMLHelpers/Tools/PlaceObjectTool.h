#ifndef PLACEOBJECTTOOL_H
#define PLACEOBJECTTOOL_H
#include <QObject>
#include "EditorTool.h"
#include "../../Events/EventBus.h"

class PlaceObjectTool : public QObject, public IEditorTool, EventSubscriber
{
public:
    PlaceObjectTool(const QSize & objSize, QObject * parent = nullptr);
    virtual void draw(QPainter *painter) override;
    void onHoverChanged(const QVariant &event);
    virtual void activate() override;
    virtual void deactivate() override;
    virtual void clearData() override{}
    virtual QRect toolRect() const override;
    virtual void mousePressed(Qt::MouseButton button) override;
    virtual void mouseReleased() override {}
    virtual QObject *toQObject() override{return this;}
    virtual bool neadUpdate() const override;
protected:
    void updateTargetPolygon();
    virtual void placeObject() = 0;
protected:
    QPoint m_lastPos;
    QPolygonF m_targetPolygon;
    QPolygonF m_displayPolygon;
    QImage m_preview;
    bool m_canBePlaced = false;
    QSize m_size;
};

#endif // PLACEOBJECTTOOL_H

#include "PlaceTombTool.h"
#include "../../Events/MapEvents.h"
#include "Commands/CommandBus.h"
#include "Commands/EditCommands.h"
#include "MapObjects/CrystalObject.h"
#include "MapObjects/TombObject.h"
#include "Engine/GameInstance.h"
#include "Engine/Components/ResourceManager.h"

PlaceTombTool::PlaceTombTool(QObject *parent) : PlaceObjectTool(QSize(1,1), parent)
{
    m_killer = new OwnerSelectModel(this);
}


void PlaceTombTool::init()
{
    m_preview = RESOLVE(ResourceManager)->getImageNoCash("IsoCmon", "G000TB0000G");
    m_killer->init();
    m_killer->setOwnerIndexByText("L_NEUTRAL");
    emit changed();
}

void PlaceTombTool::activate()
{
    init();
    PlaceObjectTool::activate();
}

void PlaceTombTool::reset()
{

}

void PlaceTombTool::placeObject()
{
    TombObject * obj = new TombObject();
    D2Tomb::TombEntry entry;
    entry.name = m_stackName;
    entry.turn = m_day;
    entry.killer = m_killer->currentOwner();
    entry.owner = m_killer->currentOwner();
    obj->tomb.entries << entry;
    obj->x = m_lastPos.x();
    obj->y = m_lastPos.y();
    RESOLVE(MapStateHolder)->addObject(QSharedPointer<MapObject>(obj));
}


int PlaceTombTool::day() const
{
    return m_day;
}

void PlaceTombTool::setDay(int newDay)
{
    if (m_day == newDay)
        return;
    m_day = newDay;
    emit changed();
}

const QString &PlaceTombTool::stackName() const
{
    return m_stackName;
}

void PlaceTombTool::setStackName(const QString &newStackName)
{
    if (m_stackName == newStackName)
        return;
    m_stackName = newStackName;
    emit changed();
}

OwnerSelectModel *PlaceTombTool::killer() const
{
    return m_killer;
}

#ifndef SEARCHOBJECTTOOL_H
#define SEARCHOBJECTTOOL_H

#include <QObject>
#include "EditorTool.h"
#include "../../Events/EventBus.h"
#include <QAbstractListModel>

class SearchObjectTool : public QAbstractListModel, public IEditorTool, EventSubscriber
{
    Q_OBJECT
public:
    struct RowData
    {
        QPair<int, int> uid;
        QString objName;
        QString targetName;
        QPoint pos;
        QSize size;
    };
    explicit SearchObjectTool(QObject *parent = nullptr);
    enum Roles
    {
        ROLE_NAME = Qt::UserRole + 1,
        ROLE_POS,
        ROLE_ITEM_NAME
    };
    Q_PROPERTY(QString filter READ filter WRITE setFilter NOTIFY settingsChanged)


    int rowCount(const QModelIndex & parent = QModelIndex()) const;
    virtual QVariant data(const QModelIndex & index, int role = Qt::DisplayRole) const;
    QHash<int, QByteArray> roleNames() const Q_DECL_OVERRIDE;

    void onObjectChanged(const QVariant &event);
    const QString &filter() const;
    void setFilter(const QString &newFilter);
    Q_INVOKABLE void clicked(int index);
    Q_INVOKABLE void dblClicked(int index);
signals:
    void settingsChanged();
private:
    void updateFilter();
    void updateSimple();
    void updateByExpression();
private:
    QHash<int, QByteArray> m_roles;

    QList<RowData> m_data;
    QString m_filter;
    QPair<int, int> m_selectedUid;
    QRect m_selectedRect;
    // IEditorTool interface

public:
    virtual void activate() override;
    virtual void deactivate() override;
    virtual void clearData() override;
    virtual QObject *toQObject() override;
    virtual QString editorFile() const override;
    virtual void draw(QPainter *painter) override;
    virtual QRect toolRect() const override;
    virtual bool neadUpdate() const override;
    void mousePressed(Qt::MouseButton button) override{Q_UNUSED(button)}
    void mouseReleased() override {}
};

#endif // SEARCHOBJECTTOOL_H

#ifndef PLACERUINTOOL_H
#define PLACERUINTOOL_H
#include "PlaceObjectTool.h"
#include "QMLHelpers/Models/SelectGameObjectModel.h"
#include "QMLHelpers/Components/GroupUnitsEditor.h"
#include "QMLHelpers/Components/StackOrderEditor.h"
#include "QMLHelpers/Models/OptionsModel.h"
#include "QMLHelpers/Models/InventoryModel.h"
#include "Generation/Generators.h"
#include "QMLHelpers/Models/OwnerSelectModel.h"

class PlaceRuinTool : public PlaceObjectTool
{
    Q_OBJECT
public:
    explicit PlaceRuinTool(QObject *parent = nullptr);

    Q_PROPERTY(GroupUnitsEditor* stack READ stack WRITE setStack NOTIFY changed)
    Q_PROPERTY(bool generateStack READ generateStack WRITE setGenerateStack NOTIFY changed)
    Q_PROPERTY(bool generateLoot READ generateLoot WRITE setGenerateLoot NOTIFY changed)
    Q_PROPERTY(OptionsModel * stackOptions READ stackOptions NOTIFY changed)
    Q_PROPERTY(OptionsModel * lootOptions READ lootOptions NOTIFY changed)
    Q_PROPERTY(QString reward READ reward NOTIFY changed)
    Q_PROPERTY(QString item READ item NOTIFY changed)
    Q_PROPERTY(int imageIndex READ imageIndex WRITE setImageIndex NOTIFY changed)
    Q_INVOKABLE QString toolName() const{return "Place ruin";}

    void onHoverChanged(const QVariant &event);

    Q_INVOKABLE void init();
    Q_INVOKABLE QString imageName(int imageIndex);
public slots:
    void onStackOptionChanged(const QString & name);
    void onLootOptionChanged(const QString & name);
    void onGenerateStackRequested();
    void onGenerateLootRequested();
    void onUnitDrag();
signals:
    void changed();
public:
    virtual void activate() override;
    virtual QString editorFile() const override {return "Tool_PlaceRuin.qml";}

    GroupUnitsEditor *stack() const;
    bool generateStack() const;
    bool generateLoot() const;
    OptionsModel *stackOptions() const;
    OptionsModel *lootOptions() const;

    void setGenerateStack(bool newGenerateStack);

    void setGenerateLoot(bool newGenerateLoot);
    void setStack(GroupUnitsEditor *newStack);

    const QString &reward() const;

    const QString &item() const;

    int imageIndex() const;
    void setImageIndex(int newImageIndex);
private:
    virtual void placeObject() override;
    void reset();
    QSharedPointer<IStackGenerator> stackGen() const;
    QSharedPointer<ILootGenerator> lootGen() const;
private:
    GroupUnitsEditor *m_stack;
    bool m_generateStack = true;
    bool m_generateLoot = true;
    QSharedPointer<OptionsModel>m_stackOptions;
    QSharedPointer<OptionsModel>m_lootOptions;

    QList<QSharedPointer<IStackGenerator>> m_stackGenerators;
    int m_currentStackGenIndex = 0;
    QList<QSharedPointer<ILootGenerator>> m_lootGenerators;
    int m_currentLootGenIndex = 0;
    InventoryModel *m_inventory;
    OwnerSelectModel *m_ownerModel;
    QString m_reward;
    QString m_item;
    int m_imageIndex = -1;
};

#endif // PLACERUINTOOL_H

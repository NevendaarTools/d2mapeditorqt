#include "SearchObjectTool.h"
#include "../../Events/MapEvents.h"
#include "MapObjects/StackObject.h"
#include "MapObjects/FortObject.h"
#include "MapObjects/RuinObject.h"
#include "MapObjects/TreasureObject.h"
#include "MapObjects/MerchantObject.h"
#include "Commands/EditCommands.h"
#include "Engine/MapView/CustomMapObject.h"
#include "Common/MathHelper.h"
#include "Engine/GameInstance.h"
#include "Engine/Map/MapStateHolder.h"
#include "Engine/Components/TranslationHelper.h"

SearchObjectTool::SearchObjectTool(QObject *parent)
    : QAbstractListModel{parent}
{
    m_roles[ROLE_NAME] = "ObjName";
    m_roles[ROLE_POS] = "ObjPos";
    m_roles[ROLE_ITEM_NAME] = "TargetName";
    m_selectedUid = EMPTY_ID;
}

int SearchObjectTool::rowCount(const QModelIndex &parent) const
{
    if (parent.isValid())
        return 0;
    return m_data.count();
}

QVariant SearchObjectTool::data(const QModelIndex &index, int role) const
{
    int i = index.row();
    if (role == ROLE_NAME)
    {
        return m_data[i].objName;
    }
    if (role == ROLE_POS)
    {
        return QString("%1:%2").arg(m_data[i].pos.x()).arg(m_data[i].pos.y());
    }
    if (role == ROLE_ITEM_NAME)
    {
        return m_data[i].targetName;
    }
    return QVariant();
}

QHash<int, QByteArray> SearchObjectTool::roleNames() const
{
    return m_roles;
}

void SearchObjectTool::onObjectChanged(const QVariant &event)
{
    m_selectedUid = EMPTY_ID;
    updateFilter();
}

void SearchObjectTool::updateFilter()
{
    beginResetModel();
    m_data.clear();
    if (m_filter.count() < 3)
    {
        endResetModel();
        return;
    }
    if (m_filter.startsWith("$"))
        updateByExpression();
    else
        updateSimple();



    endResetModel();
}

void SearchObjectTool::updateSimple()
{
    {
        qDebug()<<"Search in stacks";
        ObjectsCollection collection = RESOLVE(MapStateHolder)->collectionByType(MapObject::Stack);
        foreach (const QSharedPointer<MapObject> & obj, collection.data.values())
        {
            StackObject * casted = static_cast<StackObject*>(obj.data());
            if (casted->stack.name.contains(m_filter, Qt::CaseInsensitive))
            {
                RowData data;
                data.objName = casted->stack.name;
                data.targetName = "";
                data.pos = QPoint(obj->x, obj->y);
                data.size = QSize(1,1);
                data.uid = obj->uid;
                m_data << data;
            }

            for (int i = 0; i < casted->stack.units.count(); ++i)
            {
                QString unitId = RESOLVE(MapStateHolder)->objectByIdT<UnitObject>(casted->stack.units[i].first).id;
                auto unit = RESOLVE(DBFModel)->get<Gunit>(unitId);
                if (unit->name_txt->text.contains(m_filter, Qt::CaseInsensitive))
                {
                    RowData data;
                    data.objName = casted->stack.name;
                    data.targetName = unit->name_txt->text;
                    data.pos = QPoint(obj->x, obj->y);
                    data.size = QSize(1,1);
                    data.uid = obj->uid;
                    m_data << data;
                }
            }
            for (int i = 0; i < casted->stack.inventory.items.count(); ++i)
            {
                QString itemId = casted->stack.inventory.items[i].type;
                auto item = RESOLVE(DBFModel)->get<GItem>(itemId);
                if (item->name_txt->text.contains(m_filter, Qt::CaseInsensitive))
                {
                    RowData data;
                    data.objName = casted->stack.name;
                    data.targetName = item->name_txt->text;
                    data.pos = QPoint(obj->x, obj->y);
                    data.size = QSize(1,1);
                    data.uid = obj->uid;
                    m_data << data;
                }
            }
        }
    }
    {
        qDebug()<<"Search in Forts";
        ObjectsCollection collection = RESOLVE(MapStateHolder)->collectionByType(MapObject::Fort);
        foreach (const QSharedPointer<MapObject> & obj, collection.data.values())
        {
            FortObject * casted = static_cast<FortObject*>(obj.data());
            if (casted->garrison.name.contains(m_filter, Qt::CaseInsensitive))
            {
                RowData data;
                data.objName = casted->name;
                data.targetName = "";
                data.pos = QPoint(obj->x, obj->y);
                data.size = QSize(5,5);
                data.uid = obj->uid;
                m_data << data;
            }
            for (int i = 0; i < casted->garrison.units.count(); ++i)
            {
                QString unitId = RESOLVE(MapStateHolder)->objectByIdT<UnitObject>(casted->garrison.units[i].first).id;
                auto unit = RESOLVE(DBFModel)->get<Gunit>(unitId);
                if (unit->name_txt->text.contains(m_filter, Qt::CaseInsensitive))
                {
                    RowData data;
                    data.objName = casted->name;
                    data.targetName = unit->name_txt->text;
                    data.pos = QPoint(obj->x, obj->y);
                    data.size = QSize(5,5);
                    data.uid = obj->uid;
                    m_data << data;
                }
            }
            for (int i = 0; i < casted->garrison.inventory.items.count(); ++i)
            {
                QString itemId = casted->garrison.inventory.items[i].type;
                auto item = RESOLVE(DBFModel)->get<GItem>(itemId);
                if (item->name_txt->text.contains(m_filter, Qt::CaseInsensitive))
                {
                    RowData data;
                    data.objName = casted->name;
                    data.targetName = item->name_txt->text;
                    data.pos = QPoint(obj->x, obj->y);
                    data.size = QSize(5,5);
                    data.uid = obj->uid;
                    m_data << data;
                }
            }
        }
    }

    {
        qDebug()<<"Search in ruins";
        ObjectsCollection collection = RESOLVE(MapStateHolder)->collectionByType(MapObject::Ruin);
        foreach (const QSharedPointer<MapObject> & obj, collection.data.values())
        {
            RuinObject * casted = static_cast<RuinObject*>(obj.data());
            if (casted->guards.name.contains(m_filter, Qt::CaseInsensitive))
            {
                RowData data;
                data.objName = casted->name;
                data.targetName = "";
                data.pos = QPoint(obj->x, obj->y);
                data.size = QSize(3,3);
                data.uid = obj->uid;
                m_data << data;
            }
            for (int i = 0; i < casted->guards.units.count(); ++i)
            {
                QString unitId = RESOLVE(MapStateHolder)->objectByIdT<UnitObject>(casted->guards.units[i].first).id;
                auto unit = RESOLVE(DBFModel)->get<Gunit>(unitId);
                if (unit->name_txt->text.contains(m_filter, Qt::CaseInsensitive))
                {
                    RowData data;
                    data.objName = casted->name;
                    data.targetName = unit->name_txt->text;
                    data.pos = QPoint(obj->x, obj->y);
                    data.size = QSize(3,3);
                    data.uid = obj->uid;
                    m_data << data;
                }
            }
            QString itemId = casted->item;
            if (isNullId(itemId))
                continue;
            auto item = RESOLVE(DBFModel)->get<GItem>(itemId);
            if (item->name_txt->text.contains(m_filter, Qt::CaseInsensitive))
            {
                RowData data;
                data.objName = casted->name;
                data.targetName = item->name_txt->text;
                data.pos = QPoint(obj->x, obj->y);
                data.size = QSize(3,3);
                data.uid = obj->uid;
                m_data << data;
            }
        }
    }

    {
        qDebug()<<"Search in treasure";
        ObjectsCollection collection = RESOLVE(MapStateHolder)->collectionByType(MapObject::Treasure);
        foreach (const QSharedPointer<MapObject> & obj, collection.data.values())
        {
            TreasureObject * casted = static_cast<TreasureObject*>(obj.data());

            for (int i = 0; i < casted->inventory.items.count(); ++i)
            {
                QString itemId = casted->inventory.items[i].type;
                auto item = RESOLVE(DBFModel)->get<GItem>(itemId);
                if (item->name_txt->text.contains(m_filter, Qt::CaseInsensitive))
                {
                    RowData data;
                    data.objName = TranslationHelper::tr("Treasure");
                    data.targetName = item->name_txt->text;
                    data.pos = QPoint(obj->x, obj->y);
                    data.size = QSize(1,1);
                    data.uid = obj->uid;
                    m_data << data;
                }
            }
        }
    }
    {
        qDebug()<<"Search in merchants";
        ObjectsCollection collection = RESOLVE(MapStateHolder)->collectionByType(MapObject::Merchant);
        foreach (const QSharedPointer<MapObject> & obj, collection.data.values())
        {
            MerchantObject * casted = static_cast<MerchantObject*>(obj.data());

            for (int i = 0; i < casted->inventory.items.count(); ++i)
            {
                QString itemId = casted->inventory.items[i].type;
                auto item = RESOLVE(DBFModel)->get<GItem>(itemId);
                if (item->name_txt->text.contains(m_filter, Qt::CaseInsensitive))
                {
                    RowData data;
                    data.objName = casted->name;
                    data.targetName = item->name_txt->text;
                    data.pos = QPoint(obj->x, obj->y);
                    data.size = QSize(3,3);
                    data.uid = obj->uid;
                    m_data << data;
                }
            }
            for (int i = 0; i < casted->units.items.count(); ++i)
            {
                QString unitId = casted->units.items[i].first.id;
                auto unit = RESOLVE(DBFModel)->get<Gunit>(unitId);
                if (unit->name_txt->text.contains(m_filter, Qt::CaseInsensitive))
                {
                    RowData data;
                    data.objName = casted->name;
                    data.targetName = unit->name_txt->text;
                    data.pos = QPoint(obj->x, obj->y);
                    data.size = QSize(3,3);
                    data.uid = obj->uid;
                    m_data << data;
                }
            }
        }
    }
}

void SearchObjectTool::updateByExpression()
{
    QHash<QString, int> replaces;
    QString filter = m_filter;
    filter.remove(0,1);
    if (!MathHelper::isConditionFinished(filter))
        return;
    QString condition;
    {
        qDebug()<<"Search in stacks";
        ObjectsCollection collection = RESOLVE(MapStateHolder)->collectionByType(MapObject::Stack);
        foreach (const QSharedPointer<MapObject> & obj, collection.data.values())
        {
            StackObject * casted = static_cast<StackObject*>(obj.data());
            replaces["exp_kill"] = groupExpForKill(casted->stack);
            Currency cost;
            for(int i = 0; i < casted->stack.inventory.items.count(); ++i)
            {
                auto item = RESOLVE(DBFModel)->get<GItem>(casted->stack.inventory.items[i].type);
                if (!item.isNull())
                {
                    Currency val = Currency::fromString(item->value);
                    cost += val;
                }
            }
            replaces["items.cost.gold"]   = cost.gold;
            replaces["items.cost.demons"] = cost.demons;
            replaces["items.cost.empire"] = cost.empire;
            replaces["items.cost.undead"] = cost.undead;
            replaces["items.cost.clans"]  = cost.clans;
            replaces["items.cost.elves"]  = cost.elves;

            condition = filter;
            MathHelper::replaceStrings(condition, replaces);
            if (MathHelper::evaluateCondition(condition))
            {
                RowData data;
                data.objName = casted->stack.name;
                data.targetName = "";
                data.pos = QPoint(obj->x, obj->y);
                data.size = QSize(1,1);
                data.uid = obj->uid;
                m_data << data;
            }
        }
    }
    replaces.clear();
    {
        qDebug()<<"Search in forts";
        ObjectsCollection collection = RESOLVE(MapStateHolder)->collectionByType(MapObject::Fort);
        foreach (const QSharedPointer<MapObject> & obj, collection.data.values())
        {
            FortObject * casted = static_cast<FortObject*>(obj.data());
            replaces["exp_kill"] = groupExpForKill(casted->garrison);
            Currency cost;
            for(int i = 0; i < casted->garrison.inventory.items.count(); ++i)
            {
                auto item = RESOLVE(DBFModel)->get<GItem>(casted->garrison.inventory.items[i].type);
                if (!item.isNull())
                {
                    Currency val = Currency::fromString(item->value);
                    cost += val;
                }
            }
            replaces["items.cost.gold"]   = cost.gold;
            replaces["items.cost.demons"] = cost.demons;
            replaces["items.cost.empire"] = cost.empire;
            replaces["items.cost.undead"] = cost.undead;
            replaces["items.cost.clans"]  = cost.clans;
            replaces["items.cost.elves"]  = cost.elves;

            condition = filter;
            MathHelper::replaceStrings(condition, replaces);
            if (MathHelper::evaluateCondition(condition))
            {
                RowData data;
                data.objName = casted->name;
                data.targetName = "";
                data.pos = QPoint(obj->x, obj->y);
                data.size = QSize(5,5);
                data.uid = obj->uid;
                m_data << data;
            }
        }
    }
    replaces.clear();
    {
        qDebug()<<"Search in ruins";
        ObjectsCollection collection = RESOLVE(MapStateHolder)->collectionByType(MapObject::Ruin);
        foreach (const QSharedPointer<MapObject> & obj, collection.data.values())
        {
            RuinObject * casted = static_cast<RuinObject*>(obj.data());
            replaces["exp_kill"] = groupExpForKill(casted->guards);
            Currency cost = casted->reward;
            auto item = RESOLVE(DBFModel)->get<GItem>(casted->item);
            if (!item.isNull())
            {
                Currency val = Currency::fromString(item->value);
                cost += val;
            }

            replaces["items.cost.gold"]   = cost.gold;
            replaces["items.cost.demons"] = cost.demons;
            replaces["items.cost.empire"] = cost.empire;
            replaces["items.cost.undead"] = cost.undead;
            replaces["items.cost.clans"]  = cost.clans;
            replaces["items.cost.elves"]  = cost.elves;

            condition = filter;
            MathHelper::replaceStrings(condition, replaces);
            if (MathHelper::evaluateCondition(condition))
            {
                RowData data;
                data.objName = casted->name;
                data.targetName = "";
                data.pos = QPoint(obj->x, obj->y);
                data.size = QSize(3,3);
                data.uid = obj->uid;
                m_data << data;
            }
        }
    }
    replaces.clear();
    {
        qDebug()<<"Search in treasure";
        ObjectsCollection collection = RESOLVE(MapStateHolder)->collectionByType(MapObject::Treasure);
        foreach (const QSharedPointer<MapObject> & obj, collection.data.values())
        {
            TreasureObject * casted = static_cast<TreasureObject*>(obj.data());

            Currency cost;
            for(int i = 0; i < casted->inventory.items.count(); ++i)
            {
                auto item = RESOLVE(DBFModel)->get<GItem>(casted->inventory.items[i].type);
                if (!item.isNull())
                {
                    Currency val = Currency::fromString(item->value);
                    cost += val;
                }
            }
            replaces["items.cost.gold"]   = cost.gold;
            replaces["items.cost.demons"] = cost.demons;
            replaces["items.cost.empire"] = cost.empire;
            replaces["items.cost.undead"] = cost.undead;
            replaces["items.cost.clans"]  = cost.clans;
            replaces["items.cost.elves"]  = cost.elves;

            condition = filter;
            MathHelper::replaceStrings(condition, replaces);
            if (MathHelper::evaluateCondition(condition))
            {
                RowData data;
                data.objName = "Treasure";
                data.targetName = "";
                data.pos = QPoint(obj->x, obj->y);
                data.size = QSize(1,1);
                data.uid = obj->uid;
                m_data << data;
            }
        }
    }
    {
        qDebug()<<"Search in merchants";
        ObjectsCollection collection = RESOLVE(MapStateHolder)->collectionByType(MapObject::Merchant);
        foreach (const QSharedPointer<MapObject> & obj, collection.data.values())
        {
            MerchantObject * casted = static_cast<MerchantObject*>(obj.data());
        }
    }
}


void SearchObjectTool::activate()
{
    subscribe<ObjectChangedEvent>([this](const QVariant &event){onObjectChanged(event);});
}

void SearchObjectTool::deactivate()
{
    unsub();
}

void SearchObjectTool::clearData()
{
    m_data.clear();
    m_filter.clear();
    m_selectedUid = EMPTY_ID;
    m_selectedRect = QRect();
}

QObject *SearchObjectTool::toQObject()
{
    return this;
}

QString SearchObjectTool::editorFile() const
{
    return "Tool_SearchObject.qml";
}

void SearchObjectTool::draw(QPainter *painter)
{
    if (m_selectedUid != EMPTY_ID)
    {
        painter->setPen(QPen(Qt::green, 3));
        painter->drawEllipse(m_selectedRect);
        painter->setPen(QPen(Qt::red, 2));
        painter->drawLine(m_selectedRect.topLeft(), m_selectedRect.bottomRight());
        painter->drawLine(m_selectedRect.topRight(), m_selectedRect.bottomLeft());
    }
    foreach (auto obj, m_data)
    {
        int w = obj.size.width();
        int h = obj.size.height();
        float x = obj.pos.x();
        float y = obj.pos.y();
        QPointF point = cartesianToIsometric(x * TILE_SIZE, y * TILE_SIZE).toPoint();
        QRect rect(RESOLVE(MapStateHolder)->mapSize() * TILE_SIZE + point.x() -  w * TILE_SIZE / 2,
                    point.y(),
                    w * TILE_SIZE, h * TILE_SIZE);
        painter->setPen(QPen(Qt::green, 3));
        painter->drawEllipse(rect);
    }
}

QRect SearchObjectTool::toolRect() const
{
    return QRect();
}

bool SearchObjectTool::neadUpdate() const
{
    return false;
}

const QString &SearchObjectTool::filter() const
{
    return m_filter;
}

void SearchObjectTool::setFilter(const QString &newFilter)
{
    if (m_filter == newFilter)
        return;
    m_filter = newFilter;
    m_selectedUid = EMPTY_ID;
    updateFilter();
    emit settingsChanged();
}

void SearchObjectTool::clicked(int index)
{
    HoveredObjectChangedEvent command;
    command.hover_in = m_data[index].uid;
    command.hover_out = EMPTY_ID;
    RESOLVE(EventBus)->notify(command);

    FocusOnCommand focusCom;
    focusCom.pos = m_data[index].pos;
    RESOLVE(CommandBus)->execute(focusCom);

    m_selectedUid = m_data[index].uid;
    QSharedPointer<MapObject> mapObj =
            RESOLVE(MapStateHolder)->objectById(m_selectedUid);
    QSharedPointer<IMapObjectAccessor> accessor =
            RESOLVE(AccessorHolder)->objectAccessor(mapObj->uid.first);
    int w = accessor->getW(mapObj);
    int h = accessor->getH(mapObj);
    int x = mapObj->x;
    int y = mapObj->y;
    QPointF point = cartesianToIsometric(x * TILE_SIZE, y * TILE_SIZE).toPoint();
    m_selectedRect = QRect(RESOLVE(MapStateHolder)->mapSize() * TILE_SIZE + point.x() -  w * TILE_SIZE / 2,
                           point.y(),
                           w * TILE_SIZE, h * TILE_SIZE);
}

void SearchObjectTool::dblClicked(int index)
{
    EditCommand command;
    command.uid = m_data[index].uid;
    RESOLVE(CommandBus)->execute(command);
}

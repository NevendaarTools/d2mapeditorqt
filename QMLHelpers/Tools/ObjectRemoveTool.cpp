#include "ObjectRemoveTool.h"
#include "../../Events/MapEvents.h"
#include "Commands/CommandBus.h"
#include "Commands/EditCommands.h"
#include "Engine/GameInstance.h"
#include "Engine/Map/MapStateHolder.h"
#include "Engine/Components/AccessorHolder.h"

ObjectRemoveTool::ObjectRemoveTool(QObject *parent)
    : QObject{parent}
{
    m_hoveredUid = EMPTY_ID;
}

QString ObjectRemoveTool::selectedObjectName() const
{
    if (m_hoveredUid != EMPTY_ID)
    {
        QSharedPointer<MapObject> mapObj =
                RESOLVE(MapStateHolder)->objectById(m_hoveredUid);
        QSharedPointer<IMapObjectAccessor> accessor =
                RESOLVE(AccessorHolder)->objectAccessor(mapObj->uid.first);
        QString result =  accessor->getName(mapObj) + QString(" [%1:%2]").arg(mapObj->x).arg(mapObj->y);
        result += QString("\n(%1  - %2)").arg(m_hoveredUid.first).arg(m_hoveredUid.second);
        return result;
    }
    return QString();
}

void ObjectRemoveTool::activate()
{
    subscribe<HoveredObjectChangedEvent>([this](const QVariant &event){onHoverObjectChanged(event);});
}

void ObjectRemoveTool::deactivate()
{
    unsub();
}

void ObjectRemoveTool::onHoverObjectChanged(const QVariant &event)
{
    HoveredObjectChangedEvent command = event.value<HoveredObjectChangedEvent>();
    if (command.hover_in != EMPTY_ID)
    {
        m_hoveredUid = command.hover_in;
    }
    else
    {
        m_hoveredUid = EMPTY_ID;
    }
    emit objectChanged();
}

void ObjectRemoveTool::draw(QPainter *painter)
{
    if (m_targetPolygon.count() > 0)
    {
        painter->setPen(QPen(Qt::blue, 2));
        painter->drawPolygon(m_targetPolygon);
    }
}



QRect ObjectRemoveTool::toolRect() const
{
    return QRect();
}

bool ObjectRemoveTool::neadUpdate() const
{
    return true;
}

void ObjectRemoveTool::mousePressed(Qt::MouseButton button)
{
    if (button == Qt::MiddleButton)
        return;
    if (button == Qt::LeftButton)
        return;
    if (m_hoveredUid != EMPTY_ID)
    {
        RemoveObjectCommand command;
        command.uid = m_hoveredUid;
        RESOLVE(CommandBus)->execute(command);
        m_hoveredUid = EMPTY_ID;
        emit objectChanged();
    }
}

void ObjectRemoveTool::reset()
{
    m_hoveredUid = EMPTY_ID;
    emit objectChanged();
}

#ifndef OBJECTMOVETOOL_H
#define OBJECTMOVETOOL_H

#include <QObject>
#include "EditorTool.h"
#include "../../Events/EventBus.h"

class ObjectMoveTool : public QObject, public IEditorTool, EventSubscriber
{
    Q_OBJECT
public:
    explicit ObjectMoveTool(QObject *parent = nullptr);
    Q_INVOKABLE QString toolName() const{return "Move object";}
    Q_INVOKABLE QString selectedObjectName() const;

    void onHoverObjectChanged(const QVariant &event);
    void onHoverChanged(const QVariant &event);
signals:
    void objectChanged();

    // IEditorTool interface
public:
    virtual void activate() override;
    virtual void deactivate() override;
    virtual void clearData() override{}

    virtual QRect toolRect() const override;
    virtual bool neadUpdate() const override;
    virtual void draw(QPainter *painter) override;
    void mousePressed(Qt::MouseButton button) override;
    void mouseReleased() override {}
    virtual QObject *toQObject() override {return this;}
    virtual QString editorFile() const override {return "Tool_MoveObject.qml";}
private:
    void reset();
private:
    QPair<int, int> m_hoveredUid;
    QPair<int, int> m_selectedUid;

    QPoint m_objSize;
    QPoint m_lastPos;
    QPolygonF m_sourcePolygon;
    QPolygonF m_targetPolygon;
    bool m_canBePlaced = false;
    mutable bool m_toolWasReset;
};

#endif // OBJECTMOVETOOL_H

#ifndef PLACETOMBTOOL_H
#define PLACETOMBTOOL_H
#include "PlaceObjectTool.h"
#include "QMLHelpers/Models/SelectMapObjectModel.h"
#include "QMLHelpers/Models/OwnerSelectModel.h"

class PlaceTombTool : public PlaceObjectTool
{
    Q_OBJECT
public:
    explicit PlaceTombTool(QObject *parent = nullptr);
    Q_PROPERTY(int day READ day WRITE setDay NOTIFY changed)
    Q_PROPERTY(QString stackName READ stackName WRITE setStackName NOTIFY changed)
    Q_PROPERTY(OwnerSelectModel *  killer READ killer NOTIFY changed)
    Q_INVOKABLE QString toolName() const{return "Place tomb";}
    Q_INVOKABLE void init();
signals:
    void changed();
public:
    virtual void activate() override;
    virtual QString editorFile() const override {return "Tool_PlaceTomb.qml";}

    int selectedIndex() const;
    void setSelectedIndex(int newSelectedIndex);

    int day() const;
    void setDay(int newDay);

    const QString &stackName() const;
    void setStackName(const QString &newStackName);

    OwnerSelectModel *killer() const;

private:
    void reset();
    virtual void placeObject() override;
private:
    int m_day;
    QString m_stackName;
    OwnerSelectModel *m_killer;
};

#endif // PLACETOMBTOOL_H

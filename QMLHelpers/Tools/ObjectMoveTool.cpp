#include "ObjectMoveTool.h"
#include "../../Events/MapEvents.h"
#include "Commands/CommandBus.h"
#include "Commands/EditCommands.h"
#include "MapObjects/FortObject.h"
#include "Engine/GameInstance.h"
#include "Engine/Map/MapStateHolder.h"
#include "Engine/Components/AccessorHolder.h"

ObjectMoveTool::ObjectMoveTool(QObject *parent)
    : QObject{parent}
{
    m_hoveredUid = EMPTY_ID;
    m_selectedUid = EMPTY_ID;
    m_toolWasReset = false;
}

QString ObjectMoveTool::selectedObjectName() const
{
    if (m_selectedUid != EMPTY_ID)
    {
        QSharedPointer<MapObject> mapObj =
                RESOLVE(MapStateHolder)->objectById(m_selectedUid);
        QSharedPointer<IMapObjectAccessor> accessor =
                RESOLVE(AccessorHolder)->objectAccessor(mapObj->uid.first);
        QString result =  accessor->getName(mapObj) + QString(" [%1:%2]").arg(mapObj->x).arg(mapObj->y);
        result += QString("\n(%1  - %2)").arg(m_selectedUid.first).arg(m_selectedUid.second);
        return result;
    }
    return QString();
}

void ObjectMoveTool::activate()
{
    subscribe<HoveredObjectChangedEvent>([this](const QVariant &event){onHoverObjectChanged(event);});
    subscribe<HoverChangedEvent>([this](const QVariant &event){onHoverChanged(event);});
}

void ObjectMoveTool::deactivate()
{
    unsub();
}

void ObjectMoveTool::onHoverObjectChanged(const QVariant &event)
{
    HoveredObjectChangedEvent command = event.value<HoveredObjectChangedEvent>();
    if (command.hover_in != EMPTY_ID)
    {
        m_hoveredUid = command.hover_in;

    }
    else
    {
        m_hoveredUid = EMPTY_ID;
    }
}

void ObjectMoveTool::onHoverChanged(const QVariant &event)
{
    HoverChangedEvent hoverEvent = event.value<HoverChangedEvent>();
    m_lastPos = hoverEvent.pos;
    if (m_selectedUid != EMPTY_ID)
    {

        m_targetPolygon.clear();
        m_targetPolygon<<QPointF(0, 0);
        m_targetPolygon<< cartesianToIsometric(m_objSize.x() * TILE_SIZE, 0);
        m_targetPolygon<< cartesianToIsometric(m_objSize.x() * TILE_SIZE, m_objSize.y() * TILE_SIZE);
        m_targetPolygon<< cartesianToIsometric(0, m_objSize.y() * TILE_SIZE);
        QPointF pos = cartesianToIsometric(hoverEvent.pos.x() * TILE_SIZE, hoverEvent.pos.y() * TILE_SIZE);
        m_targetPolygon.translate(pos.x() + RESOLVE(MapStateHolder)->mapSize() * TILE_SIZE, pos.y());
        MapObjBinding binding = RESOLVE(MapStateHolder)->getGrid().objBinging;
        int mapSize = RESOLVE(MapStateHolder)->mapSize();
        for (int i = 0; i < m_objSize.x(); ++i)
        {
            for (int k = 0; k < m_objSize.y(); ++k)
            {
                int x = hoverEvent.pos.x();
                int y = hoverEvent.pos.y();
                if (x + i >= mapSize || y + k >= mapSize)
                {
                    m_canBePlaced = false;
                    return;
                }
                if (x + i < 0 || y + k < 0)
                {
                    m_canBePlaced = false;
                    return;
                }
                if (binding.binding[x + i][y + k].items.count() > 0)
                {
                    for (int q = 0; q < binding.binding[x + i][y + k].items.count(); ++q)
                    {
                        if (binding.binding[x + i][y + k].items[q] != m_selectedUid)
                        {
                            m_canBePlaced = false;
                            return;
                        }
                    }
                }
            }
        }
        m_canBePlaced = true;
    }
}


void ObjectMoveTool::draw(QPainter *painter)
{
    if (m_sourcePolygon.count() > 0)
    {
        painter->setPen(QPen(Qt::green, 3));
        painter->drawPolygon(m_sourcePolygon);
    }
    if (m_targetPolygon.count() > 0)
    {
        if (m_canBePlaced)
            painter->setPen(QPen(Qt::blue, 2));
        else
            painter->setPen(QPen(Qt::red, 2));
        painter->drawPolygon(m_targetPolygon);
    }
}



QRect ObjectMoveTool::toolRect() const
{
    return QRect();
}

bool ObjectMoveTool::neadUpdate() const
{
    if (m_selectedUid != EMPTY_ID || m_toolWasReset)
    {
        m_toolWasReset = false;
        return true;
    }
    return false;
}

void ObjectMoveTool::mousePressed(Qt::MouseButton button)
{
    if (button == Qt::MiddleButton)
        return;
    if (button == Qt::LeftButton)
        return;
    if (m_selectedUid != EMPTY_ID)
    {
        if (!m_canBePlaced)
            return;
        QSharedPointer<MapObject> mapObj =
                RESOLVE(MapStateHolder)->objectById(m_selectedUid);
        mapObj->x = m_lastPos.x();
        mapObj->y = m_lastPos.y();
        RESOLVE(MapStateHolder)->replaceObject(mapObj);

        if (mapObj->uid.first == MapObject::Fort)
        {
            FortObject * obj = static_cast<FortObject*>(mapObj.data());
            if (obj->visiter.second != -1)
            {
                QSharedPointer<MapObject> stackObj =
                        RESOLVE(MapStateHolder)->objectById(obj->visiter);
                stackObj->x = m_lastPos.x();
                stackObj->y = m_lastPos.y();
                RESOLVE(MapStateHolder)->replaceObject(stackObj);
            }
        }

        QPair<int, int> curId = m_selectedUid;
        reset();
        m_hoveredUid = curId;
        HoveredObjectChangedEvent event;
        event.hover_in = curId;
        event.hover_out = EMPTY_ID;
        RESOLVE(EventBus)->notify(event);
        return;
    }
    if (m_hoveredUid != EMPTY_ID)
    {
        m_selectedUid = m_hoveredUid;
        QSharedPointer<MapObject> mapObj =
                RESOLVE(MapStateHolder)->objectById(m_selectedUid);
        QSharedPointer<IMapObjectAccessor> accessor =
                RESOLVE(AccessorHolder)->objectAccessor(mapObj->uid.first);
        int w = accessor->getW(mapObj);
        int h = accessor->getH(mapObj);
        m_objSize.setX(w);
        m_objSize.setY(h);
        int x= mapObj->x;
        int y = mapObj->y;
        m_sourcePolygon.clear();
        m_sourcePolygon<<QPointF(0, 0);
        m_sourcePolygon<< cartesianToIsometric(w * TILE_SIZE, 0);
        m_sourcePolygon<< cartesianToIsometric(w * TILE_SIZE, h * TILE_SIZE);
        m_sourcePolygon<< cartesianToIsometric(0, h * TILE_SIZE);
        QPointF pos = cartesianToIsometric(x * TILE_SIZE, y * TILE_SIZE);
        m_sourcePolygon.translate(pos.x() + RESOLVE(MapStateHolder)->mapSize() * TILE_SIZE, pos.y());
        m_targetPolygon = m_sourcePolygon;
        emit objectChanged();
    }
}

void ObjectMoveTool::reset()
{
    m_selectedUid = EMPTY_ID;
    m_hoveredUid = EMPTY_ID;
    m_sourcePolygon.clear();
    m_targetPolygon.clear();
    m_toolWasReset = true;
    emit objectChanged();
}

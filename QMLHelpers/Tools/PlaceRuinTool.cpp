#include "PlaceRuinTool.h"
#include "../../Events/MapEvents.h"
#include "Commands/CommandBus.h"
#include "Commands/EditCommands.h"
#include "MapObjects/RuinObject.h"
#include "Generation/VilgeforcStackGenerator.h"
#include "Generation/VilgeforcLootGenerator.h"
#include "Engine/Components/TranslationHelper.h"
#include "Engine/GameInstance.h"
#include "Engine/Components/ResourceManager.h"

PlaceRuinTool::PlaceRuinTool(QObject *parent)
    : PlaceObjectTool(QSize(3,3),parent)
{
    //m_stack = new GroupUnitsEditor(this);
    m_stackOptions = QSharedPointer<OptionsModel>(new OptionsModel(this));
    m_lootOptions = QSharedPointer<OptionsModel>(new OptionsModel(this));
    m_stackGenerators << QSharedPointer<IStackGenerator>(new VilgeforcStackGenerator());
    m_lootGenerators << QSharedPointer<ILootGenerator>(new VilgeforcLootGenerator());
    connect(m_stackOptions.data(), SIGNAL(optionChanged(QString)), SLOT(onStackOptionChanged(QString)));
    connect(m_lootOptions.data(), SIGNAL(optionChanged(QString)), SLOT(onLootOptionChanged(QString)));

    m_ownerModel = new OwnerSelectModel(this);
    m_inventory = new InventoryModel(this);
}

void PlaceRuinTool::activate()
{
    init();
    PlaceObjectTool::activate();
}

void PlaceRuinTool::init()
{
    lootGen()->init(RESOLVE(DBFModel), RESOLVE(MapStateHolder), m_lootOptions);
    stackGen()->init(RESOLVE(DBFModel), RESOLVE(MapStateHolder), m_stackOptions);

    QList<QString> fractionNames;
    auto fractions = RESOLVE(MapStateHolder)->collectionByType(MapObject::SubRace).data.values();
    foreach (auto fraction, fractions)
    {
        SubRaceObject fractionObj = RESOLVE(MapStateHolder)->objectByIdT<SubRaceObject>(fraction->uid);
        QString name = fractionObj.name;
        if (name.isEmpty())
        {
            QSharedPointer<LSubRace> race = RESOLVE(DBFModel)->get<LSubRace>(QString::number(fractionObj.subrace));
            if (!race.isNull())
                name = TranslationHelper::tr(race->text);
        }
        fractionNames << name;
    }
    m_ownerModel->init();
    m_ownerModel->setOwnerIndexByText("L_NEUTRAL");
    setImageIndex(0);
    emit changed();
}

QString PlaceRuinTool::imageName(int imageIndex)
{
    return "G000RU0000" + QString::number(imageIndex).rightJustified(3,'0');
}

void PlaceRuinTool::onStackOptionChanged(const QString &name)
{
    if (name == "Exp Killed")
    {
        QString value = m_stackOptions->value(name);
        m_stackOptions->setValue("Exp result", value);
    }
    stackGen()->propertyChanged(name);
}

void PlaceRuinTool::onLootOptionChanged(const QString &name)
{


}

void PlaceRuinTool::onGenerateStackRequested()
{
    qDebug()<<Q_FUNC_INFO;
    m_stack->setGroup(stackGen()->generate());
    lootGen()->updateContext(0,0, MapLocation(), m_stack->group());
    onGenerateLootRequested();
    emit changed();
}

void PlaceRuinTool::onGenerateLootRequested()
{
    m_inventory->setInventory(lootGen()->generate());
}

void PlaceRuinTool::onUnitDrag()
{
    m_stack->moveUnit(m_stack->dragSource(), m_stack, m_stack->dragTarget());
    m_stack->resetDrag();
    emit changed();
}

void PlaceRuinTool::reset()
{
    m_targetPolygon.clear();
    onGenerateLootRequested();
    onGenerateStackRequested();
    m_canBePlaced = false;
}

QSharedPointer<IStackGenerator> PlaceRuinTool::stackGen() const
{
    return m_stackGenerators[m_currentStackGenIndex];
}

QSharedPointer<ILootGenerator> PlaceRuinTool::lootGen() const
{
    return m_lootGenerators[m_currentLootGenIndex];
}

GroupUnitsEditor *PlaceRuinTool::stack() const
{
    return m_stack;
}

bool PlaceRuinTool::generateStack() const
{
    return m_generateStack;
}

bool PlaceRuinTool::generateLoot() const
{
    return m_generateLoot;
}

OptionsModel *PlaceRuinTool::stackOptions() const
{
    return m_stackOptions.data();
}

OptionsModel *PlaceRuinTool::lootOptions() const
{
    return m_lootOptions.data();
}

void PlaceRuinTool::setGenerateStack(bool newGenerateStack)
{
    if (m_generateStack == newGenerateStack)
        return;
    m_generateStack = newGenerateStack;
    emit changed();
}

void PlaceRuinTool::setGenerateLoot(bool newGenerateLoot)
{
    if (m_generateLoot == newGenerateLoot)
        return;
    m_generateLoot = newGenerateLoot;
    emit changed();
}

void PlaceRuinTool::setStack(GroupUnitsEditor *newStack)
{
    m_stack = newStack;
    connect(m_stack, SIGNAL(dragFinished()), this, SLOT(onUnitDrag()));
    onGenerateStackRequested();
    onGenerateLootRequested();
    emit changed();
}

const QString &PlaceRuinTool::reward() const
{
    return m_reward;
}

const QString &PlaceRuinTool::item() const
{
    return m_item;
}

int PlaceRuinTool::imageIndex() const
{
    return m_imageIndex;
}

void PlaceRuinTool::setImageIndex(int newImageIndex)
{
    if (m_imageIndex == newImageIndex)
        return;
    m_imageIndex = newImageIndex;
    static QStringList ffsource = QStringList()<<"IsoCmon"<<"IsoAnim";
    QString key = imageName(m_imageIndex);
    m_preview = RESOLVE(ResourceManager)->getImageNoCash(ffsource, key);
    emit changed();
}

void PlaceRuinTool::placeObject()
{
    RuinObject obj;
    obj.x = m_lastPos.x();
    obj.y = m_lastPos.y();
    obj.guards = m_stack->group();
    obj.image = m_imageIndex;
    RESOLVE(MapStateHolder)->addObject(QSharedPointer<MapObject>(new RuinObject(obj)));
}

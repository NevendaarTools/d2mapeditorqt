#include "PlaceTreasureTool.h"
#include "../../Events/MapEvents.h"
#include "Commands/CommandBus.h"
#include "Commands/EditCommands.h"
#include "MapObjects/TreasureObject.h"
#include "Generation/VilgeforcStackGenerator.h"
#include "Generation/VilgeforcLootGenerator.h"

PlaceTreasureTool::PlaceTreasureTool(QObject *parent)
    : PlaceObjectTool(QSize(1,1), parent)
{

}

void PlaceTreasureTool::activate()
{
    init();
    PlaceObjectTool::activate();
}

void PlaceTreasureTool::init()
{
    setImageIndex(0);
    emit changed();
}

QString PlaceTreasureTool::imageName(int imageIndex)
{
//    return "G000BG00000" + QString::number(imageIndex).rightJustified(2, '0');
    return "G000BG00001" + QString::number(imageIndex).rightJustified(2, '0');
}

void PlaceTreasureTool::placeObject()
{
    TreasureObject *obj = new TreasureObject;
    obj->x = m_lastPos.x();
    obj->y = m_lastPos.y();
    obj->image = m_imageIndex;
    RESOLVE(MapStateHolder)->addObject(QSharedPointer<MapObject>(obj));
}

int PlaceTreasureTool::imageIndex() const
{
    return m_imageIndex;
}

void PlaceTreasureTool::setImageIndex(int newIndex)
{
    if (m_imageIndex == newIndex)
        return;
    m_imageIndex = newIndex;
    emit changed();
}

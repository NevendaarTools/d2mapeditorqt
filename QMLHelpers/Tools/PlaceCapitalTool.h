#ifndef PlaceCapitalTool_H
#define PlaceCapitalTool_H
#include "PlaceObjectTool.h"
#include "QMLHelpers/Models/SelectGameObjectModel.h"
#include "MapObjects/FortObject.h"

class PlaceCapitalTool : public PlaceObjectTool
{
    Q_OBJECT
public:
    explicit PlaceCapitalTool(QObject *parent = nullptr);
    Q_PROPERTY(SelectGameObjectModel* model READ model NOTIFY modelChanged)
    Q_PROPERTY(bool existFlag READ existFlag NOTIFY modelChanged)

    Q_INVOKABLE QString toolName() const{return "Place race";}

    Q_INVOKABLE void init();
    Q_INVOKABLE bool exist(const QString& id) const;
    Q_INVOKABLE void editCapital(const QString &id);
    virtual void activate() override;
public slots:
    void onSelectObject();
signals:
    void modelChanged();
public:
    virtual QString editorFile() const override {return "Tool_PlaceCapital.qml";}
    SelectGameObjectModel *model() const;
    bool existFlag() const;

private:
    void reset();
    virtual void placeObject() override;
    FortObject * capitalById(const QString & id) const;
private:
    QHash<QString, QString> m_mapping;
    SelectGameObjectModel *m_model;
};

#endif // PlaceCapitalTool_H

#include "ToolsProvider.h"
#include "Tools.h"
#include "Events/MapEvents.h"
#include "Engine/Components/TranslationHelper.h"

ToolsProvider::ToolsProvider(QObject *parent)
    : QObject{parent}
{
    m_activeTool = nullptr;
    m_tools.insert("move", new ObjectMoveTool(this));
    m_tools.insert("place_lmark", new PlaceLMarkTool(this));
    m_tools.insert("place_capital", new PlaceCapitalTool(this));
    m_tools.insert("configure_layers", new LayersTool(this));
    m_tools.insert("search_objects", new SearchObjectTool(this));
    m_tools.insert("place_stack", new PlaceStackTool(this));
    m_tools.insert("map_edit", new MapEditTool(this));
    m_tools.insert("map_generate", new MapGenerationTool(this));
    m_tools.insert("place_ruin", new PlaceRuinTool(this));
    m_tools.insert("place_crystal", new PlaceCrystalTool(this));
    m_tools.insert("place_tomb", new PlaceTombTool(this));
    m_tools.insert("place_village", new PlaceVillageTool(this));
    m_tools.insert("place_treasure", new PlaceTreasureTool(this));
    m_tools.insert("place_merchant", new PlaceMerchantTool(this));
    m_tools.insert("remove", new ObjectRemoveTool(this));
    m_tools.insert("clone", new ObjectCloneTool(this));
    //m_activeTool = m_tools["move"];
    //m_activeTool->activate();
    subscribe<MapLoadedEvent>([this](const QVariant &event){onMapChanged(event);});
}

void ToolsProvider::closeTool()
{
    if (hasActiveTool())
    {
        m_activeTool->deactivate();
        m_activeTool = nullptr;
        emit toolClosed();
    }
}

void ToolsProvider::resetTool()
{
    if (hasActiveTool())
    {
        m_activeTool->clearData();
        emit toolClosed();
    }
}

QString ToolsProvider::tr(const QString &text)
{
    return TranslationHelper::tr(text);
}

void ToolsProvider::mousePressed(Qt::MouseButton button)
{
    if (hasActiveTool())
    {
        qDebug()<<Q_FUNC_INFO<<button;
        m_activeTool->mousePressed(button);
    }
}

bool ToolsProvider::hasActiveTool() const
{
    //return false;
    return m_activeTool != nullptr;
}

void ToolsProvider::mouseReleased()
{
    if (hasActiveTool())
    {
        m_activeTool->mouseReleased();
    }
}

void ToolsProvider::selectTool(const QString &name)
{
    qDebug()<<Q_FUNC_INFO<<name;
    closeTool();
    m_activeTool = m_tools[name];
    m_activeTool->activate();
    qDebug()<<"activate"<<name;
    emit toolChanged();
}

void ToolsProvider::onMapChanged(const QVariant &event)
{
    Q_UNUSED(event);
    closeTool();
}

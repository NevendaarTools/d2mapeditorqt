#include "PlaceVillageTool.h"
#include "../../Events/MapEvents.h"
#include "Commands/CommandBus.h"
#include "Commands/EditCommands.h"
#include "MapObjects/FortObject.h"
#include "Generation/VilgeforcStackGenerator.h"
#include "Generation/VilgeforcLootGenerator.h"
#include "Engine/GameInstance.h"
#include "Engine/Components/ResourceManager.h"

PlaceVillageTool::PlaceVillageTool(QObject *parent)
    : PlaceObjectTool(QSize(4,4), parent)
{

}

void PlaceVillageTool::activate()
{
    init();
    PlaceObjectTool::activate();
}

void PlaceVillageTool::init()
{
    setVillageLvl(1);
    emit changed();
}

QString PlaceVillageTool::imageName(int imageIndex)
{
    return "G000FT0000NE" + QString::number(imageIndex);
}

void PlaceVillageTool::placeObject()
{
    FortObject *obj = new FortObject;
    obj->x = m_lastPos.x();
    obj->y = m_lastPos.y();
    obj->level = m_villageLvl;
    obj->fortType = FortObject::Village;
    RESOLVE(MapStateHolder)->addObject(QSharedPointer<MapObject>(obj));
}

int PlaceVillageTool::villageLvl() const
{
    return m_villageLvl;
}

void PlaceVillageTool::setVillageLvl(int newLvl)
{
    if (m_villageLvl == newLvl)
        return;
    m_villageLvl = newLvl;
    static QStringList ffsource = QStringList()<<"IsoCmon"<<"IsoAnim";
    QString key = imageName(m_villageLvl);
    m_preview = RESOLVE(ResourceManager)->getImageNoCash(ffsource, key);
    emit changed();
}

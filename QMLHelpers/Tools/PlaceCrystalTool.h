#ifndef PLACECRYSTALTOOL_H
#define PLACECRYSTALTOOL_H
#include "PlaceObjectTool.h"
#include "QMLHelpers/Models/SelectMapObjectModel.h"

class PlaceCrystalTool : public PlaceObjectTool
{
    Q_OBJECT
public:
    explicit PlaceCrystalTool(QObject *parent = nullptr);
    Q_PROPERTY(SelectMapObjectModel* crystalsModel READ crystalsModel NOTIFY changed)
    Q_PROPERTY(SelectMapObjectModel* rodsModel READ rodsModel NOTIFY changed)
    Q_PROPERTY(int selectedIndex READ selectedIndex WRITE setSelectedIndex NOTIFY changed)
    Q_INVOKABLE QString toolName() const{return "Place crystal";}
    Q_INVOKABLE void init();
signals:
    void changed();
public:
    virtual void activate() override;
    virtual QString editorFile() const override {return "Tool_PlaceCrystal.qml";}

    int selectedIndex() const;
    void setSelectedIndex(int newSelectedIndex);

    SelectMapObjectModel *crystalsModel() const;
    SelectMapObjectModel *rodsModel() const;
private:
    void reset();
    virtual void placeObject() override;
private:
    int m_selectedIndex = -1;
    SelectMapObjectModel *m_crystalsModel;
    SelectMapObjectModel *m_rodsModel;
};

#endif // PLACECRYSTALTOOL_H

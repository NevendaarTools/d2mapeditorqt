#include "LayersTool.h"
#include "Events/EventBus.h"
#include "Engine/GameInstance.h"
#include "Engine/Components/DisplaySettingsManager.h"

LayersTool::LayersTool(QObject *parent) : QObject(parent)
{
    m_options = new OptionsModel(this);
    m_options->addOption(Option::boolOption("SimpleView", false, "SimpleView_desc"));
    m_options->addOption(Option::title("Information"));
    m_options->addOption(Option::boolOption("GridLayer", false, "GridLayer_desc"));
    m_options->addOption(Option::boolOption("PassableLayer", false, "PassableLayer_desc"));
    m_options->addOption(Option::boolOption("DangerLayer", false, "DangerLayer_desc"));
    m_options->addOption(Option::boolOption("TerraformingLayer", false, "TerraformingLayer_desc"));
    m_options->addOption(Option::boolOption("ForestLayer", false, "ForestLayer_desc"));
    m_options->addOption(Option::boolOption("RoadLayer", false, "RoadLayer_desc"));


    m_options->addOption(Option::spacer());
    m_options->addOption(Option::title("Map Objects"));
    m_options->addOption(Option::boolOption("StackLayer", true, "StackLayer_desc"));
    m_options->addOption(Option::boolOption("LandMarkLayer", true, "LandMarkLayer_desc"));
    m_options->addOption(Option::boolOption("FortsLayer", true, "FortsLayer_desc"));
    m_options->addOption(Option::boolOption("MountainLayer", true, "MountainLayer_desc"));
    m_options->addOption(Option::boolOption("CrystalLayer", true, "CrystalLayer_desc"));
    m_options->addOption(Option::boolOption("RuinLayer", true, "RuinLayer_desc"));
    m_options->addOption(Option::boolOption("TreasureLayer", true, "TreasureLayer_desc"));
    m_options->addOption(Option::boolOption("MerchantLayer", true, "MerchantLayer_desc"));
    m_options->addOption(Option::boolOption("LocationLayer", false, "LocationLayer_desc"));

    connect(m_options, SIGNAL(dataChanged(QModelIndex,QModelIndex,QVector<int>)), this, SLOT(commitSettings()));
}

OptionsModel *LayersTool::options() const
{
    return m_options;
}

void LayersTool::commitSettings()
{
    RESOLVE(DisplaySettingsManager)->settings.dangerEnabled = Option::getBoolOption(m_options->options(), "DangerLayer");
    RESOLVE(DisplaySettingsManager)->settings.passableEnabled = Option::getBoolOption(m_options->options(), "PassableLayer");
    RESOLVE(DisplaySettingsManager)->settings.terraformingEnabled = Option::getBoolOption(m_options->options(), "TerraformingLayer");
    RESOLVE(DisplaySettingsManager)->settings.gridEnabled = Option::getBoolOption(m_options->options(), "GridLayer");
    RESOLVE(DisplaySettingsManager)->settings.forestEnabled = Option::getBoolOption(m_options->options(), "ForestLayer");
    RESOLVE(DisplaySettingsManager)->settings.roadsEnabled = Option::getBoolOption(m_options->options(), "RoadLayer");
    RESOLVE(DisplaySettingsManager)->settings.simpleView = Option::getBoolOption(m_options->options(), "SimpleView");
    RESOLVE(DisplaySettingsManager)->settings.hidenObjects.clear();
    if (!Option::getBoolOption(m_options->options(), "StackLayer"))
        RESOLVE(DisplaySettingsManager)->settings.hidenObjects << MapObject::Stack;
    if (!Option::getBoolOption(m_options->options(), "LandMarkLayer"))
        RESOLVE(DisplaySettingsManager)->settings.hidenObjects << MapObject::LandMark;
    if (!Option::getBoolOption(m_options->options(), "FortsLayer"))
        RESOLVE(DisplaySettingsManager)->settings.hidenObjects << MapObject::Fort;
    if (!Option::getBoolOption(m_options->options(), "MountainLayer"))
        RESOLVE(DisplaySettingsManager)->settings.hidenObjects << MapObject::Mountain;
    if (!Option::getBoolOption(m_options->options(), "CrystalLayer"))
        RESOLVE(DisplaySettingsManager)->settings.hidenObjects << MapObject::Crystal;
    if (!Option::getBoolOption(m_options->options(), "RuinLayer"))
        RESOLVE(DisplaySettingsManager)->settings.hidenObjects << MapObject::Ruin;
    if (!Option::getBoolOption(m_options->options(), "TreasureLayer"))
        RESOLVE(DisplaySettingsManager)->settings.hidenObjects << MapObject::Treasure;
    if (!Option::getBoolOption(m_options->options(), "MerchantLayer"))
        RESOLVE(DisplaySettingsManager)->settings.hidenObjects << MapObject::Merchant;
    if (!Option::getBoolOption(m_options->options(), "LocationLayer"))
        RESOLVE(DisplaySettingsManager)->settings.hidenObjects << MapObject::Location;
    RESOLVE(EventBus)->notify(QVariant::fromValue(DisplaySettingsChangedEvent()));
}


void LayersTool::activate()
{

}

void LayersTool::deactivate()
{
}

QObject *LayersTool::toQObject()
{
    return this;
}

QString LayersTool::editorFile() const
{
    return "Tool_ConfigureLayers.qml";
}

void LayersTool::draw(QPainter *painter)
{
}

QRect LayersTool::toolRect() const
{
    return QRect();
}

bool LayersTool::neadUpdate() const
{
    return false;
}


#include "MapGenerationTool.h"
#include <qrandom.h>
#include "MapObjects/MountainObject.h"
#include "Events/MapEvents.h"
#include "Engine/Components/TranslationHelper.h"
#include "Engine/Map/MapStateHolder.h"
#include "Engine/GameInstance.h"
#include "toolsqt/MapUtils/MapGrid.h"
#include "Engine/SettingsManager.h"

MapGenerationTool::MapGenerationTool(QObject *parent)
    : QObject(parent)
{
    m_tiles.setDefault(TileType::Plain);
    m_tiles.loadDataFromFile("gen_trained_data.bin");
    m_areaDesc = TranslationHelper::tr("Select area using right click.");
}

void MapGenerationTool::learn()
{
    auto inGrid = RESOLVE(MapStateHolder)->map().getGrid();
    std::vector<std::vector<int8_t>> tiles(inGrid.cells.count(), std::vector<int8_t>(inGrid.cells.count(), UnknownTile));

    for (int i = 0; i < inGrid.cells.count(); ++i)
    {
        for (int k = 0; k < inGrid.cells.count(); ++k)
        {
            auto value = inGrid.cells[i][k].value;
            const int ground = tileGround(value);
            if (ground == 3)//water
            {
                tiles[i][k] = TileType::Water;
                continue;
            }
            if (ground == 1) //tree
            {
                tiles[i][k] = (TileType::Tree);
                continue;
            }
            if (ground == 4)//mountain
            {
                tiles[i][k] = (TileType::Mountain);
                continue;
            }
            tiles[i][k] = (TileType::Plain);
        }
    }
    qDebug()<<"Input filled.";
    m_tiles.train(tiles);
    qDebug()<<"Rules filled. Tiles count = "<<m_tiles.getVariants().size();
    m_tiles.saveDataToFile("gen_trained_data.bin");
}
#include <QElapsedTimer>
void MapGenerationTool::fill()
{
    auto inGrid = RESOLVE(MapStateHolder)->getGrid();
    std::vector<std::vector<int8_t>> tiles(inGrid.cells.count(), std::vector<int8_t>(inGrid.cells.count(), UnknownTile));

    //todo: fill extra data - villages, landmarks...
    QElapsedTimer timer;
    timer.start();
    m_tiles.fill(tiles);
    qDebug()<<"data filled, time = "<<timer.elapsed();

    int mountainsCount = 0;
    QList<MountainObject> mountains;
    for (int i = 0; i < inGrid.cells.count(); ++i)
    {
        for (int k = 0; k < inGrid.cells.count(); ++k)
        {
            uint8_t value = tiles[i][k];
            int result = 5;
            if (value == TileType::Water)
            {
                inGrid.cells[i][k] = 3 << 3;
                continue;
            }
            if (value == TileType::Tree)
            {
                result |= (QRandomGenerator::global()->generate() % 10) <<19;//tree index
                result |= 1 << 3;
            }
            else if (value  == TileType::Mountain)
            {
                mountainsCount++;
                result |= 4 << 3;
                MountainObject mountain;
                mountain.w = 1;
                mountain.x = i;
                mountain.y = k;
                mountain.h = 1;
                mountain.image = QRandomGenerator::global()->generate() % 10;
                mountain.race = 4;
                mountains << mountain;
            }
            inGrid.cells[i][k] = result;
        }
    }
//    GameMap map = RESOLVE(MapStateHolder)->map();
//    map.removeByType<MountainObject>();
//    foreach (auto mountain, mountains)
//    {
//        map.addObject(mountain);
//    }
//    map.setGrid(inGrid);
    RESOLVE(MapStateHolder)->setGrid(inGrid);
    qDebug()<<"Map filled. MountainsCount = "<<mountainsCount;
}

void MapGenerationTool::onHoverChanged(const QVariant &event)
{
    HoverChangedEvent hoverEvent = event.value<HoverChangedEvent>();
    m_lastPos = hoverEvent.pos;
    if (m_startPointSet)
    {
        updatePolygon(m_start, m_lastPos);
    }
}

void MapGenerationTool::activate()
{
    subscribe<HoverChangedEvent>([this](const QVariant &event){onHoverChanged(event);});
    RESOLVE(SettingsManager)->setValue("enable_object_menu", false);
}

void MapGenerationTool::deactivate()
{
    unsub();
    RESOLVE(SettingsManager)->setValue("enable_object_menu", true);
}



QRect MapGenerationTool::toolRect() const
{
    return QRect();
}

bool MapGenerationTool::neadUpdate() const
{
    return true;
}

void MapGenerationTool::draw(QPainter *painter)
{
    painter->setPen(QPen(Qt::darkGreen, m_startPointSet? 3: 8));
    painter->drawPolygon(m_targetPolygon);
}

void MapGenerationTool::mousePressed(Qt::MouseButton button)
{
    if (button == Qt::RightButton)
    {
        if (m_startPointSet)
        {
            m_workingRect = QRect(m_start, m_lastPos);
            updatePolygon(m_start, m_lastPos);

            m_areaDesc = TranslationHelper::tr("Start point:") + QString("%1:%2").arg(m_start.x()).arg(m_start.y());
            m_areaDesc +="<br>" + TranslationHelper::tr("End point:") + QString("%1:%2").arg(m_lastPos.x()).arg(m_lastPos.y());
            m_startPointSet = false;
        }
        else
        {
            m_startPointSet = true;
            m_start = m_lastPos;
            m_areaDesc = TranslationHelper::tr("Start point:") + QString("%1:%2").arg(m_start.x()).arg(m_start.y());
        }
        emit changed(m_areaDesc);
    }
}

void MapGenerationTool::updatePolygon(const QPoint &start, const QPoint &end)
{
    int w = end.x() - start.x(), h = end.y() - start.y();
    m_targetPolygon.clear();
    m_targetPolygon<<QPointF(0, 0);
    m_targetPolygon<< cartesianToIsometric(w * TILE_SIZE, 0);
    m_targetPolygon<< cartesianToIsometric(w * TILE_SIZE, h * TILE_SIZE);
    m_targetPolygon<< cartesianToIsometric(0,h * TILE_SIZE);
    QPointF pos = cartesianToIsometric(start.x() * TILE_SIZE, start.y() * TILE_SIZE);
    m_targetPolygon.translate(pos.x() + RESOLVE(MapStateHolder)->mapSize() * TILE_SIZE, pos.y());
}

QString MapGenerationTool::areaDescAreaDesc() const
{
    return m_areaDesc;
}

void MapGenerationTool::reset()
{
    emit objectChanged();
}

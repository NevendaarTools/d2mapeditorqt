#ifndef TOOLSPROVIDER_H
#define TOOLSPROVIDER_H

#include <QObject>
#include "EditorTool.h"
#include "Events/EventBus.h"

class ToolsProvider : public QObject, public EventSubscriber
{
    Q_OBJECT
public:
    explicit ToolsProvider(QObject *parent = nullptr);
    Q_INVOKABLE void closeTool();
    Q_INVOKABLE void resetTool();

    Q_INVOKABLE QObject * currentToolObj() {return m_activeTool->toQObject();}
    Q_INVOKABLE QString editorFile() const {return m_activeTool->editorFile();}
    Q_INVOKABLE QString tr(const QString &text);

    IEditorTool * currentTool() {return m_activeTool;}
    bool hasActiveTool() const;
    void mousePressed(Qt::MouseButton button);
    void mouseReleased();
public slots:
    void selectTool(const QString& name);
    void onMapChanged(const QVariant & event);
signals:
    void toolChanged();
    void toolClosed();

private:
    QHash<QString, IEditorTool*> m_tools;
    IEditorTool* m_activeTool;
};

#endif // TOOLSPROVIDER_H

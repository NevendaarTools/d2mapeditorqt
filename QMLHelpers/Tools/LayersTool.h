#ifndef LAYERSTOOL_H
#define LAYERSTOOL_H

#include <QObject>
#include "EditorTool.h"
#include "../../Events/EventBus.h"
#include <Commands/UIEvents.h>
#include "QMLHelpers/Models/OptionsModel.h"

class LayersTool : public QObject, public IEditorTool
{
    Q_OBJECT
public:
    explicit LayersTool(QObject *parent = nullptr);

    Q_PROPERTY(OptionsModel * options READ options NOTIFY changed)
    OptionsModel *options() const;

public slots:
    void commitSettings();
signals:
    void changed();

private:
    OptionsModel *m_options;

    // IEditorTool interface
public:
    void activate() override;
    void deactivate() override;
    void clearData() override{}

    QObject *toQObject() override;
    QString editorFile() const override;
    void draw(QPainter *painter) override;
    QRect toolRect() const override;
    bool neadUpdate() const override;
    void mousePressed(Qt::MouseButton button) override{Q_UNUSED(button)}
    void mouseReleased() override {}
};

#endif // LAYERSTOOL_H

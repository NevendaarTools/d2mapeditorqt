#include "PlaceMerchantTool.h"
#include "../../Events/MapEvents.h"
#include "Commands/CommandBus.h"
#include "Commands/EditCommands.h"
#include "MapObjects/MerchantObject.h"
#include "Generation/VilgeforcStackGenerator.h"
#include "Generation/VilgeforcLootGenerator.h"
#include "Engine/GameInstance.h"
#include "Engine/Map/MapStateHolder.h"
#include "Engine/Components/ResourceManager.h"

PlaceMerchantTool::PlaceMerchantTool(QObject *parent)
    : PlaceObjectTool(QSize(3,3), parent)
{

}

void PlaceMerchantTool::activate()
{
    init();
    PlaceObjectTool::activate();
}

void PlaceMerchantTool::init()
{
    setImageIndex(0);
    emit changed();
}

QString PlaceMerchantTool::imageName(int imageIndex)
{
    MerchantObject::Type objType = (MerchantObject::Type)m_merchType;
    QString key;
    switch (objType) {
    case MerchantObject::Items:
        key = "G000SI0000MERH";
        break;
    case MerchantObject::Spells:
        key = "G000SI0000MAGE";
        break;
    case MerchantObject::Units:
        key = "G000SI0000MERC";
        break;
    case MerchantObject::Trainer:
        key = "G000SI0000TRAI";
        break;
    }
    key += QString::number(imageIndex).rightJustified(2,'0');
    return key;
}

void PlaceMerchantTool::placeObject()
{
    MerchantObject *obj = new MerchantObject;
    obj->x = m_lastPos.x();
    obj->y = m_lastPos.y();
    obj->image = m_imageIndex;
    obj->type = (MerchantObject::Type)m_merchType;
    RESOLVE(MapStateHolder)->addObject(QSharedPointer<MapObject>(obj));
}

int PlaceMerchantTool::imageIndex() const
{
    return m_imageIndex;
}

void PlaceMerchantTool::setImageIndex(int newIndex)
{
    if (m_imageIndex == newIndex)
        return;
    m_imageIndex = newIndex;
    static QStringList ffsource = QStringList()<<"IsoCmon"<<"IsoAnim";
    QString key = imageName(m_imageIndex);
    m_preview = RESOLVE(ResourceManager)->getImageNoCash(ffsource, key);
    emit changed();
}

int PlaceMerchantTool::merchType() const
{
    return m_merchType;
}

void PlaceMerchantTool::setMerchType(int newMerchType)
{
    if (m_merchType == newMerchType)
        return;
    m_merchType = newMerchType;
    static QStringList ffsource = QStringList()<<"IsoCmon"<<"IsoAnim";
    QString key = imageName(m_imageIndex);
    m_preview = RESOLVE(ResourceManager)->getImageNoCash(ffsource, key);
    emit changed();
}

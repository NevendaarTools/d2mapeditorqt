#ifndef OBJECTREMOVETOOL_H
#define OBJECTREMOVETOOL_H

#include <QObject>
#include "EditorTool.h"
#include "../../Events/EventBus.h"

class ObjectRemoveTool : public QObject, public IEditorTool, EventSubscriber
{
    Q_OBJECT
public:
    explicit ObjectRemoveTool(QObject *parent = nullptr);
    Q_INVOKABLE QString toolName() const{return "Remove object";}
    Q_INVOKABLE QString selectedObjectName() const;

    void onHoverObjectChanged(const QVariant &event);
    void onHoverChanged(const QVariant &event);
signals:
    void objectChanged();

    // IEditorTool interface
public:
    virtual void activate() override;
    virtual void deactivate() override;
    virtual void clearData() override{}

    virtual QRect toolRect() const override;
    virtual bool neadUpdate() const override;
    virtual void draw(QPainter *painter) override;
    void mousePressed(Qt::MouseButton button) override;
    void mouseReleased() override {}
    virtual QObject *toQObject() override {return this;}
    virtual QString editorFile() const override {return "Tool_RemoveObject.qml";}
private:
    void reset();
private:
    QPair<int, int> m_hoveredUid;

    QPoint m_objSize;
    QPoint m_lastPos;
    QPolygonF m_targetPolygon;
};

#endif // OBJECTREMOVETOOL_H

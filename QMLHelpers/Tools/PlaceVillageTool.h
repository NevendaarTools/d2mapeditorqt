#ifndef PLACEVILLAGETOOL_H
#define PLACEVILLAGETOOL_H
#include "PlaceObjectTool.h"

class PlaceVillageTool : public PlaceObjectTool
{
    Q_OBJECT
public:
    explicit PlaceVillageTool(QObject *parent = nullptr);
    Q_PROPERTY(int villageLvl READ villageLvl WRITE setVillageLvl NOTIFY changed)
    Q_INVOKABLE QString toolName() const{return "Place village";}

    void onHoverChanged(const QVariant &event);

    Q_INVOKABLE void init();
    Q_INVOKABLE QString imageName(int imageIndex);
public slots:

signals:
    void changed();
public:
    virtual void activate() override;
    virtual QString editorFile() const override {return "Tool_PlaceVillage.qml";}

    int villageLvl() const;
    void setVillageLvl(int newLvl);

private:
    virtual void placeObject() override;
private:
    int m_villageLvl = -1;
};

#endif // PLACEVILLAGETOOL_H

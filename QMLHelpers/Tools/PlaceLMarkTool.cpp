#include "PlaceLMarkTool.h"
#include "../../Events/MapEvents.h"
#include "Commands/CommandBus.h"
#include "Commands/EditCommands.h"
#include "MapObjects/LandmarkObject.h"
#include "Engine/GameInstance.h"
#include "Engine/Components/ResourceManager.h"
#include "Engine/Map/MapStateHolder.h"

PlaceLMarkTool::PlaceLMarkTool(QObject *parent)
    : PlaceObjectTool(QSize(3,3), parent)
{
    m_model = new SelectGameObjectModel(this);
    connect(m_model, SIGNAL(targetChanged()), SLOT(onSelectObject()));
}

void PlaceLMarkTool::activate()
{
    init();
    PlaceObjectTool::activate();
}

void PlaceLMarkTool::init()
{
    m_model->init<GLmark>(
        [](const QSharedPointer<GameData>& item)
    {
        GLmark * lmark = static_cast<GLmark*>(item.data());
        return lmark->name_txt->text;
    },
        [](const QSharedPointer<GameData>& item)
    {
        GLmark * lmark = static_cast<GLmark*>(item.data());
        return lmark->lmark_id.toUpper();
    });
    QSharedPointer<ITagProvider> tagProvider = QSharedPointer<ITagProvider>(new LmarkTagsProvider());
    m_model->setTagProvider(tagProvider, QStringList());
    emit modelChanged();
}

void PlaceLMarkTool::onSelectObject()
{
    auto selectedUid = m_model->selectedId().toUpper();
    if (selectedUid.isEmpty())
    {
        m_size.setWidth(0);
        m_size.setHeight(0);
        updateTargetPolygon();
        return;
    }
    qDebug()<<"Selected lmark id = "<<selectedUid;
    auto lmark = RESOLVE(DBFModel)->get<GLmark>(selectedUid);
    m_size.setWidth(lmark->cx);
    m_size.setHeight(lmark->cy);
    updateTargetPolygon();
    QStringList ffsource = QStringList()<<"IsoCmon"<<"IsoAnim";
    m_preview = RESOLVE(ResourceManager)->getImageNoCash(ffsource, selectedUid);
}

void PlaceLMarkTool::placeObject()
{
    LandmarkObject *obj = new LandmarkObject;
    obj->x = m_lastPos.x();
    obj->y = m_lastPos.y();
    obj->lmarkId = m_model->selectedId().toUpper();
    RESOLVE(MapStateHolder)->addObject(QSharedPointer<MapObject>(obj));
}

void PlaceLMarkTool::reset()
{
    m_model->setTargetIndex(-1);
}

SelectGameObjectModel *PlaceLMarkTool::model() const
{
    return m_model;
}

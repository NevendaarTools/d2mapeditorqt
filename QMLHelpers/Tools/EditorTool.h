#ifndef EDITORTOOL_H
#define EDITORTOOL_H
#include <QPainter>
#include <QRect>
#include <QObject>

class IEditorTool
{
public:
    virtual ~IEditorTool(){}
    virtual void activate() = 0;
    virtual void deactivate() = 0;
    virtual void clearData() = 0;
    virtual QObject * toQObject() = 0;
    virtual QString editorFile() const = 0;

    virtual void draw(QPainter *painter) = 0;
    virtual QRect toolRect() const = 0;
    virtual bool neadUpdate() const = 0;

    virtual void mousePressed(Qt::MouseButton button) = 0;
    virtual void mouseReleased() = 0;
};

#endif // EDITORTOOL_H

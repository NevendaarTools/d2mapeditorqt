#ifndef MAPEDITTOOL_H
#define MAPEDITTOOL_H
#include <QObject>
#include "EditorTool.h"
#include "../../Events/EventBus.h"
#include <Commands/UIEvents.h>
#include "QMLHelpers/Models/OptionsModel.h"
#include "QMLHelpers/Models/SelectGameObjectModel.h"
#include "QMLHelpers/Models/MountainSelectModel.h"
#include "QMLHelpers/Models/TreeSelectModel.h"

class MapEditTool : public QObject, public IEditorTool, EventSubscriber
{
    Q_OBJECT
public:
    enum BrushType
    {
        Terrain,
        Water,
        Land,
        Clear,
        Road,
        Mountains,
        Trees
    };
    Q_ENUM(BrushType)

    explicit MapEditTool(QObject *parent = nullptr);

    Q_PROPERTY(int brushSize READ brushSize WRITE setBrushSize NOTIFY changed)
    Q_PROPERTY(int brushIndex READ brushIndex WRITE setBrushIndex NOTIFY changed)
    Q_PROPERTY(int brushType READ brushType WRITE setBrushType NOTIFY changed)
    Q_PROPERTY(OptionsModel * options READ options NOTIFY changed)
    Q_PROPERTY(SelectGameObjectModel * terrainModel READ terrainModel NOTIFY changed)
    Q_PROPERTY(MountainSelectModel * mountainsModel READ mountainsModel NOTIFY changed)
    Q_PROPERTY(TreeSelectModel * treesModel READ treesModel NOTIFY changed)
    OptionsModel *options() const;

    virtual void activate() override;
    virtual void deactivate() override;
    virtual void clearData() override{}

    virtual QObject *toQObject() override;
    virtual QString editorFile() const override;
    virtual void draw(QPainter *painter) override;
    virtual QRect toolRect() const override;
    virtual bool neadUpdate() const override;
    void mousePressed(Qt::MouseButton button) override;
    void mouseReleased() override;
    SelectGameObjectModel *terrainModel() const;
    int brushSize() const;
    void setBrushSize(int newBrushSize);
    int brushIndex() const;
    void setBrushIndex(int newBrushIndex);
    MountainSelectModel *mountainsModel() const;
    TreeSelectModel *treesModel() const;
    int brushType() const;
    void setBrushType(int newBrushType);

public slots:
    void commitSettings();
    void onHoverChanged(const QVariant &event);
signals:
    void changed();
private:
    void replaceTerrain(bool replaceGround,     int32_t newValue,   bool fillWater,
                        bool replaceForest,     int32_t newForest);
    void applyBrush();
    void updateTargetPolygon();
    bool replaceMountains(int brushSize, bool remove = false);
    void fillMountains(int brushSize);
private:
    QPolygonF m_displayPolygon;
    QPolygonF m_targetPolygon;
    OptionsModel *m_options;
    TreeSelectModel *m_treesModel;
    MountainSelectModel *m_mountainsModel;
    SelectGameObjectModel *m_terrainModel;
    int m_brushSize = 3;
    BrushType m_brushType = Terrain;
    QPoint m_lastPos;
    int m_brushIndex = 0;
    bool m_mousePressed = false;

};

#endif // MAPEDITTOOL_H

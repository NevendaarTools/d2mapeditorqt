#ifndef MAPGENERATIONTOOL_H
#define MAPGENERATIONTOOL_H
#include <QObject>
#include "EditorTool.h"
#include "../../Events/EventBus.h"
#include <QRandomGenerator>
#include "Generation/WaveFunctionCollapse.h"

enum TileType
{
    UnknownTipe = 0,
    Water,
    Plain,
    Tree,
    Mountain,

    TileCount
};

class MapGenerationTool : public QObject, public IEditorTool, EventSubscriber
{
    Q_OBJECT
public:
    explicit MapGenerationTool(QObject *parent = nullptr);
    Q_INVOKABLE QString toolName() const{return "Generate map";}
    Q_PROPERTY(QString areaDesc READ areaDescAreaDesc NOTIFY changed)

    Q_INVOKABLE void learn();
    Q_INVOKABLE void fill();

    void onHoverChanged(const QVariant &event);

signals:
    void objectChanged();

    // IEditorTool interface
    void changed(QString areaDesc);

public:
    virtual void activate() override;
    virtual void deactivate() override;
    virtual void clearData() override{}

    virtual QRect toolRect() const override;
    virtual bool neadUpdate() const override;
    virtual void draw(QPainter *painter) override;
    void mousePressed(Qt::MouseButton button) override;
    void mouseReleased() override {}
    virtual QObject *toQObject() override {return this;}
    virtual QString editorFile() const override {return "Tool_GenerateMap.qml";}
    QString areaDescAreaDesc() const;

private:
    void reset();
    void updatePolygon(const QPoint & start, const QPoint & end);
private:
    WaveFunctionCollapse m_tiles;
    QRect m_workingRect;
    QPolygonF m_targetPolygon;
    QPoint m_start;
    QPoint m_lastPos;
    bool m_startPointSet = false;
    QString m_areaDesc;
};

#endif // MAPGENERATIONTOOL_H

#include "PlaceCapitalTool.h"
#include "../../Events/MapEvents.h"
#include "Commands/CommandBus.h"
#include "Commands/EditCommands.h"
#include "Engine/GameInstance.h"
#include "Engine/Components/ResourceManager.h"
#include "Engine/Map/MapStateHolder.h"

static QString capitalIdByRace(const QString &raceId)
{
    auto race = RESOLVE(DBFModel)->get<Grace>(raceId);
    QString key = race->race_type.value->text;
    key = "G000FT0000" + key.mid(2, 2) + "0";
    return key;
}

PlaceCapitalTool::PlaceCapitalTool(QObject *parent)
    : PlaceObjectTool(QSize(5,5), parent)
{
    m_model = new SelectGameObjectModel(this);
    connect(m_model, SIGNAL(targetChanged()), SLOT(onSelectObject()));
}

void PlaceCapitalTool::activate()
{
    init();
    PlaceObjectTool::activate();
}

void PlaceCapitalTool::init()
{
    m_mapping.clear();
    auto list = RESOLVE(DBFModel)->getList<Grace>();
    foreach (auto race, list)
    {
        m_mapping.insert(capitalIdByRace(race->race_id.toUpper()), race->race_id.toUpper());
    }

    m_model->init<Grace>(
        [](const QSharedPointer<GameData>& item)
    {
        Grace * race = static_cast<Grace*>(item.data());
        return race->name_txt->text;
    },
        [](const QSharedPointer<GameData>& item)
    {
        Grace * race = static_cast<Grace*>(item.data());
        return capitalIdByRace(race->race_id);
    },
    [](const QSharedPointer<GameData>& item)
    {
        Grace * race = static_cast<Grace*>(item.data());
        return race->playable;
    });
    emit modelChanged();
    m_model->setTargetIndex(0);
    onSelectObject();
}

bool PlaceCapitalTool::exist(const QString &id) const
{
    return (capitalById(id)  != nullptr);
}

void PlaceCapitalTool::editCapital(const QString &id)
{
    FortObject * capital = capitalById(id);
    if (capital != nullptr)
    {
        EditCommand command;
        command.uid = capital->uid;
        RESOLVE(CommandBus)->execute(command);
    }
}

void PlaceCapitalTool::onSelectObject()
{
    auto selectedUid = m_model->selectedId().toUpper();
    if (selectedUid.isEmpty())
    {
        m_size.setWidth(0);
        m_size.setHeight(0);
        return;
    }
    FortObject * capital = capitalById(selectedUid);
    if (capital != nullptr)
    {
        FocusOnCommand command;
        command.pos = QPoint(capital->x + 2, capital->y);
        RESOLVE(CommandBus)->execute(command);
        m_model->setTargetIndex(-1);
        m_size.setWidth(0);
        m_size.setHeight(0);
        m_canBePlaced = false;
    }
    else
    {
        qDebug()<<"Selected capital id = "<<selectedUid;
        m_size.setWidth(5);
        m_size.setHeight(5);
        QStringList ffsource = QStringList()<<"IsoCmon"<<"IsoAnim";
        m_preview = RESOLVE(ResourceManager)->getImageNoCash(ffsource, selectedUid);
    }
}

void PlaceCapitalTool::placeObject()
{
    auto selectedUid = m_model->selectedId().toUpper();
    FortObject * capital = capitalById(selectedUid);
    if (capital != nullptr)
        return;
    FortObject *obj = new FortObject;
    obj->x = m_lastPos.x();
    obj->y = m_lastPos.y();
    obj->fortType = FortObject::Capital;
    obj->raceId = m_mapping.value(m_model->selectedId().toUpper());
    RESOLVE(MapStateHolder)->addObject(QSharedPointer<MapObject>(obj));
    reset();
}

void PlaceCapitalTool::reset()
{
    m_model->setTargetIndex(-1);
}

FortObject *PlaceCapitalTool::capitalById(const QString &id) const
{
    auto value = m_mapping.value(id);
    ObjectsCollection collection = RESOLVE(MapStateHolder)->collectionByType(MapObject::Fort);
    foreach (const QSharedPointer<MapObject> & obj, collection.data.values())
    {
        FortObject * casted = static_cast<FortObject*>(obj.data());
        if (casted->raceId == value)
            return casted;
    }
    return nullptr;
}

SelectGameObjectModel *PlaceCapitalTool::model() const
{
    return m_model;
}

bool PlaceCapitalTool::existFlag() const
{
    return true;
}

#ifndef PLACESTACKTOOL_H
#define PLACESTACKTOOL_H

#include <QObject>
#include "EditorTool.h"
#include "../../Events/EventBus.h"
#include "QMLHelpers/Models/SelectGameObjectModel.h"
#include "MapObjects/FortObject.h"
#include "MapObjects/Diplomacy.h"
#include "QMLHelpers/Components/GroupUnitsEditor.h"
#include "QMLHelpers/Components/StackOrderEditor.h"
#include "QMLHelpers/Models/OptionsModel.h"
#include "QMLHelpers/Models/StringModel.h"
#include "QMLHelpers/Models/InventoryModel.h"
#include "Generation/Generators.h"
#include "QMLHelpers/Models/OwnerSelectModel.h"

class PlaceStackTool : public QObject, public IEditorTool, EventSubscriber
{
    Q_OBJECT
public:
    explicit PlaceStackTool(QObject *parent = nullptr);

    Q_PROPERTY(GroupUnitsEditor* stack READ stack WRITE setStack NOTIFY changed)
    Q_PROPERTY(bool generateStack READ generateStack WRITE setGenerateStack NOTIFY changed)
    Q_PROPERTY(bool generateLoot READ generateLoot WRITE setGenerateLoot NOTIFY changed)
    Q_PROPERTY(OwnerSelectModel *  ownerModel READ ownerModel NOTIFY changed)
    Q_PROPERTY(OptionsModel * stackOptions READ stackOptions NOTIFY changed)
    Q_PROPERTY(OptionsModel * lootOptions READ lootOptions NOTIFY changed)
    Q_PROPERTY(StackOrderEditor* orderEditor READ orderEditor NOTIFY changed)
    Q_PROPERTY(InventoryModel* inventory READ inventory NOTIFY changed)
    Q_INVOKABLE QString toolName() const{return "Place stack";}

    void onHoverChanged(const QVariant &event);

    Q_INVOKABLE void init();
public slots:
    void onStackOptionChanged(const QString & name);
    void onLootOptionChanged(const QString & name);
    void onGenerateStackRequested();
    void onGenerateLootRequested();
    void onUnitDrag();
signals:
    void changed();
public:
    virtual void activate() override;
    virtual void deactivate() override;
    virtual void clearData() override{}

    virtual QRect toolRect() const override;
    virtual bool neadUpdate() const override;
    virtual void draw(QPainter *painter) override;
    void mousePressed(Qt::MouseButton button) override;
    void mouseReleased() override {}
    virtual QObject *toQObject() override{return this;}
    virtual QString editorFile() const override {return "Tool_PlaceStack.qml";}

    GroupUnitsEditor *stack() const;
    bool generateStack() const;
    bool generateLoot() const;
    OptionsModel *stackOptions() const;
    OptionsModel *lootOptions() const;

    StackOrderEditor *orderEditor() const;

    void setGenerateStack(bool newGenerateStack);

    void setGenerateLoot(bool newGenerateLoot);

    InventoryModel *inventory() const;
    void setStack(GroupUnitsEditor *newStack);
    OwnerSelectModel *ownerModel() const;

private:
    void reset();
    QSharedPointer<IStackGenerator> stackGen() const;
    QSharedPointer<ILootGenerator> lootGen() const;
private:
    QPoint m_lastPos;
    QImage m_preview;
    QPolygonF m_targetPolygon;
    bool m_canBePlaced = false;

    GroupUnitsEditor *m_stack;
    StringModel *m_ownersModel;//TODO: Not used any more
    bool m_generateStack = true;
    bool m_generateLoot = true;
    QSharedPointer<OptionsModel>m_stackOptions;
    QSharedPointer<OptionsModel>m_lootOptions;
    StackOrderEditor *m_orderEditor;

    QList<QSharedPointer<IStackGenerator>> m_stackGenerators;
    int m_currentStackGenIndex = 0;
    QList<QSharedPointer<ILootGenerator>> m_lootGenerators;
    int m_currentLootGenIndex = 0;
    InventoryModel *m_inventory;
    OwnerSelectModel *m_ownerModel;
};

#endif // PLACESTACKTOOL_H

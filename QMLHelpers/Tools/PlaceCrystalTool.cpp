#include "PlaceCrystalTool.h"
#include "../../Events/MapEvents.h"
#include "Commands/CommandBus.h"
#include "Commands/EditCommands.h"
#include "MapObjects/CrystalObject.h"
#include "MapObjects/RodObject.h"
#include "Engine/GameInstance.h"
#include "Engine/Components/ResourceManager.h"

PlaceCrystalTool::PlaceCrystalTool(QObject *parent): PlaceObjectTool(QSize(1,1), parent)
{
    m_rodsModel = new SelectMapObjectModel(this);
    m_crystalsModel = new SelectMapObjectModel(this);
}

void PlaceCrystalTool::init()
{
    QList<QSharedPointer<MapObject>> crystals;
    for (int i = 0; i < 6; ++i) {
        CrystalObject * ob = new CrystalObject();
        ob->resource = CrystalObject::ResourceType(i);
        ob->uid.second = i;
        crystals << QSharedPointer<MapObject>(ob);
    }
    auto getCrystalText = [](const QSharedPointer<MapObject>& item)
    {
        CrystalObject * crystal = static_cast<CrystalObject*>(item.data());
        return crystal->CrystalIdByResource(crystal->resource);
    };
    auto getCrystalId = [](const QSharedPointer<MapObject>& item)
    {
        CrystalObject * crystal = static_cast<CrystalObject*>(item.data());
        return crystal->uid;
    };
    m_crystalsModel->init<CrystalObject>(getCrystalText, getCrystalId, crystals);

    auto players = RESOLVE(MapStateHolder)->objectsByType<PlayerObject>();
    QList<QSharedPointer<MapObject>> rods;
    foreach (const auto & player, players) {
        QSharedPointer<Grace> race = RESOLVE(DBFModel)->get<Grace>(player.raceId);
        if (race.isNull() || !race->playable)
            continue;
        RodObject * obj = new RodObject();
        obj->owner = player.uid.second;
        obj->uid=  player.uid;
        rods << QSharedPointer<MapObject>(obj);
    }
    auto getRodText = [](const QSharedPointer<MapObject>& item)
    {
        RodObject * obj = static_cast<RodObject *>(item.data());
        PlayerObject player = obj->owner.get();
        QSharedPointer<Grace> race = RESOLVE(DBFModel)->get<Grace>(player.raceId);
        int raceId = RodObject::rodByRaceID(race->race_type.key);
        QString key = QString("G000RR%1RROD8").arg(QString::number(raceId).rightJustified(4,'0'));
        return key;
    };
    auto getRodId = [](const QSharedPointer<MapObject>& item)
    {
        RodObject * obj = static_cast<RodObject *>(item.data());
        PlayerObject player = obj->owner.get();
        return player.uid;
    };
    m_rodsModel->init<RodObject>(getRodText, getRodId, rods);
    emit changed();
}

void PlaceCrystalTool::activate()
{
    init();
    PlaceObjectTool::activate();
}

void PlaceCrystalTool::reset()
{
    m_selectedIndex = -1;
}

void PlaceCrystalTool::placeObject()
{
    if (m_selectedIndex != -1)
    {
        if (m_selectedIndex < 100) // crystals
        {
            CrystalObject * obj = new CrystalObject();
            obj->resource = CrystalObject::ResourceType(m_selectedIndex);
            obj->x = m_lastPos.x();
            obj->y = m_lastPos.y();
            RESOLVE(MapStateHolder)->addObject(QSharedPointer<MapObject>(obj));
        }
        else
        {
            RodObject * obj = new RodObject();
            auto selectedObj = m_rodsModel->objectAtIndex(m_selectedIndex - 100);
            obj->owner = selectedObj->uid.second;
            obj->x = m_lastPos.x();
            obj->y = m_lastPos.y();
            RESOLVE(MapStateHolder)->addObject(QSharedPointer<MapObject>(obj));//TODO: change terrain
        }
    }
}

int PlaceCrystalTool::selectedIndex() const
{
    return m_selectedIndex;
}

void PlaceCrystalTool::setSelectedIndex(int newSelectedIndex)
{
    if (m_selectedIndex == newSelectedIndex)
        return;
    m_selectedIndex = newSelectedIndex;
    if (m_selectedIndex < 100) // crystals
    {
        QStringList ffsource = QStringList()<<"IsoCmon"<<"IsoAnim";
        m_preview = RESOLVE(ResourceManager)->getImageNoCash(
                    ffsource,
                    CrystalObject::CrystalIdByResource(CrystalObject::ResourceType(m_selectedIndex))
                    );
    }
    else //rods
    {
        QStringList ffsource = QStringList()<<"IsoCmon";
        auto obj = m_rodsModel->objectAtIndex(m_selectedIndex - 100);
        PlayerObject player = RESOLVE(MapStateHolder)->objectByIdT<PlayerObject>(obj->uid);
        QSharedPointer<Grace> race = RESOLVE(DBFModel)->get<Grace>(player.raceId);
        int raceId = RodObject::rodByRaceID(race->race_type.key);
        QString key = QString("G000RR%1RROD8").arg(QString::number(raceId).rightJustified(4,'0'));
        m_preview = RESOLVE(ResourceManager)->getImageNoCash(ffsource, key);
    }
    emit changed();
}

SelectMapObjectModel *PlaceCrystalTool::crystalsModel() const
{
    return m_crystalsModel;
}

SelectMapObjectModel *PlaceCrystalTool::rodsModel() const
{
    return m_rodsModel;
}

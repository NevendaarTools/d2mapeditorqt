#include "PlaceStackTool.h"
#include "../../Events/MapEvents.h"
#include "Commands/CommandBus.h"
#include "Commands/EditCommands.h"
#include "Generation/VilgeforcStackGenerator.h"
#include "Generation/VilgeforcLootGenerator.h"
#include "Engine/Components/TranslationHelper.h"

PlaceStackTool::PlaceStackTool(QObject *parent)
    : QObject{parent}
{
    //m_stack = new GroupUnitsEditor(this);
    m_stackOptions = QSharedPointer<OptionsModel>(new OptionsModel(this));
    m_lootOptions = QSharedPointer<OptionsModel>(new OptionsModel(this));
    m_ownersModel = new StringModel(this);
    m_orderEditor = nullptr;
    m_stackGenerators << QSharedPointer<IStackGenerator>(new VilgeforcStackGenerator());
    m_lootGenerators << QSharedPointer<ILootGenerator>(new VilgeforcLootGenerator());
    connect(m_stackOptions.data(), SIGNAL(optionChanged(QString)), SLOT(onStackOptionChanged(QString)));
    connect(m_lootOptions.data(), SIGNAL(optionChanged(QString)), SLOT(onLootOptionChanged(QString)));

    m_ownerModel = new OwnerSelectModel(this);
    m_inventory = new InventoryModel(this);
}

void PlaceStackTool::activate()
{
    init();
    subscribe<HoverChangedEvent>([this](const QVariant &event){onHoverChanged(event);});
}

void PlaceStackTool::deactivate()
{
    unsub();
}

void PlaceStackTool::onHoverChanged(const QVariant &event)
{
    HoverChangedEvent hoverEvent = event.value<HoverChangedEvent>();

    m_targetPolygon.clear();
    m_targetPolygon<<QPointF(0, 0);
    m_targetPolygon<< cartesianToIsometric(TILE_SIZE, 0);
    m_targetPolygon<< cartesianToIsometric(TILE_SIZE, TILE_SIZE);
    m_targetPolygon<< cartesianToIsometric(0, TILE_SIZE);

    QPointF pos = cartesianToIsometric(hoverEvent.pos.x() * TILE_SIZE, hoverEvent.pos.y() * TILE_SIZE);
    m_targetPolygon.translate(pos.x() + RESOLVE(MapStateHolder)->mapSize() * TILE_SIZE, pos.y());

    m_lastPos = hoverEvent.pos;
    MapObjBinding binding = RESOLVE(MapStateHolder)->getGrid().objBinging;
    int x = hoverEvent.pos.x();
    int y = hoverEvent.pos.y();
    m_canBePlaced = binding.binding[x][y].items.count() == 0;
}

void PlaceStackTool::init()
{
    if (!m_orderEditor)
        m_orderEditor = new StackOrderEditor(this);
    m_orderEditor->setOrder(StackObject::Normal);
    m_orderEditor->setOrderTarget(EMPTY_ID);
    lootGen()->init(RESOLVE(DBFModel), RESOLVE(MapStateHolder), m_lootOptions);
    stackGen()->init(RESOLVE(DBFModel), RESOLVE(MapStateHolder), m_stackOptions);

    QList<QString> fractionNames;
    auto fractions = RESOLVE(MapStateHolder)->collectionByType(MapObject::SubRace).data.values();
    foreach (auto fraction, fractions)
    {
        SubRaceObject fractionObj = RESOLVE(MapStateHolder)->objectByIdT<SubRaceObject>(fraction->uid);
        QString name = fractionObj.name;
        if (name.isEmpty())
        {
            QSharedPointer<LSubRace> race = RESOLVE(DBFModel)->get<LSubRace>(QString::number(fractionObj.subrace));
            if (!race.isNull())
                name = TranslationHelper::tr(race->text);
        }
        fractionNames << name;
    }
    m_ownersModel->setSimpleStringList(fractionNames);
    m_ownerModel->init();
    m_ownerModel->setOwnerIndexByText("L_NEUTRAL");
    emit changed();
}

void PlaceStackTool::onStackOptionChanged(const QString &name)
{
    if (name == "Exp Killed")
    {
        QString value = m_stackOptions->value(name);
        m_stackOptions->setValue("Exp result", value);
    }
    stackGen()->propertyChanged(name);
}
#include <QElapsedTimer>
void PlaceStackTool::onLootOptionChanged(const QString &name)
{

    QElapsedTimer timer;
    timer.start();
    qDebug()<<"#Start generation test";
    for (int i = 0; i < 100; ++i)
    {
        stackGen()->generate();
    }
    //3550 - 4000
    qDebug()<<"#end generation test. time = ";
    qDebug()<<timer.elapsed();
}

void PlaceStackTool::onGenerateStackRequested()
{
    qDebug()<<Q_FUNC_INFO;
    m_stack->setGroup(stackGen()->generate());
    lootGen()->updateContext(0,0, MapLocation(), m_stack->group());
    onGenerateLootRequested();
    emit changed();
}

void PlaceStackTool::onGenerateLootRequested()
{
    m_inventory->setInventory(lootGen()->generate());
}

void PlaceStackTool::onUnitDrag()
{
    m_stack->moveUnit(m_stack->dragSource(), m_stack, m_stack->dragTarget());
    m_stack->resetDrag();
    emit changed();
}

void PlaceStackTool::draw(QPainter *painter)
{
    int dx =  - m_preview.width();
    int dy =  - m_preview.height() + TILE_SIZE;
    QRect rect (dx/2, dy/2, m_preview.width(), m_preview.height());
    rect.translate(m_targetPolygon.boundingRect().center().x(), m_targetPolygon.boundingRect().top());
    painter->drawImage( rect,
                       m_preview,
                       m_preview.rect());
    if (m_canBePlaced)
        painter->setPen(QPen(Qt::blue, 2));
    else
        painter->setPen(QPen(Qt::red, 2));
    painter->drawPolygon(m_targetPolygon);
}



QRect PlaceStackTool::toolRect() const
{
    return QRect();
}

bool PlaceStackTool::neadUpdate() const
{
    return true;
}

void PlaceStackTool::mousePressed(Qt::MouseButton button)
{
    if (button == Qt::LeftButton)
        return;
    if (button == Qt::MiddleButton)
    {
        reset();
        return;
    }
    if (!m_canBePlaced)
        return;
    stackGen()->acceptGeneration();
    StackObject obj;
    obj.x = m_lastPos.x();
    obj.y = m_lastPos.y();
    obj.stack = m_stack->group();
    obj.stack.inventory = m_inventory->inventory();
    obj.order = m_orderEditor->order();
    obj.subrace = m_ownerModel->currentSubrace();
    obj.orderTarget = m_orderEditor->orderTargetNative();
    RESOLVE(MapStateHolder)->addObject(QSharedPointer<MapObject>(new StackObject(obj)));
    reset();
    emit changed();
//    if (!m_selectedUid.isEmpty())
//    {
//        if (!m_canBePlaced)
//            return;
//        CapitalObject obj;
//        obj.x = m_lastPos.x();
//        obj.y = m_lastPos.y();
//        obj.raceId = m_mapping.value(m_selectedUid);
//        AddObjectCommand command;
//        command.object = QSharedPointer<MapObject>(new CapitalObject(obj));
//        RESOLVE(CommandBus)->execute(command);
//        reset();
//        emit modelChanged();
//    }
}

void PlaceStackTool::reset()
{
    m_targetPolygon.clear();
    onGenerateLootRequested();
    onGenerateStackRequested();
    m_canBePlaced = false;
}

QSharedPointer<IStackGenerator> PlaceStackTool::stackGen() const
{
    return m_stackGenerators[m_currentStackGenIndex];
}

QSharedPointer<ILootGenerator> PlaceStackTool::lootGen() const
{
    return m_lootGenerators[m_currentLootGenIndex];
}

GroupUnitsEditor *PlaceStackTool::stack() const
{
    return m_stack;
}

bool PlaceStackTool::generateStack() const
{
    return m_generateStack;
}

bool PlaceStackTool::generateLoot() const
{
    return m_generateLoot;
}

OptionsModel *PlaceStackTool::stackOptions() const
{
    return m_stackOptions.data();
}

OptionsModel *PlaceStackTool::lootOptions() const
{
    return m_lootOptions.data();
}

StackOrderEditor *PlaceStackTool::orderEditor() const
{
    return m_orderEditor;
}

void PlaceStackTool::setGenerateStack(bool newGenerateStack)
{
    if (m_generateStack == newGenerateStack)
        return;
    m_generateStack = newGenerateStack;
    emit changed();
}

void PlaceStackTool::setGenerateLoot(bool newGenerateLoot)
{
    if (m_generateLoot == newGenerateLoot)
        return;
    m_generateLoot = newGenerateLoot;
    emit changed();
}

InventoryModel *PlaceStackTool::inventory() const
{
    return m_inventory;
}

void PlaceStackTool::setStack(GroupUnitsEditor *newStack)
{
    m_stack = newStack;
    connect(m_stack, SIGNAL(dragFinished()), this, SLOT(onUnitDrag()));
    onGenerateStackRequested();
    onGenerateLootRequested();
    emit changed();
}

OwnerSelectModel *PlaceStackTool::ownerModel() const
{
    return m_ownerModel;
}

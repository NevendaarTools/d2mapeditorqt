#ifndef TOOLS_H
#define TOOLS_H
#include "LayersTool.h"
#include "MapEditTool.h"
#include "MapGenerationTool.h"
#include "ObjectMoveTool.h"
#include "SearchObjectTool.h"
#include "ToolsProvider.h"
#include "PlaceCapitalTool.h"
#include "PlaceRuinTool.h"
#include "PlaceStackTool.h"
#include "PlaceLMarkTool.h"
#include "PlaceCrystalTool.h"
#include "PlaceTombTool.h"
#include "PlaceVillageTool.h"
#include "PlaceTreasureTool.h"
#include "PlaceMerchantTool.h"
#include "ObjectRemoveTool.h"
#include "ObjectCloneTool.h"

static void toolsRegister()
{
    qmlRegisterType<ToolsProvider>("QMLToolsProvider", 1, 0,"ToolsProvider");

    qmlRegisterType<PlaceLMarkTool>("QMLPlaceLMarkTool", 1, 0,"PlaceLMarkTool");
    qmlRegisterType<PlaceCapitalTool>("QMLPlaceCapitalTool", 1, 0,"PlaceCapitalTool");
    qmlRegisterType<PlaceStackTool>("QMLPlaceStackTool", 1, 0,"PlaceStackTool");
    qmlRegisterType<PlaceRuinTool>("QMLPlaceRuinTool", 1, 0,"PlaceRuinTool");
    qmlRegisterType<LayersTool>("QMLLayersTool", 1, 0,"LayersTool");
    qmlRegisterType<MapEditTool>("QMLMapEditTool", 1, 0,"MapEditTool");
    qmlRegisterType<SearchObjectTool>("QMLSearchObjectTool", 1, 0,"SearchObjectTool");
    qmlRegisterType<MapGenerationTool>("QMLMapGenerationTool", 1, 0,"MapGenerationTool");
    qmlRegisterType<PlaceCrystalTool>("QMLPlaceCrystalTool", 1, 0,"PlaceCrystalTool");
    qmlRegisterType<PlaceTombTool>("QMLPlaceTombTool", 1, 0,"PlaceTombTool");
    qmlRegisterType<PlaceVillageTool>("QMLPlaceVillageTool", 1, 0,"PlaceVillageTool");
    qmlRegisterType<PlaceTreasureTool>("QMLPlaceTreasureTool", 1, 0,"PlaceTreasureTool");
    qmlRegisterType<PlaceMerchantTool>("QMLPlaceMerchantTool", 1, 0,"PlaceMerchantTool");
    qmlRegisterType<ObjectMoveTool>("QMLObjectMoveTool", 1, 0,"ObjectMoveTool");
    qmlRegisterType<ObjectRemoveTool>("QMLObjectRemoveTool", 1, 0,"ObjectRemoveTool");
    qmlRegisterType<ObjectCloneTool>("QMLObjectCloneTool", 1, 0,"ObjectCloneTool");
}

#endif // TOOLS_H

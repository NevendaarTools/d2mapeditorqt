#ifndef PLACEMERCHANTTOOL_H
#define PLACEMERCHANTTOOL_H
#include "PlaceObjectTool.h"
#include "QMLHelpers/Models/SelectMapObjectModel.h"
#include "QMLHelpers/Models/OwnerSelectModel.h"

class PlaceMerchantTool : public PlaceObjectTool
{
    Q_OBJECT
public:
    explicit PlaceMerchantTool(QObject *parent = nullptr);
    Q_PROPERTY(int merchType READ merchType WRITE setMerchType NOTIFY changed)
    Q_PROPERTY(int imageIndex READ imageIndex WRITE setImageIndex NOTIFY changed)
    Q_INVOKABLE QString toolName() const{return "Place merchant";}

    void onHoverChanged(const QVariant &event);

    Q_INVOKABLE void init();
    Q_INVOKABLE QString imageName(int imageIndex);
public slots:

signals:
    void changed();
public:
    virtual void activate() override;
    virtual QString editorFile() const override {return "Tool_PlaceMerchant.qml";}

    int imageIndex() const;
    void setImageIndex(int newIndex);

    int merchType() const;
    void setMerchType(int newMerchType);

private:
    virtual void placeObject() override;
private:
    int m_imageIndex = -1;
    int m_merchType = 0;
};

#endif // PLACEMERCHANTTOOL_H

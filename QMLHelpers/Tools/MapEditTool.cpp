#include "MapEditTool.h"
#include "../../Events/MapEvents.h"
#include "Commands/CommandBus.h"
#include <QRandomGenerator>
#include "MapObjects/MountainObject.h"
#include "Common/Profiling.h"
#include "Engine/Map/MapStateHolder.h"
#include "Engine/SettingsManager.h"
#include "Engine/GameInstance.h"

MapEditTool::MapEditTool(QObject *parent) : QObject(parent)
{
    m_options = new OptionsModel(this);
    m_options->addOption(Option::boolOption("SimpleView", false, "SimpleView_desc"));
    m_options->addOption(Option::spacer());
    m_options->addOption(Option::title("Map Objects"));

    connect(m_options, SIGNAL(dataChanged(QModelIndex,QModelIndex,QVector<int>)), this, SLOT(commitSettings()));
    m_treesModel = new TreeSelectModel(this);
    m_mountainsModel = new MountainSelectModel(this);
    m_terrainModel = new SelectGameObjectModel(this);
//    connect(m_model, SIGNAL(targetChanged()), SLOT(onSelectObject()));
//    connect(m_model, SIGNAL(targetChanged()), SLOT(onSelectObject()));
}

OptionsModel *  MapEditTool::options() const
{
    return m_options;
}

void MapEditTool::commitSettings()
{

}

int32_t replaceRightmost5Bits(int32_t value, int8_t newTerrain) {
    int32_t clearMask = ~0x1F;
    value &= clearMask;
    int32_t newBits = newTerrain & 0x1F;
    value |= newBits;
    return value;
}

int32_t replaceLeftmost6Bits(int32_t value, int8_t newTerrain) {
    int32_t clearMask = ~(0x3F << 26);
    value &= clearMask;
    int32_t newBits = (newTerrain & 0x3F) << 26;
    value |= newBits;
    return value;
}

int32_t replaceSecondLeftmost6Bits(int32_t value, int8_t newTerrain) {
    int32_t clearMask = ~(0x3F << 20);
    value &= clearMask;
    int32_t newBits = (newTerrain & 0x3F) << 20;
    value |= newBits;
    return value;
}

bool hasTopRoad(int x, int y, MapGrid & grid)
{
    if (y == 0)
        return true;
    return grid.cells[x][y - 1].roadType != -1;
}
bool hasBottomRoad(int x, int y, MapGrid & grid)
{
    if (y >= grid.cells.count())
        return true;
    return grid.cells[x][y + 1].roadType != -1;
}
bool hasLeftRoad(int x, int y, MapGrid & grid)
{
    if (x == 0)
        return true;
    return grid.cells[x - 1][y].roadType != -1;
}
bool hasRightRoad(int x, int y, MapGrid & grid)
{
    if (x >= grid.cells.count())
        return true;
    return grid.cells[x + 1][y].roadType != -1;
}

void updateRoad(int x, int y, MapGrid & grid)
{
    if (x < 0 || y < 0 || x >= grid.cells.count() || y >= grid.cells.count())
        return;
    if (grid.cells[x][y].roadType < 0)
        return;
    int roadType = 0;
    if (hasTopRoad(x, y, grid))
        roadType |= 1;
    if (hasBottomRoad(x, y, grid))
        roadType |= 2;
    if (hasLeftRoad(x, y, grid))
        roadType |= 4;
    if (hasRightRoad(x, y, grid))
        roadType |= 8;
    if (roadType == 0 || roadType == 15)
    {
        grid.cells[x][y].roadType = 0;
        return;
    }
    if (roadType == 12)
    {
        grid.cells[x][y].roadType = 2;
        return;
    }
    if (roadType == 3)
    {
        grid.cells[x][y].roadType = 3;
        return;
    }
    if (roadType == 7)
    {
        grid.cells[x][y].roadType = 4;
        return;
    }
    if (roadType == 13)
    {
        grid.cells[x][y].roadType = 5;
        return;
    }
    if (roadType == 14)
    {
        grid.cells[x][y].roadType = 6;
        return;
    }
    if (roadType == 11)
    {
        grid.cells[x][y].roadType = 7;
        return;
    }
    if (roadType == 9)
    {
        grid.cells[x][y].roadType = 8;
        return;
    }
    if (roadType == 10)
    {
        grid.cells[x][y].roadType = 9;
        return;
    }
    if (roadType == 6)
    {
        grid.cells[x][y].roadType = 10;
        return;
    }
    if (roadType == 5)
    {
        grid.cells[x][y].roadType = 11;
        return;
    }
    if (roadType == 8)
    {
        grid.cells[x][y].roadType = 12;
        return;
    }
    if (roadType == 2)
    {
        grid.cells[x][y].roadType = 13;
        return;
    }
    if (roadType == 1)
    {
        grid.cells[x][y].roadType = 14;
        return;
    }
    if (roadType == 4)
    {
        grid.cells[x][y].roadType = 15;
        return;
    }
    grid.cells[x][y].roadType = 16;
}

void MapEditTool::applyBrush()
{
    TimerMeasure("applyBrush");
    if (m_brushType == Terrain)
    {
        auto terr = m_terrainModel->itemAtIndex(m_brushIndex);
        Lterrain * terrain = static_cast<Lterrain*>(terr.data());
        replaceTerrain(true, terrain->id, false,
                       false, 0);
        return;
    }
    if (m_brushType == Water)
    {
        replaceTerrain(true, 29, true,
                       false, 0);
        return;
    }
    if (m_brushType == Land)
    {
        replaceTerrain(true, 5, true,
                       false, 0);
        return;
    }
    if (m_brushType == Clear)
    {
        replaceTerrain(false, 0, false,
                       true, 0);
        replaceMountains(m_brushSize, true);
        return;
    }
    if (m_brushType == Mountains)
    {
        if (m_brushIndex != 0)
        {
            MountainInfo info = m_mountainsModel->info(m_brushIndex);
            if (info.size != 1)
            {
                if (replaceMountains(info.size))
                {
                    int startX = m_lastPos.x() - info.size / 2;
                    int startY = m_lastPos.y() - info.size / 2;
                    MountainObject object;
                    object.x = startX;
                    object.y = startY;
                    object.w = info.size;
                    object.h = info.size;
                    object.race = 0;
                    object.image = info.variant;
                    auto uid = RESOLVE(MapStateHolder)->addObject(object);
                }
                return;
            }
        }
        replaceMountains(m_brushSize, true);
        fillMountains(m_brushSize);

        return;
    }
    if (m_brushType == Trees)
    {
        if (m_brushIndex ==  0)//random tree
        {

            replaceTerrain(false, 0, false,
                           true, -1);
        }
        else
        {
            const auto tree = m_treesModel->info(m_brushIndex);
            replaceTerrain(true, m_treesModel->intByShotRace(tree.shortRace), false,
                           true, tree.variant);
        }
        return;
    }
    if (m_brushType == Road)
    {
        MapGrid grid = RESOLVE(MapStateHolder)->getGrid();

        grid.cells[m_lastPos.x()][m_lastPos.y()].roadType = 0;
        grid.cells[m_lastPos.x()][m_lastPos.y()].roadVar = 0;
        updateRoad(m_lastPos.x(), m_lastPos.y(), grid);
        updateRoad(m_lastPos.x() + 1, m_lastPos.y(), grid);
        updateRoad(m_lastPos.x() - 1, m_lastPos.y(), grid);
        updateRoad(m_lastPos.x(), m_lastPos.y() + 1, grid);
        updateRoad(m_lastPos.x(), m_lastPos.y() - 1, grid);

        qDebug()<<"#"<<m_lastPos.x()<<m_lastPos.y()<<" " <<grid.cells[m_lastPos.x()][m_lastPos.y()].roadType;

        grid.cells[m_lastPos.x()][m_lastPos.y()].value =
                tileTerrain(grid.cells[m_lastPos.x()][m_lastPos.y()].value);
        RESOLVE(MapStateHolder)->setGrid(grid);
        return;
    }
}


void MapEditTool::updateTargetPolygon()
{
    int size = m_brushSize;
    if (m_brushType == BrushType::Road)
    {
        size = 1;
    }
    if (m_brushType == BrushType::Mountains && brushIndex() != 0)
    {
        auto info = m_mountainsModel->info(m_brushIndex);
        if (info.size != 1)
            size = info.size;
    }
    m_targetPolygon.clear();
    m_targetPolygon<<QPointF(0, 0);
    m_targetPolygon<< cartesianToIsometric(TILE_SIZE * size, 0);
    m_targetPolygon<< cartesianToIsometric(TILE_SIZE * size, TILE_SIZE * size);
    m_targetPolygon<< cartesianToIsometric(0, TILE_SIZE * size);
}

bool MapEditTool::replaceMountains(int brushSize, bool remove)
{
    MapGrid grid = RESOLVE(MapStateHolder)->getGrid();
    int startX = m_lastPos.x() - brushSize / 2;
    int startY = m_lastPos.y() - brushSize / 2;
    bool cantReplace = false;
    QList<QPair<int, int>> objectsToRemove;
    for (int i = 0; i < brushSize; ++i)
    {
        for (int k = 0; k < brushSize; ++k)
        {
            int curX = startX + i, curY = startY + k;
            if (curX < 0 || curY < 0)
                continue;
            if (curY > grid.cells.count() - 1)
                continue;
            if (curX > grid.cells.count() - 1)
                continue;
//            if (grid.cells[curX][curY].value != 37)
//                continue;
            auto & objects = grid.objBinging.binding[curX][curY].items;
            for(int q = 0; q < objects.count(); ++q)
            {
                if (objects[q].first == (int)MapObject::Mountain)
                {
                    objectsToRemove << objects[q];
                    continue;
                }
                cantReplace = true;
                if (!remove)
                    break;
            }
            if (cantReplace && !remove)
                break;
        }
        if (cantReplace && !remove)
            break;
    }
    if (cantReplace && !remove)
        return false;
    for(auto id: qAsConst(objectsToRemove))
    {
        RESOLVE(MapStateHolder)->removeObject(id);
    }
    return true;
}

void MapEditTool::fillMountains(int brushSize)
{
    MapGrid grid = RESOLVE(MapStateHolder)->getGrid();
    int startX = m_lastPos.x() - brushSize / 2;
    int startY = m_lastPos.y() - brushSize / 2;
    for (int i = 0; i < brushSize; ++i)
    {
        for (int k = 0; k < brushSize; ++k)
        {
            int curX = startX + i, curY = startY + k;
            if (curX < 0 || curY < 0)
                continue;
            if (curY > grid.cells.count() - 1)
                continue;
            if (curX > grid.cells.count() - 1)
                continue;
            if (grid.cells[curX][curY].value == 29)
                continue;
            MountainInfo info = m_mountainsModel->info(m_brushIndex);
            MountainObject object;
            object.x = curX;
            object.y = curY;
            object.w = info.size;
            object.h = info.size;
            object.race = 0;
            object.image = info.variant;
            auto uid = RESOLVE(MapStateHolder)->addObject(object);
        }
    }
}

void MapEditTool::onHoverChanged(const QVariant &event)
{
    HoverChangedEvent hoverEvent = event.value<HoverChangedEvent>();
    m_lastPos = hoverEvent.pos;

    if (m_mousePressed)
    {
       applyBrush();
    }
    int size = m_brushSize;
    if (m_brushType == BrushType::Road)
    {
        size = 1;
    }
    if (m_brushType == BrushType::Mountains && brushIndex() != 0)
    {
        auto info = m_mountainsModel->info(m_brushIndex);
        if (info.size != 1)
            size = info.size;
    }
    QPointF pos = cartesianToIsometric((hoverEvent.pos.x() - size / 2) * TILE_SIZE,
                                       (hoverEvent.pos.y() - size / 2) *  TILE_SIZE);
    m_displayPolygon = m_targetPolygon;
    m_displayPolygon.translate(pos.x() + RESOLVE(MapStateHolder)->mapSize() * TILE_SIZE, pos.y());
}

void MapEditTool::replaceTerrain(bool replaceGround, int32_t newValue, bool fillWater,
                                 bool replaceForest, int32_t newForest)
{
    MapGrid grid = RESOLVE(MapStateHolder)->getGrid();
    int startX = m_lastPos.x() - m_brushSize / 2;
    int startY = m_lastPos.y() - m_brushSize / 2;
    for (int i = 0; i < m_brushSize; ++i)
    {
        for (int k = 0; k < m_brushSize; ++k)
        {
            int curX = startX + i, curY = startY + k;
            if (curX < 0 || curY < 0)
                continue;
            if (curY > grid.cells.count() - 1)
                continue;
            if (curX > grid.cells.count() - 1)
                continue;
            if (replaceGround)
            {
                if (grid.cells[curX][curY].value == 37)
                    continue;
                if (grid.cells[curX][curY].value == 29)
                {
                    if (fillWater)
                    {
                        grid.cells[curX][curY].value = newValue;
                    }
                    else
                        continue;
                }
                if (newValue == 29)
                {
                    grid.cells[curX][curY].value = newValue;
                    grid.cells[curX][curY].roadType = -1;
                    grid.cells[curX][curY].roadVar = -1;
                    continue;
                }
                int forestValue = tileForestImage(grid.cells[curX][curY].value);
                if (forestValue != 0)
                {
                    const auto & available = m_treesModel->availableForTile(tileTerrain(newValue));
                    if (!available.contains(forestValue))
                    {
                        int index = 0;
                        if (available.count() > 1) {
                            index = QRandomGenerator::global()->bounded(0, available.count() - 1);
                        }
                        forestValue = available.values()[index];
                    }
                    grid.cells[curX][curY].value = (1 << 3) | (forestValue << 26) | newValue;
                }
                else
                    grid.cells[curX][curY].value = newValue;
            }
            if (replaceForest)
            {
                if (grid.cells[curX][curY].value == 37)
                    continue;
                if (!fillWater && grid.cells[curX][curY].value == 29)
                    continue;
                if (newForest == 0)
                {
                    int terrain =  tileTerrain(grid.cells[curX][curY].value);
                    grid.cells[curX][curY].value = terrain;
                    continue;
                }
                int forestValue = newForest;
                if (newForest == -1)
                {
                    const auto & available = m_treesModel->availableForTile(tileTerrain(grid.cells[curX][curY].value));
                    int index = 0;
                    if (available.count() > 1) {
                        index = QRandomGenerator::global()->bounded(0, available.count() - 1);
                    }
                    forestValue = available.values()[index];
                }
                int terrain =  tileTerrain(grid.cells[curX][curY].value);
                grid.cells[curX][curY].value = (1 << 3) | (forestValue << 26) | terrain;//replaceLeftmost6Bits(grid.cells[curX][curY].value, forestValue);
                //grid.cells[curX][curY].value = replaceSecondLeftmost6Bits(grid.cells[curX][curY].value, forestValue);
            }
        }
    }
    RESOLVE(MapStateHolder)->setGrid(grid);
}


void MapEditTool::activate()
{
    subscribe<HoverChangedEvent>([this](const QVariant &event){onHoverChanged(event);});
    m_terrainModel->init<Lterrain>(
        [](const QSharedPointer<GameData>& item)
        {
            Lterrain * terr = static_cast<Lterrain*>(item.data());
            return QString::number(terr->id);
        },
        [](const QSharedPointer<GameData>& item)
        {
            Lterrain * terr = static_cast<Lterrain*>(item.data());
            QString key = terr->text;
            key = key.replace("L_", "").left(2);
            key = "ISO" + key;
            return key;
        });
    m_mountainsModel->init();
    m_treesModel->init();
    RESOLVE(SettingsManager)->setValue("enable_map_drag", false);
}

void MapEditTool::deactivate()
{
    RESOLVE(SettingsManager)->setValue("enable_map_drag", true);
    unsub();
}

QObject * MapEditTool::toQObject()
{
    return this;
}

QString  MapEditTool::editorFile() const
{
    return "Tool_MapEdit.qml";
}

void MapEditTool::draw(QPainter *painter)
{
    painter->setPen(QPen(Qt::blue, 2));
    painter->drawPolygon(m_displayPolygon);
}

QRect MapEditTool::toolRect() const
{
    return QRect();
}

bool MapEditTool::neadUpdate() const
{
    return true;
}

void MapEditTool::mousePressed(Qt::MouseButton button)
{
    if (button == Qt::MiddleButton || button == Qt::LeftButton)
        return;
    m_mousePressed = true;
    applyBrush();
}

void MapEditTool::mouseReleased()
{
    m_mousePressed = false;
}

SelectGameObjectModel *MapEditTool::terrainModel() const
{
    return m_terrainModel;
}

int MapEditTool::brushSize() const
{
    return m_brushSize;
}

void MapEditTool::setBrushSize(int newBrushSize)
{
    if (m_brushSize == newBrushSize)
        return;
    m_brushSize = newBrushSize;
    updateTargetPolygon();
    emit changed();
}

int MapEditTool::brushIndex() const
{
    return m_brushIndex;
}

void MapEditTool::setBrushIndex(int newBrushIndex)
{
    if (m_brushIndex == newBrushIndex)
        return;
    m_brushIndex = newBrushIndex;
    updateTargetPolygon();
    emit changed();
}

MountainSelectModel *MapEditTool::mountainsModel() const
{
    return m_mountainsModel;
}

TreeSelectModel *MapEditTool::treesModel() const
{
    return m_treesModel;
}

int MapEditTool::brushType() const
{
    return m_brushType;
}

void MapEditTool::setBrushType(int newBrushType)
{
    if (m_brushType == newBrushType)
        return;
    m_brushType = (BrushType)newBrushType;
    updateTargetPolygon();
    emit changed();
}

#include "PlaceObjectTool.h"
#include "MapObjects/MapObject.h"
#include "Common/MathHelper.h"
#include "../../Events/MapEvents.h"
#include "Engine/Map/MapStateHolder.h"
#include "Engine/GameInstance.h"

PlaceObjectTool::PlaceObjectTool(const QSize &objSize, QObject *parent) : QObject(parent), m_size(objSize)
{
    updateTargetPolygon();
    m_displayPolygon = m_targetPolygon;
}

void PlaceObjectTool::draw(QPainter *painter)
{
    if (m_size.width() == 0)
        return;
    int dx =  - m_preview.width();
    int dy =  - m_preview.height() + TILE_SIZE * m_size.height();
    QRect rect (dx/2, dy/2, m_preview.width(), m_preview.height());
    rect.translate(m_displayPolygon.boundingRect().center().x(), m_displayPolygon.boundingRect().top());

    if (m_canBePlaced)
        painter->setPen(QPen(Qt::blue, 2));
    else
        painter->setPen(QPen(Qt::red, 2));
    painter->drawPolygon(m_displayPolygon);
    //rect.translate(0, TILE_SIZE / 2);
    painter->drawImage( rect,
                        m_preview,
                        m_preview.rect());
}

void PlaceObjectTool::onHoverChanged(const QVariant &event)
{
    HoverChangedEvent hoverEvent = event.value<HoverChangedEvent>();
    QPointF pos = cartesianToIsometric(hoverEvent.pos.x() * TILE_SIZE, hoverEvent.pos.y() *  TILE_SIZE);
    m_displayPolygon = m_targetPolygon;
    m_displayPolygon.translate(pos.x() + RESOLVE(MapStateHolder)->mapSize() * TILE_SIZE, pos.y());
    m_lastPos = hoverEvent.pos;
    MapObjBinding binding = RESOLVE(MapStateHolder)->getGrid().objBinging;
    int mapSize = RESOLVE(MapStateHolder)->mapSize();
    m_canBePlaced = m_size.width() > 0;
    if (m_canBePlaced)
    {
        for (int i = 0; i < m_size.width(); ++i)
        {
            for (int k = 0; k < m_size.height(); ++k)
            {
                int x = hoverEvent.pos.x();
                int y = hoverEvent.pos.y();
                if (x + i >= mapSize || y + k >= mapSize)
                {
                    m_canBePlaced = false;
                    return;
                }
                if (x + i < 0 || y + k < 0)
                {
                    m_canBePlaced = false;
                    return;
                }
                if (binding.binding[x + i][y + k].items.count() > 0)
                {
                    m_canBePlaced = false;
                    return;
                }
            }
        }
        m_canBePlaced = true;
    }
}

void PlaceObjectTool::activate()
{
    subscribe<HoverChangedEvent>([this](const QVariant &event){onHoverChanged(event);});
}

void PlaceObjectTool::deactivate()
{
    unsub();
}

QRect PlaceObjectTool::toolRect() const
{
    return QRect();
}

void PlaceObjectTool::mousePressed(Qt::MouseButton button)
{
    if (button == Qt::MiddleButton)
        return;
    if (button == Qt::LeftButton)
        return;
    if (!m_canBePlaced)
        return;
    placeObject();
}

bool PlaceObjectTool::neadUpdate() const
{
    return true;
}

void PlaceObjectTool::updateTargetPolygon()
{
    m_targetPolygon.clear();
    m_targetPolygon<<QPointF(0, 0);
    m_targetPolygon<< cartesianToIsometric(TILE_SIZE * m_size.width(), 0);
    m_targetPolygon<< cartesianToIsometric(TILE_SIZE * m_size.width(), TILE_SIZE * m_size.height());
    m_targetPolygon<< cartesianToIsometric(0, TILE_SIZE * m_size.height());

}

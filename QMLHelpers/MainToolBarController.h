#ifndef MAINTOOLBARCONTROLLER_H
#define MAINTOOLBARCONTROLLER_H

#include <QObject>
#include "QMLHelpers/Tools/ToolsProvider.h"

class MainToolBarController : public QObject, public EventSubscriber
{
    Q_OBJECT
public:
    enum Mode
    {
        Default = 0,
        Delete,
        Meashure
    };
    explicit MainToolBarController(QObject *parent = nullptr);
    Q_PROPERTY(ToolsProvider* tools READ tools NOTIFY toolsChanged)
    ToolsProvider *tools() const;
    Q_PROPERTY(QString mapImage READ getMapImage NOTIFY selectedMapChanged)
    Q_PROPERTY(QPointF mapPosition READ getMapPosition NOTIFY mapPositionChanged)

    QString getMapImage() const;
    void onMapLoaded(const QVariant& event);
    void onHoverChanged(const QVariant &event);
    QPointF getMapPosition() const;

public slots:
    void minimapClicked(float x, float y);
signals:
    void toolsChanged();
    void selectedMapChanged();
    void mapPositionChanged();

public slots:
    void save();
    void open();
    void exportMap();
    void settings();
    void exit();
    void requestDataView();
    void openQuestLineEditor();
    void openMapStatistic();
    void openMapInfoEditor();

    void setMode(int mode);
private:
    ToolsProvider *m_provider;
    ToolsProvider *m_tools = nullptr;
    QString m_mapImage;
    QPointF m_mapPosition;
};

#endif // MAINTOOLBARCONTROLLER_H

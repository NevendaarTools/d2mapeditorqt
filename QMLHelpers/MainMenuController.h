#ifndef MAINMENUCONTROLLER_H
#define MAINMENUCONTROLLER_H

#include <QObject>
#include "Events/EventBus.h"
#include "QMLHelpers/Models/StringModel.h"

class MainMenuController : public QObject, EventSubscriber
{
    Q_OBJECT
public:
    explicit MainMenuController(QObject *parent = nullptr);
    Q_PROPERTY(QString gameName READ getGameName NOTIFY gamePathChanged)
    Q_PROPERTY(QString gamePath READ getGamePath NOTIFY gamePathChanged)
    Q_PROPERTY(QString editorVersion READ editorVersion NOTIFY gamePathChanged)
    Q_PROPERTY(StringModel * mapListModel READ getMapListModel NOTIFY gamePathChanged)
    Q_PROPERTY(StringModel * importListModel READ getImportListModel NOTIFY gamePathChanged)

    Q_PROPERTY(QString mapName READ getMapName NOTIFY selectedMapChanged)
    Q_PROPERTY(QString mapInfo READ getMapInfo NOTIFY selectedMapChanged)
    Q_PROPERTY(QString mapDesc READ getMapDesc NOTIFY selectedMapChanged)
    Q_PROPERTY(QString mapImage READ getMapImage NOTIFY selectedMapChanged)

    Q_INVOKABLE QString tr(const QString& str);
    void onGameLoaded(const QVariant &event);
    void onMapCreated(const QVariant &event);

    Q_INVOKABLE void setSelectedMap(int newSelectedGenerator, bool importMap);
    Q_INVOKABLE void openMap(int index, bool importMap);
    Q_INVOKABLE void openMapFromFile();
    Q_INVOKABLE void selectGamePath();
    Q_INVOKABLE void exit();
    Q_INVOKABLE void mapTypeChange();
    Q_INVOKABLE QString about();
    Q_INVOKABLE bool hasImportedMap(int index);

    const QString &getGameName() const;
    const QString &getGamePath() const;
    StringModel *getMapListModel() const;
    const QString &getMapName() const;
    const QString &getMapDesc() const;
    const QString &getMapImage() const;
    const QString &editorVersion() const;

    QString getMapInfo() const;

    StringModel *getImportListModel() const;
public slots:
    void requestDataView();
signals:
    void gamePathChanged();
    void generatorsListModelChanged();
    void selectedGeneratorChanged();
    void selectedMapChanged();
private:
    bool importMap(const QString &filePath);
private:
    QString m_gameName;
    QString m_gamePath;
    StringModel *m_mapListModel;
    QString m_mapName;
    QString m_mapDesc;
    QString m_mapImage;
    QString m_mapInfo;
    StringModel *m_importListModel = nullptr;
};

#endif // MAINMENUCONTROLLER_H

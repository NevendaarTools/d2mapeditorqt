#include "TreasureEditor.h"
#include "Engine/Map/MapStateHolder.h"
#include "Engine/GameInstance.h"

TreasureEditor::TreasureEditor(QObject *parent) : QObject(parent)
{
    m_items = new InventoryModel(this);
    m_maxImage = 99;
    m_object.uid = EMPTY_ID;
    connect(m_items, SIGNAL(changed()), SLOT(save()));
    connect(this, SIGNAL(dataChanged()), this, SLOT(save()));
}

void TreasureEditor::setUID(const QString &uid)
{
    QStringList data = uid.split(":");
    if (data.count() < 2)
        return;
    setUID(QPair<int,int>(data[0].toInt(), data[1].toInt()));
}

void TreasureEditor::setUID(QPair<int, int> uid)
{
    m_object = RESOLVE(MapStateHolder)->objectByIdT<TreasureObject>(uid);
    setTreasure(m_object);
}

void TreasureEditor::setup(QQuickWidget *editor)
{
    QQmlContext* context = editor->rootContext();
    context->setContextProperty("treasure", this->toObject());
    editor->setSource(QUrl::fromLocalFile("QML/edit_Treasure.qml"));
}

void TreasureEditor::setTreasure(TreasureObject stack)
{
    m_initState = true;
    m_object = stack;
    m_items->setInventory(m_object.inventory);
    emit dataChanged();
    m_initState = false;
}

InventoryModel *TreasureEditor::items() const
{
    return m_items;
}

int TreasureEditor::maxImage() const
{
    return m_maxImage;
}

int TreasureEditor::priority() const
{
    return m_object.AIpriority;
}

void TreasureEditor::setPriority(int newPriority)
{
    if (m_object.AIpriority == newPriority)
        return;
    m_object.AIpriority = newPriority;
    emit dataChanged();
}

QString TreasureEditor::image() const
{
    return QString::number(m_object.image).rightJustified(2,'0');;
}

void TreasureEditor::setImage(const QString &newImage)
{
    if (m_object.image == newImage.toInt())
        return;
    m_object.image = newImage.toInt();
    emit dataChanged();
}

bool TreasureEditor::water() const
{
    if (m_object.uid == EMPTY_ID)
        return false;
    if (RESOLVE(MapStateHolder)->getGrid().cells[m_object.x][m_object.y].isWater())
        return true;
    return false;
}

void TreasureEditor::save()
{
    if (m_object.uid == EMPTY_ID || m_initState)
        return;
    m_object.inventory = m_items->inventory();
    RESOLVE(MapStateHolder)->replaceObject(QSharedPointer<MapObject>(new TreasureObject(m_object)));
}

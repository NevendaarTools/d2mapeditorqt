#ifndef QUESTLINEEDITOR_H
#define QUESTLINEEDITOR_H
#include <QObject>
#include "EditorWidget.h"
#include "QMLHelpers/Models/StringModel.h"
#include "QMLHelpers/Models/SelectMapObjectModel.h"
#include "MapObjects/QuestLine.h"

class QuestLineEditor : public QObject, public IObjectEditor
{
    Q_OBJECT
public:
    explicit QuestLineEditor(QObject *parent = nullptr);
    Q_PROPERTY(SelectMapObjectModel *  baseQuests READ baseQuests NOTIFY dataChanged)
    Q_PROPERTY(SelectMapObjectModel *  utilQuests READ utilQuests NOTIFY dataChanged)

    Q_PROPERTY(QString selectedId READ selectedId WRITE setSelectedId NOTIFY dataChanged)
    Q_PROPERTY(int stageCount READ stageCount NOTIFY dataChanged)
    Q_PROPERTY(bool stageToInfo READ stageToInfo WRITE setStageToInfo NOTIFY dataChanged)
    Q_PROPERTY(QString questLineName READ questLineName WRITE setQuestLineName NOTIFY dataChanged)

    virtual void setUID(QPair<int, int> uid) override;
    virtual void setup(QQuickWidget *editor) override;
    void setQuestLine(QuestLine line);
    virtual QObject *toObject() override {return this;}
    QString validate() override{return QString();}

    SelectMapObjectModel *baseQuests() const;

    SelectMapObjectModel *utilQuests() const;

    const QString selectedId() const;

    void setSelectedId(const QString &newSelectedId);

    SelectMapObjectModel *stageQuests() const;

    int maxStage() const;

    const QString &stageName() const;
    void setStageName(const QString &newStageName);

    const QString &stageDesc() const;
    void setStageDesc(const QString &newStageDesc);

    bool stageToInfo() const;
    void setStageToInfo(bool newStageToInfo);
    Q_INVOKABLE void addStage(const QString& name);
    Q_INVOKABLE QString addQuestLine(const QString& name);
    int stageCount() const;

    QString questLineName() const;
    void setQuestLineName(QString newQuestLineName);
public slots:
    void save();
signals:
    void dataChanged();
private:
    void reloadModels();
private:

    SelectMapObjectModel *m_baseQuests;
    SelectMapObjectModel *m_utilQuests;
    QuestLine m_line;
    QPair<int, int> m_selectedUid;
    bool m_initState = false;
};

#endif // QUESTLINEEDITOR_H

#include "EditorWidget.h"
#include "Events/MapEvents.h"
#include "MapObjects/CrystalObject.h"
#include "Engine/Components/AccessorHolder.h"
#include "Engine/GameInstance.h"
#include "Engine/Map/MapStateHolder.h"
#include "Events/EventBus.h"

EditorWidget::EditorWidget(QWidget *view, QObject *parent)
    : QObject(parent),
        m_graphicsView(view),
        m_editorWidget(nullptr),
        m_imageProvider(new ImageProvider())
{
    m_overlayWrapper = new OverlayWrapper(this);
    registerExecutor<EditCommand>
            ([this](const QVariant &command){return executeEditCommand(command);});
    registerExecutor<ChangeCrystalCommand>
            ([this](const QVariant &command){return executeChangeCrystalCommand(command);});
}

CommandResult EditorWidget::executeEditCommand(const QVariant &commandVar)
{
    qDebug()<<Q_FUNC_INFO;
    EditCommand command = commandVar.value<EditCommand>();
    if (!m_editors.contains(command.uid.first))
    {
        QSharedPointer<IMapObjectAccessor> accessor =
                RESOLVE(AccessorHolder)->objectAccessor(command.uid.first);
        IObjectEditor * editor = accessor->createEditor();
        if (editor == nullptr)
            return CommandResult::fail("Failed to find editor for object");
        m_editors.insert(command.uid.first, QSharedPointer<IObjectEditor>(editor));
    }
    // if (!RESOLVE(MapStateHolder)->hasObject(command.uid))
    // {
    //     qDebug()<<"Failed to get object by uid = "<<command.uid;
    //     return CommandResult::fail("Failed to get object by uid");
    // }
    HoveredObjectChangedEvent hoverEvent;
    hoverEvent.hover_in = EMPTY_ID;
    RESOLVE(EventBus)->notify(hoverEvent);

    if (m_editorWidget)
        m_editorWidget->deleteLater();
    m_editorWidget = new QQuickWidget(m_graphicsView);
    m_editorWidget->setAttribute(Qt::WA_DeleteOnClose);
    m_editorWidget->setAttribute(Qt::WA_TranslucentBackground, true);
    m_editorWidget->setAttribute(Qt::WA_AlwaysStackOnTop, true);
    m_editorWidget->setAttribute(Qt::WA_TransparentForMouseEvents, false);
    m_editorWidget->setClearColor(Qt::transparent);
    m_editorWidget->engine()->addImageProvider(QLatin1String("provider"), new ImageProvider());
    m_editorWidget->rootContext()->setContextProperty("editor", this);
    m_editorWidget->rootContext()->setContextProperty("overlay", m_overlayWrapper);
    m_editorWidget->rootContext()->setContextProperty("translate", m_overlayWrapper);
    m_currentEditor = m_editors.value(command.uid.first);
    m_currentEditor->setup(m_editorWidget);
    qDebug()<<"###status = "<< m_editorWidget->status();
    if (m_editorWidget->status() == QQuickWidget::Error) {
        auto errors = m_editorWidget->errors();
        for (const auto &error : errors) {
            qDebug() << "QML Load Error:" << error.toString();
        }
    }
    if (auto engine = m_editorWidget->engine()) {
        connect(engine, &QQmlEngine::warnings, this, [](const QList<QQmlError> &warnings) {
            for (const auto &warning : warnings) {
                qDebug() << "QML Warning:" << warning.toString();
            }
        });
    }

    m_currentEditor->setUID(command.uid);
    RESOLVE(MapStateHolder)->startEdit();

    m_editorWidget->setResizeMode(QQuickWidget::SizeRootObjectToView);
    m_editorWidget->show();
    m_editorWidget->setGeometry(0,0, m_graphicsView->width(), m_graphicsView->height());
    m_editorWidget->setFocus();
    m_overlayWrapper->hideMapOverlay();
    return CommandResult::success();
}

CommandResult EditorWidget::executeChangeCrystalCommand(const QVariant &commandVar)
{
    ChangeCrystalCommand command = commandVar.value<ChangeCrystalCommand>();
    QSharedPointer<MapObject> object = RESOLVE(MapStateHolder)->objectById(command.uid);
    CrystalObject * obj = static_cast<CrystalObject *>(object.data());
    obj->resource = (CrystalObject::ResourceType)command.type;
    RESOLVE(MapStateHolder)->replaceObject(object);
    return CommandResult::success();
}

bool EditorWidget::hasChanges() const
{
    return RESOLVE(MapStateHolder)->hasChanges();
}

QString EditorWidget::validate() const
{
    if (m_currentEditor.isNull())
        return QString();
    return m_currentEditor->validate();
}

void EditorWidget::commit()
{
    m_editorWidget->close();
    m_editorWidget = nullptr;
    m_currentEditor = nullptr;
    RESOLVE(MapStateHolder)->acceptEdit();
    m_overlayWrapper->showMapOverlay();
}

void EditorWidget::cancel()
{
    m_editorWidget->close();
    m_editorWidget = nullptr;
    m_currentEditor = nullptr;
    RESOLVE(MapStateHolder)->rejectEdit();
    m_overlayWrapper->showMapOverlay();
}

#include "QuestLineEditor.h"
#include <QStringList>

QuestLineEditor::QuestLineEditor(QObject *parent) : QObject(parent)
{
    m_baseQuests = new SelectMapObjectModel(this);
    m_utilQuests = new SelectMapObjectModel(this);
    m_selectedUid = EMPTY_ID;
}

void QuestLineEditor::setUID(QPair<int, int> uid)
{
    m_line = RESOLVE(MapStateHolder)->objectByIdT<QuestLine>(m_selectedUid);
    setQuestLine(m_line);
}

void QuestLineEditor::setup(QQuickWidget *editor)
{

    QQmlContext* context = editor->rootContext();
    context->setContextProperty("quest", this->toObject());
    editor->setSource(QUrl::fromLocalFile("QML/edit_QuestLines.qml"));
}

void QuestLineEditor::setQuestLine(QuestLine line)
{
    m_initState = true;
    m_line = line;
    m_selectedUid = line.uid;
    reloadModels();
    emit dataChanged();
    m_initState = false;
}

SelectMapObjectModel *QuestLineEditor::baseQuests() const
{
    return m_baseQuests;
}

SelectMapObjectModel *QuestLineEditor::utilQuests() const
{
    return m_utilQuests;
}

const QString QuestLineEditor::selectedId() const
{
    return QString("%1:%2").arg(m_selectedUid.first).arg(m_selectedUid.second);
}

void QuestLineEditor::setSelectedId(const QString &newSelectedId)
{
    QStringList data = newSelectedId.split(":");
    if (data.count() < 2)
        return;
    m_selectedUid = IdFromString(newSelectedId);
    QuestLine line = RESOLVE(MapStateHolder)->objectByIdT<QuestLine>(m_selectedUid);
    setQuestLine(line);
}

bool QuestLineEditor::stageToInfo() const
{
    return m_line.display;
}

void QuestLineEditor::setStageToInfo(bool newStageToInfo)
{
    if (m_line.display == newStageToInfo)
        return;
    m_line.display = newStageToInfo;
    save();
    emit dataChanged();
}

void QuestLineEditor::addStage(const QString &name)
{
    m_line.stages << QuestLine::QuestStage{name};
    save();
    emit dataChanged();
}

QString QuestLineEditor::addQuestLine(const QString &name)
{
    QuestLine line;
    line.name = name;
    auto id = RESOLVE(MapStateHolder)->addObject<>(line);
    save();
    reloadModels();
    emit dataChanged();
    return IdToString(id);
}

void QuestLineEditor::reloadModels()
{
    auto getText = [](const QSharedPointer<MapObject>& item)
    {
        QuestLine * quest = static_cast<QuestLine*>(item.data());
        return quest->name;
    };
    auto getId = [](const QSharedPointer<MapObject>& item)
    {
        QuestLine * quest = static_cast<QuestLine*>(item.data());
        return quest->uid;
    };

    m_baseQuests->init<QuestLine>(
        getText, getId,
    [](const QSharedPointer<MapObject>& item)
    {
        QuestLine * quest = static_cast<QuestLine*>(item.data());
        return !quest->utility;
    });

    m_utilQuests->init<QuestLine>(
        getText, getId,
    [](const QSharedPointer<MapObject>& item)
    {
        QuestLine * quest = static_cast<QuestLine*>(item.data());
        return quest->utility;
    });
}

int QuestLineEditor::stageCount() const
{
    return m_line.stages.count();
}

QString QuestLineEditor::questLineName() const
{
    return m_line.name;
}

void QuestLineEditor::setQuestLineName(QString newQuestLineName)
{
    if (m_line.name == newQuestLineName)
        return;
    m_line.name = newQuestLineName;

    save();
    reloadModels();
    emit dataChanged();
}

void QuestLineEditor::save()
{
    if (m_line.uid == EMPTY_ID || m_initState)
        return;
    RESOLVE(MapStateHolder)->replaceObject(m_line);
}

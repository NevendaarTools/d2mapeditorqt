#include "VillageEditor.h"
#include <QDebug>
#include "Engine/Map/MapStateHolder.h"
#include "Engine/GameInstance.h"

FortEditor::FortEditor(FortObject village, QObject *parent)
    : QObject(parent), m_fort(village)
{
    m_garrison = new GroupUnitsEditor(this);
    m_visiter = new GroupUnitsEditor(this);

    m_visiterInventory = new InventoryModel(this);
    m_garrisonInventory = new InventoryModel(this);
    m_ownerModel = new OwnerSelectModel(this);

    connect(m_garrison, SIGNAL(unitsChanged()), this, SIGNAL(changed()));
    connect(m_visiter, SIGNAL(unitsChanged()), this, SIGNAL(changed()));

    connect(m_visiter, SIGNAL(dragFinished()), this, SLOT(onUnitDrag()));
    connect(m_garrison, SIGNAL(dragFinished()), this, SLOT(onUnitDrag()));

    connect(m_garrisonInventory, SIGNAL(changed()), SLOT(save()));
    connect(m_visiterInventory, SIGNAL(changed()), SLOT(save()));
    connect(m_ownerModel, SIGNAL(changed()), SLOT(save()));
    connect(this, SIGNAL(changed()), this, SLOT(save()));
}

void FortEditor::setUID(const QString &uid)
{
    QStringList data = uid.split(":");
    if (data.count() < 2)
        return;
    setUID(QPair<int,int>(data[0].toInt(), data[1].toInt()));
}

void FortEditor::setup(QQuickWidget *editor)
{
    QQmlContext* context = editor->rootContext();
    context->setContextProperty("village", this->toObject());
    editor->setSource(QUrl::fromLocalFile("QML/edit_Village.qml"));
}

void FortEditor::setUID(QPair<int, int> uid)
{
    m_fort = RESOLVE(MapStateHolder)->objectByIdT<FortObject>(uid);
    setVillage(m_fort);
}

void FortEditor::setVillage(FortObject village)
{
    m_initState = true;
    m_fort = village;
    m_garrison->setGroup(village.garrison);
    m_visiterObject.stack.name = "";
    if (m_fort.visiter.second != -1)
    {
        m_visiterObject = RESOLVE(MapStateHolder)->objectByIdT<StackObject>(m_fort.visiter);
    }
    m_visiter->setGroup(m_visiterObject.stack);
    m_visiterInventory->setInventory(m_visiterObject.stack.inventory);

    m_garrisonInventory->setInventory(village.garrison.inventory);
    m_ownerModel->init();
    m_ownerModel->setOwnerIndexByUid(m_fort.subrace);
    updateImage();

    emit changed();
    m_initState = false;
}

int FortEditor::level() const
{
    return m_fort.level;
}

void FortEditor::setLevel(int level)
{
    if (m_fort.level == level)
        return;

    m_fort.level = level;
    updateImage();
    emit changed();
    save();
}

void FortEditor::onUnitDrag()
{
    GroupUnitsEditor * source = nullptr;
    GroupUnitsEditor * target = nullptr;
    if (m_visiter->isDragSource())
        source = m_visiter;
    if (m_visiter->isDragTarget())
        target = m_visiter;
    if (m_garrison->isDragSource())
        source = m_garrison;
    if (m_garrison->isDragTarget())
        target = m_garrison;
    if (source != nullptr &&  target != nullptr)
    {
        source->moveUnit(source->dragSource(), target, target->dragTarget());
    }
    m_visiter->resetDrag();
    m_garrison->resetDrag();
    emit changed();
    save();
}

void FortEditor::updateImage()
{
    m_image = "IsoAnim-IsoCmon-G000FT0000NE" + QString::number(m_fort.level);
}

void FortEditor::save()
{
    if (m_fort.uid == EMPTY_ID || m_initState)
        return;
    m_fort.garrison = m_garrison->group();
    m_fort.garrison.inventory = m_garrisonInventory->inventory();
    m_fort.subrace = m_ownerModel->currentSubrace();
    m_fort.owner = m_ownerModel->currentOwner();
    RESOLVE(MapStateHolder)->replaceObject(QSharedPointer<MapObject>(new FortObject(m_fort)));
    if (m_fort.visiter.second != -1)
    {
        auto visiter = m_visiter->group();
        visiter.inventory = m_visiterInventory->inventory();
        auto visiterObj = RESOLVE(MapStateHolder)->objectByIdT<StackObject>(m_fort.visiter);
        visiterObj.stack = visiter;
        RESOLVE(MapStateHolder)->replaceObject(QSharedPointer<MapObject>(new StackObject(visiterObj)));
    }
}

GroupUnitsEditor *FortEditor::garrison() const
{
    return m_garrison;
}

GroupUnitsEditor *FortEditor::visiter() const
{
    return m_visiter;
}

InventoryModel *FortEditor::garrisonInventory() const
{
    return m_garrisonInventory;
}

InventoryModel *FortEditor::visiterInventory() const
{
    return m_visiterInventory;
}

const QString &FortEditor::name() const
{
    return m_fort.name;
}

void FortEditor::setName(const QString &newName)
{
    if (m_fort.name == newName)
        return;
    m_fort.name = newName;
    emit changed();
    save();
}

OwnerSelectModel *FortEditor::ownerModel() const
{
    return m_ownerModel;
}

QString FortEditor::image() const
{
    return m_image;
}

const QString FortEditor::stackName() const
{
    return m_visiterObject.stack.name;
}

void FortEditor::setStackName(const QString &newStackName)
{
    if (m_visiterObject.stack.name == newStackName)
        return;
    RESOLVE(MapStateHolder)->replaceObject(m_visiterObject);
    emit changed();
    save();
}

int FortEditor::fortType() const
{
    return m_fort.fortType;
}

QString FortEditor::uid() const
{
    return IdToString(m_fort.uid);
}

QString FortEditor::stackUid() const
{
    return IdToString(m_visiterObject.uid);
}

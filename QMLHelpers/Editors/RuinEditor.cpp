#include "RuinEditor.h"
#include "Engine/GameInstance.h"
#include "Engine/Map/MapStateHolder.h"

RuinEditor::RuinEditor(QObject *parent) : QObject(parent)
{
    m_units = new GroupUnitsEditor(this);
    connect(m_units, SIGNAL(unitsChanged()), this, SIGNAL(dataChanged()));
    connect(this, SIGNAL(dataChanged()), this, SLOT(save()));
}

void RuinEditor::setup(QQuickWidget *editor)
{
    QQmlContext* context = editor->rootContext();
    context->setContextProperty("ruin", this->toObject());
    editor->setSource(QUrl::fromLocalFile("QML/edit_Ruin.qml"));
}

void RuinEditor::setUID(QPair<int, int> uid)
{
    m_object = RESOLVE(MapStateHolder)->objectByIdT<RuinObject>(uid);
    setRuin(m_object);
}

void RuinEditor::setUID(const QString &uid)
{
    QStringList data = uid.split(":");
    if (data.count() < 2)
        return;
    setUID(QPair<int,int>(data[0].toInt(), data[1].toInt()));
}

void RuinEditor::setRuin(RuinObject object)
{
    m_initState = true;
    m_object = object;
    m_units->setGroup(m_object.guards);
    emit dataChanged();
    m_initState = false;
}

GroupUnitsEditor *RuinEditor::units() const
{
    return m_units;
}

const QString &RuinEditor::item() const
{
    return m_object.item;
}

QString RuinEditor::cost() const
{
    return m_object.reward.toString();
}

int RuinEditor::maxImage() const
{
    return 99;
}

int RuinEditor::priority() const
{
    return m_object.priority;
}

void RuinEditor::setPriority(int newPriority)
{
    if (m_object.priority == newPriority)
        return;
    m_object.priority = newPriority;
    emit dataChanged();
}

QString RuinEditor::image() const
{
    return QString::number(m_object.image).rightJustified(2,'0');
}

void RuinEditor::setImage(const QString &newImage)
{
    if (m_object.image == newImage.toInt())
        return;
    m_object.image = newImage.toInt();
    emit dataChanged();
}

void RuinEditor::setCost(const QString &newCost)
{
    if (newCost.toUpper() == m_object.reward.toString())
        return;
    m_object.reward = Currency::fromString(newCost);
    emit dataChanged();
}

const QString &RuinEditor::name() const
{
    return m_object.name;
}

void RuinEditor::setName(const QString &newName)
{
    if (m_object.name == newName)
        return;
    m_object.name = newName;
    emit dataChanged();
}

const QString &RuinEditor::desc() const
{
    return m_object.desc;
}

void RuinEditor::setDesc(const QString &newDesc)
{
    if (m_object.desc == newDesc)
        return;
    m_object.desc = newDesc;
    emit dataChanged();
}

void RuinEditor::setItem(const QString &newItem)
{
    if (m_object.item == newItem)
        return;
    m_object.item = newItem;
    emit dataChanged();
}

void RuinEditor::save()
{
    if (m_object.uid == EMPTY_ID || m_initState)
        return;
    m_object.guards = m_units->group();
    RESOLVE(MapStateHolder)->replaceObject(QSharedPointer<MapObject>(new RuinObject(m_object)));
}

QString RuinEditor::uid() const
{
    return IdToString(m_object.uid);
}

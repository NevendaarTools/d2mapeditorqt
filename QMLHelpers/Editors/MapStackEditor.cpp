#include "MapStackEditor.h"
#include <QStringList>
#include "Engine/Map/MapStateHolder.h"
#include "Engine/GameInstance.h"

MapStackEditor::MapStackEditor(QObject *parent) : QObject(parent)
{
    m_stack = new GroupUnitsEditor(this);
    m_inventory = new InventoryModel(this);
    m_orderEditor = new StackOrderEditor(this);
    m_ownerModel = new OwnerSelectModel(this);
    m_object.uid = EMPTY_ID;
    connect(m_stack, SIGNAL(unitsChanged()), this, SIGNAL(dataChanged()));
    connect(m_stack, SIGNAL(dragFinished()), this, SLOT(onUnitDrag()));
    connect(m_ownerModel, SIGNAL(changed()), SLOT(save()));
    connect(m_orderEditor, SIGNAL(targetChanged()), SLOT(save()));
    connect(m_inventory, SIGNAL(changed()), SLOT(save()));
    connect(m_stack, SIGNAL(unitsChanged()), SLOT(save()));
    connect(this, SIGNAL(dataChanged()), this, SLOT(save()));
}

void MapStackEditor::setup(QQuickWidget *editor)
{
    QQmlContext* context = editor->rootContext();
    context->setContextProperty("stack", this->toObject());
    editor->setSource(QUrl::fromLocalFile("QML/edit_Stack.qml"));
    emit dataChanged();
    emit loadFinished();
}

void MapStackEditor::setUID(QPair<int, int> uid)
{
    setStack(RESOLVE(MapStateHolder)->objectByIdT<StackObject>(uid));
}

QString MapStackEditor::validate()
{
    if (!m_stack->hasLeader())
    {
        return "There is no leader in the squad, add him to save";
    }
    return QString();
}

void MapStackEditor::setUID(const QString &uid)
{
    QStringList data = uid.split(":");
    if (data.count() < 2)
        return;
    setUID(QPair<int,int>(data[0].toInt(), data[1].toInt()));
}

void MapStackEditor::setStack(StackObject stack)
{
    m_initState = true;
    m_object = stack;
    m_stack->setGroup(m_object.stack);
    m_inventory->setInventory(m_object.stack.inventory);
    m_orderEditor->setOrder(m_object.order);
    m_orderEditor->setOrderTarget(m_object.orderTarget);
    m_ownerModel->init();
    m_ownerModel->setOwnerIndexByUid(m_object.subrace);
    emit dataChanged();
    emit loadFinished();
    m_initState = false;
}

GroupUnitsEditor *MapStackEditor::stack() const
{
    return m_stack;
}

InventoryModel *MapStackEditor::inventory() const
{
    return m_inventory;
}

int MapStackEditor::rotation() const
{
    return m_object.rotation;
}

void MapStackEditor::setRotation(int newRotation)
{
    if (m_object.rotation == newRotation)
        return;
    m_object.rotation = newRotation;
    save();
    emit dataChanged();
}

void MapStackEditor::onUnitDrag()
{
    m_stack->moveUnit(m_stack->dragSource(), m_stack, m_stack->dragTarget());
    m_stack->resetDrag();
    save();
    emit dataChanged();
}

void MapStackEditor::save()
{
    if (m_object.uid == EMPTY_ID || m_initState)
        return;
    m_object.stack = m_stack->group();
    m_object.stack.inventory = m_inventory->inventory();
    m_object.order = m_orderEditor->order();
    m_object.orderTarget = m_orderEditor->orderTargetNative();
    m_object.subrace = m_ownerModel->currentSubrace();
    RESOLVE(MapStateHolder)->replaceObject(QSharedPointer<MapObject>(new StackObject(m_object)));
}

QString MapStackEditor::name() const
{
    return m_stack->group().name;
}

void MapStackEditor::setName(const QString &newName)
{
    if (m_stack->group().name == newName)
        return;
    m_stack->setName(newName);
    save();
    emit dataChanged();
}

QString MapStackEditor::icon() const
{
    QString leaderId = m_stack->group().leaderId();
    return "IsoAnim-IsoUnit-" + leaderId.toUpper() + "STOP" + QString::number(rotation());
}

bool MapStackEditor::ignore() const
{
    return m_object.ignoreAI;
}

void MapStackEditor::setIgnore(bool newIgnore)
{
    if (m_object.ignoreAI == newIgnore)
        return;
    m_object.ignoreAI = newIgnore;
    save();
    emit dataChanged();
}

int MapStackEditor::priority() const
{
    return m_object.priority;
}

void MapStackEditor::setPriority(int newPriority)
{
    if (m_object.priority == newPriority)
        return;
    m_object.priority = newPriority;
    save();
    emit dataChanged();
}

StackOrderEditor *MapStackEditor::orderEditor() const
{
    return m_orderEditor;
}

OwnerSelectModel *MapStackEditor::ownerModel() const
{
    return m_ownerModel;
}

QString MapStackEditor::note() const
{
    return m_object.note;
}

void MapStackEditor::setNote(const QString &newNote)
{
    if (m_object.note == newNote)
        return;
    m_object.note = newNote;
    save();
    emit dataChanged();
}

QString MapStackEditor::uid() const
{
    return IdToString(m_object.uid);
}

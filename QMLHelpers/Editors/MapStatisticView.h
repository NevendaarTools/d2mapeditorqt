#ifndef MAPSTATISTICVIEW_H
#define MAPSTATISTICVIEW_H
#include <QObject>
#include "MapObjects/TreasureObject.h"
#include "Commands/CommandBus.h"
#include <QSharedPointer>
#include <QQuickWidget>
#include "EditorWidget.h"
#include "QMLHelpers/Models/InventoryModel.h"
#include <QQmlContext>

class MapStatisticView : public QObject, public IObjectEditor
{
    Q_OBJECT
public:
    explicit MapStatisticView(QObject *parent = nullptr);

    Q_PROPERTY(QString mapName READ mapName NOTIFY statsChanged FINAL)

    Q_PROPERTY(int expKillStack     READ expKillStack       NOTIFY statsChanged FINAL)
    Q_PROPERTY(int expKillRuin      READ expKillRuin        NOTIFY statsChanged FINAL)
    Q_PROPERTY(int expKillVillage   READ expKillVillage     NOTIFY statsChanged FINAL)
    Q_PROPERTY(int expKillTemplate  READ expKillTemplate    NOTIFY statsChanged FINAL)

    Q_PROPERTY(int expKillStackOverage     READ expKillStackOverage       NOTIFY statsChanged FINAL)
    Q_PROPERTY(int expKillRuinOverage      READ expKillRuinOverage        NOTIFY statsChanged FINAL)
    Q_PROPERTY(int expKillVillageOverage   READ expKillVillageOverage     NOTIFY statsChanged FINAL)
    Q_PROPERTY(int expKillTemplateOverage  READ expKillTemplateOverage    NOTIFY statsChanged FINAL)

    Q_PROPERTY(float waterProportion        READ waterProportion        NOTIFY statsChanged FINAL)
    Q_PROPERTY(float treeProportion         READ treeProportion         NOTIFY statsChanged FINAL)
    Q_PROPERTY(float roadProportion         READ roadProportion         NOTIFY statsChanged FINAL)
    Q_PROPERTY(float unpassableProportion   READ unpassableProportion   NOTIFY statsChanged FINAL)
signals:

    // IObjectEditor interface
    void statsChanged();

public:
    virtual void setup(QQuickWidget *editor) override;
    virtual void setUID(QPair<int, int> uid) override;
    virtual QObject *toObject() override;
    virtual QString validate() override;

    int expKillStack() const;
    int expKillRuin() const;
    int expKillVillage() const;
    int expKillTemplate() const;
    int expKillStackOverage() const;
    int expKillRuinOverage() const;
    int expKillVillageOverage() const;
    int expKillTemplateOverage() const;

    float waterProportion() const;
    float treeProportion() const;
    float roadProportion() const;
    float unpassableProportion() const;
    QString mapName() const;

private:
    void updateProportions();
private:
    int m_expKillStack = 0;
    int m_expKillRuin = 0;
    int m_expKillVillage = 0;
    int m_expKillTemplate = 0;
    int m_expKillStackOverage;
    int m_expKillRuinOverage;
    int m_expKillVillageOverage;
    int m_expKillTemplateOverage;
    float m_waterProportion;
    float m_treeProportion;
    float m_roadProportion;
    float m_unpassableProportion;
    QString m_mapName;
};

#endif // MAPSTATISTICVIEW_H

#ifndef MAPSTACKEDITOR_H
#define MAPSTACKEDITOR_H

#include <QObject>
#include <QObject>
#include "MapObjects/StackObject.h"
#include "Commands/CommandBus.h"
#include <QSharedPointer>
#include <QQuickWidget>
#include "QMLHelpers/Models/InventoryModel.h"
#include <QQmlContext>
#include "QMLHelpers/Components/GroupUnitsEditor.h"
#include "QMLHelpers/Components/StackOrderEditor.h"
#include "QMLHelpers/Models/OwnerSelectModel.h"

class MapStackEditor : public QObject, public IObjectEditor
{
    Q_OBJECT
public:
    explicit MapStackEditor(QObject *parent = nullptr);
    Q_PROPERTY(GroupUnitsEditor* stack READ stack NOTIFY dataChanged)
    Q_PROPERTY(InventoryModel* inventory READ inventory NOTIFY dataChanged)
    Q_PROPERTY(int rotation READ rotation WRITE setRotation NOTIFY dataChanged)
    Q_PROPERTY(int priority READ priority WRITE setPriority NOTIFY dataChanged)
    Q_PROPERTY(QString name READ name WRITE setName NOTIFY dataChanged)
    Q_PROPERTY(QString note READ note WRITE setNote NOTIFY dataChanged)
    Q_PROPERTY(QString icon READ icon NOTIFY dataChanged)
    Q_PROPERTY(bool ignore READ ignore WRITE setIgnore NOTIFY dataChanged)
    Q_PROPERTY(StackOrderEditor* orderEditor READ orderEditor NOTIFY dataChanged)
    Q_PROPERTY(QString uid READ uid NOTIFY dataChanged)
    Q_PROPERTY(OwnerSelectModel *  ownerModel READ ownerModel NOTIFY changed)

    virtual void setup(QQuickWidget* editor) override;
    virtual void setUID(QPair<int, int> uid) override;
    virtual QObject * toObject() override {return this;}
    QString validate() override;

    Q_INVOKABLE void setUID(const QString& uid);
    void setStack(StackObject stack);
    GroupUnitsEditor *stack() const;
    InventoryModel *inventory() const;

    int rotation() const;
    void setRotation(int newRotation);
    QString name() const;
    void setName(const QString &newName);

    QString icon() const;

    bool ignore() const;
    void setIgnore(bool newIgnore);
    int priority() const;
    void setPriority(int newPriority);

    StackOrderEditor *orderEditor() const;

    OwnerSelectModel *ownerModel() const;

    QString note() const;
    void setNote(const QString &newNote);

    QString uid() const;

public slots:
    void save();
    void onUnitDrag();
signals:
    void dataChanged();
    void loadFinished();
    void changed();
private:
private:
    StackObject m_object;
    GroupUnitsEditor *m_stack;
    InventoryModel *m_inventory;
    QString m_icon;
    bool m_ignore;
    int m_priority;
    StackOrderEditor *m_orderEditor;
    OwnerSelectModel *m_ownerModel;
    bool m_initState = false;
};

#endif // MAPSTACKEDITOR_H

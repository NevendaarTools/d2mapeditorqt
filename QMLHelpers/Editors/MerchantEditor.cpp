#include "MerchantEditor.h"
#include "Engine/GameInstance.h"
#include "Engine/Map/MapStateHolder.h"

MerchantEditor::MerchantEditor(QObject *parent) : QObject(parent)
{
    m_goods = new InventoryModel(this);
    m_spells = new SpellsListModel(this);
    m_units = new UnitHireModel(this);
    m_typeBinding.insert(MerchantObject::Items, "G000SI0000MERH");
    m_typeBinding.insert(MerchantObject::Spells, "G000SI0000MAGE");
    m_typeBinding.insert(MerchantObject::Units, "G000SI0000MERC");
    m_typeBinding.insert(MerchantObject::Trainer, "G000SI0000TRAI");
    connect(m_goods, SIGNAL(changed()), SLOT(save()));
    connect(m_spells, SIGNAL(changed()), SLOT(save()));
    connect(m_units, SIGNAL(changed()), SLOT(save()));
    connect(this, SIGNAL(dataChanged()), this, SLOT(save()));
    m_initState = true;
}

void MerchantEditor::setObject(const MerchantObject &newObject)
{
    m_initState = true;
    m_object = newObject;
    m_goods->setInventory(m_object.inventory);
    m_spells->setSpells(m_object.spells);
    m_units->setUnits(m_object.units);
    m_maxImage = maxImageForType(m_object.type);
    emit dataChanged();
    m_initState = false;
}

void MerchantEditor::setUID(const QString &uid)
{
    QStringList data = uid.split(":");
    if (data.count() < 2)
        return;
    setUID(QPair<int,int>(data[0].toInt(), data[1].toInt()));
}

void MerchantEditor::setup(QQuickWidget *editor)
{
    QQmlContext* context = editor->rootContext();
    context->setContextProperty("merchant", this->toObject());
    editor->setSource(QUrl::fromLocalFile("QML/edit_Merchant.qml"));
}

void MerchantEditor::setUID(QPair<int, int> uid)
{
    m_object = RESOLVE(MapStateHolder)->objectByIdT<MerchantObject>(uid);
    setObject(m_object);
}

InventoryModel *MerchantEditor::goods() const
{
    return m_goods;
}

QString MerchantEditor::type() const
{
    return m_typeBinding[m_object.type];
}

void MerchantEditor::setType(const QString &newType)
{
    foreach (int type, m_typeBinding.keys())
    {
        if (m_typeBinding[type] == newType)
            m_object.type = (MerchantObject::Type)type;
    }
    m_maxImage = maxImageForType(m_object.type);
    emit dataChanged();
}

QString MerchantEditor::image() const
{
    return QString::number(m_object.image).rightJustified(2,'0');
}

void MerchantEditor::setImage(const QString & newImage)
{
    if (m_object.image == newImage.toInt())
        return;
    m_object.image = newImage.toInt();
    emit dataChanged();
}

int MerchantEditor::maxImage() const
{
    return m_maxImage;
}

void MerchantEditor::setMaxImage(int newMaxImage)
{
    if (m_maxImage == newMaxImage)
        return;
    m_maxImage = newMaxImage;
    emit dataChanged();
}

int MerchantEditor::maxImageForType(int type) const
{
    return 99;
}

const QString &MerchantEditor::name() const
{
    return m_object.name;
}

void MerchantEditor::setName(const QString &newName)
{
    if (m_object.name == newName)
        return;
    m_object.name = newName;
    emit dataChanged();
}

const QString &MerchantEditor::desc() const
{
    return m_object.desc;
}

void MerchantEditor::setDesc(const QString &newDesc)
{
    if (m_object.desc == newDesc)
        return;
    m_object.desc = newDesc;
    emit dataChanged();
}

int MerchantEditor::priority() const
{
    return m_object.priority;
}

void MerchantEditor::setPriority(int newPriority)
{
    if (m_object.priority == newPriority)
        return;
    m_object.priority = newPriority;
    emit dataChanged();
}

SpellsListModel *MerchantEditor::spells() const
{
    return m_spells;
}

int MerchantEditor::typeId() const
{
    return m_object.type;
}

void MerchantEditor::setTypeId(int newTypeId)
{
    if (m_object.type == newTypeId)
        return;
    m_object.type = (MerchantObject::Type)newTypeId;
    emit dataChanged();
}

UnitHireModel *MerchantEditor::units() const
{
    return m_units;
}

void MerchantEditor::save()
{
    if (m_object.uid == EMPTY_ID || m_initState)
        return;
    m_object.inventory = m_goods->inventory();
    m_object.spells = m_spells->getSpells();
    m_object.units = m_units->units();
    RESOLVE(MapStateHolder)->replaceObject(QSharedPointer<MapObject>(new MerchantObject(m_object)));
}

QString MerchantEditor::uid() const
{
    return IdToString(m_object.uid);
}

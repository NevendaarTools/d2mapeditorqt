#ifndef MERCHANTEDITOR_H
#define MERCHANTEDITOR_H
#include <QObject>
#include "MapObjects/MerchantObject.h"
#include "Commands/CommandBus.h"
#include <QSharedPointer>
#include <QQuickWidget>
#include "EditorWidget.h"
#include "QMLHelpers/Models/InventoryModel.h"
#include "QMLHelpers/Models/SpellsListModel.h"
#include "QMLHelpers/Models/UnitHireModel.h"
#include <QQmlContext>

class MerchantEditor : public QObject, public IObjectEditor
{
    Q_OBJECT
public:
    Q_PROPERTY(InventoryModel* goods READ goods NOTIFY dataChanged)
    Q_PROPERTY(SpellsListModel* spells READ spells NOTIFY dataChanged)
    Q_PROPERTY(UnitHireModel* units READ units NOTIFY dataChanged)
    Q_PROPERTY(QString type READ type WRITE setType NOTIFY dataChanged)
    Q_PROPERTY(int typeId READ typeId WRITE setTypeId NOTIFY dataChanged)
    Q_PROPERTY(QString image READ image WRITE setImage NOTIFY dataChanged)
    Q_PROPERTY(QString name READ name WRITE setName NOTIFY dataChanged)
    Q_PROPERTY(QString desc READ desc WRITE setDesc NOTIFY dataChanged)
    Q_PROPERTY(int maxImage READ maxImage WRITE setMaxImage NOTIFY dataChanged)
    Q_PROPERTY(int priority READ priority WRITE setPriority NOTIFY dataChanged)
    Q_PROPERTY(QString uid READ uid NOTIFY dataChanged)

    explicit MerchantEditor(QObject *parent = nullptr);
    Q_INVOKABLE void setUID(const QString& uid);
    virtual void setup(QQuickWidget* editor) override;
    virtual void setUID(QPair<int, int> uid) override;
    virtual QObject * toObject() override {return this;}
    InventoryModel *goods() const;
    QString validate() override{return QString();}

    QString type() const;
    void setType(const QString &newType);

    QString image() const;
    void setImage(const QString & newImage);

    int maxImage() const;
    void setMaxImage(int newMaxImage);

    const QString &name() const;
    void setName(const QString &newName);

    const QString &desc() const;
    void setDesc(const QString &newDesc);

    int priority() const;
    void setPriority(int newPriority);

    SpellsListModel *spells() const;

    int typeId() const;
    void setTypeId(int newTypeId);

    UnitHireModel *units() const;
    void setObject(const MerchantObject &newObject);

    QString uid() const;

public slots:
    void save();
signals:
    void dataChanged();
private:
    int maxImageForType(int type) const;
private:
    MerchantObject m_object;
    InventoryModel *m_goods;
    SpellsListModel *m_spells;
    UnitHireModel *m_units;
    QHash<int, QString> m_typeBinding;
    int m_maxImage = 5;
    bool m_initState = false;
};

#endif // MERCHANTEDITOR_H

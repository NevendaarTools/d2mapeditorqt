#include "MapInfoEditor.h"
#include "Engine/Map/MapStateHolder.h"
#include "Engine/GameInstance.h"

MapInfoEditor::MapInfoEditor(QObject *parent) : QObject(parent)
{
    connect(this, SIGNAL(dataChanged()), this, SLOT(save()));
}

void MapInfoEditor::setUID(const QString &uid)
{
    QStringList data = uid.split(":");
    if (data.count() < 2)
        return;
    setUID(QPair<int,int>(data[0].toInt(), data[1].toInt()));
}

void MapInfoEditor::setUID(QPair<int, int> uid)
{
    m_info = RESOLVE(MapStateHolder)->objectByIdT<MapInfo>(uid);
    setInfo(m_info);
}

void MapInfoEditor::setup(QQuickWidget *editor)
{
    QQmlContext* context = editor->rootContext();
    context->setContextProperty("mapInfoEditor", this->toObject());
    editor->setSource(QUrl::fromLocalFile("QML/edit_Info.qml"));
}

QString MapInfoEditor::validate()
{
    return QString();
}

void MapInfoEditor::setInfo(MapInfo info)
{
    m_initState = true;
    emit dataChanged();
    m_initState = false;
}

QString MapInfoEditor::name() const
{
    return m_info.info.name;
}

void MapInfoEditor::setName(const QString &newName)
{
    if (m_info.info.name == newName)
        return;
    m_info.info.name = newName;
    emit dataChanged();
}

QString MapInfoEditor::author() const
{
    return m_info.info.creator;
}

void MapInfoEditor::setAuthor(const QString &newAuthor)
{
    if (m_info.info.creator == newAuthor)
        return;
    m_info.info.creator = newAuthor;
    emit dataChanged();
}

QString MapInfoEditor::desc() const
{
    return m_info.info.desc;
}

void MapInfoEditor::setDesc(const QString &newDesc)
{
    if (m_info.info.desc == newDesc)
        return;
    m_info.info.desc = newDesc;
    emit dataChanged();
}

QString MapInfoEditor::target() const
{
    return m_info.info.briefing;
}

void MapInfoEditor::setTarget(const QString &newTarget)
{
    if (m_info.info.briefing == newTarget)
        return;
    m_info.info.briefing = newTarget;
    emit dataChanged();
}

QString MapInfoEditor::story() const
{
    return m_info.info.brieflong1;
}

void MapInfoEditor::setStory(const QString &newStory)
{
    if (m_info.info.brieflong1 == newStory)
        return;
    m_info.info.brieflong1 = newStory;
    emit dataChanged();
}

int MapInfoEditor::maxHero() const
{
    return m_info.info.maxLeader;
}

void MapInfoEditor::setMaxHero(int newMaxHero)
{
    if (m_info.info.maxLeader == newMaxHero)
        return;
    m_info.info.maxLeader = newMaxHero;
    emit dataChanged();
}

QString MapInfoEditor::victory() const
{
    return m_info.info.debunkw;
}

void MapInfoEditor::setVictory(const QString &newVictory)
{
    if (m_info.info.debunkw == newVictory)
        return;
    m_info.info.debunkw = newVictory;
    emit dataChanged();
}

QString MapInfoEditor::loose() const
{
    return m_info.info.debunkl;
}

void MapInfoEditor::setLoose(const QString &newLoose)
{
    if (m_info.info.debunkl == newLoose)
        return;
    m_info.info.debunkl = newLoose;
    emit dataChanged();
}

int MapInfoEditor::maxSpell() const
{
    return m_info.info.maxSpell;
}

void MapInfoEditor::setMaxSpell(int newMaxSpell)
{
    if (m_info.info.maxSpell == newMaxSpell)
        return;
    m_info.info.maxSpell = newMaxSpell;
    emit dataChanged();
}

int MapInfoEditor::maxUnit() const
{
    return m_info.info.maxUnit;
}

void MapInfoEditor::setMaxUnit(int newMaxUnit)
{
    if (m_info.info.maxUnit == newMaxUnit)
        return;
    m_info.info.maxUnit = newMaxUnit;
    emit dataChanged();
}

int MapInfoEditor::inititalHero() const
{
    return m_info.info.suggLvl;
}

void MapInfoEditor::setInititalHero(int newInititalHero)
{
    if (m_info.info.suggLvl == newInititalHero)
        return;
    m_info.info.suggLvl = newInititalHero;
    emit dataChanged();
}

void MapInfoEditor::save()
{
    if (m_info.uid == EMPTY_ID || m_initState)
        return;
    qDebug()<<Q_FUNC_INFO;
    RESOLVE(MapStateHolder)->replaceObject(m_info);
}

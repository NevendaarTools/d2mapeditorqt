#ifndef TREASUREEDITOR_H
#define TREASUREEDITOR_H
#include <QObject>
#include "MapObjects/TreasureObject.h"
#include "Commands/CommandBus.h"
#include <QQuickWidget>
#include "EditorWidget.h"
#include "QMLHelpers/Models/InventoryModel.h"
#include <QQmlContext>

class TreasureEditor : public QObject, public IObjectEditor
{
    Q_OBJECT
public:
    explicit TreasureEditor(QObject *parent = nullptr);
    Q_PROPERTY(InventoryModel* items READ items NOTIFY dataChanged)
    Q_PROPERTY(int maxImage READ maxImage NOTIFY dataChanged)
    Q_PROPERTY(int priority READ priority WRITE setPriority NOTIFY dataChanged)
    Q_PROPERTY(QString image READ image WRITE setImage NOTIFY dataChanged)
    Q_PROPERTY(bool water READ water NOTIFY dataChanged)

    Q_INVOKABLE void setUID(const QString &uid);
    virtual void setUID(QPair<int, int> uid) override;
    virtual void setup(QQuickWidget* editor) override;
    void setTreasure(TreasureObject stack);
    virtual QObject * toObject() override {return this;}

    InventoryModel *items() const;

    int maxImage() const;

    int priority() const;
    void setPriority(int newPriority);
    QString validate() override{return QString();}

    QString image() const;
    void setImage(const QString &newImage);

    bool water() const;
public slots:
    void save();
signals:
    void dataChanged();

private:
    TreasureObject m_object;
    InventoryModel *m_items;
    int m_maxImage;
    bool m_initState = false;
};

#endif // TREASUREEDITOR_H

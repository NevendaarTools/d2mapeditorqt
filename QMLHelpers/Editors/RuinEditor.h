#ifndef RUINEDITOR_H
#define RUINEDITOR_H

#include <QObject>
#include "MapObjects/RuinObject.h"
#include "Commands/CommandBus.h"
#include <QSharedPointer>
#include <QQuickWidget>
#include "EditorWidget.h"
#include "QMLHelpers/Models/InventoryModel.h"
#include <QQmlContext>
#include "QMLHelpers/Components/GroupUnitsEditor.h"
#include "QMLHelpers/Models/EventsListModel.h"

class RuinEditor : public QObject, public IObjectEditor
{
    Q_OBJECT
public:
    explicit RuinEditor(QObject *parent = nullptr);
    Q_PROPERTY(GroupUnitsEditor* units READ units NOTIFY dataChanged)
    Q_PROPERTY(QString item READ item WRITE setItem NOTIFY dataChanged)
    Q_PROPERTY(QString cost READ cost WRITE setCost NOTIFY dataChanged)
    Q_PROPERTY(QString name READ name WRITE setName NOTIFY dataChanged)
    Q_PROPERTY(QString desc READ desc WRITE setDesc NOTIFY dataChanged)
    Q_PROPERTY(int maxImage READ maxImage NOTIFY dataChanged)
    Q_PROPERTY(int priority READ priority WRITE setPriority NOTIFY dataChanged)
    Q_PROPERTY(QString image READ image WRITE setImage NOTIFY dataChanged)
    Q_PROPERTY(QString uid READ uid NOTIFY dataChanged)


    virtual void setup(QQuickWidget* editor) override;
    virtual void setUID(QPair<int, int> uid) override;
    Q_INVOKABLE void setUID(const QString &uid);
    void setRuin(RuinObject object);
    virtual QObject * toObject() override {return this;}
    QString validate() override{return QString();}

    GroupUnitsEditor *units() const;

    const QString &item() const;

    QString cost() const;

    int maxImage() const;

    int priority() const;
    void setPriority(int newPriority);

    QString image() const;
    void setImage(const QString &newImage);

    void setCost(const QString &newCost);

    const QString &name() const;
    void setName(const QString &newName);

    const QString &desc() const;
    void setDesc(const QString &newDesc);

    void setItem(const QString &newItem);

    QString uid() const;

signals:
    void dataChanged();
private slots:
    void save();
private:
    RuinObject m_object;
    GroupUnitsEditor *m_units;
    bool m_initState = false;
};

#endif // RUINEDITOR_H

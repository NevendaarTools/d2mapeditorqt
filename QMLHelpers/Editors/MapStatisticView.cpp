#include "MapStatisticView.h"
#include "MapObjects/StackObject.h"
#include "MapObjects/FortObject.h"
#include "MapObjects/RuinObject.h"
#include "MapObjects/StackTemplateObject.h"
#include "Engine/GameInstance.h"
#include "Engine/Map/MapStateHolder.h"

MapStatisticView::MapStatisticView(QObject *parent)
    : QObject{parent}
{}

void MapStatisticView::setup(QQuickWidget *editor)
{
    QQmlContext* context = editor->rootContext();
    context->setContextProperty("mapStatisticView", this->toObject());
    editor->setSource(QUrl::fromLocalFile("QML/edit_MapStatistics.qml"));
}

void MapStatisticView::setUID(QPair<int, int> uid)
{
    m_mapName = RESOLVE(MapStateHolder)->getMapName();

    m_expKillStack = 0;
    m_expKillRuin = 0;
    m_expKillVillage = 0;
    m_expKillTemplate = 0;
    m_expKillStackOverage = 0;
    m_expKillRuinOverage = 0;
    m_expKillVillageOverage = 0;
    m_expKillTemplateOverage = 0;

    auto villages = RESOLVE(MapStateHolder)->objectsByType<FortObject>();
    foreach (const FortObject & village, villages)
    {
        if (village.fortType == FortObject::Capital)
            continue;
        m_expKillVillage += groupExpForKill(village.garrison);
    }
    if (villages.count())
        m_expKillVillageOverage = m_expKillVillage / (double)(villages.count());

    auto ruins = RESOLVE(MapStateHolder)->objectsByType<RuinObject>();
    foreach (const auto & ruin, ruins)
    {
        m_expKillRuin += groupExpForKill(ruin.guards);
    }
    if (ruins.count())
        m_expKillRuinOverage = m_expKillRuin / (double)(ruins.count());

    auto stacks = RESOLVE(MapStateHolder)->objectsByType<StackObject>();
    foreach (const auto & stack, stacks)
    {
        m_expKillStack += groupExpForKill(stack.stack);
    }
    if (stacks.count())
        m_expKillStackOverage = m_expKillStack / (double)(stacks.count());

    auto templates = RESOLVE(MapStateHolder)->objectsByType<StackTemplateObject>();
    foreach (const auto & stackTemplate, templates)
    {
        m_expKillTemplate += groupExpForKill(stackTemplate.stack);
    }
    if (templates.count())
        m_expKillTemplateOverage = m_expKillTemplate / (double)(templates.count());

    updateProportions();

    emit statsChanged();
}

QObject *MapStatisticView::toObject()
{
    return this;
}

QString MapStatisticView::validate()
{
    return QString();
}

int MapStatisticView::expKillStack() const
{
    return m_expKillStack;
}

int MapStatisticView::expKillRuin() const
{
    return m_expKillRuin;
}

int MapStatisticView::expKillVillage() const
{
    return m_expKillVillage;
}

int MapStatisticView::expKillTemplate() const
{
    return m_expKillTemplate;
}

int MapStatisticView::expKillStackOverage() const
{
    return m_expKillStackOverage;
}

int MapStatisticView::expKillRuinOverage() const
{
    return m_expKillRuinOverage;
}

int MapStatisticView::expKillVillageOverage() const
{
    return m_expKillVillageOverage;
}

int MapStatisticView::expKillTemplateOverage() const
{
    return m_expKillTemplateOverage;
}

float MapStatisticView::waterProportion() const
{
    return m_waterProportion;
}

float MapStatisticView::treeProportion() const
{
    return m_treeProportion;
}

float MapStatisticView::roadProportion() const
{
    return m_roadProportion;
}

float MapStatisticView::unpassableProportion() const
{
    return m_unpassableProportion;
}

void MapStatisticView::updateProportions()
{
    m_waterProportion = 0;
    m_treeProportion = 0;
    m_roadProportion = 0;
    m_unpassableProportion = 0;

    MapGrid grid = RESOLVE(MapStateHolder)->getGrid();
    int total = grid.cells.count() * grid.cells.count();

    QSet<int> nonPassable;
    nonPassable << (int)MapObject::LandMark;
    nonPassable << (int)MapObject::Fort;
    nonPassable << (int)MapObject::Mountain;
    nonPassable << (int)MapObject::Crystal;
    nonPassable << (int)MapObject::Ruin;
    nonPassable << (int)MapObject::Merchant;

    for (int k = 0; k < grid.cells.count(); ++k)
    {
        for (int i = 0; i < grid.cells.count(); i++)
        {
            if (grid.cells[i][k].isWater())
                m_waterProportion++;
            if (grid.cells[i][k].roadType != -1)
                m_roadProportion++;
            if (tileGround(grid.cells[i][k].value)  == 1)
                m_treeProportion++;
        }
    }
    QVector<QVector<MapObjBinding::UIDList>> binding = grid.objBinging.binding;
    for (int k = 0; k <  binding.count(); ++k)
    {
        for (int i = 0; i <  binding.count(); i++)
        {
            for(const auto & id: binding[k][i].items)
            {
                if (nonPassable.contains(id.first))
                {
                    m_unpassableProportion++;
                    continue;
                }

            }
        }
    }


    m_waterProportion /= (float)total;
    m_roadProportion /= (float)total;
    m_treeProportion /= (float)total;
    m_unpassableProportion /= (float)total;

    m_waterProportion *= 100;
    m_roadProportion *= 100;
    m_treeProportion *= 100;
    m_unpassableProportion *= 100;
}

QString MapStatisticView::mapName() const
{
    return m_mapName;
}

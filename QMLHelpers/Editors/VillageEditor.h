#ifndef VILLAGEEDITOR_H
#define VILLAGEEDITOR_H

#include <QObject>
#include "MapObjects/FortObject.h"
#include <QSharedPointer>
#include <QQuickWidget>
#include "QMLHelpers/Models/InventoryModel.h"
#include <QQmlContext>
#include "QMLHelpers/Components/GroupUnitsEditor.h"
#include "QMLHelpers/Models/OwnerSelectModel.h"

class FortEditor : public QObject, public IObjectEditor
{
    Q_OBJECT
public:
    Q_PROPERTY(int level READ level WRITE setLevel NOTIFY changed)
    Q_PROPERTY(QString name READ name WRITE setName NOTIFY changed)
    Q_PROPERTY(QString stackName READ stackName WRITE setStackName NOTIFY changed)
    Q_PROPERTY(GroupUnitsEditor* garrison READ garrison NOTIFY changed)
    Q_PROPERTY(GroupUnitsEditor* visiter READ visiter NOTIFY changed)
    Q_PROPERTY(InventoryModel* garrisonInventory READ garrisonInventory NOTIFY changed)
    Q_PROPERTY(InventoryModel* visiterInventory READ visiterInventory NOTIFY changed)
    Q_PROPERTY(OwnerSelectModel *  ownerModel READ ownerModel NOTIFY changed)
    Q_PROPERTY(QString image READ image NOTIFY changed)
    Q_PROPERTY(int fortType READ fortType NOTIFY changed)
    Q_PROPERTY(QString uid READ uid NOTIFY changed)
    Q_PROPERTY(QString stackUid READ stackUid NOTIFY changed)

    explicit FortEditor(FortObject village = FortObject(), QObject *parent = nullptr);
    Q_INVOKABLE void setUID(const QString &uid);
    virtual void setup(QQuickWidget* editor) override;
    virtual void setUID(QPair<int, int> uid) override;
    void setVillage(FortObject village);
    int level() const;
    virtual QObject * toObject() override {return this;}
    QString validate() override{return QString();}

    GroupUnitsEditor* garrison() const;
    GroupUnitsEditor* visiter() const;

    InventoryModel *garrisonInventory() const;
    InventoryModel *visiterInventory() const;

    const QString &name() const;
    void setName(const QString &newName);
    OwnerSelectModel *ownerModel() const;

    QString image() const;

    const QString stackName() const;
    void setStackName(const QString &newStackName);

    int fortType() const;

    QString uid() const;

    QString stackUid() const;

public slots:
    void setLevel(int level);
    void close(){}
    void onUnitDrag();
    void save();
signals:
    void changed();
private:
    void updateImage();
private:
    FortObject m_fort;
    GroupUnitsEditor * m_garrison;
    GroupUnitsEditor * m_visiter;
    InventoryModel *m_garrisonInventory;
    InventoryModel *m_visiterInventory;
    OwnerSelectModel *m_ownerModel;
    QString m_image;
    StackObject m_visiterObject;
    bool m_initState = false;
};

#endif // VILLAGEEDITOR_H


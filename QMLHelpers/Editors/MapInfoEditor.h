#ifndef MAPINFOEDITOR_H
#define MAPINFOEDITOR_H
#include "EditorWidget.h"
#include "MapObjects/Diplomacy.h"

class MapInfoEditor : public QObject, public IObjectEditor
{
    Q_OBJECT
public:
    MapInfoEditor(QObject *parent = nullptr);
    Q_PROPERTY(QString name READ name WRITE setName NOTIFY dataChanged)
    Q_PROPERTY(QString author READ author WRITE setAuthor NOTIFY dataChanged)

    Q_PROPERTY(QString desc READ desc WRITE setDesc NOTIFY dataChanged)
    Q_PROPERTY(QString target READ target WRITE setTarget NOTIFY dataChanged)
    Q_PROPERTY(QString story READ story WRITE setStory NOTIFY dataChanged)
    Q_PROPERTY(QString victory READ victory WRITE setVictory NOTIFY dataChanged)
    Q_PROPERTY(QString loose READ loose WRITE setLoose NOTIFY dataChanged)

    Q_PROPERTY(int maxSpell READ maxSpell WRITE setMaxSpell NOTIFY dataChanged)
    Q_PROPERTY(int maxUnit READ maxUnit WRITE setMaxUnit NOTIFY dataChanged)
    Q_PROPERTY(int maxHero READ maxHero WRITE setMaxHero NOTIFY dataChanged)
    Q_PROPERTY(int inititalHero READ inititalHero WRITE setInititalHero NOTIFY dataChanged)

    Q_INVOKABLE void setUID(const QString &uid);
    virtual void setUID(QPair<int, int> uid) override;
    virtual void setup(QQuickWidget* editor) override;
    virtual QObject * toObject() override {return this;}
    virtual QString validate() override;
    void setInfo(MapInfo info);
    QString name() const;
    void setName(const QString &newName);

    QString author() const;
    void setAuthor(const QString &newAuthor);

    QString desc() const;
    void setDesc(const QString &newDesc);

    QString target() const;
    void setTarget(const QString &newTarget);

    QString story() const;
    void setStory(const QString &newStory);

    int maxHero() const;
    void setMaxHero(int newMaxHero);

    QString victory() const;
    void setVictory(const QString &newVictory);

    QString loose() const;
    void setLoose(const QString &newLoose);

    int maxSpell() const;
    void setMaxSpell(int newMaxSpell);

    int maxUnit() const;
    void setMaxUnit(int newMaxUnit);

    int inititalHero() const;
    void setInititalHero(int newInititalHero);

public slots:
    void save();
signals:
    void dataChanged();
private:
    MapInfo m_info;
    bool m_initState = false;
};

#endif // MAPINFOEDITOR_H

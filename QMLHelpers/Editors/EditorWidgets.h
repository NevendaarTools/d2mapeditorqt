#ifndef EDITORWIDGETS_H
#define EDITORWIDGETS_H
#include "VillageEditor.h"
#include "MerchantEditor.h"
#include "RuinEditor.h"
#include "TreasureEditor.h"
#include "QuestLineEditor.h"
#include "MapStackEditor.h"

static void editorsRegister()
{
    qmlRegisterType<QuestLineEditor>("QMLQuestLineEditor", 1, 0,"QuestLineEditor");
    qmlRegisterType<FortEditor>("QMLVillageEditor", 1, 0,"VillageEditor");
    qmlRegisterType<TreasureEditor>("QMLTreasureEditor", 1, 0,"TreasureEditor");
    qmlRegisterType<FortEditor>("QMLCapitalEditor", 1, 0,"CapitalEditor");
    qmlRegisterType<MerchantEditor>("QMLMerchantEditor", 1, 0,"MerchantEditor");
    qmlRegisterType<RuinEditor>("QMLRuinEditor", 1, 0,"RuinEditor");
    qmlRegisterType<MapStackEditor>("QMLMapStackEditor", 1, 0,"MapStackEditor");
}

#endif // EDITORWIDGETS_H

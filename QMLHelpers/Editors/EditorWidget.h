#ifndef EDITORWIDGET_H
#define EDITORWIDGET_H

#include <QObject>
#include "Commands/EditCommands.h"
#include <QQuickWidget>
#include <QGraphicsView>
#include <QQmlEngine>
#include <QQmlContext>
#include "Commands/CommandBus.h"
#include "QMLHelpers/Common/ImageProvider.h"
#include "Engine/Components/TranslationHelper.h"
#include "QMLHelpers/Common/OverlayWrapper.h"

class EditorWidget : public QObject, public CommandExecutor
{
    Q_OBJECT
public:
    explicit EditorWidget(QWidget *view, QObject *parent = nullptr);

    CommandResult executeEditCommand(const QVariant & commandVar);
    CommandResult executeChangeCrystalCommand(const QVariant & commandVar);
    Q_INVOKABLE bool hasChanges() const;
    Q_INVOKABLE QString validate() const;
public slots:
    void commit();
    void cancel();
signals:
private:
    QWidget * m_graphicsView;
    QQuickWidget* m_editorWidget;
    QHash<int, QSharedPointer<IObjectEditor>> m_editors;
    QSharedPointer<IObjectEditor> m_currentEditor;
    QSharedPointer<ImageProvider> m_imageProvider;
    OverlayWrapper * m_overlayWrapper;
};

#endif // EDITORWIDGET_H

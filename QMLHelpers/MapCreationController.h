#ifndef MAPCREATIONCONTROLLER_H
#define MAPCREATIONCONTROLLER_H

#include <QObject>
#include <QPair>
#include <QList>
#include "Events/EventBus.h"

struct RaceSettings
{
    int uid;
    bool selected = false;
    QString image = "";
    QString toString() const{return QString("%1 %2").arg(uid).arg(selected);}
};

class MapCreationController : public QObject, EventSubscriber
{
    Q_OBJECT
public:
    explicit MapCreationController(QObject *parent = nullptr);
    Q_PROPERTY(QString mapName READ mapName WRITE setMapName NOTIFY settingsChanged)
    Q_PROPERTY(int mapSize READ mapSize WRITE setMapSize NOTIFY settingsChanged)
    Q_PROPERTY(int raceCount READ raceCount NOTIFY settingsChanged)
    Q_PROPERTY(int selectedCount READ selectedCount NOTIFY settingsChanged)

    const QString &mapName() const;
    int mapSize() const;
    int raceCount() const;

    void setMapName(const QString &newMapName);
    void setMapSize(int newMapSize);

    Q_INVOKABLE QString raceImage(int index) const;
    Q_INVOKABLE bool raceSelected(int index) const;
    Q_INVOKABLE void selectRace(int index);
    Q_INVOKABLE void createMap();

    int selectedCount() const;
    void onGameLoaded(const QVariant &event);

signals:
    void settingsChanged();
private:
    QList<RaceSettings> m_races;
    QString m_mapName ="newMap";
    int m_mapSize = 48;
    int m_selectedCount = 0;
};

#endif // MAPCREATIONCONTROLLER_H

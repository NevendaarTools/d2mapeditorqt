#include "MapCreationController.h"
#include <QDebug>
#include "Events/MapEvents.h"
#include "toolsqt/MapUtils/D2MapEditor.h"
#include "Commands/MenuCommands.h"
#include "Engine/MapConverter.h"
#include "Engine/GameInstance.h"
#include "toolsqt/DBFModel/DBFModel.h"
#include "Engine/Map/MapStateHolder.h"
#include "Engine/SettingsManager.h"
#include "Engine/GameInstance.h"
#include "Commands/CommandBus.h"
#include "Events/EventBus.h"
#include "Events/MapEvents.h"

MapCreationController::MapCreationController(QObject *parent)
    : QObject{parent}
{
    onGameLoaded(QVariant());
    subscribe<GameLoadedEvent>([this](const QVariant &event){onGameLoaded(event);});
}

const QString &MapCreationController::mapName() const
{
    return m_mapName;
}

int MapCreationController::mapSize() const
{
    return m_mapSize;
}

void MapCreationController::setMapName(const QString &newMapName)
{
    if (m_mapName == newMapName)
        return;
    m_mapName = newMapName;
    emit settingsChanged();
}

void MapCreationController::setMapSize(int newMapSize)
{
    if (m_mapSize == newMapSize)
        return;
    m_mapSize = newMapSize;
    emit settingsChanged();
}

QString MapCreationController::raceImage(int index) const
{
    return m_races[index].image;
}

bool MapCreationController::raceSelected(int index) const
{
    qDebug()<<Q_FUNC_INFO;
    return m_races[index].selected;
}

void MapCreationController::selectRace(int index)
{
    qDebug()<<Q_FUNC_INFO;
    m_races[index].selected = !m_races[index].selected;
    for(auto & race: m_races)
        qDebug()<<race.toString();
    m_selectedCount = 0;
    for(auto & race: m_races)
            if (race.selected)
                m_selectedCount++;
    emit settingsChanged();
}

void MapCreationController::createMap()
{
    D2MapEditor editor(RESOLVE(DBFModel));
    editor.createMap(m_mapSize);
    editor.rename(m_mapName, "");

    int y = 1;
    if (m_races[0].selected)
    {
        editor.addRace("G000RR0000", 1, y, Glord::Warrior, "Гоудфрой",
                       "В молодости, этот могущественный лорд сумел"
                       " в одиночку выстоять против целой армии чудовищ"
                       " из ада и в конце концов победил их, запечатав лаз"
                       " в земле, откуда эти полчища извергались. После"
                       " этого он стал верховным правителем своего королевства.",
                       "Роттердам",
                       "Кловис"
                       );
        y+=6;
    }
    if (m_races[1].selected)
    {
        editor.addRace("G000RR0003", 1, y, Glord::Warrior, "Гоудфрой",
                       "В молодости, этот могущественный лорд сумел"
                       " в одиночку выстоять против целой армии чудовищ"
                       " из ада и в конце концов победил их, запечатав лаз"
                       " в земле, откуда эти полчища извергались. После"
                       " этого он стал верховным правителем своего королевства.",
                       "Роттердам",
                       "Кловис"
                       );
        y+=6;
    }
    if (m_races[2].selected)
    {
        editor.addRace("G000RR0002", 1, y, Glord::Warrior, "Гоудфрой",
                       "В молодости, этот могущественный лорд сумел"
                       " в одиночку выстоять против целой армии чудовищ"
                       " из ада и в конце концов победил их, запечатав лаз"
                       " в земле, откуда эти полчища извергались. После"
                       " этого он стал верховным правителем своего королевства.",
                       "Роттердам",
                       "Кловис"
                       );
        y+=6;
    }
    if (m_races[3].selected)
    {
        editor.addRace("G000RR0001", 1, y, Glord::Warrior, "Хейрик",
                       "Самый могущественный из повелителей карликов, "
                       "известен страстностью своего напора в бою и "
                       "смелостью перед лицом врага. Однажды он дрался "
                       "в течении семи дней и ночей с могущественным "
                       "красным драконом АРФУРУЗОНОМ и в конце концов победил его.",
                       "Замок Гордон",
                       "Кловис"
                       );
        y+=6;
    }
    if (m_races[4].selected)
    {
        editor.addRace("G000RR0005", 1, y, Glord::Warrior, "Хейрик",
                       "Самый могущественный из повелителей карликов, "
                       "известен страстностью своего напора в бою и "
                       "смелостью перед лицом врага. Однажды он дрался "
                       "в течении семи дней и ночей с могущественным "
                       "красным драконом АРФУРУЗОНОМ и в конце концов победил его.",
                       "Замок Гордон",
                       "Кловис"
                       );
        y+=6;
    }
    editor.updateSubraces();
    editor.commitGrid();

    GameMap resMap;
    auto d2map = editor.map();
    MapConverter::importMap(&resMap, d2map);
    resMap.setVersion(EDITOR_VERSION);
    RESOLVE(MapStateHolder)->setMap(resMap);
    PopulateMapCommand command(RESOLVE(DBFModel)->path() + "/Exports/" + m_mapName + ".sg");
    RESOLVE(CommandBus)->execute<PopulateMapCommand>(command);
    RESOLVE(CommandBus)->execute<SaveMapCommand>(SaveMapCommand());
    RESOLVE(EventBus)->notify(MapCreatedEvent());
    qDebug()<<Q_FUNC_INFO;
}

int MapCreationController::raceCount() const
{
    return m_races.count();
}

int MapCreationController::selectedCount() const
{
    return m_selectedCount;
}

void MapCreationController::onGameLoaded(const QVariant &event)
{
    qDebug()<<Q_FUNC_INFO;
    m_races.clear();
    auto races = RESOLVE(DBFModel)->getList<Lrace>();
    for (auto & race: races)
    {
        if (race->id == 4)//skip neutral
            continue;
        QString name = race->text;
        name.replace("L_", "");
        m_races<< RaceSettings{race->id, false,"Interf-DLG_HOTSEAT_LOBBY_RACE_R" + name.toUpper()};
    }

    m_selectedCount = 0;
    emit settingsChanged();
}

#include "MainMenuController.h"
#include "Events/MapEvents.h"
#include "QMLHelpers/Common/IStringDataProvider.h"
#include "toolsqt/MapUtils/DataBlock.h"
#include "Common/MapJsonIO.h"
#include "Commands/MenuCommands.h"
#include <QFileDialog>
#include <QApplication>
#include "toolsqt/MapUtils/D2SagaModel.h"
#include "Engine/Components/TranslationHelper.h"
#include "toolsqt/DBFModel/DBFModel.h"
#include "Engine/GameInstance.h"
#include "Engine/MapView/MinimapHelper.h"
#include "Engine/Components/ResourceManager.h"
#include "Engine/SettingsManager.h"
#include "Commands/CommandBus.h"
#include "Engine/Map/MapStateHolder.h"
#include "MapObjects/Diplomacy.h"

MainMenuController::MainMenuController(QObject *parent)
    : QObject{parent}
{
    subscribe<GameLoadedEvent>([this](const QVariant &event){onGameLoaded(event);});
    subscribe<MapCreatedEvent>([this](const QVariant &event){onMapCreated(event);});
    m_mapListModel = new StringModel();
    m_importListModel = new StringModel();
}

QString MainMenuController::tr(const QString &str)
{
    return TranslationHelper::tr(str);
}

void MainMenuController::onGameLoaded(const QVariant &event)
{
    m_gameName = TranslationHelper::tr("GAME_VERSION");
    qDebug()<<"GameName = "<<m_gameName;
    m_gameName = convertToHtml(m_gameName);//m_gameName.replace("\\n", " ");
    m_gamePath = RESOLVE(DBFModel)->path();
{
    QSharedPointer<IStringDataProvider> provider = m_mapListModel->provider();
    if (provider.isNull())
        provider = QSharedPointer<IStringDataProvider> (new SimpleStringProvider());
    SimpleStringProvider * prov = dynamic_cast<SimpleStringProvider*>(provider.data());
    prov->clear();
    QDir dir(m_gamePath + "/Exports");
    dir.setFilter(QDir::Dirs | QDir::NoDotAndDotDot);
    QStringList folderList = dir.entryList(QStringList("*.nme"), QDir::Dirs);

    foreach (const QString& folderName, folderList)
    {
        QDir folder(dir.absoluteFilePath(folderName));
        GameMap::MapInfo mapInfo;
        if (MapJsonIO::readMapInfo(mapInfo, folder.absolutePath()))
        {
            prov->addRow(mapInfo.name, folder.dirName());
        }
    }
    qDebug()<<"### maps count = "<<provider->dataCount();
    m_mapListModel->setProvider(provider);
}
{
    QSharedPointer<IStringDataProvider> provider = m_importListModel->provider();
    if (provider.isNull())
        provider = QSharedPointer<IStringDataProvider> (new SimpleStringProvider());
    SimpleStringProvider * prov = dynamic_cast<SimpleStringProvider*>(provider.data());
    prov->clear();
    QDir dir(m_gamePath + "/Exports", "*.sg");
    QFileInfoList genNames = dir.entryInfoList();
    foreach (const QFileInfo& name, genNames)
    {
        MapHeaderBlock header;
        QFile file(name.absoluteFilePath());
        file.open(QIODevice::ReadOnly);
        QByteArray data = file.read(3000);
        int index = 0;
        header.read(data, index);
        prov->addRow(header.name, name.fileName());
    }
    qDebug()<<"### exports count = "<<provider->dataCount();
    m_importListModel->setProvider(provider);
}

    MinimapHelper::init();
    emit gamePathChanged();
    // if (m_mapListModel->rowCount() > 0)
    //     setSelectedMap(0);
}

void MainMenuController::onMapCreated(const QVariant &event)
{
    onGameLoaded(event);
}


void MainMenuController::setSelectedMap(int selectedMap, bool importMap)
{
    if (selectedMap == -1)
        return;
    if (importMap)
    {
        QString path = m_gamePath + "/Exports/" + m_importListModel->getDesc(selectedMap);
        if (path.endsWith(".sg"))
        {
            MapHeaderBlock header;
            QFile file(path);
            file.open(QIODevice::ReadOnly);
            QByteArray data = file.read(3000);
            int index = 0;
            header.read(data, index);
            m_mapName = header.name;
            m_mapDesc = header.description;
            m_mapImage = "mapPreview";
            D2MapModel model;
            model.open(path);
            QImage mapImage = MinimapHelper::miniMap(model, RESOLVE(DBFModel));
            RESOLVE(ResourceManager)->addImage(mapImage, "mapPreview");
            m_mapInfo = QString("Author: %1\nSize: %2\nRaces: %3").arg(header.author).arg(header.mapSize).arg("???");
        }
        else if (path.endsWith(".csg"))
        {
            D2SagaModel saga;
            if (!saga.load(path))
            {
                m_mapName = "";
                m_mapDesc = "";
            }
            m_mapName = saga.name();
            m_mapDesc += TranslationHelper::tr("Maps:") + "\n";
            for (int i = 0; i < saga.mapCount(); ++i)
            {
                const auto & header = saga.getMap(i).header;
                m_mapDesc += header.name + "\n";
                if (i==0)
                    m_mapInfo = QString("Author: %1\nSize: %2\nRaces: %3").arg(header.author).arg(header.mapSize).arg("???");
            }
        }
        else
        {
            GameMap  map;
            MapJsonIO::load(map, path);
            m_mapName = map.getMapName();
            m_mapDesc = map.getMapDesc();
        }
    }
    else
    {
        QString path = m_gamePath + "/Exports/" + m_mapListModel->getDesc(selectedMap);
        GameMap::MapInfo mapInfo;
        qDebug()<<"###"<<path;
        if (MapJsonIO::readMapInfo(mapInfo, path))
        {
            qDebug()<<"### opened"<<path;
            m_mapName = mapInfo.name;
            m_mapDesc = mapInfo.desc;
            m_mapImage = "mapPreview";
            GameMap  map;
            MapJsonIO::load(map, path);
            QImage mapImage = MinimapHelper::miniMap(map, RESOLVE(DBFModel));
            RESOLVE(ResourceManager)->addImage(mapImage, "mapPreview");
            auto extInfo = map.objectByIdT<MapInfo>(0);
            m_mapInfo = QString("Author: %1\nSize: %2\nRaces: %3").arg(extInfo.info.creator).arg(mapInfo.size).arg("???");
        }
    }
    emit selectedMapChanged();
}

void MainMenuController::openMap(int index, bool importMap)
{
    if (importMap)
    {
        qDebug()<<Q_FUNC_INFO;
        QString mapPath = m_gamePath + "/Exports/";
        mapPath += m_importListModel->getDesc(index);
        this->importMap(mapPath);
        onGameLoaded(QVariant());
    } else {
        QString mapPath = m_gamePath + "/Exports/";
        mapPath += m_mapListModel->getDesc(index);
        mapPath += "/mapInfo.json";
        RESOLVE(SettingsManager)->setValue("LatestMapFolder", mapPath);
        RESOLVE(CommandBus)->execute(OpenMapCommand(mapPath));
    }
}

void MainMenuController::openMapFromFile()
{
    QString latestPath = RESOLVE(SettingsManager)->get("LatestMapFolder", "");
    QString str = QFileDialog::getOpenFileName(0, "Open map", latestPath, "*.csg *.sg mapInfo.json");
    if (str != "")
    {
        RESOLVE(SettingsManager)->setValue("LatestMapFolder", str);
        RESOLVE(CommandBus)->execute(OpenMapCommand(str));
    }
}

void MainMenuController::selectGamePath()
{
    RESOLVE(CommandBus)->execute(OpenSettingsCommand());
}

void MainMenuController::exit()
{
    QApplication::quit();
}

void MainMenuController::mapTypeChange()
{
    emit selectedMapChanged();
}

QString MainMenuController::about()
{
    return R"(
<html>
    <body style="font-family: Arial, sans-serif; font-size: 12pt; color: black;">
        <p>An open-source map editor for the game <b>Disciples 2</b>, written in <b>Qt</b>, which introduces the following features:</p>
        <ol>
            <li><b>Mana in ruins</b> – Allows the placement of mana in ruins for enhanced gameplay scenarios.</li>
            <li><b>Placement of specific objects</b> – Includes options to position objects like rods and graves on the map.</li>
            <li><b>Hiring limit for units in camps</b> – Sets a cap on the number of units that can be hired from camps.</li>
            <li><b>Overleveling units not at the end of a branch</b> – Enables units in non-final positions of their upgrade tree to exceed standard level caps.</li>
            <li><b>Pre-built capital buildings</b> – Allows some buildings in the capital to start as already constructed.</li>
            <li><b>Using non-quest items in events</b> – Makes it possible to utilize standard items (not limited to quest items) in event triggers.</li>
            <li><b>More than 10 consequences in events</b> – Increases the limit of event consequences beyond the default 10.</li>
        </ol>
        <p>For more information and source code, visit the <a href="https://bitbucket.org/NevendaarTools/d2mapeditorqt">project repository</a>.</p>
    </body>
</html>
)";
}

bool MainMenuController::hasImportedMap(int index)
{
    QString mapPath = m_gamePath + "/Exports/";
    mapPath += m_importListModel->getDesc(index);
    QFileInfo info(mapPath);
    qDebug()<<info.fileName();
    return true;
}



const QString &MainMenuController::getGameName() const
{
    return m_gameName;
}

const QString &MainMenuController::getGamePath() const
{
    return m_gamePath;
}

StringModel *MainMenuController::getMapListModel() const
{
    return m_mapListModel;
}

const QString &MainMenuController::getMapName() const
{
    return m_mapName;
}

const QString &MainMenuController::getMapDesc() const
{
    return m_mapDesc;
}

const QString &MainMenuController::getMapImage() const
{
    return m_mapImage;
}

bool MainMenuController::importMap(const QString &mapPath)
{
    qDebug()<<Q_FUNC_INFO<<mapPath;
    RESOLVE(CommandBus)->execute(OpenMapCommand(mapPath));
    QFileInfo fileInfo(mapPath);
    if (!fileInfo.exists()) {
        qWarning() << "Файл не существует:" << mapPath;
        return false;
    }

    QString baseName = fileInfo.completeBaseName();
    QString parentDir = fileInfo.absolutePath();
    QString newDirName = parentDir + QDir::separator() + baseName + ".nme";

    QDir parentDirectory(parentDir);
    if (!parentDirectory.exists(newDirName)) {
        if (!parentDirectory.mkdir(newDirName)) {
            qWarning() << "Не удалось создать директорию:" << newDirName;
            return false;
        }
    }

    QDir newDirectory(newDirName);
    if (!newDirectory.exists("sounds")) {
        if (!newDirectory.mkdir("sounds")) {
            qWarning() << "Не удалось создать директорию sounds в" << newDirName;
            return false;
        }
    }
    MapJsonIO::save(RESOLVE(MapStateHolder)->map(), newDirName);
    return true;
}

const QString & MainMenuController::editorVersion() const
{
    static QString version = QString("D2MapEditor by Vilgeforc. v%1").arg(EDITOR_VERSION);
    return version;
}

QString MainMenuController::getMapInfo() const
{
    return m_mapInfo;
}

StringModel *MainMenuController::getImportListModel() const
{
    return m_importListModel;
}

void MainMenuController::requestDataView()
{
    RESOLVE(CommandBus)->execute(OpenDataStorageViewCommand());
}

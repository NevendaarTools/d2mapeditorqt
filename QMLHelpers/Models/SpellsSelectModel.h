#ifndef SPELLSSELECTMODEL_H
#define SPELLSSELECTMODEL_H
#include <QAbstractListModel>
#include "QMLHelpers/Common/ITagProvider.h"
#include "TagSelectModel.h"
#include "toolsqt/DBFModel/GameClases.h"

class SpellsTagsProvider: public ITagProvider
{
public:
    void init();
    QList<QString> tagGroups() const
    {
        return m_groupNames;
    }
    QList<QString> availableTags(int groupIndex) const
    {
        return m_groupTags[groupIndex];
    }
    QList<QString> itemTags(const QString &item) const
    {
        return m_itemTags[item];
    }
    bool groupItemsExclusive(int groupIndex) const
    {
        if (groupIndex == 3)//mana
            return true;
        return false;
    }
private:

private:
    QHash<QString, QList<QString>> m_itemTags;
    QList<QList<QString>> m_groupTags;
    QList<QString> m_groupNames;
};

class SpellsSelectModel : public QAbstractListModel
{
    Q_OBJECT
public:
    explicit SpellsSelectModel(QObject *parent = nullptr);
    enum SortMode
    {
        Name = 0,
        Cost
    };
    struct FilterSettings
    {
        QString filter;
        SortMode mode = Name;
        QList<QList<QString>> tags;
    };
    enum Roles
    {
        ROLE_ID = Qt::UserRole + 1,
        ROLE_ICON,
        ROLE_NAME,
        ROLE_DESC,
        ROLE_COST,
        ROLE_RESEARCH_COST,
    };
    Q_PROPERTY(int mode READ sortMode WRITE setSortMode NOTIFY settingsChanged)
    Q_PROPERTY(QString filter READ filter WRITE setFilter NOTIFY settingsChanged)
    Q_PROPERTY(int tagGroupsCount READ tagGroupsCount NOTIFY settingsChanged)

    int rowCount(const QModelIndex & parent = QModelIndex()) const;
    virtual QVariant data(const QModelIndex & index, int role = Qt::DisplayRole) const;
    QHash<int, QByteArray> roleNames() const Q_DECL_OVERRIDE;

    Q_INVOKABLE TagSelectModel* tagModel(int index) const;
    int sortMode() const;
    void setSortMode(int newMode);

    const QString &filter() const;
    void setFilter(const QString &newFilter);

    int tagGroupsCount() const;
public slots:
    void onTagsChanged();
signals:
    void settingsChanged();
private:
    void updateSettings();
private:
    QHash<int, QByteArray> m_roles;

    QVector<QSharedPointer<GSpell>> m_data;
    QVector<QSharedPointer<GSpell>> m_sortedData;
    FilterSettings m_settings;
    QSharedPointer<ITagProvider> m_tagProvider;
    QList<TagSelectModel*> m_tagModels;
};

#endif // SPELLSSELECTMODEL_H

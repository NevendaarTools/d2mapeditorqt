#include "EventsListModel.h"
#include "Engine/GameInstance.h"
#include "Engine/Components/TranslationHelper.h"
#include "Engine/Components/AccessorHolder.h"
#include "Engine/Map/MapStateHolder.h"
#include "QMLHelpers/Components/EventConvertionHelper.h"
#include "toolsqt/MapUtils/DataBlocks/D2Event.h"
#include "toolsqt/MapUtils/DataBlocks/D2Ruin.h"
#include "toolsqt/MapUtils/DataBlocks/D2Village.h"
#include "toolsqt/MapUtils/DataBlocks/D2Merchant.h"


EventsListModel::EventsListModel(QObject *parent)
    : QAbstractListModel{parent}
{
    m_roles[ROLE_NAME] = "Name";
    m_roles[ROLE_DESC] = "Desc";
    m_roles[ROLE_ID] = "Id";
    m_availableEvents = new StringModel();
}

int EventsListModel::rowCount(const QModelIndex &parent) const
{
    if (parent.isValid())
        return 0;
    return m_events.count();
}

QVariant EventsListModel::data(const QModelIndex &index, int role) const
{
    int i = index.row();
    if (i >= m_events.count())
        return "";

    if (role == ROLE_NAME)
    {
        if (m_isConditionType[i])
            return "[?]" + m_events[i].name;
        else
            return "[!]" + m_events[i].name;
    }
    if (role == ROLE_DESC)//todo: add uid to d2event
    {
        QString desc = TranslationHelper::tr("Conditions:");
        for (int k = 0; k < m_events[i].conditions.count(); ++k)
        {
            desc += "\n[" + QString::number(k) + "]    " +
                    TranslationHelper::tr(m_events[i].conditions[k].condition.toString());
            if (m_events[i].conditions[k].targets.count() > 0)
            {
                desc+="\n[ ";
                foreach (auto target , m_events[i].conditions[k].targets)
                {
                    QSharedPointer<MapObject> mapObj =
                            RESOLVE(MapStateHolder)->objectById(target);
                    QSharedPointer<IMapObjectAccessor> accessor =
                            RESOLVE(AccessorHolder)->objectAccessor(target.first);
                    if (!accessor.isNull() && !mapObj.isNull())
                        desc += accessor->getName(mapObj) + QString(" [%1:%2] ").arg(mapObj->x).arg(mapObj->y);
                }
                desc += "]";
            }
        }
        desc += "\n" +  TranslationHelper::tr("Effects:");
        for (int k = 0; k < m_events[i].effects.count(); ++k)
        {
            desc += "\n[" + QString::number(k) + "]    " +
                    TranslationHelper::tr(m_events[i].effects[k].effect.toString());
            if (m_events[i].effects[k].targets.count() > 0)
            {
                desc+="\n[ ";
                foreach (auto target , m_events[i].effects[k].targets)
                {
                    QSharedPointer<MapObject> mapObj =
                            RESOLVE(MapStateHolder)->objectById(target);
                    QSharedPointer<IMapObjectAccessor> accessor =
                            RESOLVE(AccessorHolder)->objectAccessor(target.first);
                    if (!accessor.isNull() && !mapObj.isNull())
                        desc += accessor->getName(mapObj) + QString(" [%1:%2] ").arg(mapObj->x).arg(mapObj->y);
                }
                desc += "]";
            }
        }
        return desc;
    }
    if (role == ROLE_ID)
    {
        return IdToString(m_events[i].uid);
    }
    return QVariant();
}

QHash<int, QByteArray> EventsListModel::roleNames() const
{
    return m_roles;
}

void EventsListModel::init(QPair<int, int> objId, DisplayType type)
{
    beginResetModel();
    m_events.clear();
    m_isConditionType.clear();
    auto events = RESOLVE(MapStateHolder)->collectionByType(MapObject::Event).data.values();
    foreach (const QSharedPointer<MapObject> & eventObj, events)
    {
        EventObject * event = static_cast<EventObject *>(eventObj.data());

        if (type == Conditions || type == Both)
        {
            if (event->conditionTargets().contains(objId))
            {
                m_events << *event;
                m_isConditionType << true;
            }
        }
        if (type == Effects || type == Both)
        {
            if (event->effectTargets().contains(objId))
            {
                m_events << *event;
                m_isConditionType << false;
            }
        }
    }
    endResetModel();
    updateAvailable();
}

QString EventsListModel::objectUID() const
{
    return m_objectUID;
}

void EventsListModel::setObjectUID(const QString &newObjectUID)
{
    if (m_objectUID == newObjectUID)
        return;
    m_objectUID = newObjectUID;
    QStringList data = m_objectUID.split(":");
    if (data.count() < 2)
        return;
    init(QPair<int,int>(data[0].toInt(), data[1].toInt()), Both);
    emit dataChanged();
}

QString EventsListModel::effectName(int eventType) const
{
    if (eventType < 1000)
        return D2Event::EventEffect::toString((D2Event::EventEffect::EffectType)eventType);
    eventType-=1000;
    return D2Event::EventCondition::toString((D2Event::EventCondition::ConditionType)eventType);
}

void EventsListModel::updateAvailable() const
{
    QList<int> result;
    QStringList data = m_objectUID.split(":");
    if (data.count() < 2)
        return;
    MapObject::Type objType = (MapObject::Type)data[0].toInt();
    switch (objType){
    case MapObject::Stack:
    {
        // result << D2Event::EventEffect::CREATE_NEW_STACK;
        result << D2Event::EventEffect::CHANGE_STACK_ORDER;
        result << D2Event::EventEffect::CHANGE_STACK_OWNER;
        result << D2Event::EventEffect::REMOVE_STACK;
        result << D2Event::EventEffect::MOVE_STACK_TO_SPECIFIC_LOCATION;
        result << D2Event::EventEffect::MOVE_STACK_NEXT_TO_TRIGGERER;
        result << D2Event::EventEffect::GO_INTO_BATTLE;

        result << 1000 + D2Event::EventCondition::DESTROY_STACK;
        result << 1000 + D2Event::EventCondition::OWNING_AN_ITEM;
        result << 1000 + D2Event::EventCondition::STACK_EXISTANCE;
        result << 1000 + D2Event::EventCondition::STACK_IN_LOCATION;
        result << 1000 + D2Event::EventCondition::STACK_IN_CITY;

        break;
    }
    case MapObject::StackTemplate:
    case MapObject::LandMark:
    case MapObject::Fort:
    {
        result << 1000 + D2Event::EventCondition::ENTERING_A_CITY;
        result << 1000 + D2Event::EventCondition::OWNING_A_CITY;

        break;
    }
    case MapObject::Mountain:
    case MapObject::Crystal:
    case MapObject::Ruin:
    {
        result << 1000 + D2Event::EventCondition::LOOTING_A_RUIN;

        break;
    }
    case MapObject::Treasure:
    case MapObject::Merchant:
    {
        result << 1000 + D2Event::EventCondition::VISITING_A_SITE;

        break;
    }
    case MapObject::Location:
    case MapObject::Event:
    case MapObject::Unit:
    case MapObject::SubRace:
    case MapObject::Player:
    case MapObject::Info:
    case MapObject::ScenVariable:
    case MapObject::Relation:
    case MapObject::Rod:
    case MapObject::Tomb:
    case MapObject::QuestLine:
    case MapObject::TextLink:
    case MapObject::COUNT:
        break;
    }
    QSharedPointer<IStringDataProvider> provider = m_availableEvents->provider();
    if (provider.isNull())
        provider = QSharedPointer<IStringDataProvider> (new SimpleStringProvider());
    SimpleStringProvider * prov = dynamic_cast<SimpleStringProvider*>(provider.data());
    prov->clear();
    for(auto & resItem: result)
        prov->addRow(TranslationHelper::tr(effectName(resItem)), QString::number(resItem),
                     resItem < 1000?"image://provider/Effect":"image://provider/Condition");
    m_availableEvents->setProvider(provider);
}

StringModel *EventsListModel::availableEvents() const
{
    return m_availableEvents;
}

void EventsListModel::fillEvent(EventObject & event, int eventType, int objType)
{
    switch (objType){
    case MapObject::Stack:
    {
        switch (eventType) {
        case D2Event::EventEffect::CHANGE_STACK_ORDER:
        {
            EventObject::EventEffect effect;
            D2Event::EventEffect d2effect;
            {
                D2Event::EventEffect::ChangeStackOrderEffect effect;
                effect.num = 0;
                effect.stackId = stackIdToD2(m_objectUID);
                effect.orderTarget = "000000";
                effect.firstOnly = true;
                effect.order = 2;
                d2effect.effect = effect;
                d2effect.category = D2Event::EventEffect::CHANGE_STACK_ORDER;
            }
            effect.effect = d2effect;
            effect.targets << IdFromString(m_objectUID);
            event.effects <<effect;
            break;
        }
        case D2Event::EventEffect::CHANGE_STACK_OWNER:
        {
            EventObject::EventEffect effect;
            D2Event::EventEffect d2effect;
            {
                D2Event::EventEffect::ChangeStackOwnerEffect effect;
                effect.num = 0;
                effect.stackId = stackIdToD2(m_objectUID);
                effect.firstOnly = true;
                d2effect.effect = effect;
                d2effect.category = D2Event::EventEffect::CHANGE_STACK_OWNER;
            }
            effect.effect = d2effect;
            effect.targets << IdFromString(m_objectUID);
            event.effects <<effect;

            break;
        }
        case D2Event::EventEffect::REMOVE_STACK:
        {
            EventObject::EventEffect effect;
            D2Event::EventEffect d2effect;
            {
                D2Event::EventEffect::RemoveStackEffect effect;
                effect.num = 0;
                effect.stackId = stackIdToD2(m_objectUID);
                effect.firstOnly = true;
                d2effect.effect = effect;
                d2effect.category = D2Event::EventEffect::REMOVE_STACK;
            }
            effect.effect = d2effect;
            effect.targets << IdFromString(m_objectUID);
            event.effects <<effect;

            break;
        }
        case D2Event::EventEffect::MOVE_STACK_TO_SPECIFIC_LOCATION:
        {
            EventObject::EventEffect effect;
            D2Event::EventEffect d2effect;
            {
                D2Event::EventEffect::MoveStackToLocationEffect effect;
                effect.num = 0;
                effect.stackTmpId = stackIdToD2(m_objectUID);
                d2effect.effect = effect;
                d2effect.category = D2Event::EventEffect::MOVE_STACK_TO_SPECIFIC_LOCATION;
            }
            effect.effect = d2effect;
            effect.targets << IdFromString(m_objectUID);
            event.effects <<effect;

            break;
        }
        case D2Event::EventEffect::MOVE_STACK_NEXT_TO_TRIGGERER:
        {
            EventObject::EventEffect effect;
            D2Event::EventEffect d2effect;
            {
                D2Event::EventEffect::MoveStackToTriggererEffect effect;
                effect.num = 0;
                effect.stackId = stackIdToD2(m_objectUID);
                d2effect.effect = effect;
                d2effect.category = D2Event::EventEffect::MOVE_STACK_NEXT_TO_TRIGGERER;
            }
            effect.effect = d2effect;
            effect.targets << IdFromString(m_objectUID);
            event.effects <<effect;

            break;
        }
        case D2Event::EventEffect::GO_INTO_BATTLE:
        {
            EventObject::EventEffect effect;
            D2Event::EventEffect d2effect;
            {
                D2Event::EventEffect::GoBattleEffect effect;
                effect.num = 0;
                effect.stackId = stackIdToD2(m_objectUID);
                d2effect.effect = effect;
                d2effect.category = D2Event::EventEffect::GO_INTO_BATTLE;
            }
            effect.effect = d2effect;
            effect.targets << IdFromString(m_objectUID);
            event.effects <<effect;

            break;
        }
        case 1000 + D2Event::EventCondition::DESTROY_STACK:
        {
            EventObject::EventCondition effect;
            D2Event::EventCondition d2effect;
            {
                D2Event::EventCondition::ConditionDestroyStack effect;
                effect.stackId = stackIdToD2(m_objectUID);
                d2effect.condition = effect;
                d2effect.category = D2Event::EventCondition::DESTROY_STACK;
            }
            effect.condition = d2effect;
            effect.targets << IdFromString(m_objectUID);
            event.conditions <<effect;

            break;
        }
        case 1000 + D2Event::EventCondition::OWNING_AN_ITEM:
        {
            EventObject::EventCondition effect;
            D2Event::EventCondition d2effect;
            {
                D2Event::EventCondition::ConditionLeaderOwningItem effect;
                effect.stackId = stackIdToD2(m_objectUID);
                d2effect.condition = effect;
                d2effect.category = D2Event::EventCondition::OWNING_AN_ITEM;
            }
            effect.condition = d2effect;
            effect.targets << IdFromString(m_objectUID);
            event.conditions <<effect;

            break;
        }
        case 1000 + D2Event::EventCondition::STACK_EXISTANCE:
        {
            EventObject::EventCondition effect;
            D2Event::EventCondition d2effect;
            {
                D2Event::EventCondition::ConditionStackExist effect;
                effect.stackId = stackIdToD2(m_objectUID);
                d2effect.condition = effect;
                d2effect.category = D2Event::EventCondition::STACK_EXISTANCE;
            }
            effect.condition = d2effect;
            effect.targets << IdFromString(m_objectUID);
            event.conditions <<effect;

            break;
        }
        case 1000 + D2Event::EventCondition::STACK_IN_LOCATION:
        {
            EventObject::EventCondition effect;
            D2Event::EventCondition d2effect;
            {
                D2Event::EventCondition::ConditionStackInLocation effect;
                effect.stackId = stackIdToD2(m_objectUID);
                d2effect.condition = effect;
                d2effect.category = D2Event::EventCondition::STACK_IN_LOCATION;
            }
            effect.condition = d2effect;
            effect.targets << IdFromString(m_objectUID);
            event.conditions <<effect;

            break;
        }
        case 1000 + D2Event::EventCondition::STACK_IN_CITY:
        {
            EventObject::EventCondition effect;
            D2Event::EventCondition d2effect;
            {
                D2Event::EventCondition::ConditionStackInCity effect;
                effect.stackId = stackIdToD2(m_objectUID);
                d2effect.condition = effect;
                d2effect.category = D2Event::EventCondition::STACK_IN_CITY;
            }
            effect.condition = d2effect;
            effect.targets << IdFromString(m_objectUID);
            event.conditions <<effect;

            break;
        }
        default:

            break;
        }
        break;
    }
    case MapObject::StackTemplate:
    case MapObject::LandMark:
    case MapObject::Fort: {
        if (eventType == D2Event::EventCondition::ENTERING_A_CITY + 1000) {
            EventObject::EventCondition effect;
            D2Event::EventCondition d2effect;
            {
                D2Event::EventCondition::ConditionEnterCity effect;
                effect.villageId = stringIdToD2<D2Village>(m_objectUID);
                d2effect.condition = effect;
                d2effect.category = D2Event::EventCondition::ENTERING_A_CITY;
            }
            effect.condition = d2effect;
            effect.targets << IdFromString(m_objectUID);
            event.conditions <<effect;
        }
        if (eventType == D2Event::EventCondition::OWNING_A_CITY + 1000) {
            EventObject::EventCondition effect;
            D2Event::EventCondition d2effect;
            {
                D2Event::EventCondition::ConditionOwningCity effect;
                effect.villageId = stringIdToD2<D2Village>(m_objectUID);
                d2effect.condition = effect;
                d2effect.category = D2Event::EventCondition::OWNING_A_CITY;
            }
            effect.condition = d2effect;
            effect.targets << IdFromString(m_objectUID);
            event.conditions <<effect;
        }
        break;
    }
    case MapObject::Mountain:
    case MapObject::Crystal:
    case MapObject::Ruin: {
        if (eventType == D2Event::EventCondition::LOOTING_A_RUIN + 1000) {
            EventObject::EventCondition effect;
            D2Event::EventCondition d2effect;
            {
                D2Event::EventCondition::ConditionLootingRuin effect;
                effect.ruinId = stringIdToD2<D2Ruin>(m_objectUID);
                d2effect.condition = effect;
                d2effect.category = D2Event::EventCondition::LOOTING_A_RUIN;
            }
            effect.condition = d2effect;
            effect.targets << IdFromString(m_objectUID);
            event.conditions <<effect;
        }
        break;
    }
    case MapObject::Treasure:
    case MapObject::Merchant: {
        if (eventType == D2Event::EventCondition::VISITING_A_SITE + 1000) {
            EventObject::EventCondition effect;
            D2Event::EventCondition d2effect;
            {
                D2Event::EventCondition::ConditionVisitingSite effect;
                effect.siteId = stringIdToD2<D2Merchant>(m_objectUID);
                d2effect.condition = effect;
                d2effect.category = D2Event::EventCondition::ENTERING_A_CITY;
            }
            effect.condition = d2effect;
            effect.targets << IdFromString(m_objectUID);
            event.conditions <<effect;
        }
        break;
    }
    case MapObject::Location:
    case MapObject::Event:
    case MapObject::Unit:
    case MapObject::SubRace:
    case MapObject::Player:
    case MapObject::Info:
    case MapObject::ScenVariable:
    case MapObject::Relation:
    case MapObject::Rod:
    case MapObject::Tomb:
    case MapObject::QuestLine:
    case MapObject::TextLink:
    case MapObject::COUNT:
        break;
    }
}

QString EventsListModel::createEvent(int eventType)
{
    qDebug()<<Q_FUNC_INFO<<eventType;
    QStringList data = m_objectUID.split(":");
    if (data.count() < 2)
        return "";
    MapObject::Type objType = (MapObject::Type)data[0].toInt();

    EventObject obj;
    fillEvent(obj, eventType, objType);
    auto res = RESOLVE(MapStateHolder)->addObject<>(obj);
    QuestLine line = RESOLVE(MapStateHolder)->objectByIdT<QuestLine>(QPair<int, int>(MapObject::QuestLine, 0));
    while (line.stages.count() < 1)
    {
        line.stages << QuestLine::QuestStage();
    }
    line.stages[0].events << res;
    RESOLVE(MapStateHolder)->replaceObject(line);
    return IdToString(res);
}

#ifndef STRINGMODEL_H
#define STRINGMODEL_H

#include <QObject>
#include <QAbstractListModel>
#include <QSharedPointer>
#include "QMLHelpers/Common/IStringDataProvider.h"

class StringModel : public QAbstractListModel
{
    Q_OBJECT
public:
    StringModel(QObject *parent = nullptr);
    enum Roles
    {
        ROLE_NAME = Qt::UserRole + 1,
        ROLE_DESC,
        ROLE_ICON,
        MODEL_DATA,
        ROLE_LABEL
    };

    int rowCount(const QModelIndex & parent = QModelIndex()) const;
    virtual QVariant data(const QModelIndex & index, int role = Qt::DisplayRole) const;
    QHash<int, QByteArray> roleNames() const Q_DECL_OVERRIDE;

    QSharedPointer<IStringDataProvider> provider() const;
    void setProvider(const QSharedPointer<IStringDataProvider> &provider);
    Q_INVOKABLE QString getName(int index) const {return m_provider->getName(index);}
    Q_INVOKABLE QString getDesc(int index) const {return m_provider->getDesc(index);}
    void setSimpleStringList(const QList<QString> & list);
private:
    QHash<int, QByteArray>      m_roles;
    QSharedPointer<IStringDataProvider> m_provider;
};

#endif // STRINGMODEL_H

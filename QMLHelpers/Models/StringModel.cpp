#include "StringModel.h"

StringModel::StringModel(QObject *parent) : QAbstractListModel(parent)
{
    m_roles[ROLE_NAME] = "Name";
    m_roles[ROLE_DESC] = "Desc";
    m_roles[MODEL_DATA] = "modelData";
    m_roles[ROLE_LABEL] = "label";
    m_roles[ROLE_ICON] = "icon";
}

int StringModel::rowCount(const QModelIndex &parent) const
{
    if (m_provider.isNull() || parent.isValid())
        return 0;
    return m_provider->dataCount();
}

QVariant StringModel::data(const QModelIndex &index, int role) const
{
    if (role == ROLE_NAME || role == MODEL_DATA || role == ROLE_LABEL)
    {
        return m_provider->getName(index.row());
    }
    if (role == ROLE_DESC)
    {
        return m_provider->getDesc(index.row());
    }
    if (role == ROLE_ICON)
    {
        return m_provider->getIcon(index.row());
    }
    return QVariant();
}

QHash<int, QByteArray> StringModel::roleNames() const
{
    return m_roles;
}

QSharedPointer<IStringDataProvider> StringModel::provider() const
{
    return m_provider;
}

void StringModel::setProvider(const QSharedPointer<IStringDataProvider> &provider)
{
    beginResetModel();
    m_provider = provider;
    endResetModel();
}

void StringModel::setSimpleStringList(const QList<QString> &list)
{
    auto provider = new SimpleStringProvider();
    foreach (auto str, list) {
        provider->addRow(str, "");
    }
    setProvider(QSharedPointer<SimpleStringProvider>(provider));
}

#include "InventoryModel.h"
#include "Engine/GameInstance.h"
#include "toolsqt/DBFModel/DBFModel.h"

InventoryModel::InventoryModel(QObject *parent) : QAbstractListModel(parent)
{
    m_roles[ROLE_ICON] = "Icon";
    m_roles[ROLE_ICON2] = "Icon2";
    m_roles[ROLE_ID] = "Id";
    m_roles[ROLE_COUNT] = "Count";
    m_roles[ROLE_NAME] = "Name";
    m_roles[ROLE_CHARGES] = "Charges";
}

int InventoryModel::rowCount(const QModelIndex &parent) const
{
    if (parent.isValid())
        return 0;
    return m_inventory.items.count();
}

QVariant InventoryModel::data(const QModelIndex &index, int role) const
{
    int i = index.row();
    if (i >= m_inventory.items.count())
        return "";
    if (role == ROLE_ICON)
    {
        auto item = RESOLVE(DBFModel)->get<GItem>(m_inventory.items[i].type);
        if (item.isNull())
        {
            qDebug()<<"NUll item - "<<m_inventory.items[i].type;
            return "";
        }
        if (!item.isNull() && item->item_cat == 8)
        {
            if (!item->spell_id.isEmpty())
                return "IconSpel-" + item->spell_id.toUpper();
        }
        else
            return "IconItem-" + m_inventory.items[i].type;

    }
    if (role == ROLE_ICON2)
    {
        auto item = RESOLVE(DBFModel)->get<GItem>(m_inventory.items[i].type);
        if (item.isNull())
        {
            qDebug()<<"NUll item - "<<m_inventory.items[i].type;
            return "";
        }
        if (!item.isNull() && item->item_cat == 8)
        {
            return "IconItem-SCROLLHU";
        }
        return "";
    }
    if (role == ROLE_ID)
    {
        return m_inventory.items[i].type;
    }
    if (role == ROLE_COUNT)
    {
        return m_inventory.items[i].count;
    }
    if (role == ROLE_CHARGES)
    {
        return m_inventory.items[i].charges;
    }
    if (role == ROLE_NAME)
    {
        auto item = RESOLVE(DBFModel)->get<GItem>(m_inventory.items[i].type);
        if (item.isNull())
        {
            qDebug()<<"NUll item - "<<m_inventory.items[i].type;
            return "";
        }
        return item->name_txt->text;
    }
    return QVariant();
}

QHash<int, QByteArray> InventoryModel::roleNames() const
{
    return m_roles;
}

const Inventory &InventoryModel::inventory() const
{
    return m_inventory;
}

void InventoryModel::setInventory(const Inventory &newInventory)
{
    beginResetModel();
    m_inventory = newInventory;
    endResetModel();
}

void InventoryModel::setCount(int index, int count)
{
    if (index >= m_inventory.items.count())
        return;
    if (m_inventory.items[index].count == count)
        return;
    if (count == 0)
    {
        beginRemoveRows(QModelIndex(), index, index);
        m_inventory.items.removeAt(index);
        endRemoveRows();
    }
    else
    {
        beginResetModel();
        m_inventory.items[index].count = count;
        endResetModel();
    }
    emit changed();
}

void InventoryModel::setCharges(int index, int count)
{
    if (m_inventory.items[index].charges == count)
        return;
    if (count == 0)
        return;
    beginResetModel();
    m_inventory.items[index].charges = count;
    endResetModel();
    emit changed();
}

void InventoryModel::addItem(const QString &id)
{
    beginResetModel();
    auto item = RESOLVE(DBFModel)->get<GItem>(id);
    if (item->item_cat == GItem::Talisman)
        m_inventory.addItem(id, 1, 5);
    else
        m_inventory.addItem(id);
    endResetModel();
    emit changed();
}

void InventoryModel::removeItem(const QString &id)
{
    beginResetModel();
    m_inventory.removeAll(id);
    endResetModel();
    emit changed();
}

QStringList InventoryModel::toPreset() const
{
    QStringList result;
    for(int i = 0; i < m_inventory.items.count(); ++i)
    {
        QString record = QString("%1:%2").arg(m_inventory.items[i].type)
                  .arg(m_inventory.items[i].count);
        if (m_inventory.items[i].charges != 0)
            record += ":" + QString::number(m_inventory.items[i].charges);
    }
    return result;
}

void InventoryModel::loadPreset(const QStringList &preset)
{
    beginResetModel();
    m_inventory.items.clear();
    for (int i = 0; i < preset.count(); ++i)
    {
        QStringList data = preset[i].split(":");
        if (data.count() < 2)
            continue;
        int charges = 0;
        if (data.count() > 2)
            charges = data[2].toInt();
        m_inventory.addItem(data[0], data[1].toInt(), charges);
    }
    endResetModel();
    emit changed();
}

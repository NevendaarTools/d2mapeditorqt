#ifndef EVENTSLISTMODEL_H
#define EVENTSLISTMODEL_H
#include <QAbstractListModel>
#include "MapObjects/BaseClases.h"
#include "MapObjects/EventObject.h"
#include "StringModel.h"
#include <QObject>

class EventsListModel : public QAbstractListModel
{
    Q_OBJECT
public:
    explicit EventsListModel(QObject *parent = nullptr);
    enum Roles
    {
        ROLE_NAME = Qt::UserRole + 1,
        ROLE_DESC,
        ROLE_ID
    };
    enum DisplayType
    {
        Conditions = 0,
        Effects,
        Both
    };

    int rowCount(const QModelIndex & parent = QModelIndex()) const;
    virtual QVariant data(const QModelIndex & index, int role = Qt::DisplayRole) const;
    QHash<int, QByteArray> roleNames() const Q_DECL_OVERRIDE;
    void init(QPair<int, int> objId, DisplayType type);

    Q_PROPERTY(QString objectUID READ objectUID WRITE setObjectUID NOTIFY dataChanged)
    Q_PROPERTY(StringModel* availableEvents READ availableEvents NOTIFY dataChanged)
    QString objectUID() const;
    void setObjectUID(const QString &newObjectUID);
    Q_INVOKABLE QString effectName(int eventType) const;
    StringModel *availableEvents() const;
public slots:
    QString createEvent(int eventType);
signals:

    void dataChanged();

private:
    void updateAvailable() const;
    void fillEvent(EventObject &event, int eventType, int objType);
private:
    QHash<int, QByteArray> m_roles;
    QList<EventObject> m_events;
    QList<bool> m_isConditionType;
    QString m_objectUID;
    StringModel *m_availableEvents = nullptr;
};

#endif // EVENTSLISTMODEL_H

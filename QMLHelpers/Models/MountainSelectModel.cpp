#include "MountainSelectModel.h"
#include <QRandomGenerator>
#include "Engine/GameInstance.h"
#include "toolsqt/DBFModel/DBFModel.h"

MountainSelectModel::MountainSelectModel(QObject *parent)
{
    m_roles[ROLE_ICON] = "Icon";
    m_roles[ROLE_ID] = "Id";
    m_roles[ROLE_SIZE] = "Size";
    m_filterSettings = new FilterSettingsModel();
    connect(m_filterSettings, SIGNAL(settingsChanged()), this, SLOT(updateSettings()));
}

int MountainSelectModel::rowCount(const QModelIndex &parent) const
{
    if (parent.isValid())
        return 0;
    return m_sortedData.count();
}

QVariant MountainSelectModel::data(const QModelIndex &index, int role) const
{
    int i = index.row();
    if (role == ROLE_ICON)
    {
        return m_sortedData[i].image;
    }
    if (role == ROLE_ID)
    {
        return m_sortedData[i].id;
    }
    if (role == ROLE_SIZE)
    {
        return m_sortedData[i].size;
    }
    return QVariant();
}

QHash<int, QByteArray> MountainSelectModel::roleNames() const
{
    return m_roles;
}

void MountainSelectModel::init()
{
    m_data.clear();
    // m_data << MountainInfo{"RandomMountain", "-1", 0, 0};
    m_data << MountainInfo{"IntfScen-DLG_MAP_BMOUNT_N", "-1", 0, 0};
    auto items = RESOLVE(DBFModel)->getList<GTileDBI>();
    foreach (QSharedPointer<GTileDBI> tile, items)
    {
        if ((tile->terrain == "NE" && tile->ground == "M"))
        {
            if (tile->ndx == 0)
                continue;
            for (int k = 0; k < tile->qty; ++k)
            {
                MountainInfo info;
                info.id = "MOMNE" +
                          QString::number(tile->ndx, 10).rightJustified(2, '0') +
                          QString::number(k, 10).rightJustified(2, '0');
                info.image = "IsoTerrn-" + info.id;
                info.size = tile->ndx;
                info.variant = k;
                m_data << info;
            }
        }
    }
    m_sortedData = m_data;
    m_tagProvider = QSharedPointer<ITagProvider>(new MountainTagsProvider());
    m_filterSettings->setTagProvider(m_tagProvider);
    //m_filterSettings->setModeNames(sortModes);
    updateSettings();
}

void MountainSelectModel::updateSettings()
{
    beginResetModel();
    m_sortedData.clear();
    FilterSettingsModel::FilterSettings settings = m_filterSettings->settings();
    qDebug()<<Q_FUNC_INFO<<settings.tags;
    foreach(const MountainInfo & mountain, m_data)
    {
        if (!acceptTags(m_tagProvider, settings.tags , mountain.id))
            continue;

        m_sortedData.append(mountain);
    }
    endResetModel();
}

FilterSettingsModel *MountainSelectModel::filterSettings() const
{
    return m_filterSettings;
}

QList<MountainInfo> MountainSelectModel::availableRandom() const
{
    QList<MountainInfo> result;
    for(const auto& info: qAsConst(m_sortedData))
        if (info.size == 1)
            result << info;
    return result;
}

MountainInfo MountainSelectModel::info(int index) const
{
    auto result =  m_sortedData[index];
    if (index == 0)
    {
        int index = 0;
        const auto & available = availableRandom();
        if (available.count() > 1) {
            index = QRandomGenerator::global()->bounded(0, available.count() - 1);
        }
        result = available[index];
    }
    return result;
}

void MountainTagsProvider::init()
{
    m_itemTags.clear();
    m_groupTags.clear();
    m_groupNames.clear();

    m_groupNames <<"Size";

    QList<QString> sizes;

    auto items = RESOLVE(DBFModel)->getList<GTileDBI>();
    foreach (QSharedPointer<GTileDBI> tile, items)
    {
        if ((tile->terrain == "NE" && tile->ground == "M"))
        {
            if (tile->ndx == 0)
                continue;
            QString tag = QString("%1 x %1").arg(tile->ndx);
            m_itemTags["-1"]<<tag;
            sizes<<tag;
            for (int k = 0; k < tile->qty; ++k)
            {
                QString id = "MOMNE" +
                             QString::number(tile->ndx, 10).rightJustified(2, '0') +
                             QString::number(k, 10).rightJustified(2, '0');
                m_itemTags[id]<<tag;
            }
        }
    }
    m_groupTags.append(sizes);
}

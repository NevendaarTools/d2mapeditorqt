#ifndef INVENTORYMODEL_H
#define INVENTORYMODEL_H
#include <QAbstractListModel>
#include "MapObjects/BaseClases.h"

class InventoryModel : public QAbstractListModel
{
    Q_OBJECT
public:
    explicit InventoryModel(QObject *parent = nullptr);
    enum Roles
    {
        ROLE_ID = Qt::UserRole + 1,
        ROLE_ICON,
        ROLE_ICON2,
        ROLE_NAME,
        ROLE_CHARGES,
        ROLE_COUNT
    };

    int rowCount(const QModelIndex & parent = QModelIndex()) const;
    virtual QVariant data(const QModelIndex & index, int role = Qt::DisplayRole) const;
    QHash<int, QByteArray> roleNames() const Q_DECL_OVERRIDE;

    const Inventory &inventory() const;
    void setInventory(const Inventory &newInventory);

    Q_INVOKABLE void setCount(int index, int count);
    Q_INVOKABLE void setCharges(int index, int count);
    Q_INVOKABLE void addItem(const QString& id);
    Q_INVOKABLE void removeItem(const QString& id);
    Q_INVOKABLE QStringList toPreset() const;
    Q_INVOKABLE void loadPreset(const QStringList& preset);
signals:
    void changed();
private:
    QHash<int, QByteArray> m_roles;

    Inventory m_inventory;
};

#endif // INVENTORYMODEL_H

#include "UnitSelectModel.h"
#include "toolsqt/DBFModel/DBFModel.h"
#include "Engine/GameInstance.h"

UnitSelectModel::UnitSelectModel(QObject *parent) : QAbstractListModel(parent)
{
    m_data = RESOLVE(DBFModel)->getList<Gunit>();
    m_sortedData = m_data;
    m_roles[ROLE_NAME] = "Name";
    m_roles[ROLE_ICON] = "Icon";
    m_roles[ROLE_ID] = "Id";
    m_roles[ROLE_SMALL] = "Small";
    m_roles[ROLE_LEVEL] = "Level";
    m_roles[ROLE_HP] = "Hp";
    m_roles[ROLE_EXP] = "Exp";
    m_roles[ROLE_EXPKILL] = "ExpKill";
    m_roles[ROLE_CATEGORY] = "Category";
    m_tagProvider = QSharedPointer<ITagProvider>(new UnitTagsProvider());
    m_filterSettings = new FilterSettingsModel();
    m_filterSettings->setTagProvider(m_tagProvider);
    m_filterSettings->setModeNames(QStringList ()<<"Name"<< "Exp"<< "ExpKilled");
    m_filterSettings->setSortMode(SortMode::Name);
    connect(m_filterSettings, SIGNAL(settingsChanged()), this, SLOT(updateSettings()));

    updateSettings();
}

int UnitSelectModel::rowCount(const QModelIndex &parent) const
{
    if (parent.isValid())
        return 0;
    return m_sortedData.count();
}

QVariant UnitSelectModel::data(const QModelIndex &index, int role) const
{
    if (role == ROLE_NAME)
    {
        return m_sortedData.at(index.row())->name_txt.value->text;
    }
    if (role == ROLE_ICON)
    {
        auto gunit = m_sortedData[index.row()];
        QString unitIconId = gunit->base_unit.getKey() + "FACE";
        if (gunit->base_unit.value == nullptr)
            unitIconId = gunit->unit_id + "FACE";
        return "Faces-" + unitIconId;
    }
    if (role == ROLE_ID)
    {
        return m_sortedData.at(index.row())->unit_id;
    }
    if (role == ROLE_SMALL)
    {
        return m_sortedData.at(index.row())->size_small;
    }
    if (role == ROLE_LEVEL)
    {
        return m_sortedData.at(index.row())->level;
    }
    if (role == ROLE_HP)
    {
        return m_sortedData.at(index.row())->hit_point;
    }
    if (role == ROLE_EXP)
    {
        return m_sortedData.at(index.row())->xp_next;
    }
    if (role == ROLE_EXPKILL)
    {
        return m_sortedData.at(index.row())->xp_killed;
    }
    if (role == ROLE_CATEGORY)
    {
        return m_sortedData.at(index.row())->unit_cat->text;
    }
    return QVariant();
}

QHash<int, QByteArray> UnitSelectModel::roleNames() const
{
    return m_roles;
}

bool UnitSelectModel::leader() const
{
    return m_leader;
}

void UnitSelectModel::setLeader(bool newLeader)
{
    if (m_leader == newLeader)
        return;
    m_leader = newLeader;
    updateSettings();
    emit settingsChanged();
}

bool UnitSelectModel::smallOnly() const
{
    return m_smallOnly;
}

void UnitSelectModel::setSmallOnly(bool newSmallOnly)
{
    if (m_smallOnly == newSmallOnly)
        return;
    m_smallOnly = newSmallOnly;
    updateSettings();
    emit settingsChanged();
}

void UnitSelectModel::updateSettings()
{
    beginResetModel();
    m_sortedData.clear();
    FilterSettingsModel::FilterSettings settings = m_filterSettings->settings();
    foreach(auto unit, m_data)
    {
        if (m_leader != (unit->unit_cat.value->text == "L_LEADER" || unit->unit_cat.value->text == "L_NOBLE"))
            continue;
        if (m_showSummons != (unit->unit_cat.value->text == "L_ILLUSION" || unit->unit_cat.value->text == "L_SUMMON"))
            continue;
        if (m_smallOnly && !unit->size_small)
            continue;

        if (!settings.filter.isEmpty())
        {
            if ((!unit->name_txt->text.contains(settings.filter, Qt::CaseInsensitive)) &&
                (!unit->desc_txt->text.contains(settings.filter, Qt::CaseInsensitive)))
            {
                continue;
            }
        }

        if (!acceptTags(m_tagProvider, settings.tags, unit->unit_id))
            continue;

        m_sortedData.append(unit);
    }
    switch((SortMode)settings.mode)
    {
    case UnitSelectModel::Name:
        std::sort(m_sortedData.begin(), m_sortedData.end(),
              [](const QSharedPointer<Gunit> a, const QSharedPointer<Gunit> b) -> bool
            { return a->name_txt.value->text < b->name_txt.value->text; });
        break;
    case UnitSelectModel::Exp:
        std::sort(m_sortedData.begin(), m_sortedData.end(),
              [](const QSharedPointer<Gunit> a, const QSharedPointer<Gunit> b) -> bool
            { return a->xp_next < b->xp_next; });
        break;
    case UnitSelectModel::ExpKill:
        std::sort(m_sortedData.begin(), m_sortedData.end(),
              [](const QSharedPointer<Gunit> a, const QSharedPointer<Gunit> b) -> bool
            { return a->xp_killed < b->xp_killed; });
        break;

    }

    endResetModel();
    emit settingsChanged();
}

FilterSettingsModel *UnitSelectModel::filterSettings() const
{
    return m_filterSettings;
}

bool UnitSelectModel::showSummons() const
{
    return m_showSummons;
}

void UnitSelectModel::setShowSummons(bool newShowSummons)
{
    if (m_showSummons == newShowSummons)
        return;
    m_showSummons = newShowSummons;
    emit settingsChanged();
}

void UnitTagsProvider::init()
{
    m_itemTags.clear();
    m_groupTags.clear();
    m_groupNames.clear();

    m_groupNames <<"Race";
    m_groupNames <<"Area";
    m_groupNames <<"Class";
    m_groupNames <<"Source";

    QList<QString> raceNames;
    QList<QString> attackReaches;
    QList<QString> attackClases;
    QList<QString> attackSources;
    auto units = RESOLVE(DBFModel)->getList<Gunit>();
    foreach (QSharedPointer<Gunit> unit, units)
    {
        QString raceName = unit->subrace->text;
        if (!raceNames.contains(raceName))
            raceNames << raceName;
        m_itemTags[unit->unit_id]<<raceName;
        fillAttack(unit->attack_id.value, unit->unit_id, attackReaches, attackClases, attackSources);
        fillAttack(unit->attack_id->alt_attack.value, unit->unit_id, attackReaches, attackClases, attackSources);
        fillAttack(unit->attack2_id.value, unit->unit_id, attackReaches, attackClases, attackSources, true);
    }
    m_groupTags.append(raceNames);
    m_groupTags.append(attackReaches);
    m_groupTags.append(attackClases);
    m_groupTags.append(attackSources);
}

void UnitTagsProvider::fillAttack(QSharedPointer<Gattack> attack, const QString &id, QList<QString> &attackReaches, QList<QString> &attackClases, QList<QString> &attackSources, bool ignoreReach)
{
    if (attack.isNull())
        return;
    QString attackSource;
    if (attack->source->name_txt.value.isNull())
        attackSource = attack->source->text;
    else
        attackSource = attack->source->name_txt->text;

    QString attackReach = attack->reach->text;
    QString attackClass = attack->clas->text;
    if (!attack->reach->reach_txt.value.isNull())
        attackReach = attack->reach->reach_txt->text;

    //        attackReach.prepend("reach_");
    //        attackClass.prepend("class_");
    //        attackSource.prepend("source_");

    if (!attackReaches.contains(attackReach) && !ignoreReach)
        attackReaches << attackReach;
    if (!attackClases.contains(attackClass))
        attackClases << attackClass;
    if (!attackSources.contains(attackSource))
        attackSources << attackSource;

    if (!ignoreReach)
        m_itemTags[id]<<attackReach;
    m_itemTags[id]<<attackClass;
    m_itemTags[id]<<attackSource;
}

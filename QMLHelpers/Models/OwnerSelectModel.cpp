#include "OwnerSelectModel.h"
#include "MapObjects/Diplomacy.h"
#include "Engine/GameInstance.h"
#include "Engine/Map/MapStateHolder.h"
#include "Engine/Components/TranslationHelper.h"

void OwnerSelectModel::init()
{
    beginResetModel();
    m_data.clear();
    auto fractions = RESOLVE(MapStateHolder)->collectionByType(MapObject::SubRace).data.values();

    foreach (auto fraction, fractions)
    {
        SubRaceObject fractionObj = RESOLVE(MapStateHolder)->objectByIdT<SubRaceObject>(fraction->uid);
        QString name = fractionObj.getName();
        m_data << OwnerSelectModel::OwnerRow
            {
                TranslationHelper::tr(name),
                name.trimmed(),
                MapLink<SubRaceObject>(fraction->uid.second),
                ""
            };//TODO: race images
    }
    endResetModel();
}

void OwnerSelectModel::setOwnerIndexByUid(QPair<int, int> uid)
{
    for (int i = 0; i < m_data.count(); ++i)
    {
        if (m_data[i].id == uid)
        {
            setOwnerIndex(i);
            return;
        }
    }
}

void OwnerSelectModel::setOwnerIndexByText(const QString &text)
{
    for (int i = 0; i < m_data.count(); ++i)
    {
        if (m_data[i].text == text)
        {
            setOwnerIndex(i);
            return;
        }
    }
}

int OwnerSelectModel::rowCount(const QModelIndex &parent) const
{
    if (parent.isValid())
        return 0;
    return m_data.count();
}

QVariant OwnerSelectModel::data(const QModelIndex &index, int role) const
{
    int i = index.row();
    if (role == ROLE_ICON)
    {
        return m_data[i].icon;
    }
    if (role == ROLE_ID)
    {
        return m_data[i].id.second;
    }
    if (role == ROLE_NAME)
    {
        return m_data[i].name;
    }
    return QVariant();
}

QHash<int, QByteArray> OwnerSelectModel::roleNames() const
{
    return m_roles;
}

int OwnerSelectModel::ownerIndex() const
{
    return m_ownerIndex;
}

void OwnerSelectModel::setOwnerIndex(int newOwnerIndex)
{
    if (m_ownerIndex == newOwnerIndex)
        return;
    m_ownerIndex = newOwnerIndex;
    emit changed();
}

MapLink<PlayerObject> OwnerSelectModel::currentOwner() const
{
    SubRaceObject fractionObj = RESOLVE(MapStateHolder)->objectByIdT<SubRaceObject>(m_data[m_ownerIndex].id);
    return fractionObj.playerUid;
}

OwnerSelectModel::OwnerSelectModel(QObject *parent)
    : QAbstractListModel(parent)
{
    m_roles[ROLE_ICON] = "Icon";
    m_roles[ROLE_ID] = "Id";
    m_roles[ROLE_NAME] = "Name";
}

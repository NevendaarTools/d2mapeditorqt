#ifndef OPTIONSMODEL_H
#define OPTIONSMODEL_H

#include <QObject>
#include <QAbstractListModel>
#include "Engine/Option.h"
#include <QStringList>

class OptionsModel : public QAbstractListModel
{
    Q_OBJECT
public:
    explicit OptionsModel(QObject *parent = nullptr);
    enum Roles
    {
        ROLE_NAME = Qt::UserRole + 1,
        ROLE_DESC,
        ROLE_VALUE,
        ROLE_ENABLED,
        ROLE_TYPE
    };

    int rowCount(const QModelIndex & parent = QModelIndex()) const;
    virtual QVariant data(const QModelIndex & index, int role = Qt::DisplayRole) const;
    QHash<int, QByteArray> roleNames() const Q_DECL_OVERRIDE;
    Q_INVOKABLE QStringList getVariants(int index) const;
    Q_INVOKABLE void setValue(int index, const QString& value);
    void setValue(const QString& name, const QString& value);
    QString value(const QString& name);

    void addOption(const Option& option);

    const QList<Option> &options() const;
    void setOptions(const QList<Option> &newOptions);
    void clear();
signals:
    void optionChanged(const QString& name);
private:
    QList<Option> m_options;
    QHash<int, QByteArray> m_roles;
};

#endif // OPTIONSMODEL_H

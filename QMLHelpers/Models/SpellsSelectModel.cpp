#include "SpellsSelectModel.h"
#include "Engine/GameInstance.h"
#include "toolsqt/DBFModel/DBFModel.h"
#include "MapObjects/MapObject.h"

SpellsSelectModel::SpellsSelectModel(QObject *parent)
    : QAbstractListModel{parent}
{
    m_data = RESOLVE(DBFModel)->getList<GSpell>();
    m_sortedData = m_data;
    m_roles[ROLE_ICON] = "Icon";
    m_roles[ROLE_ID] = "Id";
    m_roles[ROLE_NAME] = "Name";
    m_roles[ROLE_DESC] = "Desc";
    m_roles[ROLE_COST] = "Cost";
    m_roles[ROLE_RESEARCH_COST] = "ReserchCost";
    m_tagProvider = QSharedPointer<ITagProvider>(new SpellsTagsProvider());
    m_tagProvider->init();
    QList<QString> groups = m_tagProvider->tagGroups();
    for (int i = 0; i < groups.count(); ++i)
    {
        m_settings.tags <<QList<QString>();
        TagSelectModel * model = new TagSelectModel(this);
        model->init(m_tagProvider, i);
        connect(model, SIGNAL(settingsChanged()), this, SLOT(onTagsChanged()));
        m_tagModels<<model;
    }
    m_settings.mode = SortMode::Name;
    updateSettings();
}

int SpellsSelectModel::rowCount(const QModelIndex &parent) const
{
    if (parent.isValid())
        return 0;
    return m_sortedData.count();
}

QVariant SpellsSelectModel::data(const QModelIndex &index, int role) const
{
    int i = index.row();
    if (role == ROLE_ICON)
    {
        auto item = m_sortedData[i];
        return "IconSpel-" + item->spell_id.toUpper();
    }
    if (role == ROLE_ID)
    {
        auto item = m_sortedData[i];
        return item->spell_id;
    }
    if (role == ROLE_NAME)
    {
        auto item = m_sortedData[i];
        return item->name_txt->text;
    }
    if (role == ROLE_DESC)
    {
        auto item = m_sortedData[i];
        return item->desc_txt->text;
    }
    if (role == ROLE_COST)
    {
        auto item = m_sortedData[i];
        return item->casting_c;
    }
    if (role == ROLE_RESEARCH_COST)
    {
        auto item = m_sortedData[i];
        return item->casting_c;
    }
    return QVariant();
}

QHash<int, QByteArray> SpellsSelectModel::roleNames() const
{
    return m_roles;
}

TagSelectModel *SpellsSelectModel::tagModel(int index) const
{
    return m_tagModels[index];
}

int SpellsSelectModel::sortMode() const
{
    return m_settings.mode;
}

void SpellsSelectModel::setSortMode(int newMode)
{
    if (m_settings.mode == newMode)
        return;
    m_settings.mode = (SortMode)newMode;
    updateSettings();
    emit settingsChanged();
}

const QString &SpellsSelectModel::filter() const
{
    return m_settings.filter;
}

void SpellsSelectModel::setFilter(const QString &newFilter)
{
    if (m_settings.filter == newFilter)
        return;
    m_settings.filter = newFilter;
    updateSettings();
    emit settingsChanged();
}

int SpellsSelectModel::tagGroupsCount() const
{
    return m_tagModels.count();
}

void SpellsSelectModel::onTagsChanged()
{
    for (int i = 0; i < m_tagModels.count(); ++i)
    {
        m_settings.tags[i] = m_tagModels[i]->selectedTags();
    }
    updateSettings();
}

void SpellsSelectModel::updateSettings()
{
    beginResetModel();
    m_sortedData.clear();
    foreach(QSharedPointer<GSpell> spell, m_data)
    {
        if (!m_settings.filter.isEmpty())
        {
            if ((!spell->name_txt->text.contains(m_settings.filter, Qt::CaseInsensitive)) &&
                (!spell->desc_txt->text.contains(m_settings.filter, Qt::CaseInsensitive)))
            {
                continue;
            }
        }

        if (!acceptTags(m_tagProvider, m_settings.tags ,spell->spell_id))
            continue;

        m_sortedData.append(spell);
    }

    switch(m_settings.mode)
    {
    case SortMode::Name:
        std::sort(m_sortedData.begin(), m_sortedData.end(),
              [](const QSharedPointer<GSpell> a, const QSharedPointer<GSpell> b) -> bool
            { return a->name_txt.value->text < b->name_txt.value->text; });
        break;
    case SortMode::Cost:
        std::sort(m_sortedData.begin(), m_sortedData.end(),
              [](const QSharedPointer<GSpell> a, const QSharedPointer<GSpell> b) -> bool
            { return Currency::fromString(a->buy_c).totalCount()
                    < Currency::fromString(b->buy_c).totalCount(); });
        break;
    }

    endResetModel();
}


void SpellsTagsProvider::init()
{
    m_itemTags.clear();
    m_groupTags.clear();
    m_groupNames.clear();

    m_groupNames <<"Level";
    m_groupNames <<"Race";
    m_groupNames <<"Category";
    m_groupNames <<"Mana";

    QList<QString> spellLevels;
    QList<QString> spellRaces;
    QList<QString> spellCats;
    QList<QString> spellMana;
    spellMana<<"MANA_GOLD";
    spellMana<<"MANA_DEMONS";
    spellMana<<"MANA_EMPIRE";
    spellMana<<"MANA_UNDEAD";
    spellMana<<"MANA_CLANS";
    spellMana<<"MANA_ELVES";
    for (int i = 0; i < 5; ++i)
    {
        spellLevels << "SpellLevel_" + QString::number(i + 1);
    }
    auto items = RESOLVE(DBFModel)->getList<GSpell>();
    foreach (QSharedPointer<GSpell> spell, items)
    {
        QString spellLevel = "SpellLevel_" + QString::number(spell->level);
        if (!spellLevels.contains(spellLevel))
            spellLevels << spellLevel;
        m_itemTags[spell->spell_id]<<spellLevel;

        QString spellCat = "SpellCategory_" + QString::number(spell->category);
        if (!spellCats.contains(spellCat))
            spellCats << spellCat;
        m_itemTags[spell->spell_id]<<spellCat;

        auto spellLord = RESOLVE(DBFModel)->get<GspellR>(spell->spell_id);
        if (!spellLord.isNull())
        {
            auto lord = RESOLVE(DBFModel)->get<Glord>(spellLord->lord_id);
            if (!lord.isNull())
            {
                QString race = lord->race_id->name_txt->text;
                if (!spellRaces.contains(race))
                    spellRaces << race;
                m_itemTags[spell->spell_id]<<race;
            }
        }
        Currency cost = Currency::fromString(spell->casting_c);
        if (cost.gold > 0) m_itemTags[spell->spell_id]<<"MANA_GOLD";
        if (cost.demons > 0) m_itemTags[spell->spell_id]<<"MANA_DEMONS";
        if (cost.empire > 0) m_itemTags[spell->spell_id]<<"MANA_EMPIRE";
        if (cost.undead > 0) m_itemTags[spell->spell_id]<<"MANA_UNDEAD";
        if (cost.clans > 0) m_itemTags[spell->spell_id]<<"MANA_CLANS";
        if (cost.elves > 0) m_itemTags[spell->spell_id]<<"MANA_ELVES";

    }
    m_groupTags.append(spellLevels);
    m_groupTags.append(spellRaces);
    m_groupTags.append(spellCats);
    m_groupTags.append(spellMana);
}

#include "TreeSelectModel.h"
#include "Engine/MapView/MapTileHelper.h"
#include "toolsqt/DBFModel/DBFModel.h"
#include "Engine/GameInstance.h"

TreeSelectModel::TreeSelectModel(QObject *parent)
{
    m_roles[ROLE_ICON] = "Icon";
    m_roles[ROLE_ID] = "Id";
    m_roles[ROLE_SIZE] = "TreeSize";
    m_roles[ROLE_BACK] = "Back";
    m_filterSettings = new FilterSettingsModel();
    connect(m_filterSettings, SIGNAL(settingsChanged()), this, SLOT(updateSettings()));
}

int TreeSelectModel::rowCount(const QModelIndex &parent) const
{
    if (parent.isValid())
        return 0;
    return m_sortedData.count();
}

QVariant TreeSelectModel::data(const QModelIndex &index, int role) const
{
    int i = index.row();
    if (role == ROLE_ICON)
    {
        return m_sortedData[i].image;
    }
    if (role == ROLE_ID)
    {
        return m_sortedData[i].id;
    }
    if (role == ROLE_SIZE)
    {
        return m_sortedData[i].size;
    }
    if (role == ROLE_BACK)
    {
        return "PalMap-ISO" + m_sortedData[i].shortRace;
    }
    return QVariant();
}

QHash<int, QByteArray> TreeSelectModel::roleNames() const
{
    return m_roles;
}

void TreeSelectModel::init()
{
    m_data.clear();
    // m_data << TreeInfo{"RandomTree", "-1", 0, 0};
    m_data << TreeInfo{"IntfScen-DLG_MAP_FOR_N", "-1", 0, 0};
    auto items = RESOLVE(DBFModel)->getList<GTileDBI>();
    foreach (QSharedPointer<GTileDBI> tile, items)
    {
        if (tile->ground == "F")
        {
            for (int k = 0; k < tile->qty; ++k)
            {
                TreeInfo info;
                info.id = tile->terrain + "F" +
                          QString::number(k, 10).rightJustified(4, '0');
                info.image = "IsoTerrn-" + info.id;
                info.size = tile->ndx;
                info.variant = k;
                info.shortRace = tile->terrain;
                m_data << info;
            }
        }
    }
    m_sortedData = m_data;
    m_tagProvider = QSharedPointer<ITagProvider>(new TreeTagsProvider());
    m_filterSettings->setTagProvider(m_tagProvider);
    //m_filterSettings->setModeNames(sortModes);
    updateSettings();
    m_shotRaces.clear();
    auto races = RESOLVE(DBFModel)->getList<Lterrain>();
    for(QSharedPointer<Lterrain> race : races)
    {
        m_shotRaces.insert(race->text.mid(2), race->id);
    }
}

void TreeSelectModel::updateSettings()
{
    beginResetModel();
    m_sortedData.clear();
    FilterSettingsModel::FilterSettings settings = m_filterSettings->settings();
    foreach(const TreeInfo & mountain, m_data)
    {
        if (!acceptTags(m_tagProvider, settings.tags , mountain.id))
            continue;

        m_sortedData.append(mountain);
    }
    endResetModel();
}

QString TreeSelectModel::shotRaceByInt(int value) const
{
    QHashIterator<QString, int> i(m_shotRaces);
    while (i.hasNext()) {
        i.next();
        if (i.value() == value)
            return i.key();
    }
    return "";
}

int TreeSelectModel::intByShotRace(const QString &text) const
{
    return m_shotRaces[text];
}

FilterSettingsModel *TreeSelectModel::filterSettings() const
{
    return m_filterSettings;
}

QSet<int> TreeSelectModel::availableForTile(int8_t tile) const
{
    QString shortRace = shotRaceByInt(tile);
    QSet<int> result;
    for(const TreeInfo & tree: m_data)
        if (tree.shortRace == shortRace)
            result << tree.variant;
    return result;
}

TreeInfo TreeSelectModel::info(int index) const
{
    return m_sortedData[index];
}

void TreeTagsProvider::init()
{
    m_itemTags.clear();
    m_groupTags.clear();
    m_groupNames.clear();

    m_groupNames <<"Race";

    QList<QString> races;

    auto items = RESOLVE(DBFModel)->getList<GTileDBI>();
    foreach (QSharedPointer<GTileDBI> tile, items)
    {
        if (tile->ground == "F")
        {
            QString tag = tile->terrain;
            if (!races.contains(tag))
            {
                races << tag;
                m_itemTags["-1"]<<tag;
            }
            for (int k = 0; k < tile->qty; ++k)
            {
                QString id = tile->terrain + "F" +
                             QString::number(k, 10).rightJustified(4, '0');
                m_itemTags[id]<<tag;
            }
        }
    }
    m_groupTags.append(races);
}

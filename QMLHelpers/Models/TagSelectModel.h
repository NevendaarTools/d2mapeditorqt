#ifndef TAGSELECTMODEL_H
#define TAGSELECTMODEL_H

#include <QAbstractListModel>
#include "QMLHelpers/Common/ITagProvider.h"
#include <QSharedPointer>

class TagSelectModel : public QAbstractListModel
{
    Q_OBJECT
public:
    explicit TagSelectModel(QObject *parent = nullptr);
    enum Roles
    {
        ROLE_NAME = Qt::UserRole + 1,
        ROLE_CHECKED,
    };
    int rowCount(const QModelIndex & parent = QModelIndex()) const;
    virtual QVariant data(const QModelIndex & index, int role = Qt::DisplayRole) const;
    QHash<int, QByteArray> roleNames() const Q_DECL_OVERRIDE;
    Q_PROPERTY(QString groupName READ groupName NOTIFY settingsChanged)
    Q_PROPERTY(bool hasSelected READ hasSelected NOTIFY settingsChanged)

    Q_INVOKABLE void setChecked(int index, bool checked);
    Q_INVOKABLE void setInversed(int index);
    Q_INVOKABLE void setExclusive(int index);
    Q_INVOKABLE void clearTags();
    const QList<QString> &selectedTags() const;
    void setSelectedTags(const QList<QString> &newSelectedTags);

    QString groupName() const;

    void init(QSharedPointer<ITagProvider> tagProvider, int index);
    bool hasSelected() const;

signals:
    void settingsChanged();

private:
    QHash<int, QByteArray> m_roles;
    QSharedPointer<ITagProvider> m_tagProvider;
    int m_groupIndex;
    QList<QString> m_selectedTags;
};

#endif // TAGSELECTMODEL_H

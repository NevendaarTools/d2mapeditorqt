#ifndef TREESELECTMODEL_H
#define TREESELECTMODEL_H
#include <QObject>
#include <QAbstractListModel>
#include "QMLHelpers/Common/ITagProvider.h"
#include "TagSelectModel.h"
#include "FilterSettingsModel.h"

class TreeTagsProvider: public ITagProvider
{
public:
    void init();
    QList<QString> tagGroups() const
    {
        return m_groupNames;
    }
    QList<QString> availableTags(int groupIndex) const
    {
        return m_groupTags[groupIndex];
    }
    QList<QString> itemTags(const QString &item) const
    {
        return m_itemTags[item];
    }
    virtual bool groupItemsExclusive(int groupIndex) const override
    {
        Q_UNUSED(groupIndex)
        return false;
    }
private:

private:
    QHash<QString, QList<QString>> m_itemTags;
    QList<QList<QString>> m_groupTags;
    QList<QString> m_groupNames;
};

struct TreeInfo
{
    QString image;
    QString id;
    int size;
    int variant;
    QString shortRace;
};

class TreeSelectModel : public QAbstractListModel
{
    Q_OBJECT
public:
    TreeSelectModel(QObject *parent = nullptr);

    struct FilterSettings
    {
        QList<QList<QString>> tags;
    };
    enum Roles
    {
        ROLE_ID = Qt::UserRole + 1,
        ROLE_ICON,
        ROLE_SIZE,
        ROLE_BACK
    };
    Q_PROPERTY(FilterSettingsModel * filterSettings READ filterSettings NOTIFY settingsChanged)

    void init();
    int rowCount(const QModelIndex & parent = QModelIndex()) const;
    virtual QVariant data(const QModelIndex & index, int role = Qt::DisplayRole) const;
    QHash<int, QByteArray> roleNames() const Q_DECL_OVERRIDE;
    FilterSettingsModel *filterSettings() const;
    QSet<int> availableForTile(int8_t tile) const;
    TreeInfo info(int index) const;
    QString shotRaceByInt(int value) const;
    int intByShotRace(const QString& text) const;
signals:
    void settingsChanged();
private slots:
    void updateSettings();
private:
private:
    QHash<int, QByteArray> m_roles;
    QHash<QString, int> m_shotRaces;

    QVector<TreeInfo> m_data;
    QVector<TreeInfo> m_sortedData;
    FilterSettings m_settings;
    QSharedPointer<ITagProvider> m_tagProvider;
    FilterSettingsModel *m_filterSettings;
};

#endif // TREESELECTMODEL_H

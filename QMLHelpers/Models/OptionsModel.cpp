#include "OptionsModel.h"
#include <QDebug>

OptionsModel::OptionsModel(QObject *parent)
    : QAbstractListModel{parent}
{
    m_roles[ROLE_NAME] = "Name";
    m_roles[ROLE_DESC] = "Desc";
    m_roles[ROLE_VALUE] = "Value";
    m_roles[ROLE_ENABLED] = "Enabled";
    m_roles[ROLE_TYPE] = "OptionType";
}

int OptionsModel::rowCount(const QModelIndex &parent) const
{
    if (parent.isValid())
        return 0;
    return m_options.count();
}

QVariant OptionsModel::data(const QModelIndex &index, int role) const
{
    int i = index.row();
    if (role == ROLE_NAME)
    {
        return m_options[i].name;
    }
    if (role == ROLE_DESC)
    {
        return m_options[i].desc;
    }
    if (role == ROLE_VALUE)
    {
        return m_options[i].value;
    }
    if (role == ROLE_ENABLED)
    {
        return m_options[i].status != Option::Disabled;
    }
    if (role == ROLE_TYPE)
    {
        return (int)m_options[i].type;
    }

    return QVariant();
}

QHash<int, QByteArray> OptionsModel::roleNames() const
{
    return m_roles;
}

QStringList OptionsModel::getVariants(int index) const
{
    return m_options[index].variants;
}

void OptionsModel::setValue(int index, const QString &value)
{
    if (m_options[index].value.trimmed() == value.trimmed())
        return;
    m_options[index].value = value;
    emit dataChanged(this->index(index,0), this->index(index,0));
    emit optionChanged(m_options[index].name);
}

void OptionsModel::setValue(const QString &name, const QString &value)
{
    for (int i = 0; i < m_options.count(); ++i)
    {
        if (m_options[i].name == name)
        {
            setValue(i, value);
            return;
        }
    }
}

QString OptionsModel::value(const QString &name)
{
    for (int i = 0; i < m_options.count(); ++i)
    {
        if (m_options[i].name == name)
        {
            return m_options[i].value;
        }
    }
    return "";
}

void OptionsModel::addOption(const Option &option)
{
    beginResetModel();
    m_options.append(option);
    endResetModel();
}

const QList<Option> &OptionsModel::options() const
{
    return m_options;
}

void OptionsModel::setOptions(const QList<Option> &newOptions)
{
    beginResetModel();
    m_options = newOptions;
    endResetModel();
}

void OptionsModel::clear()
{
    beginResetModel();
    m_options.clear();
    endResetModel();
}

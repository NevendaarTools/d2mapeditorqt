#ifndef UNITSELECTMODEL_H
#define UNITSELECTMODEL_H
#include <QAbstractListModel>
#include "QMLHelpers/Common/ITagProvider.h"
#include "TagSelectModel.h"
#include "FilterSettingsModel.h"
#include "toolsqt/DBFModel/GameClases.h"

class UnitTagsProvider: public ITagProvider
{
public:
    void init();
    QList<QString> tagGroups() const
    {
        return m_groupNames;
    }
    QList<QString> availableTags(int groupIndex) const
    {
        return m_groupTags[groupIndex];
    }
    QList<QString> itemTags(const QString &item) const
    {
        return m_itemTags[item];
    }
    bool groupItemsExclusive(int groupIndex) const
    {
        Q_UNUSED(groupIndex);
        return false;
    }
private:
    void fillAttack(QSharedPointer<Gattack> attack, const QString & id,
                    QList<QString> & attackReaches,
                    QList<QString> & attackClases,
                    QList<QString> & attackSources,
                    bool ignoreReach = false);
private:
    QHash<QString, QList<QString>> m_itemTags;
    QList<QList<QString>> m_groupTags;
    QList<QString> m_groupNames;
};

class UnitSelectModel : public QAbstractListModel
{
    Q_OBJECT
public:
    enum Roles
    {
        ROLE_NAME = Qt::UserRole + 1,
        ROLE_ICON,
        ROLE_ID,
        ROLE_SMALL,
        ROLE_LEVEL,
        ROLE_HP,
        ROLE_EXP,
        ROLE_EXPKILL,
        ROLE_CATEGORY
    };

    enum SortMode
    {
        Name = 0,
        Exp,
        ExpKill
    };

    explicit UnitSelectModel(QObject *parent = nullptr);

    int rowCount(const QModelIndex & parent = QModelIndex()) const;
    virtual QVariant data(const QModelIndex & index, int role = Qt::DisplayRole) const;
    QHash<int, QByteArray> roleNames() const Q_DECL_OVERRIDE;
    Q_PROPERTY(FilterSettingsModel * filterSettings READ filterSettings NOTIFY settingsChanged)
    Q_PROPERTY(bool leader READ leader WRITE setLeader NOTIFY settingsChanged)
    Q_PROPERTY(bool smallOnly READ smallOnly WRITE setSmallOnly NOTIFY settingsChanged)
    Q_PROPERTY(bool showSummons READ showSummons WRITE setShowSummons NOTIFY settingsChanged)

    bool leader() const;
    void setLeader(bool newLeader);

    bool smallOnly() const;
    void setSmallOnly(bool newSmallOnly);
    FilterSettingsModel *filterSettings() const;

    bool showSummons() const;
    void setShowSummons(bool newShowSummons);

public slots:

signals:
    void settingsChanged();

private slots:
    void updateSettings();
private:
    QHash<int, QByteArray> m_roles;
    bool m_smallOnly = false;
    bool m_leader = false;
    bool m_showSummons = false;

    QVector<QSharedPointer<Gunit>> m_data;
    QVector<QSharedPointer<Gunit>> m_sortedData;
    QSharedPointer<ITagProvider> m_tagProvider;
    FilterSettingsModel *m_filterSettings;
};

#endif // UNITSELECTMODEL_H

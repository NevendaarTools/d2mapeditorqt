#include "ItemSelectModel.h"
#include "Engine/GameInstance.h"
#include "toolsqt/DBFModel/DBFModel.h"
#include "MapObjects/MapObject.h"

ItemSelectModel::ItemSelectModel(QObject *parent): QAbstractListModel(parent)
{
    m_data = RESOLVE(DBFModel)->getList<GItem>();
    m_sortedData = m_data;
    m_roles[ROLE_ICON] = "Icon";
    m_roles[ROLE_ICON2] = "Icon2";
    m_roles[ROLE_ID] = "Id";
    m_roles[ROLE_NAME] = "Name";
    m_roles[ROLE_DESC] = "Desc";
    m_roles[ROLE_COST] = "Cost";
    m_tagProvider = QSharedPointer<ITagProvider>(new ItemTagsProvider());
    m_filterSettings = new FilterSettingsModel();
    m_filterSettings->setTagProvider(m_tagProvider);
    m_filterSettings->setModeNames(QStringList ()<<"Name"<< "Cost");
    m_filterSettings->setSortMode(SortMode::Name);
    connect(m_filterSettings, SIGNAL(settingsChanged()), this, SLOT(updateSettings()));
    updateSettings();
}

int ItemSelectModel::rowCount(const QModelIndex &parent) const
{
    if (parent.isValid())
        return 0;
    return m_sortedData.count();
}

QVariant ItemSelectModel::data(const QModelIndex &index, int role) const
{
    int i = index.row();
    if (role == ROLE_ICON)
    {
        auto item = m_sortedData[i];
        if (item->item_cat == 8)
        {
            if (!item->spell_id.isEmpty())
                return "IconSpel-" + item->spell_id.toUpper();
        }
        else
            return "IconItem-" + item->item_id.toUpper();

    }
    if (role == ROLE_ICON2)
    {
        auto item = m_sortedData[i];
        if (item->item_cat == 8)
        {
            return "IconItem-SCROLLHU";
        }
        return "";
    }
    if (role == ROLE_ID)
    {
        auto item = m_sortedData[i];
        return item->item_id;
    }
    if (role == ROLE_NAME)
    {
        auto item = m_sortedData[i];
        return item->name_txt->text;
    }
    if (role == ROLE_DESC)
    {
        auto item = m_sortedData[i];
        QString resDesc = item->desc_txt->text;
        resDesc.replace("\\n","<br>");
        if (resDesc.length() > 120)
            resDesc = resDesc.left(120) + "...";
        return resDesc;
    }
    if (role == ROLE_COST)
    {
        auto item = m_sortedData[i];
        return item->value;
    }
    return QVariant();
}

QHash<int, QByteArray> ItemSelectModel::roleNames() const
{
    return m_roles;
}

void ItemSelectModel::updateSettings()
{
    beginResetModel();
    m_sortedData.clear();
    FilterSettingsModel::FilterSettings settings = m_filterSettings->settings();
    foreach(auto unit, m_data)
    {
        if (!settings.filter.isEmpty())
        {
            if ((!unit->name_txt->text.contains(settings.filter, Qt::CaseInsensitive)) &&
                (!unit->desc_txt->text.contains(settings.filter, Qt::CaseInsensitive)))
            {
                continue;
            }
        }

        if (!acceptTags(m_tagProvider, settings.tags, unit->item_id))
            continue;

        m_sortedData.append(unit);
    }

    switch(settings.mode)
    {
    case ItemSelectModel::Name:
        std::sort(m_sortedData.begin(), m_sortedData.end(),
              [](const QSharedPointer<GItem> a, const QSharedPointer<GItem> b) -> bool
            { return a->name_txt.value->text < b->name_txt.value->text; });
        break;
    case ItemSelectModel::Cost:
        std::sort(m_sortedData.begin(), m_sortedData.end(),
              [](const QSharedPointer<GItem> a, const QSharedPointer<GItem> b) -> bool
            { return Currency::fromString(a->value).gold < Currency::fromString(b->value).gold; });
        break;
    }

    endResetModel();
}


FilterSettingsModel *ItemSelectModel::filterSettings() const
{
    return m_filterSettings;
}

void ItemTagsProvider::init()
{
    m_itemTags.clear();
    m_groupTags.clear();
    m_groupNames.clear();

    m_groupNames <<"Category";

    QList<QString> itemTypes;
    auto items = RESOLVE(DBFModel)->getList<GItem>();
    foreach (QSharedPointer<GItem> item, items)
    {
        QString itemType = "ItemCategory_" + QString::number(item->item_cat);
        if (itemType == "ItemCategory_6")
            itemType = "ItemCategory_5";
        if (itemType == "ItemCategory_0")
            itemType = "ItemCategory_2";
        if (!itemTypes.contains(itemType))
            itemTypes << itemType;
        m_itemTags[item->item_id]<<itemType;
    }
    m_groupTags.append(itemTypes);
}

#ifndef SELECTMAPOBJECTMODEL_H
#define SELECTMAPOBJECTMODEL_H

#include <QAbstractListModel>
#include "toolsqt/DBFModel/GameClases.h"
#include "QMLHelpers/Common/ITagProvider.h"
#include "TagSelectModel.h"
#include "FilterSettingsModel.h"
#include "Engine/Map/MapStateHolder.h"
#include "Engine/GameInstance.h"

class SelectMapObjectModel : public QAbstractListModel
{
    Q_OBJECT
public:
    enum Roles
    {
        ROLE_ID = Qt::UserRole + 1,
        ROLE_TEXT
    };
    struct FilterSettings
    {
        QString filter;
        QList<QList<QString>> tags;
    };
    explicit SelectMapObjectModel(QObject *parent = nullptr);
    Q_PROPERTY(int targetIndex READ targetIndex WRITE setTargetIndex NOTIFY targetChanged)
    Q_PROPERTY(FilterSettingsModel * filterSettings READ filterSettings NOTIFY settingsChanged)

    int rowCount(const QModelIndex & parent = QModelIndex()) const;
    virtual QVariant data(const QModelIndex & index, int role = Qt::DisplayRole) const;
    QHash<int, QByteArray> roleNames() const Q_DECL_OVERRIDE;

    Q_INVOKABLE QString selectedId () const;

    template <class classT>
    void init(std::function<QString(const QSharedPointer<MapObject>& item)> getName,
              std::function<QPair<int, int>(const QSharedPointer<MapObject>& item)> getId,
              std::function<bool(const QSharedPointer<MapObject>& item)> accept)
    {
        m_data.clear();
        m_getNameFunction = getName;
        m_getIdFunction = getId;
        QList<classT> list = RESOLVE(MapStateHolder)->objectsByType<classT>();
        qDebug()<<Q_FUNC_INFO<<"total = "<<list.count();
        for (int i = 0; i < list.count(); ++i)
        {
            classT * newT = new classT(list[i]);
            auto pointer = QSharedPointer<MapObject>(newT);
            if (!accept(pointer))
                continue;
            m_data << pointer;
        }
        std::sort(m_data.begin(), m_data.end(),
                [](const QSharedPointer<MapObject>& a, const QSharedPointer<MapObject>& b) {
                    return a->uid.second < b->uid.second;
                });
        qDebug()<<Q_FUNC_INFO<<"result = "<<m_data.count();

        updateSettings();
    }

    template <class classT>
    void init(std::function<QString(const QSharedPointer<MapObject>& item)> getName,
              std::function<QPair<int, int>(const QSharedPointer<MapObject>& item)> getId)
    {
        init<classT>(getName, getId, [](const QSharedPointer<MapObject>& item){return true;});
    }

    template <class classT>
    void init(std::function<QString(const QSharedPointer<MapObject>& item)> getName,
              std::function<QPair<int, int>(const QSharedPointer<MapObject>& item)> getId,
              QList<QSharedPointer<MapObject>> data)
    {
        m_data = data;
        std::sort(m_data.begin(), m_data.end(),
                [](const QSharedPointer<MapObject>& a, const QSharedPointer<MapObject>& b) {
                    return a->uid.second < b->uid.second;
                });
        m_getNameFunction = getName;
        m_getIdFunction = getId;
        updateSettings();
    }


    int targetIndex() const;
    void setTargetIndex(int newTargetIndex);
    void setTagProvider(QSharedPointer<ITagProvider> newTagProvider, const QStringList & sortModes);

    FilterSettingsModel *filterSettings() const;
    void refresh();
    QSharedPointer<MapObject> objectAtIndex(int index) const;
    void updateAtIndex(int itemIndex);
    void updateWithId(const QPair<int, int> &id);
    void saveData();
signals:
    void targetChanged();
    void settingsChanged();
private slots:
    void updateSettings();

private:
    QHash<int, QByteArray> m_roles;
    QList<QSharedPointer<MapObject>> m_data;
    QList<QSharedPointer<MapObject>> m_filteredData;
    std::function<QString(const QSharedPointer<MapObject>& item)> m_getNameFunction;
    std::function<QPair<int, int>(const QSharedPointer<MapObject>& item)> m_getIdFunction;
    int m_targetIndex;
    FilterSettings m_settings;
    QSharedPointer<ITagProvider> m_tagProvider;
    FilterSettingsModel *m_filterSettings;
};

class SelectStackModel: public SelectMapObjectModel
{
public:
    SelectStackModel(QObject *parent = nullptr);
};

class SelectStackTemplateModel: public SelectMapObjectModel
{
public:
    SelectStackTemplateModel(QObject *parent = nullptr);
};

class SelectStackOrTemplateModel: public SelectMapObjectModel
{
public:
    SelectStackOrTemplateModel(QObject *parent = nullptr);
};

class SelectLocationModel: public SelectMapObjectModel
{
public:
    SelectLocationModel(QObject *parent = nullptr);
};

class SelectFortModel: public SelectMapObjectModel
{
public:
    SelectFortModel(QObject *parent = nullptr);
};

class SelectSiteModel: public SelectMapObjectModel
{
public:
    SelectSiteModel(QObject *parent = nullptr);
};

class SelectRuinModel: public SelectMapObjectModel
{
public:
    SelectRuinModel(QObject *parent = nullptr);
};

class SelectPlayerModel: public SelectMapObjectModel
{
public:
    SelectPlayerModel(QObject *parent = nullptr);
};

class SelectVariableModel: public SelectMapObjectModel
{
public:
    SelectVariableModel(QObject *parent = nullptr);
};

class SelectMapLandmarkModel: public SelectMapObjectModel
{
public:
    SelectMapLandmarkModel(QObject *parent = nullptr);
};

#endif // SELECTMAPOBJECTMODEL_H

#include "LogModel.h"
#define ROW_COUNT 1000

LogModel::LogModel(QObject *parent) : QAbstractListModel(parent)
{
    m_roles[ROLE_TEXT] = "message";
    m_roles[ROLE_LEVEL] = "level";
    subscribe<LogUpdatedEvent>([this](const QVariant &event){notifyLog(event);});
}

int LogModel::rowCount(const QModelIndex &parent) const
{
//    if (parent.isValid())
        return 0;
    return qMin(m_records.count(), ROW_COUNT);
}

QVariant LogModel::data(const QModelIndex &index, int role) const
{
    int realIndex  = m_records.count() - index.row() - 1;
    if (role == ROLE_TEXT)
    {
        return m_records.at(realIndex).message;
    }
    if (role == ROLE_LEVEL)
    {
        return m_records.at(realIndex).level;
    }
    return QVariant();
}

QHash<int, QByteArray> LogModel::roleNames() const
{
    return m_roles;
}

void LogModel::notifyLog(const QVariant &event)
{
    beginResetModel();
   // m_records = Logger::records(ROW_COUNT);
    endResetModel();
    //LogUpdatedEvent
}

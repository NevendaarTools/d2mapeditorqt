#ifndef LOGMODEL_H
#define LOGMODEL_H
#include <QAbstractListModel>
#include "Events/MapEvents.h"
#include "toolsqt/Common/Logger.h"
#include "Events/EventBus.h"

class LogModel : public QAbstractListModel, public EventSubscriber
{
    Q_OBJECT
public:
    enum Roles
    {
        ROLE_TEXT = Qt::UserRole + 1,
        ROLE_LEVEL
    };

    explicit LogModel(QObject *parent = nullptr);
    int rowCount(const QModelIndex & parent = QModelIndex()) const;
    virtual QVariant data(const QModelIndex & index, int role = Qt::DisplayRole) const;
    QHash<int, QByteArray> roleNames() const Q_DECL_OVERRIDE;
public slots:
    void notifyLog(const QVariant & event);

signals:
private:
    QHash<int, QByteArray> m_roles;
    QList<Logger::Record> m_records;
};

#endif // LOGMODEL_H

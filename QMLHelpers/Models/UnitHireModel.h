#ifndef UNITHIREMODEL_H
#define UNITHIREMODEL_H
#include <QAbstractListModel>
#include "MapObjects/BaseClases.h"
#include "MapObjects/MerchantObject.h"

class UnitHireModel : public QAbstractListModel
{
    Q_OBJECT
public:
    explicit UnitHireModel(QObject *parent = nullptr);
    enum Roles
    {
        ROLE_ID = Qt::UserRole + 1,
        ROLE_ICON,
        ROLE_NAME,
        ROLE_DESC,
        ROLE_LEVEL,
        ROLE_BASE_LEVEL,
        ROLE_COUNT,
    };

    int rowCount(const QModelIndex & parent = QModelIndex()) const;
    virtual QVariant data(const QModelIndex & index, int role = Qt::DisplayRole) const;
    QHash<int, QByteArray> roleNames() const Q_DECL_OVERRIDE;

    Q_INVOKABLE void addItem(const QString& id);
    Q_INVOKABLE void removeItem(int index);
    Q_INVOKABLE QStringList toPreset() const;
    Q_INVOKABLE void loadPreset(const QStringList& preset);
    Q_INVOKABLE void addPreset(const QStringList& preset);
    Q_INVOKABLE int unitMaxLvl();
    Q_INVOKABLE void setUnitLvl(int index, int level);
    Q_INVOKABLE void setUnitCount(int index, int count);

    const MerchantObject::UnitHireList &units() const;
    void setUnits(const MerchantObject::UnitHireList &newUnits);

signals:
    void changed();
private:
    QHash<int, QByteArray> m_roles;

    MerchantObject::UnitHireList m_units;
};

#endif // UNITHIREMODEL_H

#ifndef ITEMSELECTMODEL_H
#define ITEMSELECTMODEL_H
#include <QAbstractListModel>
#include "QMLHelpers/Common/ITagProvider.h"
#include "TagSelectModel.h"
#include "FilterSettingsModel.h"
#include "toolsqt/DBFModel/GameClases.h"

class ItemTagsProvider: public ITagProvider
{
public:
    void init();
    QList<QString> tagGroups() const
    {
        return m_groupNames;
    }
    QList<QString> availableTags(int groupIndex) const
    {
        return m_groupTags[groupIndex];
    }
    QList<QString> itemTags(const QString &item) const
    {
        return m_itemTags[item];
    }
    bool groupItemsExclusive(int groupIndex) const
    {
        Q_UNUSED(groupIndex);
        return false;
    }
private:

private:
    QHash<QString, QList<QString>> m_itemTags;
    QList<QList<QString>> m_groupTags;
    QList<QString> m_groupNames;

};

class ItemSelectModel : public QAbstractListModel
{
    Q_OBJECT
public:
    explicit ItemSelectModel(QObject *parent = nullptr);
    enum SortMode
    {
        Name = 0,
        Cost
    };

    enum Roles
    {
        ROLE_ID = Qt::UserRole + 1,
        ROLE_ICON,
        ROLE_ICON2,
        ROLE_NAME,
        ROLE_DESC,
        ROLE_COST,
    };
    Q_PROPERTY(FilterSettingsModel * filterSettings READ filterSettings NOTIFY settingsChanged)

    int rowCount(const QModelIndex & parent = QModelIndex()) const;
    virtual QVariant data(const QModelIndex & index, int role = Qt::DisplayRole) const;
    QHash<int, QByteArray> roleNames() const Q_DECL_OVERRIDE;

    FilterSettingsModel *filterSettings() const;

signals:
    void settingsChanged();
private slots:
    void updateSettings();
private:
    QHash<int, QByteArray> m_roles;

    QVector<QSharedPointer<GItem>> m_data;
    QVector<QSharedPointer<GItem>> m_sortedData;
    QSharedPointer<ITagProvider> m_tagProvider;
    FilterSettingsModel *m_filterSettings;
};

#endif // ITEMSELECTMODEL_H

#ifndef SELECTGAMEOBJECTMODEL_H
#define SELECTGAMEOBJECTMODEL_H
#include "MapObjects/StackObject.h"
#include <QAbstractListModel>
#include "toolsqt/DBFModel/GameClases.h"
#include "QMLHelpers/Common/ITagProvider.h"
#include "TagSelectModel.h"
#include "FilterSettingsModel.h"
#include "Engine/GameInstance.h"
#include "toolsqt/DBFModel/DBFModel.h"

class SelectGameObjectModel : public QAbstractListModel
{
    Q_OBJECT
public:
    enum Roles
    {
        ROLE_ID = Qt::UserRole + 1,
        ROLE_TEXT
    };
    struct FilterSettings
    {
        QString filter;
        QList<QList<QString>> tags;
    };
    explicit SelectGameObjectModel(QObject *parent = nullptr);
    Q_PROPERTY(int targetIndex READ targetIndex WRITE setTargetIndex NOTIFY targetChanged)
    Q_PROPERTY(FilterSettingsModel * filterSettings READ filterSettings NOTIFY settingsChanged)

    int rowCount(const QModelIndex & parent = QModelIndex()) const;
    virtual QVariant data(const QModelIndex & index, int role = Qt::DisplayRole) const;
    QHash<int, QByteArray> roleNames() const Q_DECL_OVERRIDE;

    QString selectedId () const;

    template <class classT>
    void init(std::function<QString(const QSharedPointer<GameData>& item)> getName,
              std::function<QString(const QSharedPointer<GameData>& item)> getId,
              std::function<bool(const QSharedPointer<GameData>& item)> accept)
    {
        m_data.clear();
        m_getNameFunction = getName;
        m_getIdFunction = getId;
        QSharedPointer<DBFModel>  data = RESOLVE(DBFModel);
        QVector<QSharedPointer<classT>> list = data->getList<classT>();
        for (int i = 0; i < list.count(); ++i)
        {
            if (!accept(list[i]))
                continue;
            classT * newT = new classT(*list[i].data());
            m_data << QSharedPointer<GameData>(newT);
        }

        updateSettings();
    }

    template <class classT>
    void init(QList<QSharedPointer<GameData>> data,
              std::function<QString(const QSharedPointer<GameData>& item)> getName,
              std::function<QString(const QSharedPointer<GameData>& item)> getId)
    {
        m_data = data;
        m_getNameFunction = getName;
        m_getIdFunction = getId;
        updateSettings();
    }

    template <class classT>
    void init(std::function<QString(const QSharedPointer<GameData>& item)> getName,
              std::function<QString(const QSharedPointer<GameData>& item)> getId)
    {
        init<classT>(getName, getId, [](const QSharedPointer<GameData>& item){return true;});
    }

    void removeItemAt(int index);
    void addItem(QSharedPointer<GameData> data);
    int targetIndex() const;
    void setTargetIndex(int newTargetIndex);
    void setTagProvider(QSharedPointer<ITagProvider> newTagProvider, const QStringList & sortModes);
    QList<QSharedPointer<GameData>> getData() const;
    FilterSettingsModel *filterSettings() const;
    void refresh();
    QSharedPointer<GameData> itemAtIndex(int index) const;
signals:
    void targetChanged();
    void settingsChanged();
private slots:
    void updateSettings();
protected:
    std::function<QString(const QSharedPointer<GameData>& item)> m_getNameFunction;
    std::function<QString(const QSharedPointer<GameData>& item)> m_getIdFunction;
private:
    QHash<int, QByteArray> m_roles;
    QList<QSharedPointer<GameData>> m_data;
    QList<QSharedPointer<GameData>> m_filteredData;
    int m_targetIndex;
    FilterSettings m_settings;
    QSharedPointer<ITagProvider> m_tagProvider;
    FilterSettingsModel *m_filterSettings;
};

class SelectSpellModel: public SelectGameObjectModel
{
public:
    SelectSpellModel(QObject *parent = nullptr);
};

class SelectLMarkModel: public SelectGameObjectModel
{
    Q_OBJECT
public:
    SelectLMarkModel(QObject *parent = nullptr);
    Q_PROPERTY(int lmSize READ lmSize WRITE setLmSize NOTIFY lmSizeChanged FINAL)
    int lmSize() const;
    void setLmSize(int newLmSize);

signals:
    void lmSizeChanged();
private:
    int m_lmSize;
};

#endif // SELECTGAMEOBJECTMODEL_H

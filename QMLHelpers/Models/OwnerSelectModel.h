#ifndef OWNERSELECTMODEL_H
#define OWNERSELECTMODEL_H
#include <QObject>
#include <QAbstractListModel>
#include <MapObjects/Diplomacy.h>

class OwnerSelectModel : public QAbstractListModel
{
    Q_OBJECT
public:
    struct OwnerRow
    {
        QString name;
        QString text;
        MapLink<SubRaceObject> id;
        QString icon;
    };
    OwnerSelectModel(QObject *parent);
    enum Roles
    {
        ROLE_ID = Qt::UserRole + 1,
        ROLE_ICON,
        ROLE_NAME
    };
    Q_PROPERTY(int ownerIndex READ ownerIndex WRITE setOwnerIndex NOTIFY changed)
    void init();
    void setOwnerIndexByUid(QPair<int,int> uid);
    void setOwnerIndexByText(const QString & text);
    int rowCount(const QModelIndex & parent = QModelIndex()) const;
    virtual QVariant data(const QModelIndex & index, int role = Qt::DisplayRole) const;
    QHash<int, QByteArray> roleNames() const Q_DECL_OVERRIDE;
    int ownerIndex() const;
    void setOwnerIndex(int newOwnerIndex);
    QList<OwnerRow> ownersList () const {return m_data;}
    OwnerRow owner(int index) const {return m_data[index];}
    MapLink<SubRaceObject> currentSubrace() const{return m_data[m_ownerIndex].id;}
    MapLink<PlayerObject> currentOwner() const;
signals:
    void changed();
private:
    QHash<int, QByteArray> m_roles;
    QList<OwnerRow> m_data;
    int m_ownerIndex = 0;
};

#endif // OWNERSELECTMODEL_H

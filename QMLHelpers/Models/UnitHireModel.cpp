#include "UnitHireModel.h"
#include "toolsqt/DBFModel/DBFModel.h"
#include "Engine/GameInstance.h"

UnitHireModel::UnitHireModel(QObject *parent) : QAbstractListModel(parent)
{
    m_roles[ROLE_ICON] = "Icon";
    m_roles[ROLE_ID] = "Id";
    m_roles[ROLE_NAME] = "Name";
    m_roles[ROLE_DESC] = "Desc";
    m_roles[ROLE_LEVEL] = "Level";
    m_roles[ROLE_BASE_LEVEL] = "BaseLevel";
    m_roles[ROLE_COUNT] = "Count";
}

int UnitHireModel::rowCount(const QModelIndex &parent) const
{
    if (parent.isValid())
        return 0;
    return m_units.items.count();
}

QVariant UnitHireModel::data(const QModelIndex &index, int role) const
{
    int i = index.row();
    if (i >= m_units.items.count())
        return "";
    if (role == ROLE_ICON)
    {
        auto gunit = RESOLVE(DBFModel)->get<Gunit>(m_units.items[i].first.id);
        if (!gunit.isNull())
        {
            QString unitIconId = gunit->base_unit.getKey() + "FACE";
            if (gunit->base_unit.value == nullptr)
                unitIconId = gunit->unit_id + "FACE";
            return "Faces-" + unitIconId;
        }
    }
    if (role == ROLE_BASE_LEVEL)
    {
        auto gunit = RESOLVE(DBFModel)->get<Gunit>(m_units.items[i].first.id);
        if (!gunit.isNull())
        {
            return gunit->level;
        }
        return 1;
    }
    if (role == ROLE_ID)
    {
        return m_units.items[i].first.id;
    }
    if (role == ROLE_NAME)
    {
        auto unit = RESOLVE(DBFModel)->get<Gunit>(m_units.items[i].first.id);
        if (!unit.isNull())
            return unit->name_txt->text.trimmed();
    }
    if (role == ROLE_DESC)
    {
        auto unit = RESOLVE(DBFModel)->get<Gunit>(m_units.items[i].first.id);
        if (!unit.isNull())
            return unit->desc_txt->text.trimmed();
    }
    if (role == ROLE_LEVEL)
    {
        return m_units.items[i].first.level;
    }
    if (role == ROLE_COUNT)
    {
        return m_units.items[i].second;
    }
    return QVariant();
}

QHash<int, QByteArray> UnitHireModel::roleNames() const
{
    return m_roles;
}

void UnitHireModel::addItem(const QString &id)
{
    auto gunit = RESOLVE(DBFModel)->get<Gunit>(id);
    if (gunit.isNull())
        return;

    beginResetModel();
    UnitObject unit;
    unit.id = id;
    unit.level = gunit->level;
    m_units.items.append(QPair<UnitObject, int> (unit, 0));
    endResetModel();
    emit changed();
}

void UnitHireModel::removeItem(int index)
{
    beginResetModel();
    m_units.items.removeAt(index);
    endResetModel();
    emit changed();
}

QStringList UnitHireModel::toPreset() const
{
    QStringList result;
    for(int i = 0; i < m_units.items.count(); ++i)
    {
        result << QString("%1:%2:3")
                  .arg(m_units.items[i].first.id)
                  .arg(m_units.items[i].first.level)
                  .arg(m_units.items[i].second);
    }
    return result;
}

void UnitHireModel::loadPreset(const QStringList &preset)
{
    beginResetModel();
    m_units.items.clear();
    for (int i = 0; i < preset.count(); ++i)
    {
        QStringList data = preset[i].split(":");
        if (data.count() < 3)
            continue;
        UnitObject unit;
        unit.id = data[0];
        unit.level = data[1].toInt();
        m_units.items.append(QPair<UnitObject, int> (unit, data[2].toInt()));
    }
    endResetModel();
    emit changed();
}

void UnitHireModel::addPreset(const QStringList &preset)
{
    beginResetModel();
    for (int i = 0; i < preset.count(); ++i)
    {
        QStringList data = preset[i].split(":");
        if (data.count() < 3)
            continue;
        UnitObject unit;
        unit.id = data[0];
        unit.level = data[1].toInt();
        m_units.items.append(QPair<UnitObject, int> (unit, data[2].toInt()));
    }

    endResetModel();
    emit changed();
}

int UnitHireModel::unitMaxLvl()
{
    return 65536 / RESOLVE(DBFModel)->getKeysList<Gunit>().count();
}

void UnitHireModel::setUnitLvl(int index, int level)
{
    if (level == m_units.items[index].first.level)
        return;
    beginResetModel();
    m_units.items[index].first.level = level;
    endResetModel();
    emit changed();
}

void UnitHireModel::setUnitCount(int index, int count)
{
    if (count == m_units.items[index].second)
        return;
    beginResetModel();
    m_units.items[index].second = count;
    endResetModel();
    emit changed();
}

const MerchantObject::UnitHireList &UnitHireModel::units() const
{
    return m_units;
}

void UnitHireModel::setUnits(const MerchantObject::UnitHireList &newUnits)
{
    beginResetModel();
    m_units = newUnits;
    endResetModel();
    emit changed();
}

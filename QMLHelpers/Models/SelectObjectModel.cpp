#include "SelectObjectModel.h"
#include "Engine/GameInstance.h"
#include "Engine/Map/MapStateHolder.h"

SelectObjectModel::SelectObjectModel(QObject *parent) : QAbstractListModel(parent)
{
    m_roles[ROLE_ID] = "Id";
    m_roles[ROLE_TEXT] = "Name";
}

int SelectObjectModel::rowCount(const QModelIndex &parent) const
{
    if (parent.isValid())
        return 0;
    return m_filteredData.count();
}

QVariant SelectObjectModel::data(const QModelIndex &index, int role) const
{
    int i = index.row();
    if (role == ROLE_ID)
    {
        return m_filteredData[i]->uid.second;
    }
    if (role == ROLE_TEXT)
    {
        return m_accessor->getName(m_filteredData[i]) + QString("[%1 : %2]").arg(m_filteredData[i]->x).arg(m_filteredData[i]->y);
    }
    return QVariant();
}

QHash<int, QByteArray> SelectObjectModel::roleNames() const
{
    return m_roles;
}

QString SelectObjectModel::target() const
{
    return IdToString(m_targetID);
}

void SelectObjectModel::setTarget(const QString &newTarget)
{
    QString current = target();
    if (current == newTarget)
        return;
    QStringList data = newTarget.split(":");
    if (data.count() < 2)
        return;
    beginResetModel();
    m_targetID.first = data[0].toInt();
    m_targetID.second = data[1].toInt();
    m_data = RESOLVE(MapStateHolder)->collectionByType(m_targetID.first).data.values();
    m_filteredData = m_data;
    m_accessor = RESOLVE(AccessorHolder)->objectAccessor(m_targetID.first);
    m_targetIndex = -1;
    for (int i = 0; i < m_filteredData.count(); ++i)
    {
        if (m_filteredData[i]->uid.second == m_targetID.second)
        {
            m_targetIndex = i;
            break;
        }
    }
    endResetModel();
    emit targetChanged();
}

QString SelectObjectModel::targetName() const
{
    QSharedPointer<MapObject> targ = RESOLVE(MapStateHolder)->objectById(m_targetID);
    if (targ.isNull())
        return "";
    return m_accessor->getName(targ) + QString("[%1 : %2]").arg(targ->x).arg(targ->y);;
}

int SelectObjectModel::targetIndex() const
{
    return m_targetIndex;
}

void SelectObjectModel::setTargetIndex(int newTargetIndex)
{
    if (m_targetIndex == newTargetIndex)
        return;
    m_targetID.second = m_filteredData[newTargetIndex]->uid.second;
    m_targetIndex = newTargetIndex;
    emit targetChanged();
}

const QString &SelectObjectModel::filter() const
{
    return m_filter;
}

void SelectObjectModel::setFilter(const QString &newFilter)
{
    if (m_filter == newFilter)
        return;
    beginResetModel();
    m_filter = newFilter;
    m_filteredData.clear();
    for (int i = 0; i < m_data.count(); ++i)
    {
        QString name = m_accessor->getName(m_data[i]);
        if (name.contains(m_filter, Qt::CaseInsensitive))
        {
            m_filteredData.append(m_data[i]);
            continue;
        }
    }
    m_targetIndex = -1;
    for (int i = 0; i < m_filteredData.count(); ++i)
    {
        if (m_filteredData[i]->uid.second == m_targetID.second)
        {
            m_targetIndex = i;
            break;
        }
    }
    endResetModel();
    emit targetChanged();
}

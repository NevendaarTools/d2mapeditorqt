#include "FilterSettingsModel.h"
#include <QDebug>

FilterSettingsModel::FilterSettingsModel(QObject *parent)
    : QObject(parent)
{
    m_sortModes = new StringModel(this);
    m_sortModes->setProvider(QSharedPointer<SimpleStringProvider>(&m_modes));
}

int FilterSettingsModel::tagGroupsCount() const
{
    return m_tagModels.count();
}

TagSelectModel *FilterSettingsModel::tagModel(int index) const
{
    if (m_tagModels.count() > index)
        return m_tagModels[index];
    return nullptr;
}

const QString &FilterSettingsModel::filter() const
{
    return m_settings.filter;
}

void FilterSettingsModel::setFilter(const QString &newFilter)
{
    if (m_settings.filter == newFilter)
        return;
    m_settings.filter = newFilter;
    emit settingsChanged();
}

int FilterSettingsModel::sortMode() const
{
    return m_settings.mode;
}

void FilterSettingsModel::setSortMode(int newSortMode)
{
    if (m_settings.mode == newSortMode)
        return;
    m_settings.mode = newSortMode;
    emit settingsChanged();
}

void FilterSettingsModel::setModeNames(const QStringList &newModeNames)
{
    m_modes.clear();
    foreach (const QString & mode, newModeNames)
    {
        m_modes.addRow(mode,"");
    }
    emit settingsChanged();
}

StringModel *FilterSettingsModel::sortModes() const
{
    return m_sortModes;
}

void FilterSettingsModel::setTagProvider(QSharedPointer<ITagProvider> newTagProvider)
{
    m_tagProvider = newTagProvider;
    m_tagProvider->init();
    QList<QString> groups = m_tagProvider->tagGroups();
    for (int i = 0; i < groups.count(); ++i)
    {
        m_settings.tags <<QList<QString>();
        TagSelectModel * model = new TagSelectModel(this);
        model->init(m_tagProvider, i);
        connect(model, SIGNAL(settingsChanged()), this, SLOT(onTagsUpdated()));
        m_tagModels<<model;
    }
    emit settingsChanged();
}

const FilterSettingsModel::FilterSettings &FilterSettingsModel::settings() const
{
    return m_settings;
}

void FilterSettingsModel::onTagsUpdated()
{
    m_settings.tags.clear();
    QList<QString> groups = m_tagProvider->tagGroups();
    for (int i = 0; i < groups.count(); ++i)
    {
        TagSelectModel * model = m_tagModels[i];
        m_settings.tags += model->selectedTags();
    }
    emit settingsChanged();
}

#ifndef SPELLSLISTMODEL_H
#define SPELLSLISTMODEL_H
#include <QAbstractListModel>
#include "MapObjects/BaseClases.h"

class SpellsListModel : public QAbstractListModel
{
    Q_OBJECT
public:
    explicit SpellsListModel(QObject *parent = nullptr);
    enum Roles
    {
        ROLE_ID = Qt::UserRole + 1,
        ROLE_ICON,
        ROLE_NAME,
        ROLE_DESC,
        ROLE_COUNT
    };

    int rowCount(const QModelIndex & parent = QModelIndex()) const;
    virtual QVariant data(const QModelIndex & index, int role = Qt::DisplayRole) const;
    QHash<int, QByteArray> roleNames() const Q_DECL_OVERRIDE;

    Q_INVOKABLE void addItem(const QString& id);
    Q_INVOKABLE void removeItem(const QString& id);
    Q_INVOKABLE QStringList toPreset() const;
    Q_INVOKABLE void loadPreset(const QStringList& preset);
    Q_INVOKABLE void addPreset(const QStringList& preset);
    const QList<QString> &getSpells() const;
    void setSpells(const QList<QString> &newSpells);

signals:
    void changed();
private:
    QHash<int, QByteArray> m_roles;

    QList<QString> m_spells;
};

#endif // SPELLSLISTMODEL_H

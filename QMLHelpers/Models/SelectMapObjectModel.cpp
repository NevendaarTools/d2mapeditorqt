#include "SelectMapObjectModel.h"
#include "MapObjects/LandmarkObject.h"
#include "MapObjects/StackObject.h"
#include "MapObjects/StackTemplateObject.h"
#include "MapObjects/LocationObject.h"
#include "MapObjects/FortObject.h"
#include "MapObjects/MerchantObject.h"
#include "MapObjects/RuinObject.h"
#include "MapObjects/Diplomacy.h"
#include "MapObjects/ScenVariableObject.h"
#include "toolsqt/DBFModel/DBFModel.h"

SelectMapObjectModel::SelectMapObjectModel(QObject *parent) : QAbstractListModel(parent)
{
    m_roles[ROLE_ID] = "Id";
    m_roles[ROLE_TEXT] = "Name";
    m_filterSettings = new FilterSettingsModel();
    connect(m_filterSettings, SIGNAL(settingsChanged()), this, SLOT(updateSettings()));
}

int SelectMapObjectModel::rowCount(const QModelIndex &parent) const
{
    if (parent.isValid())
        return 0;
    return m_filteredData.count();
}

QVariant SelectMapObjectModel::data(const QModelIndex &index, int role) const
{
    int i = index.row();
    if (role == ROLE_ID)
    {
        QPair<int, int> tmp =  m_getIdFunction(m_filteredData[i]);
        return QString("%1:%2").arg(tmp.first).arg(tmp.second);
    }
    if (role == ROLE_TEXT)
    {
        const QString note =  m_filteredData[i]->note;
        QString res = m_getNameFunction(m_filteredData[i]);
        if (!note.isEmpty())
            res += "(" + note + ")";
        return res;
    }
    return QVariant();
}

QHash<int, QByteArray> SelectMapObjectModel::roleNames() const
{
    return m_roles;
}

QString SelectMapObjectModel::selectedId() const
{
    if (m_targetIndex < 0 || m_targetIndex >= m_filteredData.count())
        return QString();
    auto tmp =  m_getIdFunction(m_filteredData[m_targetIndex]);
    return QString("%1:%2").arg(tmp.first).arg(tmp.second);
}

int SelectMapObjectModel::targetIndex() const
{
    return m_targetIndex;
}

void SelectMapObjectModel::setTargetIndex(int newTargetIndex)
{
    if (m_targetIndex == newTargetIndex)
        return;
    m_targetIndex = newTargetIndex;
    emit targetChanged();
}

void SelectMapObjectModel::updateSettings()
{
    beginResetModel();
    m_filteredData.clear();
    FilterSettingsModel::FilterSettings settings = m_filterSettings->settings();
    foreach(auto unit, m_data)
    {
        QString name = m_getNameFunction(unit);
        if (!settings.filter.isEmpty())
        {
            if ((!name.contains(settings.filter, Qt::CaseInsensitive)) &&
                (!name.contains(settings.filter, Qt::CaseInsensitive)))
            {
                continue;
            }
        }
        auto tmp =  m_getIdFunction(unit);
        QString id = QString("%1:%2").arg(tmp.first).arg(tmp.second);
        if (!acceptTags(m_tagProvider, settings.tags, id))
            continue;

        m_filteredData.append(unit);
    }

    endResetModel();
}

void SelectMapObjectModel::setTagProvider(QSharedPointer<ITagProvider> newTagProvider, const QStringList &sortModes)
{
    m_tagProvider = newTagProvider;
    m_filterSettings->setTagProvider(m_tagProvider);
    m_filterSettings->setModeNames(sortModes);
    //m_filterSettings->setModeNames(QStringList ()<<"Name"<< "Exp"<< "ExpKilled");
    //m_filterSettings->setSortMode(SortMode::Name);
    updateSettings();
}


FilterSettingsModel *SelectMapObjectModel::filterSettings() const
{
    return m_filterSettings;
}

void SelectMapObjectModel::refresh()
{
    updateSettings();
    return;
    beginResetModel();
    endResetModel();
    emit settingsChanged();
}

QSharedPointer<MapObject> SelectMapObjectModel::objectAtIndex(int index) const
{
    return m_filteredData.at(index);
}

void SelectMapObjectModel::updateAtIndex(int itemIndex)
{
    emit dataChanged(index(itemIndex, 0), index(itemIndex,0));
}

void SelectMapObjectModel::updateWithId(const QPair<int, int> &id)
{
    QSharedPointer<MapObject> item = RESOLVE(MapStateHolder)->objectById(id);
    int indexToUpdate = -1;
    for (int i = 0; i < m_filteredData.count(); ++i)
    {
        const auto & itemId = m_getIdFunction(m_filteredData[i]);
        if (itemId == id)
        {
            m_filteredData[i]= item;
            break;
        }
    }
    for (int i = 0; i < m_data.count(); ++i)
    {
        const auto & itemId = m_getIdFunction(m_data[i]);
        if (itemId == id)
        {
            m_data[i]= item;
            break;
        }
    }
    if (indexToUpdate != -1)
        updateAtIndex(indexToUpdate);
}

void SelectMapObjectModel::saveData()
{
    for(auto item: std::as_const(m_data))
    {
        RESOLVE(MapStateHolder)->replaceObject(item);
    }
}

SelectStackModel::SelectStackModel(QObject *parent) : SelectMapObjectModel(parent)
{
    auto getText = [](const QSharedPointer<MapObject>& item)
    {
        StackObject * quest = static_cast<StackObject*>(item.data());
        return quest->stack.name + item->getNote();
    };
    auto getId = [](const QSharedPointer<MapObject>& item)
    {
        StackObject * quest = static_cast<StackObject*>(item.data());
        return quest->uid;
    };
    this->init<StackObject>(getText, getId);
}

SelectLocationModel::SelectLocationModel(QObject *parent) : SelectMapObjectModel(parent)
{
    auto getText = [](const QSharedPointer<MapObject>& item)
    {
        LocationObject * quest = static_cast<LocationObject*>(item.data());
        return quest->name + item->getNote();
    };
    auto getId = [](const QSharedPointer<MapObject>& item)
    {
        LocationObject * quest = static_cast<LocationObject*>(item.data());
        return quest->uid;
    };
    this->init<LocationObject>(getText, getId);
}

SelectStackTemplateModel::SelectStackTemplateModel(QObject *parent) : SelectMapObjectModel(parent)
{
    auto getText = [](const QSharedPointer<MapObject>& item)
    {
        StackTemplateObject * quest = static_cast<StackTemplateObject*>(item.data());
        return quest->stack.name + item->getNote();
    };
    auto getId = [](const QSharedPointer<MapObject>& item)
    {
        StackTemplateObject * quest = static_cast<StackTemplateObject*>(item.data());
        return quest->uid;
    };
    this->init<StackTemplateObject>(getText, getId);
}

SelectFortModel::SelectFortModel(QObject *parent) : SelectMapObjectModel(parent)
{
    auto getText = [](const QSharedPointer<MapObject>& item)
    {
        FortObject * quest = static_cast<FortObject*>(item.data());
        return quest->name + item->getNote();
    };
    auto getId = [](const QSharedPointer<MapObject>& item)
    {
        FortObject * quest = static_cast<FortObject*>(item.data());
        return quest->uid;
    };
    this->init<FortObject>(getText, getId);
}

SelectSiteModel::SelectSiteModel(QObject *parent) : SelectMapObjectModel(parent)
{
    auto getText = [](const QSharedPointer<MapObject>& item)
    {
        MerchantObject * quest = static_cast<MerchantObject*>(item.data());
        return quest->name + item->getNote();
    };
    auto getId = [](const QSharedPointer<MapObject>& item)
    {
        MerchantObject * quest = static_cast<MerchantObject*>(item.data());
        return quest->uid;
    };
    this->init<MerchantObject>(getText, getId);
}

SelectRuinModel::SelectRuinModel(QObject *parent) : SelectMapObjectModel(parent)
{
    auto getText = [](const QSharedPointer<MapObject>& item)
    {
        RuinObject * quest = static_cast<RuinObject*>(item.data());
        return quest->name + item->getNote();
    };
    auto getId = [](const QSharedPointer<MapObject>& item)
    {
        RuinObject * quest = static_cast<RuinObject*>(item.data());
        return quest->uid;
    };
    this->init<RuinObject>(getText, getId);
}

SelectPlayerModel::SelectPlayerModel(QObject *parent) : SelectMapObjectModel(parent)
{
    auto getText = [](const QSharedPointer<MapObject>& item)
    {
        PlayerObject * quest = static_cast<PlayerObject*>(item.data());
        return quest->name + item->getNote();
    };
    auto getId = [](const QSharedPointer<MapObject>& item)
    {
        PlayerObject * quest = static_cast<PlayerObject*>(item.data());
        return quest->uid;
    };
    this->init<PlayerObject>(getText, getId);
}

SelectVariableModel::SelectVariableModel(QObject *parent) : SelectMapObjectModel(parent)
{
    auto getText = [](const QSharedPointer<MapObject>& item)
    {
        ScenVariableObject * variable = static_cast<ScenVariableObject*>(item.data());
        return variable->name;
    };
    auto getId = [](const QSharedPointer<MapObject>& item)
    {
        ScenVariableObject * variable = static_cast<ScenVariableObject*>(item.data());
        return variable->uid;
    };

   this->init<ScenVariableObject>(getText, getId);
}

SelectStackOrTemplateModel::SelectStackOrTemplateModel(QObject *parent) : SelectMapObjectModel(parent)
{
    QList<QSharedPointer<MapObject>> data;
    QList<StackObject> stackList = RESOLVE(MapStateHolder)->objectsByType<StackObject>();
    for (int i = 0; i < stackList.count(); ++i)
    {
        StackObject * newT = new StackObject(stackList[i]);
        auto pointer = QSharedPointer<MapObject>(newT);
        data << pointer;
    }

    QList<StackTemplateObject> stackTempList = RESOLVE(MapStateHolder)->objectsByType<StackTemplateObject>();
    for (int i = 0; i < stackTempList.count(); ++i)
    {
        StackTemplateObject * newT = new StackTemplateObject(stackTempList[i]);
        auto pointer = QSharedPointer<MapObject>(newT);
        data << pointer;
    }

    auto getText = [](const QSharedPointer<MapObject>& item)
    {
        if (item->uid.first == MapObject::Stack)
        {
            StackObject * quest = static_cast<StackObject*>(item.data());
            return quest->stack.name + item->getNote();
        }
        StackTemplateObject * quest = static_cast<StackTemplateObject*>(item.data());
        return quest->stack.name + "[TEMP]" + item->getNote();
    };
    auto getId = [](const QSharedPointer<MapObject>& item)
    {
        return item->uid;
    };

    std::sort(data.begin(), data.end(),
              [](const QSharedPointer<MapObject>& a, const QSharedPointer<MapObject>& b) {
                  if (a->uid.first == b->uid.first)
                      return a->uid.second < b->uid.second;
                  return a->uid.first < b->uid.first;
              });
    this->init<StackObject>(getText, getId, data);

}

SelectMapLandmarkModel::SelectMapLandmarkModel(QObject *parent)
{
    auto getText = [](const QSharedPointer<MapObject>& item)
    {
        LandmarkObject * variable = static_cast<LandmarkObject*>(item.data());
        QSharedPointer<DBFModel>  data = RESOLVE(DBFModel);
        auto mark = data->get<GLmark>(variable->lmarkId);
        return mark->name_txt->text;
    };
    auto getId = [](const QSharedPointer<MapObject>& item)
    {
        LandmarkObject * variable = static_cast<LandmarkObject*>(item.data());
        return variable->uid;
    };

    this->init<LandmarkObject>(getText, getId);
}

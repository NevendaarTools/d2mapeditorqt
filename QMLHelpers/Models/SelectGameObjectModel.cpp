#include "SelectGameObjectModel.h"

SelectGameObjectModel::SelectGameObjectModel(QObject *parent) : QAbstractListModel(parent)
{
    m_roles[ROLE_ID] = "Id";
    m_roles[ROLE_TEXT] = "Name";
    m_filterSettings = new FilterSettingsModel();
    connect(m_filterSettings, SIGNAL(settingsChanged()), this, SLOT(updateSettings()));
}

int SelectGameObjectModel::rowCount(const QModelIndex &parent) const
{
    if (parent.isValid())
        return 0;
    return m_filteredData.count();
}

QVariant SelectGameObjectModel::data(const QModelIndex &index, int role) const
{
    int i = index.row();
    if (role == ROLE_ID)
    {
        return m_getIdFunction(m_filteredData[i]);
    }
    if (role == ROLE_TEXT)
    {
        return m_getNameFunction(m_filteredData[i]);
    }
    return QVariant();
}

QHash<int, QByteArray> SelectGameObjectModel::roleNames() const
{
    return m_roles;
}

QString SelectGameObjectModel::selectedId() const
{
    if (m_targetIndex < 0 || m_targetIndex >= m_filteredData.count())
        return QString();
    return m_getIdFunction(m_filteredData[m_targetIndex]);
}

void SelectGameObjectModel::removeItemAt(int index)
{
    m_data.removeAt(index);//TODO: update with filters
    updateSettings();
}

void SelectGameObjectModel::addItem(QSharedPointer<GameData> data)
{
    m_data << data;//TODO: update with filters
    updateSettings();
}

int SelectGameObjectModel::targetIndex() const
{
    return m_targetIndex;
}

void SelectGameObjectModel::setTargetIndex(int newTargetIndex)
{
    if (m_targetIndex == newTargetIndex)
        return;
    m_targetIndex = newTargetIndex;
    emit targetChanged();
}

void SelectGameObjectModel::updateSettings()
{
    beginResetModel();
    m_filteredData.clear();
    FilterSettingsModel::FilterSettings settings = m_filterSettings->settings();
    foreach(auto unit, m_data)
    {
        QString name = m_getNameFunction(unit);
        if (!settings.filter.isEmpty())
        {
            if ((!name.contains(settings.filter, Qt::CaseInsensitive)) &&
                (!name.contains(settings.filter, Qt::CaseInsensitive)))
            {
                continue;
            }
        }
        QString id = m_getIdFunction(unit);
        if (!acceptTags(m_tagProvider, settings.tags, id))
            continue;

        m_filteredData.append(unit);
    }

    endResetModel();
}

void SelectGameObjectModel::setTagProvider(QSharedPointer<ITagProvider> newTagProvider, const QStringList &sortModes)
{
    m_tagProvider = newTagProvider;
    m_filterSettings->setTagProvider(m_tagProvider);
    m_filterSettings->setModeNames(sortModes);
    //m_filterSettings->setModeNames(QStringList ()<<"Name"<< "Exp"<< "ExpKilled");
    //m_filterSettings->setSortMode(SortMode::Name);
    updateSettings();
}

QList<QSharedPointer<GameData> > SelectGameObjectModel::getData() const
{
    return m_data;
}


FilterSettingsModel *SelectGameObjectModel::filterSettings() const
{
    return m_filterSettings;
}

void SelectGameObjectModel::refresh()
{
    updateSettings();
    return;
    beginResetModel();
    endResetModel();
    emit settingsChanged();
}

QSharedPointer<GameData> SelectGameObjectModel::itemAtIndex(int index) const
{
    return m_data[index];
}

SelectSpellModel::SelectSpellModel(QObject *parent) : SelectGameObjectModel(parent)
{
    auto getText = [](const QSharedPointer<GameData>& item)
    {
        GSpell * quest = static_cast<GSpell*>(item.data());
        return quest->name_txt->text;
    };
    auto getId = [](const QSharedPointer<GameData>& item)
    {
        GSpell * quest = static_cast<GSpell*>(item.data());
        return quest->spell_id.toUpper();
    };
    this->init<GSpell>(getText, getId);
}

SelectLMarkModel::SelectLMarkModel(QObject *parent)
{
    auto getText = [](const QSharedPointer<GameData>& item)
    {
        GLmark * quest = static_cast<GLmark*>(item.data());
        return quest->name_txt->text + QString("( %1 x %2 )").arg(quest->cx).arg(quest->cy);
    };
    auto getId = [](const QSharedPointer<GameData>& item)
    {
        GLmark * quest = static_cast<GLmark*>(item.data());
        return quest->lmark_id.toUpper();
    };
    this->init<GLmark>(getText, getId);
}

int SelectLMarkModel::lmSize() const
{
    return m_lmSize;
}

void SelectLMarkModel::setLmSize(int newLmSize)
{
    if (m_lmSize == newLmSize)
        return;
    m_lmSize = newLmSize;
    init<GLmark>(m_getNameFunction, m_getIdFunction, [newLmSize](const QSharedPointer<GameData>& item){
        GLmark * quest = static_cast<GLmark*>(item.data());
        return quest->cx == newLmSize;
    });
    emit lmSizeChanged();
}

#ifndef MODELS_H
#define MODELS_H
#include "InventoryModel.h"
#include "ItemSelectModel.h"
#include "LogModel.h"
#include "MountainSelectModel.h"
#include "OptionsModel.h"
#include "SelectGameObjectModel.h"
#include "SelectMapObjectModel.h"
#include "SelectObjectModel.h"
#include "SpellsListModel.h"
#include "SpellsSelectModel.h"
#include "StringModel.h"
#include "TagSelectModel.h"
#include "TreeSelectModel.h"
#include "UnitHireModel.h"
#include "UnitSelectModel.h"
#include "EventsListModel.h"
#include "FilterSettingsModel.h"
#include "OwnerSelectModel.h"
#include "qqml.h"

static void modelsRegister()
{
    qmlRegisterType<UnitSelectModel>("QMLUnitSelectModel", 1, 0,"UnitSelectModel");
    qmlRegisterType<SpellsSelectModel>("QMLSpellsSelectModel", 1, 0,"SpellsSelectModel");
    qmlRegisterType<ItemSelectModel>("QMLItemSelectModel", 1, 0,"ItemSelectModel");
    qmlRegisterType<LogModel>("QMLLogModel", 1, 0,"LogModel");
    qmlRegisterType<InventoryModel>("QMLInventoryModel", 1, 0,"InventoryModel");
    qmlRegisterType<SelectObjectModel>("QMLSelectObjectModel", 1, 0,"SelectObjectModel");
    qmlRegisterType<OptionsModel>("QMLOptionsModel", 1, 0,"OptionsModel");
    qmlRegisterType<TagSelectModel>("QMLTagSelectModel", 1, 0,"TagSelectModel");
    qmlRegisterType<SelectGameObjectModel>("QMLSelectGameObjectModel", 1, 0,"SelectGameObjectModel");
    qmlRegisterType<EventsListModel>("NTModels", 1, 0,"EventsListModel");
    qmlRegisterType<FilterSettingsModel>("QMLFilterSettingsModel", 1, 0,"FilterSettingsModel");
    qmlRegisterType<SelectMapObjectModel>   ("NTModels", 1, 0,"SelectMapObjectModel");
    qmlRegisterType<SelectStackModel>       ("NTModels", 1, 0,"SelectStackModel");
    qmlRegisterType<SelectLocationModel>    ("NTModels", 1, 0,"SelectLocationModel");
    qmlRegisterType<SelectStackTemplateModel>    ("NTModels", 1, 0,"SelectStackTemplateModel");
    qmlRegisterType<SelectFortModel>    ("NTModels", 1, 0,"SelectFortModel");
    qmlRegisterType<SelectSiteModel>    ("NTModels", 1, 0,"SelectSiteModel");
    qmlRegisterType<SelectRuinModel>    ("NTModels", 1, 0,"SelectRuinModel");
    qmlRegisterType<SelectPlayerModel>  ("NTModels", 1, 0,"SelectPlayerModel");
    qmlRegisterType<SelectVariableModel> ("NTModels", 1, 0, "SelectVariableModel");
    qmlRegisterType<SelectStackOrTemplateModel> ("NTModels", 1, 0, "SelectStackOrTemplateModel");
    qmlRegisterType<OwnerSelectModel> ("NTModels", 1, 0, "OwnerSelectModel");
    qmlRegisterType<SelectSpellModel> ("NTModels", 1, 0, "SelectSpellModel");
    qmlRegisterType<SelectLMarkModel> ("NTModels", 1, 0, "SelectLMarkModel");
    qmlRegisterType<SelectMapLandmarkModel> ("NTModels", 1, 0, "SelectMapLandmarkModel");
}


#endif // MODELS_H

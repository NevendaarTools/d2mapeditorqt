#ifndef FILTERSETTINGSMODEL_H
#define FILTERSETTINGSMODEL_H

#include <QAbstractItemModel>
#include "QMLHelpers/Common/ITagProvider.h"
#include "QMLHelpers/Models/TagSelectModel.h"
#include "QMLHelpers/Models/StringModel.h"

class FilterSettingsModel : public QObject
{
    Q_OBJECT

public:
    struct FilterSettings
    {
        QString filter;
        int mode;
        QList<QList<QString>> tags;
    };

    explicit FilterSettingsModel(QObject *parent = nullptr);
    Q_PROPERTY(QString filter READ filter WRITE setFilter NOTIFY settingsChanged)
    Q_PROPERTY(int tagGroupsCount READ tagGroupsCount NOTIFY settingsChanged)
    Q_PROPERTY(int sortMode READ sortMode WRITE setSortMode NOTIFY settingsChanged)
    Q_PROPERTY(StringModel* sortModes READ sortModes NOTIFY settingsChanged)

    int tagGroupsCount() const;
    Q_INVOKABLE TagSelectModel* tagModel(int index) const;

    const QString &filter() const;
    void setFilter(const QString &newFilter);

    int sortMode() const;
    void setSortMode(int newSortMode);

    void setModeNames(const QStringList &newModeNames);
    StringModel *sortModes() const;

    void setTagProvider(QSharedPointer<ITagProvider> newTagProvider);

    const FilterSettings &settings() const;

public slots:
    void onTagsUpdated();
signals:
    void settingsChanged();

private:
    QList<TagSelectModel*> m_tagModels;
    QSharedPointer<ITagProvider> m_tagProvider;
    SimpleStringProvider m_modes;


    FilterSettings m_settings;
    StringModel *m_sortModes;
};

#endif // FILTERSETTINGSMODEL_H

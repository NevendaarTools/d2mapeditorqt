#include "TagSelectModel.h"

TagSelectModel::TagSelectModel(QObject *parent)
    : QAbstractListModel(parent)
{
    m_roles[ROLE_NAME] = "Name";
    m_roles[ROLE_CHECKED] = "Checked";
}

int TagSelectModel::rowCount(const QModelIndex &parent) const
{
    if (parent.isValid())
        return 0;
    if (m_tagProvider.isNull())
        return 0;
    return m_tagProvider->availableTags(m_groupIndex).count();
}

QVariant TagSelectModel::data(const QModelIndex &index, int role) const
{
    if (role == ROLE_NAME)
    {
        return m_tagProvider->availableTags(m_groupIndex)[index.row()];
    }
    if (role == ROLE_CHECKED)
    {
        QString name = m_tagProvider->availableTags(m_groupIndex)[index.row()];
        return m_selectedTags.contains(name);
    }
    return QVariant();
}

QHash<int, QByteArray> TagSelectModel::roleNames() const
{
    return m_roles;
}

void TagSelectModel::setChecked(int index, bool checked)
{
    QString name = m_tagProvider->availableTags(m_groupIndex)[index];
    if (checked)
        m_selectedTags.append(name);
    else
        m_selectedTags.removeAll(name);
    emit dataChanged(this->index(index,0), this->index(index,0));
    emit settingsChanged();
}

void TagSelectModel::setInversed(int index)
{
    beginResetModel();
    m_selectedTags.clear();
    int count = m_tagProvider->availableTags(m_groupIndex).count();
    for (int i = 0; i < count; ++i)
    {
        m_selectedTags << m_tagProvider->availableTags(m_groupIndex)[i];
    }

    QString name = m_tagProvider->availableTags(m_groupIndex)[index];
    m_selectedTags.removeAll(name);
    endResetModel();
    emit settingsChanged();
}

void TagSelectModel::setExclusive(int index)
{
    beginResetModel();
    m_selectedTags.clear();
    QString name = m_tagProvider->availableTags(m_groupIndex)[index];
    m_selectedTags.append(name);
    endResetModel();
    emit settingsChanged();
}

void TagSelectModel::clearTags()
{
    beginResetModel();
    m_selectedTags.clear();
    endResetModel();
    emit settingsChanged();
}

const QList<QString> &TagSelectModel::selectedTags() const
{
    return m_selectedTags;
}

void TagSelectModel::setSelectedTags(const QList<QString> &newSelectedTags)
{
    beginResetModel();
    m_selectedTags = newSelectedTags;
    endResetModel();
    emit settingsChanged();
}

QString TagSelectModel::groupName() const
{
    if (m_tagProvider.isNull())
        return 0;
    return m_tagProvider->tagGroups()[m_groupIndex];
}

void TagSelectModel::init(QSharedPointer<ITagProvider> tagProvider, int index)
{
    beginResetModel();
    m_tagProvider = tagProvider;
    m_groupIndex = index;
    endResetModel();
    emit settingsChanged();
}

bool TagSelectModel::hasSelected() const
{
    return m_selectedTags.count() > 0;
}

#include "SpellsListModel.h"
#include "toolsqt/DBFModel/DBFModel.h"
#include "Engine/GameInstance.h"

SpellsListModel::SpellsListModel(QObject *parent) : QAbstractListModel(parent)
{
    m_roles[ROLE_ICON] = "Icon";
    m_roles[ROLE_ID] = "Id";
    m_roles[ROLE_NAME] = "Name";
    m_roles[ROLE_DESC] = "Desc";
    m_roles[ROLE_COUNT] = "Count";
}

int SpellsListModel::rowCount(const QModelIndex &parent) const
{
    if (parent.isValid())
        return 0;
    return m_spells.count();
}

QVariant SpellsListModel::data(const QModelIndex &index, int role) const
{
    int i = index.row();
    if (i >= m_spells.count())
        return "";
    if (role == ROLE_ICON)
    {
        return "IconSpel-" + m_spells[i].toUpper();

    }
    if (role == ROLE_ID)
    {
        return m_spells[i];
    }
    if (role == ROLE_NAME)
    {
        auto spell = RESOLVE(DBFModel)->get<GSpell>(m_spells[i]);
        if (!spell.isNull())
            return spell->name_txt->text.trimmed();
    }
    if (role == ROLE_DESC)
    {
        auto spell = RESOLVE(DBFModel)->get<GSpell>(m_spells[i]);
        if (!spell.isNull())
            return spell->modif_txt->text.trimmed();
//            return spell->desc_txt->text.trimmed();
    }
    if (role == ROLE_COUNT)
    {
        return 1;
    }
    return QVariant();
}

QHash<int, QByteArray> SpellsListModel::roleNames() const
{
    return m_roles;
}

void SpellsListModel::addItem(const QString &id)
{
    beginResetModel();
    if (!m_spells.contains(id))
        m_spells.append(id);
    endResetModel();
    emit changed();
}

void SpellsListModel::removeItem(const QString &id)
{
    beginResetModel();
    m_spells.removeAll(id);
    endResetModel();
    emit changed();
}

QStringList SpellsListModel::toPreset() const
{
    return m_spells;
}

void SpellsListModel::loadPreset(const QStringList &preset)
{
    beginResetModel();
    m_spells = preset;
    endResetModel();
    emit changed();
}

void SpellsListModel::addPreset(const QStringList &preset)
{
    beginResetModel();
    foreach (const QString & id, preset)
    {
        if (!m_spells.contains(id))
            m_spells.append(id);
    }

    endResetModel();
    emit changed();
}

const QList<QString> &SpellsListModel::getSpells() const
{
    return m_spells;
}

void SpellsListModel::setSpells(const QList<QString> &newSpells)
{
    beginResetModel();
    m_spells = newSpells;
    endResetModel();
    emit changed();
}

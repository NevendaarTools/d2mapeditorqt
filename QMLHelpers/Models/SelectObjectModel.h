#ifndef SELECTOBJECTMODEL_H
#define SELECTOBJECTMODEL_H
#include "MapObjects/StackObject.h"
#include <QAbstractListModel>
#include "Engine/Components/AccessorHolder.h"

class SelectObjectModel : public QAbstractListModel
{
    Q_OBJECT
public:
    enum Roles
    {
        ROLE_ID = Qt::UserRole + 1,
        ROLE_TEXT
    };
    explicit SelectObjectModel(QObject *parent = nullptr);
    Q_PROPERTY(QString target READ target WRITE setTarget NOTIFY targetChanged)
    Q_PROPERTY(QString filter READ filter WRITE setFilter NOTIFY targetChanged)
    Q_PROPERTY(QString targetName READ targetName NOTIFY targetChanged)
    Q_PROPERTY(int targetIndex READ targetIndex WRITE setTargetIndex NOTIFY targetChanged)

    int rowCount(const QModelIndex & parent = QModelIndex()) const;
    virtual QVariant data(const QModelIndex & index, int role = Qt::DisplayRole) const;
    QHash<int, QByteArray> roleNames() const Q_DECL_OVERRIDE;

    QString target() const;
    void setTarget(const QString &newTarget);

    QString targetName() const;

    int targetIndex() const;
    void setTargetIndex(int newTargetIndex);

    const QString &filter() const;
    void setFilter(const QString &newFilter);

signals:
    void targetChanged();

private:
    QHash<int, QByteArray> m_roles;
    QList<QSharedPointer<MapObject>> m_data;
    QList<QSharedPointer<MapObject>> m_filteredData;
    QPair<int, int> m_targetID;
    QSharedPointer<IMapObjectAccessor> m_accessor;
    int m_targetIndex;
    QString m_filter;
};

#endif // SELECTOBJECTMODEL_H

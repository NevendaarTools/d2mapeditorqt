#ifndef OVERLAYWIDGET_H
#define OVERLAYWIDGET_H
#include <QQuickWidget>
#include <QQuickItem>
#include "Events/EventBus.h"
#include "Commands/CommandBus.h"

class OverlayWidget : public QQuickWidget,
    public CommandExecutor, public EventSubscriber
{
    Q_OBJECT
public:
    OverlayWidget(QWidget* parent = nullptr);
    Q_PROPERTY(QString position READ getPosition NOTIFY changed)
    Q_PROPERTY(QString gameV READ getGameV NOTIFY changed)
    void init();

    void onMapOverlaySettingsChanged(const QVariant &event);
    void onGameLoaded(const QVariant &event);
    void onHoverObjectChanged(const QVariant &event);
    void onHoverChanged(const QVariant &event);
    void onObjectRemoved(const QVariant &event);

    Q_INVOKABLE QString tr(const QString & text);
    Q_INVOKABLE void setRootItem(QQuickItem *newRootItem);

    QString getPosition() const;
    QString getGameV() const;


signals:
    void tooltipRequested(int x, int y, const QString & text);
    void tooltipHideRequested();

    void previewRequested(int x, int y, const QString & source, const QString & uid);
    void previewHideRequested();
    void changed();
    void requestRoot();
private:
    void showPos(const QPoint &pos);
private:
    QString m_position;
    QString m_gameV;
    QQuickItem* m_rootItem;
    bool m_versionVisible = false;
    bool m_posVisible = true;
};

#endif // OVERLAYWIDGET_H

#pragma once
#include <QString>
#include <QMetaType>
#include <QPoint>
#include "MapObjects/GameMap.h"

struct OpenMapCommand
{
    OpenMapCommand(const QString & path_ = "") : path(path_){}
    QString path;
};
Q_DECLARE_METATYPE(OpenMapCommand)

struct PopulateMapCommand
{
    PopulateMapCommand(const QString & path_ = "") : path(path_){}
    QString path;
};
Q_DECLARE_METATYPE(PopulateMapCommand)


struct SaveMapCommand
{
    SaveMapCommand(const QString & path_ = "") : path(path_){}
    QString path;
};
Q_DECLARE_METATYPE(SaveMapCommand)

struct ExportMapCommand
{
    ExportMapCommand(){}
};
Q_DECLARE_METATYPE(ExportMapCommand)

struct OpenSettingsCommand
{
    OpenSettingsCommand(){}
};
Q_DECLARE_METATYPE(OpenSettingsCommand)

struct OpenDataStorageViewCommand
{
    OpenDataStorageViewCommand(){}
};
Q_DECLARE_METATYPE(OpenDataStorageViewCommand)

struct CloseMapCommand
{
    CloseMapCommand(){}
};
Q_DECLARE_METATYPE(CloseMapCommand)

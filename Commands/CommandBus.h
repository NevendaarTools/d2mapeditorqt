#ifndef COMMANDBUS_H
#define COMMANDBUS_H

#include <QObject>
#include <QVariant>
#include <QDebug>
#include "Command.h"

class CommandExecutor: public ICommandExecutor
{
public:
    virtual ~CommandExecutor();

    CommandResult execute(const QVariant& command)
    {
        const QString typeName = command.typeName();
        return m_executors[typeName](command);
    }

    void registerExecutor(const QString& name, std::function<CommandResult(const QVariant& command)> function);
    template <typename commandT>
    void registerExecutor(std::function<CommandResult(const QVariant& command)> function)
    {
        const QString& name = QMetaType::typeName(qMetaTypeId<commandT>());
        registerExecutor(name, function);
    }
private:
    QHash<QString, std::function<CommandResult(const QVariant& command)>> m_executors;
};

class CommandBus
{
public:
    explicit CommandBus(){}

    void registerExecutor(const QString& name, ICommandExecutor * executor)
    {
        if (m_executors.contains(name)) {
            LOG_WARNING("Executor already registered : " + name);
            return;
        }
        LOG_MESSAGE("Register executor : " + name);
        m_executors.insert(name, executor);
    }

    void unregisterExecutor(ICommandExecutor * executor)
    {
        QStringList keys = m_executors.keys();
        for (int i = 0; i < keys.count(); ++i)
        {
            if (m_executors[keys[i]] == executor)
            {
                LOG_MESSAGE("Unregister executor : " + keys[i]);
                m_executors.remove(keys[i]);
            }
        }
    }

    template <typename commandT>
    CommandResult execute(const commandT& command)
    {
        return execute(QVariant::fromValue(command));
    }

    CommandResult execute(const QVariant& command)
    {
        const QString typeName = command.typeName();
        if (!m_executors.contains(typeName)){
            LOG_WARNING("Failed to find executor to execute : " + typeName);
            return CommandResult::fail("Failed to find executor");
        }
        return m_executors[typeName]->execute(command);
    }
private:
    QHash<QString, ICommandExecutor*> m_executors;
};

//Q_DECLARE_METATYPE(
#endif // COMMANDBUS_H

#ifndef COMMAND_H
#define COMMAND_H
#include <QString>
#include <QVariant>
#include "Common/StringHelper.h"

struct CommandResult
{
    enum Type{
        Success = 0,
        Fail
    };

    QString message;
    Type type;

    CommandResult(Type typeIn = CommandResult::Success) : type(typeIn) {}

    static CommandResult fail(const QString& message)
    {
        CommandResult result(CommandResult::Fail);
        result.message = message;
        return result;
    }
    static CommandResult success(const QString& message = QString())
    {
        CommandResult result(CommandResult::Success);
        result.message = message;
        return result;
    }
};

class ICommandExecutor
{
public:
    virtual ~ICommandExecutor(){}
    virtual CommandResult execute(const QVariant& variant) = 0;
};

#endif // COMMAND_H

#ifndef OVERLAYCOMMANDS_H
#define OVERLAYCOMMANDS_H
#include <QVariant>

struct ShowToolTipCommand
{
    int x;
    int y;
    QString text;
};
Q_DECLARE_METATYPE(ShowToolTipCommand)

struct HideToolTipCommand
{

};
Q_DECLARE_METATYPE(HideToolTipCommand)

struct ShowPreviewCommand
{
    int x;
    int y;
    QString source;
    QString id;
};
Q_DECLARE_METATYPE(ShowPreviewCommand)

struct HidePreviewCommand
{

};
Q_DECLARE_METATYPE(HidePreviewCommand)

struct MapOverlaySettingsChanged
{
    MapOverlaySettingsChanged(bool pos = true, bool vers = false)
        : positionVisible(pos), gameVVisible(vers)
    {

    }
    bool positionVisible = true;
    bool gameVVisible = false;
};


#endif // OVERLAYCOMMANDS_H

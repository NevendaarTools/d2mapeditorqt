#ifndef EDITCOMMANDS_H
#define EDITCOMMANDS_H
#include <QVariant>
#include "../MapObjects/MapObject.h"

struct EditCommand
{
    QPair<int, int> uid;
    EditCommand(QPair<int, int> objectUid = QPair<int, int>()) : uid(objectUid)
    {

    }
};
Q_DECLARE_METATYPE(EditCommand)

struct RemoveObjectCommand
{
    QPair<int, int> uid;
    RemoveObjectCommand(QPair<int, int> objectUid = QPair<int, int>()) : uid(objectUid)
    {

    }
};
Q_DECLARE_METATYPE(RemoveObjectCommand)

struct ChangeCrystalCommand
{
    QPair<int, int> uid;
    int type;
    ChangeCrystalCommand(QPair<int, int> objectUid = QPair<int, int>(), int newType = 0) : uid(objectUid), type(newType)
    {

    }
};
Q_DECLARE_METATYPE(ChangeCrystalCommand)

struct FocusOnCommand
{
    QPoint pos;
};
Q_DECLARE_METATYPE(FocusOnCommand)

#endif // EDITCOMMANDS_H

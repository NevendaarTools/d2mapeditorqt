#include "CommandBus.h"
#include "Engine/GameInstance.h"
#include "Commands/CommandBus.h"

CommandExecutor::~CommandExecutor()
{
    RESOLVE(CommandBus)->unregisterExecutor(this);
}

void CommandExecutor::registerExecutor(const QString &name, std::function<CommandResult (const QVariant &)> function)
{
    m_executors.insert(name, function);
    RESOLVE(CommandBus)->registerExecutor(name, this);
}

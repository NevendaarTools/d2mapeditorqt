#pragma once
#include <QString>
#include <QMetaType>
#include <QPoint>
#include "MapObjects/MapObject.h"

struct SettingsChangedEvent
{
    QString group;
    QString name;
};
Q_DECLARE_METATYPE(SettingsChangedEvent)

struct DisplaySettingsChangedEvent
{

};
Q_DECLARE_METATYPE(DisplaySettingsChangedEvent)

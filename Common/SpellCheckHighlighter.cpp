#include "SpellCheckHighlighter.h"

#include "SpellCheckHighlighter.h"

#include <QRegularExpression>
#include <QTextCharFormat>


struct HunspellInstance
{
    static HunspellInstance* instance()
    {
        static HunspellInstance inst;
        return &inst;
    }
    static void initInstance(const QString &path)
    {
        instance()->init(path);
    }

    static bool isCorrect(const QString &word)
    {
        return
                instance()->m_checker_en->spell(word) ||
                instance()->m_checker_ru->spell(word);
    }

private:
    HunspellInstance(){}
    QSharedPointer<HunspellChecker> m_checker_en;
    QSharedPointer<HunspellChecker> m_checker_ru;
    bool m_initialised = false;
    void init(const QString &path)
    {
        if (m_initialised)
            return;
        m_checker_en = QSharedPointer<HunspellChecker> (new HunspellChecker());
        m_checker_ru = QSharedPointer<HunspellChecker> (new HunspellChecker());

        m_checker_en->initialize(path + "/libhunspell.dll", path + "/en_US.aff", path + "/en_US.dic");
        m_checker_ru->initialize(path + "/libhunspell.dll", path + "/ru_RU.aff", path + "/ru_RU.dic");
        m_initialised = true;
        qDebug()<<"cat"<<m_checker_en->spell("cat");
        qDebug()<<"catg"<<m_checker_en->spell("catg");
        qDebug()<<"кошка"<<m_checker_ru->spell("кошка");
        qDebug()<<"кошкар"<<m_checker_ru->spell("кошкар");
    }
};

SpellCheckHighlighter::SpellCheckHighlighter(QTextDocument *document)
    : QSyntaxHighlighter(document)
{
    HunspellInstance::initInstance("ExtraDeps");
}

void SpellCheckHighlighter::highlightBlock(const QString &text)
{
    QTextCharFormat errorFormat;
    errorFormat.setForeground(Qt::red);

    QRegularExpression wordRegex("[a-zA-Zа-яА-Я]*");
    QRegularExpressionMatchIterator i = wordRegex.globalMatch(text);

    while (i.hasNext()) {
        QRegularExpressionMatch match = i.next();
        QString word = match.captured();
        if (word.isEmpty())
            continue;
        if (!HunspellInstance::isCorrect(word)) {
            setFormat(match.capturedStart(), match.capturedLength(), errorFormat);
        }
    }
}


void SpellCheckWrapper::setDocument(QQuickTextDocument *document)
{
    if (m_document == document)
        return;

    m_document = document;
    if (m_document) {
        m_highlighter = new SpellCheckHighlighter(m_document->textDocument());
    } else {
        m_highlighter->deleteLater();
    }

    emit documentChanged();
}

SpellCheckWrapper::SpellCheckWrapper(QObject *parent) : QObject(parent), m_document(nullptr) {}

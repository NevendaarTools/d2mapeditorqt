#include "MapJsonIO.h"
#include "../MapObjects/MapObject.h"
#include <QJsonObject>
#include <QJsonArray>
#include <QJsonDocument>
#include <QFile>
#include <QStringList>
#include "../MapObjects/StackObject.h"
#include "../MapObjects/LandmarkObject.h"
#include "../MapObjects/FortObject.h"
#include "../MapObjects/MountainObject.h"
#include "../MapObjects/CrystalObject.h"
#include "../MapObjects/RuinObject.h"
#include "../MapObjects/TreasureObject.h"
#include "../MapObjects/MerchantObject.h"
#include "../MapObjects/LocationObject.h"
#include "../MapObjects/StackTemplateObject.h"
#include "../MapObjects/RodObject.h"
#include "../MapObjects/TombObject.h"
#include "../MapObjects/QuestLine.h"
#include "../MapObjects/ScenVariableObject.h"
#include "Engine/MapView/MinimapHelper.h"
#include "Engine/MapConverter.h"
#include "toolsqt/Common/Logger.h"
#include "Engine/SettingsManager.h"
#include "Engine/Components/AccessorHolder.h"

QPair<int, int> uidFromString(const QString & id)
{
    QStringList idList = id.split(":");
    if (idList.count() == 2)
    {
        return QPair<int, int>(idList[0].toInt(), idList[1].toInt());
    }
    return EMPTY_ID;
}


QString uidToString(const QPair<int, int> & id)
{
    return QString("%1:%2").arg(id.first).arg(id.second);
}

static void saveCurrency(QJsonObject & target, const Currency & data)
{
    target["gold"]   = (int)data.gold;
    target["demons"] = (int)data.demons;
    target["empire"] = (int)data.empire;
    target["undead"] = (int)data.undead;
    target["clans"]  = (int)data.clans;
    target["elves"]  = (int)data.elves;
}

static void loadCurrency(const QJsonObject & target, Currency & data)
{
    data.gold = target["gold"].toInt();
    data.demons = target["demons"].toInt();
    data.empire = target["empire"].toInt();
    data.undead = target["undead"].toInt();
    data.clans = target["clans"].toInt();
    data.elves = target["elves"].toInt();
}

static void saveInventory(QJsonArray & target, const Inventory & data)
{
    for (int i = 0; i < data.items.count(); ++i)
    {
        QJsonObject item;
        item["id"] = data.items[i].type;
        item["count"] = data.items[i].count;
        if (data.items[i].charges > 0)
            item["charges"] = data.items[i].charges;
        target<<item;
    }
}

static void loadInventory(const QJsonArray & target, Inventory & data)
{
    data.items.clear();
    for (int i = 0; i < target.count(); i++)
    {
        const QJsonObject & item = target[i].toObject();
        data.addItem(item["id"].toString(), item["count"].toInt(), item["charges"].toInt());
    }
}

static void saveGroup(QJsonObject & target, const GroupData & data)
{
    target["name"] = data.name;
    QJsonArray units;
    for (int i = 0; i < data.units.count(); ++i)
    {
        QJsonObject unit;
        unit["uid"] = uidToString(data.units[i].first);
        unit["x"] = data.units[i].second.x();
        unit["y"] = data.units[i].second.y();
        units<<unit;
    }
    target["units"] = units;
    QJsonArray inventoryObject;
    saveInventory(inventoryObject, data.inventory);
    target["inventory"] = inventoryObject;
}

static void loadGroup(const QJsonObject & target, GroupData & data)
{
    data.name = target["name"].toString();
    QJsonArray units = target["units"].toArray();
    for (int i = 0; i < units.count(); i++)
    {
        const QJsonObject & unitObj = units[i].toObject();
        QPoint pos(unitObj["x"].toInt(), unitObj["y"].toInt());
        auto uid = uidFromString(unitObj["uid"].toString());
        data.units.append(QPair<QPair<int,int>, QPoint>(uid, pos));
    }
    QJsonArray inventoryObject = target["inventory"].toArray();
    loadInventory(inventoryObject, data.inventory);
}

static void saveStack(QJsonObject & target, QSharedPointer<MapObject> obj)
{
    StackObject * stack = static_cast<StackObject *>(obj.data());
    target["rotation"] = stack->rotation;
    target["order"] = stack->order;
    target["priority"] = stack->priority;
    target["orderTarget"] = uidToString(stack->orderTarget);
    target["owner"] = uidToString(stack->owner);
    target["ignoreAI"] = stack->ignoreAI;
    target["inside"] = uidToString(stack->inside);
    QJsonObject groupObject;
    saveGroup(groupObject, stack->stack);
    target["stack"] = groupObject;
}

static void loadStack(const QJsonObject & target, QSharedPointer<MapObject> obj)
{
    StackObject * stack = static_cast<StackObject *>(obj.data());

    stack->rotation = target["rotation"].toInt();
    stack->order = target["order"].toInt();
    stack->priority = target["priority"].toInt();
    stack->orderTarget = uidFromString(target["orderTarget"].toString());
    stack->owner = uidFromString(target["owner"].toString());
    stack->ignoreAI = target["ignoreAI"].toBool();
    stack->inside = uidFromString(target["inside"].toString());
    loadGroup(target["stack"].toObject(), stack->stack);
}

static void saveStackTemplate(QJsonObject & target, QSharedPointer<MapObject> obj)
{
    StackTemplateObject * stack = static_cast<StackTemplateObject *>(obj.data());
    target["rotation"] = stack->rotation;
    target["order"] = stack->order;
    target["priority"] = stack->priority;
    target["orderTarget"] = uidToString(stack->orderTarget);
    target["owner"] = uidToString(stack->owner);
    target["ignoreAI"] = stack->ignoreAI;
    target["useFacing"] = stack->useFacing;
    QJsonObject groupObject;
    saveGroup(groupObject, stack->stack);
    target["stack"] = groupObject;
}

static void loadStackTemplate(const QJsonObject & target, QSharedPointer<MapObject> obj)
{
    StackTemplateObject * stack = static_cast<StackTemplateObject *>(obj.data());
    stack->rotation = target["rotation"].toInt();
    stack->order = target["order"].toInt();
    stack->priority = target["priority"].toInt();
    stack->orderTarget = uidFromString(target["orderTarget"].toString());
    stack->owner = uidFromString(target["owner"].toString());
    stack->ignoreAI = target["ignoreAI"].toBool();
    stack->useFacing = target["useFacing"].toBool();
    loadGroup(target["stack"].toObject(), stack->stack);
}

static void saveUnit(QJsonObject & target, QSharedPointer<MapObject> obj)
{
    UnitObject * unit = static_cast<UnitObject *>(obj.data());
    target["baseId"] = unit->id;
    target["currentHp"] = unit->currentHp;
    target["currentExp"] = unit->currentExp;
    target["level"] = unit->level;
    target["name"] = unit->name;
    target["leader"] = unit->leader;
    target["modifiers"] = QJsonArray::fromStringList(unit->modifiers);
}

static void loadUnit(const QJsonObject & target, QSharedPointer<MapObject> obj)
{
    UnitObject * unit = static_cast<UnitObject *>(obj.data());
    unit->id = target["baseId"].toString();
    unit->currentHp = target["currentHp"].toInt();
    unit->currentExp = target["currentExp"].toInt();
    unit->level = target["level"].toInt();
    unit->name = target["name"].toString();
    unit->leader = target["leader"].toBool();
    QJsonArray modifiers = target["modifiers"].toArray();
    for (int i = 0; i < modifiers.count(); i++)
    {
        unit->modifiers<<modifiers[i].toString() ;
    }
}

static void saveVillage(QJsonObject & target, QSharedPointer<MapObject> obj)
{
    FortObject * village = static_cast<FortObject *>(obj.data());

    target["level"] = village->level;
    target["name"] = village->name;
    target["desc"] = village->desc;
    target["subrace"] = uidToString(village->subrace);
    target["owner"] = uidToString(village->owner);
    target["visiter"] = uidToString(village->visiter);
    target["raceId"] = village->raceId;
    target["fortType"] = village->fortType;
    QJsonObject garrison;
    saveGroup(garrison, village->garrison);
    target["garrison"] = garrison;
}

static void loadVillage(const QJsonObject & target, QSharedPointer<MapObject> obj)
{
    FortObject * village = static_cast<FortObject *>(obj.data());

    village->level = target["level"].toInt();
    village->name = target["name"].toString();
    village->desc = target["desc"].toString();
    village->owner = uidFromString(target["owner"].toString());
    village->visiter = uidFromString(target["visiter"].toString());
    village->subrace = uidFromString(target["subrace"].toString());
    loadGroup(target["garrison"].toObject(), village->garrison);
    village->raceId = target["raceId"].toString();
    village->fortType = (FortObject::FortType)target["fortType"].toInt();
}

static void saveLandMark(QJsonObject & target, QSharedPointer<MapObject> obj)
{
    LandmarkObject * lmark = static_cast<LandmarkObject *>(obj.data());
    target["lmarkId"] = lmark->lmarkId;
    target["desc"] = lmark->desc;
}

static void loadLandMark(const QJsonObject & target, QSharedPointer<MapObject> obj)
{
    LandmarkObject * lmark = static_cast<LandmarkObject *>(obj.data());
    lmark->lmarkId = target["lmarkId"].toString();
    lmark->desc = target["desc"].toString();
}

static void saveMountain(QJsonObject & target, QSharedPointer<MapObject> obj)
{
    MountainObject * mountain = static_cast<MountainObject *>(obj.data());
    target["w"] = mountain->w;
    target["h"] = mountain->h;
    target["image"] = mountain->image;
    target["race"] = mountain->race;
}

static void loadMountain(const QJsonObject & target, QSharedPointer<MapObject> obj)
{
    MountainObject * mountain = static_cast<MountainObject *>(obj.data());
    mountain->w = target["w"].toInt();
    mountain->h = target["h"].toInt();
    mountain->image = target["image"].toInt();
    mountain->race = target["race"].toInt();
}

static void saveCrystal(QJsonObject & target, QSharedPointer<MapObject> obj)
{
    CrystalObject * crystal = static_cast<CrystalObject *>(obj.data());
    target["resource"] = (int)crystal->resource;
}

static void loadCrystal(const QJsonObject & target, QSharedPointer<MapObject> obj)
{
    CrystalObject * crystal = static_cast<CrystalObject *>(obj.data());
    crystal->resource = (CrystalObject::ResourceType)target["resource"].toInt();
}

static void saveRuin(QJsonObject & target, QSharedPointer<MapObject> obj)
{
    RuinObject * ruin = static_cast<RuinObject *>(obj.data());
    target["name"] = ruin->name;
    target["desc"] = ruin->desc;
    target["image"] = ruin->image;
    target["item"] = ruin->item;
    target["priority"] = ruin->priority;
    QJsonObject reward;
    saveCurrency(reward, ruin->reward);
    target["reward"] = reward;

    QJsonObject guards;
    saveGroup(guards, ruin->guards);
    target["guards"] = guards;
}

static void loadRuin(const QJsonObject & target, QSharedPointer<MapObject> obj)
{
    RuinObject * ruin = static_cast<RuinObject *>(obj.data());
    ruin->name = target["name"].toString();
    ruin->desc = target["desc"].toString();
    ruin->image = target["image"].toInt();
    ruin->item = target["item"].toString();
    ruin->priority = ruin->priority = target["priority"].toInt();
    loadCurrency(target["reward"].toObject(), ruin->reward);
    loadGroup(target["guards"].toObject(), ruin->guards);
}

static void saveTreasure(QJsonObject & target, QSharedPointer<MapObject> obj)
{
    TreasureObject * treasure = static_cast<TreasureObject *>(obj.data());
    target["image"] = treasure->image;
    target["AIpriority"] = treasure->AIpriority;
    QJsonArray items;
    saveInventory(items, treasure->inventory);
    target["items"] = items;
}

static void loadTreasure(const QJsonObject & target, QSharedPointer<MapObject> obj)
{
    TreasureObject * treasure = static_cast<TreasureObject *>(obj.data());
    treasure->image = target["image"].toInt();
    treasure->AIpriority = target["AIpriority"].toInt();
    loadInventory(target["items"].toArray(), treasure->inventory);
}

static void saveMerchant(QJsonObject & target, QSharedPointer<MapObject> obj)
{
    MerchantObject * merchant = static_cast<MerchantObject *>(obj.data());
    target["type"] = (int)merchant->type;
    target["name"] = merchant->name;
    target["desc"] = merchant->desc;
    target["image"] = merchant->image;
    target["priority"] = merchant->priority;
    target["spells"] = QJsonArray::fromStringList(merchant->spells);

    QJsonArray items;
    saveInventory(items, merchant->inventory);
    target["items"] = items;

    QJsonArray units;
    for (int i = 0; i < merchant->units.items.count(); ++i)
    {
        QJsonObject unit;
        unit["id"] = merchant->units.items[i].first.id;
        unit["level"] = merchant->units.items[i].first.level;
        unit["mods"] = QJsonArray::fromStringList(merchant->units.items[i].first.modifiers);
        unit["count"] = merchant->units.items[i].second;
        units << unit;
    }
    target["units"] = units;
}

static void loadMerchant(const QJsonObject & target, QSharedPointer<MapObject> obj)
{
    MerchantObject * merchant = static_cast<MerchantObject *>(obj.data());
    merchant->type = (MerchantObject::Type) target["type"].toInt();
    merchant->name = target["name"].toString();
    merchant->desc = target["desc"].toString() ;
    merchant->image = target["image"].toInt();
    merchant->priority = target["priority"].toInt();

    QJsonArray spells = target["spells"].toArray();
    for (int i = 0; i < spells.count(); i++)
    {
        merchant->spells<<spells[i].toString() ;
    }
    loadInventory(target["items"].toArray(), merchant->inventory);

    QJsonArray units = target["units"].toArray();
    for (int i = 0; i < units.count(); ++i)
    {
        const QJsonObject & item = units[i].toObject();
        UnitObject unit;
        unit.id = item["id"].toString();
        unit.level = item["level"].toInt();
        QJsonArray mods = target["mods"].toArray();
        for (int i = 0; i < mods.count(); i++)
        {
            unit.modifiers<<mods[i].toString() ;
        }

        merchant->units.addItem(unit, item["count"].toInt());
    }
}

static void saveLocation(QJsonObject & target, QSharedPointer<MapObject> obj)
{
    LocationObject * location = static_cast<LocationObject *>(obj.data());
    target["name"] = location->name;
    target["radius"] = location->r;
}

static void loadLocation(const QJsonObject & target, QSharedPointer<MapObject> obj)
{
    LocationObject * location = static_cast<LocationObject *>(obj.data());
    location->name = target["name"].toString();
    location->r = target["radius"].toInt();
}

static void saveSubrace(QJsonObject & target, QSharedPointer<MapObject> obj)
{
    auto subRace = static_cast<SubRaceObject*>(obj.data());
    target["playerUid"] = uidToString(subRace->playerUid);
    target["name"] = subRace->name;
    target["banner"] = subRace->banner;
    target["number"] = subRace->number;
    target["subrace"] = subRace->subrace;
}

static void loadSubrace(const QJsonObject & target, QSharedPointer<MapObject> obj)
{
    auto subRace = static_cast<SubRaceObject*>(obj.data());
    subRace->playerUid = uidFromString(target["playerUid"].toString());
    subRace->name = target["name"].toString();
    subRace->banner = target["banner"].toInt();
    subRace->number = target["number"].toInt();
    subRace->subrace = target["subrace"].toInt();
}

static void savePlayer(QJsonObject & target, QSharedPointer<MapObject> obj)
{
    auto player = static_cast<PlayerObject*>(obj.data());
    QJsonObject bank;
    saveCurrency(bank, player->bank);
    target["bank"] = bank;
    target["name"] = player->name;
    target["desc"] = player->desc;
    target["lordCat"] = static_cast<int>(player->lordCat);
    target["raceId"] = player->raceId;
    target["spellbook"] = QJsonArray::fromStringList(player->spellbook);
    target["buildings"] = QJsonArray::fromStringList(player->buildings);
    target["isHuman"] = player->isHuman;
    target["alwaysAI"] = player->alwaysAI;
    target["researchThisTurn"] = player->researchThisTurn;
    target["buildThisTurn"] = player->buildThisTurn;
    target["face"] = player->face;
    target["attitude"] = player->attitude;
}

static void loadPlayer(const QJsonObject & target, QSharedPointer<MapObject> obj)
{
    auto player = static_cast<PlayerObject*>(obj.data());
    loadCurrency(target["bank"].toObject(), player->bank);
    player->name = target["name"].toString();
    player->desc = target["desc"].toString();
    player->lordCat = (Glord::Category)target["lordCat"].toInt();
    player->raceId = target["raceId"].toString();
    QJsonArray spellbook = target["spellbook"].toArray();
    for (int i = 0; i < spellbook.count(); i++)
    {
        player->spellbook<<spellbook[i].toString() ;
    }
    QJsonArray buildings = target["buildings"].toArray();
    for (int i = 0; i < buildings.count(); i++)
    {
        player->buildings<<buildings[i].toString() ;
    }
    player->isHuman = target["isHuman"].toBool();
    player->alwaysAI = target["alwaysAI"].toBool();
    player->researchThisTurn = target["researchThisTurn"].toBool();
    player->buildThisTurn = target["buildThisTurn"].toBool();
    player->face = target["face"].toInt();
    player->attitude = target["attitude"].toInt();
}

static void saveRelation(QJsonObject & target, QSharedPointer<MapObject> obj)
{
    auto relations = static_cast<Relations*>(obj.data());
    QJsonArray relationsArray;
    for (const Relations::RelationEntry &entry : relations->entries) {
        QJsonObject relationObject;
        relationObject["race1"] = IdToString(entry.race1);
        relationObject["race2"] = IdToString(entry.race2);
        relationObject["relation"] = entry.relation;
        relationObject["hasSpy1"] = entry.hasSpy1;
        relationObject["hasSpy2"] = entry.hasSpy2;
        relationsArray.append(relationObject);
    }
    target["entries"] = relationsArray;
}

static void loadRelation(const QJsonObject & target, QSharedPointer<MapObject> obj)
{
    auto relations = static_cast<Relations*>(obj.data());
    QJsonArray relationsArray = target["entries"].toArray();
    for (int i = 0; i < relationsArray.count(); i++)
    {
        Relations::RelationEntry entry;
        QJsonObject relationObject = relationsArray[i].toObject();
        entry.race1 = IdFromString(relationObject["race1"].toString());
        entry.race2 = IdFromString(relationObject["race2"].toString());
        entry.relation = relationObject["relation"].toInt();
        entry.hasSpy1 = relationObject["hasSpy1"].toBool();
        entry.hasSpy2 = relationObject["hasSpy2"].toBool();
        relations->entries << entry;
    }
}

static void saveInfo(QJsonObject & target, QSharedPointer<MapObject> obj)
{
    auto mapInfo = static_cast<MapInfo*>(obj.data());
    target["campaign"] = mapInfo->info.campaign;
    target["sourceM"] = mapInfo->info.sourceM;
    target["qtyCities"] = mapInfo->info.qtyCities;
    target["name"] = mapInfo->info.name;
    target["desc"] = mapInfo->info.desc;
    target["briefing"] = mapInfo->info.briefing;
    target["debunkw"] = mapInfo->info.debunkw;
    target["debunkw2"] = mapInfo->info.debunkw2;
    target["debunkw3"] = mapInfo->info.debunkw3;
    target["debunkw4"] = mapInfo->info.debunkw4;
    target["debunkw5"] = mapInfo->info.debunkw5;
    target["debunkl"] = mapInfo->info.debunkl;
    target["brieflong1"] = mapInfo->info.brieflong1;
    target["brieflong2"] = mapInfo->info.brieflong2;
    target["brieflong3"] = mapInfo->info.brieflong3;
    target["brieflong4"] = mapInfo->info.brieflong4;
    target["brieflong5"] = mapInfo->info.brieflong5;
    target["o"] = mapInfo->info.o;
    target["curTurn"] = mapInfo->info.curTurn;
    target["maxUnit"] = mapInfo->info.maxUnit;
    target["maxSpell"] = mapInfo->info.maxSpell;
    target["maxLeader"] = mapInfo->info.maxLeader;
    target["maxCity"] = mapInfo->info.maxCity;
    target["mapSize"] = mapInfo->info.mapSize;
    target["diffscen"] = mapInfo->info.diffscen;
    target["diffgame"] = mapInfo->info.diffgame;
    target["creator"] = mapInfo->info.creator;
    target["suggLvl"] = mapInfo->info.suggLvl;
    target["mapSeed"] = mapInfo->info.mapSeed;

    // Сериализация массива players
    QJsonArray playersArray;
    for (int i = 0; i < 13; ++i) {
        playersArray.append(mapInfo->info.players[i]);
    }
    target["players"] = playersArray;
}

static void loadInfo(const QJsonObject &source, QSharedPointer<MapObject> obj)
{
    auto mapInfo = static_cast<MapInfo*>(obj.data());

    mapInfo->info.campaign = source["campaign"].toString();
    mapInfo->info.sourceM = source["sourceM"].toBool();
    mapInfo->info.qtyCities = source["qtyCities"].toInt();
    mapInfo->info.name = source["name"].toString();
    mapInfo->info.desc = source["desc"].toString();
    mapInfo->info.briefing = source["briefing"].toString();
    mapInfo->info.debunkw = source["debunkw"].toString();
    mapInfo->info.debunkw2 = source["debunkw2"].toString();
    mapInfo->info.debunkw3 = source["debunkw3"].toString();
    mapInfo->info.debunkw4 = source["debunkw4"].toString();
    mapInfo->info.debunkw5 = source["debunkw5"].toString();
    mapInfo->info.debunkl = source["debunkl"].toString();
    mapInfo->info.brieflong1 = source["brieflong1"].toString();
    mapInfo->info.brieflong2 = source["brieflong2"].toString();
    mapInfo->info.brieflong3 = source["brieflong3"].toString();
    mapInfo->info.brieflong4 = source["brieflong4"].toString();
    mapInfo->info.brieflong5 = source["brieflong5"].toString();
    mapInfo->info.o = source["o"].toBool();
    mapInfo->info.curTurn = source["curTurn"].toInt();
    mapInfo->info.maxUnit = source["maxUnit"].toInt();
    mapInfo->info.maxSpell = source["maxSpell"].toInt();
    mapInfo->info.maxLeader = source["maxLeader"].toInt();
    mapInfo->info.maxCity = source["maxCity"].toInt();
    mapInfo->info.mapSize = source["mapSize"].toInt();
    mapInfo->info.diffscen = source["diffscen"].toInt();
    mapInfo->info.diffgame = source["diffgame"].toInt();
    mapInfo->info.creator = source["creator"].toString();
    mapInfo->info.suggLvl = source["suggLvl"].toInt();
    mapInfo->info.mapSeed = source["mapSeed"].toInt();

    QJsonArray playersArray = source["players"].toArray();
    for (int i = 0; i < playersArray.size(); ++i) {
        if (i < 13) {
            mapInfo->info.players[i] = playersArray[i].toInt();
        }
    }
}


static void saveRod(QJsonObject & target, QSharedPointer<MapObject> obj)
{
    auto rod = static_cast<RodObject*>(obj.data());
    target["owner"] = IdToString(rod->owner);
}

static void loadRod(const QJsonObject & target, QSharedPointer<MapObject> obj)
{
    auto rod = static_cast<RodObject*>(obj.data());
    rod->owner = IdFromString(target["owner"].toString());
}

static void saveTomb(QJsonObject & target, QSharedPointer<MapObject> obj)
{
    auto tomb = static_cast<TombObject*>(obj.data())->tomb;
    QJsonArray entriesArray;
    for (const D2Tomb::TombEntry &entry : qAsConst(tomb.entries)) {
        QJsonObject entryObject;

        entryObject["owner"] = IdToString(entry.owner);
        entryObject["killer"] = IdToString(entry.killer);

        entryObject["turn"] = entry.turn;
        entryObject["name"] = entry.name;

        entriesArray.append(entryObject);
    }
    target["entries"] = entriesArray;
}

static void loadTomb(const QJsonObject &source, QSharedPointer<MapObject> obj)
{
    auto tombObject = static_cast<TombObject*>(obj.data());
    D2Tomb &tomb = tombObject->tomb;
    tomb.entries.clear();

    QJsonArray entriesArray = source["entries"].toArray();
    for (int i = 0; i < entriesArray.size(); ++i) {
        QJsonObject entryObject = entriesArray[i].toObject();

        D2Tomb::TombEntry entry;
        entry.owner = IdFromString(entryObject["owner"].toString());
        entry.killer = IdFromString(entryObject["killer"].toString());
        entry.turn = entryObject["turn"].toInt();
        entry.name = entryObject["name"].toString();

        tomb.entries.append(entry);
    }
}

static void saveQuestLine(QJsonObject & target, QSharedPointer<MapObject> obj)
{
    auto questLine = static_cast<QuestLine *>(obj.data());
    target["name"] = questLine->name;
    target["utility"] = questLine->utility;
    target["display"] = questLine->display;

    QJsonArray stagesArray;
    for (const QuestLine::QuestStage &stage : questLine->stages) {
        QJsonObject stageObject;
        stageObject["name"] = stage.name;
        stageObject["stageText"] = stage.stageText;

        QJsonArray eventsArray;
        for (const QPair<int, int> &event : stage.events) {
            QJsonArray eventPair;
            eventPair.append(event.first);
            eventPair.append(event.second);
            eventsArray.append(eventPair);
        }

        stageObject["events"] = eventsArray;
        stagesArray.append(stageObject);
    }
    target["stages"] = stagesArray;
}

static void loadQuestLine(const QJsonObject &source, QSharedPointer<MapObject> obj)
{
    auto questLine = static_cast<QuestLine*>(obj.data());

    questLine->name = source["name"].toString();
    questLine->utility = source["utility"].toBool();
    questLine->display = source["display"].toBool();

    questLine->stages.clear();

    QJsonArray stagesArray = source["stages"].toArray();
    for (const QJsonValue &value : stagesArray) {
        QJsonObject stageObject = value.toObject();
        QuestLine::QuestStage stage;

        stage.name = stageObject["name"].toString();
        stage.stageText = stageObject["stageText"].toString();
        QJsonArray eventsArray = stageObject["events"].toArray();
        for (const QJsonValue &eventValue : eventsArray) {
            QJsonArray eventPair = eventValue.toArray();
            if (eventPair.size() == 2) {
                int first = eventPair.at(0).toInt();
                int second = eventPair.at(1).toInt();
                stage.events.append(QPair<int, int>(first, second));
            }
        }

        questLine->stages.append(stage);
    }
}

static void saveCondition(const D2Event::EventCondition & condit, QJsonObject & target)
{
    D2Event::EventCondition::ConditionType category = condit.category;
    auto condition = condit.condition;
    target["category"] = (int)category;
    switch (category)
    {
    case D2Event::EventCondition::RESOURCE_AMOUNT:{
        D2Event::EventCondition::ConditionResourceAmount cond;
        target["BANK"] = cond.bank;
        target["GREATER_OR_EQUAL"] = cond.greaterOrEqual;
        break;
    }
    case D2Event::EventCondition::ConditionType::FREQUENCY:
    {
        auto& cond = std::get<D2Event::EventCondition::ConditionFrequency>(condition);
        target["FREQUENCY"] = cond.frequency;
        break;
    }
    case D2Event::EventCondition::ConditionType::ENTERING_A_PREDEFINED_ZONE:
    {
        auto& cond = std::get<D2Event::EventCondition::ConditionEnterZone>(condition);
        target["ID_LOC"] = cond.locId;
        break;
    }
    case D2Event::EventCondition::ConditionType::ENTERING_A_CITY:
    {
        auto& cond = std::get<D2Event::EventCondition::ConditionEnterCity>(condition);
        target["ID_CITY"] = cond.villageId;
        break;
    }
    case D2Event::EventCondition::ConditionType::OWNING_A_CITY:
    {
        auto& cond = std::get<D2Event::EventCondition::ConditionOwningCity>(condition);
        target["ID_CITY"] = cond.villageId;
        break;
    }
    case D2Event::EventCondition::ConditionType::DESTROY_STACK:
    {
        auto& cond = std::get<D2Event::EventCondition::ConditionDestroyStack>(condition);
        target["ID_STACK"] = cond.stackId;
        break;
    }
    case D2Event::EventCondition::ConditionType::OWNING_AN_ITEM:
    {
        auto& cond = std::get<D2Event::EventCondition::ConditionOwningItem>(condition);
        target["TYPE_ITEM"] = cond.itemType;
        break;
    }
    case D2Event::EventCondition::ConditionType::SPECIFIC_LEADER_OWNING_AN_ITEM:
    {
        auto& cond = std::get<D2Event::EventCondition::ConditionLeaderOwningItem>(condition);
        target["TYPE_ITEM"] = cond.itemType;
        target["ID_STACK"] =  cond.stackId;
        break;
    }
    case D2Event::EventCondition::ConditionType::DIPLOMACY_RELATIONS:
    {
        auto& cond = std::get<D2Event::EventCondition::ConditionDiplomacyRelation>(condition);
        target["ID_PLAYER1"] = cond.player1;
        target["ID_PLAYER2"] = cond.player2;
        target["DIPLOMACY"] = cond.diplomacy;
        break;
    }
    case D2Event::EventCondition::ConditionType::ALLIANCE:
    {
        auto& cond = std::get<D2Event::EventCondition::ConditionAlliance>(condition);
        target["ID_PLAYER1"] = cond.player1;
        target["ID_PLAYER2"] = cond.player2;
        break;
    }
    case D2Event::EventCondition::ConditionType::LOOTING_A_RUIN:
    {
        auto& cond = std::get<D2Event::EventCondition::ConditionLootingRuin>(condition);
        target["ID_RUIN"] = cond.ruinId;
        break;
    }
    case D2Event::EventCondition::ConditionType::TRANSFORMING_LAND:
    {
        auto& cond = std::get<D2Event::EventCondition::ConditionTransformLand>(condition);
        target["PCT_LAND"] = cond.value;
        break;
    }
    case D2Event::EventCondition::ConditionType::VISITING_A_SITE:
    {
        auto& cond = std::get<D2Event::EventCondition::ConditionVisitingSite>(condition);
        target["ID_SITE"] = cond.siteId;
        break;
    }
    case D2Event::EventCondition::ConditionType::STACK_IN_LOCATION:
    {
        auto& cond = std::get<D2Event::EventCondition::ConditionStackInLocation>(condition);
        target["ID_STACK"] = cond.stackId;
        target["ID_LOC"] = cond.locId;
        break;
    }
    case D2Event::EventCondition::STACK_IN_CITY:
    {
        const auto& cond = std::get<D2Event::EventCondition::ConditionStackInCity>(condition);
        target["ID_STACK"] = cond.stackId;
        target["ID_CITY"] = cond.villageId;
        break;
    }
    case D2Event::EventCondition::ITEM_TO_LOCATION:
    {
        const auto& cond = std::get<D2Event::EventCondition::ConditionItemToLocation>(condition);
        target["TYPE_ITEM"] = cond.itemType;
        target["ID_LOC"] =  cond.locId;
        break;
    }
    case D2Event::EventCondition::STACK_EXISTANCE:
    {
        const auto& cond = std::get<D2Event::EventCondition::ConditionStackExist>(condition);
        target["ID_STACK"] = cond.stackId;
        target["MISC_INT"] = cond.exist ? 0 : 1;
        break;
    }
    case D2Event::EventCondition::VARIABLE_IS_IN_RANGE:
    {
        const auto& cond = std::get<D2Event::EventCondition::ConditionVarInRange>(condition);
        target["MISC_INT"] = cond.var1;
        target["MISC_INT2"] = cond.min_1;
        target["MISC_INT3"] =  cond.max_1;
        target["MISC_INT4"] = cond.var2;
        target["MISC_INT5"] = cond.min_2;
        target["MISC_INT6"] = cond.max_2;
        target["MISC_INT7"] = cond.relation;
        break;
    }
    case D2Event::EventCondition::COMPARE_VAR:
    {
        const auto& cond = std::get<D2Event::EventCondition::ConditionCompareVar>(condition);
        target["VAR_1"] = cond.var1;
        target["VAR_2"] = cond.var2;
        target["CMP"] = (int)cond.relation;
        break;
    }
    case D2Event::EventCondition::CUSTOM_SCRIPT:
    {
        const auto& cond = std::get<D2Event::EventCondition::ConditionCustomScript>(condition);
        target["CODE_LEN"] = cond.code.size();
        target["CODE"] = cond.code;
        target["DESCR_LEN"] = cond.desc.size();
        target["DESCR"] = cond.desc;
        break;
    }
    case D2Event::EventCondition::CHECK_GAMEMODE:
    {
        const auto& cond = std::get<D2Event::EventCondition::ConditionGameMode>(condition);
        target["MODE"] = (int)cond.mode;
        break;
    }
    case D2Event::EventCondition::CHECK_FOR_HUMAN:
    {
        const auto& cond = std::get<D2Event::EventCondition::ConditionPlayerType>(condition);
        target["AI"] = cond.isAI;
        break;
    }
    default:
        qWarning() << "Unsupported condition category: " << category;
        break;
    }
}
void loadCondition(D2Event::EventCondition &condit, const QJsonObject &source)
{
    D2Event::EventCondition::ConditionType category = static_cast<D2Event::EventCondition::ConditionType>(source["category"].toInt());

    switch (category) {
    case D2Event::EventCondition::ConditionType::ENTERING_A_CITY: {
        D2Event::EventCondition::ConditionEnterCity cond;
        cond.villageId = source["ID_CITY"].toString();
        condit.condition = cond;
        break;
    }
    case D2Event::EventCondition::ConditionType::OWNING_A_CITY: {
        D2Event::EventCondition::ConditionOwningCity cond;
        cond.villageId = source["ID_CITY"].toString();
        condit.condition = cond;
        break;
    }
    case D2Event::EventCondition::ConditionType::DESTROY_STACK: {
        D2Event::EventCondition::ConditionDestroyStack cond;
        cond.stackId = source["ID_STACK"].toString();
        condit.condition = cond;
        break;
    }
    case D2Event::EventCondition::ConditionType::DIPLOMACY_RELATIONS: {
        D2Event::EventCondition::ConditionDiplomacyRelation cond;
        cond.player1 = source["ID_PLAYER1"].toString();
        cond.player2 = source["ID_PLAYER2"].toString();
        cond.diplomacy = source["DIPLOMACY"].toInt();
        condit.condition = cond;
        break;
    }
    case D2Event::EventCondition::ConditionType::ALLIANCE: {
        D2Event::EventCondition::ConditionAlliance cond;
        cond.player1 = source["ID_PLAYER1"].toString();
        cond.player2 = source["ID_PLAYER2"].toString();
        condit.condition = cond;
        break;
    }
    case D2Event::EventCondition::ConditionType::LOOTING_A_RUIN: {
        D2Event::EventCondition::ConditionLootingRuin cond;
        cond.ruinId = source["ID_RUIN"].toString();
        condit.condition = cond;
        break;
    }
    case D2Event::EventCondition::ConditionType::TRANSFORMING_LAND: {
        D2Event::EventCondition::ConditionTransformLand cond;
        cond.value = source["PCT_LAND"].toInt();
        condit.condition = cond;
        break;
    }
    case D2Event::EventCondition::ConditionType::VISITING_A_SITE: {
        D2Event::EventCondition::ConditionVisitingSite cond;
        cond.siteId = source["ID_SITE"].toString();
        condit.condition = cond;
        break;
    }
    case D2Event::EventCondition::ConditionType::STACK_IN_LOCATION: {
        D2Event::EventCondition::ConditionStackInLocation cond;
        cond.stackId = source["ID_STACK"].toString();
        cond.locId = source["ID_LOC"].toString();
        condit.condition = cond;
        break;
    }
    case D2Event::EventCondition::ConditionType::STACK_IN_CITY: {
        D2Event::EventCondition::ConditionStackInCity cond;
        cond.stackId = source["ID_STACK"].toString();
        cond.villageId = source["ID_CITY"].toString();
        condit.condition = cond;
        break;
    }
    case D2Event::EventCondition::ConditionType::ITEM_TO_LOCATION: {
        D2Event::EventCondition::ConditionItemToLocation cond;
        cond.itemType = source["TYPE_ITEM"].toString();
        cond.locId = source["ID_LOC"].toString();
        condit.condition = cond;
        break;
    }
    case D2Event::EventCondition::ConditionType::STACK_EXISTANCE: {
        D2Event::EventCondition::ConditionStackExist cond;
        cond.stackId = source["ID_STACK"].toString();
        cond.exist = source["MISC_INT"].toInt() == 0 ? true : false;
        condit.condition = cond;
        break;
    }
    case D2Event::EventCondition::ConditionType::VARIABLE_IS_IN_RANGE: {
        D2Event::EventCondition::ConditionVarInRange cond;
        cond.var1 = source["MISC_INT"].toInt();
        cond.min_1 = source["MISC_INT2"].toInt();
        cond.max_1 = source["MISC_INT3"].toInt();
        cond.var2 = source["MISC_INT4"].toInt();
        cond.min_2 = source["MISC_INT5"].toInt();
        cond.max_2 = source["MISC_INT6"].toInt();
        cond.relation = source["MISC_INT7"].toInt();
        condit.condition = cond;
        break;
    }
    case D2Event::EventCondition::ConditionType::RESOURCE_AMOUNT: {
        D2Event::EventCondition::ConditionResourceAmount cond;
        cond.bank = source["BANK"].toString();
        cond.greaterOrEqual = source["GREATER_OR_EQUAL"].toBool();
        condit.condition = cond;
        break;
    }
    case D2Event::EventCondition::ConditionType::CHECK_GAMEMODE: {
        D2Event::EventCondition::ConditionGameMode cond;
        cond.mode = static_cast<D2Event::EventCondition::ConditionGameMode::GameMode>(source["MODE"].toInt());
        condit.condition = cond;
        break;
    }
    case D2Event::EventCondition::ConditionType::CHECK_FOR_HUMAN: {
        D2Event::EventCondition::ConditionPlayerType cond;
        cond.isAI = source["AI"].toBool();
        condit.condition = cond;
        break;
    }
    case D2Event::EventCondition::ConditionType::COMPARE_VAR: {
        D2Event::EventCondition::ConditionCompareVar cond;
        cond.var1 = source["VAR_1"].toInt();
        cond.var2 = source["VAR_2"].toInt();
        cond.relation = static_cast<D2Event::EventCondition::ConditionCompareVar::CompareType>(source["CMP"].toInt());
        condit.condition = cond;
        break;
    }
    case D2Event::EventCondition::ConditionType::FREQUENCY: {
        D2Event::EventCondition::ConditionFrequency cond;
        cond.frequency = source["FREQUENCY"].toInt();
        condit.condition = cond;
        break;
    }
    case D2Event::EventCondition::ConditionType::ENTERING_A_PREDEFINED_ZONE: {
        D2Event::EventCondition::ConditionEnterZone cond;
        cond.locId = source["ID_LOC"].toString();
        condit.condition = cond;
        break;
    }
    case D2Event::EventCondition::ConditionType::OWNING_AN_ITEM: {
        D2Event::EventCondition::ConditionOwningItem cond;
        cond.itemType = source["TYPE_ITEM"].toString();
        condit.condition = cond;
        break;
    }
    case D2Event::EventCondition::ConditionType::SPECIFIC_LEADER_OWNING_AN_ITEM: {
        D2Event::EventCondition::ConditionLeaderOwningItem cond;
        cond.itemType = source["TYPE_ITEM"].toString();
        cond.stackId = source["ID_STACK"].toString();
        condit.condition = cond;
        break;
    }
    case D2Event::EventCondition::ConditionType::CUSTOM_SCRIPT: {
        D2Event::EventCondition::ConditionCustomScript cond;
        cond.code = source["CODE"].toString();
        cond.desc = source["DESCR"].toString();
        condit.condition = cond;
        break;
    }
    default:
        qWarning() << "Unsupported condition category: " << category;
        break;
    }

    condit.category = category;
}

static void saveEventEffect(const D2Event::EventEffect & eventEffect, QJsonObject & target)
{
    auto category = eventEffect.category;
    target["category"] = (int)category;
    switch (category) {
    case D2Event::EventEffect::WIN_OR_LOSE_SCENARIO:
    {
        const auto& effect = std::get<D2Event::EventEffect::WinLooseEffect>(eventEffect.effect);
        target["NUM"] = effect.num;
        target["WIN_SCEN"] = effect.win;
        target["ID_PLAYER1"] = effect.playerId;
        break;
    }
    case D2Event::EventEffect::CREATE_NEW_STACK:
    {
        const auto& effect = std::get<D2Event::EventEffect::CreateStackEffect>(eventEffect.effect);
        target["NUM"] = effect.num;
        target["ID_STKTEMP"] = effect.templateId;
        target["ID_LOC"] = effect.locId;
        break;
    }
    case D2Event::EventEffect::CAST_SPELL_ON_TRIGGERER:
    {
        const auto& effect = std::get<D2Event::EventEffect::CastSpellOnTriggerEffect>(eventEffect.effect);
        target["NUM"] = effect.num;
        target["TYPE_SPELL"] = effect.spellType;
        target["ID_PLAYER1"] = effect.playerId1;
        break;
    }
    case D2Event::EventEffect::CAST_SPELL_AT_SPECIFIC_LOCATION:
    {
        const auto& effect = std::get<D2Event::EventEffect::CastSpellOnLocationEffect>(eventEffect.effect);
        target["NUM"] = effect.num;
        target["TYPE_SPELL"] = effect.spellType;
        target["ID_LOC"] = effect.locId;
        target["ID_PLAYER1"] = effect.playerId1;
        break;
    }
    case D2Event::EventEffect::CHANGE_STACK_OWNER:
    {
        const auto& effect = std::get<D2Event::EventEffect::ChangeStackOwnerEffect>(eventEffect.effect);
        target["NUM"] = effect.num;
        target["ID_STACK"] = effect.stackId;
        target["ID_PLAYER1"] = effect.playerId1;
        target["FIRST_ONLY"] = effect.firstOnly;
        target["PLAY_ANIM"] = effect.anim;
        break;
    }
    case D2Event::EventEffect::MOVE_STACK_NEXT_TO_TRIGGERER:
    {
        const auto& effect = std::get<D2Event::EventEffect::MoveStackToTriggererEffect>(eventEffect.effect);
        target["NUM"] = effect.num;
        target["ID_STACK"] = effect.stackId;
        break;
    }
    case D2Event::EventEffect::GO_INTO_BATTLE:
    {
        const auto& effect = std::get<D2Event::EventEffect::GoBattleEffect>(eventEffect.effect);
        target["NUM"] = effect.num;
        target["ID_STACK"] = effect.stackId;
        target["FIRST_ONLY"] = effect.firstOnly;
        break;
    }
    case D2Event::EventEffect::ENABLE_DISABLE_ANOTHER_EVENT:
    {
        const auto& effect = std::get<D2Event::EventEffect::DisableEventEffect>(eventEffect.effect);
        target["NUM"] = effect.num;
        target["ID_EVENT"] = effect.eventId;
        target["ENABLE"] = effect.enable;
        break;
    }
    case D2Event::EventEffect::GIVE_SPELL:
    {
        const auto& effect = std::get<D2Event::EventEffect::GiveSpellEffect>(eventEffect.effect);
        target["NUM"] = effect.num;
        target["TYPE_SPELL"] = effect.spellType;
        break;
    }
    case D2Event::EventEffect::GIVE_ITEM:
    {
        const auto& effect = std::get<D2Event::EventEffect::GiveItemEffect>(eventEffect.effect);
        target["NUM"] = effect.num;
        target["GIVETO"] = effect.giveTo;
        target["TYPE_ITEM"] = effect.itemType;
        break;
    }
    case D2Event::EventEffect::MOVE_STACK_TO_SPECIFIC_LOCATION:
    {
        const auto& effect = std::get<D2Event::EventEffect::MoveStackToLocationEffect>(eventEffect.effect);
        target["NUM"] = effect.num;
        target["ID_STKTEMP"] = effect.stackTmpId;
        target["ID_LOC"] = effect.locId;
        target["BOOLVALUE"] = effect.boolVal;
        break;
    }
    case D2Event::EventEffect::ALLY_TWO_AI_PLAYERS:
    {
        const auto& effect = std::get<D2Event::EventEffect::AllyAIPlayersEffect>(eventEffect.effect);
        target["NUM"] = effect.num;
        target["ID_PLAYER1"] = effect.playerId1;
        target["ID_PLAYER2"] = effect.playerId2;
        target["PERMALLI"] = effect.permally;
        break;
    }
    case D2Event::EventEffect::CHANGE_PLAYER_DIPLOMACY_METER:
    {
        const auto& effect = std::get<D2Event::EventEffect::ChangeDiplomacyEffect>(eventEffect.effect);
        target["NUM"] = effect.num;
        target["ID_PLAYER1"] = effect.playerId1;
        target["ID_PLAYER2"] = effect.playerId2;
        target["DIPLOMACY"] = effect.diplomacy;
        target["ENABLE"] = effect.enabled;
        break;
    }
    case D2Event::EventEffect::UNFOG_OR_FOG_AN_AREA_ON_THE_MAP:
    {
        const auto& effect = std::get<D2Event::EventEffect::ChangeFogEffect>(eventEffect.effect);
        target["NUM"] = effect.num;
        target["ID_LOC"] = effect.locId;
        QJsonArray eventsArray;
        for (const auto& entry : effect.events)
        {
            QJsonObject eventPair;
            eventPair["EVENT_ID"] = entry.eventId;
            eventPair["PLAYER"] = entry.player;
            eventsArray.append(eventPair);
        }
        target["EVENTS"] = eventsArray;
        target["ENABLE"] = effect.enable;
        target["NUMVALUE"] = effect.value;
        break;
    }
    case D2Event::EventEffect::REMOVE_MOUNTAINS_AROUND_A_LOCATION:
    {
        const auto& effect = std::get<D2Event::EventEffect::RemoveMountainsEffect>(eventEffect.effect);
        target["NUM"] = effect.num;
        target["ID_LOC"] = effect.locId;
        break;
    }
    case D2Event::EventEffect::REMOVE_LANDMARK:
    {
        const auto& effect = std::get<D2Event::EventEffect::RemoveLandMarkEffect>(eventEffect.effect);
        target["NUM"] = effect.num;
        target["ID_LMARK"] = effect.lmarkId;
        target["BOOLVALUE"] = effect.boolVal;
        break;
    }
    case D2Event::EventEffect::CHANGE_SCENARIO_OBJECTIVE_TEXT:
    {
        const auto& effect = std::get<D2Event::EventEffect::ChangeScenarioTextEffect>(eventEffect.effect);
        target["NUM"] = effect.num;
        target["OBJECT_TXT"] = effect.text;
        break;
    }
    case D2Event::EventEffect::DISPLAY_POPUP_MESSAGE:
    {
        const auto& effect = std::get<D2Event::EventEffect::DisplayPopupEffect>(eventEffect.effect);
        target["NUM"] = effect.num;
        target["POPUP_TXT"] = effect.text;
        target["MUSIC"] = effect.music;
        target["SOUND"] = effect.sound;
        target["IMAGE"] = effect.image;
        target["IMAGE2"] = effect.image2;
        target["LEFT_SIDE"] = effect.leftSide;
        target["POPUP_SHOW"] = effect.popupShow;
        target["BOOLVALUE"] = effect.boolValue;
        break;
    }
    case D2Event::EventEffect::CHANGE_STACK_ORDER:
    {
        const auto& effect = std::get<D2Event::EventEffect::ChangeStackOrderEffect>(eventEffect.effect);
        target["NUM"] = effect.num;
        target["ID_STACK"] = effect.stackId;
        target["ORDER_TARG"] = effect.orderTarget;
        target["FIRST_ONLY"] = effect.firstOnly;
        target["ORDER"] = effect.order;
        break;
    }
    case D2Event::EventEffect::DESTROY_ITEM:
    {
        const auto& effect = std::get<D2Event::EventEffect::DestroyItemEffect>(eventEffect.effect);
        target["NUM"] = effect.num;
        target["TYPE_ITEM"] = effect.itemType;
        target["TRIG_ONLY"] = effect.triggerOnly;
        break;
    }
    case D2Event::EventEffect::REMOVE_STACK:
    {
        const auto& effect = std::get<D2Event::EventEffect::RemoveStackEffect>(eventEffect.effect);
        target["NUM"] = effect.num;
        target["ID_STACK"] = effect.stackId;
        target["FIRST_ONLY"] = effect.firstOnly;
        break;
    }
    case D2Event::EventEffect::CHANGE_LANDMARK:
    {
        const auto& effect = std::get<D2Event::EventEffect::ChangeLandmarkEffect>(eventEffect.effect);
        target["NUM"] = effect.num;
        target["ID_LMARK"] = effect.lmarkId;
        target["TYPE_LMARK"] = effect.lmarkType;
        break;
    }
    case D2Event::EventEffect::CHANGE_TERRAIN:
    {
        const auto& effect = std::get<D2Event::EventEffect::ChangeTerrainEffect>(eventEffect.effect);
        target["NUM"] = effect.num;
        target["ID_LOC"] = effect.locId;
        target["LOOKUP"] = effect.lookup;
        target["NUMVALUE"] = effect.value;
        break;
    }
    case D2Event::EventEffect::MODIFY_VARIABLE:
    {
        const auto& effect = std::get<D2Event::EventEffect::ModifyVarEffect>(eventEffect.effect);
        target["NUM"] = effect.num;
        target["LOOKUP"] = effect.lookup;
        target["NUMVALUE"] = effect.val1;
        target["NUMVALUE2"] = effect.val2;
        break;
    }
    default:
    {
        break;
    }
    }
}

void loadEventEffect(D2Event::EventEffect &eventEffect, const QJsonObject &source)
{
    D2Event::EventEffect::EffectType category = static_cast<D2Event::EventEffect::EffectType>(source["category"].toInt());
    eventEffect.category = category;

    switch (category) {
    case D2Event::EventEffect::CAST_SPELL_ON_TRIGGERER: {
        D2Event::EventEffect::CastSpellOnTriggerEffect effect;
        effect.num = source["NUM"].toInt();
        effect.spellType = source["TYPE_SPELL"].toString();
        effect.playerId1 = source["ID_PLAYER1"].toString();
        eventEffect.effect = effect;
        break;
    }
    case D2Event::EventEffect::CAST_SPELL_AT_SPECIFIC_LOCATION: {
        D2Event::EventEffect::CastSpellOnLocationEffect effect;
        effect.num = source["NUM"].toInt();
        effect.spellType = source["TYPE_SPELL"].toString();
        effect.locId = source["ID_LOC"].toString();
        effect.playerId1 = source["ID_PLAYER1"].toString();
        eventEffect.effect = effect;
        break;
    }
    case D2Event::EventEffect::CHANGE_STACK_OWNER: {
        D2Event::EventEffect::ChangeStackOwnerEffect effect;
        effect.num = source["NUM"].toInt();
        effect.stackId = source["ID_STACK"].toString();
        effect.playerId1 = source["ID_PLAYER1"].toString();
        effect.firstOnly = source["FIRST_ONLY"].toBool();
        effect.anim = source["PLAY_ANIM"].toBool();
        eventEffect.effect = effect;
        break;
    }
    case D2Event::EventEffect::MOVE_STACK_NEXT_TO_TRIGGERER: {
        D2Event::EventEffect::MoveStackToTriggererEffect effect;
        effect.num = source["NUM"].toInt();
        effect.stackId = source["ID_STACK"].toString();
        eventEffect.effect = effect;
        break;
    }
    case D2Event::EventEffect::GO_INTO_BATTLE: {
        D2Event::EventEffect::GoBattleEffect effect;
        effect.num = source["NUM"].toInt();
        effect.stackId = source["ID_STACK"].toString();
        effect.firstOnly = source["FIRST_ONLY"].toBool();
        eventEffect.effect = effect;
        break;
    }
    case D2Event::EventEffect::ENABLE_DISABLE_ANOTHER_EVENT: {
        D2Event::EventEffect::DisableEventEffect effect;
        effect.num = source["NUM"].toInt();
        effect.eventId = source["ID_EVENT"].toString();
        effect.enable = source["ENABLE"].toBool();
        eventEffect.effect = effect;
        break;
    }
    case D2Event::EventEffect::GIVE_SPELL: {
        D2Event::EventEffect::GiveSpellEffect effect;
        effect.num = source["NUM"].toInt();
        effect.spellType = source["TYPE_SPELL"].toString();
        eventEffect.effect = effect;
        break;
    }
    case D2Event::EventEffect::GIVE_ITEM: {
        D2Event::EventEffect::GiveItemEffect effect;
        effect.num = source["NUM"].toInt();
        effect.giveTo = source["GIVETO"].toInt();
        effect.itemType = source["TYPE_ITEM"].toString();
        eventEffect.effect = effect;
        break;
    }
    case D2Event::EventEffect::MOVE_STACK_TO_SPECIFIC_LOCATION: {
        D2Event::EventEffect::MoveStackToLocationEffect effect;
        effect.num = source["NUM"].toInt();
        effect.stackTmpId = source["ID_STKTEMP"].toString();
        effect.locId = source["ID_LOC"].toString();
        effect.boolVal = source["BOOLVALUE"].toBool();
        eventEffect.effect = effect;
        break;
    }
    case D2Event::EventEffect::ALLY_TWO_AI_PLAYERS: {
        D2Event::EventEffect::AllyAIPlayersEffect effect;
        effect.num = source["NUM"].toInt();
        effect.playerId1 = source["ID_PLAYER1"].toString();
        effect.playerId2 = source["ID_PLAYER2"].toString();
        effect.permally = source["PERMALLI"].toBool();
        eventEffect.effect = effect;
        break;
    }
    case D2Event::EventEffect::CHANGE_PLAYER_DIPLOMACY_METER: {
        D2Event::EventEffect::ChangeDiplomacyEffect effect;
        effect.num = source["NUM"].toInt();
        effect.playerId1 = source["ID_PLAYER1"].toString();
        effect.playerId2 = source["ID_PLAYER2"].toString();
        effect.diplomacy = source["DIPLOMACY"].toInt();
        effect.enabled = source["ENABLE"].toBool();
        eventEffect.effect = effect;
        break;
    }
    case D2Event::EventEffect::UNFOG_OR_FOG_AN_AREA_ON_THE_MAP: {
        D2Event::EventEffect::ChangeFogEffect effect;
        effect.num = source["NUM"].toInt();
        effect.locId = source["ID_LOC"].toString();
        QJsonArray eventsArray = source["EVENTS"].toArray();
        for (const auto &entryValue : eventsArray) {
            QJsonObject entryObject = entryValue.toObject();
            D2Event::EventEffect::ChangeFogEffect::FogEntry entry;
            entry.eventId = entryObject["EVENT_ID"].toString();
            entry.player = entryObject["PLAYER"].toString();
            effect.events.append(entry);
        }
        effect.enable = source["ENABLE"].toBool();
        effect.value = source["NUMVALUE"].toInt();
        eventEffect.effect = effect;
        break;
    }
    case D2Event::EventEffect::REMOVE_MOUNTAINS_AROUND_A_LOCATION: {
        D2Event::EventEffect::RemoveMountainsEffect effect;
        effect.num = source["NUM"].toInt();
        effect.locId = source["ID_LOC"].toString();
        eventEffect.effect = effect;
        break;
    }
    case D2Event::EventEffect::REMOVE_LANDMARK: {
        D2Event::EventEffect::RemoveLandMarkEffect effect;
        effect.num = source["NUM"].toInt();
        effect.lmarkId = source["ID_LMARK"].toString();
        effect.boolVal = source["BOOLVALUE"].toBool();
        eventEffect.effect = effect;
        break;
    }
    case D2Event::EventEffect::CHANGE_SCENARIO_OBJECTIVE_TEXT: {
        D2Event::EventEffect::ChangeScenarioTextEffect effect;
        effect.num = source["NUM"].toInt();
        effect.text = source["OBJECT_TXT"].toString();
        eventEffect.effect = effect;
        break;
    }
    case D2Event::EventEffect::DISPLAY_POPUP_MESSAGE: {
        D2Event::EventEffect::DisplayPopupEffect effect;
        effect.num = source["NUM"].toInt();
        effect.text = source["POPUP_TXT"].toString();
        effect.music = source["MUSIC"].toString();
        effect.sound = source["SOUND"].toString();
        effect.image = source["IMAGE"].toString();
        effect.image2 = source["IMAGE2"].toString();
        effect.leftSide = source["LEFT_SIDE"].toBool();
        effect.popupShow = source["POPUP_SHOW"].toString();
        effect.boolValue = source["BOOLVALUE"].toBool();
        eventEffect.effect = effect;
        break;
    }
    case D2Event::EventEffect::CHANGE_STACK_ORDER: {
        D2Event::EventEffect::ChangeStackOrderEffect effect;
        effect.num = source["NUM"].toInt();
        effect.stackId = source["ID_STACK"].toString();
        effect.orderTarget = source["ORDER_TARG"].toString();
        effect.firstOnly = source["FIRST_ONLY"].toBool();
        effect.order = source["ORDER"].toInt();
        eventEffect.effect = effect;
        break;
    }
    case D2Event::EventEffect::DESTROY_ITEM: {
        D2Event::EventEffect::DestroyItemEffect effect;
        effect.num = source["NUM"].toInt();
        effect.itemType = source["TYPE_ITEM"].toString();
        effect.triggerOnly = source["TRIG_ONLY"].toBool();
        eventEffect.effect = effect;
        break;
    }
    case D2Event::EventEffect::WIN_OR_LOSE_SCENARIO: {
        D2Event::EventEffect::WinLooseEffect effect;
        effect.num = source["NUM"].toInt();
        effect.win = source["WIN_SCEN"].toBool();
        effect.playerId = source["ID_PLAYER1"].toString();
        eventEffect.effect = effect;
        break;
    }
    case D2Event::EventEffect::CREATE_NEW_STACK: {
        D2Event::EventEffect::CreateStackEffect effect;
        effect.num = source["NUM"].toInt();
        effect.templateId = source["ID_STKTEMP"].toString();
        effect.locId = source["ID_LOC"].toString();
        eventEffect.effect = effect;
        break;
    }
    case D2Event::EventEffect::REMOVE_STACK: {
        D2Event::EventEffect::RemoveStackEffect effect;
        effect.num = source["NUM"].toInt();
        effect.stackId = source["ID_STACK"].toString();
        effect.firstOnly = source["FIRST_ONLY"].toBool();
        eventEffect.effect = effect;
        break;
    }
    case D2Event::EventEffect::CHANGE_LANDMARK: {
        D2Event::EventEffect::ChangeLandmarkEffect effect;
        effect.num = source["NUM"].toInt();
        effect.lmarkId = source["ID_LMARK"].toString();
        effect.lmarkType = source["TYPE_LMARK"].toString();
        eventEffect.effect = effect;
        break;
    }
    case D2Event::EventEffect::CHANGE_TERRAIN: {
        D2Event::EventEffect::ChangeTerrainEffect effect;
        effect.num = source["NUM"].toInt();
        effect.locId = source["ID_LOC"].toString();
        effect.lookup = source["LOOKUP"].toInt();
        effect.value = source["NUMVALUE"].toInt();
        eventEffect.effect = effect;
        break;
    }
    case D2Event::EventEffect::MODIFY_VARIABLE: {
        D2Event::EventEffect::ModifyVarEffect effect;
        effect.num = source["NUM"].toInt();
        effect.lookup = source["LOOKUP"].toInt();
        effect.val1 = source["NUMVALUE"].toInt();
        effect.val2 = source["NUMVALUE2"].toInt();
        eventEffect.effect = effect;
        break;
    }
    default:
        qWarning() << "Unsupported effect category: " << category;
        break;
    }
}


static void saveEvent(QJsonObject & target, QSharedPointer<MapObject> obj)
{
    auto eventObject = static_cast<EventObject *>(obj.data());
    target["enabled"] = eventObject->enabled;
    target["occurOnce"] = eventObject->occurOnce;
    target["chance"] = eventObject->chance;
    target["order"] = eventObject->order;
    target["name"] = eventObject->name;

    QJsonArray activatedByArray;
    for (int i = 0; i < 6; ++i) {
        activatedByArray.append(eventObject->activatedBy[i]);
    }
    target["activatedBy"] = activatedByArray;
    QJsonArray effectsToArray;
    for (int i = 0; i < 6; ++i) {
        effectsToArray.append(eventObject->effectsTo[i]);
    }
    target["effectsTo"] = effectsToArray;

    QJsonArray conditionsArray;
    for (const auto &condition : qAsConst(eventObject->conditions)) {
        QJsonObject conditionObject;
        saveCondition(condition.condition, conditionObject);
        conditionsArray.append(conditionObject);
    }
    target["conditions"] = conditionsArray;

    QJsonArray effectsArray;
    for (const auto &effect : qAsConst(eventObject->effects)) {
        QJsonObject effectObject;
        saveEventEffect(effect.effect, effectObject);
        effectsArray.append(effectObject);
    }
    target["effects"] = effectsArray;
}

static void loadEvent(const QJsonObject &source, QSharedPointer<MapObject> obj)
{
    auto eventObject = static_cast<EventObject*>(obj.data());

    eventObject->enabled = source["enabled"].toBool();
    eventObject->occurOnce = source["occurOnce"].toBool();
    eventObject->chance = source["chance"].toInt();
    eventObject->order = source["order"].toInt();
    eventObject->name = source["name"].toString();

    QJsonArray activatedByArray = source["activatedBy"].toArray();
    for (int i = 0; i < activatedByArray.size() && i < 6; ++i) {
        eventObject->activatedBy[i] = activatedByArray[i].toBool();
    }

    QJsonArray effectsToArray = source["effectsTo"].toArray();
    for (int i = 0; i < effectsToArray.size() && i < 6; ++i) {
        eventObject->effectsTo[i] = effectsToArray[i].toBool();
    }

    QJsonArray conditionsArray = source["conditions"].toArray();
    for (const QJsonValue &conditionValue : conditionsArray) {
        D2Event::EventCondition condition;
        QJsonObject conditionObject = conditionValue.toObject();
        loadCondition(condition, conditionObject);
        EventObject::EventCondition result;
        result.condition = condition;
        MapConverter::fillCondition(result);
        eventObject->conditions.append(result);
    }

    QJsonArray effectsArray = source["effects"].toArray();
    for (const QJsonValue &effectValue : effectsArray) {
        D2Event::EventEffect effect;
        QJsonObject effectObject = effectValue.toObject();
        loadEventEffect(effect, effectObject);
        EventObject::EventEffect result;
        result.effect = effect;
        MapConverter::fillEffectTargets(result);
        eventObject->effects.append(result);
    }
}

void saveScenVariable(QJsonObject &target, QSharedPointer<MapObject> obj) {
    auto scenVariable = static_cast<ScenVariableObject*>(obj.data());
    target["name"] = scenVariable->name;
    target["desc"] = scenVariable->desc;
    target["value"] = scenVariable->value;
}

void loadScenVariable(const QJsonObject &source, QSharedPointer<MapObject> obj) {
    auto scenVariable = static_cast<ScenVariableObject*>(obj.data());
    scenVariable->name = source["name"].toString();
    scenVariable->desc = source["desc"].toString();
    scenVariable->value = source["value"].toInt();
}


void saveObject(QJsonObject & target, QSharedPointer<MapObject> obj)
{
    MapObject::Type objType = MapObject::Type (obj->uid.first);
    switch (objType) {
    case MapObject::Stack:
        saveStack(target, obj);
        break;
    case MapObject::StackTemplate:
        saveStackTemplate(target, obj);
        break;
    case MapObject::Fort:
        saveVillage(target, obj);
        break;
    case MapObject::LandMark:
        saveLandMark(target, obj);
        break;
    case MapObject::Mountain:
        saveMountain(target, obj);
        break;
    case MapObject::Crystal:
        saveCrystal(target, obj);
        break;
    case MapObject::Ruin:
        saveRuin(target, obj);
        break;
    case MapObject::Treasure:
        saveTreasure(target, obj);
        break;
    case MapObject::Merchant:
        saveMerchant(target, obj);
        break;
    case MapObject::Location:
        saveLocation(target, obj);
        break;
    case MapObject::Event:
        saveEvent(target, obj);
        break;
    case MapObject::Unit:
        saveUnit(target, obj);
        break;
    case MapObject::SubRace:
        saveSubrace(target, obj);
        break;
    case MapObject::Player:
        savePlayer(target, obj);
        break;
    case MapObject::Info:
        saveInfo(target, obj);
        break;
    case MapObject::ScenVariable:
        saveScenVariable(target, obj);
        break;
    case MapObject::Relation:
        saveRelation(target, obj);
        break;
    case MapObject::Rod:
        saveRod(target, obj);
        break;
    case MapObject::Tomb:
        saveTomb(target, obj);
        break;
    case MapObject::QuestLine:
        saveQuestLine(target, obj);
        break;
    default:
        break;
    }
}

QSharedPointer<MapObject> loadObject(const QJsonObject & inobject, MapObject::Type type)
{
    MapObject::Type objType = static_cast<MapObject::Type>(type);

    QSharedPointer<MapObject> obj;

    switch (objType) {
    case MapObject::LandMark:{
        LandmarkObject * stack = new LandmarkObject();
        obj = QSharedPointer<MapObject>(stack);
        loadLandMark(inobject, obj);
        break;
    }
    case MapObject::Fort:{
        FortObject * stack = new FortObject();
        obj = QSharedPointer<MapObject>(stack);
        loadVillage(inobject, obj);
        break;
    }
    case MapObject::Mountain:{
        MountainObject * stack = new MountainObject();
        obj = QSharedPointer<MapObject>(stack);
        loadMountain(inobject, obj);
        break;
    }
    case MapObject::Crystal:{
        CrystalObject * stack = new CrystalObject();
        obj = QSharedPointer<MapObject>(stack);
        loadCrystal(inobject, obj);
        break;
    }
    case MapObject::Ruin:{
        RuinObject * stack = new RuinObject();
        obj = QSharedPointer<MapObject>(stack);
        loadRuin(inobject, obj);
        break;
    }
    case MapObject::Treasure:{
        TreasureObject * stack = new TreasureObject();
        obj = QSharedPointer<MapObject>(stack);
        loadTreasure(inobject, obj);
        break;
    }
    case MapObject::Merchant:{
        MerchantObject * stack = new MerchantObject();
        obj = QSharedPointer<MapObject>(stack);
        loadMerchant(inobject, obj);
        break;
    }
    case MapObject::Location:{
        LocationObject * stack = new LocationObject();
        obj = QSharedPointer<MapObject>(stack);
        loadLocation(inobject, obj);
        break;
    }
    case MapObject::Unit: {
        UnitObject *unit = new UnitObject();
        obj = QSharedPointer<MapObject>(unit);
        loadUnit(inobject, obj);
        break;
    }
    case MapObject::SubRace: {
        SubRaceObject *subRace = new SubRaceObject();
        obj = QSharedPointer<MapObject>(subRace);
        loadSubrace(inobject, obj);
        break;
    }
    case MapObject::Player: {
        PlayerObject *player = new PlayerObject();
        obj = QSharedPointer<MapObject>(player);
        loadPlayer(inobject, obj);
        break;
    }
    case MapObject::Info: {
        MapInfo *info = new MapInfo();
        obj = QSharedPointer<MapObject>(info);
        loadInfo(inobject, obj);
        break;
    }
    case MapObject::ScenVariable: {
        ScenVariableObject *scenVariable = new ScenVariableObject();
        obj = QSharedPointer<MapObject>(scenVariable);
        loadScenVariable(inobject, obj);
        break;
    }
    case MapObject::Relation: {
        Relations *relation = new Relations();
        obj = QSharedPointer<MapObject>(relation);
        loadRelation(inobject, obj);
        break;
    }
    case MapObject::Rod: {
        RodObject *rod = new RodObject();
        obj = QSharedPointer<MapObject>(rod);
        loadRod(inobject, obj);
        break;
    }
    case MapObject::Tomb: {
        TombObject *tomb = new TombObject();
        obj = QSharedPointer<MapObject>(tomb);
        loadTomb(inobject, obj);
        break;
    }
    case MapObject::QuestLine: {
        QuestLine *questLine = new QuestLine();
        obj = QSharedPointer<MapObject>(questLine);
        loadQuestLine(inobject, obj);
        break;
    }
    case MapObject::Stack: {
        StackObject *stack = new StackObject();
        obj = QSharedPointer<MapObject>(stack);
        loadStack(inobject, obj);
        break;
    }
    case MapObject::StackTemplate: {
        StackTemplateObject *stackTemplate = new StackTemplateObject();
        obj = QSharedPointer<MapObject>(stackTemplate);
        loadStackTemplate(inobject, obj);
        break;
    }
    case MapObject::Event: {
        EventObject *event = new EventObject();
        obj = QSharedPointer<MapObject>(event);
        loadEvent(inobject, obj);
        break;
    }
    default:
        break;
    }

    return obj;
}


MapJsonIO::MapJsonIO()
{

}

bool MapJsonIO::save(GameMap & map, const QString &path)
{
    QFileInfo fileInfo(path);
    qDebug()<<fileInfo.absolutePath();
    qDebug()<<Q_FUNC_INFO<<path;
    QDir parentDirectory(fileInfo.absolutePath());
    if (!parentDirectory.exists(path)) {
        if (!parentDirectory.mkpath(path)) {
            Logger::logError("Error creating dir: " + path);
            return false;
        }
    }
    QDir mapDirectory(path);
    if (!mapDirectory.exists("sounds")) {
        if (!mapDirectory.mkdir("sounds")) {
            Logger::logError("Error creating dir sounds in " + path);
            return false;
        }
    }
    static SettingsManager settingsManager;
    //map info
    QFile mapInfoFile(path + "/mapInfo.json");
    if (mapInfoFile.open(QIODevice::WriteOnly))
    {
        QJsonObject root;
        QJsonObject mapInfo;
        mapInfo["name"] = map.getMapName();
        mapInfo["desc"] = map.getMapDesc();
        mapInfo["size"] = map.getGrid().cells.count();
        mapInfo["version"] = map.getVersion();

        root["mapInfo"] = mapInfo;
        QJsonDocument jsonDocument(root);
        mapInfoFile.write(jsonDocument.toJson(QJsonDocument::Indented));
        mapInfoFile.close();

    } else {
        Logger::logError("Error opening file: " + mapInfoFile.fileName());
        return false;
    }
    //settings
    settingsManager.setSettings(map.settings());
    if (!settingsManager.save(path + "/mapSettings.json")){
        Logger::logError("Error opening file: " + path + "/mapsettings.json");
        return false;
    }
    //preview
    if (!QFile::exists(path + "/preview.png"))
    {
        QImage mapImage = MinimapHelper::miniMap(map, RESOLVE(DBFModel));
        mapImage.save(path + "/preview.png");
    }

    //map objects
    QFile jsonFile(path + "/mapObjects.json");
    if (jsonFile.open(QIODevice::WriteOnly))
    {
        QJsonObject root;
        QJsonArray mapObjectsArray;
        auto types = map.collectionTypes();
        foreach (int i, types) {
            QJsonObject objectCollection;
            objectCollection["_type"] = MapObject::typeName((MapObject::Type)i);
            objectCollection["typeId"] = i;
            QJsonArray array;
            ObjectsCollection collection = map.collectionByType(i);
            foreach (QSharedPointer<MapObject> obj, collection.data)
            {
                QJsonObject jsonObj;
                jsonObj["id"] = uidToString(obj->uid);
                if (!obj->note.isEmpty())
                    jsonObj["note"] = obj->note;
                if (obj->populate)
                {
                    jsonObj["x"] = obj->x;
                    jsonObj["y"] = obj->y;
                }
                saveObject(jsonObj, obj);
                array << jsonObj;
            }
            objectCollection["objects"] = array;
            mapObjectsArray<<objectCollection;
        }
        root["objects"] = mapObjectsArray;
        QJsonDocument jsonDocument(root);
        jsonFile.write(jsonDocument.toJson(QJsonDocument::Indented));
        jsonFile.close();
    } else {
        Logger::logError("Error opening file: " + path + "/mapObjects.json");
        return false;
    }

    QFile gridFile(path + "/mapGrid.bin");
    if (gridFile.open(QIODevice::WriteOnly)) {
        QDataStream out(&gridFile);
        out.setVersion(QDataStream::Qt_5_12);
        for (const QList<MapCell>& row : map.getGrid().cells) {
            for (const MapCell& cell : row) {
                out << cell.value;
                out << cell.roadType;
            }
        }
        gridFile.close();
    } else {
        Logger::logError("Error opening file: " + path + "/mapGrid.bin");
        return false;
    }
    return true;
}

bool MapJsonIO::load(GameMap & map, const QString &path)
{
    static SettingsManager settingsManager;
    QFile mapInfoFile(path + "/mapInfo.json");
    if (mapInfoFile.open(QIODevice::ReadOnly))
    {
        QJsonDocument jsonDocument = QJsonDocument::fromJson(mapInfoFile.readAll());
        QJsonObject root = jsonDocument.object();
        QJsonObject mapInfo = root["mapInfo"].toObject();

        map.setMapName(mapInfo["name"].toString());
        map.setMapDesc(mapInfo["desc"].toString());
        auto grid  = map.getGrid();
        grid.init(mapInfo["size"].toInt());
        map.setGrid(grid);
        float version = mapInfo["version"].toDouble();
        map.setVersion(version);
        mapInfoFile.close();
    } else {
        Logger::logError("Error opening file: " + mapInfoFile.fileName());
        return false;
    }

    if (!settingsManager.load(path + "/mapSettings.json")) {
        Logger::logError("Error opening file: " + path + "/mapSettings.json");
        return false;
    }
    map.setSettings(settingsManager.settings());
    QFile jsonFile(path + "/mapObjects.json");
    if (jsonFile.open(QIODevice::ReadOnly)) {
        QJsonDocument jsonDocument = QJsonDocument::fromJson(jsonFile.readAll());
        QJsonObject root = jsonDocument.object();
        QJsonArray mapObjectsArray = root["objects"].toArray();
        for (int i = 0; i < mapObjectsArray.count(); i++)
        {
            const QJsonObject& object = mapObjectsArray.at(i).toObject();
            const QJsonArray& objectsArray = object["objects"].toArray();
            MapObject::Type type = (MapObject::Type)object["typeId"].toInt();
            for (int k = 0; k < objectsArray.count(); ++k)
            {
                const QJsonObject& inobject = objectsArray.at(k).toObject();
                QSharedPointer<MapObject> res = loadObject(inobject, type);
                if (res->populate)
                {
                    res->x = inobject["x"].toInt();
                    res->y = inobject["y"].toInt();
                }
                res->uid = uidFromString(inobject["id"].toString());
                res->note = inobject["note"].toString();
                map.addObject(res, res->uid);
            }
        }
        jsonFile.close();
    } else {
        Logger::logError("Error opening file: " + path + "/mapObjects.json");
        return false;
    }

    QFile gridFile(path + "/mapGrid.bin");
    if (gridFile.open(QIODevice::ReadOnly)) {
        QDataStream in(&gridFile);
        in.setVersion(QDataStream::Qt_5_12);

        auto grid = map.getGrid();
        for (QList<MapCell>& row : grid.cells) {
            for (MapCell& cell : row) {
                in >> cell.value;
                in >> cell.roadType;
            }
        }
        QList<QSharedPointer<MapObject> > objects = map.objects();
        foreach(QSharedPointer<MapObject> mapObj, objects)
        {
            if (!mapObj->populate)
                continue;
            QSharedPointer<IMapObjectAccessor> accessor =
                    RESOLVE(AccessorHolder)->objectAccessor(mapObj->uid.first);
            if (accessor.isNull())
                continue;
            int w = accessor->getW(mapObj);
            int h = accessor->getH(mapObj);
            int x = mapObj->x;
            int y = mapObj->y;
            for(int i = 0; i < w; ++i)
            {
                for(int k = 0; k < h; ++k)
                {
                    if (x + i >= grid.cells.count() || y + k >= grid.cells.count())
                        continue;
                    if (x + i < 0 || y + k < 0)
                        continue;
                    if (mapObj->uid.first == MapObject::Location)
                        grid.locationsBinging.binding[x + i][y + k].items << mapObj->uid;
                    else
                        grid.objBinging.binding[x + i][y + k].items << mapObj->uid;
                }
            }
        }
        map.setGrid(grid);

        gridFile.close();
    } else {
        Logger::logError("Error opening file: " + path + "/mapGrid.bin");
        return false;
    }

    return true;
}

bool MapJsonIO::readMapInfo(GameMap::MapInfo &mapInfo, const QString &path)
{
    QFile file(path + "/mapInfo.json");
    if (!file.open(QIODevice::ReadOnly | QIODevice::Text)) {
        Logger::logError("Error opening file: " + path + "/mapInfo.bin");
        return false;
    }

    QByteArray fileData = file.readAll();
    file.close();

    QJsonParseError parseError;
    QJsonDocument jsonDoc = QJsonDocument::fromJson(fileData, &parseError);

    QJsonObject jsonObject = jsonDoc.object();
    QJsonObject mapInfoObject = jsonObject["mapInfo"].toObject();

    mapInfo.desc = mapInfoObject["desc"].toString();
    mapInfo.name = mapInfoObject["name"].toString();
    mapInfo.size = mapInfoObject["size"].toInt();
    mapInfo.version = mapInfoObject["version"].toDouble();

    return true;
}

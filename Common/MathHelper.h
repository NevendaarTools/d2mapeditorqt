#ifndef MATHHELPER_H
#define MATHHELPER_H
#include <QString>
#include <QHash>
#include <QPointF>

class MathHelper
{
public:
    static bool evaluateSimpleCondition(const QString& condition);
    static void replaceStrings(QString & string, const QHash<QString, int> & replaces);
    static bool evaluateCondition(QString& condition);
    static bool isConditionFinished(const QString& condition);
};

#endif // MATHHELPER_H

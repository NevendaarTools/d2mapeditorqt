#include "AutoCompleteModel.h"
#include <QDebug>

AutoCompleteModel::AutoCompleteModel(QObject *parent) : QAbstractListModel(parent)
{
    //https://github.com/VladimirMakeev/D2ModdingToolset/blob/master/luaApi.md#enumerations
    m_wordList << "scenario:getLocation(" << "Id.new(" << "scenario:getStack(" << "leader.impl:hasModifier(" << "leader.impl.type"
               << "location.position" << "Race.Human" << "Race.Elf"<< "Race.Undead" << "Race.Heretic" << "Race.Dwarf" << "Race.Neutral"
               << "Ability.WandScrollUse" << "Ability.Incorruptible" << "Ability.WeaponMaster"
               << "Ability.WeaponArmorUse" << "Ability.BannerUse" << "Ability.JewelryUse" << "Ability.Rod"
               << "Ability.OrbUse" << "Ability.TalismanUse" << "Ability.TravelItemUse" << "Ability.CriticalHit "
               << "Lord.Mage"<< "Lord.Warrior"<< "Lord.Diplomat"
               << "true"<<"false"<<"then\n    end"<<"scenario:getFort"
               << "local "<<"stack.leader"<<"stack.leader.impl"<<"stack.leader.impl.type"<<"Unit.Soldier"
               <<"Unit.Noble"<<"Unit.Leader"<<"Unit.Summon"<<"Unit.Illusion"<<"Unit.Guardian"
               << "stack.inventory"<<"fort.owner"<<"owner.race"<<"fort.inventory"<<"stack.leader"<<"leader:hasAbility";
}

int AutoCompleteModel::rowCount(const QModelIndex &parent) const
{
    return m_completions.count();
}

QVariant AutoCompleteModel::data(const QModelIndex &index, int role) const
{
    if (!index.isValid() || index.row() >= m_completions.count())
            return QVariant();
    if (role == TextRole || role == Qt::DisplayRole)
            return m_completions.at(index.row());
    return QVariant();
}

void AutoCompleteModel::updateCompletions(const QString &text, int index)
{
    beginResetModel();
    m_completions.clear();
    int ind = lastWorldStart(text, index);
    QString prefix = text.mid(ind, index - ind);

    if (prefix.count() >= 2)
    {
        for (const QString &word : m_wordList) {
            if (word.startsWith(prefix) && word != prefix) {
                m_completions << word;
            }
        }
    }
    endResetModel();
}

QHash<int, QByteArray> AutoCompleteModel::roleNames() const
{
    QHash<int, QByteArray> roles;
    roles[TextRole] = "Text";
    return roles;
}

int AutoCompleteModel::lastWorldStart(const QString &text, int index)
{
    int i = index - 1;
    for (; i > 0; --i)
    {
        if (text[i] ==  ' ' ||
            text[i] == '\n' ||
            text[i] == '\t')
        {
            i+=1;
            break;
        }
    }
    return i;
}

#ifndef STRINGHELPER_H
#define STRINGHELPER_H
#include <QString>
#define LOG_MESSAGE(string) qDebug()<<string
#define LOG_WARNING(string) qDebug()<<string
#define LOG_ERROR(string) qDebug()<<string

#include <QString>
#include <QRegularExpression>

static QString convertToHtml(const QString& input) {
    QString html = input;

    // Замена шрифтов
    html.replace("\\fSmall;", "<span style='font-size:small;'>");
    html.replace("\\fNormal;", "<span style='font-size:medium;'>");
    html.replace("\\fMenu;", "<span style='font-size:medium; font-weight:bold;'>");
    html.replace("\\fMedium;", "<span style='font-size:large;'>");
    html.replace("\\fMedBold;", "<span style='font-size:large; font-weight:bold;'>");
    html.replace("\\fLarge;", "<span style='font-size:x-large;'>");
    html.replace("\\fVLarge;", "<span style='font-size:xx-large; font-weight:bold; font-style:italic;'>");

    // Замена цветов текста и фона
    static QRegularExpression colorRegex("\\\\c(\\d+);(\\d+);(\\d+);");
    html.replace(colorRegex, "<span style='color:rgb(\\1,\\2,\\3);'>");

    static QRegularExpression bgColorRegex("\\\\o(\\d+);(\\d+);(\\d+);");
    html.replace(bgColorRegex, "<span style='background-color:rgb(\\1,\\2,\\3);'>");

    // Замена выравнивания
    html.replace("\\hL;", "<div style='text-align:left;'>");
    html.replace("\\hC;", "<div style='text-align:center;'>");
    html.replace("\\hR;", "<div style='text-align:right;'>");

    html.replace("\\vT;", "<div style='vertical-align:top;'>");
    html.replace("\\vC;", "<div style='vertical-align:middle;'>");
    html.replace("\\vB;", "<div style='vertical-align:bottom;'>");

    // Замена специальных символов
    html.replace("\\t", "&emsp;");
    html.replace("\\n", "<br>");

    if (html.contains("<span "))
    {
        // Закрытие тегов
        html.append("</span></div>");
    }

    return html;
}
#endif // STRINGHELPER_H

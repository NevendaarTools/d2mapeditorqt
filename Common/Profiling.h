#ifndef PROFILING_H
#define PROFILING_H
#include <QElapsedTimer>
#include <QDebug>

class TimerMeasure
{
public:
    TimerMeasure(const QString& text): m_text(text){
        qDebug()<<"Start profiling - "<<m_text;
        m_timer.start();
    }
    ~TimerMeasure(){qDebug()<<m_text<<" tooks "<<m_timer.elapsed();}
    void logStep()
    {
        qDebug()<<m_text<<"Step - "<<step++;
    }
private:
    QElapsedTimer m_timer;
    QString m_text;
    int step = 0;
};
#endif // PROFILING_H

#ifndef MAPJSONIO_H
#define MAPJSONIO_H
#include <QSharedPointer>
#include "../MapObjects/GameMap.h"

class MapJsonIO
{
public:
    MapJsonIO();
    static bool save(GameMap &map, const QString & path);
    static bool load(GameMap &map, const QString &path);
    static bool readMapInfo(GameMap::MapInfo & mapInfo, const QString &path);

};

#endif // MAPJSONIO_H

#ifndef HUNSPELLDLL_H
#define HUNSPELLDLL_H

#include <QLibrary>
#include <QString>
#include <QStringList>

class HunspellDLL
{
public:
    HunspellDLL();
    ~HunspellDLL();

    bool load(const QString &dllPath);
    void *create(const char *aff, const char *dic);
    void destroy(void *pHunspell);
    int spell(void *pHunspell, const char *word);
    char **suggest(void *pHunspell, const char *word, int *slst);
    void free_list(void *pHunspell, char **slst, int n);

private:
    QLibrary m_lib;
    typedef void* (*CreateProc)(const char*, const char*);
    typedef void (*DestroyProc)(void*);
    typedef int (*SpellProc)(void*, const char*);
    typedef char** (*SuggestProc)(void*, const char*, int*);
    typedef void (*FreeListProc)(void*, char**, int);

    CreateProc m_create;
    DestroyProc m_destroy;
    SpellProc m_spell;
    SuggestProc m_suggest;
    FreeListProc m_free_list;
};

#endif // HUNSPELLDLL_H

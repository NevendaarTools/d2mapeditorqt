#pragma once
#include <QObject>
#include <QString>
#include <QStringList>
#include "HunspellDLL.h"

class HunspellChecker
{
public:
    explicit HunspellChecker();
    ~HunspellChecker();

    bool initialize(const QString &dllPath, const QString &affPath, const QString &dicPath);
    bool spell(const QString &word);
    QStringList suggest(const QString &word);

private:
    HunspellDLL m_hunspellDLL;
    void *m_hunspell;
};

#ifndef AUTOCOMPLETEMODEL_H
#define AUTOCOMPLETEMODEL_H

#include <QObject>
#include <QAbstractListModel>

class AutoCompleteModel : public QAbstractListModel
{
    Q_OBJECT
public:
    enum Roles {
        TextRole = Qt::UserRole + 1
    };

    explicit AutoCompleteModel(QObject *parent = nullptr);

    int rowCount(const QModelIndex &parent = QModelIndex()) const override;
    QVariant data(const QModelIndex &index, int role = Qt::DisplayRole) const override;

    Q_INVOKABLE void updateCompletions(const QString &text, int index);
    QHash<int, QByteArray> roleNames() const;
    Q_INVOKABLE int lastWorldStart(const QString &text, int index);
private:
    QStringList m_completions;
    QStringList m_wordList;
};
#endif // AUTOCOMPLETEMODEL_H

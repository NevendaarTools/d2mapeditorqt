#include "MathHelper.h"
#include <QDebug>
#include <QRegularExpression>

bool MathHelper::evaluateSimpleCondition(const QString &condition)
{
    // Определяем шаблон для поиска операндов и оператора
    QRegularExpression pattern("(\\d+|true|false)([<>!=]+|\\&\\&|\\|\\|)(\\d+|true|false)");
    QRegularExpressionMatch match = pattern.match(condition);

    if (!match.hasMatch()) {
        if (condition.trimmed() == "1" || condition.trimmed() == "true")
            return true;
        return false;
    }

    QString operand1 = match.captured(1);
    QString op = match.captured(2);
    QString operand2 = match.captured(3);

    // Получить значения операндов
    int value1 = operand1.toInt();
    int value2 = operand2.toInt();

    // Выполнить операцию
    if (op == "==") {
        return value1 == value2;
    } else if (op == "!=") {
        return value1 != value2;
    } else if (op == ">") {
        return value1 > value2;
    } else if (op == ">=") {
        return value1 >= value2;
    } else if (op == "<") {
        return value1 < value2;
    } else if (op == "<=") {
        return value1 <= value2;
    } else if (op == "&&") {
        return  (operand1.toLower() == "true" && operand2.toLower() == "true") ||
               (operand1 == "1" && operand2 == "1");
    } else if (op == "||") {
        return (operand1.toLower() == "true" || operand2.toLower() == "true") ||
               (operand1 == "1" || operand2 == "1");
    } else {
        return false;
    }
}

void MathHelper::replaceStrings(QString &string, const QHash<QString, int> &replaces)
{
    string.remove(" ");
    for (auto it = replaces.begin(); it != replaces.end(); ++it)
    {
        string.replace(it.key(), QString::number(it.value()));
    }
}

bool MathHelper::evaluateCondition(QString &condition) {
    if (!condition.contains("("))
    {
        bool result = evaluateSimpleCondition(condition);
        condition = QString::number(result);
        qDebug()<<result<<1;
        return result;
    }
    QStringList operands;
    int depth = 0;
    int start = 0;
    for (int i = 0; i < condition.size(); ++i)
    {
        if (condition[i] == '(')
        {
            if (depth == 0)
                start = i;
            ++depth;
        }
        else if (condition[i] == ')')
        {
            --depth;
            if (depth == 0)
            {
                QString part = condition.mid(start, i - start + 1);
                bool result = evaluateSimpleCondition(condition);
                part = QString::number(result);
                condition.remove(start, i - start + 1);
                condition.insert(start, part);
                i = start + part.length();
            }
        }
    }

    bool result =  evaluateSimpleCondition(condition);
    return result;
}

bool MathHelper::isConditionFinished(const QString &condition)
{
    int depth = 0;
    for (int i = 0; i < condition.size(); ++i)
    {
        if (condition[i] == '(')
        {
            ++depth;
        }
        else if (condition[i] == ')')
        {
            --depth;
        }
    }
    return depth == 0;
}

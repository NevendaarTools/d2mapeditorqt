#include "HunspellDLL.h"

HunspellDLL::HunspellDLL() : m_create(nullptr), m_destroy(nullptr), m_spell(nullptr), m_suggest(nullptr), m_free_list(nullptr) {}

HunspellDLL::~HunspellDLL()
{
    if (m_lib.isLoaded()) {
        m_lib.unload();
    }
}

bool HunspellDLL::load(const QString &dllPath)
{
    if (m_lib.isLoaded()) {
        m_lib.unload();
    }

    m_lib.setFileName(dllPath);
    if (!m_lib.load()) {
        return false;
    }

    m_create = reinterpret_cast<CreateProc>(m_lib.resolve("Hunspell_create"));
    m_destroy = reinterpret_cast<DestroyProc>(m_lib.resolve("Hunspell_destroy"));
    m_spell = reinterpret_cast<SpellProc>(m_lib.resolve("Hunspell_spell"));
    m_suggest = reinterpret_cast<SuggestProc>(m_lib.resolve("Hunspell_suggest"));
    m_free_list = reinterpret_cast<FreeListProc>(m_lib.resolve("Hunspell_free_list"));

    return m_create && m_destroy && m_spell && m_suggest && m_free_list;
}

void *HunspellDLL::create(const char *aff, const char *dic)
{
    return m_create ? m_create(aff, dic) : nullptr;
}

void HunspellDLL::destroy(void *pHunspell)
{
    if (m_destroy) m_destroy(pHunspell);
}

int HunspellDLL::spell(void *pHunspell, const char *word)
{
    return m_spell ? m_spell(pHunspell, word) : 0;
}

char **HunspellDLL::suggest(void *pHunspell, const char *word, int *slst)
{
    return m_suggest ? m_suggest(pHunspell, word, slst) : nullptr;
}

void HunspellDLL::free_list(void *pHunspell, char **slst, int n)
{
    if (m_free_list) m_free_list(pHunspell, slst, n);
}

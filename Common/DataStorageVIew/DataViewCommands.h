#ifndef DATAVIEWCOMMANDS_H
#define DATAVIEWCOMMANDS_H

#include <QVariant>
#include <QString>

struct DisplayFileCommand
{
    QString fileName;
    int line;
};

Q_DECLARE_METATYPE(DisplayFileCommand)

#endif // DATAVIEWCOMMANDS_H

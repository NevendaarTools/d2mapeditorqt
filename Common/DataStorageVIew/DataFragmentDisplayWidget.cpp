#include "DataFragmentDisplayWidget.h"
#include <QMessageBox>
#include <QHBoxLayout>
#include <QClipboard>
#include "FileViewerWidget.h"
#include "DataViewCommands.h"

DataFragmentDisplayWidget::DataFragmentDisplayWidget(QWidget *parent)
    : QWidget{parent}
{
    setLayout(new QHBoxLayout());
    m_textBrowser = new QTextBrowser(this);
    m_textBrowser->setOpenLinks(false);
    m_textBrowser->setOpenExternalLinks(false);
//        textBrowser->setReadOnly(false);
    layout()->addWidget(m_textBrowser);
    resize(800, 600);
    connect(m_textBrowser, &QTextBrowser::anchorClicked, this, &DataFragmentDisplayWidget::onLinkClicked);
}

void DataFragmentDisplayWidget::setHtml(const QString &htmlText)
{
    m_textBrowser->setHtml(htmlText);
}

void DataFragmentDisplayWidget::setDataFragments(QList<DataFragment> data)
{
    QList<DataFragment> tmp;
    m_data.clear();
//    m_data = data;
    for(auto & frag : data)
    {
        if (m_data.count() > 0)
        {
            auto & last = tmp.last();
            if (last.filePath == frag.filePath && last.type == frag.type)
            {
                if ((last.startPosition + last.symbolsCount >= frag.startPosition))
                {
                    auto lastPos = frag.startPosition + frag.symbolsCount;
                    last.symbolsCount = lastPos - last.startPosition;
                    continue;
                }
            }
        }
        tmp << frag;
    }

    uint64_t position = 0;
    QString html;
    int index = 0;
    MappedDataFragmentHelper helper(storage, doc);

    for(const auto & frag : tmp)
    {
        MappedDataFragment resFrag = helper.fillFragment(frag, QString::number(index));
        resFrag.mappedStart = position;
        position+= resFrag.mappedCount;
        // if (index < 100)
            html += resFrag.text;


        m_data << resFrag;
        ++index;
    }
    setHtml(html);
}

QString dataByFragment(QSharedPointer<DataStorage> storage,
                       const DataFragment & frag,
                       QList<int> & indexes)
{
    if(indexes.count() == 0)
        return storage->getFragmentText(frag);
    int index = indexes.first();
    indexes.removeFirst();
    if (frag.attached.count() > index)
    {
        return dataByFragment(storage, frag.attached[index], indexes);
    }
    return "";
}
void DataFragmentDisplayWidget::onLinkClicked(const QUrl &link)
{
    ActionLink actionLink = ActionLink::parse(link.toString());
    if (actionLink.type == ActionLink::Invalid)
        return;
    MappedDataFragment & frag = m_data[actionLink.indexes.first()];
    switch (actionLink.type) {
    case ActionLink::Open: {
        DisplayFileCommand command;
        command.fileName = frag.fragment.filePath;
        command.line = frag.fragment.startPosition;
        emit commandRequested(QVariant::fromValue(command));
        break;
    }
    case ActionLink::LoadAfter: {
        int linesToLoad = 350;
        uint64_t linesCount = storage->getFileSize(frag.fragment.filePath);
        uint64_t fragEnd = frag.fragment.startPosition + frag.fragment.symbolsCount;

        int step = fragEnd + linesToLoad < linesCount ? linesToLoad: linesCount - (fragEnd + linesToLoad);
        if (step <= 0)
            return;

        uint64_t prevStart = frag.mappedStart;
        uint64_t prevCount = frag.mappedCount;
        frag.fragment.symbolsCount += step;
        MappedDataFragmentHelper helper(storage, doc);
        MappedDataFragment resFrag = helper.fillFragment(frag.fragment, QString::number(actionLink.indexes.first()));
        frag.mappedCount = resFrag.mappedCount;
        frag.text = resFrag.text;
        replaceTextInTextBrowser(prevStart,
                                 prevCount,
                                 frag.text);
        for (int i = actionLink.indexes.first() + 1; i < m_data.count(); ++i)
        {
            auto & data = m_data[i];
            data.mappedStart += frag.mappedCount - prevCount;
        }
        break;
    }
    case ActionLink::LoadBefore: {
        int linesToLoad = 350;
        int step = frag.fragment.startPosition > linesToLoad ? linesToLoad: frag.fragment.startPosition;
        if (step == 0)
            return;
        uint64_t prevStart = frag.mappedStart;
        uint64_t prevCount = frag.mappedCount;
        frag.fragment.startPosition -= step;
        frag.fragment.symbolsCount  += step;
        for(auto & tmp: frag.fragment.highlights)
            tmp.startPosition += step;

        MappedDataFragmentHelper helper(storage, doc);
        MappedDataFragment resFrag = helper.fillFragment(frag.fragment, QString::number(actionLink.indexes.first()));
        frag.mappedCount = resFrag.mappedCount;
        frag.text = resFrag.text;
        replaceTextInTextBrowser(prevStart,
                                 prevCount,
                                 frag.text);
        for (int i = actionLink.indexes.first() + 1; i < m_data.count(); ++i)
        {
            auto & data = m_data[i];
            data.mappedStart += frag.mappedCount - prevCount;
        }
        break;
    }
    case ActionLink::Copy: {
        auto tmp = actionLink.indexes;
        tmp.removeFirst();
        auto text = dataByFragment(storage, frag.fragment, tmp);
        text.replace("<br>","\n");
        qDebug()<<"Copied: "<<text;
        QClipboard *clipboard = QApplication::clipboard();
        clipboard->setText(text);
        break;
    }
    case ActionLink::Remove:
        break;
    case ActionLink::Edit:
        break;
    case ActionLink::Hide: {
        replaceTextInTextBrowser(frag.mappedStart,
                                 frag.mappedCount,
                                 "<br>");
        for (int i = actionLink.indexes.first() + 1; i < m_data.count(); ++i)
        {
            auto & data = m_data[i];
            data.mappedStart -= frag.mappedCount;
        }
        m_data.removeAt(actionLink.indexes.first());
        break;
    }
    default:
        break;
    }
}

void DataFragmentDisplayWidget::replaceTextInTextBrowser(int start, int count, const QString &newText)
{
    if (!m_textBrowser)
        return;
    int end = start + count;
    QTextCursor cursor = m_textBrowser->textCursor();
    cursor.setPosition(start);
    cursor.setPosition(end, QTextCursor::KeepAnchor);
    cursor.insertHtml(newText);
}

void DataFragmentDisplayWidget::setStorage(QSharedPointer<DataStorage> newStorage)
{
    storage = newStorage;
}

void DataFragmentDisplayWidget::setLineToDisplay(int lineNumber)
{
    QTextBlock block = m_textBrowser->document()->findBlockByLineNumber(lineNumber);
    if (block.isValid()) {
        QTextCursor cursor(block);

        // Подсветить строку
        QTextEdit::ExtraSelection selection;
        selection.format.setBackground(Qt::lightGray);
        selection.cursor = cursor;
        selection.cursor.select(QTextCursor::BlockUnderCursor);
        QList<QTextEdit::ExtraSelection> extraSelections;
        extraSelections.append(selection);
        m_textBrowser->setExtraSelections(extraSelections);

        // Прокрутить к подсвеченной строке
        m_textBrowser->setTextCursor(cursor);
        m_textBrowser->ensureCursorVisible();

        // Дополнительная прокрутка, чтобы показать несколько следующих строк
        for (int i = 0; i < 10 && cursor.movePosition(QTextCursor::Down); ++i) {
            m_textBrowser->setTextCursor(cursor);
            m_textBrowser->ensureCursorVisible();
        }
    }
    else
        qDebug()<<"Invalid block"<<m_textBrowser->document()->lineCount();
}

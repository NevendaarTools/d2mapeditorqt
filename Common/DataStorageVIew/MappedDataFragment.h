#ifndef MAPPEDDATAFRAGMENT_H
#define MAPPEDDATAFRAGMENT_H
#include "DataStorage.h"
#include <QTextDocument>

struct MappedDataFragment
{
    DataFragment fragment;
    QString text;
    QStringList content;
    uint64_t mappedStart;
    uint64_t mappedCount;
};

struct ActionLink
{
    enum Type{
        Invalid,
        Open,
        LoadAfter,
        LoadBefore,
        Copy,
        Remove,
        Edit,
        Hide
    };
    Type type = Invalid;
    QList<int> indexes;
    static ActionLink parse(const QString & string)
    {
        QStringList data = string.split(':');
        qDebug()<<data;
        if (data.count() < 2)
            return {};

        ActionLink result;
        QString command = data[0];
        QString strIndexes = data[1];
        QStringList indexesData = strIndexes.split('-');
        for(const QString& index: indexesData)
            result.indexes << index.toInt();

        if (command == "load-after") {
            result.type = LoadAfter;
        } else if (command == "load-before") {
            result.type = LoadBefore;
        } else if (command == "copy") {
            result.type = Copy;
        } else if (command == "open") {
            result.type = Open;
        } else if (command == "remove") {
            result.type = Remove;
        } else if (command == "edit") {
            result.type = Edit;
        } else if (command == "hide") {
            result.type = Hide;
        } else {
            qWarning() << "Unknown command:" << command;
            return {};
        }

        return result;
    }
};

struct MappedDataFragmentHelper
{
    MappedDataFragmentHelper(QSharedPointer<DataStorage> storage_, QTextDocument & doc_) : storage(storage_), doc(doc_){}
    QSharedPointer<DataStorage> storage;
    QTextDocument & doc;

    MappedDataFragment fillFragment(const DataFragment &fragment, const QString &path);
    void getText(QString & text, QStringList &content, MappedDataFragment & fragment, const QString &path);
    void fillSimpleTextFragment(MappedDataFragment & fragment, const QString &path);
    void fillCodeFragment(QTextDocument &doc, MappedDataFragment &fragment, const QString &path);
    void fillLinkedFragment(QTextDocument &doc, MappedDataFragment &fragment, const QString &path);
    void fillTitleTextFragment(MappedDataFragment &fragment, const QString &path);
    void fillMultyFragment(MappedDataFragment &fragment, const QString &path);
};

#endif // MAPPEDDATAFRAGMENT_H

#ifndef DATAFRAGMENTDISPLAYWIDGET_H
#define DATAFRAGMENTDISPLAYWIDGET_H

#include <QWidget>
#include <QTextBrowser>
#include "DataStorage.h"
#include <QTextBlock>
#include "MappedDataFragment.h"

class DataFragmentDisplayWidget : public QWidget
{
    Q_OBJECT
public:
    explicit DataFragmentDisplayWidget(QWidget *parent = nullptr);

    void setHtml(const QString& htmlText);
    void setDataFragments(QList<DataFragment> data);
    void setStorage(QSharedPointer<DataStorage> newStorage);
    void setLineToDisplay(int lineNumber);
private slots:
    void onLinkClicked(const QUrl &link);
signals:
    void commandRequested(const QVariant & command);
private:
    void replaceTextInTextBrowser(int start, int count, const QString& newText);
private:
    QTextBrowser *m_textBrowser;
    QList<MappedDataFragment> m_data;
    QSharedPointer<DataStorage> storage;
    QTextDocument doc;
};

#endif // DATAFRAGMENTDISPLAYWIDGET_H

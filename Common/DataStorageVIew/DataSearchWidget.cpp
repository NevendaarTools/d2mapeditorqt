#include "DataSearchWidget.h"

DataSearchWidget::DataSearchWidget(QWidget *parent) :
    QWidget(parent),
    m_lineEdit(new QLineEdit(this)),
    m_countLabel(new QLabel("0", this))
{
    QVBoxLayout* mainLayout = new QVBoxLayout();
    setLayout(mainLayout);
    QHBoxLayout* topLayout = new QHBoxLayout();
    topLayout->addWidget(m_lineEdit);
    topLayout->addStretch();
    topLayout->addWidget(m_countLabel);
    mainLayout->addLayout(topLayout);

    m_display = new DataFragmentDisplayWidget(this);
    m_display->setSizePolicy(QSizePolicy::Expanding, QSizePolicy::Expanding);
    layout()->addWidget(m_display);

    connect(m_lineEdit, &QLineEdit::textChanged, this, [this]() {
        updateSearchResults();
    });
    connect(m_display, SIGNAL(commandRequested(QVariant)), this, SIGNAL(commandRequested(QVariant)));
}

void DataSearchWidget::updateSearchResults() {
//    QList<QString> content = m_storage->getFileContentList(fileName);
//    auto fragments = DataStorage::parseContent(content);
//    for (auto & frag: fragments)
//        frag.filePath = fileName;
//    int resultCount = content.count();
//    m_display->setDataFragments(fragments);
    if (m_lineEdit->text().count() < 3)
        return;
    auto result = m_storage->getFragmentsWithSubstring(m_lineEdit->text());
    int resultCount = result.count();
    m_countLabel->setText(QString("Found: %1").arg(resultCount));
    m_display->setDataFragments(result);
}

void DataSearchWidget::setStorage(QSharedPointer<DataStorage> newStorage)
{
    m_storage = newStorage;
    m_display->setStorage(m_storage);
}

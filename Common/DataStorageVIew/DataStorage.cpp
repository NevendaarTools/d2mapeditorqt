#include "DataStorage.h"

struct Reader
{

    Helper help;

    Reader()
    {
        help.factories.insert("{{{", new MultiBlockReader());
        help.factories.insert("```lua", new SimpleDataFragmentReader("```lua","```", DataFragment::Code));
        help.factories.insert("```", new SimpleDataFragmentReader("```","```", DataFragment::Code));
        help.factories.insert("***", new SimpleDataFragmentReader("***","***", DataFragment::Title));
        help.factories.insert("#### ", new SimpleDataFragmentReader("#### ","\n", DataFragment::Title));
        help.factories.insert("### ", new SimpleDataFragmentReader("### ","\n", DataFragment::Title));
        help.factories.insert("# ", new SimpleDataFragmentReader("# ","\n", DataFragment::Title));
    }

    QList<DataFragment> parse(const QString& content)
    {
        QList<DataFragment> result;

        for (int index = 0; index < content.length() - 3; ++index)
        {
            IDataFragmentReader * fact = help.readerByString(index, content);
            if (fact)
            {
                result << fact->parse(help, index, content);
            }
        }
        return result;
    }
};

DataStorage::DataStorage()
{

}

void DataStorage::init(const QString &directoryPath)
{
    m_path = directoryPath;
    QDir directory(directoryPath);

    if (!directory.exists())
    {
        qWarning() << "Директория не существует:" << directoryPath;
        return;
    }

    QStringList filters;
    filters << "*.txt"<<"*.md";
    directory.setNameFilters(filters);

    QFileInfoList fileList = directory.entryInfoList(QDir::Files);

    if (fileList.isEmpty())
    {
        qDebug() << "Нет файлов с расширением .txt в директории:" << directoryPath;
        return;
    }

    // Выводим имена файлов в консоль
    foreach (const QFileInfo &fileInfo, fileList)
    {
        auto file = new FileRepresentation();
        file->path = fileInfo.absoluteFilePath();
        file->name = fileInfo.baseName();
        file->load();
        m_files.insert(file->name, QSharedPointer<FileRepresentation>(file));
    }
}

QList<DataFragment> DataStorage::getFragmentsWithSubstring(const QString &substr)
{
    QList<DataFragment> fragments;
    auto fileNames = m_files.keys();
    int contextSize = 150;
    for(const auto &fileName: fileNames)
    {
        const auto & file = m_files[fileName];
        QString mainString = file->content;
        uint64_t pos = 0;
        auto fileFragments = file->fragments;
        while ((pos = mainString.indexOf(substr, pos)) != -1) {
            uint64_t startPos = qMax((uint64_t)0, pos - contextSize);
            uint64_t endPos = qMin((uint64_t)mainString.length(), pos + substr.length() + contextSize);

            DataFragment fragment;
            fragment.filePath = fileName;
            fragment.startPosition = startPos;
            fragment.symbolsCount = endPos - startPos;
            fragment.highlights<<DataHighlight{pos - startPos, (uint64_t)substr.length(), "search"};
            pos += substr.length();

            for(const DataFragment & frag: fileFragments)
            {
                if (frag.startPosition<= pos && (frag.startPosition + frag.symbolsCount - 1) >= fragment.symbolsCount )
                {
                    if (frag.type != DataFragment::FragmentType::SimpleText)
                        fragment.attached << frag;
                }
            }
            fragments<<fragment;
        }
    }
    return fragments;
}

QString DataStorage::getFragmentText(const DataFragment &frag)
{
    const auto & file = m_files[frag.filePath];
    auto result =  file->content.mid(frag.startPosition, frag.symbolsCount);
    return result;
}

QString DataStorage::getFragmentText(const QString &path, int line, int count)
{
    const auto & file = m_files[path];
    QString result;
//    for(int i = 0; i < count; ++i)
//        result += file->data[line + i] + "<br>";
    return result;
}

uint64_t DataStorage::getFileSize(const QString &path) const
{
    const auto & file = m_files[path];
    return file->content.length();
}

QString DataStorage::getFileContent(const QString &path) const
{
    const auto & file = m_files[path];
    QString result;
    for (int i = 0; i < file->data.count(); ++i)
    {
//        if (file->data[i].isEmpty())
//            continue;
        result += file->data[i] + "<br>";
    }

    return result;
}

QList<QString> DataStorage::getFileContentList(const QString &path) const
{
    const auto & file = m_files[path];
    return file->data;
}

QString DataStorage::getFilePath(const QString &path) const
{
    const auto & file = m_files[path];
    return file->path;
}

QList<DataFragment> DataStorage::getFileFragments(const QString &path)
{
    const auto & file = m_files[path];
    return file->fragments;
}

bool FileRepresentation::load()
{
    data.clear();
    QFile file(path);
    if (!file.open(QIODevice::ReadOnly | QIODevice::Text))
    {
        qWarning() << "Не удалось открыть файл:" << path;
        return false;
    }
    content = file.readAll();
    Reader reader;
    fragments = reader.parse(content);
    uint64_t last = 0;
    QList<DataFragment> textFragments;
    for(auto & data: fragments)
    {
        if (data.realStartPosition - last > 10)
        {
            DataFragment result;
            result.type = DataFragment::SimpleText;
            result.startPosition = last;
            result.symbolsCount = data.realStartPosition - last;
            result.filePath = this->name;
            textFragments << result;
        }
        data.setPath(this->name);
        last = data.realStartPosition + data.realSymbolsCount;
        qDebug()<<DataFragment::fragmentTypeToString(data.type);
        qDebug()<<data.startPosition<<" - "<<data.startPosition + data.symbolsCount;
    }
    fragments+= textFragments;
    std::sort(fragments.begin(), fragments.end(), [](const DataFragment a, const DataFragment b) -> bool
        { return a.startPosition < b.startPosition; });
    for(auto & data: fragments)
    {
        qDebug()<<DataFragment::fragmentTypeToString(data.type);
        qDebug()<<data.startPosition<<" - "<<data.startPosition + data.symbolsCount;
    }
    if (fragments.isEmpty())
    {
        DataFragment result;
        result.type = DataFragment::SimpleText;
        result.startPosition = 0;
        result.symbolsCount = content.length();
        result.filePath = this->name;
        fragments << result;
    }
    file.close();
    loaded = true;
    return true;
}

#include "DataStorageWidget.h"
#include "DataViewCommands.h"

DataStorageWidget::DataStorageWidget(QWidget *parent) : QWidget(parent)
{
    QHBoxLayout* mainLayout = new QHBoxLayout(this);

    // Левая часть
    QWidget* leftWidget = new QWidget;
    leftLayout = new QVBoxLayout(leftWidget);
    comboBox = new QComboBox;
    searchButton = new QPushButton("Поиск");
    leftLayout->addWidget(comboBox);
    leftLayout->addWidget(searchButton);
    leftLayout->addStretch();

    leftWidget->setFixedWidth(200);

    // Правая часть
    tabWidget = new QTabWidget;

    mainLayout->addWidget(leftWidget);
    mainLayout->addWidget(tabWidget);

    m_storage = QSharedPointer<DataStorage>(new DataStorage());
    connect(searchButton, &QPushButton::clicked, this, &DataStorageWidget::onSearchButtonClicked);

    dataSearchWidget = new DataSearchWidget;
    tabWidget->addTab(dataSearchWidget, "Поиск данных");
    dataSearchWidget->setStorage(m_storage);
    connect(dataSearchWidget, SIGNAL(commandRequested(QVariant)),
            this, SLOT(onCommand(QVariant)));
}

void DataStorageWidget::initStorage(const QString &path)
{
    m_storage->init(path);
    auto names = m_storage->getFileNames();
    for (auto & name : names)
        addDynamicButton(name);
}

void DataStorageWidget::addDynamicButton(const QString &buttonName) {
    createDynamicButton(buttonName);
}

void DataStorageWidget::createDynamicButton(const QString &buttonName) {
    if (!dynamicButtons.contains(buttonName)) {
        QPushButton* button = new QPushButton(buttonName);
        dynamicButtons[buttonName] = button;
        leftLayout->insertWidget(leftLayout->count() - 1, button);
        connect(button, &QPushButton::clicked, this, [this, buttonName]() {
            onDynamicButtonClicked(buttonName);
        });
    }
}

void DataStorageWidget::onSearchButtonClicked()
{
    tabWidget->setCurrentWidget(dataSearchWidget);
}

void DataStorageWidget::onDynamicButtonClicked(const QString &buttonName) {
    createFileView(buttonName);
    fileViewers[buttonName]->loadFile(buttonName);
}

void DataStorageWidget::createFileView(const QString &fileName)
{
    if (!fileViewers.contains(fileName)) {
        FileViewerWidget* viewer = new FileViewerWidget();
        fileViewers[fileName] = viewer;
        viewer->setStorage(m_storage);
        tabWidget->addTab(viewer, fileName);
    }
    tabWidget->setCurrentWidget(fileViewers[fileName]);
}
void DataStorageWidget::onCommand(const QVariant &commandVar)
{
    if (commandVar.canConvert<DisplayFileCommand>())
    {
        DisplayFileCommand command = commandVar.value<DisplayFileCommand>();
        createFileView(command.fileName);
        fileViewers[command.fileName]->loadFile(command.fileName);
        fileViewers[command.fileName]->selectLine(command.line);
    }
}



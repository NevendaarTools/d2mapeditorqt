#ifndef DATASELECTWIDGET_H
#define DATASELECTWIDGET_H


#include <QWidget>
#include <QLineEdit>
#include <QListWidget>
#include <QVBoxLayout>

class DataSelectWidget : public QWidget
{
    Q_OBJECT

public:
    explicit DataSelectWidget(QWidget *parent = nullptr);

    void addFile(const QString &fileName);
signals:
    void textChanged(const QString &text);
    void fileClicked(const QString &fileName);

private:
    QLineEdit *m_lineEdit;
    QListWidget *m_fileList;
};


#endif // DATASELECTWIDGET_H

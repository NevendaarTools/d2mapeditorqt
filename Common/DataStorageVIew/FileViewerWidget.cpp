#include "FileViewerWidget.h"
#include <QTextBlock>
#include <QTextDocument>

FileViewerWidget::FileViewerWidget(QWidget *parent) : QWidget(parent) {
    QVBoxLayout *layout = new QVBoxLayout(this);
    m_textBrowser = new DataFragmentDisplayWidget(this);
    layout->addWidget(m_textBrowser);
    setLayout(layout);
}

void FileViewerWidget::loadFile(const QString &fileName) {
    //    QFile file(fileName);
    //    if (!file.open(QIODevice::ReadOnly | QIODevice::Text)) {
    //        QMessageBox::critical(this, "Error", "Could not open file");
    //        return;
    //    }

//    QTextStream in(&file);
//    QString content = in.readAll();
//    m_dataFragmentDisplayWidget->setHtml(content);
//    QList<QString> content = m_storage->getFileContentList(fileName);
//    auto fragments = DataStorage::parseContent(content);
//    for (auto & fragment: fragments)
//        fragment.filePath = fileName;
    auto fragments = m_storage->getFileFragments(fileName);

    m_textBrowser->setHtml("");
    m_textBrowser->setDataFragments(fragments);
}

void FileViewerWidget::selectLine(int lineNumber)
{
    m_textBrowser->setLineToDisplay(lineNumber);
}

void FileViewerWidget::setStorage(QSharedPointer<DataStorage> newStorage)
{
    m_storage = newStorage;
    m_textBrowser->setStorage(m_storage);
}

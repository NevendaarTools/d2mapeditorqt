#ifndef DATASTORAGEWIDGET_H
#define DATASTORAGEWIDGET_H
#include <QHBoxLayout>
#include <QWidget>
#include <QComboBox>
#include "DataSelectWidget.h"
#include "DataFragmentDisplayWidget.h"
#include "DataStorage.h"
#include "DataSearchWidget.h"
#include "FileViewerWidget.h"

class DataStorageWidget : public QWidget {
    Q_OBJECT

public:
    DataStorageWidget(QWidget* parent = nullptr);
    void initStorage(const QString & path);
    void addDynamicButton(const QString& buttonName);
private:
    void createDynamicButton(const QString& buttonName);
    void createFileView(const QString &fileName);
private slots:
    void onSearchButtonClicked();
    void onDynamicButtonClicked(const QString& buttonName);
    void onCommand(const QVariant& commandVar);
private:
    QComboBox* comboBox;
    QPushButton* searchButton;
    QVBoxLayout* leftLayout;
    QTabWidget* tabWidget;
    QMap<QString, QPushButton*> dynamicButtons;
    QMap<QString, FileViewerWidget*> fileViewers;
    DataSearchWidget* dataSearchWidget;
    QSharedPointer<DataStorage> m_storage;
};

#endif // DATASTORAGEWIDGET_H

#include "MappedDataFragment.h"

MappedDataFragment MappedDataFragmentHelper::fillFragment(const DataFragment &fragment, const QString &path)
{
    MappedDataFragment resFrag;
    resFrag.fragment = fragment;
    if (fragment.type == DataFragment::FragmentType::LinkedText)
    {
        fillLinkedFragment(doc, resFrag, path);
    }
    if (fragment.type == DataFragment::FragmentType::SimpleText)
    {
        fillSimpleTextFragment(resFrag, path);
    }
    if (fragment.type == DataFragment::FragmentType::Code)
    {
        fillCodeFragment(doc, resFrag, path);
    }
    if (fragment.type == DataFragment::FragmentType::Title)
    {
        fillTitleTextFragment(resFrag, path);
    }
    if (fragment.type == DataFragment::FragmentType::Multy)
    {
        fillMultyFragment(resFrag, path);
    }
    doc.setHtml(resFrag.text);
    resFrag.mappedCount = doc.toPlainText().length();
    return resFrag;
}

void MappedDataFragmentHelper::getText(QString &text, QStringList &content, MappedDataFragment &fragment, const QString &path)
{
    text = storage->getFragmentText(fragment.fragment);
    content << text;
    for (int i = fragment.fragment.highlights.count() - 1; i >= 0; --i)
    {
        auto tmp = fragment.fragment.highlights[i];
        text.insert(tmp.startPosition + tmp.symbolsCount,"</a>");
        text.insert(tmp.startPosition,"<a style=\"background-color:hsla(80, 61%, 50%, 0.5);\">");
    }
    int contentIndex = 1;
    for(DataFragment & attached: fragment.fragment.attached)
    {
        MappedDataFragment attachedFrag = fillFragment(attached, path);
        text += attachedFrag.text;
        auto subContent = attachedFrag.content;
        content.append(subContent);
        contentIndex+= subContent.count();
    }
}

void MappedDataFragmentHelper::fillSimpleTextFragment(MappedDataFragment &fragment, const QString &path)
{
    getText(fragment.text, fragment.content, fragment, path);
    fragment.text.replace("\n","<br>");
}

void MappedDataFragmentHelper::fillTitleTextFragment(MappedDataFragment &fragment, const QString &path)
{
    QString text;
    getText(text, fragment.content, fragment, path);
    text.replace("\n","<br>");
    fragment.text = "<strong>" + text + "</strong><br>";
}

void MappedDataFragmentHelper::fillMultyFragment(MappedDataFragment &fragment, const QString & path)
{
    QString text;
    int contentIndex = 0;
    for(DataFragment & attached: fragment.fragment.attached)
    {
        MappedDataFragment attachedFrag = fillFragment(attached, path + QString("-%1").arg(contentIndex++));
        text += attachedFrag.text;
    }
    fragment.text = "<hr />" + text + "<hr />";
}

void MappedDataFragmentHelper::fillCodeFragment(QTextDocument &doc, MappedDataFragment &fragment, const QString &path)
{
    QString copyText = "<span style='color: gray; font-size: small;'>";
    copyText += QString("<a href='copy:%1'>(copy)</a>").arg(path);
    copyText += "</span>";


    QString text;
    getText(text, fragment.content, fragment, path);

//    if (index == -1)
//        text += "</td></tr></table>";
//    else
        text += "</td><td>" + copyText + "</td></tr></table>";

    fragment.text = "<table style='table-layout:fixed; width: 100%'><tr><td style=\"border: 1px solid black; background: lightgrey; white-space: pre; word-break:break-all;\">";
    fragment.text += text;
}

void MappedDataFragmentHelper::fillLinkedFragment(QTextDocument &doc, MappedDataFragment &fragment, const QString &path)
{
    //fill start
    QString htmlTemplate =
            "<span style='color: gray; font-size: small;'>"
            "<a href='load-before:%4'>(...)</a>"
            "%1 %2:%3 "
            "<a href='open:%4'>(открыть)</a>"
            "</span><br>";

    QString htmlString = htmlTemplate
            .arg(fragment.fragment.filePath)
            .arg(fragment.fragment.startPosition)
            .arg(fragment.fragment.symbolsCount)
            .arg(path);

    QString text;
    getText(text, fragment.content, fragment, path);
    fragment.text = htmlString + text;
    fragment.text += "<span style='color: gray; font-size: small;'>";
    fragment.text += QString("<a href='load-after:%1'>(...)</a>").arg(path);
    fragment.text += "</span><br><br>";
}



#include "DataSelectWidget.h"


DataSelectWidget::DataSelectWidget(QWidget *parent) : QWidget(parent)
{
    m_lineEdit = new QLineEdit(this);
    m_fileList = new QListWidget(this);

    QVBoxLayout *layout = new QVBoxLayout(this);
    layout->addWidget(m_lineEdit);
    layout->addWidget(m_fileList);

    setLayout(layout);

    connect(m_lineEdit, &QLineEdit::textChanged, this, &DataSelectWidget::textChanged);
    connect(m_fileList, &QListWidget::itemClicked, this, [this](QListWidgetItem *item) {
        emit fileClicked(item->text());
    });
}

void DataSelectWidget::addFile(const QString &fileName)
{
    m_fileList->addItem(fileName);
}

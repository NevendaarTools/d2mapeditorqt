#ifndef DATASTORAGE_H
#define DATASTORAGE_H
#include <QString>
#include <QDir>
#include <QDebug>

struct DataHighlight
{
    uint64_t startPosition;
    uint64_t symbolsCount;
    QString tag;
};


struct DataFragment
{
    enum FragmentType
    {
        LinkedText = 0,
        SimpleText,
        Code,
        Multy,
        Title
    };
    static QString fragmentTypeToString(const FragmentType& type) {
        switch (type) {
            case FragmentType::LinkedText:
                return "LinkedText";
            case FragmentType::SimpleText:
                return "SimpleText";
            case FragmentType::Code:
                return "Code";
            case FragmentType::Multy:
                return "Multy";
            case FragmentType::Title:
                return "Title";
            default:
                return "Unknown";
        }
    }

    FragmentType type = FragmentType::LinkedText;

    QString filePath;
    uint64_t startPosition;
    uint64_t symbolsCount;
    QList<DataHighlight> highlights;
    QList<DataFragment> attached;
    void setPath(const QString & path)
    {
        filePath = path;
        for(auto & item: attached)
            item.setPath(path);
    }

    uint64_t realStartPosition;
    uint64_t realSymbolsCount;
};
class IDataFragmentReader;
struct Helper
{
    QHash<QString, IDataFragmentReader*>factories;
    IDataFragmentReader* readerByString(int index, const QString & data)
    {
        QString tmp = data.mid(index, 6);
        if (factories.contains(tmp))
            return factories[tmp];
        tmp = data.mid(index, 5);
        if (factories.contains(tmp))
            return factories[tmp];
        tmp = data.mid(index, 4);
        if (factories.contains(tmp))
            return factories[tmp];
        tmp = data.mid(index, 3);
        if (factories.contains(tmp))
            return factories[tmp];
        tmp = data.mid(index, 2);
        if (factories.contains(tmp))
            return factories[tmp];
        return nullptr;
    }
};
class IDataFragmentReader
{
public:
    ~IDataFragmentReader(){}
    virtual DataFragment parse(Helper & help, int & index, const QString& data) = 0;
};

class SimpleDataFragmentReader: public IDataFragmentReader
{
private:
    QString start;
    QString end;
    DataFragment::FragmentType type;
public:
    SimpleDataFragmentReader(const QString & start_, const QString & end_, DataFragment::FragmentType type_)
        : start(start_),
          end(end_),
          type(type_)
    {

    }

    virtual DataFragment parse(Helper & help, int & index, const QString& data)
    {
        DataFragment result;
        result.type = type;
        result.realStartPosition = index;
        index+=start.length();
        result.startPosition = index;
        for (; index < data.length() - end.length() + 1; index++)
        {
            if (data.mid(index, end.length()) == end)//todo: optimise
            {
                qDebug()<<end<<" "<<data.mid(result.startPosition, index - result.startPosition);
                break;
            }
        }
        result.symbolsCount = index - result.startPosition;
        result.realSymbolsCount = result.symbolsCount + end.length() + start.length();
        return result;
    }
};

class MultiBlockReader: public IDataFragmentReader
{
public:
    virtual DataFragment parse(Helper & help, int & index, const QString& data)
    {
        DataFragment result;
        result.type = DataFragment::Multy;
        result.realStartPosition = index;
        index+=3;
        result.startPosition = index;
        for (; index < data.length() - 2; index++)
        {
            IDataFragmentReader * fact = help.readerByString(index, data);
            if (fact)
            {
                result.attached << fact->parse(help, index, data);
            }
            if (data.mid(index, 3) == "}}}")//todo: optimise
                break;
        }
        result.symbolsCount = index - result.startPosition;
        uint64_t last = result.startPosition - 3;
        QList<DataFragment> textFragments;
        for(auto & data: result.attached)
        {
            if (data.startPosition - last > 10)
            {
                DataFragment result;
                result.type = DataFragment::SimpleText;
                result.startPosition = last + 3;
                result.symbolsCount = data.startPosition - last - 6;
                textFragments << result;
            }
            last = data.startPosition + data.symbolsCount;
        }
        result.attached+= textFragments;
        std::sort(result.attached.begin(), result.attached.end(), [](const DataFragment a, const DataFragment b) -> bool
            { return a.startPosition < b.startPosition; });
        index+=2;
        result.realSymbolsCount = index - result.realStartPosition;
        return result;
    }
};

//FileViewerWidget
struct FileRepresentation
{
    QList<QString> data;
    QList<uint64_t> positions;
    QList<DataFragment> fragments;
    QString path;
    QString name;
    QString content;
    bool loaded = false;
    bool load();
};

class DataStorage
{
public:
    DataStorage();
    void init(const QString & directoryPath);

    QList<DataFragment> getFragmentsWithSubstring(const QString& substr);
    QString getFragmentText(const DataFragment& frag);
    QString getFragmentText(const QString & path, int line, int count);
    uint64_t getFileSize(const QString & path) const;
    QList<QString> getFileNames() const{return m_files.keys();}
    QString getFileContent(const QString & path) const;
    QList<QString> getFileContentList(const QString & path) const;
    QString getFilePath(const QString & path) const;
    QList<DataFragment> getFileFragments(const QString & path);
private:
    QString m_path;
    QHash<QString, QSharedPointer<FileRepresentation>> m_files;
};

#endif // DATASTORAGE_H

#ifndef DATASEARCHWIDGET_H
#define DATASEARCHWIDGET_H

#include <QWidget>
#include <QLineEdit>
#include "DataFragmentDisplayWidget.h"

#include <QVBoxLayout>
#include <QLabel>
#include <QHBoxLayout>
#include <QPushButton>
#include <QListWidget>
#include <QTextEdit>

class DataSearchWidget : public QWidget {
    Q_OBJECT
public:
    explicit DataSearchWidget(QWidget *parent = nullptr);

    void setStorage(QSharedPointer<DataStorage> newStorage);

private slots:
    void updateSearchResults();
signals:
    void commandRequested(const QVariant & command);
private:
    QLineEdit *m_lineEdit;
    DataFragmentDisplayWidget *m_display;
    QLabel* m_countLabel;
    QSharedPointer<DataStorage> m_storage;
};

#endif // DATASEARCHWIDGET_H

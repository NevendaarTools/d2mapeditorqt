#ifndef FILEVIEWERWIDGET_H
#define FILEVIEWERWIDGET_H

#include <QApplication>
#include <QWidget>
#include <QVBoxLayout>
#include <QTextEdit>
#include <QFile>
#include <QTextStream>
#include <QMessageBox>
#include "DataFragmentDisplayWidget.h"

class FileViewerWidget : public QWidget {
    Q_OBJECT

public:
    FileViewerWidget(QWidget *parent = nullptr);

    void loadFile(const QString &fileName);
    void selectLine(int lineNumber);
    void setStorage(QSharedPointer<DataStorage> newStorage);

private:
    DataFragmentDisplayWidget *m_textBrowser;
    QSharedPointer<DataStorage> m_storage;
};


#endif // FILEVIEWERWIDGET_H

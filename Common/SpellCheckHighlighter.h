#ifndef SPELLCHECKHIGHLIGHTER_H
#define SPELLCHECKHIGHLIGHTER_H

#include <QObject>
#include <QSyntaxHighlighter>
#include "HunspellChecker.h"
#include <QQuickTextDocument>

class SpellCheckHighlighter : public QSyntaxHighlighter {
    Q_OBJECT
public:
    explicit SpellCheckHighlighter(QTextDocument *document);
signals:
    void documentChanged();

protected:
    void highlightBlock(const QString &text) override;
private:
};

class SpellCheckWrapper : public QObject
{
    Q_OBJECT
    Q_PROPERTY(QQuickTextDocument* document READ document WRITE setDocument NOTIFY documentChanged)

public:
    explicit SpellCheckWrapper(QObject *parent = nullptr);

    QQuickTextDocument* document() const { return m_document; }
    void setDocument(QQuickTextDocument* document);

signals:
    void documentChanged();

private:
    QQuickTextDocument* m_document;
    SpellCheckHighlighter* m_highlighter;
};


#endif // SPELLCHECKHIGHLIGHTER_H


#include "HunspellChecker.h"
#include <QDebug>

HunspellChecker::HunspellChecker() : m_hunspell(nullptr) {}

HunspellChecker::~HunspellChecker()
{
    if (m_hunspell) {
        m_hunspellDLL.destroy(m_hunspell);
    }
}

bool HunspellChecker::initialize(const QString &dllPath, const QString &affPath, const QString &dicPath)
{
    qDebug()<<Q_FUNC_INFO;
    if (!m_hunspellDLL.load(dllPath)) {
        qDebug()<<"Failed to load dll";
        return false;
    }

    if (m_hunspell) {
        m_hunspellDLL.destroy(m_hunspell);
    }

    m_hunspell = m_hunspellDLL.create(affPath.toUtf8().constData(), dicPath.toUtf8().constData());
    if (m_hunspell == nullptr)
        qDebug()<<"failed to create hunspell";
    return m_hunspell != nullptr;
}

bool HunspellChecker::spell(const QString &word)
{
    if (!m_hunspell)
        return false;
    return m_hunspellDLL.spell(m_hunspell, word.toUtf8().constData()) != 0;
}

QStringList HunspellChecker::suggest(const QString &word)
{
    QStringList suggestions;
    if (word.isEmpty())
        return suggestions;
    if (!m_hunspell) return suggestions;

    int suggestionsCount = 0;
    char **suggestionList = m_hunspellDLL.suggest(m_hunspell, word.toUtf8().constData(), &suggestionsCount);

    for (int i = 0; i < suggestionsCount; ++i) {
        suggestions.append(QString::fromUtf8(suggestionList[i]));
    }

    m_hunspellDLL.free_list(m_hunspell, suggestionList, suggestionsCount);

    return suggestions;
}

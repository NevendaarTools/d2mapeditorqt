import QtQuick 2.14
import QtQuick.Controls 2.14
import QtQuick.Layouts 1.15
import QMLSpellsSelectModel 1.0

Item {
    signal itemDBLClicked(string item);
    property alias listWidth: itemsList.width
    Item{
        id: filterArea
        anchors.top: parent.top
        width: parent.width
        height: 58
        Rectangle
        {
            anchors.fill: parent
            color: "gray"
        }
        TextFilterItem{
            id:nameFilter
            anchors.top: parent.top
            anchors.left: parent.left
            anchors.right: parent.right
            anchors.margins: 5
            height: 28
            text: itemsModel.filter
            onFilterChanged: {
                itemsModel.filter = text
            }
            onSortModeChanged: {
                itemsModel.mode = mode;
            }
            model: ListModel {
                id: nameModel
                ListElement { name: "Name" }
                ListElement { name: "Cost" }
            }
        }

        Rectangle{
            anchors.top: nameFilter.bottom
            anchors.topMargin: 5
            anchors.left: parent.left
            anchors.leftMargin: 5
            width: parent.width - 10
            height: 30
            color: "gray"
            border.color: "transparent"
            border.width: 2
            ListView{
                id: tagsGroupList
                anchors.fill: parent
                spacing: 3
                clip: true
                orientation: Qt.Horizontal
                interactive: false
                model: itemsModel.tagGroupsCount
                delegate:  NTTextButton {
                    width:  64
                    height: 18
                    font.pointSize: 10
                    text: itemsModel.tagModel(index).groupName
                    acceptedButtons: Qt.LeftButton | Qt.RightButton
                    onClicked:
                    {
                        if (mouse.button === Qt.RightButton)
                        {
                            itemsModel.tagModel(index).clearTags();
                            return;
                        }

                        if (tagsList.model === itemsModel.tagModel(index) && tagsList.visible)
                        {
                            tagsList.visible = false
                            return;
                        }
                        tagsList.visible = true
                        tagsList.model = itemsModel.tagModel(index)
                    }
                    Rectangle{
                        id: hasSelectedRect
                        color: "transparent"
                        border.color: "brown"
                        border.width: 2
                        anchors.fill: parent
                        visible: itemsModel.tagModel(index).hasSelected
                    }
                    Rectangle{
                        color: "transparent"
                        border.color: "black"
                        border.width: 1
                        anchors.margins: 3
                        anchors.fill: parent
                        visible: tagsList.model === itemsModel.tagModel(index) && tagsList.visible
                    }
                }
            }
        }
    }
    Rectangle
    {
        anchors.fill: itemsList
        anchors.margins: -5
        color: "gray"
    }
    ListView{
        SpellsSelectModel{
            id: itemsModel
        }
        ScrollBar.vertical: ScrollBar {
            active: true
        }
        id: itemsList
        anchors.top: filterArea.bottom
        anchors.topMargin: 5
        anchors.bottom: parent.bottom
        anchors.bottomMargin: 5
        anchors.left: parent.left
        anchors.leftMargin: 5
        width: filterArea.width - 10

        spacing: 2
        clip: true
        orientation: Qt.Vertical

        model: itemsModel
        delegate:  Item {
            id: deleg
            width:  itemsList.width
            height: 105

            Drag.active: dragArea.drag.active
            Drag.dragType: Drag.Automatic
            Drag.mimeData: {
                "id": Id,
                "mode": "SpellAdd"
            }
            MouseArea {
                id: dragArea
                anchors.top: parent.top
                anchors.bottom: parent.bottom
                anchors.left: parent.left
                width: parent.width/2
                drag.target: icon
                onPressed: icon.grabToImage(function(result) {
                    parent.Drag.imageSource = result.url
                })
                onDoubleClicked: itemDBLClicked(Id)
            }
            Rectangle
            {
                color: "grey"
                border.color: "black"
                border.width: 1
                anchors.fill: parent
            }

            Image {
                anchors.left: parent.left
                anchors.leftMargin: 0
                anchors.verticalCenter: parent.verticalCenter
                id: icon
                source: "image://provider/" + Icon
                width: 64
                height: 64

            }
            Column{
                x: 64
                width: parent.width - 64
                anchors.top: parent.top
                anchors.topMargin: 2
                height: 90
                spacing: 2
                Text {
                    id: name
                    text: Name
                    font.bold: itemsModel.mode == 0
                }
                Text {
                    id: desc
                    width: parent.width
                    wrapMode: Text.WordWrap
                    text: Desc
                }

            }
            NTCostView {
                id: costView
                width: costView.model.rowCount() * (costView.editable?112:48)
                anchors.horizontalCenter: parent.horizontalCenter
                anchors.bottom: parent.bottom
                anchors.margins: 2
                value: Cost
            }
        }
    }

    Rectangle{
        color: "gray"
        border.color: "black"
        border.width: 2
        anchors.fill: tagsList
        visible: tagsList.visible
        anchors.margins: -5
        anchors.topMargin: -50
        Button{
            anchors.top: parent.top
            anchors.margins: 10
            anchors.right: parent.right
            width: 60
            height: 30
            text: translate.tr("Close")
            onClicked: {
                tagsList.visible = false;
            }
        }
        Button{
            anchors.top: parent.top
            anchors.margins: 10
            anchors.left: parent.left
            width: 60
            height: 30
            text: "Clear"
            onClicked: {
                tagsList.model.clearTags()
            }
        }
    }

    ListView{
        ScrollBar.vertical: ScrollBar {
            active: true
        }
        id: tagsList
        anchors.top: filterArea.top
        anchors.topMargin: 90
        anchors.left: filterArea.right
        anchors.bottomMargin: 40
        anchors.bottom: parent.bottom
        anchors.leftMargin: 10
        width: filterArea.width - 20

        spacing: 7
        clip: true
        orientation: Qt.Vertical
        visible: false

        model: 0
        delegate:  NTCheckBox {
            width:  itemsList.width
            height: 25
            text: translate.tr(Name)
            checked: Checked
            onClicked: {
                if (mouse.button === Qt.RightButton)
                {
//                    tagsList.model.setInversed(index)
                    tagsList.model.setExclusive(index)
                    return;
                }
                tagsList.model.setChecked(index, checked)
            }
        }
    }
}

import QtQuick 2.0
import QMLItemView 1.0
import QMLCostEditor 1.0
import QtQuick.Controls 2.14
import QtQuick.Layouts 1.15

ListView{
    property alias value: costEditor.value
    property alias editable: costEditor.editable
    signal valueModified(string value)

    CostEditor{
        id:costEditor

        onValueChanged: {
            costView.valueModified(costEditor.value);
        }
    }
    //        ScrollBar.vertical: ScrollBar {
    //            active: true
    //        }
    id: costView
    height: 18

    spacing: 2
    //clip: true
    orientation: Qt.Horizontal

    model: costEditor
    interactive: false
    delegate:  NTSpinBoxInt {
        id: deleg
        width:  editable?112:48
        height: editable?24:18
        imageSource: "image://provider/" + Icon
        editable: costView.editable
        step:25
        value: Count
        onValueModified: (value) => {
            if (value !== Count)
                costEditor.setValue(index, value + "");
        }
    }
}

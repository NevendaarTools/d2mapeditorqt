import QtQuick 2.15
import QMLMainMenuController 1.0
import QtQuick.Layouts 1.15
import QtQuick.Controls 2.15
import QMLStyleManager 1.0
import QMLDynamicImageItem 1.0
import NevendaarTools 1.0

NTRectangle{
    property alias editor: textEdit
    StyleManager
    {
        id: style
    }
    function insertTextAtCursor(newText) {
        var position = textEdit.cursorPosition;
        textEdit.insert(position, newText);
    }

    ScrollView {
        id: scrollView
        anchors.fill: parent
        TextEdit {
            id: textEdit
            font.pointSize: style.fontSize
            width: scrollView.width - 5
            x: 5
            height: Math.max(scrollView.height, implicitHeight + 50)
            topPadding:50
            bottomPadding: 100
            anchors.margins: 5
            selectionColor:  "grey"
            selectByMouse: true
            onTextChanged: {
                autoCompleteModel.updateCompletions(text, textEdit.cursorPosition)
                if (autoCompleteModel.rowCount() > 0) {
                    popup.open()
                    popup.currentIndex = 0
                } else {
                    popup.close()
                }
            }

            Keys.onDownPressed: function(event){
                if (popup.visible) {
                    if (popup.currentIndex < autoCompleteModel.rowCount() - 1)
                        popup.currentIndex++
                }
                else
                    event.accepted = false
            }
            Keys.onUpPressed: function(event){
                if (popup.visible) {
                    if (popup.currentIndex > 0)
                        popup.currentIndex--
                }
                else
                    event.accepted = false
            }

            Keys.onTabPressed: function(event){
                // if (popup.visible && popup.currentIndex >= 0) {
                //     insertCompletion()
                //     event.accepted = true
                // }
                textEdit.insert(textEdit.cursorPosition, '    ')
            }
            Keys.onReturnPressed: function(event){
                if (!(event.modifiers & Qt.ShiftModifier)) {
                    if (popup.visible && popup.currentIndex >= 0) {
                        insertCompletion()
                        event.accepted = true
                    }
                    else {
                        event.accepted = false
                    }
                } else {
                    event.accepted = false
                }
            }
            AutoCompleteModel {
                id: autoCompleteModel
            }
            Popup {
                id: popup
                y: textEdit.cursorRectangle.bottom
                x: textEdit.cursorRectangle.x
                property alias currentIndex: listView.currentIndex
                width: 200
                height: Math.min(200, listView.contentHeight)
                contentItem: NTRectangle{
                    anchors.fill: parent
                }

                ListView {
                    id: listView
                    anchors.fill: parent
                    model: autoCompleteModel
                    currentIndex: -1
                    delegate: ItemDelegate {
                        width: popup.width
                        height: 30
                        text: model.Text
                        font.pointSize: style.fontSize
                        highlighted: ListView.isCurrentItem
                        contentItem: NTRectangle{
                            anchors.fill: parent
                            height: 30
                            NTRectangle{
                                color: "white"
                                opacity: 0.3
                                visible: listView.currentIndex == index
                                anchors.fill: parent
                            }

                            Text {
                                text: model.Text
                                anchors.centerIn: parent
                            }
                        }

                        // x: -12
                        // y: -12
                        onClicked: {
                            textEdit.insertCompletion()
                        }
                        Keys.onReturnPressed: insertCompletion()
                        Keys.onTabPressed: {
                            textEdit.insert(textEdit.cursorPosition, '    ')
                        }
                    }
                }
            }

            function insertCompletion() {
                if (popup.visible && listView.currentIndex >= 0) {
                    var cursorPosition = textEdit.cursorPosition
                    var text = textEdit.text
                    var wordStart = autoCompleteModel.lastWorldStart(textEdit.text, cursorPosition)
                    var completion = autoCompleteModel.data(autoCompleteModel.index(listView.currentIndex, 0), AutoCompleteModel.TextRole)
                    console.log("#completion " + completion)
                    var wordEnd = cursorPosition
                    while (wordEnd < text.length && !/\s/.test(text.charAt(wordEnd))) {
                        wordEnd++
                    }
                    textEdit.remove(wordStart, wordEnd)
                    textEdit.insert(wordStart, completion)
                    textEdit.cursorPosition = wordStart + completion.length
                    popup.close()
                }
            }
        }
    }
}

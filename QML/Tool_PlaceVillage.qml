import QtQuick 2.15
import QtQuick.Layouts 1.2
import QtQuick.Controls 2.0
import QMLMainToolBarController 1.0
import QMLPlaceVillageTool 1.0
import QMLToolsProvider 1.0
import QMLGroupUnitsEditor 1.0

Item {
    id: tool_PlaceStack
    property ToolsProvider toolsProv
    property PlaceVillageTool toolPlaceVillage

    function init(tool)
    {
        toolsProv = tool;
        toolPlaceVillage = toolsProv.currentToolObj();
        name.text = toolPlaceVillage.toolName()
        toolPlaceVillage.init()
        imageSpin.value = toolPlaceVillage.villageLvl
    }

    Text {
        id: name
        height: 24
        horizontalAlignment: TextInput.AlignLeft
        verticalAlignment: TextInput.AlignVCenter
        font.pointSize: 14
        anchors.top: parent.top
        anchors.left: parent.left
        anchors.right: parent.right
        anchors.margins: 5
    }
    ImageButton{
        anchors.top: parent.top
        anchors.right: parent.right
        width: 24
        height: 24
        anchors.margins: 5
        source: "image://provider/Exit"
        onClicked:
        {
            toolsProv.closeTool();
        }
    }

    Item{
        anchors.horizontalCenter: parent.horizontalCenter
        anchors.top: name.bottom
        width: parent.width - 10
        height: width
        id: iconRect
        anchors.topMargin: 10
        Image {
            anchors.centerIn: parent
            id: icon
            sourceSize: Qt.size(160, 160)
            source: "image://provider/IsoCmon-IsoAnim-" + toolPlaceVillage.imageName(toolPlaceVillage.villageLvl)
        }
    }

    NTSpinBoxInt {
        id: imageSpin
        height: 22
        width: 80
        anchors.top: iconRect.bottom
        min: 1
        max: 5
        backgroundRect.visible: true
        anchors.horizontalCenter: parent.horizontalCenter
        editable: true
        image.width: 0
        anchors.topMargin: -30

        onValueChanged:
        {
            toolPlaceVillage.villageLvl = value
        }
    }
}

import QtQuick 2.14
import QtQuick.Controls 2.14
import QtQuick.Layouts 1.15
import QMLUnitSelectModel 1.0
import QMLFilterSettingsModel 1.0

Item{
    id: filterArea

    property FilterSettingsModel settingsModel
    property bool openToRight: true
    property alias tagListHeight: tagsList.height
    property alias sortVisible: nameFilter.sortVisible
    property bool filterVisible: true
    Rectangle
    {
        anchors.fill: parent
        color: "gray"
    }

    TextFilterItem{
        id:nameFilter
        anchors.top: parent.top
        anchors.left: parent.left
        anchors.right: parent.right
        anchors.margins: 0
        anchors.topMargin: 5
        height: filterArea.filterVisible ? 28 : 0
        text: settingsModel.filter
        onFilterChanged: {
            settingsModel.filter = text
        }
        onSortModeChanged: {
            settingsModel.sortMode = mode;
        }
        model: settingsModel.sortModes
    }

    Rectangle{
        anchors.top: nameFilter.bottom
        anchors.topMargin: 5
        anchors.left: parent.left
        anchors.leftMargin: 5
        width: parent.width - 10
        height: 30
        color: "gray"
        border.color: "transparent"
        border.width: 2
        ListView{
            id: tagsGroupList
            anchors.fill: parent
            spacing: 3
            clip: true
            orientation: Qt.Horizontal
            interactive: false
            model: settingsModel.tagGroupsCount
            delegate:  NTTextButton {
                width:  64
                height: 18
                font.pointSize: 10
                text: settingsModel.tagModel(index).groupName
                acceptedButtons: Qt.LeftButton | Qt.RightButton
                onClicked: (mouse)=>
                {
                    if (mouse.button === Qt.RightButton)
                    {
                        settingsModel.tagModel(index).clearTags();
                        return;
                    }

                    if (tagsList.model === settingsModel.tagModel(index) && tagsList.visible)
                    {
                        tagsList.visible = false
                        return;
                    }
                    tagsList.visible = true
                    tagsList.model = settingsModel.tagModel(index)
                }
                Rectangle{
                    id: hasSelectedRect
                    color: "transparent"
                    border.color: "brown"
                    border.width: 2
                    anchors.fill: parent
                    visible: settingsModel.tagModel(index).hasSelected
                }
                Rectangle{
                    color: "transparent"
                    border.color: "black"
                    border.width: 1
                    anchors.margins: 3
                    anchors.fill: parent
                    visible: tagsList.model === settingsModel.tagModel(index) && tagsList.visible
                }
            }
        }
    }
    Rectangle{
        color: "gray"
        border.color: "black"
        border.width: 2
        anchors.fill: tagsList
        visible: tagsList.visible
        anchors.margins: -5
        anchors.topMargin: filterArea.filterVisible ? -50 : -78
        Button{
            anchors.top: parent.top
            anchors.margins: 10
            anchors.right: parent.right
            width: 60
            height: 30
            text: translate.tr("Close")
            onClicked: ()=> {
                tagsList.visible = false;
            }
        }
        Button{
            anchors.top: parent.top
            anchors.margins: 10
            anchors.left: parent.left
            width: 60
            height: 30
            text: "Clear"
            onClicked: ()=>{
                tagsList.model.clearTags()
            }
        }
    }

    ListView{
        ScrollBar.vertical: ScrollBar {
            active: true
        }
        id: tagsList
        anchors.top: filterArea.top
        anchors.topMargin: openToRight?90:110
        anchors.left: openToRight?filterArea.right:filterArea.left
        anchors.leftMargin: 10
        //anchors.bottomMargin: 40
        height: 600
        //anchors.bottom: parent.bottom
        width: filterArea.width - 20

        spacing: 7
        clip: true
        orientation: Qt.Vertical
        visible: false

        model: 0
        delegate:  NTCheckBox {
            width:  filterArea.width
            height: 25
            text: translate.tr(Name)
            checked: Checked
            onClicked: (checked, mouse)=>{
                if (mouse.button === Qt.RightButton)
                {
//                    tagsList.model.setInversed(index)
                    tagsList.model.setExclusive(index)
                    return;
                }
                tagsList.model.setChecked(index, checked)
            }
        }
    }
}

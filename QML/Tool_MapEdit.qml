import QtQuick 2.12
import QtQuick.Controls 2.14
import QMLMapEditTool 1.0
import Qt.labs.qmlmodels 1.0
import QtQuick.Layouts 1.15
import QMLOptionsModel 1.0
import QMLToolsProvider 1.0

Item {
    id: item

    property MapEditTool layersTool
    property ToolsProvider toolsProv

    function init(tool)
    {
        toolsProv = tool;
        layersTool = toolsProv.currentToolObj();
        terrainView.model = layersTool.terrainModel
        brushSizeSpinBox.value = layersTool.brushSize
        mountainsView.model = layersTool.mountainsModel
        mountainsFilterArea.settingsModel = layersTool.mountainsModel.filterSettings
        treeFilterArea.settingsModel = layersTool.treesModel.filterSettings
        treeView.model = layersTool.treesModel
    }

    Text {
        id: name
        height: 24
        horizontalAlignment: TextInput.AlignLeft
        verticalAlignment: TextInput.AlignVCenter
        font.pointSize: 14
        anchors.top: parent.top
        anchors.left: parent.left
        anchors.right: parent.right
        anchors.margins: 5
        text: "Map edit"
    }

    NTImageButton{
        anchors.top: parent.top
        anchors.right: parent.right
        width: 24
        height: 24
        anchors.margins: 5
        source: "image://provider/Exit"
        onClicked:
        {
            toolsProv.closeTool();
        }
    }

    Text {
        id: brushSizeText
        anchors.left: parent.left
        height: 25
        anchors.top: name.bottom
        width: 80
        anchors.margins: 5
        text: "Brush size"
        horizontalAlignment: TextInput.AlignHCenter
        verticalAlignment: TextInput.AlignVCenter
        font.pointSize: 11
    }

    NTSpinBoxInt {
        id: brushSizeSpinBox
        anchors.left: brushSizeText.right
        height: 25
        anchors.right: parent.right
        anchors.top: name.bottom
        anchors.margins: 5
        step: 2
        min: 1
        max: 9
        //imageSource: "image://provider/" + Icon
        editable: true
        onValueModified: {
            layersTool.brushSize = value
        }
    }

    GridView {
        id: terrainView
        anchors.left: parent.left
        anchors.margins: 5
        anchors.right: parent.right
        anchors.top: brushSizeSpinBox.bottom
        anchors.topMargin: 5
        cellWidth: 35; cellHeight: 35
        height: 35
        model: 0
        clip: true
        interactive: false
        delegate: Rectangle {

            width: 32
            height: 32
            Image {
                //width: 58
                //height: 58
                anchors.centerIn: parent
                sourceSize: Qt.size(parent.width, parent.height)
                source: "image://provider/PalMap-" + Id
            }
            Rectangle
            {
                color: "transparent"
                border.color: "green"
                border.width: 2
                visible: index === layersTool.brushIndex && layersTool.brushType === MapEditTool.Terrain
                anchors.fill: parent
            }
            MouseArea{
                anchors.fill: parent
                onClicked: {
                    layersTool.brushType = 0
                    layersTool.brushIndex = index
                }
            }
        }
    }

    NTImageButton {
        id: waterButton
        source: "image://provider/IntfScen-DLG_MAP_WAT_N"
        width: 32
        height: 32
        anchors.left: parent.left
        anchors.top: terrainView.bottom
        anchors.margins: 5
        Rectangle
        {
            color: "transparent"
            border.color: "green"
            border.width: 2
            visible: layersTool.brushType === MapEditTool.Water
            anchors.fill: parent
        }
        onClicked: {
            layersTool.brushType = MapEditTool.Water
        }
        tooltip: "Place water"
    }
    NTImageButton {
        id: landButton
        source: "image://provider/PalMap-ISONE"
        width: 32
        height: 32
        anchors.left: waterButton.right
        anchors.top: terrainView.bottom
        anchors.margins: 5
        Rectangle
        {
            color: "transparent"
            border.color: "green"
            border.width: 2
            visible: layersTool.brushType === MapEditTool.Land
            anchors.fill: parent
        }
        onClicked: {
            layersTool.brushType = MapEditTool.Land
        }
        tooltip: "Place land"
    }
    NTImageButton {
        id: roadButton
        source: "image://provider/IsoTerrn-ROAD0200"
        width: 32
        height: 32
        anchors.left: landButton.right
        anchors.top: landButton.top
        anchors.margins: 5
        anchors.leftMargin: 15
        anchors.topMargin: 0
        Rectangle
        {
            color: "transparent"
            border.color: "green"
            border.width: 2
            visible: layersTool.brushType === MapEditTool.Road
            anchors.fill: parent
        }
        onClicked: {
            layersTool.brushType = MapEditTool.Road
            layersTool.brushIndex = 0
        }
        tooltip: "Place road"
    }
    NTImageButton {
        id: clearButton
        source: "image://provider/RemoveTool"
        width: 32
        height: 32
        anchors.right: parent.right
        anchors.top: terrainView.bottom
        anchors.margins: 5
        Rectangle
        {
            color: "transparent"
            border.color: "green"
            border.width: 2
            visible: layersTool.brushType === MapEditTool.Clear
            anchors.fill: parent
        }
        onClicked: {
            layersTool.brushType = MapEditTool.Clear
        }
        tooltip: "Clear landscape"
    }
    NTImageButton {
        id: treeButton
        source: "image://provider/IntfScen-DLG_MAP_FOR_N"
        // source: "image://provider/PalMap-IsoTerrn-NEF0000"
        width: 32
        height: 32
        anchors.left: parent.left
        anchors.top: landButton.bottom
        anchors.margins: 5
        Rectangle
        {
            color: "transparent"
            border.color: "green"
            border.width: 2
            visible: layersTool.brushType === MapEditTool.Trees
            anchors.fill: parent
        }
        onClicked: {
            layersTool.brushType = MapEditTool.Trees
            layersTool.brushIndex = 0
        }
        tooltip: "Place trees"
    }

    NTImageButton {
        id: mountainButton
        source: "image://provider/IntfScen-DLG_MAP_BMOUNT_N"
        width: 32
        height: 32
        anchors.left: treeButton.right
        anchors.top: landButton.bottom
        anchors.margins: 5
        Rectangle
        {
            color: "transparent"
            border.color: "green"
            border.width: 2
            visible: layersTool.brushType === MapEditTool.Mountains
            anchors.fill: parent
        }
        onClicked: {
            layersTool.brushType = MapEditTool.Mountains
            layersTool.brushIndex = 0
        }
        tooltip: "Place mountains"
    }


    GridView {
        id: mountainsView
        visible: layersTool.brushType === MapEditTool.Mountains
        anchors.left: parent.left
        //anchors.leftMargin: 15
        anchors.margins: 5
        anchors.right: parent.right
        anchors.top: mountainsFilterArea.bottom
        anchors.topMargin: 15
        anchors.bottom: parent.bottom
        cellWidth: 80; cellHeight: 70
        model: 0
        clip: true
        //interactive: false
        delegate: Rectangle {

            width: 64
            height: 64
            Image {
                //width: 58
                //height: 58
                anchors.centerIn: parent
                sourceSize: Qt.size(parent.width, parent.height)
                source: "image://provider/" + Icon
            }
            Text {
                id: sizeText
                text: Size  > 0 ? (Size + " x " + Size) : ""
                anchors.horizontalCenter: parent.horizontalCenter
                anchors.bottom: parent.bottom
            }
            Rectangle
            {
                color: "transparent"
                border.color: "green"
                border.width: 2
                visible: index === layersTool.brushIndex && MapEditTool.Mountains
                anchors.fill: parent
            }
            MouseArea{
                anchors.fill: parent
                onClicked: layersTool.brushIndex = index
            }
        }
    }
    FilterAreaWidget {
        id: mountainsFilterArea
        anchors.top: mountainButton.top
        anchors.topMargin: 0
        anchors.left: mountainButton.right
        height: 25
        width: 170
        openToRight: false
        tagListHeight: 160
        sortVisible: false
        filterVisible: false
        visible: mountainsView.visible
        //settingsModel: toolPlaceLmark.model.filterSettings
    }

    GridView {
        id: treeView
        visible: layersTool.brushType === MapEditTool.Trees
        anchors.left: parent.left
        //anchors.leftMargin: 15
        anchors.margins: 5
        anchors.right: parent.right
        anchors.top: treeFilterArea.bottom
        anchors.topMargin: 15
        anchors.bottom: parent.bottom
        cellWidth: 80; cellHeight: 70
        model: 0
        clip: true
        //interactive: false
        delegate: Rectangle {

            width: 64
            height: 64
            Image {
                //width: 58
                //height: 58
                anchors.centerIn: parent
                sourceSize: Qt.size(parent.width, parent.height)
                source: "image://provider/" + Back
            }
            Image {
                //width: 58
                //height: 58
                anchors.centerIn: parent
                sourceSize: Qt.size(parent.width, parent.height)
                source: "image://provider/" + Icon
            }

            Rectangle
            {
                color: "transparent"
                border.color: "green"
                border.width: 2
                visible: index === layersTool.brushIndex && MapEditTool.Trees
                anchors.fill: parent
            }
            MouseArea{
                anchors.fill: parent
                onClicked: {
                    layersTool.brushIndex = index
                }
            }
        }
    }
    FilterAreaWidget {
        id: treeFilterArea
        anchors.top: mountainButton.top
        anchors.topMargin: 0
        anchors.left: mountainButton.right
        height: 25
        width: 170
        openToRight: false
        tagListHeight: 160
        sortVisible: false
        filterVisible: false
        visible: treeView.visible
        //settingsModel: toolPlaceLmark.model.filterSettings
    }
}

import QtQuick 2.0
import QMLUnitEditor 1.0
import QtQuick.Controls 2.15
import QMLStyleManager 1.0

Dialog {
    property alias unitId: unitEditor.unitid
    property alias unitLevel: unitEditor.level
    property alias unitUid: unitEditor.unitUid
    property bool acceptChanges : false
    visible: true
    width: style.editorWidht
    height: style.editorHeight
    focus:true
    id: dialog

    onClosed: {
        console.log("Dialog is closing by other means");
        console.log("Accepted "  + acceptChanges);
        if (acceptChanges)
        {
            unitEditor.save()
        }
        else
        {
            unitEditor.cancel();
        }
    }
    onAccepted: apply()
    onDiscarded: cancel()
    function cancel()
    {
        console.log("cancel")
        acceptChanges = false;
        dialog.close()
    }
    function apply()
    {
        console.log("apply")
        acceptChanges = true;
        dialog.close()
    }

    StyleManager
    {
        id: style
    }
    UnitEditor{
        id: unitEditor
        onUnitChanged: {

        }
    }
    NTRectangle {
        id: background
        anchors.fill: parent
        anchors.margins: -12
    }
    ListView{
        x:200
        anchors.top: parent.top
        anchors.bottom: parent.bottom
        //anchors.right: parent.right
        width: 200
        delegate: Rectangle {

            width: 200
            height: 22
            color: "green"
            Image {
                //width: 58
                //height: 58
                anchors.centerIn: parent
                sourceSize: Qt.size(parent.width, parent.height)
                source: "image://provider/" + Icon
            }
        }
    }

    Rectangle
    {
        x: 0
        y: attack1Start.y+ 2
        height: attack1End.y - y + attack1End.height + 8
        width: table.width
        color: "gray"
        border.color: "black"
        border.width: 1
    }
    Rectangle
    {
        x: 0
        y: attack2Start.y+ 2
        height: attack2End.y - y + attack2End.height + 8
        width: table.width
        visible: attack2Start.visible
        color: "gray"
        border.color: "black"
        border.width: 1
    }

    Column{
        x: 10
        width: 340
        anchors.top: parent.top
        anchors.topMargin: 5
        height: parent.height - 10
        spacing: 2
        id: table
        property int nameW: 80
        Text {
            id: name
            font.bold: true
            text: unitEditor.name
        }

        Row{
            Text {
                width: table.nameW
                text: translate.tr("Level:")
            }
            Text {
                text: unitEditor.level + " (" + unitEditor.limit + ")"
            }
        }
        Row{
            Text {
                width: table.nameW
                text: translate.tr("Exp:")
            }
            Text {
                text: unitEditor.currentExp + "/" + unitEditor.exp
            }
        }
        Row{
            Text {
                width: table.nameW
                text: translate.tr("Exp kill:")
            }
            Text {
                text: unitEditor.expKill
            }
        }
        Item {
            height: 15
            width: table.nameW
        }

        Row{
            Text {
                width: table.nameW
                text: translate.tr("HitPoints:")
            }
            Text {
                text: unitEditor.currentHp + "/" + unitEditor.hp
            }
        }

        Row{
            Text {
                width: table.nameW
                text: translate.tr("Regen:")
            }
            Text {
                text: unitEditor.regen
            }
        }

        Row{
            Text {
                width: table.nameW
                text: translate.tr("Armor:")
            }
            Text {
                text: unitEditor.armor
            }
        }

        Text {
            width: table.width
            wrapMode: Text.WordWrap
            text: translate.tr("Immun: ") + unitEditor.immun
        }
        Text {
            width: table.width
            wrapMode: Text.WordWrap
            text: translate.tr("Protection: ") + unitEditor.protect
        }

        Item {
            height: 15
            width: table.nameW
        }

        Row{
            Text {
                width: table.nameW
                text: translate.tr("Ini:")
            }
            Text {
                text: unitEditor.ini
            }
        }

        Item {
            height: 5
            width: table.nameW
        }

        Row{
            width: table.width
            id: attack1Start
            Text {
                width: table.nameW
                text: translate.tr("Attack:")
            }
            Text {
                width: table.width - table.nameW
                wrapMode: Text.WordWrap
                text: unitEditor.attack1
            }
        }
        Row{
            visible: unitEditor.attack1Source != ""
            width: parent.width
            Text {
                width: table.nameW
                text: translate.tr("Source:")
            }
            Text {
                width: table.width - table.nameW
                wrapMode: Text.WordWrap
                text: unitEditor.attack1Source
            }
        }
        Row{
            visible: unitEditor.attack1Accuracy != ""
            width: parent.width
            Text {
                width: table.nameW
                text: translate.tr("Accuracy:")
            }
            Text {
                width: table.width - table.nameW
                wrapMode: Text.WordWrap
                text: unitEditor.attack1Accuracy
            }
        }
        Row{
            visible: unitEditor.attack1Damage != ""
            width: parent.width
            Text {
                width: table.nameW
                text: unitEditor.attack1DamageName
            }
            Text {
                width: parent.width - table.nameW
                wrapMode: Text.WordWrap
                text: unitEditor.attack1Damage
            }
        }
        Row{
            width: parent.width
            id: attack1End
            Text {
                width: table.nameW
                text: translate.tr("Target:")
            }
            Text {
                width: table.width - table.nameW
                wrapMode: Text.WordWrap
                text: unitEditor.attack1Target
            }
        }

        Item {
            height: 5
            width: table.nameW
        }
        Text {
            visible: unitEditor.attack2 != ""
            width: table.nameW
            verticalAlignment: Text.AlignVCenter
            horizontalAlignment: Text.AlignHCenter
            text: translate.tr("Or:")
        }
        Item {
            height: 5
            width: table.nameW
        }

        Row{
            visible: unitEditor.attack2 != ""
            width: table.width
            id: attack2Start
            Text {
                width: table.nameW
                text: translate.tr("Attack:")
            }
            Text {
                width: table.width - table.nameW
                wrapMode: Text.WordWrap
                text: unitEditor.attack2
            }
        }
        Row{
            visible: unitEditor.attack2 != "" && unitEditor.attack2Source != ""

            width: table.width
            Text {
                width: table.nameW
                text: translate.tr("Source:")
            }
            Text {
                width: table.width - table.nameW
                wrapMode: Text.WordWrap
                text: unitEditor.attack2Source
            }
        }
        Row{
            visible: unitEditor.attack2 != "" && unitEditor.attack2Accuracy != ""
            width: table.width
            Text {
                width: table.nameW
                text: translate.tr("Accuracy:")
            }
            Text {
                width: table.width - table.nameW
                wrapMode: Text.WordWrap
                text: unitEditor.attack2Accuracy
            }
        }
        Row{
            visible: unitEditor.attack2 != "" && unitEditor.attack2Damage != ""
            width: table.width
            Text {
                width: table.nameW
                text: unitEditor.attack2DamageName
            }
            Text {
                width: table.width - table.nameW
                wrapMode: Text.WordWrap
                text: unitEditor.attack2Damage
            }
        }
        Row{
            visible: unitEditor.attack2 != ""
            width: table.width
            id: attack2End
            Text {
                width: table.nameW
                text: translate.tr("Target:")
            }
            Text {
                width: table.width - table.nameW
                wrapMode: Text.WordWrap
                text: unitEditor.attack2Target
            }
        }
    }
    NTRectangle{
        anchors.fill: currentList
        //anchors.topMargin: -30
        Text {
            anchors.topMargin: -30
            anchors.top: parent.top
            anchors.left: parent.left
            anchors.right: parent.right
            text: "Unit mods:"
        }
    }


    ListView{
        id: currentList
        anchors.top: parent.top
        anchors.topMargin: 35
        anchors.bottom: parent.bottom
        anchors.bottomMargin: style.buttonHeight + 10
        anchors.left: table.right
        anchors.leftMargin: 5
        width: 400

        spacing: 3
        clip: true
        orientation: Qt.Vertical

        model: unitEditor.unitModifiers
        delegate:  Item {
            width:  currentList.width
            height: 30

            Rectangle
            {
                color: "gray"
                border.color: "black"
                border.width: 1
                anchors.fill: parent
            }
            Text {
                text: Name
                anchors.fill: parent
            }
            Row{
                spacing: 5
                anchors.right: parent.right
                ImageButton{
                    width: style.iconSize
                    height: style.iconSize
                    source: "image://provider/Delete"
                    onClicked:
                    {
                        unitEditor.removeMod(index)
                    }
                }
            }
        }
    }
    NTRectangle{
        anchors.fill: totalList
        //anchors.topMargin: -30
        Text {
            id: availableText
            anchors.topMargin: -30
            anchors.top: parent.top
            anchors.left: parent.left
            text: "Available mods:"
        }
        FilterAreaWidget {
            id: filterArea
            anchors.top: parent.top
            anchors.topMargin: -40
            height: 60
            anchors.right: parent.right
            anchors.left: availableText.right
            openToRight: false
            tagListHeight: 160
            sortVisible: false
            settingsModel: unitEditor.modifiers.filterSettings
        }
    }

    ListView{
        id: totalList
        anchors.top: parent.top
        anchors.topMargin: 35
        anchors.bottom: parent.bottom
        anchors.bottomMargin: style.buttonHeight + 10
        anchors.left: currentList.right
        anchors.leftMargin: 15
        width: 400

        spacing: 3
        clip: true
        orientation: Qt.Vertical

        model: unitEditor.modifiers
        delegate:  Item {
            width:  currentList.width
            height: style.iconSize
            id: itemM
            Rectangle
            {
                color: "gray"
                border.color: "black"
                border.width: 1
                anchors.fill: parent
                Row{
                    spacing: 5
                    anchors.fill: parent
                    ImageButton{
                        width: style.iconSize
                        height: style.iconSize
                        source: "image://provider/left"
                        onClicked:
                        {
                            unitEditor.addMod(Id, 1)
                        }
                    }
                    ImageButton{
                        width: style.iconSize
                        height: style.iconSize
                        source: "image://provider/left"
                        onClicked:
                        {
                            unitEditor.addMod(Id, 5)
                        }
                    }
                    Text {
                        text: Name
                    }
                }
            }

        }
    }

    Button {
        id: applyButton
        text: qsTr("Apply")
        onClicked: apply()
        z: 1
        anchors.bottom: parent.bottom
        anchors.right: parent.right
        anchors.margins: 10
        anchors.bottomMargin: -2
        anchors.rightMargin: -2
        width: style.buttonWidht
        height: style.buttonHeight
    }
    Button {
        text: qsTr("Cancel")
        onClicked: cancel()
        z: 1
        anchors.bottom: parent.bottom
        anchors.right: applyButton.left
        anchors.margins: 10
        anchors.bottomMargin: -2
        width: style.buttonWidht
        height: style.buttonHeight
    }
}

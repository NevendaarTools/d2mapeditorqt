import QtQuick 2.0
import QMLMapGenerationTool 1.0
import QMLToolsProvider 1.0

Item {
    property ToolsProvider toolsProv
    property MapGenerationTool mapTool
    function init(tool)
    {
        toolsProv = tool;
        mapTool = toolsProv.currentToolObj();
        name.text = mapTool.toolName()
    }
    Text {
        id: name
        height: 24
        horizontalAlignment: TextInput.AlignLeft
        verticalAlignment: TextInput.AlignVCenter
        font.pointSize: 14
        anchors.top: parent.top
        anchors.left: parent.left
        anchors.right: parent.right
        anchors.margins: 5
    }

    ImageButton{
        anchors.top: parent.top
        anchors.right: parent.right
        width: 24
        height: 24
        anchors.margins: 5
        source: "image://provider/Exit"
        onClicked:
        {
            toolsProv.closeTool();
        }
    }

    Rectangle
    {
        id: toolRect
        anchors.fill: parent
        color: "gray"
        border.color: "black"
        border.width: 2
        anchors.margins: 5
        anchors.topMargin: 220
        anchors.bottomMargin: 80
        Text {
            id: areaDesc
            height: 24
            text: mapTool.areaDesc
            horizontalAlignment: TextInput.AlignHCenter
            verticalAlignment: TextInput.AlignVCenter
            font.pointSize: 14
            anchors.top: parent.top
            anchors.left: parent.left
            anchors.right: parent.right
            anchors.margins: 5
        }
        NTTextButton{
            id: learnButton
            height: 25
            width: 85
            text: "learn"
            anchors.centerIn: parent
            onClicked: {
                mapTool.learn();
            }
        }
        NTTextButton{
            height: 25
            width: 85
            text: "fill"
            anchors.top: learnButton.bottom
            anchors.right: learnButton.right
            anchors.topMargin: 10
            onClicked: {
                mapTool.fill();
            }
        }
    }
}

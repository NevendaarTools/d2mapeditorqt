import QtQuick 2.12
import QtQuick.Controls 2.14
import QMLVillageEditor 1.0
import Qt.labs.qmlmodels 1.0
import QtQuick.Layouts 1.15
import NTComponents 1.0
import QMLSelectObjectModel 1.0
import QMLStyleManager 1.0

NTEditorWindow {
    id: item
    StyleManager
    {
        id: style
    }
    Connections {
        target: stack
        function onDataChanged() {
            unitsView.reload()
        }
        function onLoadFinished() {
            selectOrderTargetWidget.visible = false;
        }
    }

    TextField {
        id: nameEdit
        height: 36
        anchors.top: parent.top
        anchors.margins: 5
        font.pointSize: style.fontSize
        anchors.left: leftPanel.right
        color: "black"
        width: 415
        text: stack.name
        horizontalAlignment: TextInput.AlignHCenter
        verticalAlignment: TextInput.AlignVCenter
        selectByMouse: true
        onTextChanged: {stack.name = text}
    }
    Image{
        anchors.top: parent.top
        anchors.margins: 5
        anchors.left: nameEdit.right
        anchors.right: noteEdit.left
        source: "image://provider/Events"
        height: 32
    }

    TextField {
        id: noteEdit
        height: 36
        anchors.top: parent.top
        anchors.margins: 5
        font.pointSize: style.fontSize
        anchors.left: nameEdit.right
        anchors.leftMargin: 36
        anchors.right: ownerLabel.left
        color: "black"
        text: stack.note
        horizontalAlignment: TextInput.AlignHCenter
        verticalAlignment: TextInput.AlignVCenter
        selectByMouse: true
        onTextChanged: {stack.note = text}
    }
    Text {
        id: ownerLabel
        anchors.right: ownerCombo.left
        anchors.margins: 5
        anchors.verticalCenter: nameEdit.verticalCenter
        height: 25
        width: 80
        horizontalAlignment: TextInput.AlignHCenter
        verticalAlignment: TextInput.AlignVCenter
        font.pointSize: style.fontSize
        text: translate.tr("Owner")
    }
    ComboBox {
        id: ownerCombo
        anchors.right: parent.right
        anchors.margins: 5
        anchors.verticalCenter: nameEdit.verticalCenter
        height: 25
        width: 160
        model: stack.ownerModel
        currentIndex: stack.ownerModel.ownerIndex
        textRole: "Name"
        onCurrentIndexChanged: {
            stack.ownerModel.ownerIndex = currentIndex
        }
    }

    NTRectangle{
        id: priorityEdit
        anchors.left: nameEdit.right
        anchors.right: parent.right
        anchors.top: ownerCombo.bottom
        anchors.topMargin: 10
        height: 30
        Row{
            Text {
                height: 30
                width: 80
                horizontalAlignment: TextInput.AlignHCenter
                verticalAlignment: TextInput.AlignVCenter
                font.pointSize: style.fontSize
                text: translate.tr("Priority:")
            }

            NTSpinBoxInt {
                min: 0
                max: 6
                value: stack.priority
                onValueModified: (value) => stack.priority = value
                height: 26
                width: 100
                anchors.margins: 2
            }
            Item{
                width: 35
                height: 30
            }

            NTCheckBox {
                id: ignoreCheckbox
                width: 80
                height: 30
                checked: stack.ignore
                text: qsTr("Ignore AI")
                onClicked: {
                    stack.ignore = checked
                }
            }
        }
    }

    GroupView
    {
        id: unitsView
        parentItem: item
        garrison: stack.stack
        isLeft: true
        anchors.left: leftPanel.right
        anchors.margins: 5
        anchors.top: nameEdit.bottom
        width: 220
        height: 400
    }

    NTRectangle{
        id: iconRect
        width: 180
        height: 180
        anchors.left: unitsView.right
        anchors.leftMargin: 10
        anchors.top: unitsView.top
        Image {
            anchors.horizontalCenter: parent.horizontalCenter
            anchors.verticalCenter: iconVCenter.verticalCenter
            id: icon
            source: "image://provider/" + stack.icon
        }
        Item {
            id: iconVCenter
            anchors.bottom: iconTypeSpin.top
            anchors.bottomMargin: 25
            height: 1
            width: parent.width
        }
        NTSpinBoxInt {
            id: iconTypeSpin
            min: -1
            max: 8
            value: stack.rotation
            onValueModified: value =>
            {
                if (value === 8)
                    iconTypeSpin.value = 1;
                if (value === -1)
                    iconTypeSpin.value = 7;
                stack.rotation = iconTypeSpin.value
            }
            anchors.bottom: parent.bottom
            anchors.left: parent.left
            height: 25
            width: 90
            anchors.margins: 2
            anchors.leftMargin: 35
        }
    }
    NTHeroPuppet {
        id: heroPuppet
        anchors.left: iconRect.left
        anchors.top: iconRect.bottom
        anchors.topMargin: 5
    }

    InventoryWidget {
        id: inventory1Grid
        model: stack.inventory
        presetType: "stack"
        anchors.top: unitsView.bottom
        anchors.left: unitsView.left
        anchors.bottom: parent.bottom
        anchors.margins: 5
        anchors.right: heroPuppet.right
    }
    Item{
        anchors.left: priorityEdit.left
        anchors.bottom: parent.bottom
        anchors.right: priorityEdit.right
        anchors.top: priorityEdit.bottom
        anchors.margins: 0
        anchors.topMargin: 5
        anchors.rightMargin: 0
        anchors.leftMargin: 0
        OrderEditor {
            id: orderRect
            orderModel: stack.orderEditor
            anchors.top: parent.top
            anchors.left: parent.left
            anchors.right: parent.right
            height: 200
            anchors.margins: 0
            onOrderSelected: hasTarget => {
                selectOrderTargetWidget.visible = hasTarget
            }
        }

        SelectOrderTargetWidget {
            anchors.right: parent.right
            anchors.left: parent.left
            anchors.margins: 0
            anchors.top: orderRect.bottom
            height: 400
            id: selectOrderTargetWidget
            target: stack.orderEditor.orderTarget
            visible: false
            onApply: {
                stack.orderEditor.orderTarget = selectOrderTargetWidget.target
            }
        }
    }

    NTLeftPanel
    {
        id: leftPanel
        width: 300
        anchors.top: parent.top
        anchors.left: parent.left
        anchors.bottom: parent.bottom
        anchors.margins: 0
        unitsView.leader: !unitsView.garrison.hasLeader
        spellsVisible: false
        eventsObjectUID: stack.uid
    }
}

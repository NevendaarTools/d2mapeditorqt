import QtQuick 2.12
import QtQuick.Controls 2.14
import QMLVillageEditor 1.0
import Qt.labs.qmlmodels 1.0
import QtQuick.Layouts 1.15
import NTComponents 1.0
import QMLSelectObjectModel 1.0

Rectangle{
    id: selectOrderTargetWidget
    color: "gray"
    border.color: "black"
    border.width: 2
    property alias target: targetsModel.target
    signal apply(string target);

    MouseArea
    {
        anchors.fill: parent
        hoverEnabled: true
    }

    Text{
        anchors.top: parent.top
        anchors.margins: 5
        height: 25
        anchors.left: parent.left
        anchors.right: parent.right
        text: targetsModel.targetName
        horizontalAlignment: TextInput.AlignHCenter
        verticalAlignment: TextInput.AlignVCenter
        font.pointSize: 16
    }

    TextField {
        anchors.top: parent.top
        anchors.topMargin: 35
        height: 30
        anchors.left: parent.left
        anchors.leftMargin: 10
        width: 70
        text: "filter:"
        readOnly: true
        enabled: false
    }

    TextField {
        id: nameFilter
        anchors.top: parent.top
        anchors.topMargin: 35
        height: 30
        anchors.left: parent.left
        anchors.leftMargin: 80
        width: parent.width - 85
        onTextChanged: {targetsModel.filter = text}
    }

    ListView{
        SelectObjectModel{
            id: targetsModel
        }
        ScrollBar.vertical: ScrollBar {
            active: true
        }

        anchors.fill: parent
        anchors.margins: 10
        anchors.topMargin: 75
        anchors.bottomMargin: 30
        spacing: 2
        clip: true
        orientation: Qt.Vertical
        id: listView

        model: targetsModel
        delegate:  Item {
            id: deleg
            width:  listView.width
            height: 25
            Rectangle{
                color: "gray"
                border.color: "black"
                border.width: 2
                anchors.fill: parent
                visible: index == targetsModel.targetIndex
            }
            Text {
                id: desc
                anchors.centerIn: parent
                wrapMode: Text.WordWrap
                text: Name
            }

            MouseArea{
                anchors.fill: parent
                onClicked: {
                    targetsModel.targetIndex = index;
                }
                onDoubleClicked: {
                    targetsModel.targetIndex = index;
                    apply(targetsModel.target);
                    selectOrderTargetWidget.visible = false
                }
            }
        }
    }
    Button {
        id: applyOrderButton
        text: qsTr("Apply")
        height: 20
        anchors.bottom: parent.bottom
        anchors.bottomMargin: 5
        anchors.horizontalCenter: parent.horizontalCenter
        width: 80
        onClicked: {
            apply(targetsModel.target);
            selectOrderTargetWidget.visible = false
        }
    }
    Button {
        text: qsTr("Cancel")
        onClicked: {
            selectOrderTargetWidget.visible = false
        }
        height: 20
        anchors.bottom: parent.bottom
        anchors.bottomMargin: 5
        anchors.right: parent.right
        anchors.rightMargin: 10
        width: 60
    }
}

import QtQuick 2.12
import QtQuick.Controls 2.1
import QMLVillageEditor 1.0
import Qt.labs.qmlmodels 1.0
import QMLPresetController 1.0
import QtQml.Models 2.15

Item{
    property alias model: inventory1Grid.model
    property alias presetType: presets.presetType
    property bool presetVisible: true
    property bool editable: true
    property alias cellW: inventory1Grid.cellWidth
    property alias cellH: inventory1Grid.cellHeight
    id: inventoryItem
    Rectangle
    {
        color: "gray"
        border.color: "black"
        border.width: 1
        anchors.fill: inventory1Grid
        anchors.margins: 0
    }
    PresetPanel {
        id: presets
        anchors.left: parent.left
        anchors.right: parent.right
        anchors.top: parent.top
        height: inventoryItem.presetVisible? 28 : 0
        visible: inventoryItem.presetVisible
        onPresetLoaded: preset => {
            inventory1Grid.model.loadPreset(preset);
        }
        onPresetSaved: name => {
            presets.controller.savePreset(name, inventory1Grid.model.toPreset());
        }
    }

    GridView{
        anchors.fill: parent
        anchors.topMargin: presetVisible? 35 : 0
        id:inventory1Grid
        cellWidth: 90; cellHeight: 104
        model: 0
        clip:true
        Component {
            id: itemDelegate
            Rectangle {
                id: wrapper
                width: inventory1Grid.cellWidth - 2
                height: inventory1Grid.cellHeight - 2
                border.color: "black"
                border.width: 2
                color: "transparent"
                Item
                {
                    id: iconRect
                    anchors.topMargin: Charges > 0? 25 : 5
                    anchors.fill: parent
                    anchors.bottomMargin: 25
                }

                Image{
                    id: icon
                    visible: Id !== ""
                    source: "image://provider/" + Icon
                    anchors.centerIn: iconRect
                    //anchors.top: parent.top
                    //anchors.margins: {5;5;5;5}
                    smooth: true
                    scale: inventory1Grid.cellWidth / 120.0
                    //width: inventory1Grid.cellWidth * 0.8
                    //height: inventory1Grid.cellHeight * 0.8
                    Drag.active: dragArea.drag.active
                    Drag.dragType: Drag.Automatic
                    Drag.mimeData: {
                        "id": Id,
                        "model": inventory1Grid.model,
                        "mode": "MoveItem"
                    }
                    MouseArea {
                        id: dragArea
                        anchors.fill: parent
                        drag.target: icon
                        hoverEnabled: true
                        onPressed: icon.grabToImage(function(result) {
                            parent.Drag.imageSource = result.url
                        })
                        onEntered: {
                            overlay.showPreview(dragArea, "ItemWidget.qml", Id)
                        }
                        onExited: {
                            overlay.hidePreview()
                        }
                    }

                }
                NTSpinBoxInt {
                    width:  70
                    height: 24
                    id: countSpin
                    anchors.bottom: parent.bottom
                    anchors.bottomMargin: 1
                    anchors.left: parent.left
                    anchors.leftMargin: 1
                    image.width: 0
                    value: Count
                    editable: inventoryItem.editable
                    leftArrow.enabled: true
                    min: 1
                    step: 1
                    max: 99
                    backgroundRect.visible: true
                    onValueModified: (value) =>
                    {
                        inventory1Grid.model.setCount(index, value)
                    }
                    onMinimumReached: () =>
                    {
                        // inventory1Grid.model.setCount(index, 0)
                    }
                }

                NTSpinBoxInt {
                    id: chargesSpin
                    width:  84
                    height: 24
                    anchors.top: parent.top
                    anchors.topMargin: 1
                    anchors.horizontalCenter: parent.horizontalCenter
                    image.source: "image://provider/Icons-ABIL0010"
                    backgroundRect.visible: true
                    image.height: 20
                    image.width: 16
                    leftArrow.enabled:  Charges > 1
                    value: Charges
                    visible: Charges > 0
                    step: 1
                    max: 99

                    onValueChanged: (value) =>
                    {
                        inventory1Grid.model.setCharges(index, value)
                    }
                }

                ImageButton{
                    anchors.right: parent.right
                    anchors.bottom: parent.bottom
                    width: 16
                    height: 20
                    source: "image://provider/Delete"
                    visible: icon.visible && inventoryItem.editable
                    onClicked: index => {
                        inventory1Grid.model.setCount(index, 0)
                    }
                }
            }

        }
        delegate: itemDelegate
    }
    DropArea {
        id:dropArea
        anchors.fill: parent
        //anchors.margins: 10
        Rectangle {
            id: dragTargetArea
            anchors.fill: parent
            border.color:  "gold"
            border.width: 3
            visible: parent.containsDrag
            color: "transparent"
        }

        onEntered: {
        }
        onExited: {
        }
        onDropped: (drop) => {
            if (drop.getDataAsString("mode") === "ItemAdd")
            {
                           console.log(drop.getDataAsString("mode"))
                           console.log(drop.getDataAsString("id"))
                inventory1Grid.model.addItem(drop.getDataAsString("id"))
            }
            if (drop.getDataAsString("mode") === "MoveItem")
            {
                //var modelFrom = drop["model"]
                //modelFrom.removeItem(drop.getDataAsString("id"))
                inventory1Grid.model.addItem(drop.getDataAsString("id"))
            }
        }
    }
}

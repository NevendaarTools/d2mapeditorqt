import QtQuick 2.15
import QtQuick.Controls 2.15
import QMLVillageEditor 1.0
import Qt.labs.qmlmodels 1.0
import QtQuick.Layouts 1.15

NTEditorWindow {
    id: item
    cancelButton.visible: false
    applyButton.text: "Ok"
    Text {
        id: nameEdit
        height: 36
        anchors.top: parent.top
        anchors.topMargin: 15
        font.pointSize: 13
        anchors.horizontalCenter: parent.horizontalCenter
        color: "black"
        text: "Map statistics:" + (mapStatisticView ? mapStatisticView.mapName : "")
    }
    NTRectangle{
        anchors.fill: expKilledGrid
        anchors.margins: -5
    }

    GridLayout {
        id: expKilledGrid
        anchors.top: nameEdit.bottom
        anchors.left: parent.left
        anchors.leftMargin: 10
        width: 260
        height: 160
        columns: 3
        rowSpacing: 10
        columnSpacing: 10

        // Заголовки столбцов
        Label {
            text: "Название"
            font.bold: true
        }
        Label {
            text: "Значение"
            font.bold: true
        }
        Label {
            text: "Среднее"
            font.bold: true
        }

        // Строки данных
        Label { text: "Stack exp kill" }
        Label { text: mapStatisticView ? mapStatisticView.expKillStack : "N/A" }
        Label { text: mapStatisticView ? mapStatisticView.expKillStackOverage : "N/A" }

        Label { text: "Ruin exp kill" }
        Label { text: mapStatisticView ? mapStatisticView.expKillRuin : "N/A" }
        Label { text: mapStatisticView ? mapStatisticView.expKillRuinOverage : "N/A" }

        Label { text: "Village exp kill" }
        Label { text: mapStatisticView ? mapStatisticView.expKillVillage : "N/A" }
        Label { text: mapStatisticView ? mapStatisticView.expKillVillageOverage : "N/A" }

        Label { text: "Template exp kill" }
        Label { text: mapStatisticView ? mapStatisticView.expKillTemplate : "N/A" }
        Label { text: mapStatisticView ? mapStatisticView.expKillTemplateOverage : "N/A" }

        Label { text: "Summ exp kill" }
        Label { text: mapStatisticView ? mapStatisticView.expKillStack +
                                         mapStatisticView.expKillRuin +
                                         mapStatisticView.expKillVillage +
                                         mapStatisticView.expKillTemplate: "N/A" }
    }
    NTRectangle{
        anchors.fill: proportionsGrid
        anchors.margins: -5
    }
    GridLayout {
        id: proportionsGrid
        anchors.top: nameEdit.bottom
        anchors.left: expKilledGrid.right
        anchors.leftMargin: 15
        width: 260
        height: 160
        columns: 2
        rowSpacing: 10
        columnSpacing: 10

        // Заголовки столбцов
        Label {
            text: "Type"
            font.bold: true
        }
        Label {
            text: "% of land"
            font.bold: true
        }

        Label { text: "Water" }
            Label { text: mapStatisticView ? mapStatisticView.waterProportion.toFixed(2) + " %": "N/A" }

            Label { text: "Trees" }
            Label { text: mapStatisticView ? mapStatisticView.treeProportion.toFixed(2) + " %" : "N/A" }

            Label { text: "Roads" }
            Label { text: mapStatisticView ? mapStatisticView.roadProportion.toFixed(2) + " %" : "N/A" }

            Label { text: "Unpassable" }
            Label { text: mapStatisticView ? mapStatisticView.unpassableProportion.toFixed(2) + " %" : "N/A" }
    }
}

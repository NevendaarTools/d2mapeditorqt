import QtQuick 2.15
import QtQuick.Layouts 1.2
import QtQuick.Controls 2.0
import QMLMainToolBarController 1.0
import QMLPlaceRuinTool 1.0
import QMLToolsProvider 1.0
import QMLGroupUnitsEditor 1.0

Item {
    id: tool_PlaceStack
    property ToolsProvider toolsProv
    property PlaceRuinTool toolPlaceRuin
    GroupUnitsEditor
    {
        id: unitsEditor
        onUnitsChanged:{
            //unitsView.reload();
        }
    }

    function init(tool)
    {
        toolsProv = tool;
        toolPlaceRuin = toolsProv.currentToolObj();
        name.text = toolPlaceRuin.toolName()
        toolPlaceRuin.stack = unitsEditor
        toolPlaceRuin.init()
        imageSpin.value = toolPlaceRuin.imageIndex
        //unitsView.reload()
        //settingsList.settingsModel = toolPlaceRuin.stackOptions
        //generateStackCheckbox.checked = toolPlaceRuin.generateStack
        //lootSettingsList.settingsModel = toolPlaceRuin.lootOptions
        //generateLootCheckbox.checked = toolPlaceRuin.generateLoot
    }

    Text {
        id: name
        height: 24
        horizontalAlignment: TextInput.AlignLeft
        verticalAlignment: TextInput.AlignVCenter
        font.pointSize: 14
        anchors.top: parent.top
        anchors.left: parent.left
        anchors.right: parent.right
        anchors.margins: 5
    }
    ImageButton{
        anchors.top: parent.top
        anchors.right: parent.right
        width: 24
        height: 24
        anchors.margins: 5
        source: "image://provider/Exit"
        onClicked:
        {
            toolsProv.closeTool();
        }
    }

    Item{
        anchors.horizontalCenter: parent.horizontalCenter
        anchors.top: name.bottom
        width: parent.width - 10
        height: width
        id: iconRect
        anchors.topMargin: 10
        Image {
            anchors.centerIn: parent
            id: icon
            sourceSize: Qt.size(160, 160)
            source: "image://provider/IsoCmon-IsoAnim-" + toolPlaceRuin.imageName(toolPlaceRuin.imageIndex)
        }
    }

    NTSpinBoxInt {
        id: imageSpin
        height: 22
        width: 80
        anchors.top: iconRect.bottom
        min: 0
        max: 99
        backgroundRect.visible: true
        anchors.horizontalCenter: parent.horizontalCenter
        editable: true
        image.width: 0
        anchors.topMargin: -30

        onValueChanged:
        {
            toolPlaceRuin.imageIndex = value
        }
    }
//    Rectangle{
//        id: genStackRect
//        property bool propVisible: false
//        anchors.top: name.bottom
//        anchors.margins: 5
//        anchors.right: parent.right
//        anchors.left: parent.left
//        height: 20
//        ImageButton
//        {
//            height: 20
//            width: 20
//            id: hideStackButton
//            source: "image://provider/Settings"
//            onClicked:
//            {
//                genStackRect.propVisible = !genStackRect.propVisible
//            }
//        }
//        ImageButton
//        {
//            height: 20
//            width: 20
//            anchors.right: parent.right
//            id: regenerateStackButton
//            source: "image://provider/Refresh"
//            onClicked:
//            {
//                toolPlaceRuin.onGenerateStackRequested()
//            }
//        }
//        NTCheckBox {
//            id: generateStackCheckbox
//            anchors.top: parent.top
//            anchors.right: regenerateStackButton.left
//            anchors.left: hideStackButton.right
//            height: 20
//            checked: true
//            text: "Generate stack"
//            onClicked: {
//                toolPlaceRuin.generateStack = checked
//                generateStackCheckbox.checked = toolPlaceRuin.generateStack
//                if (checked)
//                    genStackRect.propVisible = true
//            }
//        }
//    }
//    Item
//    {
//        id: settingsArea
//        anchors.top: genStackRect.bottom
//        anchors.margins: 5
//        anchors.right: parent.right
//        anchors.left: parent.left
//        height: visible ? 400 : 0
//        visible: generateStackCheckbox.checked && genStackRect.propVisible
//        OptionsList {
//            id: settingsList
//            anchors.verticalCenter: parent.verticalCenter
//            width: parent.width
//            height: parent.height / settingsList.scale
//            spacing: 1
//            scale: 0.8
//        }
//    }

//    Item
//    {
//        id: unitsRect
//        anchors.top: settingsArea.bottom
//        anchors.margins: 5
//        anchors.right: parent.right
//        anchors.left: parent.left
//        height: 300
//        GroupView
//        {
//            id: unitsView
//            parentItem: tool_PlaceStack
//            garrison: unitsEditor
//            isLeft: false
//            isEditable: true
//            anchors.centerIn: parent
//            width: 220
//            height: 400
//            scale: 0.7
//        }
//    }
//    Rectangle{
//        id: genLootRect
//        property bool propVisible: false
//        anchors.top: unitsRect.bottom
//        anchors.margins: 5
//        anchors.right: parent.right
//        anchors.left: parent.left
//        height: 20
//        ImageButton
//        {
//            height: 20
//            width: 20
//            id: hideLootButton
//            source: "image://provider/Settings"
//            onClicked:
//            {
//                genLootRect.propVisible = !genLootRect.propVisible
//            }
//        }
//        ImageButton
//        {
//            height: 20
//            width: 20
//            anchors.right: parent.right
//            id: regenerateLootButton
//            source: "image://provider/Refresh"
//            onClicked:
//            {
//                toolPlaceRuin.onGenerateLootRequested()
//            }
//        }
//        NTCheckBox {
//            id: generateLootCheckbox
//            anchors.top: parent.top
//            anchors.right: regenerateLootButton.left
//            anchors.left: hideLootButton.right
//            height: 20
//            checked: true
//            text: "Generate loot"
//            onClicked: {
//                toolPlaceRuin.generateLoot = checked
//                generateLootCheckbox.checked = toolPlaceRuin.generateLoot
//                if (checked)
//                    genLootRect.propVisible = true
//            }
//        }
//    }
//    Item
//    {
//        id: lootSettingsArea
//        anchors.top: genLootRect.bottom
//        anchors.margins: 5
//        anchors.right: parent.right
//        anchors.left: parent.left
//        height: visible ? 400 : 0
//        visible: generateLootCheckbox.checked && genLootRect.propVisible
//        OptionsList {
//            id: lootSettingsList
//            anchors.verticalCenter: parent.verticalCenter
//            width: parent.width
//            height: parent.height / lootSettingsList.scale
//            spacing: 1
//            scale: 0.8
//        }
//    }
}

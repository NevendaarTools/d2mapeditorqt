import QtQuick 2.12
import QtQuick.Controls 2.12
import QtQuick.Layouts 1.12
import QMLStyleManager 1.0

Popup {
    id: menuPopup
    property alias model:actionsList.model
    signal itemClicked(int index)
    property bool requested: false
    padding: 0
    onClosed: requested = false
    onOpened: requested = true

    NTRectangle{
        anchors.fill: actionsList
    }

    ListView {
        id: actionsList
        anchors.fill: parent
        model: 0

        delegate: Item {
            width: actionsList.width
            anchors.horizontalCenter: horizontalCenter.horizontalCenter
            height: style.iconSize + 5
            Rectangle{
                anchors.fill: parent
                id: hoverRect
                color: "lightgray"
                border.color: style.borderColor
                border.width: 1
                visible: false
                opacity: 0.2
                //radius:  0.2 * height
            }
            StyleManager
            {
                id: style
            }
            Row {
                anchors.verticalCenter: parent.verticalCenter
                Image {
                    source: model.icon
                    width: style.iconSize
                    height: style.iconSize
                }
                Text {
                    text: model.label
                    anchors.verticalCenter: parent.verticalCenter
                    font.pointSize: style.titleFontSize
                    horizontalAlignment: Text.AlignHCenter
                }
            }

            MouseArea {
                anchors.fill: parent
                hoverEnabled: true
                onClicked: {
                    itemClicked(index)
                    menuPopup.close();
                }
                onEntered: ()=> {
                    hoverRect.visible = true
                }

                onExited: ()=> {
                    hoverRect.visible = false
                }
            }
        }
    }
}

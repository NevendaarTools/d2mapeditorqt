import QtQuick 2.0
import QMLGroupUnitsEditor 1.0

Rectangle {
    id: groupView
    property GroupUnitsEditor garrison
    property Item parentItem: groupView
    property bool isLeft: false
    property bool isEditable: true
    color: "grey"
    border.color: "black"
    function reload()
    {
        unit_0_0.reload()
        unit_0_0.x = 20 + ((!isLeft)?1 - unit_0_0.unitX:unit_0_0.unitX) * 100
        unit_0_1.reload()
        unit_0_1.x = 20 + ((!isLeft)?1 - unit_0_1.unitX:unit_0_1.unitX) * 100
        unit_0_2.reload()
        unit_0_2.x = 20 + ((!isLeft)?1 - unit_0_2.unitX:unit_0_2.unitX) * 100
        unit_1_0.reload()
        unit_1_0.x = 20 + ((!isLeft)?1 - unit_1_0.unitX:unit_1_0.unitX) * 100
        unit_1_1.reload()
        unit_1_1.x = 20 + ((!isLeft)?1 - unit_1_1.unitX:unit_1_1.unitX) * 100
        unit_1_2.reload()
        unit_1_2.x = 20 + ((!isLeft)?1 - unit_1_2.unitX:unit_1_2.unitX) * 100
        descText.text = translate.tr("Exp for kill:") + garrison.expForKill()
    }

    Text
    {
        id: descText
        anchors.right: parent.right
        anchors.left: parent.left
        anchors.top: parent.top
        anchors.topMargin: 5
        horizontalAlignment: TextInput.AlignHCenter
        verticalAlignment: TextInput.AlignVCenter
        font.pointSize: 12
        height: 20
        text: ""
    }
    UnitView
    {
        id: unit_0_0
        isEditable: groupView.isEditable
        parentItem: groupView.parentItem
        garrison: groupView.garrison
        isLeft: groupView.isLeft
        unitX: 0
        unitY: 0
        x: 20 + ((!isLeft && garrison.isSmall(unitX, unitY))?1 - unitX:unitX) * 100
        y:  50 + unitY * 120
    }
    UnitView
    {
        id: unit_1_0
        isEditable: groupView.isEditable
        parentItem: groupView.parentItem
        garrison:  groupView.garrison
        isLeft:  groupView.isLeft
        unitX: 1
        unitY: 0
        x: 20 + ((!isLeft && garrison.isSmall(unitX, unitY))?1 - unitX:unitX) * 100
        y:  50 + unitY * 120
    }
    UnitView
    {
        isEditable: groupView.isEditable
        id: unit_0_1
        parentItem: groupView.parentItem
        garrison:  groupView.garrison
        isLeft:  groupView.isLeft
        unitX: 0
        unitY: 1
        x: 20 + ((!isLeft && garrison.isSmall(unitX, unitY))?1 - unitX:unitX) * 100
        y:  50 + unitY * 120
    }
    UnitView
    {
        id: unit_1_1
        isEditable: groupView.isEditable
        parentItem: groupView.parentItem
        garrison:  groupView.garrison
        isLeft:  groupView.isLeft
        unitX: 1
        unitY: 1
        x: 20 + ((!isLeft && garrison.isSmall(unitX, unitY))?1 - unitX:unitX) * 100
        y:  50 + unitY * 120
    }
    UnitView
    {
        id: unit_0_2
        isEditable: groupView.isEditable
        parentItem: groupView.parentItem
        garrison:  groupView.garrison
        isLeft:  groupView.isLeft
        unitX: 0
        unitY: 2
        x: 20 + ((!isLeft && garrison.isSmall(unitX, unitY))?1 - unitX:unitX) * 100
        y:  50 + unitY * 120
    }
    UnitView
    {
        id: unit_1_2
        isEditable: groupView.isEditable
        parentItem: groupView.parentItem
        garrison:  groupView.garrison
        isLeft:  groupView.isLeft
        unitX: 1
        unitY: 2
        x: 20 + ((!isLeft && garrison.isSmall(unitX, unitY))?1 - unitX:unitX) * 100
        y:  50 + unitY * 120
    }
}

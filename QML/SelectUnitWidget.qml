import QtQuick 2.14
import QtQuick.Controls 2.14
import QtQuick.Layouts 1.15
import QMLUnitSelectModel 1.0

Item {
    id: widget
    property alias model: unitsModel
    property alias listWidth: itemsList.width
    property alias leader: unitsModel.leader
    signal itemDBLClicked(string item);

    FilterAreaWidget {
        id: filterArea
        anchors.top: parent.top
        height: 58
        width: parent.width
        settingsModel: unitsModel.filterSettings
    }

    Rectangle
    {
        anchors.fill: itemsList
        anchors.margins: -5
        color: "gray"
    }
    Item{
        id: dummy
        width: 2
        anchors.left: itemsList.right
        anchors.top: itemsList.top
    }
    ListView{
        UnitSelectModel{
            id: unitsModel
            leader: false
        }
        ScrollBar.vertical: ScrollBar {
            active: true
        }
        id: itemsList
        anchors.top: filterArea.bottom
        anchors.topMargin: 5
        anchors.bottom: parent.bottom
        anchors.bottomMargin: 5
        anchors.left: parent.left
        anchors.leftMargin: 5
        width: filterArea.width - 10

        spacing: 3
        clip: true
        orientation: Qt.Vertical

        model: unitsModel
        delegate:  Item {
            id: deleg
            width:  itemsList.width
            height: 100

            Rectangle
            {
                color: "gray"
                border.color: "black"
                border.width: 1
                anchors.fill: parent
            }

            Image {
                anchors.left: parent.left
                anchors.leftMargin: 5
                anchors.verticalCenter: parent.verticalCenter
                id: icon
                source: "image://provider/" + Icon
                //width: 84 : 84 * 2 + 10
                sourceSize: Qt.size(84, 84)
                //height: 96
                Drag.active: dragArea.drag.active
                Drag.dragType: Drag.Automatic
                Drag.mimeData: {
                    "id": Id,
                    "level": Level,
                    "mode": "hire"
                }

                MouseArea {
                    id: dragArea
                    anchors.fill: parent
                    drag.target: icon
                    hoverEnabled: true
                    onPressed: icon.grabToImage(function(result) {
                        parent.Drag.imageSource = result.url
                    })
                    onEntered: {
                        overlay.showPreview(dummy, "Preview/Unit.qml", "#"+Id+":"+Level)
                    }
                    onExited: {
                        overlay.hidePreview()
                    }
                    onDoubleClicked: itemDBLClicked(Id)
                }
            }
            Rectangle
            {
                anchors.fill: catText
                color: "gray"
                border.color: "black"
                border.width: 1
            }

            Text {
                id: catText
                text: Category
                anchors.top: parent.top
                anchors.horizontalCenter: icon.horizontalCenter
            }
            Column{
                //anchors.left: icon.right
                //anchors.right: parent.right
                x: 100
                width: parent.width - 100
                anchors.top: parent.top
                anchors.topMargin: 5
                height: 90
                spacing: 2
                Text {
                    id: name
                    text: Name
                    font.bold: unitsModel.filterSettings.sortMode == 0
                }
                Text {
                    text: "Level: " + Level
                }
                Text {
                    text: "HitPoints: " + Hp
                }
                Text {
                    text: "Exp: " + Exp
                    font.bold: unitsModel.filterSettings.sortMode == 1
                }
                Text {
                    text: "Exp kill: " + ExpKill
                    font.bold: unitsModel.filterSettings.sortMode == 2
                }
            }
        }
    }
}

import QtQuick
import QMLItemView 1.0
import QMLCostEditor 1.0
import QtQuick.Controls 2.14
import QtQuick.Layouts 1.15
import QtQuick.Controls.Material

Item {
    id: root
    property alias imageSource: valueIcon.source
    property alias image: valueIcon
    property bool editable: true
    property int value: 0
    property int min: 0
    property int max: 9999
    property int step: 1
    property alias backgroundRect: background
    property alias leftArrow: leftButton
    property alias rightArrow: rightButton

    signal minimumReached()
    signal valueModified(int value)

    onValueChanged: ()=> {
        if (valueCount.text !== (value + "")) {
            valueCount.text = value
            valueModified(value)
        }
        if (root.value == min) {
            minimumReached();
        }
    }

    Rectangle{
        anchors.fill: parent
        color: "white"
        border.color: "black"
        border.width: 2
        id: background
        visible: false
    }

    Image {
        anchors.left: parent.left
        anchors.leftMargin: 0
        anchors.verticalCenter: parent.verticalCenter
        id: valueIcon

        width: parent.height
        height: parent.height
    }

    TextInput  {
        anchors.left: valueIcon.right
        anchors.right: parent.right
        anchors.top: parent.top
        anchors.bottom: parent.bottom
        anchors.margins: 0
        // anchors.rightMargin: 12
        // anchors.leftMargin: 12
        horizontalAlignment: TextInput.AlignHCenter
        verticalAlignment: TextInput.AlignVCenter
        id: valueCount
        //visible: editable
        text: root.value.toString()
        // topInset: 0
        // bottomInset: 0
        selectByMouse: true
        //font.pointSize: 8
        validator: IntValidator {bottom: root.min; top: root.max;}
        //inputMethodHints: Qt.ImhNone
        onTextEdited: () =>
        {
            var newValue = parseInt(text)
            if (newValue !== root.value) {
                root.value = newValue
            }
        }

        MouseArea {
            anchors.fill: parent
            propagateComposedEvents: true
            preventStealing: true
            acceptedButtons: Qt.NoButton
            onWheel: wheel => {
                var newValue = root.value
                if (wheel.angleDelta.y > 0) {
                    newValue = Math.min(root.value + step, root.max)
                } else {
                    newValue = Math.max(root.value - step, root.min)
                }
                if (newValue !== root.value) {
                    root.value = newValue
                }
                wheel.accepted = false
            }
        }
    }

    ImageButton{
        id: leftButton
        visible: editable
        enabled: root.value > min
        width: 21
        height: parent.height
        anchors.top: parent.top
        anchors.left: valueIcon.right
        source: "image://provider/left"
        border.visible: false
        onClicked: {
            root.value = Math.max(root.value - step, root.min)
        }
    }

    Rectangle{
        anchors.fill: leftButton
        color: "black"
        border.color: "transparent"
        opacity: 0.5
        visible: !leftButton.enabled && leftButton.visible
    }

    ImageButton{
        id: rightButton
        enabled: root.value < max
        visible: editable
        width: 21
        height: parent.height
        anchors.top: parent.top
        anchors.right: parent.right
        source: "image://provider/right"
        border.visible: false
        onClicked: {
            root.value = Math.min(root.value + step, root.max)
        }
    }

    Rectangle{
        anchors.fill: rightButton
        color: "black"
        border.color: "transparent"
        opacity: 0.5
        visible: !rightButton.enabled && rightButton.visible
    }
}

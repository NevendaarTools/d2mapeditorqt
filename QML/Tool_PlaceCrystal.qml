import QtQuick 2.15
import QtQuick.Layouts 1.2
import QtQuick.Controls 2.0
import QMLMainToolBarController 1.0
import QMLPlaceCrystalTool 1.0
import QMLToolsProvider 1.0

Item {
    id: tool_PlaceLMark
    property ToolsProvider toolsProv
    property PlaceCrystalTool toolPlaceCrystal

    function init(tool)
    {
        toolsProv = tool;
        toolPlaceCrystal = toolsProv.currentToolObj();
        name.text = toolPlaceCrystal.toolName()
        crystalView.model = toolPlaceCrystal.crystalsModel
        rodView.model = toolPlaceCrystal.rodsModel
    }

    Connections {
        target: toolPlaceCrystal

    }
    Text {
        id: name
        height: 24
        horizontalAlignment: TextInput.AlignLeft
        verticalAlignment: TextInput.AlignVCenter
        font.pointSize: 14
        anchors.top: parent.top
        anchors.left: parent.left
        anchors.right: parent.right
        anchors.margins: 5
    }
    ImageButton{
        anchors.top: parent.top
        anchors.right: parent.right
        width: 24
        height: 24
        anchors.margins: 5
        source: "image://provider/Exit"
        onClicked:
        {
            toolsProv.closeTool();
        }
    }

    GridView {
        id: crystalView
        y: 45
        anchors.left: parent.left
        anchors.right: parent.right
        anchors.margins: 5
        height: 140
        cellWidth: 60; cellHeight: 60
        model: 0
        clip: true
        delegate: Rectangle {

            width: 58
            height: 58
            Image {
                //width: 58
                //height: 58
                //anchors.verticalCenter: parent.bottom
                anchors.horizontalCenter: parent.horizontalCenter
                anchors.topMargin: 15
                anchors.top: parent.top
                height: 58
                sourceSize: Qt.size(58, 58)
                source: "image://provider/IsoCmon-IsoAnim-" + Name
            }
            Rectangle
            {
                color: "transparent"
                border.color: "green"
                border.width: 2
                visible: index === toolPlaceCrystal.selectedIndex
                anchors.fill: parent
            }
            MouseArea{
                anchors.fill: parent
                onClicked: toolPlaceCrystal.selectedIndex = index
            }
        }
    }
    GridView {
        id: rodView
        anchors.top: crystalView.bottom
        anchors.left: parent.left
        anchors.right: parent.right
        anchors.margins: 5
        height: 140
        cellWidth: 60; cellHeight: 60
        model: 0
        clip: true
        delegate: Rectangle {

            width: 58
            height: 58
            Image {
                //width: 58
                //height: 58
                //anchors.verticalCenter: parent.bottom
                anchors.horizontalCenter: parent.horizontalCenter
                anchors.topMargin: 15
                anchors.top: parent.top
                height: 58
                sourceSize: Qt.size(58, 58)
                source: "image://provider/IsoCmon-" + Name
            }
            Rectangle
            {
                color: "transparent"
                border.color: "green"
                border.width: 2
                visible: index + 100 === toolPlaceCrystal.selectedIndex
                anchors.fill: parent
            }
            MouseArea{
                anchors.fill: parent
                onClicked: toolPlaceCrystal.selectedIndex = index + 100
            }
        }
    }
}

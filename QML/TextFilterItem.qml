import QtQuick 2.15
import QtQuick.Layouts 1.2
import QtQuick.Controls 2.0
import QMLMainToolBarController 1.0
import QMLPlaceLMarkTool 1.0
import QMLToolsProvider 1.0

Item {
    id: textFilter
    property alias model: inst.model
    property alias text: nameFilter.text
    property alias sortVisible: sortButton.visible
    signal filterChanged();
    signal sortModeChanged(var mode);


    TextField {
        id: nameFilter
        anchors.top: parent.top
        height: parent.height
        anchors.left: parent.left
        anchors.leftMargin: 3//parent.height + 5
        width: sortVisible?(textFilter.width - 10 - parent.height): (textFilter.width - 5)
        selectByMouse: true
        onTextChanged: {textFilter.filterChanged();}
        verticalAlignment: TextInput.AlignVCenter
    }
    Image {
        source: "image://provider/Search"
        anchors.top: parent.top
        height: parent.height
        width: parent.height
        anchors.right: nameFilter.right
        anchors.rightMargin: 1
        opacity: 0.6
        scale: 0.85
        visible: nameFilter.text === ""
    }
    ImageButton{
        id: sortButton
        anchors.top: parent.top
        height: parent.height
        width: parent.height
        anchors.left: nameFilter.right
        anchors.leftMargin: 5
        source: "image://provider/Sort"
        onClicked: {
            menu.open();
        }
        Menu {
            //anchors.centerIn: parent
            id: menu
            Instantiator {
                id: inst
                model: 0
                MenuItem {
                    text: modelData
                    onClicked: {
                        sortModeChanged(index);
                    }
                }

                onObjectAdded: (index, object) => menu.insertItem(index, object)
                onObjectRemoved: (index, object) => menu.removeItem(object)
            }
        }

    }
}

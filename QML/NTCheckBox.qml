import QtQuick 2.0
import QMLStyleManager 1.0

Item {
    id: root
    StyleManager
    {
        id: style
    }
    property string text:    'text'
    property bool   checked: false
    property alias checkFont: checkBox.font

    signal clicked(bool checked, var mouse);   //onClicked:{root.checked = checked;  print('onClicked', checked)}

    property real padding: 0.1    // around rectangle: percent of root.height

    width: 160;  height: 32                         // default size
    opacity: enabled  &&  !mouseArea.pressed? 1: 0.3 // disabled/pressed state

    Rectangle { // check box (or circle for radio button)
        id: rectangle

        height: root.height * (1 - 2 * padding);
        width: height
        x: padding * root.height
        anchors.verticalCenter: parent.verticalCenter
        border.width: 0.05 * root.height
        radius:  0.2 * height

        Text { // check
            id: checkBox
            visible: checked
            anchors.centerIn: parent
            text: '\u2713' // CHECK MARK
            font.pointSize: style.fontSize
        }
    }

    Text {
        id: text

        text: root.text
        anchors {left: rectangle.right;  verticalCenter: rectangle.verticalCenter;  margins: padding * root.height; right:root.right}
        font.pointSize: style.fontSize
        wrapMode: Text.WordWrap
    }

    MouseArea {
        id: mouseArea
        acceptedButtons: Qt.LeftButton | Qt.RightButton
        anchors.fill: parent

        onClicked: (mouse) => root.clicked(!checked, mouse)
    }
}

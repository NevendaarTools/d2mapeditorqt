import QtQuick 2.15
import QtQuick.Layouts 1.2
import QtQuick.Controls 2.0
import QMLMainToolBarController 1.0
import QMLPlaceCapitalTool 1.0
import QMLToolsProvider 1.0

Item {
    id: tool_PlaceCapital
    property ToolsProvider toolsProv
    property PlaceCapitalTool toolPlaceCapital

    function init(tool)
    {
        toolsProv = tool;
        toolPlaceCapital = toolsProv.currentToolObj();
        name.text = toolPlaceCapital.toolName()
        listView.model = toolPlaceCapital.model
    }

    Connections {
        target: toolPlaceCapital

    }
    Text {
        id: name
        height: 24
        horizontalAlignment: TextInput.AlignLeft
        verticalAlignment: TextInput.AlignVCenter
        font.pointSize: 14
        anchors.top: parent.top
        anchors.left: parent.left
        anchors.right: parent.right
        anchors.margins: 5
    }
    ImageButton{
        anchors.top: parent.top
        anchors.right: parent.right
        width: 24
        height: 24
        anchors.margins: 5
        source: "image://provider/Exit"
        onClicked:
        {
            toolsProv.closeTool();
        }
    }

    ListView {
        id: listView
        y: 100
        x: 5
        width: parent.width; height: parent.height -  y
        model: 0
        clip: true
        delegate: Rectangle {

            width: listView.width - 10
            height: listView.width - 10
            Image {
                //width: 58
                //height: 58
                anchors.centerIn: parent
                sourceSize: Qt.size(listView.width - 10, listView.width - 10)
                source: "image://provider/IsoCmon-IsoAnim-" + Id
            }
            Rectangle
            {
                color: "transparent"
                border.color: "green"
                border.width: 2
                visible: index === toolPlaceCapital.model.targetIndex
                anchors.fill: parent
            }
            Rectangle
            {
                color: "green"
                border.color: "green"
                border.width: 2
                visible: (toolPlaceCapital.existFlag && toolPlaceCapital.exist(Id))
                anchors.fill: parent
                opacity: 0.2
            }
            MouseArea{
                anchors.fill: parent
                onClicked: toolPlaceCapital.model.targetIndex = index
                onDoubleClicked: toolPlaceCapital.editCapital(Id)
            }
        }
    }
}

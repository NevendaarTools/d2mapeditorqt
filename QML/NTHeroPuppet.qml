import QtQuick 2.12
import QtQuick.Controls 2.14
import QMLVillageEditor 1.0
import Qt.labs.qmlmodels 1.0
import QtQuick.Layouts 1.15
import NTComponents 1.0
import QMLSelectObjectModel 1.0
import QMLStyleManager 1.0

Item {
    id: nTHeroPuppet
    height: 215
    width: 180
    Image{
        anchors.fill: parent
        source: "image://provider/Puppet"
    }
    Rectangle {
        color: "transparent"
        border.color: "green"
        x: 24
        y: 24
        width: 50
        height: 50
    }
    Rectangle {
        color: "transparent"
        border.color: "green"
        x: 104
        y: 24
        width: 50
        height: 50
    }
    Rectangle {
        color: "transparent"
        border.color: "green"
        x: 3
        y: 87
        width: 50
        height: 50
    }
    Rectangle {
        color: "transparent"
        border.color: "green"
        x: 125
        y: 87
        width: 50
        height: 50
    }
    Rectangle {
        color: "transparent"
        border.color: "green"
        x: 3
        y: 148
        width: 50
        height: 50
    }
    Rectangle {
        color: "transparent"
        border.color: "green"
        x: 66
        y: 152
        width: 50
        height: 50
    }
    Rectangle {
        color: "transparent"
        border.color: "green"
        x: 125
        y: 148
        width: 50
        height: 50
    }
}

import QtQuick 2.14
import QtQuick.Controls 2.14
import QtQuick.Layouts 1.15
import QMLItemSelectModel 1.0

Item {
    signal itemDBLClicked(string item);
    property alias listWidth: itemsList.width
    FilterAreaWidget {
        id: filterArea
        anchors.top: parent.top
        height: 58
        width: parent.width
        settingsModel: itemsModel.filterSettings
    }

    Rectangle
    {
        anchors.fill: itemsList
        anchors.margins: -5
        color: "gray"
    }

    ListView{
        ItemSelectModel{
            id: itemsModel
        }
        ScrollBar.vertical: ScrollBar {
            active: true
        }
        id: itemsList
        anchors.top: filterArea.bottom
        anchors.topMargin: 5
        anchors.bottom: parent.bottom
        anchors.bottomMargin: 5
        anchors.left: parent.left
        anchors.leftMargin: 5
        width: filterArea.width - 10
        snapMode: ListView.SnapOneItem

        spacing: 2
        clip: true
        orientation: Qt.Vertical

        model: itemsModel
        delegate:  Item {
            id: deleg
            width:  itemsList.width
            height: 105

//            MouseArea
//            {
//                anchors.fill: parent
//                onClicked:
//                {
//                    unitEditor.unitId = Id
//                    itemsList.currentIndex = index
//                }
//            }
            Drag.active: dragArea.drag.active
            Drag.dragType: Drag.Automatic
            Drag.mimeData: {
                "id": Id,
                "mode": "ItemAdd"
            }
            MouseArea {
                id: dragArea
                anchors.top: parent.top
                anchors.bottom: parent.bottom
                anchors.left: parent.left
                width: parent.width/2
                drag.target: icon
                onPressed: icon.grabToImage(function(result) {
                    parent.Drag.imageSource = result.url
                })
                onDoubleClicked: itemDBLClicked(Id)
                hoverEnabled: true
                onEntered: {
                    overlay.showPreview(dragArea, "ItemWidget.qml", Id)
                }
                onExited: {
                    overlay.hidePreview()
                }
            }
            Rectangle
            {
                color: "grey"
                border.color: "black"
                border.width: 1
                anchors.fill: parent
            }

            Image {
                anchors.left: parent.left
                //anchors.top: parent.top
                anchors.leftMargin: 5
                anchors.verticalCenter: parent.verticalCenter
                id: icon
                source: "image://provider/" + Icon

            }
            Column{
                x: 70
                width: parent.width - 70
                anchors.top: parent.top
                anchors.topMargin: 2
                height: 90
                spacing: 2
                Text {
                    id: name
                    text: Name
                    font.bold: itemsModel.filterSettings.sortMode == 0
                }
                Text {
                    textFormat: Text.RichText
                    id: desc
                    width: parent.width
                    wrapMode: Text.WordWrap
                    text: Desc
                }

            }
            NTCostView {
                id: costView
                width: costView.model.rowCount() * (costView.editable?112:48)
                anchors.horizontalCenter: parent.horizontalCenter
                anchors.bottom: parent.bottom
                anchors.margins: 2
                value: Cost
            }
        }
    }
}

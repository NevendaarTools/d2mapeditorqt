import QtQuick 2.12
import QtQuick.Controls 2.14
import QMLVillageEditor 1.0
import Qt.labs.qmlmodels 1.0
import QtQuick.Layouts 1.15
import NevendaarTools 1.0
import QMLStyleManager 1.0

NTEditorWindow {
    id: item
    StyleManager
    {
        id: style
    }
    RowLayout{
        id: nameRow
        anchors.top: parent.top
        anchors.left: parent.left
        anchors.right: parent.right
        anchors.margins: 5
        height: 40
        Text {
            text: translate.tr("Map name:")
            font.pointSize: style.fontSize
        }
        TextField {
            id: mapNameEdit
            Layout.preferredWidth: 460
            height: 36
            font.pointSize: style.fontSize
            color: "black"
            text: mapInfoEditor.name
            horizontalAlignment: TextInput.AlignHCenter
            selectByMouse: true
            onTextChanged: {mapInfoEditor.name = text}
        }
        Text {
            text: translate.tr("Author:")
            font.pointSize: style.fontSize
        }
        TextField {
            id: authorEdit
            height: 36
            Layout.preferredWidth: 260
            font.pointSize: style.fontSize
            color: "black"
            text: mapInfoEditor.author
            horizontalAlignment: TextInput.AlignHCenter
            selectByMouse: true
            onTextChanged: {mapInfoEditor.author = text}
        }
    }
    Text {
        id: descLabel
        anchors.left: parent.left
        anchors.top: nameRow.bottom
        anchors.leftMargin: 5
        anchors.topMargin: 5
        text: translate.tr("Description:")
        font.pointSize: style.fontSize
    }
    NTSpellCheckedTextEdit{
        id: descEdit
        anchors.top: descLabel.bottom
        anchors.left: descLabel.left
        anchors.topMargin: 5
        width: 440
        wrapMode: TextEdit.Wrap
        height: 120
        text: mapInfoEditor.desc
        onTextChanged: {mapInfoEditor.desc = text}
    }

    Text {
        id: storyLabel
        anchors.top: descEdit.bottom
        anchors.left: descEdit.left
        anchors.topMargin: 5
        text: translate.tr("Story:")
        font.pointSize: style.fontSize
    }
    NTSpellCheckedTextEdit {
        id: storyEdit
        width: 440
        anchors.top: storyLabel.bottom
        anchors.left: storyLabel.left
        anchors.bottom: parent.bottom
        anchors.topMargin: 5
        anchors.bottomMargin: 60
        wrapMode: TextEdit.Wrap
        text: mapInfoEditor.story
        onTextChanged: {mapInfoEditor.story = text}
    }

    Text {
        id: targetLabel
        anchors.left: storyEdit.right
        anchors.top: nameRow.bottom
        anchors.margins: 5
        anchors.leftMargin: 10
        text: translate.tr("Target:")
        font.pointSize: style.fontSize
    }
    NTSpellCheckedTextEdit{
        id: targetEdit
        anchors.left: targetLabel.left
        anchors.top: targetLabel.bottom
        anchors.topMargin: 5
        width: 500
        wrapMode: TextEdit.Wrap
        height: 120
        text: mapInfoEditor.target
        onTextChanged: {mapInfoEditor.target = text}
    }

    Text {
        id: winLabel
        anchors.left: storyEdit.right
        anchors.top: targetEdit.bottom
        anchors.margins: 5
        anchors.leftMargin: 10
        text: translate.tr("Win:")
        font.pointSize: style.fontSize
    }
    NTSpellCheckedTextEdit{
        id: winEdit
        anchors.left: winLabel.left
        anchors.top: winLabel.bottom
        anchors.topMargin: 5
        anchors.bottom: parent.bottom
        anchors.bottomMargin: 60
        width: 400
        wrapMode: TextEdit.Wrap
        height: 120
        text: mapInfoEditor.victory
        onTextChanged: {mapInfoEditor.victory = text}
    }
    Text {
        id: looseLabel
        anchors.left: winEdit.right
        anchors.top: targetEdit.bottom
        anchors.margins: 5
        anchors.leftMargin: 10
        text: translate.tr("Loose:")
        font.pointSize: style.fontSize
    }
    NTSpellCheckedTextEdit{
        anchors.left: looseLabel.left
        anchors.top: looseLabel.bottom
        anchors.topMargin: 5
        anchors.bottom: parent.bottom
        anchors.bottomMargin: 60
        width: 400
        wrapMode: TextEdit.Wrap
        height: 120
        text: mapInfoEditor.loose
        onTextChanged: {mapInfoEditor.loose = text}
    }
    NTRectangle{
        anchors.top: targetEdit.top
        anchors.left: targetEdit.right
        anchors.bottom: targetEdit.bottom
        anchors.leftMargin: 5
        anchors.topMargin: -5
        anchors.bottomMargin: -5
        // anchors.right: parent.right
        width: 260
        GridLayout {
            Layout.alignment: Qt.AlignHCenter
            anchors.fill: parent
            anchors.margins: 5
            id: grid
            columns: 2
            rowSpacing: 5
            columnSpacing: 5

            Text {
                text: translate.tr("Spell level:")
                font.pointSize: style.fontSize
            }

            NTSpinBoxInt {
                min: 0
                max: 5
                height: 25
                width: 90
                value: mapInfoEditor ? mapInfoEditor.maxSpell : 0
                onValueModified: (value)=>{mapInfoEditor.maxSpell = value;}
            }
            Text {
                text: translate.tr("Unit level:")
                font.pointSize: style.fontSize
            }
            NTSpinBoxInt {
                min: 0
                max: 10
                height: 25
                width: 90
                value: mapInfoEditor ? mapInfoEditor.maxUnit : 0
                onValueModified: (value)=>{mapInfoEditor.maxUnit = value;}
            }
            Text {
                text: translate.tr("Hero level:")
                font.pointSize: style.fontSize
            }
            NTSpinBoxInt {
                min: 0
                max: 99
                height: 25
                width: 90
                value: mapInfoEditor ? mapInfoEditor.maxHero : 0
                onValueModified:(value)=> {mapInfoEditor.maxHero = value}
            }
            Text {
                text: translate.tr("Initial level:")
                font.pointSize: style.fontSize
            }
            NTSpinBoxInt {
                min: 1
                max: 100
                height: 25
                width: 90
                value: mapInfoEditor ? mapInfoEditor.inititalHero : 0
                onValueModified: (value)=> {mapInfoEditor.inititalHero = value;}
            }
        }
    }
}

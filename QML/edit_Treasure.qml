import QtQuick 2.12
import QtQuick.Controls 2.14
import QMLVillageEditor 1.0
import Qt.labs.qmlmodels 1.0
import QtQuick.Layouts 1.15

NTEditorWindow {
    id: item
    Item{
        id: centralRect
        anchors.left: leftPanel.right
        anchors.right: parent.right
        anchors.top: parent.top
        anchors.topMargin: 40
        height: 180

        Rectangle{
            id: iconRect
            color: "gray"
            border.color: "black"
            border.width: 2
            width: 180
            height: 180
            anchors.centerIn: parent
            Image {
                //anchors.top: parent.top
                //anchors.topMargin: 10
                anchors.centerIn: parent
                sourceSize: Qt.size(80, 80)
                id: icon
                source: (treasure.water) ?
                            "image://provider/IsoCmon-IsoAnim-G000BG00000" + treasure.image :
                            "image://provider/IsoCmon-IsoAnim-G000BG00001" + treasure.image
            }
            NTSpinBoxInt {
                min: 0
                max: treasure.maxImage
                value: treasure.image
                anchors.bottom: parent.bottom
                anchors.left: parent.left
                height: 25
                width: 90
                anchors.margins: 2
                anchors.leftMargin: 35
                onValueChanged:
                {
                    treasure.image = value
                }
            }
        }
    }
    NTRectangle{
        anchors.horizontalCenter: centralRect.horizontalCenter
        width: 180
        height: 30
        anchors.top: centralRect.bottom
        anchors.topMargin: 5
        Row{
            Text {
                height: 30
                width: 80
                horizontalAlignment: TextInput.AlignHCenter
                verticalAlignment: TextInput.AlignVCenter
                font.pointSize: style.fontSize
                text: translate.tr("Priority:")
            }
            NTSpinBoxInt {
                min: 0
                max: 6
                value: treasure.priority
                onValueChanged: treasure.priority = value
                height: 26
                width: 100
                anchors.margins: 2
            }
        }
    }
    InventoryWidget {
        id: goodsGrid
        presetType: "inventory"
        model: treasure.items
        anchors.top: centralRect.bottom
        anchors.horizontalCenter: centralRect.horizontalCenter
        anchors.topMargin: 45
        width: 600
        anchors.bottom: parent.bottom
        anchors.bottomMargin: 45
    }
    SelectItemWidget
    {
        id: leftPanel
        width: 275
        anchors.top: parent.top
        anchors.topMargin: 0
        anchors.bottom: parent.bottom
        anchors.left: parent.left
        onItemDBLClicked: {
            treasure.items.addItem(item)
        }
    }
}

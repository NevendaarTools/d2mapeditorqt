import QtQuick 2.0
import QMLUnitEditor 1.0
import QtQuick.Controls 2.15
import QMLMapCreationController 1.0
import QMLStyleManager 1.0

Dialog {
    visible: true
    width: style.editorWidht
    height: style.editorHeight
    focus:true
    id: dialog
    anchors.centerIn: parent
    MapCreationController{
        id: controller
    }

    function cancel()
    {
        console.log("cancel")
        dialog.close()
    }
    function apply()
    {
        console.log("apply")
        controller.createMap()
        dialog.close()
    }

    StyleManager
    {
        id: style
    }
    NTRectangle {
        id: background
        anchors.fill: parent
        anchors.margins: -12
    }
    Row{
        anchors.top: parent.top
        anchors.right: parent.right
        anchors.left: parent.left
        height: 64
        spacing: 15
        Text{
            text: "Map name:"
            verticalAlignment: TextInput.AlignVCenter
            height: mapNameEdit.height
            anchors.verticalCenter: parent.verticalCenter
        }
        TextField {
            id: mapNameEdit
            font.pointSize: style.fontSize
            color: "black"
            width: 360
            text: controller.mapName
            horizontalAlignment: TextInput.AlignHCenter
            selectByMouse: true
            onTextChanged: {controller.mapName = text}
            anchors.verticalCenter: parent.verticalCenter
        }
        ListView{
            id: raceSelectList
            anchors.verticalCenter: parent.verticalCenter
            height: 64
            width: controller.raceCount * (40 + raceSelectList.spacing)
            spacing: 8
            model: controller.raceCount
            orientation: ListView.Horizontal
            delegate: Item {
                width: 40
                height: 64
                enabled: controller.selectedCount < 4 || controller.raceSelected(index)
                Rectangle{
                    id: selectedRect
                    anchors.margins: -2
                    anchors.fill: parent
                    color: "darkgreen"
                    visible: controller.selectedCount > 0 && controller.raceSelected(index)
                }

                NTImageButton {
                    source: "image://provider/" + controller.raceImage(index)
                    width: 40
                    height: 64
                    anchors.centerIn: parent
                    onClicked: controller.selectRace(index)
                }
                Rectangle{
                    id: unavailableRect
                    anchors.fill: parent
                    color: "gray"
                    opacity: 0.4
                    visible: !parent.enabled
                }
            }
        }
        ListView {
            height: 45
            orientation: ListView.Horizontal
            anchors.verticalCenter: parent.verticalCenter
            width: (style.buttonWidht + 5) * 5
            spacing: 5
            model: ListModel {
                ListElement { text: "48"; value: 48 }
                ListElement { text: "72"; value: 72 }
                ListElement { text: "96"; value: 96 }
                ListElement { text: "120"; value: 120 }
                ListElement { text: "144"; value: 144 }
            }
            delegate:  NTTextButton{
                width: style.buttonWidht
                height: style.buttonHeight * 1.4
                text: model.text
                font.pointSize: 14
                onClicked: controller.mapSize = model.value
                border.visible: controller.mapSize === model.value
                border.width: 3
            }
        }
    }
    NTRectangle{
        anchors.fill: parent
        anchors.margins: 10
        anchors.topMargin: 80
        anchors.bottomMargin: 40
        Text {
            anchors.centerIn: parent
            text: "Generator settings"
        }
    }

    Button {
        id: applyButton
        enabled: controller.selectedCount > 0 && controller.mapName !== ""
        text: qsTr("Apply")
        onClicked: apply()
        z: 1
        anchors.bottom: parent.bottom
        anchors.right: parent.right
        anchors.margins: 10
        anchors.bottomMargin: -2
        anchors.rightMargin: -2
        width: style.buttonWidht
        height: style.buttonHeight
    }
    Button {
        text: qsTr("Cancel")
        onClicked: cancel()
        z: 1
        anchors.bottom: parent.bottom
        anchors.right: applyButton.left
        anchors.margins: 10
        anchors.bottomMargin: -2
        width: style.buttonWidht
        height: style.buttonHeight
    }
}

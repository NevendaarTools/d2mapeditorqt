import QtQuick 2.15
import QtQuick.Controls 2.15
import QMLStyleManager 1.0
import QMLSceneVariablesEditor 1.0

Dialog {
    visible: true
    width: style.editorWidht
    height: style.editorHeight
    property bool acceptChanges : false
    id: item
    StyleManager
    {
        id: style
    }
    SceneVariablesEditor{
        id: editor
    }
    onClosed: {
        console.log("Dialog is closing by other means");
        console.log("Accepted "  + acceptChanges);
        if (acceptChanges)
        {
            editor.save()
        }
        else
        {
            editor.cancel();
        }
    }

    function cancel()
    {
        acceptChanges = false;
        close()
    }
    function apply()
    {
        acceptChanges = true;
        close()
    }

    function init()
    {
        editor.init()
    }

    Component {
        id: variableDelegate
        NTRectangle {
            width: listViewQuestLinebase.width
            height: 30

            TextField {
                id: varName
                anchors.left: parent.left
                anchors.verticalCenter: parent.verticalCenter
                anchors.margins: 5
                width: 240
                text: Name
                font.pointSize: style.fontSize - 3
                color: "black"
                horizontalAlignment: TextInput.AlignHCenter
                selectByMouse: true
                onTextChanged: {
                    editor.setName(Id, text)
                }
                height: parent.height - 4
            }
            NTSpinBoxInt {
                id: valueBox
                anchors.left: varName.right
                height: 25
                anchors.verticalCenter: parent.verticalCenter
                anchors.margins: 5
                step: 1
                min: -999
                max: 999
                width: 130
                value: editor.value(Id)
                editable: true
                onValueChanged: (value)=>{
                    editor.setValue(Id, 1.0 * text)
                }
            }
            Text {
                id: liakedQuest
                anchors.left: valueBox.right
                anchors.verticalCenter: parent.verticalCenter
                anchors.margins: 5
                width: 240
                text: editor.desc(Id)
            }
        }
    }

    NTRectangle {
        id: background
        anchors.fill: parent
        anchors.margins: -6
        Text {
            id: text
            height: 32
            anchors.left: parent.left
            anchors.top: parent.top
            width: 200
            anchors.margins: 5
            horizontalAlignment: TextInput.AlignHCenter
            verticalAlignment: TextInput.AlignVCenter
            font.pointSize: style.titleFontSize
            text: translate.tr("Variables:")
        }
        ImageButton{
            NTTextInputDialog {
                id: nameDialog
                width: 240
                height: 130
                title: translate.tr("Variable name:")
                onTextAccepted: (value)=> {
                    var uid = editor.createVariable(value, 0)
                    item.focus = true
                    item.forceActiveFocus()
                }
                onRejected: ()=> {
                    item.focus = true
                    item.forceActiveFocus()
                }
            }
            height: 32
            width: 32
            anchors.left: text.right
            anchors.top: parent.top
            anchors.margins: 5
            source: "image://provider/Add"
            onClicked: {
                nameDialog.text = ""
                nameDialog.open()
            }
        }
        ListView {
            id: listViewQuestLinebase
            anchors.fill: parent
            anchors.margins: 5
            anchors.topMargin: 40
            //interactive: false
            //maximumFlickVelocity : 0
            spacing: 5
            model: editor.variables
            clip: true
            delegate: variableDelegate
        }
    }
    Button {
        id: applyButton
        text: qsTr("Apply")
        onClicked: apply()
        z: 1
        anchors.bottom: parent.bottom
        anchors.right: parent.right
        anchors.margins: 4
        width: style.buttonWidht
        height: style.buttonHeight
    }
    Button {
        text: qsTr("Cancel")
        onClicked: cancel()
        z: 1
        anchors.bottom: applyButton.bottom
        anchors.right: applyButton.left
        anchors.rightMargin: 10
        width: style.buttonWidht
        height: style.buttonHeight
    }
}

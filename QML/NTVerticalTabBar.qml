import QtQuick 2.12
import QtQuick.Controls 2.12
import QMLStyleManager 1.0

Rectangle {
    id: tabBar
    StyleManager
    {
        id: style
    }
    property int currentIndex: 0
    property alias model: listView.model

    signal tabSelected(int index)
//    color: "#f0f0f0"
    color: "transparent"

    ListView {
        id: listView
        anchors.fill: parent
        orientation: ListView.Vertical

        delegate: Rectangle {
            id: delegateItem
            width: tabBar.width
            height: style.buttonHeight
            color: index === tabBar.currentIndex ? "#dedede" : "#f0f0f0"

            Text {
                text: modelData
                font.pointSize: style.fontSize
                anchors.centerIn: parent
            }

            MouseArea {
                anchors.fill: parent
                onClicked: {
                    tabBar.currentIndex = index
                    tabBar.tabSelected(index)
                }
            }
        }
    }
}

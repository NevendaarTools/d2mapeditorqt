import QtQuick 2.3
import QtQuick.Controls
import QtQuick.Layouts 1.1
import QtQuick.Window 2.0

Item {
    id: button

    signal clicked
    signal pressed
    signal released
    signal entered
    signal exited
    signal londTapped
    property alias hoverEnabled: mouseArea.hoverEnabled
    property alias acceptedButtons: mouseArea.acceptedButtons
    property alias source: sprite.source
    property alias im: sprite
    property alias border: borderRect

    Rectangle{
        anchors.fill: parent
        id: borderRect
        //color: Qt.transparent
        color: "#00FFFFFF"
        //border.color: "#299132"
        border.width: 1
        //radius:  0.2 * height
    }

    Rectangle {
        id:rect
        color: "#0d0d0d"
        border.color: "#299132"
        opacity: 0.15
        anchors.fill: parent
        //radius: 15
    }
    Rectangle {
        id:selecredRect
        color: "#299132"
        opacity: 0
        anchors.fill: parent
        //radius: 15
    }

    //width: sprite.width
    //height: sprite.height

    Image {
        anchors.fill: parent
        id: sprite
        source: "image://provider/file"
        rotation: 0
    }

    MouseArea {
        id: mouseArea
        enabled: button.enabled
        anchors.fill: button
        hoverEnabled: true

        //onClicked: button.pressed()
        onPressed: button.pressed()
        onReleased: button.released()
        onEntered: button.entered()
        onExited: button.exited()
    }

    Timer {
        id:timer
        interval: 500
        onTriggered: {
            button.londTapped();
        }
    }

    onLondTapped: {
        //console.log("taapped");
    }

    onClicked: {
        //console.log("clicked");
    }

    onPressed: {
        opacity = 0.5
        timer.start();
    }

    onReleased: {
        opacity = 1.0
        if (timer.running) {
            timer.stop();
            button.clicked();
        }
    }
    onEntered: {
        enteredAnimation.start()
    }
    onExited: {
        exitedAnimation.start()
    }

    NumberAnimation {
        id:enteredAnimation
        target: selecredRect
        property: "opacity"
        easing.type: Easing.InQuint
        to: 0.8
        duration: 120
    }
    NumberAnimation{
        id:exitedAnimation
        target: selecredRect
        property: "opacity"
        easing.type: Easing.InQuint
        to: 0
        duration: 120
    }
    function showPresed(){
        enteredAnimation.start();
    }
}

import QtQuick 2.15
import QtQuick.Layouts 1.2
import QtQuick.Controls 2.0
import QMLMainToolBarController 1.0
import QMLPlaceLMarkTool 1.0
import QMLToolsProvider 1.0

Item {
    id: tool_PlaceLMark
    property ToolsProvider toolsProv
    property PlaceLMarkTool toolPlaceLmark

    function init(tool)
    {
        toolsProv = tool;
        toolPlaceLmark = toolsProv.currentToolObj();
        name.text = toolPlaceLmark.toolName()
        gridView.model = toolPlaceLmark.model
        filterArea.settingsModel = toolPlaceLmark.model.filterSettings
        //textFilter.text = toolPlaceLmark.model.sfilter;
        //tagsGroupList.model = toolPlaceLmark.tagGroupsCount;
    }

    Connections {
        target: toolPlaceLmark

    }
    Text {
        id: name
        height: 24
        horizontalAlignment: TextInput.AlignLeft
        verticalAlignment: TextInput.AlignVCenter
        font.pointSize: 14
        anchors.top: parent.top
        anchors.left: parent.left
        anchors.right: parent.right
        anchors.margins: 5
    }
    ImageButton{
        anchors.top: parent.top
        anchors.right: parent.right
        width: 24
        height: 24
        anchors.margins: 5
        source: "image://provider/Exit"
        onClicked:
        {
            toolsProv.closeTool();
        }
    }

    GridView {
        id: gridView
        y: 100
        x: 5
        width: parent.width; height: parent.height -  y
        cellWidth: 60; cellHeight: 60
        model: 0
        clip: true
        delegate: Rectangle {

            width: 58
            height: 58
            Image {
                //width: 58
                //height: 58
                anchors.centerIn: parent
                sourceSize: Qt.size(58, 58)
                source: "image://provider/IsoCmon-IsoAnim-" + Id
            }
            Rectangle
            {
                color: "transparent"
                border.color: "green"
                border.width: 2
                visible: index === toolPlaceLmark.model.targetIndex
                anchors.fill: parent
            }
            MouseArea{
                anchors.fill: parent
                onClicked: toolPlaceLmark.model.targetIndex = index
            }
        }
    }
    FilterAreaWidget {
        id: filterArea
        anchors.top: parent.top
        anchors.topMargin: 30
        height: 60
        width: parent.width - 10
        x:5
        openToRight: false
        tagListHeight: 160
        sortVisible: false
        //settingsModel: toolPlaceLmark.model.filterSettings
    }
}

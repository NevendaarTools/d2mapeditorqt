import QtQuick 2.0
import QMLGroupUnitsEditor 1.0
import QtQuick.Controls 2.1
import QMLStyleManager 1.0

Item {
    property GroupUnitsEditor garrison
    property bool isLeft: true
    property bool isEditable: true
    property Item parentItem
    id: item
    property int unitX: 0
    property int unitY: 0
    height: 100
    width: 84 * 2 + 10
    StyleManager
    {
        id: style
    }
    Rectangle{
        id: rect
        height: 100
        anchors.left: parent.left
        anchors.top: parent.top
        width: garrison.isSmall(unitX, unitY)? 84 : 84 * 2 + 10
        color: "transparent"
        border.color: "darkgrey"
        visible: garrison.isSmall(unitX, unitY) || (item.isLeft?(unitX == 0):(unitX == 1))
    }

    function reload()
    {
        unitView.reload();
    }

    Item {
        id: unitView
        width: garrison.isSmall(unitX, unitY)? 84 : 84 * 2 + 10

        function reload()
        {
            rect.width = garrison.isSmall(unitX, unitY)? 84 : 84 * 2 + 10
            rect.visible = garrison.isSmall(unitX, unitY) || (item.isLeft?(unitX == 0):(unitX == 1))
            visible = garrison.isSmall(unitX, unitY) || (item.isLeft?(unitX == 0):(unitX == 1))
            unitView.width = rect.width
            icon.source = "image://provider/" + garrison.icon(unitX, unitY)
            icon.mirror = (!item.isLeft)
            icon.width = rect.width
            icon.visible = garrison.unitId(unitX, unitY) !== ""
            leaderRect.visible = garrison.isLeader(unitX, unitY)
            levelSpin.min = garrison.unitBaseLvl(unitX, unitY)
            levelSpin.max = garrison.unitMaxLvl()
            levelSpin.value = garrison.unitLvl(unitX, unitY)
            levelSpin.image.width = 0

            var hp = garrison.unitCurrentHp(unitX, unitY)
            var maxHp = garrison.unitMaxHp(unitX, unitY)
            healthProgressBar.value = hp
            healthProgressBar.max = maxHp
            progressText.text = hp + " / " + maxHp

        }
        anchors.left: parent.left
        anchors.top: parent.top
        height: parent.height
        //visible: garrison.unitId(unitX, unitY) !== ""

        Image {
            id: icon
            Drag.active: dragArea.drag.active
            Drag.dragType: Drag.Automatic
            Drag.onDragStarted:
            {
                garrison.setDragSource(unitX, unitY);
//                print("drag started" + unitX + " - " + unitY);
            }
            Drag.onDragFinished: {
                garrison.finishDrag();
//                print("drag finished" + unitX + " - " + unitY);
            }
            Drag.mimeData: {
                "id": garrison.unitId(unitX, unitY),
                "mode": "moveUnit"
            }
            MouseArea {
                id: dragArea
                anchors.fill: parent
                drag.target: unitView
                hoverEnabled: true
                onPressed: icon.grabToImage(function(result) {
                    parent.Drag.imageSource = result.url
                })
                onEntered: {
                    overlay.showPreview(dragArea, "Preview/Unit.qml", garrison.unitUid(unitX, unitY))
                }
                onExited: {
                    overlay.hidePreview()
                }
            }
            mirror: (!rect.item)
            anchors.fill: parent
        }

        Rectangle {
            id:leaderRect

            anchors.fill: icon
            border.width: 2
            border.color: "green"
            color: "transparent"
        }

        NTSpinBoxInt {
            height: 22
            width: item.isEditable? 72 : 36
            id: levelSpin
            visible: icon.visible
            anchors.top: icon.top
            anchors.topMargin: -12
            image.width: 0
            backgroundRect.visible: true
            anchors.horizontalCenter: parent.horizontalCenter
            editable: item.isEditable

            onValueModified: (value)=>
            {
                garrison.setUnitLvl(unitX, unitY, value)
            }
        }
        Rectangle{
            color: "gold"
            anchors.fill: editButton
            anchors.margins: -2
            visible: garrison.hasModifiers(unitX, unitY) && editButton.visible
        }
        ImageButton{
            id: editButton
            anchors.left: parent.left
            anchors.bottom:  healthProgressBar.top
            width: style.iconSize * 0.6
            height: style.iconSize * 0.6
            source: "image://provider/Edited"
            visible: icon.visible && (item.isEditable || garrison.hasModifiers(unitX, unitY))
            onClicked: {
                var component = Qt.createComponent("UnitEditorWidget.qml");
                if (component.status === Component.Ready) {
                    var msg = component.createObject(parentItem,
                                                     {
                                                         unitUid: garrison.unitUid(unitX, unitY)
                                                     });
                    //msg.setUid(Id)
                }
            }
        }

        ImageButton{
            anchors.right: parent.right
            anchors.bottom:  healthProgressBar.top
            width: style.iconSize * 0.6
            height: style.iconSize * 0.6
            source: "image://provider/Delete"
            visible: icon.visible && item.isEditable
            onClicked: {
                garrison.remove(unitX, unitY);
                item.reload();
            }
        }
        Item{
            id: healthProgressBar
            width: icon.width
            height: style.iconSize * 0.4
            anchors.top: icon.bottom
            anchors.topMargin: -6
            anchors.horizontalCenter: icon.horizontalCenter
            visible: icon.visible

            property int value: 0
            property int max: 0

            Rectangle {
                anchors.fill: parent
                color: "#502525"
                radius: 3
            }
            Rectangle {
                width: (healthProgressBar.value / (0.0 + healthProgressBar.max)) * healthProgressBar.width
                height: parent.height
                radius: 2
                color: "#336633"
            }
        }
        Text {
            id: progressText
            visible: icon.visible
            anchors.centerIn: healthProgressBar
            font.pixelSize: 11
            font.bold: true
            color: "black"
            horizontalAlignment: TextInput.AlignHCenter
            verticalAlignment: TextInput.AlignTop
        }
        DropArea {
            id:dropArea
            anchors.left: parent.left
            anchors.top: parent.top
            height: parent.height
            width: 84
            Rectangle {
                id: dragTargetArea
                anchors.fill: parent
                border.color:  "gold"
                border.width: 3
                visible: parent.containsDrag
                color: "transparent"
            }

            onEntered: {
    //            if (drag.getDataAsString("mode") === "hire")
    //            {
    //                dragTargetArea.visible = true
    //                return;
    //            }
    //            if (drag.getDataAsString("mode") === "moveUnit")
    //            {
    //                dragTargetArea.visible = true
    //                return;
    //            }
    //            dragTargetArea.visible = false
            }
            onExited: {

            }
            onDropped: {
                if (drop.getDataAsString("mode") === "hire")
                {
                    garrison.tryHire(unitX, unitY, drop.getDataAsString("id"), drop.getDataAsString("level"))
                }
                if (drop.getDataAsString("mode") === "moveUnit")
                {
                    garrison.setDragTarget(unitX, unitY);
                }

    //            print("dropped" + unitX + " - " + unitY)
            }
        }
    }
}

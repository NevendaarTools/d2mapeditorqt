import QtQuick 2.3
import QtQuick.Controls 1.2
import QtQuick.Layouts 1.1
import QtQuick.Window 2.0
import QMLStyleManager 1.0

NTRectangle {
    id: button
    property alias text: textItem.text
    property alias font: textItem.font
    height: textItem.height + 5
    StyleManager
    {
        id: style
    }
    Rectangle {
        id:rect
        color: "#0d0d0d"
        border.color: "#299132"
        opacity: 0.15
        anchors.fill: parent
        //radius: 15
    }

    Text {
        x: 3
        width: parent.width - 6
        id: textItem
        wrapMode: Text.WordWrap
        horizontalAlignment: TextInput.AlignLeft
        verticalAlignment: TextInput.AlignVCenter
        font.pointSize: style.fontSize
        rotation: 0
    }
}

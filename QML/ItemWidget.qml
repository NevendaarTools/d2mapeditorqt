import QtQuick 2.0
import QMLItemView 1.0
import QMLCostEditor 1.0
import QtQuick.Controls 2.14
import QtQuick.Layouts 1.15

Rectangle {
    id: wrapper
    property alias itemId: view.itemId
    ItemView{
        id: view
    }
    function setUID(objectUid)
    {
        itemId = objectUid
    }

    width: 300
    height: 180
    border.color: "black"
    border.width: 2
    color: "gray"

    Image{
        id: icon
        source: "image://provider/" + view.icon
//        width: 64
//        height: 64
        anchors.top: parent.top
        anchors.left: parent.left
        anchors.margins: {5;5;5;5}
        smooth: true
        visible: itemId != ""
    }
    Rectangle{
        anchors.fill: icon2
        visible: icon2.visible
        color: "grey"
        border.color: "black"
    }

    Image{
        id: icon2
        visible: view.icon2 != ""
        source: "image://provider/" + view.icon2
        width: 21
        height: 21
        anchors.top: parent.top
        anchors.left: parent.left
        anchors.margins: {5;5;5;5}
        smooth: true
    }

    Text {
        id: name
        text: view.name
        font.bold: true
        anchors.top: parent.top
        anchors.left: icon.right
        anchors.right: parent.right
        anchors.margins: 5
    }
    Text {
        id: desc
        anchors.top: icon.bottom
        anchors.left: parent.left
        anchors.right: parent.right
        wrapMode: Text.WordWrap
        text: view.desc
        anchors.margins: 5
    }
    NTCostView {
        id: costView
        anchors.bottom: parent.bottom
        anchors.left: parent.left
        anchors.right: parent.right
        anchors.margins: 5
        value: view.cost
    }
}

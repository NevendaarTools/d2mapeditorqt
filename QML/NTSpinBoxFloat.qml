import QtQuick 2.0
import QMLItemView 1.0
import QMLCostEditor 1.0
import QtQuick.Controls 2.14
import QtQuick.Layouts 1.15

Item {
    id: root
    property alias imageSource: valueIcon.source
    property alias image: valueIcon
    property bool editable: true
    property alias text: valueCount.text
    property double min: 0
    property double max: 9999

    signal valueChanged(double value)

    Image {
        anchors.left: parent.left
        anchors.leftMargin: 0
        anchors.verticalCenter: parent.verticalCenter
        id: valueIcon

        width: parent.height
        height: parent.height
    }
    Text {
        id: valueCountText
        anchors.left: valueIcon.right
        anchors.right: parent.right
        anchors.top: parent.top
        anchors.bottom: parent.bottom
        text: valueCount.text
        visible: !editable && Count > 0
        horizontalAlignment: TextInput.AlignHCenter
        verticalAlignment: TextInput.AlignVCenter
    }
    TextField {
        anchors.left: valueIcon.right
        anchors.right: parent.right
        anchors.top: parent.top
        anchors.bottom: parent.bottom
        anchors.rightMargin: 21
        anchors.leftMargin: 21
        horizontalAlignment: TextInput.AlignHCenter
        verticalAlignment: TextInput.AlignVCenter
        id: valueCount
        visible: editable
        //text: valueCountText.text
        topInset: 0
        bottomInset: 0
        selectByMouse: true
        background: Rectangle {
            implicitWidth: parent.width
            implicitHeight: parent.height
            color: "transparent"
//            color: editable ? "grey" : "transparent"
            border.color: editable ? "black" : "transparent"
        }
        validator: DoubleValidator {bottom: root.min; top: root.max;}
        onTextChanged:
        {
            root.textChanged(text)
            root.valueChanged(text * 1)
        }

    }
    ImageButton{
        visible: editable
        width: 21
        height: parent.height
        anchors.top: parent.top
        anchors.left: valueIcon.right
        source: "image://provider/left"
        border.visible: false
        onClicked: {
            var value = valueCount.text * 1.0 - 0.15
            if (value < root.min)
                value = root.min
            valueCount.text = value
        }
    }
    ImageButton{
        visible: editable
        width: 21
        height: parent.height
        anchors.top: parent.top
        anchors.right: parent.right
        source: "image://provider/right"
        border.visible: false
        onClicked: {
            var value = valueCount.text * 1.0 + 0.15
            if (value > root.max)
                value = root.max
            valueCount.text = value
        }
    }
}

import QtQuick 2.12
import QtQuick.Controls 2.14
import QMLVillageEditor 1.0
import Qt.labs.qmlmodels 1.0
import QtQuick.Layouts 1.15
import NTModels
import QMLStyleManager 1.0

Item
{
    id: rootItem
    StyleManager
    {
        id: style
    }
    function prefferedHeight(rowCount)
    {
        return rowCount  * (rowH +eventsList.spacing)
    }

    EventsListModel {
        id: eventsModel
    }

    property alias objectUID: eventsModel.objectUID
    property bool previewMode: false
    property int rowH: 24
    property Item parentItem: null
    Text {
        id: title
        text: qsTr("Events:")
        font.pointSize: 13
        width: parent.width
        height: previewMode? 0:30
        anchors.top: parent.top
        anchors.right: parent.right
        anchors.left: parent.left
        horizontalAlignment: TextInput.AlignHCenter
        visible: !previewMode
    }
    ImageButton{
        height: 32
        width: 32
        anchors.top: parent.top
        anchors.right: parent.right
        anchors.rightMargin: 5
        source: "image://provider/Add"
        visible: !previewMode
        onClicked: {
            if (menuDropDown.requested) {
                menuDropDown.requested = false;
                menuDropDown.close();
            } else {
                menuDropDown.requested = true;
                menuDropDown.open();
            }
        }
    }
    NTDropDownButton{
        id:menuDropDown
        x: 0
        y: 35
        width: parent.width
        height: parent.height - 45
        model: eventsModel.availableEvents
        onItemClicked: index => {
            var newId = eventsModel.createEvent(eventsModel.availableEvents.getDesc(index));
            if (newId != "") {
                var component = Qt.createComponent("edit_Event.qml");
                if (component.status === Component.Ready) {
                    var msg = component.createObject(item,
                                                {
                                                    //text: stageEditor.eventDesc(Id)
                                                });
                    msg.width = item.width
                    msg.height = item.height
                    msg.x = 0
                    msg.y = 0
                    msg.setEventUid(newId)
                }
            }
        }
    }

    ListView{
        ScrollBar.vertical: ScrollBar {
            active: false
        }
        id: eventsList
        anchors.fill: parent
        anchors.topMargin: previewMode? 0:30
        anchors.leftMargin: 3
        anchors.rightMargin: 3
        model: eventsModel

        spacing: 2
        clip: true
        orientation: Qt.Vertical
        delegate:  Rectangle {
            width: rootItem.width
            height: 30
            Image{
                anchors.left: parent.left
                anchors.leftMargin: 0
                width: style.iconSize
                height: style.iconSize
                source: "image://provider/Events"
                visible: previewMode
            }
            Rectangle {
                id: hoverRect
                color: "transparent"
                border.color: "black"
                border.width: 2
                anchors.fill: parent
                visible: false
            }

            Text {
                anchors.centerIn: parent
                text: Name + " " + index//Id
            }

            MouseArea {
                id: dragArea
                anchors.fill: parent
                hoverEnabled: true
                onEntered: {
                    overlay.showTooltip(hoverRect, Desc)
                }
                onExited: {
                    overlay.hideTooltip()
                }
            }
            ImageButton{
                height: 24
                width: 24
                visible: !previewMode
                anchors.right: parent.right
                anchors.verticalCenter: parent.verticalCenter
                anchors.margins: 5
                source: "image://provider/Settings"
                onClicked: {
                    var component = Qt.createComponent("edit_Event.qml");
                    if (component.status === Component.Ready) {
                        var msg = component.createObject(item,
                                                         {
                                                             //text: stageEditor.eventDesc(Id)
                                                         });
                        msg.width = item.width
                        msg.height = item.height
                        msg.x = 0
                        msg.y = 0
                        msg.setEventUid(Id)
                    }
                }
            }
        }
    }
}

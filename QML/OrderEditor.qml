import QtQuick 2.12
import QtQuick.Controls 2.14
import QMLVillageEditor 1.0
import Qt.labs.qmlmodels 1.0
import QtQuick.Layouts 1.15
import NTComponents 1.0
import QMLSelectObjectModel 1.0

NTRectangle{
    id: orderRect
    property StackOrderEditor orderModel: StackOrderEditor{}
    signal orderSelected(bool hasTarget);
    property alias descEnabled: desc.visible
    //anchors.right: inventory1Grid.right

    ComboBox {
        id: orderCombo
        anchors.top: parent.top
        anchors.margins: 5
        anchors.left: parent.left
        width: 200
        height: 25
        model: orderModel
        currentIndex: orderModel.orderIndex
        textRole: "Name"
        onCurrentIndexChanged: {
            orderModel.orderIndex = currentIndex
            orderSelected(orderModel.currentTargetType !== 0)
        }
    }

    Button {
        id: orderButton
        anchors.verticalCenter: orderCombo.verticalCenter
        anchors.right: parent.right
        anchors.left: orderCombo.right
        anchors.margins: 5
        height: 25
        text: orderModel.targetName
        visible: orderModel.currentTargetType !== 0
        onClicked: selectOrderTargetWidget.visible = !selectOrderTargetWidget.visible
    }
    Text {
        id: desc
        text: orderModel.orderDesc
        anchors.top: orderCombo.bottom
        anchors.bottom: parent.bottom
        anchors.left: parent.left
        anchors.right: parent.right
        anchors.margins: 3
        anchors.topMargin: 10
        wrapMode: Text.WordWrap
        horizontalAlignment: TextInput.AlignHCenter
        verticalAlignment: TextInput.AlignTop
        font.pointSize: 10
    }
}

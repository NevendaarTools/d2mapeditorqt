import QtQuick 2.0
import QMLObjectRemoveTool 1.0
import QMLToolsProvider 1.0

Item {
    property ToolsProvider toolsProv
    property ObjectRemoveTool removeTool
    function init(tool)
    {
        toolsProv = tool;
        removeTool = toolsProv.currentToolObj();
        name.text = removeTool.toolName()
    }
    Connections {
        target: removeTool
        function onObjectChanged() {
            objName.text = removeTool.selectedObjectName()
        }
    }
    Text {
        id: name
        height: 24
        horizontalAlignment: TextInput.AlignLeft
        verticalAlignment: TextInput.AlignVCenter
        font.pointSize: 14
        anchors.top: parent.top
        anchors.left: parent.left
        anchors.right: parent.right
        anchors.margins: 5
    }

    ImageButton{
        anchors.top: parent.top
        anchors.right: parent.right
        width: 24
        height: 24
        anchors.margins: 5
        source: "image://provider/Exit"
        onClicked:
        {
            toolsProv.closeTool();
        }
    }

    Text {
        id: objName

        height: 30
        horizontalAlignment: TextInput.AlignLeft
        verticalAlignment: TextInput.AlignVCenter
        font.pointSize: 14
        anchors.top: name.bottom
        anchors.left: parent.left
        anchors.right: parent.right
        anchors.margins: 5
    }
}

import QtQuick 2.15
import QtQuick.Layouts 1.2
import QtQuick.Controls 2.14
import QMLMainToolBarController 1.0
import QMLPlaceLMarkTool 1.0
import QMLToolsProvider 1.0
import QMLStyleManager 1.0
import QMLDynamicImageItem 1.0

Item {
    anchors.fill: parent
    id:toolbarItem
    StyleManager
    {
        id: style
    }
    Rectangle{
        anchors.fill: parent
        color: "grey"
        border.color: "black"
        //opacity: 0.8
    }

    NTImageButton
    {
        id: menuButton
        source: "image://provider/Settings"
        width: style.iconSize
        height: style.iconSize
        anchors.top: parent.top
        anchors.right: parent.right
        anchors.margins: 5
        onClicked: {
            if (menuDropDown.requested) {
                menuDropDown.requested = false;
                menuDropDown.close();
            } else {
                menuDropDown.requested = true;
                menuDropDown.open();
            }
        }
    }
    NTImageButton
    {
        id: encyclopediaButton
        source: "image://provider/Encyclopedia"
        width: style.iconSize
        height: style.iconSize
        anchors.top: parent.top
        anchors.right: menuButton.left
        anchors.margins: 5
        tooltip: "Encyclopedia"
        onClicked: {
            toolbar.requestDataView();
        }
    }

    NTDropDownButton{
        id:menuDropDown
        x: menuButton.x - width//(parent.width - menuButton.width) / 2 - 90
        y: menuButton.y
        width: 200
        height: 4 * style.iconSize + 5 * 4
        model: ListModel {
            ListElement { label: "Save map"; icon: "image://provider/Save" }
            ListElement { label: "Export to D2 format"; icon: "image://provider/Save" }
            ListElement { label: "Settings"; icon: "image://provider/Settings" }
            ListElement { label: "Exit"; icon:  "image://provider/Exit" }
        }
        onItemClicked: index => {
            switch(index){
            case 0:
                toolbar.save();
                break;
            case 1:
                toolbar.exportMap();
                break;
            case 2:
                toolbar.settings();
                break;
            case 3:
                overlay.hidePreview();
                overlay.hideTooltip();
                toolbar.exit();
                break;
            }
        }
    }
    Item {
        anchors.left: parent.left
        anchors.right: menuButton.left
        anchors.top: parent.top
        anchors.margins: 5
        height: style.iconSize
        Row {
            anchors.verticalCenter: parent.verticalCenter
            NTImageButton{
                width: style.iconSize
                height: style.iconSize
                source: "image://provider/MoveTool"
                onClicked: {
                    tools.selectTool("move");
                }
                tooltip: "Move objects tool"
            }
            NTImageButton{
                width: style.iconSize
                height: style.iconSize
                source: "image://provider/RemoveTool"
                onClicked: {
                    tools.selectTool("remove");
                }
                tooltip: "Remove objects tool"
            }
            NTImageButton{
                width: style.iconSize
                height: style.iconSize
                source: "image://provider/CloneTool"
                onClicked: {
                    tools.selectTool("clone");
                }
                tooltip: "Clone objects tool"
            }
        }
    }
    Grid{
        anchors.right: parent.right
        anchors.left: parent.left
        anchors.top: menuButton.bottom
        anchors.bottom: toolRect.top
        anchors.margins: 5
        columns: parent.width / (style.iconSize + 5)
        spacing: 5
        NTImageButton{
            id: mapEditButton
            width: style.iconSize
            height: style.iconSize
            source: "image://provider/MapTool"
            onClicked: {
                tools.selectTool("map_edit");
            }
            tooltip: "Map edit tool"
        }
        NTImageButton{
            id: placeStackButton
            width: style.iconSize
            height: style.iconSize
            source: "image://provider/StackTool"
            onClicked: {
                tools.selectTool("place_stack");
            }
            tooltip: "Place stack tool"
        }
        NTImageButton{
            id: placeTreasureButton
            width: style.iconSize
            height: style.iconSize
            source: "image://provider/TreasureTool"
            onClicked: {
                tools.selectTool("place_treasure");
            }
            tooltip: "Place treasure tool"
        }
        NTImageButton{
            id: placeMerchantButton
            width: style.iconSize
            height: style.iconSize
            source: "image://provider/MerchantTool"
            onClicked: {
                tools.selectTool("place_merchant");
            }
            tooltip: "Place merchant tool"
        }
        NTImageButton{
            id: crystalButton
            width: style.iconSize
            height: style.iconSize
            source: "image://provider/CrystalTool"
            onClicked: {
                tools.selectTool("place_crystal");
            }
            tooltip: "Place crystal tool"
        }
        NTImageButton{
            id: ruinButton
            width: style.iconSize
            height: style.iconSize
            source: "image://provider/RuinTool"
            onClicked: {
                tools.selectTool("place_ruin");
            }
            tooltip: "Place ruin tool"
        }
        NTImageButton{
            id: villageButton
            width: style.iconSize
            height: style.iconSize
            source: "image://provider/VillageTool"
            onClicked: {
                tools.selectTool("place_village");
            }
            tooltip: "Place village tool"
        }
        NTImageButton{
            id: lmarkButton
            width: style.iconSize
            height: style.iconSize
            source: "image://provider/LandMarkTool"
            onClicked: {
                tools.selectTool("place_lmark");
            }
            tooltip: "Place landmark tool"
        }
        NTImageButton{
            id: tombButton
            width: style.iconSize
            height: style.iconSize
            source: "image://provider/TombTool"
            onClicked: {
                tools.selectTool("place_tomb");
            }
            tooltip: "Place tomb tool"
        }
    }
    property var toolWidget: null
    Connections{
        target: tools
        function onToolClosed() {
            if (toolWidget !== null)
            {
                toolbarItem.toolWidget.destroy()
                toolbarItem.toolWidget = null
            }
        }
        function onToolChanged() {
            console.debug("tool changed");
            console.debug(tools.editorFile());
            var component = Qt.createComponent(tools.editorFile());
            if (component.status === Component.Ready) {
                var msg = component.createObject(toolRect,
                                                 {

                                                 });
                msg.width = toolRect.width
                msg.height = toolRect.height
                msg.init(tools)
                msg.x = 0
                msg.y = 0
                toolbarItem.toolWidget = msg
            }
        }
    }


    Rectangle
    {
        id: toolRect
        anchors.right:  parent.right
        anchors.left:  parent.left
        anchors.top:  parent.top
        anchors.topMargin: 115
        anchors.bottom:  parent.bottom
        color: "gray"
        border.color: "black"
        border.width: 2
        anchors.margins: 5
        anchors.bottomMargin: style.iconSize * 2 + 15

        Item{
            anchors.right: toolRect.right
            anchors.left: toolRect.left
            anchors.top: toolRect.top
            height: width
            id: minimapRect
            anchors.margins:2
            visible: mapPreview.visible

        }

        DynamicImageItem {
            id: mapPreview
            source: toolbar.mapImage
            anchors.fill: minimapRect
            rotation:45
            scale: 0.75
            anchors.margins: 10
            // rotation:45
            visible: toolbarItem.toolWidget === null
            Rectangle{
                width: 10
                height: 10
                radius:4
                color: "transparent"
                border.color: "yellow"
                border.width: 3
                x: toolbar.mapPosition.x * parent.width
                y: toolbar.mapPosition.y * parent.height
            }
        }
        DynamicImageItem {
            id: mapPreview2
            source: toolbar.mapImage
            anchors.top: minimapRect.bottom
            anchors.right: toolRect.right
            anchors.left: toolRect.left
            anchors.margins: 10
            height: width
            // rotation:45
            visible: toolbarItem.toolWidget === null
            Rectangle{
                width: 10
                height: 10
                radius:4
                color: "transparent"
                border.color: "yellow"
                border.width: 3
                x: toolbar.mapPosition.x * parent.width
                y: toolbar.mapPosition.y * parent.height
            }
            MouseArea{
                anchors.fill: parent
                onClicked: mouse => {
                    var relativeX = mouse.x / parent.width;
                    var relativeY = mouse.y / parent.height;
                    toolbar.minimapClicked(relativeX.toFixed(2), relativeY.toFixed(2))
                }
            }
        }
    }

    NTImageButton{
        id: layersButton
        anchors.bottom: parent.bottom
        anchors.right: parent.right
        anchors.margins: 5
        width: style.iconSize
        height: style.iconSize
        source: "image://provider/Layers"
        //text: tools.tr("Layers")
        onClicked: {
            tools.selectTool("configure_layers");
        }
        tooltip: "Display layers tool"
    }
    NTImageButton{
        anchors.bottom: parent.bottom
        anchors.right: layersButton.left
        anchors.margins: 5
        id: searchButton
        width: style.iconSize
        height: style.iconSize
        source: "image://provider/Search"
        //text: tools.tr("Search")
        onClicked: {
            tools.selectTool("search_objects");
        }
        tooltip: "Search objects tool"
    }
    NTTwoStateButton{
        id: raceButton
        anchors.bottom: parent.bottom
        anchors.left: parent.left
        anchors.margins: 5
        source: "image://provider/MenuButton"
        activeSource: "image://provider/MenuButtonActive"
        width: style.buttonWidht
        height: style.buttonHeight
        onClicked: tools.selectTool("place_capital")
        text: tools.tr("Race")
    }

    NTImageButton{
        anchors.bottom: parent.bottom
        anchors.left: parent.left
        anchors.margins: 5
        anchors.bottomMargin: style.iconSize + 10
        width: style.iconSize
        height: style.iconSize
        source: "image://provider/Generator"
        id: generateButton
        onClicked: {
//            tools.selectTool("map_generate");
        }
        tooltip: "Map generation tool(WIP, do not use)"
    }
    NTImageButton{
        id: eventsButton
        anchors.left: generateButton.right
        anchors.top: generateButton.top
        anchors.topMargin: 0
        anchors.margins: 5
        width: style.iconSize
        height: style.iconSize
        source: "image://provider/Events"
        onClicked: {
            toolbar.openQuestLineEditor();
        }
        tooltip: "Event edit tool"
    }
    NTImageButton{
        id: statisticsButton
        anchors.left: layersButton.left
        anchors.leftMargin: 0
        anchors.top: eventsButton.top
        anchors.topMargin: 0
        anchors.margins: 5
        width: style.iconSize
        height: style.iconSize
        source: "image://provider/Events"
        onClicked: {
            toolbar.openMapStatistic();
        }
        tooltip: "Map statistics"
    }
    NTImageButton{
        id: infoButton
        anchors.right: statisticsButton.left
        anchors.leftMargin: 0
        anchors.top: eventsButton.top
        anchors.topMargin: 0
        anchors.margins: 5
        width: style.iconSize
        height: style.iconSize
        source: "image://provider/Events"
        onClicked: {
            toolbar.openMapInfoEditor();
        }
        tooltip: "Map info"
    }

    Rectangle{
        visible: menuDropDown.requested
        anchors.fill: parent
        color: "grey"
        border.color: "transparent"
        opacity: 0.8
    }
}

import QtQuick 2.5
import QtQuick.Controls 2.1
import QMLStyleManager 1.0

Item {
    id: item
    anchors.fill: parent

    StyleManager
    {
        id: style
    }
    Connections{
        target: overlayWidget
        function onTooltipRequested(x,y,text) {
            var screanCoordinates = item.mapToGlobal(Qt.point(0, 0));
            tooltipArea.showTooltip(x - screanCoordinates.x, y - screanCoordinates.y, text)
        }
        function onTooltipHideRequested() {
            tooltipArea.hideTooltip()
        }
        function onPreviewRequested(x,y,source, uid) {
            var screanCoordinates = item.mapToGlobal(Qt.point(0, 0));
            tooltipArea.showPreview(x - screanCoordinates.x, y - screanCoordinates.y, source, uid)
        }
        function onPreviewHideRequested() {
            tooltipArea.hidePreview()
        }
        function onRequestRoot(){
            overlayWidget.setRootItem(tooltipArea);
        }
    }

    Rectangle{
        anchors.top: parent.top
        anchors.left: parent.left
        width: 65
        height: 18
        color: "white"
        opacity: 0.8
        id: posRect
        border.color: "black"
        visible: overlayWidget.position !== ""
        Text {
            anchors.fill: parent
            smooth: true
            horizontalAlignment: Text.AlignHCenter
            verticalAlignment: Text.AlignVCenter
            id: textPosition
            text: overlayWidget.position
        }
    }

    Rectangle{
        anchors.fill: parent
        color: "transparent"
        border.color: "grey"
        opacity: 0.8
    }
    Text {
        anchors.top: parent.top
        anchors.right: parent.right
        anchors.rightMargin: style.toolBarPanelWidth
        text: overlayWidget.gameV
    }
    Item{
        id: tooltipArea
        anchors.fill: parent
        function showTooltip(x, y, text) {
            label.text = text
            toolipPopup.open()
            toolipPopup.x = x
            toolipPopup.y = y

            if (y < item.height / 2)
                toolipPopup.y = y + 15
            else
                toolipPopup.y = y - 15 - toolipPopup.height
            toolipPopup.x = x - toolipPopup.width / 2
            if (toolipPopup.x < 0)
                toolipPopup.x = 3
            if (toolipPopup.x + toolipPopup.width > item.width)
                toolipPopup.x = item.width - toolipPopup.width - 3
            toolipPopup.timer.restart()
        }
        function hideTooltip()
        {
            toolipPopup.close()
        }

        Popup {
            id: toolipPopup
            width: label.contentWidth
            height: label.contentHeight
            property alias timer: closeTimer
            visible: false
            Timer {
                id: closeTimer
                interval: 8000
                repeat: false
                onTriggered: {
                    toolipPopup.close()
                }
            }
            NTRectangle{
                anchors.fill: label
                anchors.margins: -5
            }
            Label {
                id: label
                anchors.centerIn: parent
            }
        }
        function showPreview(x, y, source, uid) {
            previewPopup.objectUid = uid
            previewPopup.loader.source = source
            previewPopup.open()
            previewPopup.x = x - 10
            previewPopup.y = y
            if (loader.loaded()){
                loader.item.setUID(previewPopup.objectUid)
                previewPopup.width = loader.item.implicitWidth
                previewPopup.height = loader.item.implicitHeight
                if (previewPopup.y < 0)
                    previewPopup.y = 3
                if (previewPopup.y + previewPopup.height >  item.height)
                    previewPopup.y = item.height - previewPopup.height - 3

                if (previewPopup.x < 0)
                    previewPopup.x = 3
                if (previewPopup.x + previewPopup.width > item.width)
                    previewPopup.x = item.width - previewPopup.width - 3
            }
            previewPopup.timer.restart()
        }
        function hidePreview()
        {
            previewPopup.close()
        }

        Popup {
            width: 0
            height: 0

            id: previewPopup
            //width: loader.contentWidth
            //height: label.contentHeight
            property alias timer: closeTimer2
            property alias loader: loader
            property string objectUid: ""
            visible: false
            Timer {
                id: closeTimer2
                interval: 8000
                repeat: false
                onTriggered: {
                    previewPopup.close()
                }
            }
            Loader {
                id: loader
                //height: Math.max(item ? item.implicitHeight : 0, 20)
                //width: Math.max(item ? item.implicitWidth : 0, 20)
                onLoaded: {
                    if (loader.item) {
                        if (previewPopup.y < 0)
                            previewPopup.y = 3
                        if (previewPopup.y + previewPopup.height >  item.height)
                            previewPopup.y = item.height - previewPopup.height - 3

                        if (previewPopup.x < 0)
                            previewPopup.x = 3
                        if (previewPopup.x + previewPopup.width > item.width)
                            previewPopup.x = item.width - previewPopup.width - 3
                        loader.item.x = -5
                        loader.item.y = 0
                        loader.item.setUID(previewPopup.objectUid);
                    }
                }
            }
        }
    }
}

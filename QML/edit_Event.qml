import QtQuick 2.15
import QtQuick.Controls 2.15
import QMLStyleManager 1.0
import QMLEventEditor 1.0

Dialog {
    visible: true
    width: style.editorWidht
    height: style.editorHeight
    anchors.centerIn: parent
    property bool acceptChanges : false
    StyleManager
    {
        id: style
    }
    EventEditor{
        id: event
    }
    function setEventUid(uid)
    {
        event.setUID(uid)
    }

    onClosed: {
        console.log("Dialog is closing by other means");
        console.log("Accepted "  + acceptChanges);
        if (acceptChanges)
        {
            event.save()
        }
        else
        {
            event.cancel()
        }
    }

    function cancel()
    {
        acceptChanges = false;
        close()
    }
    function apply()
    {
        acceptChanges = true;
        close()
    }

    NTRectangle {
        id: background
        anchors.fill: parent
        anchors.margins: -6

        Text {
            id: nameLabel
            text: translate.tr("Event name:")
            font.pixelSize: style.fontSize
            anchors.top: parent.top
            anchors.left: parent.left
            anchors.margins: 5
            anchors.topMargin: 20
            width: 110
            height: 35
        }
        TextField {
            id: nameEdit
            anchors.left: nameLabel.right
            anchors.bottom: nameLabel.bottom
            width: 240
            anchors.margins: 5
            height: 30
            font.pointSize: style.fontSize
            color: "black"
            text: event.name
            horizontalAlignment: TextInput.AlignHCenter
            selectByMouse: true
            onTextChanged: {
                if (event.name !== nameEdit.text)
                    event.name = nameEdit.text;
            }
        }
        Text {
            id: activationLabel
            text: translate.tr("Activation:")
            font.pixelSize: style.fontSize
            anchors.top: nameLabel.bottom
            anchors.left: parent.left
            anchors.margins: 5
            width: 110
            height: 30
        }
        NTCheckBox {
            id: infiniteCheckBox
            anchors.left: activationLabel.right
            anchors.bottom: activationLabel.bottom
            height: 30
            width: 140
            text: translate.tr("infinite")
            checked: event.occurInfinite
        }
        NTCheckBox {
            anchors.left: infiniteCheckBox.right
            anchors.bottom: activationLabel.bottom
            height: 30
            width: 140
            text: translate.tr("avaiable at start")
            checked: event.enabledFromStart
        }
        NTRectangle{
            anchors.top: activationLabel.bottom
            anchors.topMargin: 5
            anchors.left: parent.left
            anchors.leftMargin: 15
            height: 40
            width: 340
            Text {
                text: translate.tr("Conditions:")
                anchors.verticalCenter: parent.verticalCenter
                anchors.left: parent.left
                anchors.leftMargin: 5
            }
            NTDropDownButton{
                id:menuDropDownConditions
                x: 0
                y: 35
                width: 460
                height: 500
                model: event.availableConditions
                onItemClicked: index => {
                                   event.addCondition(event.availableConditions.getDesc(index))
                }
            }
            ImageButton{
                height: 32
                width: 32
                anchors.verticalCenter: parent.verticalCenter
                anchors.right: parent.right
                anchors.rightMargin: 5
                source: "image://provider/Add"
                onClicked: {
                    if (menuDropDownConditions.requested) {
                        menuDropDownConditions.requested = false;
                        menuDropDownConditions.close();
                    } else {
                        menuDropDownConditions.requested = true;
                        menuDropDownConditions.open();
                    }
                }
            }
        }

        NTRectangle{
            anchors.fill: conditionsList
            anchors.topMargin: -10
        }

        ListView{
            ScrollBar.vertical: ScrollBar {
                active: true
            }
            id: conditionsList
            anchors.top: activationLabel.bottom
            anchors.topMargin: 50
            anchors.bottom: parent.bottom
            anchors.bottomMargin: 5
            anchors.left: parent.left
            anchors.leftMargin: 15
            width: 340

            spacing: 3
            clip: true
            orientation: Qt.Vertical
            model: event.conditions
            delegate: NTRectangle {
                width: conditionsList.width - 10
                anchors.horizontalCenter: parent.horizontalCenter
                height: 34

                Rectangle {
                    id: hoverRect
                    color: "transparent"
                    border.color: "black"
                    border.width: 2
                    anchors.fill: parent
                    visible: false
                }

                Text  {
                    anchors.left: parent.left
                    anchors.leftMargin: 15
                    anchors.rightMargin: 40
                    anchors.verticalCenter: parent.verticalCenter
                    text: "[" + index + "]"
                    id: indexText
                }

                Text {
                    anchors.verticalCenter: parent.verticalCenter
                    anchors.left: indexText.right
                    anchors.right: editButton.left
                    wrapMode: Text.WordWrap
                    horizontalAlignment: TextInput.AlignHCenter
                    font.bold: true
                    text: Name
                }

                MouseArea {
                    id: dragArea
                    anchors.fill: parent
                    hoverEnabled: true
                    onEntered: {
                        overlay.showTooltip(hoverRect, Desc)
                    }
                    onExited: {
                        overlay.hideTooltip()
                    }
                }
                ImageButton{
                    height: 24
                    width: 24
                    anchors.right: parent.right
                    anchors.verticalCenter: parent.verticalCenter
                    anchors.margins: 5
                    anchors.rightMargin: 20
                    source: "image://provider/Settings"
                    id: editButton
                    onClicked: {
                        var component = Qt.createComponent("edit_EventCondition.qml");
                        if (component.status === Component.Ready) {
                            var msg = component.createObject(item,
                                                             {
                                                             });
                            msg.width = item.width
                            msg.height = item.height
                            msg.x = 0
                            msg.y = 0
                            msg.setEventIdAndConditionNum(event.getUID() + ":" + index)
                        }
                    }
                }
                ImageButton{
                    height: 16
                    width: 16
                    anchors.right: parent.right
                    anchors.bottom: parent.bottom
                    anchors.margins: 0
                    anchors.rightMargin: 2
                    source: "image://provider/CloneTool"
                    id: cloneButton
                    onClicked: {
                        event.cloneCondition(index)
                    }
                }
                ImageButton{
                    height: 16
                    width: 16
                    anchors.right: parent.right
                    anchors.top: parent.top
                    anchors.margins: 0
                    anchors.rightMargin: 2
                    source: "image://provider/Delete"
                    id: removeButton
                    onClicked: {
                        event.removeCondition(index)
                    }
                }
                ImageButton {
                    id: upButton
                    width: 15
                    height: 15
                    anchors.left: parent.left
                    anchors.top: parent.top
                    source: "image://provider/Up"
                    visible: index > 0
                    onClicked: {
                        event.moveCondition(index, -1)
                    }
                }
                ImageButton {
                    id: downButton
                    width: 15
                    height: 15
                    anchors.left: parent.left
                    anchors.bottom: parent.bottom
                    source: "image://provider/Down"
                    visible: index < conditionsList.count - 1
                    onClicked: {
                        event.moveCondition(index, 1)
                    }
                }
            }
        }

        NTRectangle{
            anchors.top: activationLabel.bottom
            anchors.topMargin: 5
            anchors.left: conditionsList.right
            anchors.leftMargin: 5
            height: 40
            width: 340
            Text {
                text: translate.tr("Effects:")
                anchors.verticalCenter: parent.verticalCenter
                anchors.left: parent.left
                anchors.leftMargin: 5
            }
            NTDropDownButton{
                id:menuDropDownEffects
                x: 0
                y: 35
                width: 460
                height: 500
                model: event.availableEffects
                onItemClicked: index => {
                    event.addEffect(event.availableEffects.getDesc(index))
                }
            }
            ImageButton{
                height: 32
                width: 32
                anchors.verticalCenter: parent.verticalCenter
                anchors.right: parent.right
                anchors.rightMargin: 5
                source: "image://provider/Add"
                onClicked: {
                    if (menuDropDownEffects.requested) {
                        menuDropDownEffects.requested = false;
                        menuDropDownEffects.close();
                    } else {
                        menuDropDownEffects.requested = true;
                        menuDropDownEffects.open();
                    }
                }
            }
        }

        NTRectangle{
            anchors.fill: effectsList
            anchors.topMargin: -10
        }

        ListView{
            ScrollBar.vertical: ScrollBar {
                active: true
            }
            id: effectsList
            anchors.top: activationLabel.bottom
            anchors.topMargin: 50
            anchors.bottom: parent.bottom
            anchors.bottomMargin: 5
            anchors.left: conditionsList.right
            anchors.leftMargin: 5
            width: 340

            spacing: 3
            clip: true
            orientation: Qt.Vertical
            model: event.effects
            delegate: NTRectangle {
                width: conditionsList.width - 10
                anchors.horizontalCenter: parent.horizontalCenter
                height: 34

                Rectangle {
                    id: hoverRectE
                    color: "transparent"
                    border.color: "black"
                    border.width: 2
                    anchors.fill: parent
                    visible: false
                }

                Text  {
                    anchors.left: parent.left
                    anchors.leftMargin: 15
                    anchors.rightMargin: 40
                    anchors.verticalCenter: parent.verticalCenter
                    text: "[" + index + "]"
                    id: indexTextE
                }

                Text {
                    anchors.verticalCenter: parent.verticalCenter
                    anchors.left: indexTextE.right
                    anchors.right: editButtonE.left
                    wrapMode: Text.WordWrap
                    horizontalAlignment: TextInput.AlignHCenter
                    font.bold: true
                    text: Name
                }

                MouseArea {
                    id: dragAreaE
                    anchors.fill: parent
                    hoverEnabled: true
                    onEntered: {
                        overlay.showTooltip(hoverRectE, Desc)
                    }
                    onExited: {
                        overlay.hideTooltip()
                    }
                }
                ImageButton {
                    height: 24
                    width: 24
                    anchors.right: parent.right
                    anchors.verticalCenter: parent.verticalCenter
                    anchors.margins: 5
                    anchors.rightMargin: 20
                    source: "image://provider/Settings"
                    id: editButtonE
                    onClicked: {
                        var component = Qt.createComponent("edit_EventEffect.qml");

                        if (component.status === Component.Ready) {
                            var msg = component.createObject(item, {});
                            if (msg === null) {
                                console.error("Не удалось создать объект из компонента.");
                                console.error("Ошибка: " + component.errorString());
                                return;
                            }
                            msg.width = item.width;
                            msg.height = item.height;
                            msg.x = 0;
                            msg.y = 0;
                            msg.setEventIdAndEffectNum(event.getUID() + ":" + index);
                        } else if (component.status === Component.Error) {
                            console.error("Ошибка при загрузке компонента: " + component.errorString());
                        } else {
                            console.log("Статус компонента: " + component.status);
                        }
                    }
                }

                ImageButton{
                    height: 16
                    width: 16
                    anchors.right: parent.right
                    anchors.bottom: parent.bottom
                    anchors.margins: 0
                    source: "image://provider/CloneTool"
                    id: cloneButtonE
                    onClicked: {
                        event.cloneEffect(index)
                    }
                }
                ImageButton{
                    height: 16
                    width: 16
                    anchors.right: parent.right
                    anchors.top: parent.top
                    anchors.margins: 0
                    source: "image://provider/Delete"
                    id: removeButtonE
                    onClicked: {
                        event.removeEffect(index)
                    }
                }
                ImageButton {
                    id: upButtonE
                    width: 15
                    height: 15
                    anchors.left: parent.left
                    anchors.top: parent.top
                    source: "image://provider/Up"
                    visible: index > 0
                    onClicked: {
                        event.moveEffect(index, -1)
                    }
                }
                ImageButton {
                    id: downButtonE
                    width: 15
                    height: 15
                    anchors.left: parent.left
                    anchors.bottom: parent.bottom
                    source: "image://provider/Down"
                    visible: index < effectsList.count - 1
                    onClicked: {
                        event.moveEffect(index, 1)
                    }
                }
            }
        }
        //conditions
        //effects
    }
    Button {
        id: applyButton
        text: qsTr("Apply")
        onClicked: apply()
        z: 1
        anchors.bottom: parent.bottom
        anchors.right: parent.right
        anchors.margins: 4
        width: style.buttonWidht
        height: style.buttonHeight
    }
    Button {
        text: qsTr("Cancel")
        onClicked: cancel()
        z: 1
        anchors.bottom: applyButton.bottom
        anchors.right: applyButton.left
        anchors.rightMargin: 10
        width: style.buttonWidht
        height: style.buttonHeight
    }
}

import QtQuick 2.0
import QtQuick 2.15
import QtQuick.Controls 2.15
import QMLStyleManager 1.0
import QtQuick.Layouts 1.15
import NevendaarTools 1.0
import NTModels 1.0

Dialog {
    visible: true
    anchors.centerIn: parent
    modal: true
    id: dialog
    property bool acceptChanges : false
    StyleManager
    {
        id: style
    }
    EventConditionEditor{
        id: conditionEditor
    }
    function setEventIdAndConditionNum(uid)
    {
        conditionEditor.uid = uid
    }

    onClosed: {
        if (acceptChanges)
        {
            conditionEditor.saveEvent()
        }
        else
        {
            //editor.cancel();
        }
    }


    function cancel()
    {
        acceptChanges = false;
        close()
    }
    function apply()
    {
        acceptChanges = true;
        close()
    }
    NTRectangle {
        id: background
        anchors.fill: parent
        anchors.margins: -12
    }
    Loader{
        id:loader
        anchors.fill: parent
        anchors.bottomMargin: 50
        sourceComponent: {
            switch (conditionEditor.conditionType) {
                case 0: return frequenceComponent;
                case 2: return enteringPredefinedZoneComponent;
                case 3: return enteringCityComponent;
                case 4: return owningCityComponent;
                case 5: return destroyStackComponent;
                case 6: return owningItemComponent;
                case 7: return specificLeaderOwningItemComponent;
                case 8: return diplomacyRelationsComponent;
                case 9: return allianceComponent;
                case 10: return lootingRuinComponent;
                case 11: return transformingLandComponent;
                case 12: return visitingSiteComponent;
                case 14: return stackToLocationComponent;
                case 15: return stackToCityComponent;
                case 16: return itemToLocationComponent;
                case 17: return stackExistanceComponent;
                case 18: return variableIsInRangeComponent;
                case 19: return resourceAmountComponent;
                case 20: return checkGameModeComponent;
                case 21: return checkForHumanComponent;
                case 22: return compareVarComponent;
                case 23: return customScriptComponent;
                default: return null;
            }
        }
        onLoaded: {
            dialog.width = loader.item.dialogWidth
            dialog.height = loader.item.dialogHeight + 40
        }
    }
    Component {
        id: frequenceComponent
        Item{
            property int dialogWidth: 240
            property int dialogHeight: 80
            width: dialogWidth
            height: dialogHeight
            StyleManager
            {
                id: style
            }
            Text {
                id: name
                font.pointSize: style.titleFontSize
                anchors.top: parent.top
                anchors.horizontalCenter: parent.horizontalCenter
                text: translate.tr("FREQUENCY")
            }
            Row{
                anchors.bottom: parent.bottom
                anchors.horizontalCenter: parent.horizontalCenter
                Text {
                    id: label
                    // anchors.centerIn: parent
                    text: translate.tr("Enter frequency:")
                    font.pointSize: style.fontSize
                }
                Item{
                    width: 20
                    height: 10
                }
                NTSpinBoxInt {
                    id: mapNameEdit
                    backgroundRect.visible: true
                    image.width: 0
                    height: 24
                    width: 100
                    min: 1
                    max:99
                    value: conditionEditor.frequency
                    onValueModified: (value)=>{conditionEditor.frequency = value}
                }
            }
        }
    }

    Component {
        id: enteringPredefinedZoneComponent
        Item{
            property int dialogWidth: 440
            property int dialogHeight: 480
            width: dialogWidth
            height: dialogHeight
            StyleManager
            {
                id: style
            }
            Text {
                id: name
                font.pointSize: style.titleFontSize
                anchors.top: parent.top
                anchors.horizontalCenter: parent.horizontalCenter
                text: translate.tr("ENTERING_A_PREDEFINED_ZONE")
            }
            Text {
                id: label
                anchors.top: parent.top
                anchors.topMargin: 30
                text: translate.tr("Select location:")
                font.pointSize: style.fontSize
            }
            NTMapObjectSelectView {
                id: locSelectView
                anchors.fill: parent
                anchors.topMargin: 60
                model:  SelectLocationModel{}
                selectedId: conditionEditor.locId
                onItemSelected: itemId => {
                    conditionEditor.locId = itemId
                }
            }
        }
    }

    Component {
        id: enteringCityComponent
        Item{
            property int dialogWidth: 440
            property int dialogHeight: 480
            width: dialogWidth
            height: dialogHeight
            StyleManager
            {
                id: style
            }
            Text {
                id: name
                font.pointSize: style.titleFontSize
                anchors.top: parent.top
                anchors.horizontalCenter: parent.horizontalCenter
                text: translate.tr("ENTERING_A_CITY")
            }
            Text {
                id: label
                anchors.top: parent.top
                anchors.topMargin: 30
                text: translate.tr("Select city:")
                font.pointSize: style.fontSize
            }
            NTMapObjectSelectView {
                id: locSelectView
                anchors.fill: parent
                anchors.topMargin: 60
                model:  SelectFortModel{}
                selectedId: conditionEditor.villageId
                onItemSelected: itemId => {
                    conditionEditor.villageId = itemId
                }
            }
        }
    }

    Component {
        id: owningCityComponent
        Item{
            property int dialogWidth: 440
            property int dialogHeight: 480
            width: dialogWidth
            height: dialogHeight
            StyleManager
            {
                id: style
            }
            Text {
                id: name
                font.pointSize: style.titleFontSize
                anchors.top: parent.top
                anchors.horizontalCenter: parent.horizontalCenter
                text: translate.tr("OWNING_A_CITY")
            }
            Text {
                id: label
                anchors.top: parent.top
                anchors.topMargin: 30
                text: translate.tr("Select city:")
                font.pointSize: style.fontSize
            }
            NTMapObjectSelectView {
                id: locSelectView
                anchors.fill: parent
                anchors.topMargin: 60
                model:  SelectFortModel{}
                selectedId: conditionEditor.villageId
                onItemSelected: itemId => {
                    conditionEditor.villageId = itemId
                }
            }
        }
    }

    Component {
        id: destroyStackComponent
        Item{
            property int dialogWidth: 440
            property int dialogHeight: 480
            width: dialogWidth
            height: dialogHeight
            StyleManager
            {
                id: style
            }
            Text {
                id: name
                font.pointSize: style.titleFontSize
                anchors.top: parent.top
                anchors.horizontalCenter: parent.horizontalCenter
                text: translate.tr("DESTROY_STACK")
            }
            Text {
                id: label
                anchors.top: parent.top
                anchors.topMargin: 30
                text: translate.tr("Select stack:")
                font.pointSize: style.fontSize
            }
            NTMapObjectSelectView {
                id: locSelectView
                anchors.fill: parent
                anchors.topMargin: 60
                model:  SelectStackModel{}
                selectedId: conditionEditor.stackId
                onItemSelected: itemId => {
                    conditionEditor.stackId = itemId
                }
            }
        }
    }

    Component {
        id: owningItemComponent
        Item{
            property int dialogWidth: 620
            property int dialogHeight: 620
            width: dialogWidth
            height: dialogHeight
            StyleManager
            {
                id: style
            }
            Text {
                id: name
                font.pointSize: style.titleFontSize
                anchors.top: parent.top
                anchors.horizontalCenter: parent.horizontalCenter
                text: translate.tr("OWNING_AN_ITEM")
            }
            Text {
                id: label
                anchors.top: parent.top
                anchors.topMargin: 30
                text: translate.tr("Select item:")
                font.pointSize: style.fontSize
            }
            ItemWidget
            {
                id: itemView
                itemId: conditionEditor.itemType
                anchors.top: label.bottom
                anchors.left: selectItem.right
                height: 280
                anchors.right: parent.right
                DropArea {
                    id:dropArea
                    anchors.fill: parent
                    //anchors.margins: 10
                    Rectangle {
                        id: dragTargetArea
                        anchors.fill: parent
                        border.color:  "gold"
                        border.width: 3
                        visible: parent.containsDrag
                        color: "transparent"
                    }

                    onEntered: {

                    }
                    onExited: {

                    }
                    onDropped: drop=> {
                        if (drop.getDataAsString("mode") === "ItemAdd")
                        {
                            conditionEditor.itemType = drop.getDataAsString("id")
                        }
                    }
                }
            }
            SelectItemWidget
            {
                id: selectItem
                anchors.top: label.bottom
                anchors.left: parent.left
                width: 300
                height: 580
                anchors.margins: 5
                onItemDBLClicked: item=>{
                    conditionEditor.itemType = item
                }
            }
        }
    }

    Component {
        id: visitingSiteComponent
        Item{
            property int dialogWidth: 440
            property int dialogHeight: 480
            width: dialogWidth
            height: dialogHeight
            StyleManager
            {
                id: style
            }
            Text {
                id: name
                font.pointSize: style.titleFontSize
                anchors.top: parent.top
                anchors.horizontalCenter: parent.horizontalCenter
                text: translate.tr("VISITING_A_SITE")
            }
            Text {
                id: label
                anchors.top: parent.top
                anchors.topMargin: 30
                text: translate.tr("Select merchant:")
                font.pointSize: style.fontSize
            }
            NTMapObjectSelectView {
                id: locSelectView
                anchors.fill: parent
                anchors.topMargin: 60
                model:  SelectSiteModel{}
                selectedId: conditionEditor.siteId
                onItemSelected: itemId => {
                    conditionEditor.siteId = itemId
                }
            }
        }
    }

    Component {
        id: stackToLocationComponent
        Item{
            property int dialogWidth: 640
            property int dialogHeight: 510
            width: dialogWidth
            height: dialogHeight
            StyleManager
            {
                id: style
            }
            Text {
                id: name
                font.pointSize: style.titleFontSize
                anchors.top: parent.top
                anchors.horizontalCenter: parent.horizontalCenter
                text: translate.tr("STACK_IN_LOCATION")
            }
            Text {
                id: label
                anchors.top: parent.top
                anchors.topMargin: 30
                anchors.left: parent.left
                text: translate.tr("Select stack:")
                font.pointSize: style.fontSize
            }
            NTMapObjectSelectView {
                id: stackSelectView
                anchors.left: parent.left
                anchors.right: parent.horizontalCenter
                anchors.margins: 5
                anchors.leftMargin: -5
                anchors.top: label.bottom
                height: 445
                model:  SelectStackModel{}
                selectedId: conditionEditor.stackId
                onItemSelected: itemId => {
                    conditionEditor.stackId = itemId
                }
            }
            Text {
                id: label2
                anchors.top: parent.top
                anchors.topMargin: 30
                anchors.left: parent.horizontalCenter
                text: translate.tr("Select location:")
                font.pointSize: style.fontSize
            }
            NTMapObjectSelectView {
                id: villageSelectView
                anchors.left: parent.horizontalCenter
                anchors.right: parent.right
                anchors.margins: 5
                anchors.rightMargin: -5
                anchors.top: label2.bottom
                height: 445
                model:  SelectLocationModel{}
                selectedId: conditionEditor.locId
                onItemSelected: itemId => {
                    conditionEditor.locId = itemId
                }
            }
        }
    }

    Component {
        id: stackToCityComponent
        Item{
            property int dialogWidth: 640
            property int dialogHeight: 510
            width: dialogWidth
            height: dialogHeight
            StyleManager
            {
                id: style
            }
            Text {
                id: name
                font.pointSize: style.titleFontSize
                anchors.top: parent.top
                anchors.horizontalCenter: parent.horizontalCenter
                text: translate.tr("STACK_IN_CITY")
            }
            Text {
                id: label
                anchors.top: parent.top
                anchors.topMargin: 30
                anchors.left: parent.left
                text: translate.tr("Select stack:")
                font.pointSize: style.fontSize
            }
            NTMapObjectSelectView {
                id: stackSelectView
                anchors.left: parent.left
                anchors.right: parent.horizontalCenter
                anchors.margins: 5
                anchors.leftMargin: -5
                anchors.top: label.bottom
                height: 445
                model:  SelectStackModel{}
                selectedId: conditionEditor.stackId
                onItemSelected: itemId => {
                    conditionEditor.stackId = itemId
                }
            }
            Text {
                id: label2
                anchors.top: parent.top
                anchors.topMargin: 30
                anchors.left: parent.horizontalCenter
                text: translate.tr("Select city:")
                font.pointSize: style.fontSize
            }
            NTMapObjectSelectView {
                id: villageSelectView
                anchors.left: parent.horizontalCenter
                anchors.right: parent.right
                anchors.margins: 5
                anchors.rightMargin: -5
                anchors.top: label2.bottom
                height: 445
                model:  SelectFortModel{}
                selectedId: conditionEditor.villageId
                onItemSelected: itemId => {
                    conditionEditor.villageId = itemId
                }
            }
        }
    }

    Component {
        id: itemToLocationComponent
        Item{
            property int dialogWidth: 940
            property int dialogHeight: 620
            width: dialogWidth
            height: dialogHeight
            StyleManager
            {
                id: style
            }
            Text {
                id: name
                font.pointSize: style.titleFontSize
                anchors.top: parent.top
                anchors.horizontalCenter: parent.horizontalCenter
                text: translate.tr("ITEM_TO_LOCATION")
            }
            Text {
                id: label
                anchors.top: parent.top
                anchors.topMargin: 30
                text: translate.tr("Select item:")
                font.pointSize: style.fontSize
            }
            ItemWidget
            {
                id: itemView
                itemId: conditionEditor.itemType
                anchors.top: label.bottom
                anchors.margins: 5
                anchors.left: selectItem.right
                height: 280
                width: 260
                DropArea {
                    id:dropArea
                    anchors.fill: parent
                    //anchors.margins: 10
                    Rectangle {
                        id: dragTargetArea
                        anchors.fill: parent
                        border.color:  "gold"
                        border.width: 3
                        visible: parent.containsDrag
                        color: "transparent"
                    }

                    onEntered: {

                    }
                    onExited: {

                    }
                    onDropped: drop=> {
                        if (drop.getDataAsString("mode") === "ItemAdd")
                        {
                            conditionEditor.itemType = drop.getDataAsString("id")
                        }
                    }
                }
            }
            SelectItemWidget
            {
                id: selectItem
                anchors.top: label.bottom
                anchors.left: parent.left
                width: 300
                height: 550
                anchors.margins: 5
                onItemDBLClicked: item=>{
                    conditionEditor.itemType = item
                }
            }
            Text {
                id: label2
                anchors.top: parent.top
                anchors.topMargin: 30
                anchors.left: itemView.right
                text: translate.tr("Select location:")
                font.pointSize: style.fontSize
            }
            NTMapObjectSelectView {
                id: locSelectView
                anchors.left: itemView.right
                anchors.right: parent.right
                anchors.margins: 5
                anchors.rightMargin: -5
                anchors.top: label2.bottom
                height: 550
                model:  SelectLocationModel{}
                selectedId: conditionEditor.locId
                onItemSelected: itemId => {
                    conditionEditor.locId = itemId
                }
            }
        }
    }

    Component {
        id: stackExistanceComponent
        Item{
            property int dialogWidth: 440
            property int dialogHeight: 480
            width: dialogWidth
            height: dialogHeight
            StyleManager
            {
                id: style
            }
            Text {
                id: name
                font.pointSize: style.titleFontSize
                anchors.top: parent.top
                anchors.horizontalCenter: parent.horizontalCenter
                text: translate.tr("STACK_EXISTANCE")
            }
            Text {
                id: label
                anchors.top: parent.top
                anchors.topMargin: 30
                text: translate.tr("Select stack:")
                font.pointSize: style.fontSize
            }
            ListView {
                height: 24
                anchors.right: parent.right
                orientation: ListView.Horizontal
                anchors.verticalCenter: label.verticalCenter
                width: (style.buttonWidht  * 1.6 + 5) * 2
                spacing: 5
                model: ListModel {
                    ListElement { text: "exist"; value: 0 }
                    ListElement { text: "not exist"; value: 1 }
                }
                delegate:  NTTextButton{
                    width: style.buttonWidht * 1.6
                    height: 24
                    text:  translate.tr(model.text)
                    font.pointSize: 14
                    onClicked: conditionEditor.miscInt = model.value
                    border.visible: conditionEditor.miscInt === model.value
                    border.width: 3
                    border.border.color: style.borderColor
                }
            }
            NTMapObjectSelectView {
                id: locSelectView
                anchors.fill: parent
                anchors.topMargin: 60
                model:  SelectStackModel{}
                selectedId: conditionEditor.stackId
                onItemSelected: itemId => {
                    conditionEditor.stackId = itemId
                }
            }
        }
    }

    Component {
        id: resourceAmountComponent
        Item{
            property int dialogWidth: 250
            property int dialogHeight: 250
            width: dialogWidth
            height: dialogHeight
            StyleManager
            {
                id: style
            }
            Text {
                id: name
                font.pointSize: style.titleFontSize
                anchors.top: parent.top
                anchors.horizontalCenter: parent.horizontalCenter
                text: translate.tr("RESOURCE_AMOUNT")
            }
            Text {
                id: label
                anchors.top: parent.top
                anchors.topMargin: 30
                height: 30
                anchors.horizontalCenter: parent.horizontalCenter
                text: translate.tr("Bank of resources should be:")
                font.pointSize: style.fontSize
            }
            ListView {
                id: listView
                height: 94
                anchors.top: label.bottom
                anchors.topMargin: 5
                orientation: ListView.Vertical
                anchors.left: parent.left
                width: (style.buttonWidht * 1.4 + 5)
                spacing: 5
                model: ListModel {
                    ListElement { text: "Greater<br>or<br>equal"; value: true }
                    ListElement { text: "Less<br>or<br>equal"; value: false }
                }
                delegate:  NTTextButton{
                    width: style.buttonWidht * 1.4
                    height: 80
                    text:  translate.tr(model.text)
                    font.pointSize: 14
                    onClicked: conditionEditor.greaterOrEqual = model.value
                    border.visible: conditionEditor.greaterOrEqual === model.value
                    border.width: 3
                    border.border.color: style.borderColor
                }
            }
            NTRectangle{
                anchors.fill: costView
                anchors.margins: -5
            }

            NTCostView {
                id: costView
                value: conditionEditor.bank
                anchors.left: listView.right
                anchors.top: listView.top
                width: 112
                height: 160
                anchors.leftMargin: 10
                orientation: Qt.Vertical
                editable: true
                onValueModified: value => {
                    conditionEditor.bank = value
                }
            }
        }
    }

    Component {
        id: checkGameModeComponent
        Item{
            property int dialogWidth: 400
            property int dialogHeight: 100
            width: dialogWidth
            height: dialogHeight
            StyleManager
            {
                id: style
            }
            Text {
                id: name
                font.pointSize: style.titleFontSize
                anchors.top: parent.top
                anchors.horizontalCenter: parent.horizontalCenter
                text: translate.tr("CHECK_GAMEMODE")
            }
            Text {
                id: label
                anchors.top: parent.top
                anchors.topMargin: 30
                anchors.horizontalCenter: parent.horizontalCenter
                text: translate.tr("Select game mode:")
                font.pointSize: style.fontSize
            }
            ListView {
                height: 40
                anchors.top: label.bottom
                anchors.topMargin: 5
                orientation: ListView.Horizontal
                anchors.horizontalCenter: parent.horizontalCenter
                width: (style.buttonWidht  * 1.6 + 5) * 3
                spacing: 5
                model: ListModel {
                    ListElement { text: "Single"; value: 0 }
                    ListElement { text: "Hot-seat"; value: 1 }
                    ListElement { text: "Online"; value: 2 }
                }
                delegate:  NTTextButton{
                    width: style.buttonWidht * 1.6
                    height: 40
                    text:  translate.tr(model.text)
                    font.pointSize: 14
                    onClicked: conditionEditor.mode = model.value
                    border.visible: conditionEditor.mode === model.value
                    border.width: 3
                    border.border.color: style.borderColor
                }
            }
        }
    }

    Component {
        id: checkForHumanComponent
        Item{
            property int dialogWidth: 400
            property int dialogHeight: 120
            width: dialogWidth
            height: dialogHeight
            StyleManager
            {
                id: style
            }
            Text {
                id: name
                font.pointSize: style.titleFontSize
                anchors.top: parent.top
                anchors.horizontalCenter: parent.horizontalCenter
                text: translate.tr("CHECK_FOR_HUMAN")
            }
            Text {
                id: label
                anchors.top: parent.top
                anchors.topMargin: 30
                anchors.horizontalCenter: parent.horizontalCenter
                text: translate.tr("Player should be controlled by:")
                font.pointSize: style.fontSize
            }
            ListView {
                height: 50
                anchors.top: label.bottom
                anchors.topMargin: 5
                orientation: ListView.Horizontal
                anchors.horizontalCenter: parent.horizontalCenter
                width: (style.buttonWidht  * 1.8 + 5) * 2
                spacing: 5
                model: ListModel {
                    ListElement { text: "Human"; value: false }
                    ListElement { text: "Artifitial<br>intellect"; value: true }
                }
                delegate:  NTTextButton{
                    width: style.buttonWidht * 1.8
                    height: 50
                    text:  translate.tr(model.text)
                    font.pointSize: 14
                    onClicked: conditionEditor.ai = model.value
                    border.visible: conditionEditor.ai === model.value
                    border.width: 3
                    border.border.color: style.borderColor
                }
            }
        }
    }

    Component {
        id: lootingRuinComponent
        Item{
            property int dialogWidth: 440
            property int dialogHeight: 480
            width: dialogWidth
            height: dialogHeight
            StyleManager
            {
                id: style
            }
            Text {
                id: name
                font.pointSize: style.titleFontSize
                anchors.top: parent.top
                anchors.horizontalCenter: parent.horizontalCenter
                text: translate.tr("LOOTING_A_RUIN")
            }
            Text {
                id: label
                anchors.top: parent.top
                anchors.topMargin: 30
                text: translate.tr("Select ruin:")
                font.pointSize: style.fontSize
            }
            NTMapObjectSelectView {
                id: ruinSelectView
                anchors.fill: parent
                anchors.topMargin: 60
                model:  SelectRuinModel{}
                selectedId: conditionEditor.ruinId
                onItemSelected: itemId => {
                    conditionEditor.ruinId = itemId
                }
            }
        }
    }

    Component {
        id: allianceComponent
        Item{
            property int dialogWidth: 640
            property int dialogHeight: 510
            width: dialogWidth
            height: dialogHeight
            StyleManager
            {
                id: style
            }
            Text {
                id: name
                font.pointSize: style.titleFontSize
                anchors.top: parent.top
                anchors.horizontalCenter: parent.horizontalCenter
                text: translate.tr("ALLIANCE")
            }
            Text {
                id: label
                anchors.top: parent.top
                anchors.topMargin: 30
                anchors.left: parent.left
                text: translate.tr("Select player:")
                font.pointSize: style.fontSize
            }
            NTMapObjectSelectView {
                id: stackSelectView
                anchors.left: parent.left
                anchors.right: parent.horizontalCenter
                anchors.margins: 5
                anchors.leftMargin: -5
                anchors.top: label.bottom
                height: 445
                model:  SelectPlayerModel{}
                selectedId: conditionEditor.player1
                onItemSelected: itemId => {
                    conditionEditor.player1 = itemId
                }
            }
            Text {
                id: label2
                anchors.top: parent.top
                anchors.topMargin: 30
                anchors.left: parent.horizontalCenter
                text: translate.tr("Select player:")
                font.pointSize: style.fontSize
            }
            NTMapObjectSelectView {
                id: villageSelectView
                anchors.left: parent.horizontalCenter
                anchors.right: parent.right
                anchors.margins: 5
                anchors.rightMargin: -5
                anchors.top: label2.bottom
                height: 445
                model:  SelectPlayerModel{}
                selectedId: conditionEditor.player2
                onItemSelected: itemId => {
                    conditionEditor.player2 = itemId
                }
            }
        }
    }

    Component {
        id: diplomacyRelationsComponent
        Item{
            property int dialogWidth: 640
            property int dialogHeight: 510
            width: dialogWidth
            height: dialogHeight
            StyleManager
            {
                id: style
            }
            Text {
                id: name
                font.pointSize: style.titleFontSize
                anchors.top: parent.top
                anchors.horizontalCenter: parent.horizontalCenter
                text: translate.tr("DIPLOMACY_RELATIONS") + conditionEditor.diplomacy
            }
            ListView {
                height: 45
                anchors.top: parent.top
                anchors.topMargin: 35
                orientation: ListView.Horizontal
                anchors.horizontalCenter: parent.horizontalCenter
                width: (style.buttonWidht  * 1.8 + 5) * 3
                spacing: 5
                model: ListModel {
                    ListElement { text: "Pease"; value: 100 }
                    ListElement { text: "Neutral"; value: 49 }
                    ListElement { text: "War"; value: 0 }
                }
                delegate:  NTTextButton{
                    width: style.buttonWidht * 1.8
                    height: 45
                    text:  translate.tr(model.text)
                    font.pointSize: 14
                    onClicked: conditionEditor.diplomacy = model.value
                    border.visible: conditionEditor.diplomacy === model.value
                    border.width: 3
                    border.border.color: style.borderColor
                }
            }

            Text {
                id: label
                anchors.top: parent.top
                anchors.topMargin: 80
                anchors.left: parent.left
                text: translate.tr("Select player:")
                font.pointSize: style.fontSize
            }
            NTMapObjectSelectView {
                id: stackSelectView
                anchors.left: parent.left
                anchors.right: parent.horizontalCenter
                anchors.margins: 5
                anchors.leftMargin: -5
                anchors.top: label.bottom
                height: 445
                model:  SelectPlayerModel{}
                selectedId: conditionEditor.player1
                onItemSelected: itemId => {
                    conditionEditor.player1 = itemId
                }
            }
            Text {
                id: label2
                anchors.top: parent.top
                anchors.topMargin: 80
                anchors.left: parent.horizontalCenter
                text: translate.tr("Select player:")
                font.pointSize: style.fontSize
            }
            NTMapObjectSelectView {
                id: villageSelectView
                anchors.left: parent.horizontalCenter
                anchors.right: parent.right
                anchors.margins: 5
                anchors.rightMargin: -5
                anchors.top: label2.bottom
                height: 445
                model:  SelectPlayerModel{}
                selectedId: conditionEditor.player2
                onItemSelected: itemId => {
                    conditionEditor.player2 = itemId
                }
            }
        }
    }

    Component {
        id: transformingLandComponent
        Item{
            property int dialogWidth: 240
            property int dialogHeight: 90
            width: dialogWidth
            height: dialogHeight
            StyleManager
            {
                id: style
            }
            Text {
                id: name
                font.pointSize: style.titleFontSize
                anchors.top: parent.top
                anchors.horizontalCenter: parent.horizontalCenter
                text: translate.tr("TRANSFORMING_LAND")
            }
            Text {
                id: label
                anchors.top: parent.top
                anchors.topMargin: 30
                text: translate.tr("Select procent:")
                font.pointSize: style.fontSize
            }
            NTSpinBoxInt{
                height: 24
                anchors.top: label.bottom
                anchors.topMargin: 5
                anchors.horizontalCenter: parent.horizontalCenter
                width: 80
                image.width: 0
                min: 0
                max: 100
                value: conditionEditor.pctLand
                onValueModified: value=> conditionEditor.pctLand = value
            }
        }
    }

    Component {
        id: specificLeaderOwningItemComponent
        Item{
            property int dialogWidth: 940
            property int dialogHeight: 620
            width: dialogWidth
            height: dialogHeight
            StyleManager
            {
                id: style
            }
            Text {
                id: name
                font.pointSize: style.titleFontSize
                anchors.top: parent.top
                anchors.horizontalCenter: parent.horizontalCenter
                text: translate.tr("SPECIFIC_LEADER_OWNING_AN_ITEM")
            }
            Text {
                id: label
                anchors.top: parent.top
                anchors.topMargin: 30
                text: translate.tr("Select item:")
                font.pointSize: style.fontSize
            }
            ItemWidget
            {
                id: itemView
                itemId: conditionEditor.itemType
                anchors.top: label.bottom
                anchors.margins: 5
                anchors.left: selectItem.right
                height: 280
                width: 260
                DropArea {
                    id:dropArea
                    anchors.fill: parent
                    //anchors.margins: 10
                    Rectangle {
                        id: dragTargetArea
                        anchors.fill: parent
                        border.color:  "gold"
                        border.width: 3
                        visible: parent.containsDrag
                        color: "transparent"
                    }

                    onEntered: {

                    }
                    onExited: {

                    }
                    onDropped: drop=> {
                        if (drop.getDataAsString("mode") === "ItemAdd")
                        {
                            conditionEditor.itemType = drop.getDataAsString("id")
                        }
                    }
                }
            }
            SelectItemWidget
            {
                id: selectItem
                anchors.top: label.bottom
                anchors.left: parent.left
                width: 300
                height: 550
                anchors.margins: 5
                onItemDBLClicked: item=>{
                    conditionEditor.itemType = item
                }
            }
            Text {
                id: label2
                anchors.top: parent.top
                anchors.topMargin: 30
                anchors.left: itemView.right
                text: translate.tr("Select stack:")
                font.pointSize: style.fontSize
            }
            NTMapObjectSelectView {
                id: locSelectView
                anchors.left: itemView.right
                anchors.right: parent.right
                anchors.margins: 5
                anchors.rightMargin: -5
                anchors.top: label2.bottom
                height: 550
                model:  SelectStackModel{}
                selectedId: conditionEditor.stackId
                onItemSelected: itemId => {
                    conditionEditor.stackId = itemId
                }
            }
        }
    }

    Component {
        id: compareVarComponent
        Item{
            property int dialogWidth: 720
            property int dialogHeight: 510
            width: dialogWidth
            height: dialogHeight
            StyleManager
            {
                id: style
            }
            Text {
                id: name
                font.pointSize: style.titleFontSize
                anchors.top: parent.top
                anchors.horizontalCenter: parent.horizontalCenter
                text: translate.tr("COMPARE_VAR")
            }
            Text {
                id: label
                anchors.top: parent.top
                anchors.topMargin: 30
                anchors.left: parent.left
                text: translate.tr("Select player:")
                font.pointSize: style.fontSize
            }
            NTMapObjectSelectView {
                id: stackSelectView
                anchors.left: parent.left
                anchors.right: parent.horizontalCenter
                anchors.margins: 5
                anchors.leftMargin: -5
                anchors.rightMargin: 80
                anchors.top: label.bottom
                height: 445
                model:  SelectVariableModel{}
                selectedId: conditionEditor.var1
                onItemSelected: itemId => {
                    conditionEditor.var1 = itemId
                }
            }
            ListView {
                id: listView
                height: 400
                anchors.top: label.bottom
                anchors.topMargin: 5
                orientation: ListView.Vertical
                anchors.horizontalCenter: parent.horizontalCenter
                width: (style.buttonWidht * 1.4 + 5)
                spacing: 5
                model: ListModel {
                    ListElement { text: "Equal"; value: 0 }
                    ListElement { text: "Not equal"; value: 1 }
                    ListElement { text: "Greater"; value: 2 }
                    ListElement { text: "Greater<br>or<br>equal"; value: 3 }
                    ListElement { text: "Less"; value: 4 }
                    ListElement { text: "Less<br>or<br>equal"; value: 5 }
                }
                delegate:  NTTextButton{
                    width: style.buttonWidht * 1.4
                    height: 80
                    text:  translate.tr(model.text)
                    font.pointSize: 14
                    onClicked: conditionEditor.cmp = model.value
                    border.visible: conditionEditor.cmp === model.value
                    border.width: 3
                    border.border.color: style.borderColor
                }
            }
            Text {
                id: label2
                anchors.top: parent.top
                anchors.topMargin: 30
                anchors.left: parent.horizontalCenter
                text: translate.tr("Select player:")
                font.pointSize: style.fontSize
            }
            NTMapObjectSelectView {
                id: villageSelectView
                anchors.left: parent.horizontalCenter
                anchors.leftMargin: 80
                anchors.right: parent.right
                anchors.margins: 5
                anchors.rightMargin: -5
                anchors.top: label2.bottom
                height: 445
                model:  SelectVariableModel{}
                selectedId: conditionEditor.var2
                onItemSelected: itemId => {
                    conditionEditor.var2 = itemId
                }
            }
        }
    }

    Component {
        id: variableIsInRangeComponent
        Item{
            property int dialogWidth: 720
            property int dialogHeight: 510
            width: dialogWidth
            height: dialogHeight
            StyleManager
            {
                id: style
            }
            Text {
                id: name
                font.pointSize: style.titleFontSize
                anchors.top: parent.top
                anchors.horizontalCenter: parent.horizontalCenter
                text: translate.tr("VARIABLE_IS_IN_RANGE")
            }
            Text {
                id: label
                anchors.top: parent.top
                anchors.topMargin: 30
                anchors.left: parent.left
                text: translate.tr("Select variable:")
                font.pointSize: style.fontSize
            }
            NTMapObjectSelectView {
                id: stackSelectView
                anchors.left: parent.left
                anchors.right: parent.horizontalCenter
                anchors.margins: 5
                anchors.leftMargin: -5
                anchors.rightMargin: 80
                anchors.top: label.bottom
                height: 415
                model:  SelectVariableModel{}
                selectedId: conditionEditor.var1
                onItemSelected: itemId => {
                    conditionEditor.var1 = itemId
                }
            }
            RowLayout{
                anchors.top: stackSelectView.bottom
                anchors.topMargin: 5
                anchors.left: stackSelectView.left
                anchors.right: stackSelectView.right
                Text {
                    width: 40
                    text: translate.tr("Min:")
                    font.pointSize: style.fontSize
                }
                NTSpinBoxInt {
                    backgroundRect.visible: true
                    image.width: 0
                    height: 24
                    width: 80
                    min: 0
                    max: 999
                    value: conditionEditor.miscInt2
                    onValueModified: (value)=>{conditionEditor.miscInt2 = value}
                }
                Text {
                    width: 40
                    text: translate.tr("Max:")
                    font.pointSize: style.fontSize
                }
                NTSpinBoxInt {
                    backgroundRect.visible: true
                    image.width: 0
                    height: 24
                    width: 80
                    min: 0
                    max: 999
                    value: conditionEditor.miscInt3
                    onValueModified: (value)=>{conditionEditor.miscInt3 = value}
                }
            }

            ListView {
                id: listView
                height: 400
                anchors.top: label.bottom
                anchors.topMargin: 5
                orientation: ListView.Vertical
                anchors.horizontalCenter: parent.horizontalCenter
                width: (style.buttonWidht * 1.4)
                spacing: 5
                model: ListModel {
                    ListElement { text: "Ignore"; value: 0 }
                    ListElement { text: "And"; value: 1 }
                    ListElement { text: "Or"; value: 2 }
                }
                delegate:  NTTextButton{
                    width: style.buttonWidht * 1.4
                    height: 80
                    text:  translate.tr(model.text)
                    font.pointSize: 14
                    onClicked: conditionEditor.miscInt7 = model.value
                    border.visible: conditionEditor.miscInt7 === model.value
                    border.width: 3
                    border.border.color: style.borderColor
                }
            }
            Text {
                id: label2
                anchors.top: parent.top
                anchors.topMargin: 30
                anchors.left: parent.horizontalCenter
                anchors.leftMargin: 80
                text: translate.tr("Select variable:")
                font.pointSize: style.fontSize
            }
            NTMapObjectSelectView {
                id: villageSelectView
                anchors.left: parent.horizontalCenter
                anchors.leftMargin: 80
                anchors.right: parent.right
                anchors.margins: 5
                anchors.rightMargin: -5
                anchors.top: label2.bottom
                height: 415
                model:  SelectVariableModel{}
                selectedId: conditionEditor.var2
                onItemSelected: itemId => {
                    conditionEditor.var2 = itemId
                }
            }
            RowLayout{
                anchors.top: villageSelectView.bottom
                anchors.topMargin: 5
                anchors.left: villageSelectView.left
                anchors.right: villageSelectView.right
                Text {
                    width: 40
                    text: translate.tr("Min:")
                    font.pointSize: style.fontSize
                }
                NTSpinBoxInt {
                    backgroundRect.visible: true
                    image.width: 0
                    height: 24
                    width: 80
                    min: 0
                    max: 999
                    value: conditionEditor.miscInt5
                    onValueModified: (value)=>{conditionEditor.miscInt5 = value}
                }
                Text {
                    width: 40
                    text: translate.tr("Max:")
                    font.pointSize: style.fontSize
                }
                NTSpinBoxInt {
                    backgroundRect.visible: true
                    image.width: 0
                    height: 24
                    width: 80
                    min: 0
                    max: 999
                    value: conditionEditor.miscInt6
                    onValueModified: (value)=>{conditionEditor.miscInt6 = value}
                }
            }
        }
    }

    Component {
        id: customScriptComponent
        Item{
            property int dialogWidth: 840
            property int dialogHeight: 640
            width: dialogWidth
            height: dialogHeight
            StyleManager
            {
                id: style
            }
            Text {
                id: name
                font.pointSize: style.titleFontSize
                anchors.top: parent.top
                anchors.horizontalCenter: parent.horizontalCenter
                text: translate.tr("CUSTOM_SCRIPT")
            }
            Text {
                id: label
                anchors.top: name.bottom
                anchors.margins: 5
                anchors.left: parent.left
                text: translate.tr("Description:")
                font.pointSize: style.fontSize
            }
            TextField {
                id: nameEdit
                anchors.left: label.right
                anchors.verticalCenter: label.verticalCenter
                anchors.right: codeEditor.right
                anchors.leftMargin: 5
                height: 30
                font.pointSize: style.fontSize
                color: "black"
                text: conditionEditor.descr
                horizontalAlignment: TextInput.AlignHCenter
                selectByMouse: true
                onTextChanged: {
                    if (conditionEditor.descr !== nameEdit.text)
                        conditionEditor.descr = nameEdit.text;
                }
            }
            NTAutocompleteTextEdit{
                id: codeEditor
                anchors.top: parent.top
                anchors.topMargin: 60
                anchors.bottom: parent.bottom
                anchors.bottomMargin: 0
                anchors.left: parent.left
                width: 540
                editor.text: conditionEditor.code
                editor.onTextChanged: {
                    if (conditionEditor.code !== editor.text)
                        conditionEditor.code = editor.text
                }
            }
            SelectStackModel {id: selectStack}
            SelectFortModel {id: selectVillage}
            SelectRuinModel {id: selectRuin}
            SelectLocationModel{id: selectLocation}
            RowLayout{
                anchors.bottom: idSelectView.top
                anchors.right: idSelectView.right
                anchors.left: idSelectView.left
                height: 30
                NTImageButton{
                    id: selectStackButton
                    width: style.iconSize
                    height: style.iconSize
                    source: "image://provider/StackTool"
                    onClicked: {
                        idSelectView.model = selectStack
                    }
                    tooltip: "Select stack"
                }
                NTImageButton{
                    id: selectVillageButton
                    width: style.iconSize
                    height: style.iconSize
                    source: "image://provider/VillageTool"
                    onClicked: {
                        idSelectView.model = selectVillage
                    }
                    tooltip: "Select village"
                }
                NTImageButton{
                    id: selectRuinButton
                    width: style.iconSize
                    height: style.iconSize
                    source: "image://provider/RuinTool"
                    onClicked: {
                        idSelectView.model = selectRuin
                    }
                    tooltip: "Select ruin"
                }
                NTImageButton{
                    id: selectLocationButton
                    width: style.iconSize
                    height: style.iconSize
                    source: "image://provider/RuinTool"
                    onClicked: {
                        idSelectView.model = selectLocation
                    }
                    tooltip: "Select location"
                }
            }

            NTMapObjectSelectView {
                id: idSelectView
                anchors.left: codeEditor.right
                anchors.right: parent.right
                anchors.margins: 5
                anchors.top: codeEditor.top
                anchors.bottom: parent.bottom
                anchors.bottomMargin: 0
                anchors.topMargin: 0
                model: selectStack
                onItemSelected: itemId => {
                    selectedId = itemId
                }
                onItemDBLClicked: itemId => {
                    codeEditor.insertTextAtCursor("'" + conditionEditor.d2Id(itemId) + "'")
                }
            }
        }
    }

    Button {
        id: applyButton
        text: qsTr("Apply")
        onClicked: apply()
        z: 1
        anchors.bottom: parent.bottom
        anchors.right: parent.right
        anchors.margins: -2
        width: style.buttonWidht
        height: style.buttonHeight
    }
    Button {
        text: qsTr("Cancel")
        onClicked: cancel()
        z: 1
        anchors.bottom: parent.bottom
        anchors.right: applyButton.left
        anchors.margins: -2
        anchors.rightMargin: 10
        width: style.buttonWidht
        height: style.buttonHeight
    }
}

import QtQuick 2.12
import QtQuick.Controls 2.14
import QMLSearchObjectTool 1.0
import Qt.labs.qmlmodels 1.0
import QtQuick.Layouts 1.15
import QMLOptionsModel 1.0
import QMLToolsProvider 1.0

Item {
    id: item

    property SearchObjectTool searchTool
    property ToolsProvider toolsProv

    function init(tool)
    {
        toolsProv = tool;
        searchTool = toolsProv.currentToolObj();
        listView.model = searchTool
        textFilter.text = searchTool.filter;
    }

    TextFilterItem {
        id: textFilter
        anchors.left: parent.left
        anchors.right: parent.right
        anchors.top: name.bottom
        anchors.topMargin: 5
        height: 24
        onFilterChanged: {
            searchTool.filter = text
        }
        onSortModeChanged: {
            console.log("cliked - " + mode)
        }
        model: ListModel {
            id: nameModel
            ListElement { name: "Name" }
            ListElement { name: "Size" }
        }
    }

    Text {
        id: name
        height: 24
        horizontalAlignment: TextInput.AlignLeft
        verticalAlignment: TextInput.AlignVCenter
        font.pointSize: 14
        anchors.top: parent.top
        anchors.left: parent.left
        anchors.right: parent.right
        anchors.margins: 5
        text: "Search objects"
    }

    ImageButton{
        anchors.top: parent.top
        anchors.right: parent.right
        width: 24
        height: 24
        anchors.margins: 5
        source: "image://provider/Exit"
        onClicked:
        {
            toolsProv.closeTool();
        }
    }

    ListView{
        anchors.right: parent.right
        anchors.left: parent.left
        anchors.bottom: parent.bottom
        anchors.top: textFilter.bottom
        anchors.margins: 5
        anchors.topMargin: 5
        anchors.bottomMargin: 5
        spacing: 2
        clip: true
        orientation: Qt.Vertical
        id: listView

        model: 0
        delegate: Item {
            id: delegateItem
            width: listView.width
            height: 36
            Rectangle{
                anchors.fill: parent
                color: "gray"
                border.color: "black"
                border.width: 2
            }
            Text {
                id: posText
                anchors.left: parent.left
                anchors.verticalCenter: parent.verticalCenter
                text: "[" + ObjPos + "]"
            }
            Text {
                anchors.left: posText.right
                anchors.top: parent.top
                anchors.right: parent.right
                height: 16
                text: TargetName
            }
            Text {
                anchors.left: posText.right
                anchors.bottom: parent.bottom
                anchors.right: parent.right
                height: 16
                text: ObjName
            }
            MouseArea{
                anchors.fill: parent
                onClicked: searchTool.clicked(index)
                onDoubleClicked: searchTool.dblClicked(index)
            }
        }
    }
}

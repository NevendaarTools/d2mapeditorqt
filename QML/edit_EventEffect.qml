import QtQuick 2.0
import QtQuick 2.15
import QtQuick.Controls 2.15
import QMLStyleManager 1.0
import NevendaarTools 1.0
import NTModels 1.0
import NTComponents 1.0

Dialog {
    visible: true
    anchors.centerIn: parent
    modal: true
    id: dialog
    property bool acceptChanges : false
    StyleManager
    {
       id: style
    }
    EventEffectEditor{
        id: effectEditor
    }
    function setEventIdAndEffectNum(uid)
    {
        effectEditor.uid = uid
    }

    onClosed: {
        if (acceptChanges)
        {
            effectEditor.saveEvent()
        }
        else
        {
            //editor.cancel();
        }
    }


    function cancel()
    {
        acceptChanges = false;
        close()
    }
    function apply()
    {
        acceptChanges = true;
        close()
    }
    NTRectangle {
        id: background
        anchors.fill: parent
        anchors.margins: -12
    }
    Loader{
        id:loader
        anchors.fill: parent
        anchors.bottomMargin: 50
        sourceComponent: {
            switch (effectEditor.effectType) {
                case 0: return winLoose
                case 1: return createStack
                case 2: return castSpellOnTriggerer
                case 3: return castSpellOnLocation
                case 4: return changeStackOwner
                case 5: return moveStackToTriggerer
                case 6: return goBattle
                case 7: return disableEvent
                case 8: return giveSpell
                case 9: return giveItem
                case 10: return moveStackToLocation
                case 11: return allyPlayers
                case 12: return changeDiplomacy
                case 13: return changeFog
                case 14: return removeMountain
                case 15: return removeLandmark
                case 16: return changeScenarioText
                case 17: return popupMessage
                case 18: return changeStackOrder
                case 19: return destroyItem
                case 20: return removeStack
                case 21: return changeLandmark
                case 22: return changeTerrain
                case 23: return modifyVariable
                default: return null
            }
        }
        onLoaded: {
            dialog.width = loader.item.dialogWidth
            dialog.height = loader.item.dialogHeight + 40
        }
    }
    Component{
        id: changeTerrain
        Item{
            property int dialogWidth: 570
            property int dialogHeight: 540
            width: dialogWidth
            height: dialogHeight
            StyleManager
            {
                id: style
            }
            Text {
                id: name
                font.pointSize: style.titleFontSize
                anchors.top: parent.top
                anchors.horizontalCenter: parent.horizontalCenter
                text: translate.tr("CHANGE_TERRAIN")
            }
            Row{
                anchors.top: parent.top
                anchors.topMargin: 30
                anchors.horizontalCenter: parent.horizontalCenter
                Text {
                    id: label
                    // anchors.centerIn: parent
                    text: translate.tr("Enter size:")
                    font.pointSize: style.fontSize
                }
                Item{
                    width: 20
                    height: 10
                }
                NTSpinBoxInt {
                    id: sizeEdit
                    backgroundRect.visible: true
                    image.width: 0
                    height: 24
                    width: 100
                    min: 1
                    max:99
                    value: effectEditor.value
                    onValueModified: (value)=>{effectEditor.value = value}
                }
            }
            Text {
                id: label3
                anchors.top: parent.top
                anchors.topMargin: 60
                anchors.left: parent.left
                text: translate.tr("Terrain type:")
                font.pointSize: style.fontSize
            }
            ListView {
                id: listView
                anchors.top: parent.top
                anchors.topMargin: 90
                orientation: ListView.Vertical
                anchors.left: parent.left
                anchors.bottom: parent.bottom
                width: (style.buttonWidht * 1.4 + 5)
                spacing: 5
                model: ListModel {
                    ListElement { text: "Empire"; value: 1 }
                    ListElement { text: "Clans"; value: 2 }
                    ListElement { text: "Demons"; value: 3 }
                    ListElement { text: "Undeads"; value: 4 }
                    ListElement { text: "Neutrals"; value: 5 }
                    ListElement { text: "Elves"; value: 6 }
                }
                delegate:  NTTextButton{
                    width: style.buttonWidht * 2.4
                    height: 30
                    text:  translate.tr(model.text)
                    font.pointSize: 14
                    onClicked: effectEditor.lookup = model.value
                    border.visible: effectEditor.lookup === model.value
                    border.width: 3
                    border.border.color: style.borderColor
                }
            }

            Text {
                id: label2
                anchors.top: parent.top
                anchors.topMargin: 60
                anchors.left: parent.horizontalCenter
                text: translate.tr("Select location:")
                font.pointSize: style.fontSize
            }
            NTMapObjectSelectView {
                id: locationSelectView
                anchors.left: parent.horizontalCenter
                anchors.right: parent.right
                anchors.margins: 5
                anchors.rightMargin: -5
                anchors.top: label2.bottom
                height: 445
                model:  SelectLocationModel{}
                selectedId: effectEditor.locId
                onItemSelected: itemId => {
                    effectEditor.locId = itemId
                }
            }
        }
    }
    Component{
        id: allyPlayers
        Item{
            property int dialogWidth: 570
            property int dialogHeight: 540
            width: dialogWidth
            height: dialogHeight
            StyleManager
            {
                id: style
            }
            Text {
                id: name
                font.pointSize: style.titleFontSize
                anchors.top: parent.top
                anchors.horizontalCenter: parent.horizontalCenter
                text: translate.tr("ALLY_TWO_AI_PLAYERS")
            }
            NTCheckBox{
                anchors.top: parent.top
                anchors.topMargin: 30
                anchors.horizontalCenter: parent.horizontalCenter
                text: translate.tr("permally ally")
                checked: effectEditor.permally
                checkFont.pointSize: 23
                onClicked: (checked)=>{
                    if (effectEditor.permally !== checked)
                        effectEditor.permally = checked
                }
            }
            Text {
                id: label3
                anchors.top: parent.top
                anchors.topMargin: 60
                anchors.left: parent.left
                text: translate.tr("Select player:")
                font.pointSize: style.fontSize
            }
            Text {
                id: label2
                anchors.top: parent.top
                anchors.topMargin: 60
                anchors.left: parent.horizontalCenter
                text: translate.tr("Select player:")
                font.pointSize: style.fontSize
            }
            NTMapObjectSelectView {
                id: playerSelectView
                anchors.left: parent.left
                anchors.right: parent.horizontalCenter
                anchors.margins: 5
                anchors.leftMargin: -5
                anchors.top: label2.bottom
                height: 445
                model:  SelectPlayerModel{}
                selectedId: effectEditor.playerId1
                onItemSelected: itemId => {
                    effectEditor.playerId1 = itemId
                }
            }
            NTMapObjectSelectView {
                id: playerSelectView2
                anchors.left: parent.horizontalCenter
                anchors.right: parent.right
                anchors.margins: 5
                anchors.rightMargin: -5
                anchors.top: label2.bottom
                height: 445
                model:  SelectPlayerModel{}
                selectedId: effectEditor.playerId2
                onItemSelected: itemId => {
                    effectEditor.playerId2 = itemId
                }
            }
        }
    }
    Component{
        id: castSpellOnLocation
        Item{
            property int dialogWidth: 720
            property int dialogHeight: 540
            width: dialogWidth
            height: dialogHeight
            StyleManager
            {
                id: style
            }
            Text {
                id: name
                font.pointSize: style.titleFontSize
                anchors.top: parent.top
                anchors.horizontalCenter: parent.horizontalCenter
                text: translate.tr("CAST_SPELL_ON_LOCATION")
            }
            Text {
                id: label3
                anchors.top: parent.top
                anchors.topMargin: 60
                anchors.left: parent.left
                text: translate.tr("Select player:")
                font.pointSize: style.fontSize
            }
            Text {
                id: label2
                anchors.top: parent.top
                anchors.topMargin: 60
                anchors.left: parent.horizontalCenter
                text: translate.tr("Select spell:")
                font.pointSize: style.fontSize
            }
            Text {
                id: label1
                anchors.top: parent.top
                anchors.topMargin: 60
                anchors.left: parent.horizontalCenter
                text: translate.tr("Select location:")
                font.pointSize: style.fontSize
            }
            Row{
                anchors.top: parent.top
                anchors.topMargin: 30
                anchors.horizontalCenter: parent.horizontalCenter
                NTMapObjectSelectView {
                    id: playerSelectView
                    height: 445
                    width: 240
                    model:  SelectPlayerModel{}
                    selectedId: effectEditor.playerId1
                    onItemSelected: itemId => {
                        effectEditor.playerId1 = itemId
                    }
                }
                NTMapObjectSelectView {
                    id: spellSelectView
                    height: 445
                    width: 240
                    model:  SelectSpellModel{}
                    selectedId: effectEditor.spellType
                    onItemSelected: itemId => {
                                        console.log("#" + effectEditor.spellType)
                        effectEditor.spellType = itemId
                                        console.log("#" + effectEditor.spellType)
                    }
                }
                NTMapObjectSelectView {
                    id: locationSelectView
                    height: 445
                    width: 240
                    model:  SelectLocationModel{}
                    selectedId: effectEditor.locId
                    onItemSelected: itemId => {
                        effectEditor.locId = itemId
                    }
                }
            }
        }
    }
    Component{
        id: castSpellOnTriggerer
        Item{
            property int dialogWidth: 480
            property int dialogHeight: 540
            width: dialogWidth
            height: dialogHeight
            StyleManager
            {
                id: style
            }
            Text {
                id: name
                font.pointSize: style.titleFontSize
                anchors.top: parent.top
                anchors.horizontalCenter: parent.horizontalCenter
                text: translate.tr("CAST_SPELL_ON_TRIGGERER")
            }
            Text {
                id: label3
                anchors.top: parent.top
                anchors.topMargin: 60
                anchors.left: parent.left
                text: translate.tr("Select player:")
                font.pointSize: style.fontSize
            }
            Text {
                id: label2
                anchors.top: parent.top
                anchors.topMargin: 60
                anchors.left: parent.horizontalCenter
                text: translate.tr("Select spell:")
                font.pointSize: style.fontSize
            }
            Row{
                anchors.top: parent.top
                anchors.topMargin: 30
                anchors.horizontalCenter: parent.horizontalCenter
                NTMapObjectSelectView {
                    id: playerSelectView
                    height: 445
                    width: 240
                    model:  SelectPlayerModel{}
                    selectedId: effectEditor.playerId1
                    onItemSelected: itemId => {
                        effectEditor.playerId1 = itemId
                    }
                }
                NTMapObjectSelectView {
                    id: spellSelectView
                    height: 445
                    width: 240
                    model:  SelectSpellModel{}
                    selectedId: effectEditor.spellType
                    onItemSelected: itemId => {
                        effectEditor.spellType = itemId
                    }
                }
            }
        }
    }
    Component{
        id: changeLandmark
        Item{
            property int dialogWidth: 480
            property int dialogHeight: 540
            width: dialogWidth
            height: dialogHeight
            StyleManager
            {
                id: style
            }
            Text {
                id: name
                font.pointSize: style.titleFontSize
                anchors.top: parent.top
                anchors.horizontalCenter: parent.horizontalCenter
                text: translate.tr("CHANGE_LANDMARK")
            }
            Text {
                id: label3
                anchors.top: parent.top
                anchors.topMargin: 60
                anchors.left: parent.left
                text: translate.tr("Select landmark:")
                font.pointSize: style.fontSize
            }
            Text {
                id: label2
                anchors.top: parent.top
                anchors.topMargin: 60
                anchors.left: parent.horizontalCenter
                text: translate.tr("Select landmark:")
                font.pointSize: style.fontSize
            }
            Row{
                anchors.top: parent.top
                anchors.topMargin: 30
                anchors.horizontalCenter: parent.horizontalCenter
                NTMapObjectSelectView {
                    id: playerSelectView
                    height: 445
                    width: 240
                    model:  SelectMapLandmarkModel{}
                    selectedId: effectEditor.lmarkId
                    onItemSelected: itemId => {
                        effectEditor.lmarkId = itemId
                    }
                }
                NTMapObjectSelectView {
                    id: spellSelectView
                    height: 445
                    width: 240
                    model:  SelectLMarkModel{lmSize: effectEditor.value}
                    selectedId: effectEditor.lmarkType
                    onItemSelected: itemId => {
                        effectEditor.lmarkType = itemId
                    }
                }
            }
        }
    }
    Component{
        id: changeDiplomacy
        Item{
            property int dialogWidth: 480
            property int dialogHeight: 540
            width: dialogWidth
            height: dialogHeight
            StyleManager
            {
                id: style
            }
            Text {
                id: name
                font.pointSize: style.titleFontSize
                anchors.top: parent.top
                anchors.horizontalCenter: parent.horizontalCenter
                text: translate.tr("CHANGE_DIPLOMACY")
            }
            ListView {
                height: 40
                anchors.top: name.bottom
                anchors.topMargin: 5
                orientation: ListView.Horizontal
                // anchors.left: parent.left
                // anchors.right: parent.right
                anchors.horizontalCenter: parent.horizontalCenter
                width: style.buttonWidht * 1.5 * 4
                spacing: 5
                model: ListModel {
                    ListElement { text: "Pease"; value: 100 }//100 false мир
                    ListElement { text: "Neutral"; value: 49 } //49 false нейтралитет
                    ListElement { text: "War"; value: 0 } // 0 false война
                    ListElement { text: "Always war"; value: 10000 }//0 - true постоянная война
                }
                delegate:  NTTextButton{
                    width: style.buttonWidht * 1.4
                    height: 32
                    text:  translate.tr(model.text)
                    font.pointSize: 14
                    onClicked: effectEditor.diplomacy = model.value
                    border.visible: effectEditor.diplomacy === model.value
                    border.width: 3
                    border.border.color: style.borderColor
                }
            }
            Row{
                anchors.top: parent.top
                anchors.topMargin: 90
                anchors.horizontalCenter: parent.horizontalCenter
                NTMapObjectSelectView {
                    id: playerSelectView
                    height: 445
                    width: 240
                    model:  SelectPlayerModel{}
                    selectedId: effectEditor.playerId1
                    onItemSelected: itemId => {
                        effectEditor.playerId1 = itemId
                    }
                }
                NTMapObjectSelectView {
                    id: player2SelectView
                    height: 445
                    width: 240
                    model:  SelectPlayerModel{}
                    selectedId: effectEditor.playerId2
                    onItemSelected: itemId => {
                        effectEditor.playerId2 = itemId
                    }
                }
            }
        }
    }
    Component{
        id: changeScenarioText
        Item{
            property int dialogWidth: 480
            property int dialogHeight: 540
            width: dialogWidth
            height: dialogHeight
            StyleManager
            {
                id: style
            }
            Text {
                id: name
                font.pointSize: style.titleFontSize
                anchors.top: parent.top
                anchors.horizontalCenter: parent.horizontalCenter
                text: translate.tr("CHANGE_SCENARIO_OBJECTIVE_TEXT")
            }
            NTSpellCheckedTextEdit{
                id: descEdit
                anchors.fill: parent
                anchors.topMargin: 40
                anchors.bottomMargin: 40
                wrapMode: TextEdit.Wrap
                height: 120
                text: effectEditor.text
                onTextChanged: {effectEditor.text = text}
            }
        }
    }
    Component{
        id: changeStackOrder
        Item{
            property int dialogWidth: 580
            property int dialogHeight: 540
            width: dialogWidth
            height: dialogHeight
            StyleManager
            {
                id: style
            }
            Text {
                id: name
                font.pointSize: style.titleFontSize
                anchors.top: parent.top
                anchors.horizontalCenter: parent.horizontalCenter
                text: translate.tr("CHANGE_STACK_ORDER")
            }
            NTMapObjectSelectView {
                id: playerSelectView
                height: 445
                width: 240
                model:  SelectPlayerModel{}
                anchors.left: parent.left
                anchors.top: parent.top
                anchors.topMargin: 45
                selectedId: effectEditor.playerId1
                onItemSelected: itemId => {
                    effectEditor.playerId1 = itemId
                }
            }
            Item{
                StackOrderEditor{
                    id: orderEditor
                }

                anchors.left: priorityEdit.left
                anchors.bottom: parent.bottom
                anchors.right: priorityEdit.right
                anchors.top: priorityEdit.bottom
                anchors.margins: 0
                anchors.topMargin: 5
                anchors.rightMargin: 0
                anchors.leftMargin: 0
                OrderEditor {
                    id: orderRect
                    orderModel: stack.orderEditor
                    anchors.top: parent.top
                    anchors.left: parent.left
                    anchors.right: parent.right
                    height: 200
                    anchors.margins: 0
                    onOrderSelected: hasTarget => {
                        selectOrderTargetWidget.visible = hasTarget
                    }
                }

                SelectOrderTargetWidget {
                    anchors.right: parent.right
                    anchors.left: parent.left
                    anchors.margins: 0
                    anchors.top: orderRect.bottom
                    height: 400
                    id: selectOrderTargetWidget
                    target: stack.orderEditor.orderTarget
                    visible: false
                    onApply: {
                        stack.orderEditor.orderTarget = selectOrderTargetWidget.target
                    }
                }
            }
            ListView {
                height: 40
                anchors.bottom: parent.bottom
                anchors.bottomMargin: -50
                orientation: ListView.Horizontal
                // anchors.left: parent.left
                // anchors.right: parent.right
                anchors.horizontalCenter: parent.horizontalCenter
                width: style.buttonWidht * 1.5 * 4
                spacing: 5
                model: ListModel {
                    ListElement { text: "First only"; value: true }
                    ListElement { text: "All"; value: false }
                }
                delegate:  NTTextButton{
                    width: style.buttonWidht * 1.4
                    height: 32
                    text:  translate.tr(model.text)
                    font.pointSize: 14
                    onClicked: effectEditor.firstOnly = model.value
                    border.visible: effectEditor.firstOnly === model.value
                    border.width: 3
                    border.border.color: style.borderColor
                }
            }
        }
    }
    Component{
        id: changeStackOwner
        Text {
            id: name
            text: parent.id
        }
    }
    Component{
        id: createStack
        Item{
            property int dialogWidth: 640
            property int dialogHeight: 510
            width: dialogWidth
            height: dialogHeight
            StyleManager
            {
                id: style
            }
            Text {
                id: name
                font.pointSize: style.titleFontSize
                anchors.top: parent.top
                anchors.horizontalCenter: parent.horizontalCenter
                text: translate.tr("CREATE_STACK")
            }
            Text {
                id: label
                anchors.top: parent.top
                anchors.topMargin: 30
                anchors.left: parent.left
                text: translate.tr("Select template:")
                font.pointSize: style.fontSize
            }
            NTMapObjectSelectView {
                id: stackSelectView
                anchors.left: parent.left
                anchors.right: parent.horizontalCenter
                anchors.margins: 5
                anchors.leftMargin: -5
                anchors.top: label.bottom
                height: 445
                model:  SelectStackTemplateModel{}
                selectedId: effectEditor.templateId
                onItemSelected: itemId => {
                    effectEditor.templateId = itemId
                }
            }
            Text {
                id: label2
                anchors.top: parent.top
                anchors.topMargin: 30
                anchors.left: parent.horizontalCenter
                text: translate.tr("Select location:")
                font.pointSize: style.fontSize
            }
            NTMapObjectSelectView {
                id: villageSelectView
                anchors.left: parent.horizontalCenter
                anchors.right: parent.right
                anchors.margins: 5
                anchors.rightMargin: -5
                anchors.top: label2.bottom
                height: 445
                model:  SelectLocationModel{}
                selectedId: effectEditor.locId
                onItemSelected: itemId => {
                    effectEditor.locId = itemId
                }
            }
        }
    }
    Component{
        id: destroyItem
        Item{
            property int dialogWidth: 620
            property int dialogHeight: 620
            width: dialogWidth
            height: dialogHeight
            StyleManager
            {
                id: style
            }
            Text {
                id: name
                font.pointSize: style.titleFontSize
                anchors.top: parent.top
                anchors.horizontalCenter: parent.horizontalCenter
                text: translate.tr("DESTROY_ITEM")
            }
            Text {
                id: label
                anchors.top: parent.top
                anchors.topMargin: 30
                text: translate.tr("Select item:")
                font.pointSize: style.fontSize
            }
            ItemWidget
            {
                id: itemView
                itemId: effectEditor.itemType
                anchors.top: label.bottom
                anchors.left: selectItem.right
                height: 280
                anchors.right: parent.right
                DropArea {
                    id:dropArea
                    anchors.fill: parent
                    //anchors.margins: 10
                    Rectangle {
                        id: dragTargetArea
                        anchors.fill: parent
                        border.color:  "gold"
                        border.width: 3
                        visible: parent.containsDrag
                        color: "transparent"
                    }

                    onEntered: {

                    }
                    onExited: {

                    }
                    onDropped: drop=> {
                        if (drop.getDataAsString("mode") === "ItemAdd")
                        {
                            effectEditor.itemType = drop.getDataAsString("id")
                        }
                    }
                }
            }
            NTCheckBox{
                height: 60
                anchors.left: itemView.left
                anchors.right: itemView.right
                anchors.top: itemView.bottom
                text: translate.tr("DESTROY_ONLY_TRIGGERER_ITEMS")
                checked: effectEditor.triggerOnly
                checkFont.pointSize: 23
                onClicked: (checked)=>{
                    if (effectEditor.triggerOnly !== checked)
                        effectEditor.triggerOnly = checked
                }
            }

            SelectItemWidget
            {
                id: selectItem
                anchors.top: label.bottom
                anchors.left: parent.left
                width: 300
                height: 580
                anchors.margins: 5
                onItemDBLClicked: item=>{
                    conditionEditor.itemType = item
                }
            }
        }
    }
    Component{
        id: popupMessage
        Text {
            id: name
            text: parent.id
        }
    }
    Component{
        id: disableEvent
        Text {
            id: name
            text: parent.id
        }
    }
    Component{
        id: giveItem
        NTRectangle{
            property int dialogWidth: 620
            property int dialogHeight: 620
            width: dialogWidth
            height: dialogHeight

            Item {
                anchors.left: selectItem.right
                anchors.right: parent.right
                anchors.top: parent.top
                height: 30
                anchors.margins: 5
                Text {
                    id: name
                    anchors.centerIn: parent
                    font.pointSize: 24
                    text: "Give Item"
                }
                MouseArea{
                    anchors.fill: parent
                    onEntered: {
//                        overlay.showTooltip(dragTargetArea, "ItemWidget.qml")
                        overlay.showPreview(dragTargetArea, "ItemWidget.qml", effectEditor.itemType)
                    }
                    hoverEnabled: true
                    onExited: {
                        overlay.hidePreview()
//                        overlay.hideTooltip()
                    }
                }
            }
            SelectItemWidget
            {
                id: selectItem
                anchors.top: parent.top
                anchors.bottom: parent.bottom
                anchors.left: parent.left
                width: 300
                anchors.margins: 5
                onItemDBLClicked: {
                    effectEditor.itemType = item
                }
            }
            ItemWidget
            {
                id: itemView
                itemId: effectEditor.itemType
                anchors.verticalCenter: parent.verticalCenter
                anchors.left: parent.horizontalCenter
                DropArea {
                    id:dropArea
                    anchors.fill: parent
                    //anchors.margins: 10
                    Rectangle {
                        id: dragTargetArea
                        anchors.fill: parent
                        border.color:  "gold"
                        border.width: 3
                        visible: parent.containsDrag
                        color: "transparent"
                    }

                    onEntered: {

                    }
                    onExited: {

                    }
                    onDropped: drop=> {
                        if (drop.getDataAsString("mode") === "ItemAdd")
                        {
                            effectEditor.itemType = drop.getDataAsString("id")
                        }
                    }
                }
                ImageButton{
                    anchors.right: parent.right
                    anchors.top:  parent.top
                    width: 20
                    height: 20
                    source: "image://provider/Delete"
                    visible: effectEditor.itemType !== ""
                    onClicked: {
                        effectEditor.itemType = ""
                    }
                }
            }
        }
    }
    Component{
        id: giveSpell
        NTRectangle{
            property int dialogWidth: 620
            property int dialogHeight: 620
            width: dialogWidth
            height: dialogHeight

            Item {
                anchors.left: selectSpell.right
                anchors.right: parent.right
                anchors.top: parent.top
                height: 30
                anchors.margins: 5
                Text {
                    id: name
                    anchors.centerIn: parent
                    font.pointSize: 24
                    text: "Give Spell"
                }
            }
            SelectSpellWidget
            {
                id: selectSpell
                anchors.top: parent.top
                anchors.bottom: parent.bottom
                anchors.left: parent.left
                width: 300
                anchors.margins: 5
                onItemDBLClicked: (item)=>{
                    effectEditor.spellType = item
                }
            }
            SpellWidget
            {
                id: spellView
                anchors.top: parent.top
                anchors.topMargin: 60
                anchors.left: selectSpell.right
                height: 280
                anchors.right: parent.right
                spellId: effectEditor.spellType
                DropArea {
                    id:dropArea
                    anchors.fill: parent
                    //anchors.margins: 10
                    Rectangle {
                        id: dragTargetArea
                        anchors.fill: parent
                        border.color:  "gold"
                        border.width: 3
                        visible: parent.containsDrag
                        color: "transparent"
                    }

                    onEntered: {

                    }
                    onExited: {

                    }
                    onDropped: drop=> {
                        if (drop.getDataAsString("mode") === "SpellAdd")
                        {
                            effectEditor.spellType = drop.getDataAsString("id")
                        }
                    }
                }
            }

        }
    }
    Component{
        id: goBattle
        Text {
            id: name
            text: parent.id
        }
    }
    Component{
        id: modifyVariable
        Text {
            id: name
            text: parent.id
        }
    }
    Component{
        id: moveStackToTriggerer
        Text {
            id: name
            text: parent.id
        }
    }
    Component{
        id: moveStackToLocation
        NTRectangle{
            property int dialogWidth: 620
            property int dialogHeight: 620
            width: dialogWidth
            height: dialogHeight

            Item {
                anchors.left: mapObjectSelectView.right
                anchors.right: parent.right
                anchors.top: parent.top
                height: 30
                anchors.margins: 5
                Text {
                    id: name
                    anchors.centerIn: parent
                    font.pointSize: 24
                    text: "Move Stack to location"
                }
            }
            NTMapObjectSelectView {
                id: mapObjectSelectView
                anchors.bottom: parent.bottom
                anchors.left: parent.left
                anchors.top: parent.top
                width: 300
                model:  SelectStackOrTemplateModel{}
                selectedId: effectEditor.stackId
                onItemSelected: itemId => {
                    effectEditor.stackId = itemId
                }
            }
            NTMapObjectSelectView {
                id: locSelectView
                anchors.bottom: parent.bottom
                anchors.left: mapObjectSelectView.right
                anchors.top: parent.top
                width: 300
                model:  SelectLocationModel{}
                selectedId: effectEditor.locId
                onItemSelected: itemId => {
                    effectEditor.locId = itemId
                }
            }
        }
    }
    Component{
        id: removeLandmark
        Text {
            id: name
            text: parent.id
        }
    }
    Component{
        id: removeMountain
        NTRectangle{
            property int dialogWidth: 620
            property int dialogHeight: 620
            width: dialogWidth
            height: dialogHeight

            Item {
                anchors.left: mapObjectSelectView.right
                anchors.right: parent.right
                anchors.top: parent.top
                height: 30
                anchors.margins: 5
                Text {
                    id: name
                    anchors.centerIn: parent
                    font.pointSize: 24
                    text: translate.tr("REMOVE_MOUNTAINS_AROUND_LOCATION")
                }
            }
            NTMapObjectSelectView {
                id: mapObjectSelectView
                anchors.bottom: parent.bottom
                anchors.left: parent.left
                anchors.top: parent.top
                width: 300
                model:  SelectLocationModel{}
                selectedId: effectEditor.locId
                onItemSelected: itemId => {
                    effectEditor.locId = itemId
                }
            }
        }
    }
    Component{
        id: removeStack
        NTRectangle{
            property int dialogWidth: 620
            property int dialogHeight: 620
            width: dialogWidth
            height: dialogHeight

            Item {
                anchors.left: mapObjectSelectView.right
                anchors.right: parent.right
                anchors.top: parent.top
                height: 30
                anchors.margins: 5
                Text {
                    id: name
                    anchors.centerIn: parent
                    font.pointSize: 24
                    text: translate.tr("REMOVE_STACK")
                }
            }
            NTMapObjectSelectView {
                id: mapObjectSelectView
                anchors.bottom: parent.bottom
                anchors.left: parent.left
                anchors.top: parent.top
                width: 300
                model:  SelectStackOrTemplateModel{}
                selectedId: effectEditor.stackId
                onItemSelected: itemId => {
                    effectEditor.stackId = itemId
                }
            }
        }
    }
    Component{
        id: changeFog
        NTRectangle{
            property int dialogWidth: 620
            property int dialogHeight: 620
            width: dialogWidth
            height: dialogHeight
            Text {
                id: name
                anchors.horizontalCenter: parent.horizontalCenter
                anchors.top: parent.top
                anchors.topMargin: 10
                font.pointSize: style.titleFontSize
                text: translate.tr("CHANGE_FOG")
            }
            ListView {
                id: listView
                height: 32
                anchors.top: name.bottom
                anchors.topMargin: 10
                orientation: ListView.Horizontal
                anchors.left: mapObjectSelectView.left
                anchors.right: mapObjectSelectView.right
                spacing: 5
                model: ListModel {
                    ListElement { text: "Show fog"; value: true }
                    ListElement { text: "Hide fog"; value: false }
                }
                delegate:  NTTextButton{
                    width: style.buttonWidht * 1.4
                    height: 30
                    text:  translate.tr(model.text)
                    font.pointSize: 12
                    onClicked: effectEditor.enabled = model.value
                    border.visible: effectEditor.enabled === model.value
                    border.width: 3
                    border.border.color: style.borderColor
                }
            }
            Text {
                text: translate.tr("Location:")
                anchors.bottom: locationSelectView.top
                anchors.bottomMargin: 5
                anchors.left: locationSelectView.left
            }
            NTMapObjectSelectView {
                id: locationSelectView
                anchors.bottom: parent.bottom
                anchors.left: parent.left
                anchors.top: parent.top
                anchors.right: parent.horizontalCenter
                anchors.margins: 5
                anchors.topMargin: 70
                model:  SelectLocationModel{}
                selectedId: effectEditor.locId
                onItemSelected: itemId => {
                    effectEditor.locId = itemId
                }
            }
            NTMapObjectSelectView {
                id: mapObjectSelectView
                anchors.bottom: parent.bottom
                anchors.margins: 5
                anchors.topMargin: 120
                anchors.left: parent.horizontalCenter
                anchors.right: parent.right
                anchors.top: parent.top
                model:  SelectPlayerModel{}
                selectedId: effectEditor.playerId1
                onItemSelected: itemId => {
                    effectEditor.playerId1 = itemId
                }
            }
            Text {
                text: translate.tr("Radius:")
                anchors.verticalCenter: valueSpin.verticalCenter
                anchors.left: mapObjectSelectView.left
            }
            NTSpinBoxInt{
                id: valueSpin
                height: 24
                anchors.top: parent.top
                anchors.topMargin: 90
                anchors.horizontalCenter: mapObjectSelectView.horizontalCenter
                width: 80
                image.width: 0
                min: 0
                max: 24
                step: 1
                value: effectEditor.value
                onValueModified: value=> effectEditor.value = value
            }
            Text {
                text: (valueSpin.value * 2 + 1) + "x" + (valueSpin.value * 2 + 1)
                width: 80
                anchors.verticalCenter: valueSpin.verticalCenter
                anchors.right: parent.right
            }
        }
    }
    Component{
        id: winLoose
        NTRectangle{
            property int dialogWidth: 620
            property int dialogHeight: 620
            width: dialogWidth
            height: dialogHeight
            Text {
                id: name
                anchors.horizontalCenter: parent.horizontalCenter
                anchors.top: parent.top
                font.pointSize: style.titleFontSize
                text: "winLoose"
            }
            ListView {
                id: listView
                height: 48
                anchors.top: name.bottom
                anchors.topMargin: 5
                orientation: ListView.Horizontal
                anchors.horizontalCenter: parent.horizontalCenter
                width: 170
                spacing: 5
                model: ListModel {
                    ListElement { text: "Win"; value: true }
                    ListElement { text: "Loose"; value: false }
                }
                delegate:  NTTextButton{
                    width: style.buttonWidht * 1.4
                    height: 40
                    text:  translate.tr(model.text)
                    font.pointSize: 14
                    onClicked: effectEditor.win = model.value
                    border.visible: effectEditor.win === model.value
                    border.width: 3
                    border.border.color: style.borderColor
                }
            }
            NTMapObjectSelectView {
                id: mapObjectSelectView
                anchors.bottom: parent.bottom
                anchors.bottomMargin: 60
                anchors.horizontalCenter: parent.horizontalCenter
                anchors.top: listView.bottom
                visible: effectEditor.win === true
                width: 300
                model:  SelectPlayerModel{}
                selectedId: effectEditor.playerId1
                onItemSelected: itemId => {
                    effectEditor.playerId1 = itemId
                }
            }
        }
    }
    Button {
        id: applyButton
        text: qsTr("Apply")
        onClicked: apply()
        z: 1
        anchors.bottom: parent.bottom
        anchors.right: parent.right
        anchors.margins: -2
        width: style.buttonWidht
        height: style.buttonHeight
    }
    Button {
        text: qsTr("Cancel")
        onClicked: cancel()
        z: 1
        anchors.bottom: parent.bottom
        anchors.right: applyButton.left
        anchors.margins: -2
        anchors.rightMargin: 10
        width: style.buttonWidht
        height: style.buttonHeight
    }
}

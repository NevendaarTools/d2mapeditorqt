import QtQuick 2.15
import QtQuick.Dialogs
import QtQuick.Controls 2.15
import QMLStyleManager 1.0

Dialog {
    id: textInputDialog
    title: "Введите текст"
    modal: true
    visible: false
    signal textAccepted(string value)
    signal cancelled()
    property alias text: nameEdit.text
    StyleManager
    {
        id: style
    }
    onOpened: {
        nameEdit.focus = true
        nameEdit.forceActiveFocus()
    }

    TextField {
        id: nameEdit
        anchors.left: parent.left
        anchors.top: parent.top
        anchors.right: parent.right
        height: 35
        font.pointSize: style.fontSize
        color: "black"
        text: ""
        horizontalAlignment: TextInput.AlignHCenter
        selectByMouse: true
        Keys.onReturnPressed: {
            textInputDialog.textAccepted(nameEdit.text)
            textInputDialog.close();
        }
    }

    Button {
        text: translate.tr("OK")
        anchors.bottom: parent.bottom
        anchors.left: parent.left
        width: style.buttonWidht
        height: style.buttonHeight
        onClicked: {
            textInputDialog.textAccepted(nameEdit.text)
            textInputDialog.close();
        }
    }

    Button {
        anchors.bottom: parent.bottom
        anchors.right: parent.right
        text: translate.tr("Cancel")
        width: style.buttonWidht
        height: style.buttonHeight
        onClicked: ()=> {
            textInputDialog.cancelled()
            textInputDialog.close();
        }
    }
}

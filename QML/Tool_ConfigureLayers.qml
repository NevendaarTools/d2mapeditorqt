import QtQuick 2.12
import QtQuick.Controls 2.14
import QMLLayersTool 1.0
import Qt.labs.qmlmodels 1.0
import QtQuick.Layouts 1.15
import QMLOptionsModel 1.0
import QMLToolsProvider 1.0

Item {
    id: item

    property LayersTool layersTool
    property ToolsProvider toolsProv

    function init(tool)
    {
        toolsProv = tool;
        layersTool = toolsProv.currentToolObj();
        listView.settingsModel = layersTool.options
    }

    Text {
        id: name
        height: 24
        horizontalAlignment: TextInput.AlignLeft
        verticalAlignment: TextInput.AlignVCenter
        font.pointSize: 14
        anchors.top: parent.top
        anchors.left: parent.left
        anchors.right: parent.right
        anchors.margins: 5
        text: "Layers settings"
    }

    ImageButton{
        anchors.top: parent.top
        anchors.right: parent.right
        width: 24
        height: 24
        anchors.margins: 5
        source: "image://provider/Exit"
        onClicked:
        {
            toolsProv.closeTool();
        }
    }

    OptionsList {
        id: listView
        anchors.bottom: parent.bottom
        anchors.bottomMargin: 5
        anchors.left: parent.left
        anchors.margins: 5
        anchors.right: parent.right
        anchors.top: name.bottom
        anchors.topMargin: 5
        clip: true
    }
}

import QtQuick 2.15
import QtQuick.Controls 2.15
import QMLQuestLineEditor 1.0
import QMLQuestStageEditor 1.0
import Qt.labs.qmlmodels 1.0
import QtQuick.Layouts 1.15
import QtQuick.Dialogs
import QMLStyleManager 1.0

NTEditorWindow {
    id: item
    StyleManager
    {
        id: style
    }
    Component {
        id: questLineDelegate
        NTRectangle {
            width: questLineList.width
            height: 30

            Rectangle {
                color: "green"
                border.color: "green"
                border.width: 2
                anchors.fill: parent
                opacity: 0.2
                visible: Id === quest.selectedId
            }

            Text {
                anchors.centerIn: parent
                text: Name + " " + Id
            }

            MouseArea {
                anchors.fill: parent
                onClicked: quest.selectedId = Id
            }

        }
    }
    NTRectangle {
        id: questLineList
        anchors.left: parent.left
        anchors.top: parent.top
        anchors.bottom: questLineUtilityList.top
        width: 200
        Text {
            height: 32
            anchors.left: parent.left
            anchors.top: parent.top
            width: parent.width - 32
            anchors.margins: 5
            horizontalAlignment: TextInput.AlignHCenter
            verticalAlignment: TextInput.AlignVCenter
            font.pointSize: style.titleFontSize
            text: qsTr("Quest lines:")
        }
        ImageButton{
            NTTextInputDialog {
                id: nameDialog
                width: 240
                height: 130
                title: translate.tr("Quest line name:")
                onTextAccepted: value => {
                    var uid = quest.addQuestLine(value)
                    quest.selectedId = uid
                    item.focus = true
                    item.forceActiveFocus()
                }
                onCancelled: {
                    item.focus = true
                    item.forceActiveFocus()
                }
            }
            height: 32
            width: 32
            anchors.right: parent.right
            anchors.top: parent.top
            anchors.margins: 5
            source: "image://provider/Add"
            onClicked: {
                nameDialog.text = ""
                nameDialog.open()
            }
        }

        ListView {
            id: listViewQuestLinebase
            anchors.fill: parent
            anchors.margins: 5
            anchors.topMargin: 40
            //interactive: false
            //maximumFlickVelocity : 0
            spacing: 5
            model: quest.baseQuests
            clip: true
            delegate: questLineDelegate
        }
    }
    NTRectangle {
        id: questLineUtilityList
        anchors.left: parent.left
        anchors.bottom: parent.bottom
        height: 140
        width: 200
        ListView {
            id: listViewQuestLineUtil
            anchors.fill: parent
            anchors.margins: 5
            interactive: false
            spacing: 5
            model: quest.utilQuests
            clip: true
            delegate: questLineDelegate
        }
    }
    NTRectangle {
        anchors.left: questLineList.right
        anchors.top: parent.top
        anchors.right: parent.right
        anchors.bottom: parent.bottom
        Rectangle {
            id: questLineView
            color: "gray"
            border.color: "black"
            border.width: 2
            anchors.left: parent.left
            anchors.top: parent.top
            width: 700
            height: 40
            anchors.margins: 5
            Text {
                id: lineLabel
                text: translate.tr("Quest line:")
                anchors.left: parent.left
                anchors.top: parent.top
                horizontalAlignment: TextInput.AlignHCenter
                verticalAlignment: TextInput.AlignVCenter
                font.pointSize: 14
                anchors.margins: 5
                height: 30
                width: 140
            }
            TextField {
                id: questNameEdit
                anchors.left: lineLabel.right
                anchors.top: parent.top
                anchors.margins: 5
                width: 240
                height: 30
                font.pointSize: 13
                color: "black"
                text: quest.questLineName
                horizontalAlignment: TextInput.AlignHCenter
                selectByMouse: true
                onTextChanged: {
                    if (quest.questLineName !== text)
                        quest.questLineName = text
                }
            }
            NTCheckBox{
                anchors.verticalCenter: parent.verticalCenter
                anchors.left: questNameEdit.right
                anchors.right: parent.right
                //anchors.margins: 5
                checked: quest.stageToInfo
                text: translate.tr("display stage text in mission briefing")
                onClicked: (checked)=>{
                    quest.stageToInfo = checked
                }
            }
        }
        NTRectangle {
            id: extraMenu
            anchors.top: parent.top
            anchors.right: parent.right
            anchors.margins: 5
            height: 40
            width: 275
            Button{
                anchors.verticalCenter: parent.verticalCenter
                anchors.left: parent.left
                anchors.margins: 5
                id: variablesButton
                width: 130
                height: 30
                text: translate.tr("Variables")
                onClicked: {
                    var component = Qt.createComponent("edit_Variables.qml");
                    if (component.status === Component.Ready) {
                        var msg = component.createObject(item,
                         {
                             //text: stageEditor.eventDesc(Id)
                         });
                        msg.width = item.width
                        msg.height = item.height
                        msg.x = 0
                        msg.y = 0
                        msg.init()
                    }
                }
            }
            Button{
                anchors.verticalCenter: parent.verticalCenter
                anchors.right: parent.right
                anchors.margins: 5
                id: templatesButton
                width: 130
                height: 30
                text: translate.tr("Templates")
                onClicked: {
                    var component = Qt.createComponent("edit_StackTemplates.qml");
                    if (component.status === Component.Ready) {
                        var msg = component.createObject(item,
                         {
                             //text: stageEditor.eventDesc(Id)
                         });
                        msg.width = item.width
                        msg.height = item.height
                        msg.x = 0
                        msg.y = 0
                        msg.init()
                    }
                }
            }
        }

        ListView {
            id: listViewStages
            anchors.fill: parent
            anchors.margins: 5
            anchors.topMargin: 45
            //interactive: false
            spacing: 5
            model: quest.stageCount
            clip: true
            orientation: ListView.Horizontal
            delegate: Rectangle{
                width: 280
                height: listViewStages.height
                color: "gray"
                border.color: "black"
                QuestStageEditor
                {
                    id:stageEditor
                    selectedId: quest.selectedId
                    stage: index
                }
                Item {
                    id: stageInfoArea
                    anchors.top: parent.top
                    anchors.left: parent.left
                    anchors.right: parent.right
                    anchors.margins: 5
                    height: quest.stageToInfo? 200 : 70
                    Text {
                        id: stageLabel
                        text: translate.tr("Stage:") + index
                        anchors.left: parent.left
                        anchors.top: parent.top
                        anchors.right: parent.right
                        horizontalAlignment: TextInput.AlignHCenter
                        verticalAlignment: TextInput.AlignVCenter
                        font.pointSize: 14
                        height: 30
                    }
                    Text {
                        id: stageNameLabel
                        text: translate.tr("Name:")
                        anchors.left: parent.left
                        anchors.top: stageLabel.bottom
                        horizontalAlignment: TextInput.AlignHCenter
                        verticalAlignment: TextInput.AlignVCenter
                        font.pointSize: 14
                        height: 30
                        width: 70
                    }
                    TextField {
                        id: nameEdit
                        anchors.left: stageNameLabel.right
                        anchors.right: parent.right
                        anchors.top: stageLabel.bottom
                        height: 30
                        font.pointSize: 13
                        color: "black"
                        text: stageEditor.stageName
                        horizontalAlignment: TextInput.AlignHCenter
                        selectByMouse: true
                        onTextChanged: {
                            if (stageEditor.stageName !== text)
                                stageEditor.stageName = text
                        }
                    }
                    Text {
                        id: stageDescLabel
                        text: translate.tr("Text in mission briefing:")
                        anchors.left: parent.left
                        anchors.right: parent.right
                        anchors.top: stageNameLabel.bottom
                        horizontalAlignment: TextInput.AlignHCenter
                        verticalAlignment: TextInput.AlignVCenter
                        font.pointSize: 14
                        height: quest.stageToInfo? 30 : 0
                        visible: quest.stageToInfo
                        width: 70
                    }
                    TextField {
                        id: descEdit
                        anchors.left: parent.left
                        anchors.top: stageDescLabel.bottom
                        anchors.bottom: parent.bottom
                        anchors.right: parent.right
                        font.pointSize: 13
                        visible: quest.stageToInfo
                        color: "black"
                        text: stageEditor.stageDesc
                        horizontalAlignment: TextInput.AlignHCenter
                        selectByMouse: true
                        //enabled: displayCheckbox.checked
                        onTextChanged: {
                            if (stageEditor.stageDesc !== text)
                                stageEditor.stageDesc = text
                        }
                    }
                }
                Rectangle {
                    id: stageQuestLineList
                    color: "gray"
                    border.color: "black"
                    border.width: 2
                    anchors.top: stageInfoArea.bottom
                    anchors.left: parent.left
                    anchors.right: parent.right
                    anchors.bottom: parent.bottom
                    FilterAreaWidget {
                        id: filterArea
                        anchors.top: parent.top
                        anchors.topMargin: 5
                        height: 32
                        width: parent.width - 40
                        x:5
                        openToRight: false
                        tagListHeight: 160
                        sortVisible: false
                        settingsModel: stageEditor.stageQuests.filterSettings
                    }
                    ImageButton{
                        height: 32
                        width: 32
                        anchors.top: parent.top
                        anchors.topMargin: 5
                        anchors.left: filterArea.right
                        source: "image://provider/Add"
                        onClicked: {
                            var newId = stageEditor.createEvent();
                            console.info(newId);
                            var component = Qt.createComponent("edit_Event.qml");
                            if (component.status === Component.Ready) {
                                var msg = component.createObject(item,
                                                                 {
                                                                     //text: stageEditor.eventDesc(Id)
                                                                 });
                                msg.width = item.width
                                msg.height = item.height
                                msg.x = 0
                                msg.y = 0
                                msg.setEventUid(newId)
                            }
                        }
                    }
                    ListView {
                        id: listViewStageQuests
                        anchors.fill: parent
                        anchors.margins: 5
                        anchors.topMargin: 45
                        //interactive: false
                        spacing: 5
                        model: stageEditor.stageQuests
                        clip: true
                        delegate: Rectangle {
                            width: listViewStageQuests.width
                            height: 30

                            Rectangle {
                                id: hoverRect
                                color: "transparent"
                                border.color: "black"
                                border.width: 2
                                anchors.fill: parent
                                visible: false
                            }

                            Text {
                                anchors.centerIn: parent
                                text: Name + " " + index//Id
                            }

                            MouseArea {
                                id: dragArea
                                anchors.fill: parent
                                hoverEnabled: true
                                onEntered: {
                                    overlay.showTooltip(hoverRect, stageEditor.eventDesc(Id))
                                }
                                onExited: {
                                    overlay.hideTooltip()
                                }
                            }
                            ImageButton{
                                height: 24
                                width: 24
                                anchors.right: parent.right
                                anchors.verticalCenter: parent.verticalCenter
                                anchors.margins: 5
                                source: "image://provider/Settings"
                                onClicked: {
                                    var component = Qt.createComponent("edit_Event.qml");
                                    if (component.status === Component.Ready) {
                                        var msg = component.createObject(item,
                                                                         {
                                                                             //text: stageEditor.eventDesc(Id)
                                                                         });
                                        msg.width = item.width
                                        msg.height = item.height
                                        msg.x = 0
                                        msg.y = 0
                                        msg.setEventUid(Id)
                                    }
                                }
                            }
                        }

                    }
                }
            }
        }
    }
}

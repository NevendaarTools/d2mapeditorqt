import QtQuick 2.0
import QMLBaseObjectPreview 1.0
import QtQuick.Controls 2.14
import QtQuick.Layouts 1.15
import QMLTreasureEditor 1.0

Item{
    id: item
    property int baseWidth: 300
    anchors.fill: parent
    function setUID(uid)
    {
        basePreview.setUID(uid);
    }

    BasePreviewItem {
        id: basePreview
        width: 400
        //height: 400
    }
}


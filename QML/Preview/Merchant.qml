import QtQuick 2.0
import QMLBaseObjectPreview 1.0
import QMLMerchantEditor 1.0
import "../../QML"

Item{
    id: item
    property int baseWidth: 300
    function setUID(uid)
    {
        basePreview.setUID(uid);
        merchant.setUID(uid);
    }
    MerchantEditor
    {
        id:merchant
    }
    BasePreviewItem {
        id: basePreview
    }
    Item {
        anchors.left: parent.left
        anchors.leftMargin: -5
        anchors.top: parent.top
        anchors.topMargin: 95
        width: baseWidth
//        height: 450
        anchors.bottom: parent.bottom

        Loader {
            //anchors.fill: parent
            sourceComponent: {
                switch(merchant.typeId) {
                    case 0: return itemsDelegate
                    case 2: return spellsDelegate
                    case 1: return unitsDelegate
                    case 3: return Item
                    default: return Item;
                }
            }
        }

        Component {
            id: itemsDelegate
            InventoryView {
                id: inventory1Grid
                model: merchant.goods
                anchors.left: parent.left
                width: baseWidth
                height: prefferedHeight(merchant.goods.rowCount())
                anchors.top: parent.top
                anchors.topMargin: 6
            }
        }

        Component {
            id: spellsDelegate
            InventoryView {
                id: inventory1Grid
                model: merchant.spells
                anchors.left: parent.left
                width: baseWidth
                height: prefferedHeight(merchant.spells.rowCount())
                anchors.top: parent.top
                anchors.topMargin: 6
                delegate: Item {
                    id: deleg
                    width:  inventory1Grid.width
                    height: rowH

                    Rectangle
                    {
                        color: "grey"
                        border.color: "black"
                        border.width: 1
                        anchors.fill: parent
                    }
                    Item
                    {
                        id: iconRect
                        anchors.left: parent.left
                        anchors.verticalCenter: parent.verticalCenter
                        width: rowH
                    }

                    Image {
                        anchors.centerIn: iconRect
                        visible: Id !== ""
                        id: icon
                        scale: 0.25
                        source: "image://provider/" + Icon

                    }
                    Text {
                        id: name
                        lineHeight: 0.8
                        wrapMode: Text.Wrap
                        elide: Text.ElideRight
                        verticalAlignment: Text.AlignTop
                        horizontalAlignment: Text.AlignHCenter
                        text: Name
                        font.bold: true
                        anchors.left: iconRect.right
                        anchors.right: parent.right
                        anchors.rightMargin: 0
                        anchors.top: parent.top
                        anchors.bottom: parent.bottom
                        antialiasing: true
                    }
                    Text {
                        id: desc
                        lineHeight: 0.8
                        wrapMode: Text.Wrap
                        elide: Text.ElideRight
                        verticalAlignment: Text.AlignBottom
                        horizontalAlignment: Text.AlignHCenter
                        text: "\n["+ Desc + "]"
                        //font.bold: true
                        anchors.fill: name
                        antialiasing: true
                        font.pointSize: 7
                        anchors.bottomMargin: 1
                    }
                }
            }
        }

        Component {
            id: unitsDelegate
            InventoryView {
                id: inventory1Grid
                model: merchant.units
                anchors.left: parent.left
                width: baseWidth
                height: prefferedHeight(merchant.units.rowCount())
                anchors.top: parent.top
                anchors.topMargin: 6
                delegate:  Item {
                    id: deleg
                    width:  inventory1Grid.width
                    height: rowH

                    Rectangle
                    {
                        color: "grey"
                        border.color: "black"
                        border.width: 1
                        anchors.fill: parent
                    }
                    Item
                    {
                        id: iconRect
                        anchors.left: parent.left
                        anchors.verticalCenter: parent.verticalCenter
                        width: rowH
                    }

                    Image {
                        anchors.centerIn: iconRect
                        visible: Id !== ""
                        id: icon
                        scale: 0.25
                        source: "image://provider/" + Icon

                    }
                    Text {
                        id: name
                        lineHeight: 0.8
                        wrapMode: Text.Wrap
                        elide: Text.ElideRight
                        verticalAlignment: Text.AlignVCenter
                        horizontalAlignment: Text.AlignHCenter
                        text: Name + " [" + Level +"]"
                        font.bold: true
                        anchors.left: iconRect.right
                        anchors.right: parent.right
                        anchors.verticalCenter: parent.verticalCenter
                        anchors.rightMargin: 40
                        antialiasing: true
                    }
                    Text {
                        id: count
                        text: Count === 0? "" : Count
                        font.bold: true
                        anchors.verticalCenter: parent.verticalCenter
                        anchors.right: parent.right
                        horizontalAlignment: Text.AlignRight
                        verticalAlignment: Text.AlignVCenter
                        width: 35
                        anchors.rightMargin: 5
                        antialiasing: true
                    }
                }
            }
        }
    }
}

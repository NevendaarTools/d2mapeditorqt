import QtQuick 2.0
import QMLBaseObjectPreview 1.0
import QMLMapStackEditor 1.0
import "../../QML"

Item{
    id: item
    property int baseWidth: 300
    //anchors.fill: parent
    function setUID(uid)
    {
        basePreview.setUID(uid);
        stackEditor.setUID(uid);
        unitsView.reload();
        inventory1Grid.model = stackEditor.inventory;
        stackEditor.setUID(uid);
    }
    BasePreviewItem {
        id: basePreview
        //width: 400
        //height: 400
    }
    MapStackEditor
    {
        id:stackEditor
    }
    Item
    {
        id:unitsRect
        anchors.left: parent.left
        anchors.leftMargin: 2
        anchors.top: parent.top
        anchors.topMargin: 95
        width: 140
        height: 290
    }

    GroupView
    {
        id: unitsView
        parentItem: item
        garrison: stackEditor.stack
        isLeft: true
        isEditable: false
        anchors.centerIn: unitsRect
        width: 220
        height: 400
        scale: 0.7
    }

    InventoryView {
        id: inventory1Grid
        model: stackEditor.inventory
        anchors.left: parent.left
        anchors.leftMargin: -5
        width: baseWidth
        height: prefferedHeight(stackEditor.inventory.rowCount())
        anchors.top: unitsRect.bottom
        anchors.topMargin: 0//6
    }
    Component_EventsView {
        id: eventsView
        width: baseWidth
        anchors.top: inventory1Grid.bottom
        anchors.topMargin: 5
        anchors.left: parent.left
        anchors.leftMargin: -50
        parentItem: item
        height: 140
        previewMode: true
        objectUID: stackEditor.uid
        scale:0.7
    }
}



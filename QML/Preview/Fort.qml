import QtQuick 2.0
import QMLBaseObjectPreview 1.0
import QMLVillageEditor 1.0
import "../../QML"

Item{
    id: item
    property int rectOffset: 144
    function setUID(uid)
    {
        basePreview.setUID(uid);
        villageEditor.setUID(uid);
        unitsView1.reload();
        unitsView2.reload();
        rectOffset = 134//(villageEditor.fortType === 0)? 134:82
    }
    BasePreviewItem {
        id: basePreview
        width: 400
        //height: 400
    }
    VillageEditor
    {
        id:villageEditor
    }
    Item
    {
        id:unitsRect1
        anchors.left: parent.left
        anchors.leftMargin: 2
        anchors.top: parent.top
        anchors.topMargin: rectOffset
        width: 140
        height: 290
    }
    Item
    {
        id:unitsRect2
        anchors.left: unitsRect1.right
        anchors.top: unitsRect1.top
        anchors.topMargin: 0
        anchors.leftMargin: 90
        width: 140
        height: 290
    }

    GroupView
    {
        id: unitsView1
        parentItem: item
        isEditable: false
        garrison: villageEditor.visiter
        isLeft: true
        anchors.centerIn: unitsRect1
        width: 220
        height: 400
        scale: 0.7
    }

    GroupView
    {
        id: unitsView2
        isEditable: false
        garrison: villageEditor.garrison
        isLeft: false
        anchors.centerIn: unitsRect2
        width: 220
        height: 400
        scale: 0.7
    }
    InventoryView {
        id: inventory1Grid
        model: villageEditor.visiterInventory
        anchors.left: parent.left
        anchors.leftMargin: -5
        height: prefferedHeight(villageEditor.visiterInventory.rowCount())
        anchors.top: unitsRect1.bottom
        anchors.topMargin: 10
        anchors.rightMargin: 10
        anchors.right: unitsRect2.left
    }
    InventoryView {
        id: inventory2Grid
        model: villageEditor.garrisonInventory
        width: inventory1Grid.width
        height: prefferedHeight(villageEditor.garrisonInventory.rowCount())
        anchors.top: inventory1Grid.top
        anchors.leftMargin: 5
        anchors.left: inventory1Grid.right
    }
    Component_EventsView {
        id: eventsView1
        width: inventory1Grid.width
        anchors.top: inventory1Grid.bottom
        anchors.topMargin: 5
        anchors.left: inventory1Grid.left
        anchors.leftMargin: -50
        height: 140
        previewMode: true
        objectUID: villageEditor.stackUid
        parentItem: item
        scale:0.7
    }
    Component_EventsView {
        id: eventsView2
        width: inventory2Grid.width
        anchors.top: inventory2Grid.bottom
        anchors.topMargin: 5
        anchors.left: inventory2Grid.left
        anchors.leftMargin: -50
        height: 140
        previewMode: true
        objectUID: villageEditor.uid
        parentItem: item
        scale:0.7
    }
}



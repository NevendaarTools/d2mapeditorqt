import QtQuick 2.0
import QMLBaseObjectPreview 1.0
import "../../QML"

Item {
    id :basePreviewItem

    function setUID(uid)
    {
        basePreview.uid = uid;
    }

    BaseObjectPreview {
        id:basePreview
    }

    Rectangle{
        anchors.fill: textName
        anchors.margins:  -5
        color: "white"
        opacity: 0.8
        id: nameRect
        visible: basePreview.name!==""
        border.color: "black"
    }
    Text {
        anchors.topMargin: 0
        anchors.top: parent.top
        textFormat: Text.RichText
        smooth: true
        horizontalAlignment: Text.AlignHCenter
        verticalAlignment: Text.AlignVCenter
        id: textName
        text: basePreview.name
        wrapMode: Text.WordWrap
        width: 180
        x: 0
    }

    Rectangle{
        anchors.fill: textDesc
        anchors.margins:  -5
        color: "white"
        opacity: 0.8
        id: descRect
        visible: textDesc.visible
        border.color: "black"
    }

    Text {
        anchors.topMargin: 6
        anchors.top: nameRect.bottom
        id: textDesc
        textFormat: Text.RichText
        smooth: true
        horizontalAlignment: Text.AlignHCenter
        verticalAlignment: Text.AlignVCenter
        text: basePreview.desc
        wrapMode: Text.WordWrap
        width: 180
        x: 0
        visible: basePreview.desc!=="" && basePreview.desc !== basePreview.name
    }
}

import QtQuick 2.0
import QMLBaseObjectPreview 1.0
import QtQuick.Controls 2.14
import QMLRuinEditor 1.0
import "../../QML"

Item{
    id: item
    property int baseWidth: 300
    function setUID(uid)
    {
        basePreview.setUID(uid);
        ruinEditor.setUID(uid);
        unitsView.reload();
        itemWidget.itemId = ruinEditor.item;
        costView.value = ruinEditor.cost
    }
    BasePreviewItem {
        id: basePreview
        width: 400
        //height: 400
    }
    RuinEditor
    {
        id:ruinEditor
    }
    Item
    {
        id:unitsRect
        anchors.left: parent.left
        anchors.top: parent.top
        anchors.topMargin: 55
        anchors.leftMargin: 2
        width: 140
        height: 290
    }

    GroupView
    {
        id: unitsView
        parentItem: item
        garrison: ruinEditor.units
        isLeft: true
        isEditable: false
        anchors.centerIn: unitsRect
        width: 220
        height: 400
        scale: 0.7
    }
    Rectangle{
        anchors.fill: costView
        anchors.leftMargin: -5
        color: "grey"
        border.color: "black"
    }
    NTCostView {
        id: costView
        anchors.top: unitsRect.top
        anchors.left: unitsRect.right
        anchors.leftMargin: 15
        anchors.topMargin: 5
        width: 60
        height: 125
        value: view.cost
        orientation: Qt.Vertical
    }
    Item{
        id:itemRect
        anchors.top: unitsRect.bottom
        anchors.left: parent.left
        anchors.topMargin: 0
        anchors.leftMargin: -5
        width: 210
        height: 110
    }

    ItemWidget{
        id: itemWidget
        anchors.centerIn: itemRect
        width: baseWidth
        height: 160
        scale: 0.7
    }
    Component_EventsView {
        id: eventsView
        width: baseWidth
        anchors.top: itemRect.bottom
        anchors.topMargin: 5
        anchors.leftMargin: -50
        anchors.left: parent.left
        objectUID: ruinEditor.uid
        height: 140
        previewMode: true
        parentItem: item
        scale:0.7
    }
}

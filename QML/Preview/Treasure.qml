import QtQuick 2.0
import QMLBaseObjectPreview 1.0
import QtQuick.Controls 2.14
import QtQuick.Layouts 1.15
import QMLTreasureEditor 1.0
import "../../QML"

Item{
    id: item
    property int baseWidth: 300
    anchors.fill: parent
    function setUID(uid)
    {
        basePreview.setUID(uid);
        treasureEditor.setUID(uid);
        inventoryView.model = treasureEditor.items;
    }
    TreasureEditor
    {
        id:treasureEditor
    }

    BasePreviewItem {
        id: basePreview
        width: 400
        //height: 400
    }
    InventoryView {
        id: inventoryView
        anchors.left: parent.left
        anchors.top: parent.top
        anchors.topMargin: 50
        width: baseWidth
        height: prefferedHeight(treasureEditor.items.rowCount())
    }
}


import QtQuick 2.0
import QMLBaseObjectPreview 1.0
import NevendaarTools 1.0
import "../../QML"

Item{
    id: item
    property int baseWidth: 300
    //anchors.fill: parent
    function setUID(uid)
    {
        basePreview.uid = uid;
        stackEditor.setUID(uid);
        unitsView.reload();
        stackEditor.setUID(uid);
    }
    StackTemplateEditor
    {
        id:stackEditor
    }
    BaseObjectPreview {
        id:basePreview
    }
    width: 400
    height: 340

    Rectangle{
        anchors.fill: textName
        anchors.margins:  -5
        color: "white"
        opacity: 0.8
        id: nameRect
        visible: basePreview.name!==""
        border.color: "black"
    }
    Text {
        anchors.topMargin: 0
        anchors.top: parent.top
        textFormat: Text.RichText
        smooth: true
        horizontalAlignment: Text.AlignHCenter
        verticalAlignment: Text.AlignVCenter
        id: textName
        text: basePreview.name
        wrapMode: Text.WordWrap
        width: 180
        x: 0
    }

    Item
    {
        id:unitsRect
        anchors.left: parent.left
        anchors.leftMargin: 2
        anchors.top: nameRect.bottom
        anchors.topMargin: 5
        width: 140
        height: 290
    }

    GroupView
    {
        id: unitsView
        parentItem: item
        garrison: stackEditor.stack
        isLeft: true
        isEditable: false
        anchors.centerIn: unitsRect
        width: 220
        height: 400
        scale: 0.7
    }

    Component_EventsView {
        id: eventsView
        width: baseWidth
        anchors.top: unitsRect.top
        anchors.margins: 5
        anchors.left: unitsRect.right
        anchors.leftMargin: -30
        height: 140
        previewMode: true
        objectUID: stackEditor.uid
        scale:0.7
        parentItem: item
    }
}



import QtQuick 2.15
import QtQuick.Dialogs
import QtQuick.Controls 2.15
import QMLStyleManager 1.0

Dialog {
    id: twoButtonsDialog
    title: ""
    modal: true
    visible: false
    property alias text: nameEdit.text

    StyleManager
    {
        id: style
    }

    Keys.onReturnPressed:()=> {
        twoButtonsDialog.accept()
    }
    Keys.onEscapePressed:()=> {
        twoButtonsDialog.reject()
    }

    Text {
        id: nameEdit
        anchors.left: parent.left
        anchors.top: parent.top
        anchors.right: parent.right
        anchors.bottom: okButton.top
        font.pointSize: style.fontSize
        color: "black"
        text: ""
        wrapMode: Text.WordWrap
        horizontalAlignment: TextInput.AlignLeft
        verticalAlignment: TextInput.AlignVCenter
    }

    Button {
        id: okButton
        text: translate.tr("OK")
        anchors.bottom: parent.bottom
        anchors.left: parent.left
        width: style.buttonWidht
        height: style.buttonHeight
        onClicked: {
            twoButtonsDialog.accept()
        }
    }

    Button {
        anchors.bottom: parent.bottom
        anchors.right: parent.right
        text: translate.tr("Cancel")
        width: style.buttonWidht
        height: style.buttonHeight
        onClicked: ()=> {
            twoButtonsDialog.reject()
        }
    }
}

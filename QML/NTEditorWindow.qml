import QtQuick 2.15
import QtQuick.Controls 2.15
import Qt.labs.qmlmodels 1.0
import QtQuick.Layouts 1.15
import QtQuick.Dialogs
import QMLStyleManager 1.0

Rectangle {
    id: item
    property alias style: style
    property alias applyButton: applyButton
    property alias cancelButton: cancelButton

    focus:true
    function cancel()
    {
        if (editor.hasChanges())
        {
            //nameDialog.text = ""
            nameDialog.open()
        }
        else
            editor.cancel()
    }
    function apply()
    {
        var text = editor.validate()
        if (text !== "")
        {
            invalidDialog.text = text
            invalidDialog.open()
        }
        else
            editor.commit()
    }

    NTTwoButtonsDialog {
        id: nameDialog
        width: 240
        height: 130
        anchors.centerIn: parent
        text: translate.tr("Are you sure you want to undo the changes you've made?")
        onAccepted: {
            editor.cancel()
        }
        onRejected: {
            item.focus = true
            item.forceActiveFocus()
        }
    }

    NTInfoDialog {
        id: invalidDialog
        width: 240
        height: 130
        anchors.centerIn: parent
        title: translate.tr("Invalid state")
        text: ""
        onAccepted: {

        }
    }


    Keys.onEscapePressed: cancel()
    Keys.onReturnPressed: apply()

    anchors.centerIn: parent
    width: style.editorWidht
    height: style.editorHeight
    color: style.backColor
    border.color: style.borderColor
    border.width: style.borderWidht

    StyleManager
    {
        id: style
    }
    Button {
        id: applyButton
        text: qsTr("Apply")
        onClicked: apply()
        z: 1
        anchors.bottom: parent.bottom
        anchors.right: parent.right
        anchors.margins: 10
        width: style.buttonWidht
        height: style.buttonHeight
    }
    Button {
        id:cancelButton
        text: qsTr("Cancel")
        onClicked: cancel()
        z: 1
        anchors.bottom: parent.bottom
        anchors.right: applyButton.left
        anchors.margins: 10
        width: style.buttonWidht
        height: style.buttonHeight
    }
}

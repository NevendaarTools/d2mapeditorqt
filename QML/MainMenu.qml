import QtQuick 2.15
import QMLMainMenuController 1.0
import QtQuick.Layouts 1.15
import QtQuick.Controls 2.15
import QMLStyleManager 1.0
import QMLDynamicImageItem 1.0
import NevendaarTools 1.0

Item {
    anchors.fill: parent
    MainMenuController
    {
        id: controller
    }
    StyleManager
    {
        id: style
    }
    MouseArea {
        anchors.fill: parent
        propagateComposedEvents: true
        preventStealing: true
        acceptedButtons: Qt.NoButton
        onWheel: wheel => {
            if (bar.currentIndex === 0)
            {
                if (wheel.angleDelta.y < 0) {
                    itemsList.currentIndex = Math.min(itemsList.count - 1, itemsList.currentIndex + 1);
                } else {
                    itemsList.currentIndex = Math.max(0, itemsList.currentIndex - 1);
                }
                if (itemsList.count === 0)
                    itemsList.currentIndex = -1;
            } else {
                if (wheel.angleDelta.y < 0) {
                    importList.currentIndex = Math.min(importList.count - 1, importList.currentIndex + 1);
                } else {
                    importList.currentIndex = Math.max(0, importList.currentIndex - 1);
                }
                if (importList.count === 0)
                    importList.currentIndex = -1;
            }
            wheel.accepted = false
        }
    }
    Rectangle{
        id: iconRect
        color: "gray"
        border.color: "black"
        border.width: 2
        anchors.fill: parent
    }

    NTRectangle{
        id: mainArea
        anchors.left: parent.left
        anchors.top: parent.top
        anchors.bottom: parent.bottom
        anchors.right: menuArea.left
        Image {
            id: imageItem
            anchors.horizontalCenter: parent.horizontalCenter
            anchors.bottom: parent.bottom
            width: parent.width
            height: width / (sourceSize.width / sourceSize.height)
            source: "image://provider/editor"
            mirror: true
            fillMode: Image.PreserveAspectCrop
        }
    }

    NTRectangle{
        id: menuArea
        anchors.right: parent.right
        anchors.top: parent.top
        anchors.bottom: parent.bottom
        width: style.toolBarPanelWidth
    }

    NTRectangle{
        anchors.fill: gameName
        anchors.margins: -5
    }
    Text {
        id: gameName
        text: controller.gameName
        anchors.topMargin: 5
        anchors.horizontalCenter: parent.horizontalCenter
        anchors.top: parent.top
        font.pointSize: 12
        //textFormat: Text.RichText
    }

    NTRectangle{
        anchors.fill: gamePath
        anchors.margins: -5
    }
    Text {
        id: gamePath
        text: controller.gamePath
        anchors.topMargin: 7
        anchors.horizontalCenter: gameName.horizontalCenter
        anchors.top: gameName.bottom
        font.pointSize: 10
    }
    NTRectangle{
        anchors.fill: versionText
        anchors.rightMargin: -10
        anchors.margins: -5
    }
    Text {
        id: versionText
        text: controller.editorVersion
        anchors.top: parent.top
        anchors.left: parent.left
        anchors.leftMargin:5
        font.pointSize: 10
    }
    NTImageButton
    {
        id: infoButton
        source: "image://provider/Info"
        width: style.iconSize * 0.7
        height: style.iconSize * 0.7
        anchors.top: versionText.top
        anchors.left: versionText.right
        anchors.margins: 0
        anchors.leftMargin: 10
        tooltip: "About"
        onClicked: {
            invalidDialog.open()
        }
    }

    Dialog {
        id: confirmationDialog
        property int mapIndex: 0
        modal: true
        anchors.centerIn: parent
        standardButtons: Dialog.Ok | Dialog.Cancel
        title: "Map Already Imported"
        Text {
            anchors.fill: parent
            text: "This map has already been imported. Reimporting will delete all your changes.\n\nDo you want to continue?\n(To open a previously imported map, go to the Maps tab.)"
        }
        visible: false

        onAccepted: {
            controller.openMap(mapIndex, true);
            bar.currentIndex = 0;
        }

        onRejected: {

        }
    }


    NTRectangle{
        id: mapInfoArea
        visible:false
        anchors.centerIn: mainArea
        width:style.editorWidht/2
        height: style.editorHeight * 0.85
        anchors.margins: 0

        Text{
            id: mapName
            anchors.top: parent.top
            text: controller.mapName
            horizontalAlignment: TextInput.AlignHCenter
            verticalAlignment: TextInput.AlignVCenter
            font.pointSize: 16
            anchors.margins: 15
            anchors.left: parent.left
            anchors.right: parent.right
            height: 60
            wrapMode: Text.WordWrap
        }

        DynamicImageItem {
            id: mapPreview
            source: controller.mapImage
            width: style.buttonWidht * 3
            height: style.buttonWidht * 3
            anchors.top: mapName.bottom
            anchors.left: parent.left
            anchors.margins: 5
        }
        Text{
            anchors.top: mapName.bottom
            text: controller.mapInfo
            horizontalAlignment: TextInput.AlignHCenter
            verticalAlignment: TextInput.AlignVCenter
            font.pointSize: 12
            anchors.margins: 5
            anchors.left: mapPreview.right
            anchors.right: parent.right
            wrapMode: Text.WordWrap
        }
        Text{
            anchors.top: mapPreview.bottom
            text: controller.mapDesc
            horizontalAlignment: TextInput.AlignHCenter
            verticalAlignment: TextInput.AlignVCenter
            font.pointSize: 12
            anchors.margins: 15
            anchors.left: parent.left
            anchors.right: parent.right
            wrapMode: Text.WordWrap
        }
        NTTwoStateButton{
            id: openMapButton
            anchors.bottom: parent.bottom
            anchors.margins: 10
            anchors.right: parent.right
            source: "image://provider/MenuButton"
            activeSource: "image://provider/MenuButtonActive"
            width: style.buttonWidht * 2
            height: style.buttonHeight * 1.4
            text: controller.tr("Open")
            font.pointSize: 14
            onClicked: {
                var importMap = bar.currentIndex === 1
                if (importMap) {
                    if (controller.hasImportedMap(importList.currentIndex)) {
                        confirmationDialog.mapIndex = importList.currentIndex;
                        confirmationDialog.open();
                    }
                    else {
                        controller.openMap(importList.currentIndex, true);
                        bar.currentIndex = 0;
                    }
                }
                else
                    controller.openMap(itemsList.currentIndex, importMap)
            }
        }
        ImageButton{
            anchors.right: parent.right
            anchors.top:  parent.top
            anchors.margins: 10
            width: style.iconSize
            height: style.iconSize
            source: "image://provider/Delete"
            onClicked: {
                mapInfoArea.visible = false
            }
        }
    }

    Row  {
        id: bar
        property int currentIndex : 0
        anchors.top: parent.top
        anchors.left: menuArea.left
        anchors.right: menuArea.right
        anchors.leftMargin: 5
        anchors.topMargin: 5
        height: 35
        spacing: 5
        Button {
            id: mapsButton
            property int index : 0
            text: qsTr("Maps")
            onClicked: {bar.currentIndex = index}
            z: 1
            width: style.buttonWidht
            height: style.buttonHeight
            background: BorderImage {
                visible: bar.currentIndex === mapsButton.index
                source: "image://provider/back"
            }
        }
        Button {
            id: importButton
            property int index : 1
            text: qsTr("import")
            onClicked: {bar.currentIndex = index}
            z: 1
            width: style.buttonWidht
            height: style.buttonHeight
            background: BorderImage {
                visible: bar.currentIndex === importButton.index
                source: "image://provider/back"
            }
        }
    }
    StackLayout {
        anchors.left: menuArea.left
        anchors.right: menuArea.right
        anchors.top: bar.bottom
        height: 440
        anchors.margins: 2
        currentIndex: bar.currentIndex
        onCurrentIndexChanged: {
            if (currentIndex === 0)
            {
                itemsList.currentIndex = Math.min(itemsList.count - 1, 0)
                controller.setSelectedMap(itemsList.currentIndex, false)
            } else {
                importList.currentIndex = Math.min(importList.count - 1, 0)
                controller.setSelectedMap(itemsList.currentIndex, true)
            }
        }

        NTRectangle{
            id: mapListArea
            ListView{
                ScrollBar.vertical: ScrollBar {
                    active: true
                }
                id: itemsList
                anchors.fill: parent
                currentIndex: -1
                spacing: 1
                clip: true
                orientation: Qt.Vertical
                onCurrentIndexChanged: {
                    if (itemsList.currentIndex !== -1)
                        controller.setSelectedMap(itemsList.currentIndex, false)
                    mapInfoArea.visible = (itemsList.currentIndex !== -1)
                }

                model: controller.mapListModel
                delegate:  Item {
                    width:  itemsList.width
                    height: 28
                    Text{
                        anchors.fill: parent
                        text: Name + " (" + Desc + ")"
                        horizontalAlignment: TextInput.AlignLeft
                        verticalAlignment: TextInput.AlignVCenter
                        font.pointSize: 8
                        anchors.margins: 2
                    }
                    Rectangle
                    {
                        color: "transparent"
                        border.color: "black"
                        border.width: 2
                        anchors.fill: parent
                        visible: index === itemsList.currentIndex
                    }
                    MouseArea{
                        onClicked:
                        {
                            // controller.setSelectedMap(index, false)
                            itemsList.currentIndex = index
                            // mapInfoArea.visible = true
                        }
                        anchors.fill: parent
                        onDoubleClicked: controller.openMap(index, false)
                    }
                }
            }
        }
        NTRectangle{
            id: importListArea
            ListView{
                ScrollBar.vertical: ScrollBar {
                    active: true
                }
                id: importList
                anchors.fill: parent
                currentIndex: -1
                spacing: 1
                clip: true
                orientation: Qt.Vertical
                onCurrentIndexChanged: {
                    if (importList.currentIndex !== -1)
                        controller.setSelectedMap(importList.currentIndex, true)
                    mapInfoArea.visible = (importList.currentIndex !== -1)
                }

                model: controller.importListModel
                delegate:  Item {
                    width:  importList.width
                    height: 28
                    Text{
                        anchors.fill: parent
                        text: Name + " (" + Desc + ")"
                        horizontalAlignment: TextInput.AlignLeft
                        verticalAlignment: TextInput.AlignVCenter
                        font.pointSize: 8
                        anchors.margins: 2
                    }
                    Rectangle
                    {
                        color: "transparent"
                        border.color: "black"
                        border.width: 2
                        anchors.fill: parent
                        visible: index === importList.currentIndex
                    }
                    MouseArea{
                        onClicked:
                        {
                            // controller.setSelectedMap(index, true)
                            importList.currentIndex = index
                            // mapInfoArea.visible = true
                        }
                        anchors.fill: parent
                        onDoubleClicked: {
                            if (controller.hasImportedMap(index)) {
                                confirmationDialog.mapIndex = index;
                                confirmationDialog.open();
                            }
                            else {
                                controller.openMap(index, true);
                                bar.currentIndex = 0;
                            }
                        }
                    }
                }
            }
        }
    }

    NTImageButton
    {
        id: encyclopediaButton
        source: "image://provider/Encyclopedia"
        width: style.iconSize
        height: style.iconSize
        anchors.top: parent.top
        anchors.right: parent.right
        anchors.margins: 5
        tooltip: "Encyclopedia"
        onClicked: {
            controller.requestDataView();
        }
    }

    NTTwoStateButton{
        id: createButton
        anchors.bottom: openFileButton.top
        anchors.margins: 10
        anchors.bottomMargin: 5
        anchors.right: parent.right
        source: "image://provider/MenuButton"
        activeSource: "image://provider/MenuButtonActive"
        width: style.buttonWidht * 3
        height: style.buttonHeight * 1.5
        onClicked: {
            var component = Qt.createComponent("CreateMapDialog.qml");
            if (component.status === Component.Ready) {
                var msg = component.createObject(parent,
                                                 {

                                                 });
                //msg.setUid(Id)
            }
        }
        text: controller.tr("Create map")
    }

    NTTwoStateButton{
        id: openFileButton
        anchors.right: parent.right
        anchors.bottom: settingsButton.top
        anchors.margins: 10
        anchors.bottomMargin: 25
        source: "image://provider/MenuButton"
        activeSource: "image://provider/MenuButtonActive"
        width: style.buttonWidht * 3
        height: style.buttonHeight * 1.5
        onClicked: controller.openMapFromFile()
        text: controller.tr("Open file")
    }
    NTTwoStateButton{
        id: exitButton
        anchors.right: parent.right
        anchors.bottom: parent.bottom
        anchors.margins: 10
        anchors.bottomMargin: 10
        source: "image://provider/MenuButton"
        activeSource: "image://provider/MenuButtonActive"
        width: style.buttonWidht * 3
        height: style.buttonHeight * 1.5
        onClicked: controller.exit()
        text: controller.tr("Exit")
    }
    NTTwoStateButton{
        id: settingsButton
        anchors.bottom: exitButton.top
        anchors.right: parent.right
        anchors.margins: 10
        anchors.bottomMargin: 5
        width: style.buttonWidht * 3
        height: style.buttonHeight * 1.5
        source: "image://provider/MenuButton"
        activeSource: "image://provider/MenuButtonActive"
        text: controller.tr("Settings")
        onClicked: controller.selectGamePath()
    }
    NTInfoDialog {
        id: invalidDialog
        width: 480
        height: 530
        modal: true
        anchors.centerIn: parent
        title: controller.tr("About D2MapEditor")
        text: controller.about()
        buttonText: "Ok"
        onAccepted: {

        }
    }
}

import QtQuick 2.12
import QtQuick.Controls 2.14
import Qt.labs.qmlmodels 1.0
import QtQuick.Layouts 1.15

NTEditorWindow {
    id: item
	Rectangle{
        anchors.fill: parent
        color: "transparent"

        TextField {
            id: nameEdit
            width: 510
            height: 36
            anchors.bottom: traderTypeBar.top
            anchors.bottomMargin: 15
            font.pointSize: 13
            anchors.horizontalCenter: parent.horizontalCenter
            color: "black"
            text: merchant.name
            horizontalAlignment: TextInput.AlignHCenter
            selectByMouse: true
            onTextChanged: {merchant.name = text}
        }

        TabBar {
            id: traderTypeBar
            width: 560
            anchors.bottom: topRect.top
            anchors.bottomMargin: 5
            anchors.horizontalCenter: parent.horizontalCenter
            height: 30
            currentIndex: merchant.typeId
            onCurrentIndexChanged:
            {
                merchant.typeId = currentIndex
                leftPanel.currentIndex = currentIndex
            }

            TabButton {
                height: 30
                text: qsTr("Items")
            }
            TabButton {
                height: 30
                text: qsTr("Units")
            }
            TabButton {
                height: 30
                text: qsTr("Spells")
            }
            TabButton {
                height: 30
                text: qsTr("Trainer")
            }
        }

        Item{
            id:topRect
            anchors.horizontalCenter: parent.horizontalCenter
            width: contentItem.width
            anchors.bottom: contentItem.top
            anchors.bottomMargin:5
            height: 260

            Rectangle{
                id: iconRect
                color: "gray"
                border.color: "black"
                border.width: 2
                width: 240
                height: 240
                anchors.verticalCenter: parent.verticalCenter

                anchors.left: parent.left
                anchors.leftMargin: 15
                Image {
                    //anchors.centerIn: parent
                    id: icon
                    //width: 200
                    //height: 200
                    anchors.top: parent.top
                    anchors.topMargin: 10
                    anchors.horizontalCenter: parent.horizontalCenter
                    sourceSize: Qt.size(200, 200)
                    source: "image://provider/IsoCmon-IsoAnim-" + merchant.type + merchant.image

                }
                SpinBox {
                    from: 0
                    to: merchant.maxImage
                    value: merchant.image
                    onValueChanged: merchant.image = value
                    anchors.left: parent.left
                    anchors.bottom: parent.bottom
                    anchors.right: parent.right
                    anchors.margins: 2
                }
            }
            Rectangle{
               // anchors.left: iconRect.left
                anchors.left: descRect.right
                anchors.top: descRect.top
                anchors.leftMargin: 20
                color: "gray"
                border.color: "black"
                width: 240
                border.width: 2
                height: 30
                SpinBox {
                    from: 0
                    to: 6
                    value: merchant.priority
                    onValueChanged: merchant.priority = value
                    anchors.bottom: parent.bottom
                    anchors.top: parent.top
                    anchors.right: parent.right
                    width: 80
                    anchors.margins: 2
                }
                Text {
                    anchors.bottom: parent.bottom
                    anchors.top: parent.top
                    anchors.left: parent.left
                    anchors.leftMargin: 10
                    horizontalAlignment: TextInput.AlignHCenter
                    verticalAlignment: TextInput.AlignVCenter
                    font.pointSize: 14
                    text: qsTr("Priority:")
                }
            }
            Rectangle{
                id:descRect
                width: 410
                height: 240
                color: "gray"
                border.color: "black"
                border.width: 2

                anchors.right: parent.right
                anchors.verticalCenter: parent.verticalCenter
                anchors.rightMargin: 5

                TextEdit {
                    anchors.fill: parent
                    anchors.margins: 10
                    text: merchant.desc
                    wrapMode: Text.WordWrap
                    font.pointSize: 12
                    selectByMouse: true
                }
            }
        }

        StackLayout {
            id: contentItem
            anchors.horizontalCenter: parent.horizontalCenter
            anchors.topMargin: 45
            anchors.top: parent.verticalCenter
            anchors.bottom: parent.bottom
            anchors.bottomMargin: 15
            width: 675
            currentIndex: traderTypeBar.currentIndex

            InventoryWidget {
                id: goodsGrid
                presetType: "items_trader"
                model: merchant.goods
            }
            UnitHireWidget {
                presetType: "units_trader"
                cellWidth: 225
                model: merchant.units
            }
            SpellsListWidget {
                id: spellsItem
                presetType: "spells_trader"
                cellWidth: 225
                model: merchant.spells
            }
            Item {

            }
        }
    }
    NTLeftPanel
    {
        id: leftPanel
        width: 260
        anchors.top: parent.top
        anchors.left: parent.left
        anchors.bottom: parent.bottom
        anchors.margins: 5
        events2Visible: false
        eventsObjectUID: merchant.uid
        itemsVisible: traderTypeBar.currentIndex === 0
        unitsVisible: traderTypeBar.currentIndex === 1
        spellsVisible: traderTypeBar.currentIndex === 2
    }
    // StackLayout {
    //     width: 275
    //     anchors.top: parent.top
    //     anchors.topMargin: 0
    //     anchors.bottom: parent.bottom
    //     anchors.left: parent.left
    //     currentIndex: traderTypeBar.currentIndex

    //     SelectItemWidget
    //     {
    //         onItemDBLClicked: {
    //             merchant.goods.addItem(item)
    //         }
    //     }
    //     SelectSpellWidget
    //     {
    //         onItemDBLClicked: {
    //             merchant.spells.addItem(item)
    //         }
    //     }
    //     SelectUnitWidget
    //     {
    //         onItemDBLClicked: {
    //             merchant.units.addItem(item)
    //         }
    //     }
    //     Item {
    //         id: activityTab
    //     }
    // }
}

import QtQuick 2.12
import QtQuick.Controls 2.14
import QMLVillageEditor 1.0
import Qt.labs.qmlmodels 1.0
import QtQuick.Layouts 1.15

NTEditorWindow {
    id: item
    Connections {
        target: village
        function onLevelChanged() {
            garrisonView.reload()
            visiterView.reload()
        }
    }
    Item {
        id: centralArea
        anchors.left: leftPanel.right
        anchors.right: parent.right
        anchors.top: parent.top
        anchors.bottom: parent.bottom
        TextField {
            id: stackNameEdit
            height: 36
            anchors.top: parent.top
            anchors.margins: 5
            font.pointSize: style.fontSize
            anchors.left: parent.left
            color: "black"
            width: 415
            text: village.stackName
            horizontalAlignment: TextInput.AlignHCenter
            selectByMouse: true
            onTextChanged: {village.stackName = text}
        }
        TextField {
            id: villageNameEdit
            height: 36
            anchors.top: parent.top
            anchors.margins: 5
            font.pointSize: style.fontSize
            anchors.left: stackNameEdit.right
            width: 370
            color: "black"
            text: village.name
            horizontalAlignment: TextInput.AlignHCenter
            selectByMouse: true
            onTextChanged: {village.name = text}
        }
        ComboBox {
            id: ownerCombo
            width:  160
            anchors.right: parent.right
            anchors.verticalCenter: villageNameEdit.verticalCenter
            anchors.margins: 5
            height: 25
            model: village.ownerModel
            currentIndex: village.ownerModel.ownerIndex
            textRole: "Name"
            onCurrentIndexChanged: {
                village.ownerModel.ownerIndex = currentIndex
            }
        }
        GroupView
        {
            id: visiterView
            parentItem: item
            garrison: village.visiter
            isLeft: true
            anchors.left: stackNameEdit.left
            anchors.top: stackNameEdit.bottom
            anchors.topMargin: 5
            width: 220
            height: 400
        }
        NTRectangle{
            id: iconRect
            width: 180
            height: 180
            anchors.left: visiterView.right
            anchors.leftMargin: 10
            anchors.top: visiterView.top
            Image {
                sourceSize: Qt.size(160, 160)
                anchors.horizontalCenter: parent.horizontalCenter
                anchors.top: parent.top
                anchors.topMargin: 10
                id: icon
                source: "image://provider/" + village.image
            }
            NTSpinBoxInt {
                id: iconTypeSpin
                min: 1
                max: 5
                value: village.level
                onValueModified: (value) =>
                {
                    village.level = value
                }
                anchors.bottom: parent.bottom
                anchors.left: parent.left
                height: 25
                width: 90
                anchors.margins: 2
                anchors.leftMargin: 35
            }
        }
        NTHeroPuppet {
            id: heroPuppet
            anchors.left: iconRect.left
            anchors.top: iconRect.bottom
            anchors.topMargin: 5
        }
        GroupView
        {
            id: garrisonView
            parentItem: item
            garrison: village.garrison
            isLeft: false
            anchors.left: villageNameEdit.left
            anchors.top: villageNameEdit.bottom
            anchors.topMargin: 10
            width: 220
            height: 400
        }
        InventoryWidget {
            id: inventory2Grid
            model: village.visiterInventory
            anchors.left: visiterView.left
            anchors.leftMargin: 0
            anchors.top: visiterView.bottom
            anchors.topMargin: 10
            anchors.bottom: parent.bottom
            anchors.bottomMargin: 10
            anchors.right: heroPuppet.right
        }
        InventoryWidget {
            id: inventory1Grid
            model: village.garrisonInventory
            anchors.left: garrisonView.left
            anchors.leftMargin: 0
            anchors.top: garrisonView.bottom
            anchors.topMargin: 10
            anchors.bottom: parent.bottom
            anchors.bottomMargin: 10
            anchors.right: parent.right
        }
    }

    NTLeftPanel
    {
        id: leftPanel
        width: 260
        anchors.top: parent.top
        anchors.left: parent.left
        anchors.bottom: parent.bottom
        anchors.margins: 5
        spellsVisible: false
        events2Visible: true
        eventsObjectUID: village.uid
        eventsObject2UID: village.stackUid
    }
}

import QtQuick 2.12
import QtQuick.Controls 2.14
import QMLLayersTool 1.0
import Qt.labs.qmlmodels 1.0
import QtQuick.Layouts 1.15
import QMLOptionsModel 1.0
import QMLToolsProvider 1.0

ListView{
    spacing: 2
    orientation: Qt.Vertical
    id: listView
    property OptionsModel settingsModel
    model: settingsModel

    Component {
        id: boolDelegate
        NTCheckBox {
            id: gridCheckbox
            width:  listView.width
            height: 25
            anchors.centerIn: parent
            checked: l_optionValue === "True"
            text: l_optionName
            onClicked: (checked, mouse) =>  {
                settingsModel.setValue(l_optionIndex, checked?"True" : "False")
            }
            enabled: l_optionEnabled
        }
    }
    Component {
        id: stringDelegate
        Text {
            id: desc
            width:  listView.width
            height: 25
            anchors.centerIn: parent
            wrapMode: Text.WordWrap
            text: l_optionName
        }
    }
    Component {
        id: titleDelegate
        Text {
            id: desc
            width:  listView.width
            height: 25
            anchors.centerIn: parent
            wrapMode: Text.WordWrap
            text: l_optionName
            horizontalAlignment: TextInput.AlignHCenter
            verticalAlignment: TextInput.AlignVCenter
            font.pointSize: 16
        }
    }
    Component {
        id: spacerDelegate
        Rectangle {
            id: desc
            width:  listView.width
            height: 5
            color: "black"
        }
    }
    Component {
        id: intDelegate
        Item{
            width:  listView.width
            height: 25
            anchors.centerIn: parent

            Text {
                id: desc
                width:  70
                height: 25
                anchors.top: parent.top
                anchors.left: parent.left
                wrapMode: Text.WordWrap
                text: l_optionName
                horizontalAlignment: TextInput.AlignHCenter
                verticalAlignment: TextInput.AlignVCenter
                font.pointSize: 11
            }

            NTSpinBoxInt {
                anchors.left: desc.right
                height: 25
                anchors.right: parent.right
                anchors.top: parent.top
                step: 25
                //imageSource: "image://provider/" + Icon
                value: l_optionValue
                editable: l_optionEnabled
                onValueChanged: {
                    settingsModel.setValue(l_optionIndex, value + "")
                }
            }
        }
    }
    delegate: Loader {
        property var l_optionType: OptionType
        property var l_optionName: translate.tr(Name)
        property var l_optionDesc: translate.tr(Desc)
        property var l_optionValue: Value
        property var l_optionEnabled: Enabled
        property var l_optionIndex: index
        sourceComponent: {
            switch(OptionType) {
            case 0: return boolDelegate
            case 2: return intDelegate
            case 32: return titleDelegate
            case 33: return spacerDelegate
            default: return stringDelegate
            }
        }
    }
}

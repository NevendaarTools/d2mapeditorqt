import QtQuick 2.15
import QMLMainMenuController 1.0
import QtQuick.Layouts 1.15
import QtQuick.Controls 2.15
import QMLStyleManager 1.0

Rectangle {
    StyleManager
    {
        id: style
    }
    border.color: style.borderColor
    border.width: style.borderWidht
    color: style.backColor
    property alias style: style
}

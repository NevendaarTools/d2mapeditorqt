import QtQuick 2.15
import QMLMainMenuController 1.0
import QtQuick.Layouts 1.15
import QtQuick.Controls 2.15

GridView{
    id:settingsGrid
    flow: GridView.FlowTopToBottom
    //ScrollBar.vertical: ScrollBar {policy: ScrollBar.AlwaysOn }
    cellWidth: 380; cellHeight: 32
    clip:true
    Component {
        id: itemOnUnitDelegate
        Rectangle {
            id: wrapper
            width: 380
            height: 32
            //                border.color: GridView.isCurrentItem ? "#006600" : "#d1c7a2"
            //border.color: "#77aa77"
            border.width: 0
            color: "transparent"
            BorderImage{
                border { left: 10; top: 10; right: 10; bottom: 10 }
                horizontalTileMode: BorderImage.Stretch
                verticalTileMode: BorderImage.Stretch
                source: "image://provider/back"
                anchors.fill: parent
                visible: true
                opacity: 0.8
            }
            Text {
                id: nameText
                anchors.top: parent.top
                anchors.left: parent.left
                anchors.bottom: parent.bottom
                anchors.margins: 5
                width: 170
                text: Name
                wrapMode: Text.Wrap
                antialiasing: true
                //style: Text.Sunken

                horizontalAlignment: Text.AlignHCenter
                font: Qt.font({
                                  //family: fontfamily,
                                  bold: true,
                                  //italic: true,
                                  pointSize: 10
                              });
                color: "#404040"
                HoverHandler {
                    onHoveredChanged: {
                        desc.visible = hovered
                        if (hovered)
                        {
                            desc.x = Math.min(parent.width / 2 + settingsGrid.x + wrapper.x, settingsGrid.width - desc.width)
                            desc.y = parent.height / 2 + settingsGrid.y + wrapper.y - settingsGrid.contentY
                            //desc.parent = parent
                            desc.name = Name
                            desc.desc = Desc
                            //desc.z = -100
                        }
                    }
                }
            }
            Loader {
                anchors.top: parent.top
                anchors.right: parent.right
                anchors.bottom: parent.bottom
                anchors.left: nameText.right
                sourceComponent: {
                    switch(OptionType) {
                    case 0: return boolDelegate
                    case 1: return floatDelegate
                    case 2: return intDelegate
                    case 3: return stringDelegate
                    case 4: return variantsDelegate
                    case 32: return titleDelegate
                    case 33: return spacerDelegate
                    default: return stringDelegate
                    }
                }
            }
            Component {
                id: spacerDelegate
                Rectangle {
                    id: desc
                    width:  settingsGrid.cellWidth
                    height: 5
                    color: "black"
                }
            }
            Component {
                id: stringDelegate
                Item{
                    anchors.fill: parent
                    Rectangle {
                        anchors.fill: parent
                        border.width: 1
                        border.color: "#000000"
                        color: "#00000000"
                        clip: false
                        anchors.margins: 4
                        TextEdit {
                            anchors.fill: parent
                            anchors.leftMargin: 4
                            anchors.rightMargin: 4
                            text: Value
                            horizontalAlignment: Text.AlignCenter
                            verticalAlignment: Text.AlignVCenter
                            //color: "#bbddbb"
                            font: Qt.font({
                                              //family: fontfamily,
                                              //bold: true,
                                              //italic: true,
                                              pointSize: 10})
                            focus: true
                            clip: true

                            onTextChanged: {
                                settingsGrid.model.setValue(index, text, false)
                            }

                        }
                    }

                }
            }
            Component {
                id: titleDelegate
                Item{
                    anchors.fill: parent
                    Rectangle {
                        anchors.fill: parent
                        border.color: "#000000"
                        color: "#00000000"
                        clip: false
                        anchors.margins: 4
                        Text {
                            anchors.fill: parent
                            anchors.leftMargin: 4
                            anchors.rightMargin: 4
                            text: Value
                            horizontalAlignment: Text.AlignCenter
                            verticalAlignment: Text.AlignVCenter
                            //color: "#bbddbb"
                            font: Qt.font({
                                              //family: fontfamily,
                                              //bold: true,
                                              //italic: true,
                                              pointSize: 10})
                            focus: true
                            clip: true
                        }
                    }

                }
            }

            Component {
                id: boolDelegate
                Item
                {
                    anchors.fill: parent
                    Image {
                        anchors.centerIn: parent

                        width: 60
                        height: 18

                        source: (Value == "True")?"image://provider/true":"image://provider/false"
                        MouseArea
                        {
                            anchors.fill: parent
                            onClicked: {
                                settingsGrid.model.setValue(index, (Value == "True")?"False":"True")
                            }
                        }
                    }
                }
            }
            Component {
                id: floatDelegate
                NTSpinBoxFloat {
                    anchors.fill: parent
                    anchors.margins: 4
                    //imageSource: "image://provider/" + Icon
                    text: Value
                    editable: true
                    onValueChanged: {
                        settingsGrid.model.setValue(index, value + "")
                    }
                }
            }
            Component {
                id: intDelegate
                NTSpinBoxInt {
                    anchors.fill: parent
                    anchors.margins: 4
                    //imageSource: "image://provider/" + Icon
                    value: Value
                    editable: true
                    onValueChanged: {
                        settingsGrid.model.setValue(index, value + "")
                    }
                }
            }
            Component {
                id: variantsDelegate
                Item {
                    anchors.fill: parent
                    anchors.margins: 4
                    //source: "image://provider/back"
                    Image {
                        id: leftButton
                        anchors.top: parent.top
                        anchors.left: parent.left
                        anchors.bottom: parent.bottom
                        anchors.margins: 0
                        width: 24
                        enabled: settingsGrid.model.currentVariantIndex(index) > 0
                        opacity: enabled? 1: 0.4
                        source: "image://provider/left"
                        MouseArea
                        {
                            anchors.fill: parent
                            onClicked: {
                                settingsGrid.model.prevVariant(index)
                            }
                        }
                    }
                    Image {
                        id: rightButton
                        anchors.top: parent.top
                        anchors.right: parent.right
                        anchors.bottom: parent.bottom
                        anchors.margins: 0
                        width: 24
                        enabled: settingsGrid.model.currentVariantIndex(index) < settingsGrid.model.variantsCount(index) - 1
                        opacity: enabled? 1: 0.4
                        source: "image://provider/right"
                        MouseArea
                        {
                            anchors.fill: parent
                            onClicked: {
                                settingsGrid.model.nextVariant(index)
                            }
                        }
                    }
                    Text {
                        anchors.top: parent.top
                        anchors.left: leftButton.right
                        anchors.right: rightButton.left
                        anchors.bottom: parent.bottom
                        anchors.margins: 4
                        text: Value
                        wrapMode: Text.NoWrap
                        horizontalAlignment: Text.AlignHCenter
                        //color: "#bbddbb"
                        font: Qt.font({
                                          //family: fontfamily,
                                          //bold: true,
                                          //italic: true,
                                          pointSize: 8})
                    }
                }
            }
        }
    }
    delegate: itemOnUnitDelegate
    Item {
        id: desc
        property alias name: nameText.text
        property alias desc: descText.text

        visible: false
        width: 220
        height: 140
        z: 120
        BorderImage{
            border { left: 10; top: 10; right: 10; bottom: 10 }
            horizontalTileMode: BorderImage.Stretch
            verticalTileMode: BorderImage.Stretch
            source: "image://provider/back"
            anchors.fill: parent
            visible: true
            opacity: 1
        }
        Text {
            id: nameText
            anchors.top: parent.top
            anchors.left: parent.left
            anchors.right: parent.right
            anchors.margins: 5
            anchors.topMargin: 15
            height: 24
            text: ""
            wrapMode: Text.Wrap
            antialiasing: true
            //style: Text.Sunken

            horizontalAlignment: Text.AlignHCenter
            font: Qt.font({
                              //family: fontfamily,
                              bold: true,
                              //italic: true,
                              pointSize: 10
                          });
            color: "#404040"
        }
        Text {
            id: descText
            anchors.top: nameText.bottom
            anchors.left: parent.left
            anchors.right: parent.right
            anchors.margins: 10
            anchors.topMargin: 15
            text: ""
            //style: Text.Sunken
            wrapMode: Text.Wrap
            horizontalAlignment: Text.AlignHCenter
            font: Qt.font({
                              //family: fontfamily,
                              //bold: true,
                              //italic: true,
                              pointSize: 8
                          });
            color: "#404040"
        }
    }
}

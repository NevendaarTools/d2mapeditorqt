import QtQuick 2.12
import QtQuick.Controls 2.1
import QMLVillageEditor 1.0
import Qt.labs.qmlmodels 1.0
import QMLPresetController 1.0
import QtQml.Models 2.15

Rectangle
{
    property alias presetType: presets.type
    property alias controller: presets
    signal presetLoaded(var preset);
    signal presetSaved(var name);
    PresetController{
        id: presets
        type: "inventory"
    }
    color: "gray"
    border.color: "black"
    Button
    {
        id: loadButton
        text: "Load preset"
        height: 24
        anchors.verticalCenter: parent.verticalCenter
        anchors.left: parent.left
        onClicked: {
            menu.open();
        }
        Menu {
            anchors.centerIn: parent
            id: menu

            Instantiator {
                model: presets.presetList
                MenuItem {
                    text: presets.presetList[index]
                    onClicked: function handleMenuClick()
                    {
                        nameField.text = presets.presetList[index];
                        presetLoaded(presets.getPreset(index))
                    }
                }
                onObjectAdded: function insert(index, object){ menu.insertItem(index, object)}
                onObjectRemoved: function remove(index, object) { menu.removeItem(object)}
            }
        }
    }

    TextField {
        id: nameField
        anchors.top: parent.top
        anchors.bottom: parent.bottom
        anchors.left: loadButton.right
        anchors.leftMargin: 15
        anchors.right: saveButton.left
        anchors.margins: 2
    }
    Button
    {
        id: saveButton
        height: 24
        text: "save"
        anchors.verticalCenter: parent.verticalCenter
        anchors.right: parent.right
        onClicked: {
            presetSaved(nameField.text)
        }
    }
    Rectangle
    {
        anchors.left: nameField.left
        anchors.right: saveButton.right
        color: "transparent"
        border.color: "black"
        border.width: 1
        anchors.top: parent.top
        anchors.bottom: parent.bottom
        anchors.margins: 2
        anchors.leftMargin: -2
        anchors.rightMargin: -2
    }
}

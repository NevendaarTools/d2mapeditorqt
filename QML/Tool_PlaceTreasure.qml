import QtQuick 2.15
import QtQuick.Layouts 1.2
import QtQuick.Controls 2.0
import QMLMainToolBarController 1.0
import QMLPlaceTreasureTool 1.0
import QMLToolsProvider 1.0
import QMLGroupUnitsEditor 1.0

Item {
    id: tool_PlaceTreasure
    property ToolsProvider toolsProv
    property PlaceTreasureTool toolPlaceTreasure
    GroupUnitsEditor
    {
        id: unitsEditor
        onUnitsChanged:{
            //unitsView.reload();
        }
    }

    function init(tool)
    {
        toolsProv = tool;
        toolPlaceTreasure = toolsProv.currentToolObj();
        name.text = toolPlaceTreasure.toolName()
        toolPlaceTreasure.stack = unitsEditor
        toolPlaceTreasure.init()
        imageSpin.value = toolPlaceTreasure.imageIndex
    }

    Text {
        id: name
        height: 24
        horizontalAlignment: TextInput.AlignLeft
        verticalAlignment: TextInput.AlignVCenter
        font.pointSize: 14
        anchors.top: parent.top
        anchors.left: parent.left
        anchors.right: parent.right
        anchors.margins: 5
    }
    ImageButton{
        anchors.top: parent.top
        anchors.right: parent.right
        width: 24
        height: 24
        anchors.margins: 5
        source: "image://provider/Exit"
        onClicked:
        {
            toolsProv.closeTool();
        }
    }

    Item{
        anchors.horizontalCenter: parent.horizontalCenter
        anchors.top: name.bottom
        width: parent.width - 10
        height: width
        id: iconRect
        anchors.topMargin: 10
        Image {
            anchors.centerIn: parent
            id: icon
            sourceSize: Qt.size(160, 160)
            source: "image://provider/IsoCmon-IsoAnim-" + toolPlaceTreasure.imageName(toolPlaceTreasure.imageIndex)
        }
    }

    NTSpinBoxInt {
        id: imageSpin
        height: 22
        width: 80
        anchors.top: iconRect.bottom
        min: 0
        max: 99
        backgroundRect.visible: true
        anchors.horizontalCenter: parent.horizontalCenter
        editable: true
        image.width: 0
        anchors.topMargin: -30

        onValueChanged:
        {
            toolPlaceTreasure.imageIndex = value
        }
    }
}

import QtQuick 2.3
import QtQuick.Controls 2
import QtQuick.Layouts 1.1
import QtQuick.Window 2.0
import QMLStyleManager 1.0

Item {
    id: button
    StyleManager
    {
        id: style
    }
    signal clicked
    signal pressed
    signal released
    signal entered
    signal exited
    signal londTapped
    property alias hoverEnabled: mouseArea.hoverEnabled
    property alias acceptedButtons: mouseArea.acceptedButtons
    property alias source: sprite.source
    property alias activeSource: activeSprite.source
    property alias im: sprite
    property alias text: buttonText.text
    property alias font: buttonText.font
    property alias extraEctive: extraActiveSprite

    Image {
        anchors.fill: parent
        id: sprite
        source: "image://provider/file"
        rotation: 0
    }
    Image {
        anchors.fill: parent
        id: activeSprite
        source: "image://provider/file"
        rotation: 0
        visible: false
    }
    Image {
        anchors.fill: parent
        id: extraActiveSprite
        source: activeSprite.source
        rotation: 0
        visible: false
    }
    Text{
        id: buttonText
        anchors.centerIn: parent
        text: ""
    }

    MouseArea {
        id: mouseArea
        enabled: button.enabled
        anchors.fill: button
        hoverEnabled: true

        //onClicked: button.pressed()
        onPressed: button.pressed()
        onReleased: button.released()
        onEntered: button.entered()
        onExited: button.exited()
    }

    Timer {
        id:timer
        interval: 500
        onTriggered: {
            button.londTapped();
        }
    }

    onLondTapped: {
        //console.log("taapped");
    }

    onClicked: {
        //console.log("clicked");
    }

    onPressed: {
        opacity = 0.5
        timer.start();
    }

    onReleased: {
        opacity = 1.0
        if (timer.running) {
            timer.stop();
            button.clicked();
        }
    }
    onEntered: {
        activeSprite.visible = true
    }
    onExited: {
        activeSprite.visible = false
    }
}

import QtQuick 2.0
import QMLLogModel 1.0
import QtQuick.Controls 2.1

Item {
    Rectangle{
        anchors.fill: logList
        color: "grey"
        border.color: "black"
        opacity: 0.8
    }
    ListView{
        LogModel{
            id: logModel
            onModelReset: {
                logList.positionViewAtEnd()
                logList.contentY = logList.contentHeight - logList.height
                logList.positionViewAtIndex(logList.count - 1, ListView.End)
                logList.positionViewAtIndex(0, ListView.End)
            }
        }
        ScrollBar.vertical: ScrollBar {
            active: true
        }
        id: logList
        anchors.fill: parent

        spacing: 7
        clip: true
        orientation: Qt.Vertical

        model: logModel
        delegate:  Text {
            id: deleg
            smooth: true
            width:  logList.width
            wrapMode: Text.WordWrap
            text: message
            color: {
                    switch (level) {
                        case 0:
                            return "red";
                        case 1:
                            return "orange";
                        case 2:
                            return "black";
                        default:
                            return "black";
                    }
                }
        }
    }
}

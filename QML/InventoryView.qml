import QtQuick 2.0
import QMLBaseObjectPreview 1.0
import QtQuick.Controls 2.14
import QtQuick.Layouts 1.15
import QMLTreasureEditor 1.0

Rectangle {
    id: inventoryView

    function prefferedHeight(rowCount)
    {
        return rowCount  * (rowH +itemsList.spacing)
    }

    property int rowH: 24
    property alias model: itemsList.model
    property alias delegate: itemsList.delegate
    color: "grey"
    border.color: "black"
    ListView{
        ScrollBar.vertical: ScrollBar {
            active: false
        }
        id: itemsList
        anchors.fill: parent

        spacing: 0
        clip: true
        orientation: Qt.Vertical

        model: 0
        delegate:  Item {
            id: deleg
            width:  inventoryView.width
            height: rowH

            Rectangle
            {
                color: "grey"
                border.color: "black"
                border.width: 1
                anchors.fill: parent
            }
            Item
            {
                id: iconRect
                anchors.left: parent.left
                anchors.verticalCenter: parent.verticalCenter
                width: rowH
            }

            Image {
                anchors.centerIn: iconRect
                visible: Id !== ""
                id: icon
                scale: 0.25
                source: "image://provider/" + Icon

            }
            Text {
                id: name
                lineHeight: 0.8
                wrapMode: Text.Wrap
                elide: Text.ElideRight
                verticalAlignment: Text.AlignVCenter
                horizontalAlignment: Text.AlignHCenter
                text: Name
                font.bold: true
                anchors.left: iconRect.right
                anchors.right: parent.right
                anchors.verticalCenter: parent.verticalCenter
                anchors.rightMargin: 40
                antialiasing: true
            }
            Text {
                id: count
                text: Count
                font.bold: true
                anchors.verticalCenter: parent.verticalCenter
                anchors.right: parent.right
                horizontalAlignment: Text.AlignRight
                verticalAlignment: Text.AlignVCenter
                width: 35
                anchors.rightMargin: 5
                antialiasing: true
            }
        }
    }
}

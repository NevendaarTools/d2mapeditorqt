import QtQuick 2.15
import QtQuick.Dialogs
import QtQuick.Controls 2.15
import QMLStyleManager 1.0

Dialog {
    id: twoButtonsDialog
    title: ""
    modal: true
    visible: false
    signal dialogAccepted()
    property alias text: nameEdit.text
    property alias buttonText: okButton.text

    StyleManager
    {
        id: style
    }

    Keys.onReturnPressed: {
        twoButtonsDialog.accepted()
        twoButtonsDialog.close();
    }
    Keys.onEscapePressed: {
        twoButtonsDialog.accepted()
        twoButtonsDialog.close();
    }
    NTRectangle{
        anchors.fill: parent
        anchors.margins: -7
    }

    TextEdit {
        id: nameEdit
        anchors.left: parent.left
        anchors.top: parent.top
        anchors.right: parent.right
        anchors.bottom: okButton.top
        font.pointSize: style.fontSize
        color: "black"
        text: "" // Здесь вы можете задать текст с HTML-ссылкой
        wrapMode: TextEdit.WordWrap
        horizontalAlignment: TextEdit.AlignLeft
        verticalAlignment: TextEdit.AlignVCenter
        readOnly: true // Чтобы текст нельзя было редактировать
        textFormat: TextEdit.RichText // Включаем поддержку HTML

        onLinkActivated: (link) => {
            Qt.openUrlExternally(link); // Открывает ссылку в браузере
        }
    }


    NTTwoStateButton{
        id: okButton
        anchors.bottom: parent.bottom
        anchors.horizontalCenter: parent.horizontalCenter
        width: style.buttonWidht
        height: style.buttonHeight
        source: "image://provider/MenuButton"
        activeSource: "image://provider/MenuButtonActive"
        onClicked: {
            twoButtonsDialog.accepted()
            twoButtonsDialog.close();
        }
        text: "Ok"
    }
}

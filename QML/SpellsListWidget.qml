import QtQuick 2.12
import QtQuick.Controls 2.14
import QMLVillageEditor 1.0
import Qt.labs.qmlmodels 1.0
import QtQuick.Layouts 1.15

Item
{
    property alias model: itemsGrid.model
    property alias cellWidth: itemsGrid.cellWidth
    property alias presetType: presets.presetType
    //property alias presetType: presets.type
    id: spellsItem
    Rectangle
    {
        color: "gray"
        border.color: "black"
        border.width: 1
        anchors.fill: itemsGrid
        anchors.margins: -10
    }
    PresetPanel {
        id: presets
        anchors.left: parent.left
        anchors.right: parent.right
        anchors.top: parent.top
        height: 28
        onPresetLoaded: {
            itemsGrid.model.loadPreset(preset);
        }
        onPresetSaved: {
            presets.controller.savePreset(name, itemsGrid.model.toPreset());
        }
    }
    GridView{
        anchors.fill: parent
        anchors.topMargin: 35
        id:itemsGrid
        cellWidth: 240; cellHeight: 60
        ScrollBar.vertical: ScrollBar {
            active: true
        }
        model: 0
        clip:true
        Component {
            id: itemDelegate
            Rectangle {
                id: wrapper
                width: itemsGrid.cellWidth - 2
                height: itemsGrid.cellHeight - 2
                border.color: "black"
                border.width: 2
                color: "transparent"

                Text {
                    id: name
                    anchors.top: wrapper.top
                    anchors.left: icon.right
                    anchors.right: parent.right
                    wrapMode: Text.WordWrap
                    text: Name
                    horizontalAlignment: TextInput.AlignHCenter
                    font.bold: true
                }
                Text {
                    id: desc
                    anchors.top: name.bottom
                    anchors.topMargin: 5
                    anchors.left: icon.right
                    anchors.right: parent.right
                    wrapMode: Text.WordWrap
                    horizontalAlignment: TextInput.AlignHCenter
                    verticalAlignment: TextInput.AlignVCenter
                    text: Desc
                }

                Image{
                    id: icon
                    visible: Id !== ""
                    source: "image://provider/" + Icon
                    anchors.verticalCenter: wrapper.verticalCenter
                    anchors.left: wrapper.left
                    anchors.leftMargin: 2
                    width: 40
                    height: 40
                    //anchors.top: parent.top
                    //anchors.margins: {5;5;5;5}
                    smooth: true


                }
                MouseArea {
                    anchors.fill: parent
                    drag.target: icon
                    hoverEnabled: true
                    onDoubleClicked: itemsGrid.model.removeItem(Id)
                    onEntered: {
                        //overlay.showPreview(dragArea, "ItemWidget.qml", Id)
                    }
                    onExited: {
                        overlay.hidePreview()
                    }
                }
                ImageButton{
                    anchors.right: parent.right
                    anchors.top:  parent.top
                    width: 20
                    height: 20
                    source: "image://provider/Delete"
                    visible: icon.visible
                    onClicked: {
                        itemsGrid.model.removeItem(Id)
                    }
                }
            }

        }
        delegate: itemDelegate
    }
    DropArea {
        id:dropArea
        anchors.fill: parent
        //anchors.margins: 10
        Rectangle {
            id: dragTargetArea
            anchors.fill: parent
            border.color:  "gold"
            border.width: 3
            visible: parent.containsDrag
            color: "transparent"
        }

        onEntered: {
        }
        onExited: {
        }
        onDropped: (drop) => {
                       console.log(drop)
            if (drop.getDataAsString("mode") === "SpellAdd")
            {
                itemsGrid.model.addItem(drop.getDataAsString("id"))
            }
        }
    }
}

import QtQuick 2.0
import QMLItemView 1.0
import QMLCostEditor 1.0
import QtQuick.Controls 2.14
import QtQuick.Layouts 1.15
import NTComponents 1.0

Rectangle {
    id: wrapper
    property alias spellId: view.spellId
    SpellView{
        id: view
    }
    function setUID(objectUid)
    {
        spellId = objectUid
    }

    width: 300
    height: 180
    border.color: "black"
    border.width: 2
    color: "gray"

    Image{
        id: icon
        source: "image://provider/" + view.icon
//        width: 64
//        height: 64
        anchors.top: parent.top
        anchors.left: parent.left
        anchors.margins: {5;5;5;5}
        smooth: true
        visible: spellId != ""
    }

    Text {
        id: name
        text: view.name
        font.bold: true
        anchors.top: parent.top
        anchors.left: icon.right
        anchors.right: parent.right
        anchors.margins: 5
    }
    Text {
        id: desc
        anchors.top: icon.bottom
        anchors.left: parent.left
        anchors.right: parent.right
        wrapMode: Text.WordWrap
        text: view.desc
        anchors.margins: 5
    }
    NTCostView {
        id: costView
        anchors.bottom: parent.bottom
        anchors.left: parent.left
        anchors.right: parent.right
        anchors.margins: 5
        value: view.cost
    }
}

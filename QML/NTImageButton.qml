import QtQuick 2.3
import QtQuick.Controls 2
import QtQuick.Layouts 1.1
import QtQuick.Window 2.0
import QMLStyleManager 1.0

Item {
    id: button
    function findRootItem(item) {
        while (item.parent) {
            item = item.parent;
        }
        return item;
    }

    StyleManager
    {
        id: style
    }
    signal clicked
    signal pressed
    signal released
    signal entered
    signal exited
    signal londTapped
    property alias hoverEnabled: mouseArea.hoverEnabled
    property alias acceptedButtons: mouseArea.acceptedButtons
    property alias source: sprite.source
    property alias im: sprite
    property alias border: borderRect
    property string tooltip: ""


    Rectangle {
        id:rect
        color: "#0d0d0d"
        border.color: "#299132"
        opacity: 0.15
        anchors.fill: parent
        //radius: 15
    }
    Rectangle {
        id:selecredRect
        color: "#299132"
        opacity: 0
        anchors.fill: parent
        //radius: 15
    }

    //width: sprite.width
    //height: sprite.height

    Image {
        anchors.fill: parent
        id: sprite
        source: "image://provider/file"
        rotation: 0
    }
    Rectangle{
        anchors.fill: parent
        id: borderRect
        color: "transparent"
        border.color: style.borderColor
        border.width: style.borderWidht
        visible: false
        //radius:  0.2 * height
    }
    Rectangle{
        anchors.fill: parent
        id: hoverRect
        color: "lightgray"
        border.color: style.borderColor
        border.width: 1
        visible: false
        opacity: 0.2
        //radius:  0.2 * height
    }
    MouseArea {
        id: mouseArea
        enabled: button.enabled
        anchors.fill: button
        hoverEnabled: true

        //onClicked: button.pressed()
        onPressed: button.pressed()
        onReleased: button.released()
        onEntered: button.entered()
        onExited: button.exited()
    }

    Timer {
        id:timer
        interval: 500
        onTriggered: {
            button.londTapped();
        }
    }

    onLondTapped: {
        //console.log("taapped");
    }

    onClicked: {
        //console.log("clicked");
    }

    onPressed: {
        opacity = 0.5
        timer.start();
    }

    onReleased: {
        opacity = 1.0
        if (timer.running) {
            timer.stop();
            button.clicked();
        }
    }

    onEntered: {
        enteredAnimation.start()
        hoverRect.visible = true
        if (button.tooltip != ""){
            overlay.showTooltip(button, button.tooltip)
        }
    }
    onExited: {
        exitedAnimation.start()
        hoverRect.visible = false
        overlay.hideTooltip()
    }

    NumberAnimation {
        id:enteredAnimation
        target: selecredRect
        property: "opacity"
        easing.type: Easing.InQuint
        to: 0.8
        duration: 120
    }
    NumberAnimation{
        id:exitedAnimation
        target: selecredRect
        property: "opacity"
        easing.type: Easing.InQuint
        to: 0
        duration: 120
    }
    function showPresed(){
        enteredAnimation.start();
    }
}

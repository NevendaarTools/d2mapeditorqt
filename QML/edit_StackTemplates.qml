import QtQuick 2.15
import QtQuick.Controls 2.15
import QMLStyleManager 1.0
import QMLSceneVariablesEditor 1.0
import NTModels 1.0
import NevendaarTools 1.0

Dialog {
    visible: true
    width: style.editorWidht
    height: style.editorHeight
    property bool acceptChanges : false
    id: item
    StyleManager
    {
        id: style
    }
    onClosed: {
        console.log("Dialog is closing by other means");
        console.log("Accepted "  + acceptChanges);
        if (acceptChanges)
        {
            //stack.saveEdit()
        }
        else
        {
            //stack.cancel();
        }
    }
    function cancel()
    {
        acceptChanges = false;
        stack.cancel();
    }
    function apply()
    {
        acceptChanges = true;
        stack.saveEdit()
    }

    function init()
    {
        //editor.init()
        stack.setUID(stackSelectView.selectedId)
    }


    NTRectangle {
        id: background
        anchors.fill: parent
        anchors.margins: -6
        SelectStackTemplateModel{
            targetIndex: 0
            id: selectStackMode
        }
        NTMapObjectSelectView {
            id: stackSelectView
            anchors.bottom: parent.bottom
            anchors.left: parent.left
            anchors.top: parent.top
            width: 300
            model:selectStackMode
            selectedId: selectStackMode.selectedId()
            onItemSelected: itemId => {
                selectedId = itemId
                stack.setUID(itemId)
            }
        }
        StackTemplateEditor
        {
            id: stack
        }
        Connections {
            target: stack
            function onDataChanged() {
                unitsView.reload()
            }
            function onLoadFinished() {
                selectOrderTargetWidget.visible = false;
            }
        }
        NTRectangle{
            id: editorRect
            anchors.top: parent.top
            anchors.bottom: parent.bottom
            anchors.right: parent.right
            anchors.left: leftPanel.right
        }

        TextField {
            id: nameEdit
            height: 36
            anchors.top: editorRect.top
            anchors.margins: 5
            font.pointSize: style.fontSize
            anchors.left: editorRect.left
            color: "black"
            width: 415
            text: stack.name
            horizontalAlignment: TextInput.AlignHCenter
            selectByMouse: true
            onTextChanged: {stack.name = text}
        }
        NTRectangle{
            id: iconRect
            width: 180
            height: 180
            anchors.horizontalCenter: unitsView.horizontalCenter
            anchors.top: nameEdit.bottom
            Image {
                anchors.horizontalCenter: parent.horizontalCenter
                anchors.verticalCenter: iconVCenter.verticalCenter
                id: icon
                source: "image://provider/" + stack.icon
            }
            Item {
                id: iconVCenter
                anchors.bottom: iconTypeSpin.top
                anchors.bottomMargin: 25
                height: 1
                width: parent.width
            }
            NTSpinBoxInt {
                id: iconTypeSpin
                min: -1
                max: 8
                value: stack.rotation
                onValueModified: value =>
                {
                    if (value === 8)
                        value = 0;
                    if (value === -1)
                        value = 7;
                    stack.rotation = value
                }
                anchors.bottom: parent.bottom
                anchors.left: parent.left
                height: 25
                width: 90
                anchors.margins: 2
                anchors.leftMargin: 35
            }
        }

        GroupView
        {
            id: unitsView
            parentItem: item
            garrison: stack.stack
            isLeft: true
            anchors.left: editorRect.left
            anchors.margins: 5
            anchors.top: iconRect.bottom
            width: 220
            height: 400
        }

        Text {
            anchors.right: ownerCombo.left
            anchors.margins: 5
            anchors.verticalCenter: nameEdit.verticalCenter
            height: 25
            width: 80
            horizontalAlignment: TextInput.AlignHCenter
            verticalAlignment: TextInput.AlignVCenter
            font.pointSize: style.fontSize
            text: translate.tr("Owner")
        }
        ComboBox {
            id: ownerCombo
            anchors.right: editorRect.right
            anchors.margins: 5
            anchors.verticalCenter: nameEdit.verticalCenter
            height: 25
            width: 160
            model: stack.ownerModel
            currentIndex: stack.ownerModel.ownerIndex
            textRole: "Name"
            onCurrentIndexChanged: {
                stack.ownerModel.ownerIndex = currentIndex
            }
        }

        NTRectangle{
            id: priorityEdit
            anchors.left: unitsView.right
            anchors.right: parent.right
            anchors.top: ownerCombo.bottom
            anchors.topMargin: 10
            anchors.rightMargin: 5
            anchors.leftMargin: 5
            height: 30
            Row{
                Text {
                    height: 30
                    width: 80
                    horizontalAlignment: TextInput.AlignHCenter
                    verticalAlignment: TextInput.AlignVCenter
                    font.pointSize: style.fontSize
                    text: translate.tr("Priority:")
                }

                NTSpinBoxInt {
                    x: 80
                    min: 0
                    max: 6
                    value: stack.priority
                    onValueModified: (value) => stack.priority = value
                    height: 26
                    width: 100
                    anchors.margins: 2
                }

                Item{
                    width: 40
                }

                NTCheckBox {
                    id: ignoreCheckbox
                    width: 80
                    height: 30
                    checked: stack.ignore
                    text: qsTr("Ignore AI")
                    onClicked: (checked)=> {
                        stack.ignore = checked
                    }
                }
            }
        }



        Item{
            anchors.left: unitsView.right
            anchors.bottom: editorRect.bottom
            anchors.right: editorRect.right
            anchors.top: priorityEdit.bottom
            anchors.margins: 0
            anchors.topMargin: 5
            anchors.rightMargin: 0
            anchors.leftMargin: 5
            OrderEditor {
                id: orderRect
                orderModel: stack.orderEditor
                anchors.top: parent.top
                anchors.left: parent.left
                anchors.right: parent.right
                height: 200
                anchors.margins: 0
                onOrderSelected: hasTarget => {
                    selectOrderTargetWidget.visible = hasTarget
                }
            }

            SelectOrderTargetWidget {
                anchors.right: parent.right
                anchors.left: parent.left
                anchors.margins: 0
                anchors.top: orderRect.bottom
                height: 400
                id: selectOrderTargetWidget
                target: stack.orderEditor.orderTarget
                visible: false
                onApply: {
                    stack.orderEditor.orderTarget = selectOrderTargetWidget.target
                }
            }
        }

        NTLeftPanel
        {
            id: leftPanel
            width: 300
            anchors.top: parent.top
            anchors.left: stackSelectView.right
            anchors.bottom: parent.bottom
            anchors.margins: 0
            unitsView.leader: !unitsView.garrison.hasLeader
            eventsModel: stack.events
            spellsVisible: false
            itemsVisible: false
            currentIndex: 1
        }

        Button {
            id: applyButton
            text: translate.tr("Save")
            onClicked: {
                apply()
                stack.startEdit()
            }
            z: 1
            anchors.bottom: parent.bottom
            anchors.right: parent.right
            anchors.margins: 10
            width: style.buttonWidht
            height: style.buttonHeight
        }
        Button {
            id: resetButton
            text: translate.tr("Reset")
            onClicked: {
                cancel()
                stack.startEdit()
            }
            z: 1
            anchors.bottom: parent.bottom
            anchors.left: editorRect.left
            anchors.leftMargin: 4
            width: style.buttonWidht
            height: style.buttonHeight
        }
        Button {
            text:  translate.tr("Close")
            onClicked: {
                cancel()
                item.close()
            }
            z: 1
            anchors.bottom: applyButton.bottom
            anchors.right: applyButton.left
            anchors.rightMargin: 10
            width: style.buttonWidht
            height: style.buttonHeight
        }
    }
}

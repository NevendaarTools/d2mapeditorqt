import QtQuick 2.12
import QtQuick.Controls 2.14
import QMLCapitalEditor 1.0
import Qt.labs.qmlmodels 1.0
import QtQuick.Layouts 1.15

NTEditorWindow {
    id: item
	Rectangle{
        anchors.fill: parent
        color: "transparent"

        Connections {
            target: capital
            function onDataChanged() {
                garrisonView.reload()
                visiterView.reload()
            }
        }
        TextField {
            id: nameEdit
            width: 510
            height: 36
            anchors.bottom: visiterView.top
            anchors.bottomMargin: 5
            font.pointSize: 13
            anchors.horizontalCenter: parent.horizontalCenter
            color: "black"
            text: capital.name
            horizontalAlignment: TextInput.AlignHCenter
            selectByMouse: true
            onTextChanged: {capital.name = text}
        }

        GroupView
        {
            id: visiterView
            parentItem: item
            garrison: capital.visiter
            isLeft: true
            anchors.right: parent.horizontalCenter
            anchors.rightMargin: 140
            anchors.bottom: parent.verticalCenter
            anchors.bottomMargin: -90
            width: 220
            height: 400
        }
        GroupView
        {
            id: garrisonView
            parentItem: item
            garrison: capital.garrison
            isLeft: false
            anchors.left: parent.horizontalCenter
            anchors.leftMargin: 155
            anchors.bottom: parent.verticalCenter
            anchors.bottomMargin: -90
            width: 220
            height: 400
        }

        InventoryWidget {
            id: inventory1Grid
            model: capital.garrisonInventory
            anchors.left: garrisonView.left
            anchors.leftMargin: -135
            anchors.top: garrisonView.bottom
            anchors.topMargin: 20
            anchors.bottom: parent.bottom
            anchors.bottomMargin: 15
            width: 360
        }

        InventoryWidget {
            id: inventory2Grid
            model: capital.visiterInventory
            anchors.right: visiterView.right
            anchors.rightMargin: -135
            anchors.top: garrisonView.bottom
            anchors.topMargin: 20
            anchors.bottom: parent.bottom
            anchors.bottomMargin: 15
            width: 360
        }

        Rectangle{
            id: mainRect
            anchors.left: parent.horizontalCenter
            anchors.leftMargin: -width / 2 + 5
            anchors.bottom: parent.verticalCenter
            anchors.bottomMargin: 10
            color: "grey"
            border.color: "red"
            width: 280
            height: 300
        }
    }
    NTLeftPanel
    {
        id: leftPanel
        width: 300
        anchors.top: parent.top
        anchors.left: parent.left
        anchors.bottom: parent.bottom
        anchors.margins: 0
        unitsView.leader: !unitsView.garrison.hasLeader
    }
}

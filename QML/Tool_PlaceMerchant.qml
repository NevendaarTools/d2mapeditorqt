import QtQuick 2.15
import QtQuick.Layouts 1.2
import QtQuick.Controls 2.0
import QMLMainToolBarController 1.0
import QMLPlaceMerchantTool 1.0
import QMLToolsProvider 1.0
import QMLGroupUnitsEditor 1.0
import QMLStyleManager 1.0

Item {
    id: tool_PlaceMerchant
    StyleManager
    {
        id: style
    }
    property ToolsProvider toolsProv
    property PlaceMerchantTool toolPlaceMerchant
    GroupUnitsEditor
    {
        id: unitsEditor
        onUnitsChanged:{
            //unitsView.reload();
        }
    }

    function init(tool)
    {
        toolsProv = tool;
        toolPlaceMerchant = toolsProv.currentToolObj();
        name.text = toolPlaceMerchant.toolName()
        toolPlaceMerchant.init()
        imageSpin.value = toolPlaceMerchant.imageIndex
    }

    Text {
        id: name
        height: 24
        horizontalAlignment: TextInput.AlignLeft
        verticalAlignment: TextInput.AlignVCenter
        font.pointSize: 14
        anchors.top: parent.top
        anchors.left: parent.left
        anchors.right: parent.right
        anchors.margins: 5
    }
    ImageButton{
        anchors.top: parent.top
        anchors.right: parent.right
        width: 24
        height: 24
        anchors.margins: 5
        source: "image://provider/Exit"
        onClicked:
        {
            toolsProv.closeTool();
        }
    }

    NTVerticalTabBar {
        id: traderTypeBar
        currentIndex: toolPlaceMerchant.merchType
        anchors.right: parent.right
        anchors.left: parent.left
        anchors.top: name.bottom
        height: style.buttonHeight * 4 + 5
        anchors.margins: 5
        model: ListModel {
            ListElement { name: "Items" }
            ListElement { name: "Spells" }
            ListElement { name: "Units" }
            ListElement { name: "Trainer" }
        }
        onCurrentIndexChanged:
        {
            toolPlaceMerchant.merchType = currentIndex
        }
    }
    Item{
        anchors.horizontalCenter: parent.horizontalCenter
        anchors.top: traderTypeBar.bottom
        width: parent.width - 10
        height: width
        id: iconRect
        anchors.topMargin: 10
        Image {
            anchors.centerIn: parent
            id: icon
            sourceSize: Qt.size(160, 160)
            source: "image://provider/IsoCmon-IsoAnim-" + toolPlaceMerchant.imageName(toolPlaceMerchant.imageIndex)
        }
    }

    NTSpinBoxInt {
        id: imageSpin
        height: 22
        width: 80
        anchors.top: iconRect.bottom
        min: 0
        max: 99
        backgroundRect.visible: true
        anchors.horizontalCenter: parent.horizontalCenter
        editable: true
        image.width: 0
        anchors.topMargin: -30

        onValueChanged:
        {
            toolPlaceMerchant.imageIndex = value
        }
    }
}

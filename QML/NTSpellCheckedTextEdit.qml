import QtQuick 2.15
import QMLMainMenuController 1.0
import QtQuick.Layouts 1.15
import QtQuick.Controls 2.15
import QMLStyleManager 1.0
import QMLDynamicImageItem 1.0
import NevendaarTools 1.0


TextEdit {
    StyleManager
    {
        id: style
    }
    id: textEdit
    font.pointSize: style.fontSize
    selectionColor:  "grey"
    selectByMouse: true
    SpellCheckWrapper {
        document: textEdit.textDocument
    }
    NTRectangle{
        anchors.fill: parent
        anchors.margins: -3
        z: -1
        color: "white"
    }
}


import QtQuick 2.0
import QtQuick 2.11
import QtQuick.Controls 2.11
import QMLStyleManager 1.0
import NevendaarTools 1.0
import NTModels 1.0

NTRectangle{
    FilterAreaWidget {
        id: filterArea
        anchors.top: parent.top
        anchors.topMargin: 5
        height: 32
        width: parent.width - 40
        x:5
        openToRight: false
        tagListHeight: 160
        sortVisible: false
        settingsModel: objectList.model.filterSettings
    }
    id: mapObjectSelectView
    property alias model: objectList.model
    property string selectedId: ""
    signal itemSelected(string itemId)
    signal itemDBLClicked(string itemId)
    Item{
        anchors.right: parent.right
        anchors.top: parent.top
        width: 5
        height: 5
        id: previewAnchor
    }

    ListView {
        id: objectList
        anchors.fill: parent
        anchors.margins: 5
        anchors.topMargin: 40
        spacing: 5
        model: 0
        clip: true
        delegate: Rectangle {
            width: objectList.width
            height: 30


            NTRectangle {
                id: hoverRect
                color: "lightgray"
                border.color: "transparent"
                border.width: 2
                anchors.fill: parent
                visible: true
            }

            Text {
                anchors.centerIn: parent
                text: Name// + " " + index + " " + Id + " " + mapObjectSelectView.selectedId
            }
            NTRectangle {
                id: mainRect
                color: "transparent"
                border.color: "black"
                border.width: 2
                anchors.fill: parent
                visible: Id === mapObjectSelectView.selectedId
            }

            MouseArea {
                id: dragArea
                anchors.fill: parent
                hoverEnabled: true
                onEntered: {
                    overlay.showPreview(previewAnchor, Id)
                }
                onExited: {
                    overlay.hidePreview()
                }
                onClicked: {
                    itemSelected(Id)
                }
                onDoubleClicked: {
                    itemDBLClicked(Id)
                }
            }
        }
    }

}

import QtQuick 2.15
import QtQuick.Layouts 1.2
import QtQuick.Controls 2.0
import QMLMainToolBarController 1.0
import QMLPlaceTombTool 1.0
import QMLToolsProvider 1.0

Item {
    id: tool_PlaceTomb
    property ToolsProvider toolsProv
    property PlaceTombTool toolPlaceTomb

    function init(tool)
    {
        toolsProv = tool;
        toolPlaceTomb = toolsProv.currentToolObj();
        name.text = toolPlaceTomb.toolName()
        ownerCombo.model = toolPlaceTomb.killer
        nameEdit.text = toolPlaceTomb.stackName
        ownerCombo.currentIndex = toolPlaceTomb.killer.ownerIndex
        daySpinBox.text = toolPlaceTomb.day
    }

    Connections {
        target: toolPlaceTomb

    }
    Text {
        id: name
        height: 24
        horizontalAlignment: TextInput.AlignLeft
        verticalAlignment: TextInput.AlignVCenter
        font.pointSize: 14
        anchors.top: parent.top
        anchors.left: parent.left
        anchors.right: parent.right
        anchors.margins: 5
    }
    ImageButton{
        anchors.top: parent.top
        anchors.right: parent.right
        width: 24
        height: 24
        anchors.margins: 5
        source: "image://provider/Exit"
        onClicked:
        {
            toolsProv.closeTool();
        }
    }
    Column{
        anchors.fill: parent
        anchors.topMargin: 45
        anchors.margins: 5
        Text {
            id: defeatedLabel
            height: 24
            horizontalAlignment: TextInput.AlignHCenter
            verticalAlignment: TextInput.AlignVCenter
            font.pointSize: 14
            text: "Defeated stack name: "
        }
        TextField {
            id: nameEdit
            height: 36
            font.pointSize: style.fontSize
            color: "black"
            text: ""
            horizontalAlignment: TextInput.AlignHCenter
            width: parent.width
            selectByMouse: true
            onTextChanged: {toolPlaceTomb.stackName = text}
        }
        Text {
            id: killerLabel
            height: 24
            horizontalAlignment: TextInput.AlignHCenter
            verticalAlignment: TextInput.AlignVCenter
            font.pointSize: 14
            text: "Winner: "
        }
        ComboBox {
            id: ownerCombo
            anchors.margins: 5
            height: 25
            width: parent.width
            model: 0
            currentIndex: 0
            textRole: "Name"
            onCurrentIndexChanged: {
                toolPlaceTomb.killer.ownerIndex = currentIndex
            }
        }
        Text {
            id: dayLabel
            height: 24
            horizontalAlignment: TextInput.AlignHCenter
            verticalAlignment: TextInput.AlignVCenter
            font.pointSize: 14
            text: "Day:"
        }
        NTSpinBoxInt {
            id: daySpinBox
            min: 0
            max: 9990
            text: "0"
            onValueChanged: toolPlaceTomb.day = value
            height: 26
            width: 100
            anchors.margins: 2
        }
    }


}

import QtQuick 2.15
import QtQuick.Layouts 1.2
import QtQuick.Controls 2.0
import QMLMainToolBarController 1.0
import QMLPlaceStackTool 1.0
import QMLToolsProvider 1.0
import QMLGroupUnitsEditor 1.0

Item {
    id: tool_PlaceStack
    property ToolsProvider toolsProv
    property PlaceStackTool toolPlaceStack
    GroupUnitsEditor
    {
        id: unitsEditor
        onUnitsChanged:{
            unitsView.reload();
        }
    }

    function init(tool)
    {
        toolsProv = tool;
        toolPlaceStack = toolsProv.currentToolObj();
        name.text = toolPlaceStack.toolName()
        toolPlaceStack.stack = unitsEditor
        toolPlaceStack.init()
        unitsView.reload()
        settingsList.settingsModel = toolPlaceStack.stackOptions
        orderRect.orderModel = toolPlaceStack.orderEditor
        selectOrderTargetWidget.target = toolPlaceStack.orderEditor.orderTarget
        generateStackCheckbox.checked = toolPlaceStack.generateStack
        lootSettingsList.settingsModel = toolPlaceStack.lootOptions
        generateLootCheckbox.checked = toolPlaceStack.generateLoot
        //listView.model = toolPlaceStack.options
        ownerCombo.model = toolPlaceStack.ownerModel
        ownerCombo.currentIndex = toolPlaceStack.ownerModel.ownerIndex
        inventoryGrid.model = toolPlaceStack.inventory
    }

    Connections {
        target: toolPlaceStack
        onChanged: ()=>{
            unitsView.reload()
        }
    }
    Text {
        id: name
        height: 24
        horizontalAlignment: TextInput.AlignLeft
        verticalAlignment: TextInput.AlignVCenter
        font.pointSize: 14
        anchors.top: parent.top
        anchors.left: parent.left
        anchors.right: parent.right
        anchors.margins: 5
    }
    ImageButton{
        anchors.top: parent.top
        anchors.right: parent.right
        width: 24
        height: 24
        anchors.margins: 5
        source: "image://provider/Exit"
        onClicked:
        {
            toolsProv.closeTool();
        }
    }
    ComboBox {
        id: ownerCombo
        anchors.top: parent.top
        anchors.margins: 5
        anchors.topMargin: 45
        anchors.right: parent.right
        anchors.left: parent.left
        height: 25
        model: 0//controller.generatorsListModel
        currentIndex: 0//controller.selectedGenerator
        textRole: "Name"
        onCurrentIndexChanged: {
            toolPlaceStack.ownerModel.ownerIndex = currentIndex
        }
    }
    OrderEditor {
        id: orderRect
        anchors.top: ownerCombo.bottom
        anchors.topMargin: 2
        width: parent.width
        height: 65
        descEnabled: false
        onOrderSelected: (hasTarget)=>{
            selectOrderTargetWidget.visible = hasTarget
            if (hasTarget)
                selectOrderTargetWidget.target = toolPlaceStack.orderEditor.orderTarget
        }
    }
    SelectOrderTargetWidget {
        anchors.top: orderRect.bottom
        anchors.margins: 5
        anchors.right: parent.right
        anchors.left: parent.left
        height: visible?240:0
        id: selectOrderTargetWidget
        visible: false
        onApply: {
            toolPlaceStack.orderEditor.orderTarget = selectOrderTargetWidget.target
        }
    }
    Rectangle{
        id: genStackRect
        property bool propVisible: false
        anchors.top: selectOrderTargetWidget.bottom
        anchors.margins: 5
        anchors.right: parent.right
        anchors.left: parent.left
        height: 20
        ImageButton
        {
            height: 20
            width: 20
            id: hideStackButton
            source: "image://provider/Settings"
            onClicked:
            {
                genStackRect.propVisible = !genStackRect.propVisible
            }
        }
        ImageButton
        {
            height: 20
            width: 20
            anchors.right: parent.right
            id: regenerateStackButton
            source: "image://provider/Refresh"
            onClicked:
            {
                toolPlaceStack.onGenerateStackRequested()
            }
        }
        NTCheckBox {
            id: generateStackCheckbox
            anchors.top: parent.top
            anchors.right: regenerateStackButton.left
            anchors.left: hideStackButton.right
            height: 20
            checked: true
            text: "Generate stack"
            onClicked: {
                toolPlaceStack.generateStack = checked
                generateStackCheckbox.checked = toolPlaceStack.generateStack
                if (checked)
                    genStackRect.propVisible = true
            }
        }
    }
    Item
    {
        id: settingsArea
        anchors.top: genStackRect.bottom
        anchors.margins: 5
        anchors.right: parent.right
        anchors.left: parent.left
        height: visible ? 400 : 0
        visible: generateStackCheckbox.checked && genStackRect.propVisible
        OptionsList {
            id: settingsList
            anchors.verticalCenter: parent.verticalCenter
            width: parent.width
            height: parent.height / settingsList.scale
            spacing: 1
            scale: 0.8
        }
    }

    Item
    {
        id: unitsRect
        anchors.top: settingsArea.bottom
        anchors.margins: 5
        anchors.right: parent.right
        anchors.left: parent.left
        height: 300
        GroupView
        {
            id: unitsView
            parentItem: tool_PlaceStack
            garrison: unitsEditor
            isLeft: false
            isEditable: true
            anchors.centerIn: parent
            width: 220
            height: 400
            scale: 0.7
        }
    }
    Rectangle{
        id: genLootRect
        property bool propVisible: false
        anchors.top: unitsRect.bottom
        anchors.margins: 5
        anchors.right: parent.right
        anchors.left: parent.left
        height: 20
        ImageButton
        {
            height: 20
            width: 20
            id: hideLootButton
            source: "image://provider/Settings"
            onClicked:
            {
                genLootRect.propVisible = !genLootRect.propVisible
            }
        }
        ImageButton
        {
            height: 20
            width: 20
            anchors.right: parent.right
            id: regenerateLootButton
            source: "image://provider/Refresh"
            onClicked:
            {
                toolPlaceStack.onGenerateLootRequested()
            }
        }
        NTCheckBox {
            id: generateLootCheckbox
            anchors.top: parent.top
            anchors.right: regenerateLootButton.left
            anchors.left: hideLootButton.right
            height: 20
            checked: true
            text: "Generate loot"
            onClicked: {
                toolPlaceStack.generateLoot = checked
                generateLootCheckbox.checked = toolPlaceStack.generateLoot
                if (checked)
                    genLootRect.propVisible = true
            }
        }
    }
    Item
    {
        id: lootSettingsArea
        anchors.top: genLootRect.bottom
        anchors.margins: 5
        anchors.right: parent.right
        anchors.left: parent.left
        height: visible ? 400 : 0
        visible: generateLootCheckbox.checked && genLootRect.propVisible
        OptionsList {
            id: lootSettingsList
            anchors.verticalCenter: parent.verticalCenter
            width: parent.width
            height: parent.height / lootSettingsList.scale
            spacing: 1
            scale: 0.8
        }
    }
    InventoryWidget {
        id: inventoryGrid
        presetVisible: false
//        editable: false
        model: 0
        //presetType: "stack"
        //anchors.horizontalCenter: parent.horizontalCenter
        anchors.left: parent.left
//        anchors.leftMargin: 10
        anchors.right: parent.right
        anchors.top: lootSettingsArea.bottom
        anchors.margins: 2
        anchors.bottom: parent.bottom
        cellW: 90
        cellH: 90
    }
}

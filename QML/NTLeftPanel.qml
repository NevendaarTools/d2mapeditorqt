import QtQuick 2.0
import QtQuick.Layouts 1.15

Item {
    NTRectangle{
        anchors.fill: bar
        anchors.leftMargin: -5
        anchors.topMargin: -5
    }
    NTRectangle{
        anchors.fill: parent
    }
    property alias itemsVisible: itemsButton.visible
    property alias unitsVisible: unitsButton.visible
    property alias spellsVisible: spellsButton.visible
    property alias generatorVisible: generatorButton.visible
    property alias eventsVisible: eventsButton.visible
    property alias events2Visible: eventsButton2.visible

    property alias currentIndex: bar.currentIndex
    property alias eventsObjectUID: eventsView.objectUID
    property alias eventsObject2UID: eventsView2.objectUID
    property alias unitsView: selectUnitsView
    Row  {
        id: bar
        property int currentIndex : 0
        anchors.top: parent.top
        anchors.left: parent.left
        anchors.right: parent.right
        anchors.leftMargin: 5
        anchors.topMargin: 5
        height: 35
        spacing: 5
        NTImageButton {
            id: itemsButton
            height: 30
            width: 30
            source: "image://provider/Items"
            property int index : 0
            onClicked: bar.currentIndex = index
            border.visible: bar.currentIndex === index
            border.border.width: 4
        }
        NTImageButton {
            id: unitsButton
            height: 30
            width: 30
            source: "image://provider/Units"
            property int index : 1
            onClicked: bar.currentIndex = index
            border.visible: bar.currentIndex === index
            border.border.width: 4
        }
        NTImageButton {
            id: spellsButton
            height: 30
            width: 30
            source: "image://provider/Spells"
            property int index : 2
            onClicked: bar.currentIndex = index
            border.visible: bar.currentIndex === index
            border.border.width: 4
        }
        NTImageButton {
            id: eventsButton
            height: 30
            width: 30
            source: "image://provider/Events"
            property int index : 3
            onClicked: bar.currentIndex = index
            border.visible: bar.currentIndex === index
            border.border.width: 4
        }
        NTImageButton {
            id: eventsButton2
            height: 30
            width: 30
            source: "image://provider/Events"
            property int index : 4
            onClicked: bar.currentIndex = index
            border.visible: bar.currentIndex === index
            visible: false
            border.border.width: 4
        }
        NTImageButton {
            id: generatorButton
            height: 30
            width: 30
            source: "image://provider/Generator"
            property int index : 5
            onClicked: bar.currentIndex = index
            border.visible: bar.currentIndex === index
            border.border.width: 4
        }

    }
    StackLayout {
        anchors.fill: parent
        anchors.topMargin: 40
        anchors.margins: 2
        currentIndex: bar.currentIndex

        SelectItemWidget
        {

        }
        SelectUnitWidget
        {
            id: selectUnitsView
        }
        SelectSpellWidget
        {

        }
        Component_EventsView {
            id: eventsView
            parentItem: item
        }
        Component_EventsView {
            id: eventsView2
            parentItem: item
        }
        Item {
            Text {
                anchors.centerIn: parent
                text: qsTr("Regeneration")
            }
        }
    }

}

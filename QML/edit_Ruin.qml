import QtQuick 2.12
import QtQuick.Controls 2.14
import QMLVillageEditor 1.0
import Qt.labs.qmlmodels 1.0
import QtQuick.Layouts 1.15
import QMLStyleManager 1.0

NTEditorWindow {
    id: item
    StyleManager
    {
        id: style
    }
    Connections {
        target: ruin
        function onDataChanged() {
            unitsView.reload()
        }
    }
    Item{
        id: central
        anchors.left: leftPanel.right
        anchors.right: parent.right
    }

    TextField {
        id: nameEdit
        height: 36
        anchors.top: parent.top
        anchors.margins: 5
        anchors.topMargin: 100
        font.pointSize: style.fontSize
        anchors.horizontalCenter: central.horizontalCenter
        color: "black"
        width: 550
        text: ruin.name
        horizontalAlignment: TextInput.AlignHCenter
        selectByMouse: true
        onTextChanged: {ruin.name = text}
    }
    GroupView
    {
        id: unitsView
        parentItem: item
        garrison: ruin.units
        isLeft: true
        anchors.left: nameEdit.left
        anchors.topMargin: 5
        anchors.top: nameEdit.bottom
        width: 220
        height: 400
    }

    Rectangle{
        id: iconRect
        color: "gray"
        border.color: "black"
        border.width: 2
        width: 180
        height: 180
        anchors.left: unitsView.right
        anchors.leftMargin: 5
        anchors.top: unitsView.top
        Image {
            anchors.top: parent.top
            anchors.topMargin: 10
            anchors.horizontalCenter: parent.horizontalCenter
            sourceSize: Qt.size(160, 160)
            id: icon
            source: "image://provider/IsoCmon-IsoAnim-G000RU00000" + ruin.image
        }
        NTSpinBoxInt {
            min: 0
            max: ruin.maxImage
            value: ruin.image
            anchors.bottom: parent.bottom
            anchors.left: parent.left
            height: 25
            width: 90
            anchors.margins: 2
            anchors.leftMargin: 35
            onValueModified: value =>
            {
                ruin.image = value
            }
        }
    }
    NTRectangle{
        id: costRect
        anchors.top: nameEdit.bottom
        anchors.topMargin: 5
        anchors.left: iconRect.right
        anchors.leftMargin: 5
        width: 140
        height: 180
        NTCostView {
            id: costView
            value: ruin.cost
            anchors.fill: parent
            anchors.margins: 10
            height: 180
            width: 112
            orientation: Qt.Vertical
            editable: true
            onValueModified: value => {
                ruin.cost = value
            }
        }
    }
    ItemWidget
    {
        id: itemView
        itemId: ruin.item
        anchors.top: iconRect.bottom
        anchors.topMargin: 5
        anchors.left: iconRect.left
        anchors.right: costRect.right
        anchors.bottom: unitsView.bottom
        DropArea {
            id:dropArea
            anchors.fill: parent
            //anchors.margins: 10
            Rectangle {
                id: dragTargetArea
                anchors.fill: parent
                border.color:  "gold"
                border.width: 3
                visible: parent.containsDrag
                color: "transparent"
            }

            onEntered: {
            }
            onExited: {
            }
            onDropped: drop =>{
                if (drop.getDataAsString("mode") === "ItemAdd")
                {
                    ruin.item = drop.getDataAsString("id")
                }
            }
        }
        ImageButton{
            anchors.right: parent.right
            anchors.top:  parent.top
            width: 20
            height: 20
            source: "image://provider/Delete"
            visible: ruin.item !== ""
            onClicked: {
                ruin.item = ""
            }
        }
    }

    NTRectangle{
        id: priorityEdit
        anchors.left: nameEdit.left
        anchors.right: nameEdit.right
        anchors.top: unitsView.bottom
        anchors.topMargin: 5
        height: 30
        Row{
            Text {
                height: 30
                width: 80
                horizontalAlignment: TextInput.AlignHCenter
                verticalAlignment: TextInput.AlignVCenter
                font.pointSize: style.fontSize
                text: translate.tr("Priority:")
            }
            NTSpinBoxInt {
                min: 0
                max: 6
                value: ruin.priority
                onValueModified: value => {
                    if (value !== ruin.priority)
                        ruin.priority = value
                }
                height: 26
                width: 100
                anchors.margins: 2
            }
            Item{
                width: 135
                height: 30
            }

            NTCheckBox {
                id: ignoreCheckbox
                width: 80
                height: 30
                checked: false//stack.ignore
                text: translate.tr("Ruined")
                onClicked: {
                    //stack.ignore = checked
                }
            }
        }
    }

    NTLeftPanel
    {
        id: leftPanel
        width: 300
        anchors.top: parent.top
        anchors.left: parent.left
        anchors.bottom: parent.bottom
        anchors.margins: 0
        spellsVisible: false
        eventsObjectUID: ruin.uid
    }
}

import QtQuick 2.12
import QtQuick.Controls 2.14
import QMLVillageEditor 1.0
import Qt.labs.qmlmodels 1.0
import QtQuick.Layouts 1.15

Item
{
    property alias model: itemsGrid.model
    property alias cellWidth: itemsGrid.cellWidth
    property alias presetType: presets.presetType
    id: unitsItem

    Rectangle
    {
        color: "gray"
        border.color: "black"
        border.width: 1
        anchors.fill: itemsGrid
        anchors.margins: -10
    }
    PresetPanel {
        id: presets
        anchors.left: parent.left
        anchors.right: parent.right
        anchors.top: parent.top
        height: 28
        onPresetLoaded: {
            itemsGrid.model.loadPreset(preset);
        }
        onPresetSaved: {
            presets.controller.savePreset(name, itemsGrid.model.toPreset());
        }
    }
    GridView{
        anchors.fill: parent
        anchors.topMargin: 35
        id:itemsGrid
        cellWidth: 240; cellHeight: 84
        ScrollBar.vertical: ScrollBar {
            active: true
        }
        model: 0
        clip:true
        Component {
            id: itemDelegate
            Rectangle {
                id: wrapper
                width: itemsGrid.cellWidth - 2
                height: itemsGrid.cellHeight - 2
                border.color: "black"
                border.width: 2
                color: "transparent"

                Text {
                    id: name
                    anchors.top: wrapper.top
                    anchors.left: icon.right
                    anchors.right: parent.right
                    wrapMode: Text.WordWrap
                    text: Name
                    horizontalAlignment: TextInput.AlignHCenter
                    font.bold: true
                }
                Text {
                    id: levelText
                    anchors.top: name.bottom
                    anchors.topMargin: 5
                    anchors.left: icon.right
                    width: 64
                    height: 18
                    horizontalAlignment: TextInput.AlignHCenter
                    verticalAlignment: TextInput.AlignVCenter
                    text: "Level:"
                }
                SpinBox {
                    height: 18
                    width: 64
                    id: levelSpin
                    down.indicator.width: 18
                    up.indicator.width: 18
                    anchors.top: levelText.top
                    anchors.topMargin: 0
                    anchors.left: levelText.right
                    value: Level
                    from: BaseLevel
                    to: itemsGrid.model.unitMaxLvl()

                    onValueChanged:
                    {
                        itemsGrid.model.setUnitLvl(index, value)
                    }
                }

                Text {
                    id: countText
                    anchors.top: levelText.bottom
                    anchors.topMargin: 2
                    anchors.left: icon.right
                    width: 64
                    height: 18
                    horizontalAlignment: TextInput.AlignHCenter
                    verticalAlignment: TextInput.AlignVCenter
                    text: "Count:"
                }
                NTCheckBox {
                    id: countValueText
                    anchors.top: levelText.bottom
                    anchors.topMargin: 2
                    anchors.left: countText.right
                    width: 64
                    height: 18
                    text: "Unlimited"
                    checked: Count === 0
                    onClicked: {
                        if (checked)
                            itemsGrid.model.setUnitCount(index, 0)
                        else
                            itemsGrid.model.setUnitCount(index, 1)
                    }
                }

                Image{
                    id: icon
                    visible: Id !== ""
                    source: "image://provider/" + Icon
                    anchors.verticalCenter: wrapper.verticalCenter
                    anchors.left: wrapper.left
                    anchors.leftMargin: 2
                    sourceSize: Qt.size(72, 72)
                    //anchors.top: parent.top
                    //anchors.margins: {5;5;5;5}
                    smooth: true
                }
                MouseArea {
                    id: dragArea
                    anchors.fill: icon
                    drag.target: icon
                    hoverEnabled: true
                    onEntered: {
                        overlay.showPreview(dragArea, "Preview/Unit.qml", "#"+Id+":"+Level)
                    }
                    onExited: {
                        overlay.hidePreview()
                    }
                }
                ImageButton{
                    anchors.right: parent.right
                    anchors.top:  parent.top
                    width: 20
                    height: 20
                    source: "image://provider/Delete"
                    visible: icon.visible
                    onClicked: {
                        itemsGrid.model.removeItem(index)
                    }
                }
            }

        }
        delegate: itemDelegate
    }
    DropArea {
        id:dropArea
        anchors.fill: parent
        //anchors.margins: 10
        Rectangle {
            id: dragTargetArea
            anchors.fill: parent
            border.color:  "gold"
            border.width: 3
            visible: parent.containsDrag
            color: "transparent"
        }

        onEntered: {
        }
        onExited: {
        }
        onDropped: {
            if (drop.getDataAsString("mode") === "hire")
            {
                itemsGrid.model.addItem(drop.getDataAsString("id"))
                console.log(drop.getDataAsString("mode"))
                console.log(drop.getDataAsString("id"))
                console.log(drop.getDataAsString("level"))
            }
        }
    }
}

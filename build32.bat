echo on
cls
set VSDIR="e:/Program Files (x86)/Microsoft Visual Studio/2019/Community/VC/Auxiliary/Build/vcvars32.bat"

if not exist %VSDIR% (
   set VSDIR="e:/Program Files (x86)/Microsoft Visual Studio/2017/Professional/VC/Auxiliary/Build/vcvars32.bat"
)

set CUR_DIR=%cd%
call %VSDIR%
cd bin32
PATH=C:\Qt\5.15.2\msvc2019\bin;%PATH%
call "C:\Qt\5.15.2\msvc2019/bin/qmake.exe" D2MapEditor.pro -spec win32-msvc "CONFIG+=release"

cd %CUR_DIR%
del /f /s /q bin32 1>nul
rmdir bin32 \s \q
mkdir bin32
xcopy %CUR_DIR%\build\build_64\release\D2MapEditor.exe %CUR_DIR%\bin32\ /R /I /Y /S
xcopy %CUR_DIR%\Images\* bin32\Images /R /I /Y /S
xcopy %CUR_DIR%\QML\* bin32\QML /R /I /Y /S
xcopy %CUR_DIR%\Presets\* bin32\Presets /R /I /Y /S
xcopy %CUR_DIR%\Generators\* bin32\Generators /R /I /Y /S
xcopy %CUR_DIR%\Translations\* bin32\Translations /R /I /Y /S
xcopy %CUR_DIR%\ExtraDeps\* bin32\ /R /I /Y /S

cd "c:/Qt/5.15.2/msvc2019/bin/"
call windeployqt.exe  -multimedia -no-opengl-sw %CUR_DIR%/bin32
cd %CUR_DIR%
del bin32\vc_redist.x86.exe

call "C:\Program Files\7-Zip\7z.exe" a ./bin32/D2MapEditor_32.zip ./bin32/*
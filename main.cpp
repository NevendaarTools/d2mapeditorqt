#include "mainwindow.h"

#include <QApplication>

#include "QMLHelpers/MainToolBarController.h"
#include "QMLHelpers/MainMenuController.h"
#include "QMLHelpers/MapCreationController.h"
#include "QMLHelpers/Tools/PlaceLMarkTool.h"
#include "QMLHelpers/Tools/PlaceCapitalTool.h"
#include "QMLHelpers/Tools/PlaceStackTool.h"
#include "QMLHelpers/Tools/LayersTool.h"
#include "QMLHelpers/Tools/MapEditTool.h"
#include "QMLHelpers/Tools/ObjectMoveTool.h"
#include "QMLHelpers/Tools/SearchObjectTool.h"
#include "QMLHelpers/Tools/MapGenerationTool.h"
#include "QMLHelpers/Models/FilterSettingsModel.h"
#include "QMLHelpers/Common/StyleManager.h"
#include "QMLHelpers/Common/OverlayWrapper.h"
#include "QMLHelpers/Common/DynamicImageItem.h"
#include "MapObjects/LandmarkObject.h"
#include "MapObjects/StackObject.h"
#include "MapObjects/StackTemplateObject.h"
#include "MapObjects/FortObject.h"
#include "MapObjects/MountainObject.h"
#include "MapObjects/CrystalObject.h"
#include "MapObjects/RodObject.h"
#include "MapObjects/TombObject.h"
#include "MapObjects/RuinObject.h"
#include "MapObjects/TreasureObject.h"
#include "MapObjects/MerchantObject.h"
#include "MapObjects/LocationObject.h"
#include "MapObjects/QuestLine.h"
#include "QMLHelpers/Editors/EditorWidgets.h"
#include "QMLHelpers/Components/EditorComponents.h"
#include "QMLHelpers/Models/Models.h"
#include "QMLHelpers/Tools/Tools.h"
#include "QMLHelpers/Common/PresetController.h"
#include "Engine/GameInstance.h"
#include "Engine/Components/AccessorHolder.h"
#include "MapObjects/ObjectAccessors.h"

#include <QtDebug>
#include <QFile>
#include <QTextStream>
#include <QMovie>

void myMessageHandler(QtMsgType type, const QMessageLogContext &, const QString & msg)
{
    switch (type) {
    case QtInfoMsg:
        Logger::log(msg);
        break;
    case QtDebugMsg:
        Logger::log(msg);
        break;
    case QtWarningMsg:
        Logger::logWarning(msg);
    break;
    case QtCriticalMsg:
        Logger::logError(msg);
    break;
    case QtFatalMsg:
        Logger::logError(msg);
    break;
    }

}

void initUIImages()
{
    QDir dataDir;
    dataDir.setPath("Images");
    QStringList files = dataDir.entryList(QStringList()<<"*.png"<<"*.jpg", QDir::Files);
    foreach (QString fileName, files) {
        QString tmpName = fileName;
        tmpName.remove(tmpName.lastIndexOf("."),4);
        RESOLVE(ResourceManager)->addImage(QImage(dataDir.path() + "/" + fileName), tmpName);
    }

    files = dataDir.entryList(QStringList()<<"*.gif", QDir::Files);
    foreach (QString fileName, files) {
        QString tmpName = fileName;
        tmpName.remove(tmpName.lastIndexOf("."),4);
        QMovie* movie = new QMovie(dataDir.path() + "/" + fileName);
        if (movie->isValid())
        {
            QList <QImage> pixmaps;
            for (int i = 0; i < movie->frameCount(); ++i)
            {
                if (!movie->jumpToFrame(i))
                    continue;
                pixmaps << movie->currentImage();
            }
            RESOLVE(ResourceManager)->addImageList(pixmaps, tmpName);
        }
        else
            qDebug()<< movie->lastErrorString();

    }
}

void initAccessors()
{
    RESOLVE(AccessorHolder)->registerAccessor(MapObject::LandMark,
                                              QSharedPointer<IMapObjectAccessor>(new LandmarkObjectAccessor()));
    RESOLVE(AccessorHolder)->registerAccessor(MapObject::Stack,
                                              QSharedPointer<IMapObjectAccessor>(new StackObjectAccessor()));
    RESOLVE(AccessorHolder)->registerAccessor(MapObject::StackTemplate,
                                              QSharedPointer<IMapObjectAccessor>(new StackTemplateObjectAccessor()));
    RESOLVE(AccessorHolder)->registerAccessor(MapObject::Fort,
                                              QSharedPointer<IMapObjectAccessor>(new FortObjectAccessor()));
    RESOLVE(AccessorHolder)->registerAccessor(MapObject::Mountain,
                                              QSharedPointer<IMapObjectAccessor>(new MountainObjectAccessor()));
    RESOLVE(AccessorHolder)->registerAccessor(MapObject::Crystal,
                                              QSharedPointer<IMapObjectAccessor>(new CrystalObjectAccessor()));
    RESOLVE(AccessorHolder)->registerAccessor(MapObject::Ruin,
                                              QSharedPointer<IMapObjectAccessor>(new RuinObjectAccessor()));
    RESOLVE(AccessorHolder)->registerAccessor(MapObject::Treasure,
                                              QSharedPointer<IMapObjectAccessor>(new TreasureObjectAccessor()));
    RESOLVE(AccessorHolder)->registerAccessor(MapObject::Merchant,
                                              QSharedPointer<IMapObjectAccessor>(new MerchantObjectAccessor()));
    RESOLVE(AccessorHolder)->registerAccessor(MapObject::Location,
                                              QSharedPointer<IMapObjectAccessor>(new LocationObjectAccessor()));
    RESOLVE(AccessorHolder)->registerAccessor(MapObject::Unit,
                                  QSharedPointer<IMapObjectAccessor>(new UnitObjectAccessor()));
    RESOLVE(AccessorHolder)->registerAccessor(MapObject::QuestLine,
                                  QSharedPointer<IMapObjectAccessor>(new QuestLineObjectAccessor()));
    RESOLVE(AccessorHolder)->registerAccessor(MapObject::Rod,
                                  QSharedPointer<IMapObjectAccessor>(new RodObjectAccessor()));
    RESOLVE(AccessorHolder)->registerAccessor(MapObject::Tomb,
                                  QSharedPointer<IMapObjectAccessor>(new TombObjectAccessor()));
    RESOLVE(AccessorHolder)->registerAccessor(MapObject::Player,
                     QSharedPointer<IMapObjectAccessor>(new PlayerObjectAccessor()));
    RESOLVE(AccessorHolder)->registerAccessor(MapObject::SubRace,
                     QSharedPointer<IMapObjectAccessor>(new SubRaceObjectAccessor()));
    RESOLVE(AccessorHolder)->registerAccessor(MapObject::Relation,
                     QSharedPointer<IMapObjectAccessor>(new RelationsAccessor()));
    RESOLVE(AccessorHolder)->registerAccessor(MapObject::Info,
                     QSharedPointer<IMapObjectAccessor>(new MapInfoAccessor()));
    RESOLVE(AccessorHolder)->registerAccessor(MapObject::Event,
                     QSharedPointer<IMapObjectAccessor>(new EventObjectAccessor()));
    RESOLVE(AccessorHolder)->registerAccessor(MapObject::ScenVariable,
                         QSharedPointer<IMapObjectAccessor>(new ScenVariableObjectAccessor()));
    RESOLVE(AccessorHolder)->registerAccessor(MapStatistics,
                                              QSharedPointer<IMapObjectAccessor>(new MapStatisticsAccessor()));
}

void test()
{
    QString path = "E:/Games/Semga_actual/Exports/DevouringMarshes_Rus.sg";
    D2MapModel map;
    map.open(path);
    map.save("E:/Games/Semga_actual/Exports/DevouringMarshes_Rus_test.sg");
}
#include "Common/SpellCheckHighlighter.h"
#include "Common/AutoCompleteModel.h"

int main(int argc, char *argv[])
{
    //Q_INIT_RESOURCE(images);
    QApplication app(argc, argv);
    QCoreApplication::addLibraryPath("ExtraDeps");
    app.setWindowIcon(QIcon("Images/icon.ico"));

    //test();
    // app.setAttribute(Qt::AA_DontCreateNativeWidgetSiblings);
    editorsRegister();
    editorComponentsRegister();
    modelsRegister();
    toolsRegister();
    qmlRegisterType<MainToolBarController>("QMLMainToolBarController", 1, 0,"MainToolBarController");
    qmlRegisterType<MainMenuController>("QMLMainMenuController", 1, 0,"MainMenuController");
    qmlRegisterType<MapCreationController>("QMLMapCreationController", 1, 0,"MapCreationController");
    qmlRegisterType<PresetController>("QMLPresetController", 1, 0,"PresetController");
    qmlRegisterType<StyleManager>("QMLStyleManager", 1, 0,"StyleManager");
    qmlRegisterType<OverlayWrapper>("NevendaarTools", 1, 0, "OverlayWrapper");
    qmlRegisterType<DynamicImageItem>("QMLDynamicImageItem", 1, 0,"DynamicImageItem");
    qmlRegisterType<SpellCheckWrapper>("NevendaarTools", 1, 0, "SpellCheckWrapper");
    qmlRegisterType<AutoCompleteModel>("NevendaarTools", 1, 0, "AutoCompleteModel");


    //qRegisterMetaType<QQmlChangeSet>();
    initUIImages();
    initAccessors();
    GameInstance::initInternal();
    MainWindow window;
    //qInstallMessageHandler(myMessageHandler);
    //window.showMaximized();
    window.resize(1280, 720);
    window.show();

    //window.showFullScreen();

    return app.exec();
}

#include "SelectMapInSagaDialog.h"
#include <QGridLayout>
#include <QHeaderView>

SelectMapInSagaDialog::SelectMapInSagaDialog(QWidget *parent)
    : QDialog{parent}
{
    setWindowFlags(windowFlags() & ~Qt::WindowContextHelpButtonHint | Qt::FramelessWindowHint);
    QGridLayout *grid = new QGridLayout();
    grid->setContentsMargins(3,3,3,3);

    m_nameLabel = new QLabel();
    m_table = new QTableView();
    m_okButton = new QPushButton("Select");
    m_cancelButton = new QPushButton("Cancel");
    grid->addWidget(m_nameLabel, 0,0,1,2);
    grid->addWidget(m_table, 1,0,1,2);
    grid->addWidget(m_okButton, 2,0,1,1);
    grid->addWidget(m_cancelButton, 2,1,1,1);
    setLayout(grid);

    m_model = new QStringListModel();
    m_table->setModel(m_model);
    m_table->horizontalHeader()->setStretchLastSection(true);
    m_table->horizontalHeader()->hide();
    m_table->verticalHeader()->hide();

    connect(m_okButton, SIGNAL(clicked(bool)), SLOT(onOk()));
    connect(m_cancelButton, SIGNAL(clicked(bool)), SLOT(onCancel()));
    connect(m_table, SIGNAL(doubleClicked(QModelIndex)), SLOT(onOk()));
}

void SelectMapInSagaDialog::init(const D2SagaModel &model)
{
    QStringList data;
    for (int i = 0; i < model.mapCount(); ++i)
    {
        data << model.getMap(i).header.name;
    }
    m_model->setStringList(data);
    m_nameLabel->setText(model.name());
    m_selectedMap = 0;
}

const int &SelectMapInSagaDialog::selectedMap() const
{
    return m_selectedMap;
}

void SelectMapInSagaDialog::setSelectedMap(const int &newSelectedMap)
{
    m_selectedMap = newSelectedMap;
}

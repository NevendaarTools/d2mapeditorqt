#include "settingseditordialog.h"
#include "Engine/SettingsManager.h"
#include "Engine/GameInstance.h"
#include "Engine/Components/TranslationHelper.h"

SettingsEditorDialog::SettingsEditorDialog(QWidget *parent)
    : QDialog(parent)
{
    setWindowTitle("Settings");
    resize(800, 600);

    m_tabWidget = new QTabWidget(this);

    QPushButton *applyButton = new QPushButton("Apply", this);

    QVBoxLayout *mainLayout = new QVBoxLayout(this);
    mainLayout->addWidget(m_tabWidget);
    mainLayout->addWidget(applyButton);
    loadSettings();
    connect(applyButton, &QPushButton::clicked, this, &SettingsEditorDialog::applyChanges);
}

void SettingsEditorDialog::loadSettings()
{
    m_tabWidget->clear();
    QMap<QString, QWidget *> groups;
    auto settings = RESOLVE(SettingsManager);

    for (const auto &option : settings->settings())
    {
        QString group = option.group;
        QWidget *groupWidget;
        QFormLayout *formLayout;

        if (!groups.contains(group))
        {
            groupWidget = new QWidget(this);
            formLayout = new QFormLayout(groupWidget);
            groupWidget->setLayout(formLayout);

            m_tabWidget->addTab(groupWidget, group.isEmpty() ? "Без группы" : group);
            groups[group] = groupWidget;
        }
        else
        {
            groupWidget = groups[group];
            formLayout = qobject_cast<QFormLayout *>(groupWidget->layout());
        }

        QWidget *valueEditor = createEditor(option);
        QString name = option.name;
        if (!option.desc.isEmpty() && option.desc != option.name)
            name += " (" + option.desc + ")";
        formLayout->addRow(name, valueEditor);
        m_optionEditors[option.name] = valueEditor;
    }
}

void SettingsEditorDialog::applyChanges()
{
    auto settings = RESOLVE(SettingsManager);
    for (auto it = m_optionEditors.cbegin(); it != m_optionEditors.cend(); ++it)
    {
        const QString &name = it.key();
        QWidget *editor = it.value();
        QString value = extractValue(editor);
        if (value == settings->get(name))
            continue;
        if (!settings->setValue(name, value))
        {
            qDebug()<< "Ошибка", "Не удалось применить значение для параметра: " + name;
        }
    }
    TranslationHelper::instance()->init("Translations");
    TranslationHelper::instance()->setCurrent(RESOLVE(SettingsManager)->get("language"));
    close();
}

QWidget *SettingsEditorDialog::createEditor(const Option &option)
{
    switch (option.type)
    {
    case Option::Bool:
    {
        QCheckBox *checkBox = new QCheckBox(this);
        checkBox->setChecked(option.value == "True");
        return checkBox;
    }
    case Option::Int:
    {
        QSpinBox *spinBox = new QSpinBox(this);
        spinBox->setRange(option.min.toInt(), option.max.toInt());
        spinBox->setValue(option.value.toInt());
        return spinBox;
    }
    case Option::Float:
    {
        QDoubleSpinBox *doubleSpinBox = new QDoubleSpinBox(this);
        doubleSpinBox->setRange(option.min.toDouble(), option.max.toDouble());
        doubleSpinBox->setValue(option.value.toDouble());
        return doubleSpinBox;
    }
    case Option::String:
    {
        QLineEdit *lineEdit = new QLineEdit(this);
        lineEdit->setText(option.value);
        return lineEdit;
    }
    case Option::Dir:
    {
        QWidget *widget = new QWidget(this);
        QHBoxLayout *layout = new QHBoxLayout(widget);
        layout->setContentsMargins(0, 0, 0, 0);

        QLineEdit *lineEdit = new QLineEdit(widget);
        lineEdit->setText(option.value);
        QPushButton *browseButton = new QPushButton("...", widget);

        layout->addWidget(lineEdit);
        layout->addWidget(browseButton);

        connect(browseButton, &QPushButton::clicked, this, [lineEdit]() {
            QString dir = QFileDialog::getExistingDirectory(nullptr, "Выберите каталог", lineEdit->text());
            if (!dir.isEmpty())
            {
                lineEdit->setText(dir);
            }
        });

        return widget;
    }
    case Option::Enum:
    {
        QComboBox *comboBox = new QComboBox(this);
        comboBox->addItems(option.variants);
        comboBox->setCurrentText(option.value);
        return comboBox;
    }
    default:
        return new QWidget(this);
    }
}

QString SettingsEditorDialog::extractValue(QWidget *editor)
{
    if (qobject_cast<QCheckBox *>(editor))
    {
        return qobject_cast<QCheckBox *>(editor)->isChecked() ? "True" : "False";
    }
    else if (qobject_cast<QSpinBox *>(editor))
    {
        return QString::number(qobject_cast<QSpinBox *>(editor)->value());
    }
    else if (qobject_cast<QDoubleSpinBox *>(editor))
    {
        return QString::number(qobject_cast<QDoubleSpinBox *>(editor)->value());
    }
    else if (qobject_cast<QLineEdit *>(editor))
    {
        return qobject_cast<QLineEdit *>(editor)->text();
    }
    else if (qobject_cast<QComboBox *>(editor))
    {
        return qobject_cast<QComboBox *>(editor)->currentText();
    }
    else if (editor->layout())
    {
        QLineEdit *lineEdit = editor->findChild<QLineEdit *>();
        if (lineEdit)
        {
            return lineEdit->text();
        }
    }
    return "";
}

#include "ValidationResultYesNoDialog.h"
#include <QVBoxLayout>
#include <QTextEdit>
#include <QPushButton>
#include "Engine/Components/TranslationHelper.h"

ValidationResultYesNoDialog::ValidationResultYesNoDialog(QWidget *parent, const ValidationResult &res, const QString &title)
    : QDialog{parent}
{
    setWindowFlags(windowFlags() & ~Qt::WindowContextHelpButtonHint);
    setAttribute(Qt::WA_DeleteOnClose);
    setModal(true);
    setWindowTitle(title);

    QVBoxLayout * layout = new  QVBoxLayout();
    layout->setContentsMargins(5,5,5,5);

    QTextEdit * textEdit = new QTextEdit();

    for (int i = 0; i < res.errors.count(); ++i)
    {
        textEdit->append(res.errors[i]);
    }
    layout->addWidget(textEdit);
    textEdit->setSizePolicy(QSizePolicy::Expanding, QSizePolicy::Expanding);
    QHBoxLayout * buttonsLayout = new QHBoxLayout();
    buttonsLayout->setContentsMargins(0,0,0,0);
    buttonsLayout->addStretch(50);
    QPushButton * applyButton = new QPushButton(TranslationHelper::tr("Continue"));
    QPushButton * cancelButton = new QPushButton(TranslationHelper::tr("Cancel"));

    buttonsLayout->addWidget(applyButton);
    buttonsLayout->addWidget(cancelButton);
    applyButton->setSizePolicy(QSizePolicy::Fixed, QSizePolicy::Fixed);
    cancelButton->setSizePolicy(QSizePolicy::Fixed, QSizePolicy::Fixed);

    layout->addLayout(buttonsLayout);
    setLayout(layout);

    connect(applyButton, SIGNAL(clicked(bool)), SLOT(accept()));
    connect(cancelButton, SIGNAL(clicked(bool)), SLOT(reject()));
}

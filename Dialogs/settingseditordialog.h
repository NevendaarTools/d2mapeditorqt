#include <QDialog>
#include <QTabWidget>
#include <QWidget>
#include <QFormLayout>
#include <QLineEdit>
#include <QSpinBox>
#include <QCheckBox>
#include <QPushButton>
#include <QHBoxLayout>
#include <QVBoxLayout>
#include <QMessageBox>
#include <QFileDialog>
#include "Engine/Option.h"
#include <QComboBox>

class SettingsEditorDialog : public QDialog
{
    Q_OBJECT

public:
    SettingsEditorDialog(QWidget *parent = nullptr);

private slots:
    void loadSettings();
    void applyChanges();
private:
    QWidget *createEditor(const Option &option);
    QString extractValue(QWidget *editor);

private:
    QTabWidget *m_tabWidget;
    QMap<QString, QWidget *> m_optionEditors;
};

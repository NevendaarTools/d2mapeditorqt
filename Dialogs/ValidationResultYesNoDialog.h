#ifndef VALIDATIONRESULTYESNODIALOG_H
#define VALIDATIONRESULTYESNODIALOG_H

#include <QDialog>
#include "Engine/MapConverter.h"

class ValidationResultYesNoDialog : public QDialog
{
    Q_OBJECT
public:
    explicit ValidationResultYesNoDialog(QWidget *parent = nullptr,
                                         const ValidationResult & res = ValidationResult(),
                                         const QString& title = QString());

signals:
public slots:

};

#endif // VALIDATIONRESULTYESNODIALOG_H

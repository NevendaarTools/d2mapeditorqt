#ifndef SELECTMAPINSAGADIALOG_H
#define SELECTMAPINSAGADIALOG_H

#include <QDialog>
#include <QTableView>
#include <QLabel>
#include <QPushButton>
#include <QStringListModel>
#include "../toolsqt/MapUtils/D2SagaModel.h"

class SelectMapInSagaDialog : public QDialog
{
    Q_OBJECT
public:
    explicit SelectMapInSagaDialog(QWidget *parent = nullptr);

    void init(const D2SagaModel & model);

    const int &selectedMap() const;
    void setSelectedMap(const int &newSelectedMap);

signals:

public slots:
    void onOk()
    {
        m_selectedMap = m_table->currentIndex().row();
        accept();
    }

    void onCancel()
    {
        reject();
    }
private:
    QLabel * m_nameLabel;
    QTableView * m_table;
    QPushButton * m_okButton;
    QPushButton * m_cancelButton;
    QStringListModel * m_model;

    int m_selectedMap;
};

#endif // SELECTMAPINSAGADIALOG_H

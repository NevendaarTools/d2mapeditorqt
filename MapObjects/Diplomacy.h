#ifndef DIPLOMACY_H
#define DIPLOMACY_H
#include "MapObject.h"
#include <QDebug>
#include "toolsqt/DBFModel/GameClases.h"
#include "MapLink.h"
#include "toolsqt/MapUtils/DataBlocks/D2ScenarioInfo.h"

class PlayerObject : public MapObject
{
public:
    PlayerObject()
    {
        this->uid.first = MapObject::Player;
        populate = false;
    }
    Currency bank;
    QString name;
    QString desc;
    Glord::Category lordCat;
    QString raceId;
    //fog
    QList<QString> spellbook;
    QList<QString> buildings;
    bool isHuman;
    bool alwaysAI;
    bool researchThisTurn;
    bool buildThisTurn;
    int face = 0;
    int attitude = 0;
    QString getName() const;
};
template <> int getTypeId<PlayerObject>() {return MapObject::Player;}
Q_DECLARE_METATYPE(PlayerObject)


class SubRaceObject : public MapObject
{
public:
    SubRaceObject()
    {
        this->uid.first = MapObject::SubRace;
        populate = false;
    }
    MapLink<PlayerObject> playerUid;
    QString name;
    int banner;
    int number;
    int subrace;

    QString getName() const;
};
template <> int getTypeId<SubRaceObject>() {return MapObject::SubRace;}

Q_DECLARE_METATYPE(SubRaceObject)


class Relations : public MapObject
{
public:
    Relations()
    {
        uid.first = MapObject::Relation;
        populate = false;
    }
    struct RelationEntry
    {
        MapLink<PlayerObject> race1;
        MapLink<PlayerObject> race2;
        int relation;
        bool hasSpy1;
        bool hasSpy2;
    };
    QList<RelationEntry> entries;
};
template <> int getTypeId<Relations>() {return MapObject::Relation;}

class MapInfo : public MapObject
{
public:
    MapInfo()
    {
        uid.first = MapObject::Info;
        populate = false;
    }
    MapInfo(const D2ScenarioInfo& info) : MapInfo()
    {
        this->info = info;
    }
    D2ScenarioInfo info;
};
template <> int getTypeId<MapInfo>() {return MapObject::Info;}

Q_DECLARE_METATYPE(MapInfo)

#endif // DIPLOMACY_H

#ifndef OBJECTACCESSORS_H
#define OBJECTACCESSORS_H
#include "Engine/Components/AccessorHolder.h"

class UnitObjectAccessor : public MapObjectAccessor
{
    qreal getZ(QSharedPointer<MapObject> object) const override;
    QString getName(QSharedPointer<MapObject> object) const override;
    QString getDesc(QSharedPointer<MapObject> object) const override;
    QSharedPointer<MapObject> cloneObject(QSharedPointer<MapObject> object) const override;
};

class TreasureObjectAccessor : public MapObjectAccessor
{
    QString getName(QSharedPointer<MapObject> object) const override;
    QSharedPointer<MapObject> cloneObject(QSharedPointer<MapObject> object) const override;

    QMenu * requestMenu(QSharedPointer<MapObject> object) const override;
    QList<FrameData> frameData(QSharedPointer<MapObject> object) const override;
    virtual IObjectEditor * createEditor() const override;
};

class TombObjectAccessor : public MapObjectAccessor
{
    QMenu * requestMenu(QSharedPointer<MapObject> object) const override;
    QList<FrameData> frameData(QSharedPointer<MapObject> object) const override;
    QString getName(QSharedPointer<MapObject> object) const;
    QString getDesc(QSharedPointer<MapObject> object) const;
    QSharedPointer<MapObject> cloneObject(QSharedPointer<MapObject> object) const override;
};

class StackTemplateObjectAccessor : public MapObjectAccessor
{
    qreal getZ(QSharedPointer<MapObject> object) const override;
    QString getName(QSharedPointer<MapObject> object) const override;
    QString getDesc(QSharedPointer<MapObject> object) const override;
    QSharedPointer<MapObject> cloneObject(QSharedPointer<MapObject> object) const override;

    QMenu * requestMenu(QSharedPointer<MapObject> object) const override;
    QList<FrameData> frameData(QSharedPointer<MapObject> object) const override;
    virtual IObjectEditor * createEditor() const override;
};

class StackObjectAccessor : public MapObjectAccessor
{
    qreal getZ(QSharedPointer<MapObject> object) const override;
    QString getName(QSharedPointer<MapObject> object) const override;
    QString getDesc(QSharedPointer<MapObject> object) const override;
    QSharedPointer<MapObject> cloneObject(QSharedPointer<MapObject> object) const override;


    QMenu * requestMenu(QSharedPointer<MapObject> object) const override;
    QList<FrameData> frameData(QSharedPointer<MapObject> object) const override;
    virtual IObjectEditor * createEditor() const override;
};

class ScenVariableObjectAccessor : public MapObjectAccessor
{
public:
    QString getName(QSharedPointer<MapObject> object) const override;
    QString getDesc(QSharedPointer<MapObject> object) const override;
    QSharedPointer<MapObject> cloneObject(QSharedPointer<MapObject> object) const override;
    QMenu* requestMenu(QSharedPointer<MapObject> object) const override;
    QList<FrameData> frameData(QSharedPointer<MapObject> object) const override;
    IObjectEditor* createEditor() const override;
};

class RuinObjectAccessor : public MapObjectAccessor
{
    int getW(QSharedPointer<MapObject> object) const override;
    int getH(QSharedPointer<MapObject> object) const override;

    QString getName(QSharedPointer<MapObject> object) const override;
    QString getDesc(QSharedPointer<MapObject> object) const override;
    QSharedPointer<MapObject> cloneObject(QSharedPointer<MapObject> object) const override;

    QMenu * requestMenu(QSharedPointer<MapObject> object) const override;
    QList<FrameData> frameData(QSharedPointer<MapObject> object) const override;
    virtual IObjectEditor * createEditor() const override;
};

class RodObjectAccessor : public MapObjectAccessor
{
    QMenu * requestMenu(QSharedPointer<MapObject> object) const override;
    QList<FrameData> frameData(QSharedPointer<MapObject> object) const override;
    QString getName(QSharedPointer<MapObject> object) const{return "";}
    QString getDesc(QSharedPointer<MapObject> object) const{return "";}
    QSharedPointer<MapObject> cloneObject(QSharedPointer<MapObject> object) const override;
};
//race      flag                rod                 smallshield         bigshield
//empire    G000RR0000FLAG8     G000RR0000RROD8     G000RR0000SHLC8     G000RR0000SHLV8
//gnoms     G000RR0001FLAG8     G000RR0001RROD8     G000RR0001SHLC8     G000RR0001SHLV8
//demons    G000RR0002FLAG8     G000RR0002RROD8     G000RR0002SHLC8     G000RR0002SHLV8
//undeads   G000RR0003FLAG8     G000RR0003RROD8     G000RR0003SHLC8     G000RR0003SHLV8
//elves     G000RR0005FLAG8     G000RR0005RROD8     G000RR0005SHLC8     G000RR0005SHLV8
//neutrals//G000RR0004FLAG8     G000RR0004RROD8     G000RR0004SHLC8     G000RR8888SHLV8

class QuestLineObjectAccessor : public MapObjectAccessor
{
    QString getName(QSharedPointer<MapObject> object) const override;
    QString getDesc(QSharedPointer<MapObject> object) const override;
    QSharedPointer<MapObject> cloneObject(QSharedPointer<MapObject> object) const override;

    QMenu * requestMenu(QSharedPointer<MapObject> object) const override{return nullptr;}
    QList<FrameData> frameData(QSharedPointer<MapObject> object) const override{return QList<FrameData>();}
    virtual IObjectEditor * createEditor() const override;
};

class MountainObjectAccessor : public MapObjectAccessor
{
    int getW(QSharedPointer<MapObject> object) const override;
    int getH(QSharedPointer<MapObject> object) const override;

    QString getName(QSharedPointer<MapObject> object) const override;
    QString getDesc(QSharedPointer<MapObject> object) const override;
    QSharedPointer<MapObject> cloneObject(QSharedPointer<MapObject> object) const override;

    QMenu * requestMenu(QSharedPointer<MapObject> object) const override;
    QList<FrameData> frameData(QSharedPointer<MapObject> object) const override;
};

class LocationObjectAccessor : public MapObjectAccessor
{
    int getW(QSharedPointer<MapObject> object) const override;
    int getH(QSharedPointer<MapObject> object) const override;

    QString getName(QSharedPointer<MapObject> object) const override;
    QString getDesc(QSharedPointer<MapObject> object) const override;
    QSharedPointer<MapObject> cloneObject(QSharedPointer<MapObject> object) const override;

    QMenu *requestMenu(QSharedPointer<MapObject> object) const override;
    QList<FrameData> frameData(QSharedPointer<MapObject> object) const override;
    qreal getZ(QSharedPointer<MapObject> object) const;
};

class LandmarkObjectAccessor : public MapObjectAccessor
{
    int getW(QSharedPointer<MapObject> object) const override;
    int getH(QSharedPointer<MapObject> object) const override;

    QString getName(QSharedPointer<MapObject> object) const override;
    QString getDesc(QSharedPointer<MapObject> object) const override;
    QSharedPointer<MapObject> cloneObject(QSharedPointer<MapObject> object) const override;

    QMenu *requestMenu(QSharedPointer<MapObject> object) const override;
    QList<FrameData> frameData(QSharedPointer<MapObject> object) const override;
};


class FortObjectAccessor : public MapObjectAccessor
{
    int getW(QSharedPointer<MapObject> object) const override;
    int getH(QSharedPointer<MapObject> object) const override;

    QString getName(QSharedPointer<MapObject> object) const override;
    QString getDesc(QSharedPointer<MapObject> object) const override;
    QSharedPointer<MapObject> cloneObject(QSharedPointer<MapObject> object) const override;

    QMenu *requestMenu(QSharedPointer<MapObject> object) const override;
    QList<FrameData> frameData(QSharedPointer<MapObject> object) const override;
    virtual IObjectEditor * createEditor() const override;
};

class CrystalObjectAccessor : public MapObjectAccessor
{
    QMenu * requestMenu(QSharedPointer<MapObject> object) const override;
    QList<FrameData> frameData(QSharedPointer<MapObject> object) const override;
    QString getName(QSharedPointer<MapObject> object) const;
    QString getDesc(QSharedPointer<MapObject> object) const;
    QSharedPointer<MapObject> cloneObject(QSharedPointer<MapObject> object) const override;
};

class PlayerObjectAccessor : public MapObjectAccessor
{
public:
    QString getName(QSharedPointer<MapObject> object) const override;
    QString getDesc(QSharedPointer<MapObject> object) const override;
    QSharedPointer<MapObject> cloneObject(QSharedPointer<MapObject> object) const override;
    QMenu* requestMenu(QSharedPointer<MapObject> object) const override;
    QList<FrameData> frameData(QSharedPointer<MapObject> object) const override;
    IObjectEditor* createEditor() const override;
};

class SubRaceObjectAccessor : public MapObjectAccessor
{
public:
    QString getName(QSharedPointer<MapObject> object) const override;
    QString getDesc(QSharedPointer<MapObject> object) const override;
    QSharedPointer<MapObject> cloneObject(QSharedPointer<MapObject> object) const override;
    QMenu* requestMenu(QSharedPointer<MapObject> object) const override;
    QList<FrameData> frameData(QSharedPointer<MapObject> object) const override;
    IObjectEditor* createEditor() const override;
};

class RelationsAccessor : public MapObjectAccessor
{
public:
    QString getName(QSharedPointer<MapObject> object) const override;
    QString getDesc(QSharedPointer<MapObject> object) const override;
    QSharedPointer<MapObject> cloneObject(QSharedPointer<MapObject> object) const override;
    QMenu* requestMenu(QSharedPointer<MapObject> object) const override;
    QList<FrameData> frameData(QSharedPointer<MapObject> object) const override;
    IObjectEditor* createEditor() const override;
};

class MapInfoAccessor : public MapObjectAccessor
{
public:
    QString getName(QSharedPointer<MapObject> object) const override;
    QString getDesc(QSharedPointer<MapObject> object) const override;
    QSharedPointer<MapObject> cloneObject(QSharedPointer<MapObject> object) const override;
    QMenu* requestMenu(QSharedPointer<MapObject> object) const override;
    QList<FrameData> frameData(QSharedPointer<MapObject> object) const override;
    IObjectEditor* createEditor() const override;
};

class MerchantObjectAccessor : public MapObjectAccessor
{
    int getW(QSharedPointer<MapObject> object) const override;
    int getH(QSharedPointer<MapObject> object) const override;

    QString getName(QSharedPointer<MapObject> object) const override;
    QString getDesc(QSharedPointer<MapObject> object) const override;
    QSharedPointer<MapObject> cloneObject(QSharedPointer<MapObject> object) const override;

    QList<FrameData> frameData(QSharedPointer<MapObject> object) const override;
    IObjectEditor *createEditor() const;
};

class EventObjectAccessor : public MapObjectAccessor
{
public:
    QString getName(QSharedPointer<MapObject> object) const override;
    QString getDesc(QSharedPointer<MapObject> object) const override;
    QSharedPointer<MapObject> cloneObject(QSharedPointer<MapObject> object) const override;
    QMenu* requestMenu(QSharedPointer<MapObject> object) const override;
    QList<FrameData> frameData(QSharedPointer<MapObject> object) const override;
    IObjectEditor* createEditor() const override;
};

class MapStatisticsAccessor : public MapObjectAccessor
{
public:
    IObjectEditor* createEditor() const override;
};

#endif // OBJECTACCESSORS_H

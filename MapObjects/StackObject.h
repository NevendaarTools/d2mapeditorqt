#ifndef STACKOBJECT_H
#define STACKOBJECT_H

#include "MapObject.h"
#include "toolsqt/DBFModel/GameClases.h"
#include "BaseClases.h"
#include "Diplomacy.h"

enum class ModifierElementType : int
{
    QtyTarget = 0,
    ScoutingRange = 1,
    Leadership = 2,
    Power = 3,
    QtyDamage = 4,
    Armor = 5,
    Hp = 6,
    MoveAllowance = 7,
    Initiative = 9,
    MoveAbility = 10,
    LeaderAbility = 11,
    Immunity = 12,
    Regeneration = 13,
    Immunityclass = 14,
    AttackDrain = 15,
    FastRetreat = 16,
    LowerCost = 17,
};

class StackObject: public MapObject
{
public :
    StackObject(int x = 0, int y = 0)
    {
        this->uid.first = MapObject::Stack;
        this->x = x;
        this->y = y;
    }
    GroupData stack;
    int rotation = 0;
    int order = 1;
    int aiorder = 1;
    int priority = 0;
    int move = 0;
    int creatLvl = 1;
    int nbbattle = 0;
    bool invisible = false;
    bool aiIgnore = false;
    QPair<int, int> orderTarget;
    QPair<int, int> aiOrderTarget;
    MapLink<PlayerObject> owner;
    MapLink<SubRaceObject> subrace;
    QPair<int, int> inside{-1,-1};
    bool ignoreAI;

    enum Order
    {
        Normal = 1,
        Stand = 2,
        Guard = 3,
        AttackStack = 4,
        DefendStack = 5,
        SecureCity = 6,
        Roam = 7,
        MoveToLocation = 8,
        DefendLocation = 9,
        Bezerk = 10,
        Assist = 11,
        ExploreUnused,
        Steal = 13,
        DefendCity = 14
    };

    static QString orderName(int order)
    {
        return orderName(StackObject::Order(order));
    }
    static QString orderName(StackObject::Order order)
    {
        switch (order) {
        case StackObject::Normal:
            return "ORDER_NORMAL";
            break;
        case StackObject::Stand:
            return "ORDER_STAND";
            break;
        case StackObject::Guard:
            return "ORDER_GUARD";
            break;
        case StackObject::AttackStack:
            return "ORDER_ATTACKSTACK";
            break;
        case StackObject::DefendStack:
            return "ORDER_DEFENDSTACK";
            break;
        case StackObject::SecureCity:
            return "ORDER_SECURECITY";
            break;
        case StackObject::Roam:
            return "ORDER_ROAM";
            break;
        case StackObject::MoveToLocation:
            return "ORDER_MOVETOLOCATION";
            break;
        case StackObject::DefendLocation:
            return "ORDER_DEFENDLOCATION";
            break;
        case StackObject::Bezerk:
            return "ORDER_BEZERK";
            break;
        case StackObject::Assist:
            return "ORDER_ASSIST";
            break;
        case StackObject::ExploreUnused:
            return "ORDER_EXPLOREUNUSED";
            break;
        case StackObject::Steal:
            return "ORDER_STEAL";
            break;
        case StackObject::DefendCity:
            return "ORDER_DEFENDCITY";
            break;
        default:
            break;
        }

        return QString();
    }
};
template <> int getTypeId<StackObject>() {return MapObject::Stack;}

#endif // STACKOBJECT_H

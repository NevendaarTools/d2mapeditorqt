#ifndef QUESTLINE_H
#define QUESTLINE_H
#include "MapObject.h"
#include "BaseClases.h"
#include "EventObject.h"

class QuestLine: public MapObject
{
public:
    struct QuestStage
    {
        QString name;
        QString stageText;
        QList<QPair<int, int>> events;
    };
    QuestLine()
    {
        this->uid.first = MapObject::QuestLine;
        this->x = -1;
        this->y = -1;
        populate = false;
        stages<<QuestStage();
    }
    QString name;
    QList<QuestStage> stages;
    bool utility = false;
    bool display = false;
};
template <> int getTypeId<QuestLine>() {return MapObject::QuestLine;}


#endif // QUESTLINE_H

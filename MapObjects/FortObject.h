#ifndef FORTOBJECT_H
#define FORTOBJECT_H
#include "MapObject.h"
#include "toolsqt/DBFModel/GameClases.h"
#include "StackObject.h"
#include "MapLink.h"

class FortObject : public MapObject
{
public:
    enum FortType{
        Capital,
        Village
    };

    FortObject()
    {
        uid.first = MapObject::Fort;
        this->visiter.first = MapObject::Stack;
        this->visiter.second = -1;
    }
    FortType fortType;
    QString name;
    QString desc;
    QString raceId;
    MapLink<StackObject> visiter;
    GroupData garrison;

    int priority = 0;
    MapLink<PlayerObject> owner;
    MapLink<SubRaceObject> subrace;
    int level;
};
template <> int getTypeId<FortObject>() {return MapObject::Fort;}
Q_DECLARE_METATYPE(FortObject)

static int shieldByRaceID(int raceId)
{
    switch (raceId) {
    case 1:
        return 3;
        break;
    case 3:
        return 1;
    case 4:
        return 8888;
    default:
        break;
    }
    return raceId;
}

#endif // FORTOBJECT_H

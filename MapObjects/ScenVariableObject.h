#ifndef SCENVARIABLEOBJECT_H
#define SCENVARIABLEOBJECT_H
#include "MapObject.h"
#include "BaseClases.h"

class ScenVariableObject: public MapObject
{
public:
    ScenVariableObject()
    {
        uid.first = MapObject::ScenVariable;
        populate = false;
    }
    QString name;
    QString desc;
    int value;
};
template <> int getTypeId<ScenVariableObject>() {return MapObject::ScenVariable;}

#endif // SCENVARIABLEOBJECT_H

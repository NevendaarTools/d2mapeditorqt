#ifndef EVENTOBJECT_H
#define EVENTOBJECT_H
#include "MapObject.h"
#include "toolsqt/DBFModel/GameClases.h"
#include "toolsqt/MapUtils/DataBlocks/D2Event.h"
#include "BaseClases.h"
#include "QuestLine.h"
#include "ScenVariableObject.h"

class EventObject : public MapObject
{
public:
    enum Race
    {
        HUMAN = 0,
        DWARF,
        UNDEAD,
        HERETIC,
        NEUTRAL,
        ELF
    };
    bool activatedBy[6];
    bool effectsTo[6];
    bool enabled = true;
    bool occurOnce = true;

    int chance = 100;
    int order = 0;
    QString name;
    QPair<int, int> lineId = EMPTY_ID;

    EventObject()
    {
        uid.first = MapObject::Event;
        populate = false;
    }
    struct EventCondition
    {
        D2Event::EventCondition condition;
        QVector<QPair<int, int>> targets;
    };
    struct EventEffect
    {
        D2Event::EventEffect effect;
        QVector<QPair<int, int>> targets;
    };


    QList<EventCondition> conditions;
    QList<EventEffect> effects;

    QVector<QPair<int, int>> conditionTargets() const
    {
        QVector<QPair<int, int>> result;
        foreach (const EventCondition condition, conditions)
        {
            result<<condition.targets;
        }
        return result;
    }
    QVector<QPair<int, int>> effectTargets() const
    {
        QVector<QPair<int, int>> result;
        foreach (const EventEffect effect, effects)
        {
            result<<effect.targets;
        }
        return result;
    }
    static QString effectToString(EventObject::EventEffect & effect);
    static QString effectToString(D2Event::EventEffect & effect_);
};

template <> int getTypeId<EventObject>() {return MapObject::Event;}
Q_DECLARE_METATYPE(EventObject)

#endif // EVENTOBJECT_H

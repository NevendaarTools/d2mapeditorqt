#ifndef STACKTEMPLATEOBJECT_H
#define STACKTEMPLATEOBJECT_H

#include "MapObject.h"
#include "toolsqt/DBFModel/GameClases.h"
#include "BaseClases.h"
#include "StackObject.h"

class StackTemplateObject: public StackObject
{
public :
    StackTemplateObject()
    {
        this->uid.first = MapObject::StackTemplate;
        this->x = -1;
        this->y = -1;
        populate = false;
    }
    bool useFacing = false;
};
template <> int getTypeId<StackTemplateObject>() {return MapObject::StackTemplate;}
Q_DECLARE_METATYPE(StackTemplateObject)

#endif // STACKTEMPLATEOBJECT_H

#ifndef UNITOBJECT_H
#define UNITOBJECT_H
#include "MapObject.h"
#include "toolsqt/DBFModel/GameClases.h"
#include "Common/TemplateFunctions.h"

class UnitObject: public MapObject
{
public:
    UnitObject(int x = 0, int y = 0) : MapObject()
    {
        this->uid.first = MapObject::Unit;
        this->uid.second = -1;
        this->x = x;
        this->y = y;
        populate = false;
    }
    QString id;
    int currentHp;
    int currentExp = 0;
    int level = 0;
    QString name;
    QList<QString> modifiers;
    bool leader = false;
    friend bool operator==(const UnitObject& left, const UnitObject& right)
    {
        return (left.id == right.id) &&
               (left.level == right.level) &&
               (left.modifiers == right.modifiers);
    }
};
template <> int getTypeId<UnitObject>() {return MapObject::Unit;}

#endif // UNITOBJECT_H

#ifndef CRYSTALOBJECT_H
#define CRYSTALOBJECT_H
#include "MapObject.h"
#include "toolsqt/DBFModel/GameClases.h"
#include "toolsqt/MapUtils/DataBlocks/D2Crystal.h"
#include "Engine/Components/TranslationHelper.h"
#include "toolsqt/DBFModel/DBFModel.h"
#include "Engine/GameInstance.h"

class CrystalObject : public MapObject
{
public:
    enum ResourceType
    {
        GOLG = 0,
        DEMONS = 1,
        EMPIRE = 2,
        UNDEAD = 3,
        CLANS = 4,
        ELVES = 5
    };

    static QString CrystalIdByResource(ResourceType resource)
    {
        switch (resource)
        {
            case ResourceType::GOLG:
                return "G000CR0000GL";
            case ResourceType::DEMONS:
                return "G000CR0000RD";
            case ResourceType::EMPIRE:
                return "G000CR0000YE";
            case ResourceType::UNDEAD:
                return "G000CR0000RG";
            case ResourceType::CLANS:
                return "G000CR0000WH";
            case ResourceType::ELVES:
                return "G000CR0000GR";
        }
        return "GOLD";
    }
    static QString CrystalNameByResource(ResourceType resource)
    {
        switch (resource)
        {
            case ResourceType::GOLG:
                return getTAppText("X150TA0007");
            case ResourceType::DEMONS:
                return getTAppText("X100TA0099");
            case ResourceType::EMPIRE:
                return getTAppText("X100TA0098");
            case ResourceType::UNDEAD:
                return getTAppText("X100TA0096");
            case ResourceType::CLANS:
                return getTAppText("X100TA0097");
            case ResourceType::ELVES:
                return getTAppText("X160TA0038");
        }
        return "";
    }

    static QString CrystalDescByResource(ResourceType resource)
    {
        QString result = getTAppText("X005TA0051") + " ";
        switch (resource)
        {
            case ResourceType::GOLG:
                result += getTAppText("X005TA0055");
            break;
            case ResourceType::DEMONS:
                result += getTAppText("X005TA0059");
            break;
            case ResourceType::EMPIRE:
                result += getTAppText("X005TA0058");
            break;
            case ResourceType::UNDEAD:
                result += getTAppText("X005TA0057");
            break;
            case ResourceType::CLANS:
                result += getTAppText("X005TA0056");
            break;
            case ResourceType::ELVES:
                result += getTAppText("X160TA0001");
            break;
        }
        auto value = RESOLVE(DBFModel)->getList<GVars>()[0];
        result.replace("%QTY%", QString::number(value->crystal_p));
        return result;
    }

    static QString CrystalByResource(ResourceType resource)
    {
        switch (resource)
        {
            case ResourceType::GOLG:
                return "GOLD";
            case ResourceType::DEMONS:
                return "RED";
            case ResourceType::EMPIRE:
                return "YELLOW";
            case ResourceType::UNDEAD:
                return "ORANGE";
            case ResourceType::CLANS:
                return "WHITE";
            case ResourceType::ELVES:
                return "BLUE";
        }
        return "GOLD";
    }
    static QString CrystalIconByResource(ResourceType resource)
    {
        switch (resource)
        {
            case ResourceType::GOLG:
                return "_RESOURCES_GOLD";
            case ResourceType::DEMONS:
                return "_RESOURCES_REDM_B";
            case ResourceType::EMPIRE:
                return "_RESOURCES_BLUEM_B";
            case ResourceType::UNDEAD:
                return "_RESOURCES_BLACKM_B";
            case ResourceType::CLANS:
                return "_RESOURCES_WHITEM_B";
            case ResourceType::ELVES:
                return "_RESOURCES_GREENM_B";
        }
        return "GOLD";
    }

    CrystalObject()
    {
        uid.first = MapObject::Crystal;
    }
    CrystalObject(const D2Crystal & crystal)
    {
        uid.first = MapObject::Crystal;
        uid.second = crystal.uid.second;
        resource = (ResourceType)crystal.resource;
        x = crystal.posX;
        y = crystal.posY;
        AIpriority = crystal.aipriority;
    }
    D2Crystal toD2() const
    {
        D2Crystal res;
        res.posX = x;
        res.posY = y;
        res.aipriority = AIpriority;
        res.resource = (int)resource;
        res.uid.second = uid.second;
        return res;
    }
    ResourceType resource;
    int AIpriority = 3;
};
template <> int getTypeId<CrystalObject>() {return MapObject::Crystal;}
Q_DECLARE_METATYPE(CrystalObject)

#endif // CRYSTALOBJECT_H

#include "GameMap.h"


GameMap::GameMap()
{

}

QPair<int, int> GameMap::addObject(QSharedPointer<MapObject> obj)
{
    m_mapObjects[obj->uid.first].addObject(obj);
    return obj->uid;
}

void GameMap::addObject(QSharedPointer<MapObject> obj, QPair<int, int> uid)
{
    m_mapObjects[obj->uid.first].addObject(obj, uid.second);
}

bool GameMap::hasObject(QPair<int, int> uid)
{
    if (!m_mapObjects[uid.first].data.contains(uid.second))
        return false;
    return true;
}

QSharedPointer<MapObject> GameMap::objectById(QPair<int, int> uid) const
{
    if (!m_mapObjects[uid.first].data.contains(uid.second))
    {
        qDebug()<<"Failed to get object by id = "<<uid;
        return QSharedPointer<MapObject>();
    }
    return m_mapObjects[uid.first].data[uid.second];
}

void GameMap::replaceObject(QSharedPointer<MapObject> obj)
{
    m_mapObjects[obj->uid.first].replaceObject(obj);
}

void GameMap::removeObject(QPair<int, int> uid)
{
    if (!m_mapObjects[uid.first].data.contains(uid.second))
        return;
    const auto & obj = m_mapObjects[uid.first].data[uid.second];
    if (obj->populate)
    {
        removeFromGrid(uid);
    }
    m_mapObjects[uid.first].removeObject(uid.second);
}

MapGrid GameMap::getGrid() const
{
    return grid;
}

void GameMap::setGrid(const MapGrid &value)
{
    grid = value;
    info.size = grid.cells.count();
}

QList<QSharedPointer<MapObject> > GameMap::objects() const
{
    QList<QSharedPointer<MapObject> > result;
    foreach(int key, m_mapObjects.keys())
    {
        QHash<int, QSharedPointer<MapObject> > typeValues = m_mapObjects[key].data;
        foreach(QSharedPointer<MapObject> item, typeValues.values())
        {
            result << item;
        }
    }
    return result;
}

QList<int> GameMap::collectionTypes() const
{
    return m_mapObjects.keys();
}

const QString &GameMap::getMapName() const
{
    return info.name;
}

void GameMap::setMapName(const QString &newMapName)
{
    info.name = newMapName;
}

const QString &GameMap::getMapDesc() const
{
    return info.desc;
}

void GameMap::setMapDesc(const QString &newMapDesc)
{
    info.desc = newMapDesc;
}

void GameMap::removeFromGrid(const QPair<int, int> &uid)
{
    for(int i = 0; i < grid.cells.count(); ++i)
    {
        for(int k = 0; k < grid.cells[i].count(); ++k)
        {
            grid.objBinging.binding[i][k].items.removeAll(uid);
        }
    }
}

const QList<Option> &GameMap::settings() const
{
    return m_settings;
}

void GameMap::setSettings(const QList<Option> &newSettings)
{
    m_settings = newSettings;
}

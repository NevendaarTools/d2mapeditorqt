#include "EventObject.h"
#include "Engine/Components/TranslationHelper.h"
#include "Engine/Map/MapStateHolder.h"
#include "Engine/Components/AccessorHolder.h"
#include "Engine/GameInstance.h"
#include "toolsqt/DBFModel/DBFModel.h"

QString EventObject::effectToString(EventEffect &effect)
{
    QString res = effectToString(effect.effect);
    if (effect.targets.count() > 0)
    {
        foreach (auto target , effect.targets)
        {
            QSharedPointer<MapObject> mapObj =
                RESOLVE(MapStateHolder)->objectById(target);
            QSharedPointer<IMapObjectAccessor> accessor =
                RESOLVE(AccessorHolder)->objectAccessor(target.first);

            res+="[ ";
            if (mapObj.isNull())
                res += "null object";
            if (accessor.isNull())
                res += "null accessor";
            if (!accessor.isNull() && !mapObj.isNull()){

                res += accessor->getName(mapObj) + " ";
                if (mapObj->populate)
                    res += QString("[%1:%2] ").arg(mapObj->x).arg(mapObj->y);
            }
            res += "]";
        }

    }
    return res;
}

QString EventObject::effectToString(D2Event::EventEffect &effect_)
{
    auto effectBase = effect_.effect;
    auto res = TranslationHelper::tr(effect_.toString());
    auto category = effect_.category;
    switch (category) {
    case D2Event::EventEffect::WIN_OR_LOSE_SCENARIO:
    {
        D2Event::EventEffect::WinLooseEffect effect =
            std::get<D2Event::EventEffect::WinLooseEffect>(effectBase);
        res = effect.win? TranslationHelper::tr("Win in scenario") : TranslationHelper::tr("Loose in scenario");
        break;
    }
    case D2Event::EventEffect::CAST_SPELL_ON_TRIGGERER:
    {
        D2Event::EventEffect::CastSpellOnTriggerEffect effect =
            std::get<D2Event::EventEffect::CastSpellOnTriggerEffect>(effectBase);
        auto spell = RESOLVE(DBFModel)->get<GSpell>(effect.spellType);
        if (!spell.isNull())
            res+= spell->name_txt->text;
        else
            res += "unknown spell";
        break;
    }
    case D2Event::EventEffect::CAST_SPELL_AT_SPECIFIC_LOCATION:
    {
        D2Event::EventEffect::CastSpellOnLocationEffect effect =
            std::get<D2Event::EventEffect::CastSpellOnLocationEffect>(effectBase);
        auto spell = RESOLVE(DBFModel)->get<GSpell>(effect.spellType);
        if (!spell.isNull())
            res+= spell->name_txt->text;
        else
            res += "unknown spell";
        break;
    }
    case D2Event::EventEffect::GO_INTO_BATTLE:
        return "GO_INTO_BATTLE";
        break;
    case D2Event::EventEffect::ENABLE_DISABLE_ANOTHER_EVENT:
    {
        D2Event::EventEffect::DisableEventEffect effect =
            std::get<D2Event::EventEffect::DisableEventEffect>(effectBase);
        break;
    }
    case D2Event::EventEffect::GIVE_SPELL:
    {
        D2Event::EventEffect::GiveSpellEffect effect =
            std::get<D2Event::EventEffect::GiveSpellEffect>(effectBase);
        auto spell = RESOLVE(DBFModel)->get<GSpell>(effect.spellType);
        if (!spell.isNull())
            res+= spell->name_txt->text;
        else
            res += "unknown spell";
        break;
    }
    case D2Event::EventEffect::GIVE_ITEM:
    {
        D2Event::EventEffect::GiveItemEffect effect =
            std::get<D2Event::EventEffect::GiveItemEffect>(effectBase);
        auto spell = RESOLVE(DBFModel)->get<GItem>(effect.itemType);
        if (!spell.isNull())
            res+= spell->name_txt->text;
        else
            res += "unknown spell";
        break;
    }
    case D2Event::EventEffect::MOVE_STACK_TO_SPECIFIC_LOCATION:
        return res;
        break;
    case D2Event::EventEffect::ALLY_TWO_AI_PLAYERS:
        return res;
        break;
    case D2Event::EventEffect::CHANGE_PLAYER_DIPLOMACY_METER:
        return res;
        break;
    case D2Event::EventEffect::UNFOG_OR_FOG_AN_AREA_ON_THE_MAP:
        return res;
        break;
    case D2Event::EventEffect::REMOVE_MOUNTAINS_AROUND_A_LOCATION:
        return res;
        break;
    case D2Event::EventEffect::REMOVE_LANDMARK:
        return res;
        break;
    case D2Event::EventEffect::CHANGE_SCENARIO_OBJECTIVE_TEXT:
        return res;
        break;
    case D2Event::EventEffect::DISPLAY_POPUP_MESSAGE:
        return res;
        break;
    case D2Event::EventEffect::CHANGE_STACK_ORDER:
        return res;
        break;
    case D2Event::EventEffect::DESTROY_ITEM:
        return res;
        break;
    case D2Event::EventEffect::REMOVE_STACK:
        return res;
        break;
    case D2Event::EventEffect::CHANGE_LANDMARK:
        return res;
        break;
    case D2Event::EventEffect::CHANGE_TERRAIN:
        return res;
        break;
    case D2Event::EventEffect::MODIFY_VARIABLE:
    {
        D2Event::EventEffect::ModifyVarEffect effect =
            std::get<D2Event::EventEffect::ModifyVarEffect>(effectBase);
        switch (effect.lookup) {
        case 0:
            res += TranslationHelper::tr("Add");
            break;
        case 1:
            res += TranslationHelper::tr("Remove");
            break;
        case 2:
            res += TranslationHelper::tr("Multiply");
            break;
        case 3:
            res += TranslationHelper::tr("Divide");
            break;
        case 4:
            res += TranslationHelper::tr("Set");
            break;
        default:
            break;
        }
        res+=" ";
        res+= QString::number(effect.val1);
        break;
    }

    }
    return res;
}

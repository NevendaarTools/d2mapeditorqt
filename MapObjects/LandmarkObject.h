#ifndef LANDMARKOBJECT_H
#define LANDMARKOBJECT_H

#include "MapObject.h"
#include "toolsqt/DBFModel/GameClases.h"

class LandmarkObject : public MapObject
{
public:
    LandmarkObject(){ uid.first = MapObject::LandMark;}
    LandmarkObject(const QString & lmarkId, int x, int y) : lmarkId(lmarkId)
    {
        uid.first = MapObject::LandMark;
        this->x = x;
        this->y = y;
    }
    QString lmarkId;
    QString desc;
};
template <> int getTypeId<LandmarkObject>() {return MapObject::LandMark;}
Q_DECLARE_METATYPE(LandmarkObject)

#endif // LANDMARKOBJECT_H

#ifndef RUINOBJECT_H
#define RUINOBJECT_H
#include "MapObject.h"
#include "toolsqt/DBFModel/GameClases.h"
#include "StackObject.h"

class RuinObject : public MapObject
{
public:
    RuinObject()
    {
        this->uid.first = MapObject::Ruin;
    }
    int image;
    QString name = "";
    QString desc = "";
    GroupData guards;
    QString item;
    Currency reward;
    int priority;
};

template <> int getTypeId<RuinObject>() {return MapObject::Ruin;}
Q_DECLARE_METATYPE(RuinObject)

#endif // RUINOBJECT_H

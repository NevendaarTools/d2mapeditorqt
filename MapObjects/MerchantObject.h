#ifndef MERCHANTOBJECT_H
#define MERCHANTOBJECT_H
#include "MapObject.h"
#include "BaseClases.h"
#include "toolsqt/DBFModel/GameClases.h"

class MerchantObject : public MapObject
{
public:
    enum Type
    {
        Items,
        Units,
        Spells,
        Trainer
    };

    struct UnitHireList
    {
        QList<QPair<UnitObject, int> > items;//unit - count
        void addItem(const UnitObject& unit, int count = 0)
        {
            for(int i = 0; i < items.count(); ++i)
            {
                if (items[i].first == unit)
                {
                    items[i].second += count;
                    return;
                }
            }
            items.append(QPair<UnitObject, int>(unit, count));
        }
    };

    MerchantObject()
    {
        this->uid.first = MapObject::Merchant;
    }

    Type type;
    Inventory inventory;
    QList<QString> spells;
    UnitHireList units;
    QString name;
    QString desc;
    int image;
    int priority;
};
template <> int getTypeId<MerchantObject>() {return MapObject::Merchant;}
Q_DECLARE_METATYPE(MerchantObject)

#endif // MERCHANTOBJECT_H

#ifndef LOCATIONOBJECT_H
#define LOCATIONOBJECT_H

#include "MapObject.h"
#include "toolsqt/DBFModel/GameClases.h"

class LocationObject : public MapObject
{
public:
    LocationObject(){ uid.first = MapObject::Location; }
    LocationObject(const QString & name, int x, int y, int r)
    {
        uid.first = MapObject::Location;
        this->x = x - (r - 1) / 2;
        this->y = y - (r - 1) / 2;
        this->r = r;
        this->name = name;
    }
    QString name;
    int r;
};
template <> int getTypeId<LocationObject>() {return MapObject::Location;}
Q_DECLARE_METATYPE(LocationObject)

#endif // LOCATIONOBJECT_H

#ifndef TREASUREOBJECT_H
#define TREASUREOBJECT_H
#include "MapObject.h"
#include "toolsqt/DBFModel/GameClases.h"
#include "BaseClases.h"

class TreasureObject : public MapObject
{
public:
    TreasureObject()
    {
        uid.first = MapObject::Treasure;
    }
    Inventory inventory;
    int image;
    int AIpriority = 3;
};
template <> int getTypeId<TreasureObject>() {return MapObject::Treasure;}
Q_DECLARE_METATYPE(TreasureObject)

//G000RR0005SHLC8 elves shield
//STACK_BANNER_1400 - elves bunner
//STACK_BANNER_EMPIRE -  bunner
//STACK_BANNER_WOLF -  bunner
//STACK_BANNER_ELF -  bunner LRace
//G000RR000[0-x]flag8
#endif // TREASUREOBJECT_H

#include "GridObject.h"
#include "Engine/Components/AccessorHolder.h"
#include <QPainter>
#include "MapObjects/StackObject.h"
#include "MapObjects/LandmarkObject.h"
#include "Engine/GameInstance.h"
#include "Engine/Map/MapStateHolder.h"
#include "toolsqt/DBFModel/DBFModel.h"
#include "toolsqt/Common/Logger.h"
#include "Events/EventBus.h"
#include "Engine/Components/DisplaySettingsManager.h"
#include "Events/MapEvents.h"

GridView::GridView() : m_mapSize(0)
{
    m_tools = nullptr;
    m_polygon.append(cartesianToIsometric(0, 0));
    m_polygon.append(cartesianToIsometric(TILE_SIZE, 0));

    m_polygon.append(cartesianToIsometric(TILE_SIZE, TILE_SIZE));
    m_polygon.append(cartesianToIsometric(0, TILE_SIZE));
    m_polygon.translate(TILE_SIZE, 0);
    m_guardBrush = QBrush(QColor(255,153,255, 120), Qt::BrushStyle::DiagCrossPattern);
    m_nonPassableBrush = QBrush(QColor(204,51,0,120), Qt::BrushStyle::SolidPattern);
    m_dangerBrush = QBrush(QColor(204,151,151,120), Qt::BrushStyle::SolidPattern);
    m_terraformingBrush = QBrush(QColor(151,152,52,120), Qt::BrushStyle::SolidPattern);
    m_forestBrush = QBrush(QColor(51,152,152,120), Qt::BrushStyle::SolidPattern);
    m_roadBrush = QBrush(QColor(153,152,152,180), Qt::BrushStyle::SolidPattern);

    subscribe<DisplaySettingsChangedEvent>([this](const QVariant &event){onLayersSettingsUpdate(event);});
    subscribe<ObjectChangedEvent>([this](const QVariant &event){onObjectUpdate(event);});
    subscribe<ObjectRemovedEvent>([this](const QVariant &event){onObjectUpdate(event);});
    subscribe<ObjectAddedEvent>([this](const QVariant &event){onObjectUpdate(event);});
    subscribe<HoveredObjectChangedEvent>([this](const QVariant &event){onHoverChanged(event);});
    setAcceptHoverEvents(true);
}

QRectF GridView::boundingRect() const
{
    return m_mapShape.boundingRect();
}

QPainterPath GridView::shape() const
{
    QPainterPath path;
    path.addPolygon(m_mapShape);
    return path;
}

void GridView::paint(QPainter *painter, const QStyleOptionGraphicsItem *item, QWidget *widget)
{
    if (m_mapSize ==0)
        return;
    auto settings = RESOLVE(DisplaySettingsManager)->settings;
    if (RESOLVE(DisplaySettingsManager)->settings.gridEnabled)
    {
        painter->setPen(QPen(QColor("#339966"), 2));
        painter->drawPolygon(m_mapShape);
        for (int i = 0; i < m_mapSize; ++i)
        {
            {
                QPointF point = m_mapShape[0];
                point.setX(point.x() + i * TILE_SIZE);
                point.setY(point.y() + i * TILE_SIZE / 2);

                QPointF point2 = m_mapShape[1];
                point2.setX(point2.x() + i * TILE_SIZE);
                point2.setY(point2.y() + i * TILE_SIZE / 2);
                painter->drawLine(point, point2);
            }
            {
                QPointF point = m_mapShape[0];
                point.setX(point.x() - i * TILE_SIZE);
                point.setY(point.y() + i * TILE_SIZE / 2);

                QPointF point2 = m_mapShape[3];
                point2.setX(point2.x() - i * TILE_SIZE);
                point2.setY(point2.y() + i * TILE_SIZE / 2);
                painter->drawLine(point, point2);
            }
        }
    }
    for (int  i = 0; i < m_mapSize; i++)
    {
        for (int k = 0; k < m_mapSize; ++k)
        {
            if (settings.terraformingEnabled && !m_terraformingMap[i][k])
            {
                drawCell(painter, i,k, m_terraformingBrush);
            }
            if (settings.passableEnabled && m_passableMap[i][k])
            {
                drawCell(painter, i,k, m_nonPassableBrush);
                continue;
            }
            if (settings.dangerEnabled && m_guardMap[i][k])
            {
                drawCell(painter, i,k, m_guardBrush);
            }
            if (settings.dangerEnabled && m_dangerMap[i][k])
            {
                drawCell(painter, i,k, m_dangerBrush);
            }
            if (settings.forestEnabled && m_forestMap[i][k])
            {
                drawCell(painter, i,k, m_forestBrush);
            }
            if (settings.roadsEnabled && m_roadMap[i][k])
            {
                drawCell(painter, i,k, m_roadBrush);
            }
        }
    }
    for (int i = 0; i < m_hovered.count(); ++i)
    {
        painter->setPen(QPen(Qt::yellow, 3));
        painter->drawPolygon(m_hovered[i]);
        painter->setPen(QPen(Qt::red, 1));
        painter->drawPolygon(m_hovered[i]);
    }
    if (m_tools != nullptr)
    {
        if (m_tools->hasActiveTool())
            m_tools->currentTool()->draw(painter);
    }
}

void GridView::drawCell(QPainter *painter, int x, int y, const QBrush &brush)
{
    QPolygonF pol = m_polygon;
    int w = m_mapSize * TILE_SIZE * 2;

    QPointF coord = cartesianToIsometric(x * TILE_SIZE, y * TILE_SIZE);
    x = w/2 + coord.x() - TILE_SIZE;
    y = coord.y();// + TILE_SIZE / 2;

    pol.translate(x,y);
    QPainterPath path;
    path.addPolygon(pol);
    painter->fillPath(path, brush);
}

void GridView::reloadMaps()
{
    MapGrid grid = RESOLVE(MapStateHolder)->getGrid();
    m_mapSize = grid.cells.count();
    setZValue(1000);
    int h = grid.cells.count() * TILE_SIZE;
    int w = grid.cells.count() * TILE_SIZE * 2;
    m_mapShape.clear();
    m_mapShape.append(QPointF(w/2, 0));
    m_mapShape.append(QPointF(0, h /2));
    m_mapShape.append(QPointF(w/2,h));
    m_mapShape.append(QPointF(w, h / 2));

    clearMapArray(m_passableMap);
    placeCollection(MapObject::LandMark, m_passableMap);
    placeCollection(MapObject::Fort, m_passableMap);
    placeCollection(MapObject::Mountain, m_passableMap);
    placeCollection(MapObject::Crystal, m_passableMap);
    placeCollection(MapObject::Ruin, m_passableMap);
    placeCollection(MapObject::Merchant, m_passableMap);

    clearMapArray(m_dangerMap);
    {
        QSharedPointer<IMapObjectAccessor> accessor = RESOLVE(AccessorHolder)->objectAccessor(MapObject::Stack);
        ObjectsCollection collection = RESOLVE(MapStateHolder)->collectionByType(MapObject::Stack);
        foreach (size_t uid, collection.data.keys())
        {
            QSharedPointer<MapObject> object = RESOLVE(MapStateHolder)->objectById(QPair<int, int>(MapObject::Stack, uid));
            StackObject * obj = static_cast<StackObject *>(object.data());
            if (obj->inside.second != -1)
                continue;
            int w = accessor->getW(object);
            int h = accessor->getH(object);

            placeObj(w, h, object, m_dangerMap, 1);
        }
    }

    clearMapArray(m_guardMap);
    {
        QSharedPointer<IMapObjectAccessor> accessor = RESOLVE(AccessorHolder)->objectAccessor(MapObject::Stack);
        ObjectsCollection collection = RESOLVE(MapStateHolder)->collectionByType(MapObject::Stack);
        auto value = RESOLVE(DBFModel)->getList<GVars>()[0];
        foreach (size_t uid, collection.data.keys())
        {
            QSharedPointer<MapObject> object = RESOLVE(MapStateHolder)->objectById(QPair<int, int>(MapObject::Stack, uid));
            StackObject * obj = static_cast<StackObject *>(object.data());
            if (obj->inside.second != -1)
                continue;
            if (obj->order == StackObject::Guard) {
                int w = accessor->getW(object);
                int h = accessor->getH(object);

                placeObj(w, h, object, m_guardMap, (value->gu_range - 1) / 2);
            }
        }
    }
    clearMapArray(m_terraformingMap);
    placeCollection(MapObject::Mountain, m_terraformingMap);
    {
        QSharedPointer<IMapObjectAccessor> accessor = RESOLVE(AccessorHolder)->objectAccessor(MapObject::LandMark);
        ObjectsCollection collection = RESOLVE(MapStateHolder)->collectionByType(MapObject::LandMark);
        foreach (size_t uid, collection.data.keys())
        {
            QSharedPointer<MapObject> object = RESOLVE(MapStateHolder)->objectById(QPair<int, int>(MapObject::LandMark, uid));
            LandmarkObject * obj = static_cast<LandmarkObject *>(object.data());
            auto base = RESOLVE(DBFModel)->get<GLmark>(obj->lmarkId);
            if (base->mountain) {
                int w = accessor->getW(object);
                int h = accessor->getH(object);

                placeObj(w, h, object, m_terraformingMap, 0);
            }
        }
    }
    for (int  i = 0; i < grid.cells.count(); i++)
    {
        for (int k = 0; k < grid.cells.count(); ++k)
        {
            if (tileGround(grid.cells[i][k].value)  == 3)
            {
                m_terraformingMap[i][k] = true;
            }
        }
    }
    clearMapArray(m_forestMap);
    for (int  i = 0; i < grid.cells.count(); i++)
    {
        for (int k = 0; k < grid.cells.count(); ++k)
        {
            if (tileGround(grid.cells[i][k].value) == 1)
            {
                m_forestMap[i][k] = true;
            }
        }
    }
    clearMapArray(m_roadMap);
    for (int  i = 0; i < grid.cells.count(); i++)
    {
        for (int k = 0; k < grid.cells.count(); ++k)
        {
            if (grid.cells[i][k].roadType != -1)
            {
                m_roadMap[i][k] = true;
            }
        }
    }

}

void GridView::placeObj(int w, int h, QSharedPointer<MapObject> object, QList<QBitArray> &array, int r)
{
    for(int i = 0; i < w + 2 * r; ++i)
    {
        for(int k = 0; k < h + 2 * r; ++k)
        {
            int x = object->x + i - r;
            int y = object->y + k - r;
            if (x >=0 && (x < m_mapSize) &&  y >= 0 && (y < m_mapSize))
                array[x][y] = true;
        }
    }
}

void GridView::placeCollection(size_t type, QList<QBitArray> &array, int r)
{
    QSharedPointer<IMapObjectAccessor> accessor = RESOLVE(AccessorHolder)->objectAccessor(type);
    ObjectsCollection collection = RESOLVE(MapStateHolder)->collectionByType(type);
    foreach (size_t uid, collection.data.keys())
    {
        QSharedPointer<MapObject> object = RESOLVE(MapStateHolder)->objectById(QPair<int, int>(type, uid));
        int w = accessor->getW(object);
        int h = accessor->getH(object);

        placeObj(w, h, object, array, r);
    }
}

void GridView::clearMapArray(QList<QBitArray> &array)
{
    array.clear();
    for (int i = 0; i < m_mapSize; ++i)
    {
        array.append(QBitArray());
        array[i].resize(m_mapSize);
        array[i].fill(false);
    }
}

void GridView::onLayersSettingsUpdate(const QVariant &eventVar)
{
    Q_UNUSED(eventVar)
    reloadMaps();
    update();
}

void GridView::onObjectUpdate(const QVariant &event)
{
    Q_UNUSED(event);
    reloadMaps();
    update();
}

void GridView::onHoverChanged(const QVariant &eventVar)
{
    m_hovered.clear();
    HoveredObjectChangedEvent event = eventVar.value<HoveredObjectChangedEvent>();
    if (event.hover_in != EMPTY_ID)
    {
        const auto & uid = event.hover_in;
        QSharedPointer<MapObject> mapObj =
            RESOLVE(MapStateHolder)->objectById(uid);
        QSharedPointer<IMapObjectAccessor> accessor =
            RESOLVE(AccessorHolder)->objectAccessor(mapObj->uid.first);
        int w = accessor->getW(mapObj);
        int h = accessor->getH(mapObj);
        int x_= mapObj->x;
        int y_ = mapObj->y;
        QPolygonF polygon;
        polygon<<QPointF(0, 0);
        polygon<< cartesianToIsometric(w * TILE_SIZE, 0);
        polygon<< cartesianToIsometric(w * TILE_SIZE, h * TILE_SIZE);
        polygon<< cartesianToIsometric(0, h * TILE_SIZE);
        QPointF pos = cartesianToIsometric(x_ * TILE_SIZE, y_ * TILE_SIZE);
        polygon.translate(pos.x() + m_mapShape.boundingRect().center().x(), pos.y());
        m_hovered << polygon;
    }
    update();
}

void GridView::onTimer()
{
    if (m_tools != nullptr && m_tools->hasActiveTool())
    {
        if (m_tools->currentTool()->neadUpdate())
            update(m_tools->currentTool()->toolRect());
    }
}

ToolsProvider *GridView::tools() const
{
    return m_tools;
}

void GridView::setTools(ToolsProvider *newTools)
{
    m_tools = newTools;
}

#include "BaseClases.h"
#include "Engine/GameInstance.h"
#include "Engine/Map/MapStateHolder.h"
#include "toolsqt/DBFModel/DBFModel.h"

QString GroupData::toString() const
{
    QString result = "";
    for(int i = 0; i < units.count(); ++i)
    {
        auto unit = RESOLVE(MapStateHolder)->objectByIdT<UnitObject>(units[i].first);
        auto gunit = RESOLVE(DBFModel)->get<Gunit>(unit.id);
        result += gunit->name_txt->text + QString(" %1-%2\n").arg(units[i].second.x())
                                              .arg(units[i].second.y());
    }
    return result;
}

GroupData::GroupData()
{

}

void GroupData::addUnit(UnitObject unit, const QPoint &pos)
{
    units.append(QPair<QPair<int,int>, QPoint>(unit.uid, pos));
}

void GroupData::addUnit(QPair<int, int> uid, const QPoint &pos)
{
    units.append(QPair<QPair<int,int>, QPoint>(uid, pos));
}

bool GroupData::isLeader(int x, int y)
{
    if (!hasUnit(QPoint(x, y)))
        return false;
    return unitByPos(QPoint(x, y)).leader;
}

void GroupData::moveUnit(const QPoint &from, const QPoint &newPos)
{
    for(int i = 0; i < units.count(); ++i)
    {
        QPoint point = units[i].second;
        if (point.x() == from.x() && point.y() == from.y())
        {
            units[i].second = newPos;
            return;
        }
    }
    for(int i = 0; i < units.count(); ++i)
    {
        QPoint point = units[i].second;
        if (point.y() == from.y())
        {
            auto unit = RESOLVE(MapStateHolder)->objectByIdT<UnitObject>(units[i].first);
            auto gunit = RESOLVE(DBFModel)->get<Gunit>(unit.id);
            if (!gunit->size_small)
            {
                units[i].second = QPoint(0, newPos.y());
                return;
            }
        }
    }
}

int GroupData::unitIndexByPos(const QPoint &pos) const
{
    for(int i = 0; i < units.count(); ++i)
    {
        QPoint point = units[i].second;
        if (point.x() == pos.x() && point.y() == pos.y())
        {
            return i;
        }
    }
    for(int i = 0; i < units.count(); ++i)
    {
        QPoint point = units[i].second;
        if (point.y() == pos.y())
        {
            auto unit = RESOLVE(MapStateHolder)->objectByIdT<UnitObject>(units[i].first);
            auto gunit = RESOLVE(DBFModel)->get<Gunit>(unit.id);
            if (!gunit->size_small)
                return i;
        }
    }
    return -1;
}

bool GroupData::hasUnit(const QPoint &pos) const
{
    return unitIndexByPos(pos) != -1;
}

void GroupData::remove(int x, int y)
{
    for(int i = 0; i < units.count(); ++i)
    {
        QPoint point = units[i].second;
        if (point.x() == x && point.y() == y)
        {
            units.removeAt(i);
            return;
        }
    }
    for(int i = 0; i < units.count(); ++i)
    {
        QPoint point = units[i].second;
        if (point.y() == y)
        {
            auto unit = RESOLVE(MapStateHolder)->objectByIdT<UnitObject>(units[i].first);
            auto gunit = RESOLVE(DBFModel)->get<Gunit>(unit.id);
            if (!gunit->size_small)
            {
                units.removeAt(i);
                return;
            }
        }
    }
}

UnitObject GroupData::unitByPos(const QPoint &pos) const
{
    for(int i = 0; i < units.count(); ++i)
    {
        QPoint point = units[i].second;
        if (point.x() == pos.x() && point.y() == pos.y())
        {
            auto unit = RESOLVE(MapStateHolder)->objectByIdT<UnitObject>(units[i].first);
            return unit;
        }
    }
    for(int i = 0; i < units.count(); ++i)
    {
        QPoint point = units[i].second;
        if (point.y() == pos.y())
        {
            auto unit = RESOLVE(MapStateHolder)->objectByIdT<UnitObject>(units[i].first);
            auto gunit = RESOLVE(DBFModel)->get<Gunit>(unit.id);
            if (!gunit->size_small)
                return unit;
        }
    }
    return UnitObject(0,0);
}

QString GroupData::leaderId() const
{
    for(int i = 0; i < units.count(); ++i)
    {
        auto unit = RESOLVE(MapStateHolder)->objectByIdT<UnitObject>(units[i].first);
        if (unit.leader)
            return unit.id;
    }
    return QString();
}

int GroupData::leaderIndex() const
{
    for(int i = 0; i < units.count(); ++i)
    {
        auto unit = RESOLVE(MapStateHolder)->objectByIdT<UnitObject>(units[i].first);
        if (unit.leader)
            return i;
    }
    return -1;
}

int GroupData::leadership() const
{
    if (leaderIndex() == -1)
        return 0;
    auto leader = RESOLVE(MapStateHolder)->objectByIdT<UnitObject>(units[leaderIndex()].first);
    auto leaderBase = RESOLVE(DBFModel)->get<Gunit>(leader.id);
    int leaderShip = leaderBase->leadership;
    foreach (const QString & modId, leader.modifiers) {
        if (modId == LEADER_INC)
            leaderShip++;
        else if (modId == LEADER_DEC)
            leaderShip--;
    }
    return leaderShip;
}

int GroupData::requiredLeadership() const
{
    int leaderShip = 0;
    for(int i = 0; i < units.count(); ++i)
    {
        auto unit = RESOLVE(MapStateHolder)->objectByIdT<UnitObject>(units[i].first);
        auto gunit = RESOLVE(DBFModel)->get<Gunit>(unit.id);
        if (gunit->size_small)
            leaderShip++;
        else
            leaderShip+=2;
    }
    return leaderShip;
}

QString groupDataDesc(const GroupData &group)
{
    QString result;
    for(int i = 0; i < group.units.count(); ++i)
    {
        auto unit = RESOLVE(MapStateHolder)->objectByIdT<UnitObject>(group.units[i].first);
        if (unit.leader) {
            result += unit.name;
        }
    }
    return result;
}

int groupExpForKill(const GroupData &group)
{
    int expForKill = 0;
    for(int i = 0; i < group.units.count(); ++i)
    {
        auto unit = RESOLVE(MapStateHolder)->objectByIdT<UnitObject>(group.units[i].first);
        expForKill += calcKilledExp(unit.id, unit.level);
    }
    return expForKill;
}

int calcKilledExp(const QString &unit, int level)
{
    auto gunit = RESOLVE(DBFModel)->get<Gunit>(unit);
    auto upgr1 = gunit->dyn_upg1;
    auto upgr2 = gunit->dyn_upg2;
    return calcLeveledValue(gunit->xp_killed,
                            gunit->level, level,
                            gunit->dyn_upg_lv,
                            upgr1.value->xp_killed,
                            upgr2.value->xp_killed);
}

int calcConsumedExp(const QString &unit, int level, bool withUpd)
{
    auto gunit = RESOLVE(DBFModel)->get<Gunit>(unit);
    int result = withUpd ? gunit->xp_next : 0;
    int over1Count = qMin(level, gunit->dyn_upg_lv) - gunit->level;
    int over2Count = qMax(level, gunit->dyn_upg_lv) - gunit->dyn_upg_lv;
    result += gunit->xp_next * (over1Count + over2Count);
    result += gunit->dyn_upg1.value->xp_next * (over1Count);
    result += gunit->dyn_upg2.value->xp_next * (over2Count);
    if (!gunit->prev_id.value.isNull())
        result += calcConsumedExp(gunit->prev_id.value->unit_id, gunit->prev_id.value->level, true);
    return result;
}

#include "Diplomacy.h"
#include "Engine/GameInstance.h"
#include "toolsqt/DBFModel/DBFModel.h"

QString SubRaceObject::getName() const
{
    QString result = this->name;
    if (result.isEmpty())
    {
        QSharedPointer<LSubRace> race = RESOLVE(DBFModel)->get<LSubRace>(QString::number(subrace));
        if (!race.isNull())
            result = race->text;
    }
    return result;
}

QString PlayerObject::getName() const
{
    QString result = this->name;
    if (result.isEmpty())
    {
        QSharedPointer<Grace> race = RESOLVE(DBFModel)->get<Grace>(raceId);
        qDebug()<<"PlayerObject::getName"<<raceId;
        if (!race.isNull())
            result = race->name_txt->text;
    }
    return result;
}

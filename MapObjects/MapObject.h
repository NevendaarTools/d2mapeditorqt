#ifndef MAPOBJECT_H
#define MAPOBJECT_H
#include <QString>
#include <QStringList>
#include <QImage>
#include <QMenu>
#include <toolsqt/MapUtils/DataBlock.h>
#include <toolsqt/MapUtils/MapGrid.h>
#include "Common/TemplateFunctions.h"

#define TILE_SIZE 32//96/2

#define EMPTY_ID QPair<int, int>(0,0)
#define MapStatistics 10000

static QString IdToString(const QPair<int, int> & id)
{
    return QString("%1:%2").arg(id.first).arg(id.second);
}
static QPair<int, int> IdFromString(const QString& id)
{
    QStringList data = id.split(":");
    if (data.count() < 2)
        return EMPTY_ID;
    return QPair<int, int>{data[0].toInt(), data[1].toInt()};
}

struct Currency
{
    //G0100:R0000:Y0000:E0000:W0000:B0000
    //    G - золото
    //R - мана преисподней
    //Y - мана жизни
    //E - мана смерти
    //W - мана рун
    //B - мана рощи
    int gold = 0;
    int demons = 0;
    int empire = 0;
    int undead = 0;
    int clans = 0;
    int elves = 0;

    int totalCount () const
    {
        return gold + demons + empire + undead + clans + elves;
    }

    static int valueByLetter(const QString& string, const QChar& letter)
    {
        int index = string.indexOf(letter, Qt::CaseInsensitive);
        if (index == -1)
            return  0;
        return string.mid(index + 1, 4).toInt();
    }

    static Currency fromString(const QString& string)
    {
        Currency result;
        result.read(string.toUpper());
        return result;
    }
    void read(const QString& string)
    {
        gold = valueByLetter(string, 'G');
        demons = valueByLetter(string, 'R');
        empire = valueByLetter(string, 'Y');
        undead = valueByLetter(string, 'E');
        clans = valueByLetter(string, 'W');
        elves = valueByLetter(string, 'B');
    }
    void clear()
    {
        gold = 0;
        demons = 0;
        empire = 0;
        undead = 0;
        clans = 0;
        elves = 0;
    }

    QString toString(const QString & del = "") const
    {
        return  "G" + QString::number(gold).rightJustified(4, '0') + del +
                "R" + QString::number(demons).rightJustified(4, '0') + del +
                "Y" + QString::number(empire).rightJustified(4, '0') + del +
                "E" + QString::number(undead).rightJustified(4, '0') + del +
                "W" + QString::number(clans).rightJustified(4, '0') + del +
                "B" + QString::number(elves).rightJustified(4, '0');
    }

    Currency& operator+=(const Currency& other)
    {
        gold += other.gold;
        demons += other.demons;
        empire += other.empire;
        undead += other.undead;
        clans += other.clans;
        elves += other.elves;
        return *this;
    }

    Currency& operator-=(const Currency& other)
    {
        gold -= other.gold;
        demons -= other.demons;
        empire -= other.empire;
        undead -= other.undead;
        clans -= other.clans;
        elves -= other.elves;
        return *this;
    }

    friend bool operator==(const Currency& lhs, const Currency& rhs)
    {
        return lhs.gold == rhs.gold
               && lhs.demons == rhs.demons
               && lhs.empire == rhs.empire
               && lhs.undead == rhs.undead
               && lhs.clans == rhs.clans
               && lhs.elves == rhs.elves;
    }

    friend bool operator!=(const Currency& lhs, const Currency& rhs)
    {
        return !(lhs == rhs);
    }

    friend Currency operator+(Currency lhs, const Currency& rhs)
    {
        lhs += rhs;
        return lhs;
    }

    friend Currency operator-(Currency lhs, const Currency& rhs)
    {
        lhs -= rhs;
        return lhs;
    }
};

class MapObject
{
public:
    enum Type
    {
        Stack = IDataBlock::Stack,
        StackTemplate = IDataBlock::StackTemplate,
        LandMark = IDataBlock::LandMark,
        Fort = IDataBlock::Fort,
        Mountain = IDataBlock::Mountain,
        Crystal = IDataBlock::Crystal,
        Ruin = IDataBlock::Ruin,
        Treasure = IDataBlock::Treasure,
        Merchant = IDataBlock::Merchant,
        Location = IDataBlock::Location,
        Event = IDataBlock::Event,
        Unit = IDataBlock::Unit,
        SubRace = IDataBlock::SubRace,
        Player = IDataBlock::Player,
        Info = IDataBlock::ScenarioInfo,
        ScenVariable = IDataBlock::Variables,
        Relation = IDataBlock::Diplomacy,
        Rod = IDataBlock::Rod,
        Tomb = IDataBlock::Tomb,
        QuestLine = 1001,

        TextLink,

        COUNT
    };
    static QString typeName(MapObject::Type type)
    {
        switch(type)
        {
        case MapObject::Info:
            return "Info";
        case MapObject::ScenVariable:
            return "ScenVariable";
        case MapObject::Relation:
            return "Relation";
        case MapObject::Rod:
            return "Rod";
        case MapObject::Tomb:
            return "Tomb";
        case MapObject::QuestLine:
            return "QuestLine";
        case MapObject::StackTemplate:
            return "StackTemplate";
        case MapObject::SubRace:
            return "SubRace";
        case MapObject::Player:
            return "Player";
        case MapObject::Stack:
            return "Stack";
        case MapObject::LandMark:
            return "LandMark";
        case MapObject::Fort:
            return "Fort";
        case MapObject::Mountain:
            return "Mountain";
        case MapObject::Crystal:
            return "Crystal";
        case MapObject::Ruin:
            return "Ruin";
        case MapObject::Treasure:
            return "Treasure";
        case MapObject::Merchant:
            return "Merchant";
        case MapObject::Location:
            return "Location";
        case MapObject::Event:
            return "Event";
        case MapObject::Unit:
            return "Unit";
        case MapObject::COUNT:
            break;
        }
        return "Unknown type";
    }

    virtual ~MapObject(){}
    QPair<int, int> uid;//type - object number
    int x;
    int y;
    bool populate = true;
    QString note;
    QString getNote() const
    {
        if (note.isEmpty())
            return "";
        return "(" + note + ")";
    }
};


class QQuickWidget;

class IObjectEditor
{
public:
    virtual ~IObjectEditor(){}

    virtual void setup(QQuickWidget* editor) = 0;
    virtual void setUID(QPair<int, int> uid) = 0;
    virtual QObject * toObject() = 0;
    virtual QString validate() = 0;
};

#endif // MAPOBJECT_H

#include  "MapLink.h"
#include "Engine/GameInstance.h"
#include "Engine/Map/MapStateHolder.h"

QSharedPointer<MapObject> MapLinkBase::getObject(QPair<int, int> uid) const
{
    return RESOLVE(MapStateHolder)->objectById(uid);
}


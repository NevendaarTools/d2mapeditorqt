#ifndef RODOBJECT_H
#define RODOBJECT_H
#include "MapObject.h"
#include "toolsqt/DBFModel/GameClases.h"
#include "toolsqt/MapUtils/DataBlocks/D2Rod.h"
#include "Diplomacy.h"

class RodObject : public MapObject
{
public:
    RodObject()
    {
        uid.first = MapObject::Rod;
    }
    static int rodByRaceID(int raceId)
    {
        switch (raceId) {
        case 1:
            return 3;
            break;
        case 3:
            return 1;
        default:
            break;
        }
        return raceId;
    }
    MapLink<PlayerObject> owner;

    RodObject(const D2Rod & rod)
    {
        uid.first = MapObject::Rod;
        uid.second = rod.uid.second;
        owner = MapLink<PlayerObject>(rod.owner.second);;
        x = rod.posX;
        y = rod.posY;
    }
    D2Rod toD2() const
    {
        D2Rod res;
        res.posX = x;
        res.posY = y;
        res.owner = owner;
        res.uid.second = uid.second;
        return res;
    }
};
template <> int getTypeId<RodObject>() {return MapObject::Rod;}
Q_DECLARE_METATYPE(RodObject)


#endif // RODOBJECT_H

#ifndef GRIDOBJECT_H
#define GRIDOBJECT_H

#include <QGraphicsItem>
#include <Commands/UIEvents.h>
#include "Commands/CommandBus.h"
#include <QBitArray>
#include <QGraphicsSceneMouseEvent>
#include "QMLHelpers/Tools/ToolsProvider.h"

class GridView : public QObject, public QGraphicsItem, public EventSubscriber
{
    Q_OBJECT
public:
    GridView();
    void setup(MapGrid grid);
    QRectF boundingRect() const override;
    QPainterPath shape() const override;
    void paint(QPainter *painter, const QStyleOptionGraphicsItem *item, QWidget *widget) override;

    MapGrid grid() const;
    void drawCell(QPainter *painter, int x, int y, const QBrush &brush);
    void reloadMaps();

    void placeObj(int w, int h,
                  QSharedPointer<MapObject> object, QList<QBitArray> & array, int r);
    void placeCollection(size_t type, QList<QBitArray> & array, int r = 0);
    void clearMapArray(QList<QBitArray> & array);

    void onLayersSettingsUpdate(const QVariant& event);
    void onObjectUpdate(const QVariant& event);
    void onHoverChanged(const QVariant& eventVar);
    void onTimer();
    ToolsProvider *tools() const;
    void setTools(ToolsProvider *newTools);
private:


private:
    QPolygonF m_mapShape;
    int m_mapSize;
    QList<QBitArray> m_passableMap;
    QList<QBitArray> m_dangerMap;
    QList<QBitArray> m_guardMap;
    QList<QBitArray> m_terraformingMap;
    QList<QBitArray> m_forestMap;
    QList<QBitArray> m_roadMap;

    QBrush m_guardBrush;
    QBrush m_dangerBrush;
    QBrush m_nonPassableBrush;
    QBrush m_terraformingBrush;
    QBrush m_forestBrush;
    QBrush m_roadBrush;
    QPolygonF m_polygon;

    QPoint  m_cursorePos;
    QPair<int, int> m_lastHovered;
    QList<QPolygonF> m_hovered;
    ToolsProvider * m_tools;
    bool m_mousePressed = false;
};

#endif // GRIDOBJECT_H

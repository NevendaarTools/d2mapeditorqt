#ifndef GAMEMAP_H
#define GAMEMAP_H
#include "MapObject.h"
#include "MapLink.h"
#include "Engine/Option.h"
#include "Engine/version.h"

struct ObjectsCollection
{
public:
    QHash<int, QSharedPointer<MapObject> > data;
    QList<int> availableIds;
    size_t lastId = 0;

    int nextUID()
    {
        if (availableIds.isEmpty())
        {
            auto used = data.keys();
            for (int i = 0, k = 0; k < 100; ++i) {
                if (used.contains(i))
                    continue;
                availableIds.append(i);
                k++;
            }
        }
        int res = availableIds.first();
        availableIds.removeFirst();
        return res;
    }

    void addObject(QSharedPointer<MapObject> obj)
    {
        int key = nextUID();
        obj->uid.second = key;
        data.insert(key, obj);
        availableIds.removeAll(obj->uid.second);
    }

    void addObject(QSharedPointer<MapObject> obj, int key)
    {
        obj->uid.second = key;
        data.insert(key, obj);
        availableIds.removeAll(obj->uid.second);
    }

    void replaceObject(QSharedPointer<MapObject> obj)
    {
        data[obj->uid.second] = obj;
        availableIds.removeAll(obj->uid.second);
    }

    void removeObject(size_t uid)
    {
        data.remove(uid);
        availableIds.insert(0, uid);
    }
};

class GameMap
{
public:
    struct MapInfo
    {
        QString desc;
        QString name;
        int size;
        float version = EDITOR_VERSION;
    };
    GameMap();

    void clear()
    {
        m_mapObjects.clear();
        grid.clear();
    }

    QPair<int, int> addObject(QSharedPointer<MapObject> obj);
    void addObject(QSharedPointer<MapObject> obj, QPair<int, int> uid);
    template <class classT>
    QPair<int, int> addObject(classT & obj)
    {
        obj.uid = addObject(QSharedPointer<MapObject>(new classT(obj)));
        return obj.uid;
    }
    bool hasObject(QPair<int, int> uid);
    QSharedPointer<MapObject> objectById(QPair<int, int> uid) const;
    template <class classT>
    classT objectByIdT(QPair<int, int> uid) const
    {
        auto object = objectById(uid);
        return classT(*static_cast<classT *>(object.data()));

    }

    template <class classT>
    classT objectByIdT(int uid)
    {
        int type = getTypeId<classT>();;
        return objectByIdT<classT>(QPair<int, int>(type, uid));
    }

    void replaceObject(QSharedPointer<MapObject> obj);
    template <class classT>
    void replaceObject(const classT & obj)
    {
        replaceObject(QSharedPointer<MapObject>(new classT(obj)));
    }
    void removeObject(QPair<int, int> uid);

    MapGrid getGrid() const;
    void setGrid(const MapGrid &value);

    int mapSize() const {return grid.cells.count();}

    QList<QSharedPointer<MapObject> > objects() const;
    QList<int>  collectionTypes() const;
    ObjectsCollection collectionByType(size_t type) const
    {
        return m_mapObjects[type];
    }

    QList<QPair<int, int>> uidsByType(int type) const
    {
        QList<QPair<int, int>> result;
        auto keys = m_mapObjects[type].data.keys();
        result.reserve(keys.count());
        foreach (const int & key, keys)
        {
            result << QPair<int, int>(type, key);
        }

        return result;
    }
    template <class classT>
    QList<classT> objectsByType()
    {
        int type = getTypeId<classT>();
        QList<classT> result;
        auto ids = uidsByType(type);
        result.reserve(ids.count());
        foreach (auto id, ids) {
            result<< objectByIdT<classT>(id);
        }
        return result;
    }

    template <class classT>
    void removeByType()
    {
        int type = getTypeId<classT>();
        auto ids = uidsByType(type);
        foreach (const auto & id, ids) {
            removeObject(id);
        }
    }

    const QString &getMapName() const;
    void setMapName(const QString &newMapName);

    const QString &getMapDesc() const;
    void setMapDesc(const QString &newMapDesc);
    void removeFromGrid(const QPair<int, int>& uid);
    const QList<Option> &settings() const;
    void setSettings(const QList<Option> &newSettings);
    float getVersion() const {return info.version;}
    void setVersion(float version) {info.version = version;}
private:
    QHash<int, ObjectsCollection> m_mapObjects;
    MapGrid grid;
    MapInfo info;
    QList<Option> m_settings;
};


#endif // GAMEMAP_H

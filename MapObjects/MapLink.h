#ifndef MAPLINK_H
#define MAPLINK_H
#include <QPair>
#include <QSharedPointer>
#include "MapObject.h"
#include "Common/TemplateFunctions.h"

struct MapLinkBase
{
protected:
    QSharedPointer<MapObject> getObject(QPair<int, int> uid) const;
};

template <class classT>
struct MapLink : public QPair<int, int>, public MapLinkBase
{
    classT get() const
    {
        auto uid = QPair<int, int>(this->first, this->second);
        auto object = getObject(uid);
        if (object.isNull())
        {
            qDebug()<<"Failed to get object by id:"<<uid;
            return classT();
        }
        return classT(*static_cast<classT *>(object.data()));
    }
    MapLink(){first = getTypeId<classT>();}
    MapLink(int id)
    {
        first = getTypeId<classT>();
        second = id;
    }
    MapLink(QPair<int, int> id)
    {
        first = getTypeId<classT>();
        second = id.second;
    }
};

#endif // MAPLINK_H

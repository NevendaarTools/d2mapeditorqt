#ifndef MOUNTAINOBJECT_H
#define MOUNTAINOBJECT_H
#include "MapObject.h"
#include "toolsqt/DBFModel/GameClases.h"

class MountainObject : public MapObject
{
public:
    MountainObject(){uid.first = MapObject::Mountain;}
    int w;
    int h;
    int image;
    int race;
};
template <> int getTypeId<MountainObject>() {return MapObject::Mountain;}
Q_DECLARE_METATYPE(MountainObject)

#endif // MOUNTAINOBJECT_H

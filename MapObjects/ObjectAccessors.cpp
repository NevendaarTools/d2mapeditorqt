#include "ObjectAccessors.h"
#include "MapObjects/UnitObject.h"
#include "MapObjects/TreasureObject.h"
#include "MapObjects/TombObject.h"
#include "MapObjects/MountainObject.h"
#include "MapObjects/RuinObject.h"
#include "MapObjects/StackTemplateObject.h"
#include "MapObjects/LocationObject.h"
#include "MapObjects/LandmarkObject.h"
#include "MapObjects/FortObject.h"
#include "MapObjects/RodObject.h"
#include "MapObjects/CrystalObject.h"
#include "Diplomacy.h"
#include "toolsqt/DBFModel/DBFModel.h"
#include "toolsqt/Common/Logger.h"
#include "Engine/GameInstance.h"
#include "Engine/Map/MapStateHolder.h"
#include "Engine/MapView/MinimapHelper.h"

#include "QMLHelpers/Editors/MapStackEditor.h"
#include "QMLHelpers/Editors/TreasureEditor.h"
#include "QMLHelpers/Editors/MerchantEditor.h"
#include "QMLHelpers/Editors/RuinEditor.h"
#include "QMLHelpers/Editors/QuestLineEditor.h"
#include "QMLHelpers/Editors/VillageEditor.h"
#include "QMLHelpers/Editors/MapStatisticView.h"
#include "QMLHelpers/Editors/MapInfoEditor.h"
#include "QMLHelpers/Components/StackTemplateEditor.h"

qreal UnitObjectAccessor::getZ(QSharedPointer<MapObject> object) const
{
    Q_UNUSED(object)
    return 0;
}

QString UnitObjectAccessor::getName(QSharedPointer<MapObject> object) const
{
    UnitObject * obj = static_cast<UnitObject *>(object.data());
    if (!obj->name.isEmpty())
        return obj->name;

    auto unit = RESOLVE(DBFModel)->get<Gunit>(obj->id);
    if (!unit.isNull())
        return unit->name_txt->text.trimmed();
    return TranslationHelper::tr("Error");
}

QString UnitObjectAccessor::getDesc(QSharedPointer<MapObject> object) const
{
    UnitObject * obj = static_cast<UnitObject *>(object.data());
    auto unit = RESOLVE(DBFModel)->get<Gunit>(obj->id);
    if (!unit.isNull())
        return unit->desc_txt->text.trimmed();
    return TranslationHelper::tr("Error");
}

QSharedPointer<MapObject> UnitObjectAccessor::cloneObject(QSharedPointer<MapObject> object) const
{
    UnitObject * obj = static_cast<UnitObject*>(object.data());
    return QSharedPointer<MapObject>(new UnitObject(*obj));
}

QString TreasureObjectAccessor::getName(QSharedPointer<MapObject> object) const
{
    return "Treasure";
}

QSharedPointer<MapObject> TreasureObjectAccessor::cloneObject(QSharedPointer<MapObject> object) const
{
    TreasureObject * obj = static_cast<TreasureObject*>(object.data());
    return QSharedPointer<MapObject>(new TreasureObject(*obj));
}

QMenu *TreasureObjectAccessor::requestMenu(QSharedPointer<MapObject> object) const
{
    return nullptr;
}

QList<FrameData> TreasureObjectAccessor::frameData(QSharedPointer<MapObject> object) const
{
    TreasureObject * obj = static_cast<TreasureObject *>(object.data());
    QStringList ffsource = QStringList()<<"IsoCmon"<<"IsoAnim";
    auto map = RESOLVE(MapStateHolder);
    QString key;
    if (map->getGrid().cells[obj->x][obj->y].isWater())
        key = "G000BG00000" + QString::number(obj->image).rightJustified(2, '0');
    else
        key = "G000BG00001" + QString::number(obj->image).rightJustified(2, '0');
    QSharedPointer<ImagesData> images =
        RESOLVE(ResourceManager)->getImagesData(ffsource, key);

    QList<FrameData> framesData;

    FrameData frames;
    frames.images = images;
    framesData << frames;
    return framesData;
}

IObjectEditor *TreasureObjectAccessor::createEditor() const
{
    return new TreasureEditor();
}

QMenu *TombObjectAccessor::requestMenu(QSharedPointer<MapObject> object) const
{
    return nullptr;
}


QList<FrameData> TombObjectAccessor::frameData(QSharedPointer<MapObject> object) const
{
    QString key = "G000TB0000G";
    QSharedPointer<ImagesData> images =
        RESOLVE(ResourceManager)->getImagesData("IsoCmon", key, true);//, PreprocessingShaderType::TransparentBlackWithSizeOptimisation);

    QList<FrameData> framesData;

    FrameData frames;
    frames.images = images;
    framesData << frames;
    return framesData;
}

QString TombObjectAccessor::getName(QSharedPointer<MapObject> object) const
{
    return "";
}

QString TombObjectAccessor::getDesc(QSharedPointer<MapObject> object) const
{
    QString result;
    TombObject * obj = static_cast<TombObject *>(object.data());
    for (int i = 0; i < obj->tomb.entries.count(); ++i)
    {
        result += "\n" + TranslationHelper::tr("day: ") + QString::number(obj->tomb.entries[i].turn) +" ";
        result += obj->tomb.entries[i].name + " ";
        PlayerObject player = RESOLVE(MapStateHolder)->objectByIdT<PlayerObject>(obj->tomb.entries[i].killer);
        result += TranslationHelper::tr("was defeated by") + " " + player.name;
    }
    return result;
}

QSharedPointer<MapObject> TombObjectAccessor::cloneObject(QSharedPointer<MapObject> object) const
{
    TombObject * obj = static_cast<TombObject *>(object.data());
    return QSharedPointer<MapObject>(new TombObject(*obj));
}

qreal StackTemplateObjectAccessor::getZ(QSharedPointer<MapObject> object) const
{
    return 0;
}

QString StackTemplateObjectAccessor::getName(QSharedPointer<MapObject> object) const
{
    StackTemplateObject * obj = static_cast<StackTemplateObject *>(object.data());
    return obj->stack.name;
}

QString StackTemplateObjectAccessor::getDesc(QSharedPointer<MapObject> object) const
{
    StackTemplateObject * obj = static_cast<StackTemplateObject *>(object.data());
    return obj->stack.name;
}

QSharedPointer<MapObject> StackTemplateObjectAccessor::cloneObject(QSharedPointer<MapObject> object) const
{
    StackTemplateObject * obj = static_cast<StackTemplateObject*>(object.data());
    QSharedPointer<IMapObjectAccessor> accessor =
        RESOLVE(AccessorHolder)->objectAccessor(MapObject::Unit);
    auto newObj = new StackTemplateObject(*obj);
    auto & stack = newObj->stack;
    for (int i = 0; i < stack.units.count(); ++i)
    {
        auto unit = RESOLVE(MapStateHolder)->objectById(stack.units[i].first);
        auto newUnit = accessor->cloneObject(unit);
        auto uid = RESOLVE(MapStateHolder)->addObject(newUnit);
        stack.units[i].first = uid;
    }
    return QSharedPointer<MapObject>(newObj);
}

QMenu *StackTemplateObjectAccessor::requestMenu(QSharedPointer<MapObject> object) const
{
    return nullptr;
}

QList<FrameData> StackTemplateObjectAccessor::frameData(QSharedPointer<MapObject> object) const
{
    return QList<FrameData>();
}

IObjectEditor *StackTemplateObjectAccessor::createEditor() const
{
    return nullptr;
}

qreal StackObjectAccessor::getZ(QSharedPointer<MapObject> object) const
{
    return 15.2;
}

QString StackObjectAccessor::getName(QSharedPointer<MapObject> object) const
{
    StackObject * obj = static_cast<StackObject *>(object.data());
    QString res = obj->stack.name;
    if (!obj->note.isEmpty())
        res += "(" + obj->note + ")";
    return res;
}

QString StackObjectAccessor::getDesc(QSharedPointer<MapObject> object) const
{
    StackObject * obj = static_cast<StackObject *>(object.data());
    QString result;// = groupDataDesc(obj->stack) +  "<br>";
    result += TranslationHelper::tr("Order:") + " ";
    result += TranslationHelper::tr(StackObject::orderName(obj->order));
    if (obj->orderTarget != EMPTY_ID)
    {
        QSharedPointer<IMapObjectAccessor> accessor = RESOLVE(AccessorHolder)->objectAccessor(obj->orderTarget.first);
        if (!accessor.isNull())
        {
            QSharedPointer<MapObject> targetObj = RESOLVE(MapStateHolder)->objectById(obj->orderTarget);
            if (!targetObj.isNull())
            {
                result += "<br>" +  accessor->getName(targetObj)  + QString("[%1 : %2]").arg(targetObj->x).arg(targetObj->y);
            }
            else
                Logger::logError("Failed to get order target: stack = "  +
                                 QString("[%1:%2]").arg(obj->uid.first).arg(obj->uid.second) +
                                 " target:" +
                                 QString("[%1:%2]").arg(obj->orderTarget.first).arg(obj->orderTarget.second));
        }
    }

    result += "<br>";
    result += TranslationHelper::tr("Owner:") + " ";
    result += obj->owner.get().getName();
    result += "<br>";
    result += TranslationHelper::tr("Fraction:") + " ";
    result += TranslationHelper::tr(obj->subrace.get().getName());
    return result;
}

QSharedPointer<MapObject> StackObjectAccessor::cloneObject(QSharedPointer<MapObject> object) const
{
    StackObject * obj = static_cast<StackObject*>(object.data());
    QSharedPointer<IMapObjectAccessor> accessor =
        RESOLVE(AccessorHolder)->objectAccessor(MapObject::Unit);

    auto newObj = new StackObject(*obj);
    auto & stack = newObj->stack;
    for (int i = 0; i < stack.units.count(); ++i)
    {
        auto unit = RESOLVE(MapStateHolder)->objectById(stack.units[i].first);
        auto newUnit = accessor->cloneObject(unit);
        auto uid = RESOLVE(MapStateHolder)->addObject(newUnit);
        stack.units[i].first = uid;
    }

    return QSharedPointer<MapObject>(newObj);
}

QMenu *StackObjectAccessor::requestMenu(QSharedPointer<MapObject> object) const
{
    return nullptr;
}

QList<FrameData> StackObjectAccessor::frameData(QSharedPointer<MapObject> object) const
{
    StackObject * obj = static_cast<StackObject *>(object.data());
    if (obj->inside.second != -1)
        return QList<FrameData>();
    QStringList ffsource = QStringList()<<"IsoAnim"<<"IsoUnit";
    QList<FrameData> framesData;

    if (RESOLVE(MapStateHolder)->getGrid().cells[obj->x][obj->y].isWater())
    {
        bool isWaterLeader = false;
        auto leader = RESOLVE(DBFModel)->get<Gunit>(obj->stack.leaderId());
        if (leader->water_only)
            isWaterLeader = true;
        bool flying = false;
        bool has_0 = false;//plains
        bool has_1 = false;//forests
        bool has_3 = false;//water
        for (int var = 0; var < leader->mobilAbil.m_value.count(); ++var)
        {
            if (leader->mobilAbil.m_value[var]->m_ability == 0)
                has_0 = true;
            if (leader->mobilAbil.m_value[var]->m_ability == 1)
                has_1 = true;
            if (leader->mobilAbil.m_value[var]->m_ability == 3)
                has_3 = true;
        }
        flying = has_0 && has_1 && has_3;

        if (!isWaterLeader && !flying)
        {
            int race = leader->race_id->race_type.key;
            QString key = QString("G000RR000%1BOAT%2").arg(race).arg(obj->rotation);
            QString keyS = QString("G000RR000%1SBOA%2").arg(race).arg(obj->rotation);
            QSharedPointer<ImagesData> shadowImages =
                RESOLVE(ResourceManager)->getImagesData(ffsource, key,
                                                       true, PreprocessingShaderType::ShadowsWithSizeOptimisation);

            {
                FrameData frames;
                frames.images = shadowImages;
                framesData << frames;
            }
            QSharedPointer<ImagesData> images =
                RESOLVE(ResourceManager)->getImagesData(ffsource, keyS);

            {
                FrameData frames;
                frames.images = images;
                framesData << frames;
            }
            return framesData;
        }
    }
    QSharedPointer<ImagesData> shadowImages =
        RESOLVE(ResourceManager)->getImagesData(ffsource, obj->stack.leaderId() + "SSTO" + QString::number(obj->rotation),
                                               true, PreprocessingShaderType::ShadowsWithSizeOptimisation);

    {
        FrameData frames;
        frames.images = shadowImages;
        framesData << frames;
    }
    QSharedPointer<ImagesData> images =
        RESOLVE(ResourceManager)->getImagesData(ffsource, obj->stack.leaderId() + "STOP" + QString::number(obj->rotation));

    {
        FrameData frames;
        frames.images = images;
        framesData << frames;
    }


    return framesData;
}
//ship elves - IsoUnit G000RR0005BBTMV[rotition:1]
//ship elves - G000RR0005BTMV[rotition:1]
//ship elves shedow - G000RR0005BOA[rotition:1]
//ship elves - G000RR0005BOAT[rotition:1]
//ship elves shedow - G000RR0005SBOA[rotition:1]
//ship elves shedow - G000RR0005SBTM[rotition:1]
IObjectEditor *StackObjectAccessor::createEditor() const
{
    return new MapStackEditor();
}

QString ScenVariableObjectAccessor::getName(QSharedPointer<MapObject> object) const
{
    ScenVariableObject* obj = static_cast<ScenVariableObject*>(object.data());
    return obj->name;
}

QString ScenVariableObjectAccessor::getDesc(QSharedPointer<MapObject> object) const
{
    ScenVariableObject* obj = static_cast<ScenVariableObject*>(object.data());
    return QString("%1 (Value: %2)").arg(obj->desc).arg(obj->value);
}

QSharedPointer<MapObject> ScenVariableObjectAccessor::cloneObject(QSharedPointer<MapObject> object) const
{
    // Implement cloning logic here
    return nullptr; // Placeholder
}

QMenu *ScenVariableObjectAccessor::requestMenu(QSharedPointer<MapObject> object) const
{
    return nullptr;
}

QList<FrameData> ScenVariableObjectAccessor::frameData(QSharedPointer<MapObject> object) const
{
    return QList<FrameData>();
}

IObjectEditor *ScenVariableObjectAccessor::createEditor() const
{
    // Implement editor creation logic here
    return nullptr; // Placeholder
}

int RuinObjectAccessor::getW(QSharedPointer<MapObject> object) const
{
    return 3;
}

int RuinObjectAccessor::getH(QSharedPointer<MapObject> object) const
{
    return 3;
}

QString RuinObjectAccessor::getName(QSharedPointer<MapObject> object) const
{
    RuinObject * obj = static_cast<RuinObject *>(object.data());
    return obj->name;
}

QString RuinObjectAccessor::getDesc(QSharedPointer<MapObject> object) const
{
    RuinObject * obj = static_cast<RuinObject *>(object.data());
    QString result = obj->desc;
    if(result.count() > 0)
        result += "<br>";

    result += groupDataDesc(obj->guards);
    return result;
}

QSharedPointer<MapObject> RuinObjectAccessor::cloneObject(QSharedPointer<MapObject> object) const
{
    RuinObject * obj = static_cast<RuinObject*>(object.data());
    QSharedPointer<IMapObjectAccessor> accessor =
        RESOLVE(AccessorHolder)->objectAccessor(MapObject::Unit);
    auto newObj = new RuinObject(*obj);
    auto & stack = newObj->guards;
    for (int i = 0; i < stack.units.count(); ++i)
    {
        auto unit = RESOLVE(MapStateHolder)->objectById(stack.units[i].first);
        auto newUnit = accessor->cloneObject(unit);
        auto uid = RESOLVE(MapStateHolder)->addObject(newUnit);
        stack.units[i].first = uid;
    }
    return QSharedPointer<MapObject>(newObj);
}

QMenu *RuinObjectAccessor::requestMenu(QSharedPointer<MapObject> object) const
{
    QMenu *menu = new QMenu;
    QAction *removeAction = menu->addAction("Remove");
    removeAction->setData(QVariant::fromValue(RemoveObjectCommand(object->uid)));
    return menu;
}

QList<FrameData> RuinObjectAccessor::frameData(QSharedPointer<MapObject> object) const
{
    RuinObject * obj = static_cast<RuinObject *>(object.data());
    QStringList ffsource = QStringList()<<"IsoCmon"<<"IsoAnim";
    QString key = "G000RU0000" + QString::number(obj->image).rightJustified(3,'0');//+100 to destructed
    QSharedPointer<ImagesData> images =
        RESOLVE(ResourceManager)->getImagesData(ffsource, key);

    QList<FrameData> framesData;

    FrameData frames;
    frames.images = images;
    framesData << frames;
    return framesData;
}

IObjectEditor *RuinObjectAccessor::createEditor() const
{
    return new RuinEditor();
}

QMenu *RodObjectAccessor::requestMenu(QSharedPointer<MapObject> object) const
{
    return nullptr;
}

QList<FrameData> RodObjectAccessor::frameData(QSharedPointer<MapObject> object) const
{
    RodObject * obj = static_cast<RodObject *>(object.data());
    PlayerObject player = obj->owner.get();
    QSharedPointer<Grace> race = RESOLVE(DBFModel)->get<Grace>(player.raceId);
    if (race.isNull())
    {
        qDebug()<<Q_FUNC_INFO;
        qDebug()<<Q_FUNC_INFO;
        QList<FrameData> framesData;
        return framesData;
    }
    int raceId = RodObject::rodByRaceID(race->race_type.key);
    QString key = QString("G000RR%1RROD8").arg(QString::number(raceId).rightJustified(4,'0'));
    QSharedPointer<ImagesData> images =
        RESOLVE(ResourceManager)->getImagesData("IsoCmon", key, true, PreprocessingShaderType::TransparentBlackWithSizeOptimisation);

    QList<FrameData> framesData;

    FrameData frames;
    frames.images = images;
    framesData << frames;
    return framesData;
}

QSharedPointer<MapObject> RodObjectAccessor::cloneObject(QSharedPointer<MapObject> object) const
{
    RodObject * obj = static_cast<RodObject*>(object.data());
    return QSharedPointer<MapObject>(new RodObject(*obj));
}

QString QuestLineObjectAccessor::getName(QSharedPointer<MapObject> object) const
{
    QuestLine * obj = static_cast<QuestLine *>(object.data());
    return obj->name;
}

QString QuestLineObjectAccessor::getDesc(QSharedPointer<MapObject> object) const
{
    QuestLine * obj = static_cast<QuestLine *>(object.data());
    return obj->name;
}

QSharedPointer<MapObject> QuestLineObjectAccessor::cloneObject(QSharedPointer<MapObject> object) const
{
    QuestLine * obj = static_cast<QuestLine*>(object.data());
    return QSharedPointer<MapObject>(new QuestLine(*obj));
}

IObjectEditor *QuestLineObjectAccessor::createEditor() const
{
    return new QuestLineEditor();
}

int MountainObjectAccessor::getW(QSharedPointer<MapObject> object) const
{
    MountainObject * obj = static_cast<MountainObject *>(object.data());
    return obj->w;
}

int MountainObjectAccessor::getH(QSharedPointer<MapObject> object) const
{
    MountainObject * obj = static_cast<MountainObject *>(object.data());
    return obj->h;
}

QString MountainObjectAccessor::getName(QSharedPointer<MapObject> object) const
{
    MountainObject * obj = static_cast<MountainObject *>(object.data());
    return QString("%1:%2").arg(obj->image).arg(obj->race);
}

QString MountainObjectAccessor::getDesc(QSharedPointer<MapObject> object) const
{
    return "";
}

QSharedPointer<MapObject> MountainObjectAccessor::cloneObject(QSharedPointer<MapObject> object) const
{
    MountainObject * obj = static_cast<MountainObject*>(object.data());
    return QSharedPointer<MapObject>(new MountainObject(*obj));
}

QMenu *MountainObjectAccessor::requestMenu(QSharedPointer<MapObject> object) const
{
    return nullptr;
}

QList<FrameData> MountainObjectAccessor::frameData(QSharedPointer<MapObject> object) const
{
    MountainObject * obj = static_cast<MountainObject *>(object.data());
    QStringList ffsource = QStringList()<<"IsoTerrn";
    auto race = RESOLVE(DBFModel)->get<Lrace>(QString::number(obj->race));
    QString key = "MOMNE" + QString::number(obj->w).rightJustified(2,'0') +
                  QString::number(obj->image).rightJustified(2,'0');
    QSharedPointer<ImagesData> images =
        RESOLVE(ResourceManager)->getImagesData(ffsource, key);

    QList<FrameData> framesData;

    FrameData frames;
    frames.images = images;
    framesData << frames;
    return framesData;
}

int MerchantObjectAccessor::getW(QSharedPointer<MapObject> object) const
{
    return 3;
}

int MerchantObjectAccessor::getH(QSharedPointer<MapObject> object) const
{
    return 3;
}

QString MerchantObjectAccessor::getName(QSharedPointer<MapObject> object) const
{
    MerchantObject * obj = static_cast<MerchantObject *>(object.data());
    return obj->name;
}

QString MerchantObjectAccessor::getDesc(QSharedPointer<MapObject> object) const
{
    MerchantObject * obj = static_cast<MerchantObject *>(object.data());
    return obj->desc;
}

QSharedPointer<MapObject> MerchantObjectAccessor::cloneObject(QSharedPointer<MapObject> object) const
{
    MerchantObject * obj = static_cast<MerchantObject *>(object.data());
    return QSharedPointer<MapObject>(new MerchantObject(*obj));
}

QList<FrameData> MerchantObjectAccessor::frameData(QSharedPointer<MapObject> object) const
{
    MerchantObject * obj = static_cast<MerchantObject *>(object.data());
    QStringList ffsource = QStringList()<<"IsoCmon"<<"IsoAnim";
    QString key;
    switch (obj->type) {
    case MerchantObject::Items:
        key = "G000SI0000MERH";
        break;
    case MerchantObject::Spells:
        key = "G000SI0000MAGE";
        break;
    case MerchantObject::Units:
        key = "G000SI0000MERC";
        break;
    case MerchantObject::Trainer:
        key = "G000SI0000TRAI";
        break;
    }
    key += QString::number(obj->image).rightJustified(2,'0');

    QSharedPointer<ImagesData> images =
        RESOLVE(ResourceManager)->getImagesData(ffsource, key);

    QList<FrameData> framesData;

    FrameData frames;
    frames.images = images;
    framesData << frames;
    return framesData;
}

IObjectEditor *MerchantObjectAccessor::createEditor() const
{
    return new MerchantEditor();
}

int LocationObjectAccessor::getW(QSharedPointer<MapObject> object) const
{
    LocationObject * obj = static_cast<LocationObject *>(object.data());
    return obj->r;
}

int LocationObjectAccessor::getH(QSharedPointer<MapObject> object) const
{
    LocationObject * obj = static_cast<LocationObject *>(object.data());
    return obj->r;
}

QString LocationObjectAccessor::getName(QSharedPointer<MapObject> object) const
{
    LocationObject * obj = static_cast<LocationObject *>(object.data());
    return obj->name;
}

QString LocationObjectAccessor::getDesc(QSharedPointer<MapObject> object) const
{
    LocationObject * obj = static_cast<LocationObject *>(object.data());
    return QString("%1x%1").arg(obj->r);
}

QSharedPointer<MapObject> LocationObjectAccessor::cloneObject(QSharedPointer<MapObject> object) const
{
    LocationObject * obj = static_cast<LocationObject *>(object.data());
    return QSharedPointer<MapObject>(new LocationObject(*obj));
}

QMenu *LocationObjectAccessor::requestMenu(QSharedPointer<MapObject> object) const
{
    return nullptr;
}

QList<FrameData> LocationObjectAccessor::frameData(QSharedPointer<MapObject> object) const
{
    LocationObject * obj = static_cast<LocationObject *>(object.data());
    QSharedPointer<ImagesData> images =
        RESOLVE(ResourceManager)->getImagesData("loc_" + QString::number(obj->r));

    QList<FrameData> framesData;

    FrameData frames;
    frames.images = images;
    framesData << frames;
    return framesData;
}

qreal LocationObjectAccessor::getZ(QSharedPointer<MapObject> object) const
{
    LocationObject * obj = static_cast<LocationObject *>(object.data());
    return 1300 + 10 / float(obj->r + 1);
}

int LandmarkObjectAccessor::getW(QSharedPointer<MapObject> object) const
{
    LandmarkObject * lmarkObj = static_cast<LandmarkObject *>(object.data());
    QSharedPointer<GLmark> mark = RESOLVE(DBFModel)->get<GLmark>(lmarkObj->lmarkId);
    return mark->cx;
}

int LandmarkObjectAccessor::getH(QSharedPointer<MapObject> object) const
{
    LandmarkObject * lmarkObj = static_cast<LandmarkObject *>(object.data());
    QSharedPointer<GLmark> mark = RESOLVE(DBFModel)->get<GLmark>(lmarkObj->lmarkId);
    return mark->cy;
}

QString LandmarkObjectAccessor::getName(QSharedPointer<MapObject> object) const
{
    LandmarkObject * lmarkObj = static_cast<LandmarkObject *>(object.data());
    QSharedPointer<GLmark> mark = RESOLVE(DBFModel)->get<GLmark>(lmarkObj->lmarkId);
    return mark->name_txt.value->text;
}

QString LandmarkObjectAccessor::getDesc(QSharedPointer<MapObject> object) const
{
    auto accessor = RESOLVE(AccessorHolder)->objectAccessor(object->uid.first);
    LandmarkObject * lmarkObj = static_cast<LandmarkObject *>(object.data());
    if (!lmarkObj->desc.isEmpty())
        return lmarkObj->desc;
    QSharedPointer<GLmark> mark = RESOLVE(DBFModel)->get<GLmark>(lmarkObj->lmarkId);
    return mark->desc_txt.value->text;
}

QSharedPointer<MapObject> LandmarkObjectAccessor::cloneObject(QSharedPointer<MapObject> object) const
{
    LandmarkObject * obj = static_cast<LandmarkObject*>(object.data());
    return QSharedPointer<MapObject>(new LandmarkObject(*obj));
}

QMenu *LandmarkObjectAccessor::requestMenu(QSharedPointer<MapObject> object) const
{
    return nullptr;
}

QList<FrameData> LandmarkObjectAccessor::frameData(QSharedPointer<MapObject> object) const
{
    LandmarkObject * lmarkObj = static_cast<LandmarkObject *>(object.data());
    QStringList ffsource = QStringList()<<"IsoCmon"<<"IsoAnim";
    QSharedPointer<ImagesData> images =
        RESOLVE(ResourceManager)->getImagesData(ffsource, lmarkObj->lmarkId.toUpper());

    QList<FrameData> framesData;

    FrameData frames;
    frames.images = images;
    framesData << frames;
    return framesData;
}

int FortObjectAccessor::getW(QSharedPointer<MapObject> object) const
{
    FortObject * obj = static_cast<FortObject*>(object.data());
    if (obj->fortType == FortObject::Capital)
        return 5;
    return 4;
}

int FortObjectAccessor::getH(QSharedPointer<MapObject> object) const
{
    FortObject * obj = static_cast<FortObject*>(object.data());
    if (obj->fortType == FortObject::Capital)
        return 5;
    return 4;
}

QString FortObjectAccessor::getName(QSharedPointer<MapObject> object) const
{
    FortObject * obj = static_cast<FortObject *>(object.data());
    return obj->name;
}

QString FortObjectAccessor::getDesc(QSharedPointer<MapObject> object) const
{
    FortObject * obj = static_cast<FortObject *>(object.data());
    QString result = obj->desc;
    if(result.count() > 0)
        result += "<br>";
    result += "Visiter:";
    result += "<br>";
    if (obj->visiter.second != -1)
    {
        auto visiterObj = RESOLVE(MapStateHolder)->objectByIdT<StackObject>(obj->visiter);
        result += groupDataDesc(visiterObj.stack);
        result += "<br>";
        result += "Garrison:";
        result += "<br>";
        result += groupDataDesc(obj->garrison);
    }
    result += "<br>";
    result += TranslationHelper::tr("Owner:") + " ";
    result += obj->owner.get().getName();
    result += "<br>";
    result += TranslationHelper::tr("Fraction:") + " ";
    result += TranslationHelper::tr(obj->subrace.get().getName());
    return result;
}

QSharedPointer<MapObject> FortObjectAccessor::cloneObject(QSharedPointer<MapObject> object) const
{
    FortObject * obj = static_cast<FortObject*>(object.data());
    QSharedPointer<IMapObjectAccessor> accessor =
        RESOLVE(AccessorHolder)->objectAccessor(MapObject::Unit);
    auto newObj = new FortObject(*obj);
    auto & stack = newObj->garrison;
    for (int i = 0; i < stack.units.count(); ++i)
    {
        auto unit = RESOLVE(MapStateHolder)->objectById(stack.units[i].first);
        auto newUnit = accessor->cloneObject(unit);
        auto uid = RESOLVE(MapStateHolder)->addObject(newUnit);
        stack.units[i].first = uid;
    }
    if (newObj->visiter != EMPTY_ID)
    {
        QSharedPointer<IMapObjectAccessor> stackAccessor =
            RESOLVE(AccessorHolder)->objectAccessor(MapObject::Stack);
        auto visiter = RESOLVE(MapStateHolder)->objectById(newObj->visiter);
        auto newVisiter = stackAccessor->cloneObject(visiter);
        auto stackuid = RESOLVE(MapStateHolder)->addObject(newVisiter);
        newObj->visiter = stackuid;
    }
    return QSharedPointer<MapObject>(newObj);
}

QMenu *FortObjectAccessor::requestMenu(QSharedPointer<MapObject> object) const
{
    return nullptr;
}



QList<FrameData> FortObjectAccessor::frameData(QSharedPointer<MapObject> object) const
{
    FortObject * obj = static_cast<FortObject *>(object.data());
    QList<FrameData> framesData;
    PlayerObject player = obj->owner.get();
    QSharedPointer<Grace> race = RESOLVE(DBFModel)->get<Grace>(player.raceId);
    int raceId = shieldByRaceID(race->race_type.key);


    if (obj->fortType == FortObject::Capital)
    {
        auto race = RESOLVE(DBFModel)->get<Grace>(obj->raceId);
        raceId = shieldByRaceID(race->race_type.key);
        QStringList ffsource = QStringList()<<"IsoCmon"<<"IsoAnim";
        QString key = race->race_type.value->text;
        key = key.mid(2, 2);
        QSharedPointer<ImagesData> images =
            RESOLVE(ResourceManager)->getImagesData(ffsource, "G000FT0000" + key + "0");


        FrameData frames;
        frames.images = images;
        framesData << frames;
    }
    else if (obj->fortType == FortObject::Village){
        QStringList ffsource;
        if (obj->level < 4)
            ffsource = QStringList()<<"IsoAnim";
        else
            ffsource =  QStringList()<<"IsoCmon";
        ffsource <<"IsoAnim";
        ffsource <<"IsoCmon";

        QSharedPointer<ImagesData> images;
        QString shortRace = shortRaceByPlayerId2(RESOLVE(MapStateHolder)->map(), RESOLVE(DBFModel), obj->owner);
        images = RESOLVE(ResourceManager)->getImagesData(ffsource, "G000FT0000NE" + QString::number(obj->level) + shortRace);
        if (images->data.count() == 0)
            images = RESOLVE(ResourceManager)->getImagesData(ffsource, "G000FT0000NE" + QString::number(obj->level));
        FrameData frames;
        frames.images = images;
        framesData << frames;
    }


    if (obj->visiter.second != -1)
    {
        QString key = QString("G000RR%1SHLV8").arg(QString::number(raceId).rightJustified(4,'0'));
        QSharedPointer<ImagesData> images =
            RESOLVE(ResourceManager)->getImagesData("IsoCmon", key, true);
        FrameData frames;
        frames.images = images;
        framesData << frames;
    }
    return framesData;
}

IObjectEditor *FortObjectAccessor::createEditor() const
{
    return new FortEditor();
}

QMenu *CrystalObjectAccessor::requestMenu(QSharedPointer<MapObject> object) const
{
    QMenu * menu = new QMenu();
    CrystalObject * obj = static_cast<CrystalObject *>(object.data());
    for (int i = 0; i < 6; ++i)
    {
        if (i == obj->resource)
            continue;
        QString transformText = TranslationHelper::tr("Transform:") + " ";
        QAction * action = menu->addAction(transformText + CrystalObject::CrystalNameByResource((CrystalObject::ResourceType)i));
        action->setData(QVariant::fromValue(ChangeCrystalCommand(object->uid, i)));
    }
    QAction * removeAction = menu->addAction(TranslationHelper::tr("Remove"));
    removeAction->setData(QVariant::fromValue(RemoveObjectCommand(object->uid)));
    return menu;
}

QList<FrameData> CrystalObjectAccessor::frameData(QSharedPointer<MapObject> object) const
{
    CrystalObject * obj = static_cast<CrystalObject *>(object.data());
    QStringList ffsource = QStringList()<<"IsoCmon"<<"IsoAnim";
    QString key = CrystalObject::CrystalIdByResource((CrystalObject::ResourceType)obj->resource);
    QSharedPointer<ImagesData> images =
        RESOLVE(ResourceManager)->getImagesData(ffsource, key, true, PreprocessingShaderType::TransparentBlackWithSizeOptimisation);

    QList<FrameData> framesData;

    FrameData frames;
    frames.images = images;
    framesData << frames;
    return framesData;
}

QString CrystalObjectAccessor::getName(QSharedPointer<MapObject> object) const
{
    CrystalObject * obj = static_cast<CrystalObject *>(object.data());
    return CrystalObject::CrystalNameByResource(obj->resource);
}

QString CrystalObjectAccessor::getDesc(QSharedPointer<MapObject> object) const
{
    CrystalObject * obj = static_cast<CrystalObject *>(object.data());
    return CrystalObject::CrystalDescByResource(obj->resource);
}

QSharedPointer<MapObject> CrystalObjectAccessor::cloneObject(QSharedPointer<MapObject> object) const
{
    CrystalObject * obj = static_cast<CrystalObject *>(object.data());
    return QSharedPointer<MapObject>(new CrystalObject(*obj));
}

QString PlayerObjectAccessor::getName(QSharedPointer<MapObject> object) const
{
    PlayerObject* obj = static_cast<PlayerObject*>(object.data());
    QSharedPointer<Grace> race = RESOLVE(DBFModel)->get<Grace>(obj->raceId);
    if (race.isNull())
        return obj->getName();
    return obj->getName() + QString("(%1)").arg(race->name_txt->text);
}

QString PlayerObjectAccessor::getDesc(QSharedPointer<MapObject> object) const
{
    PlayerObject* obj = static_cast<PlayerObject*>(object.data());
    return obj->desc;
}

QSharedPointer<MapObject> PlayerObjectAccessor::cloneObject(QSharedPointer<MapObject> object) const
{
    // Implement cloning logic here
    return nullptr; // Placeholder
}

QMenu *PlayerObjectAccessor::requestMenu(QSharedPointer<MapObject> object) const
{
    return nullptr;
}

QList<FrameData> PlayerObjectAccessor::frameData(QSharedPointer<MapObject> object) const
{
    return QList<FrameData>();
}

IObjectEditor *PlayerObjectAccessor::createEditor() const
{
    // Implement editor creation logic here
    return nullptr; // Placeholder
}

QString SubRaceObjectAccessor::getName(QSharedPointer<MapObject> object) const
{
    SubRaceObject* obj = static_cast<SubRaceObject*>(object.data());
    return obj->getName();
}

QString SubRaceObjectAccessor::getDesc(QSharedPointer<MapObject> object) const
{
    SubRaceObject* obj = static_cast<SubRaceObject*>(object.data());
    return obj->name; // Using name as description since there's no specific desc field
}

QSharedPointer<MapObject> SubRaceObjectAccessor::cloneObject(QSharedPointer<MapObject> object) const
{
    // Implement cloning logic here
    return nullptr; // Placeholder
}

QMenu *SubRaceObjectAccessor::requestMenu(QSharedPointer<MapObject> object) const
{
    return nullptr;
}

QList<FrameData> SubRaceObjectAccessor::frameData(QSharedPointer<MapObject> object) const
{
    return QList<FrameData>();
}

IObjectEditor *SubRaceObjectAccessor::createEditor() const
{
    // Implement editor creation logic here
    return nullptr; // Placeholder
}

QString RelationsAccessor::getName(QSharedPointer<MapObject> object) const
{
    return "Relations"; // Generic name since Relations doesn't have a specific name field
}

QString RelationsAccessor::getDesc(QSharedPointer<MapObject> object) const
{
    Relations* obj = static_cast<Relations*>(object.data());
    return QString("Relations (%1 entries)").arg(obj->entries.size());
}

QSharedPointer<MapObject> RelationsAccessor::cloneObject(QSharedPointer<MapObject> object) const
{
    // Implement cloning logic here
    return nullptr; // Placeholder
}

QMenu *RelationsAccessor::requestMenu(QSharedPointer<MapObject> object) const
{
    return nullptr;
}

QList<FrameData> RelationsAccessor::frameData(QSharedPointer<MapObject> object) const
{
    return QList<FrameData>();
}

IObjectEditor *RelationsAccessor::createEditor() const
{
    // Implement editor creation logic here
    return nullptr; // Placeholder
}

QString MapInfoAccessor::getName(QSharedPointer<MapObject> object) const
{
    return "Map Info"; // Generic name since MapInfo doesn't have a specific name field
}

QString MapInfoAccessor::getDesc(QSharedPointer<MapObject> object) const
{
    MapInfo* obj = static_cast<MapInfo*>(object.data());
    return QString("Scenario: %1").arg(obj->info.name); // Assuming D2ScenarioInfo has a name field
}

QSharedPointer<MapObject> MapInfoAccessor::cloneObject(QSharedPointer<MapObject> object) const
{
    // Implement cloning logic here
    return nullptr; // Placeholder
}

QMenu *MapInfoAccessor::requestMenu(QSharedPointer<MapObject> object) const
{
    return nullptr;
}

QList<FrameData> MapInfoAccessor::frameData(QSharedPointer<MapObject> object) const
{
    return QList<FrameData>();
}

IObjectEditor *MapInfoAccessor::createEditor() const
{
    // Implement editor creation logic here
    return new MapInfoEditor(); // Placeholder
}

QString EventObjectAccessor::getName(QSharedPointer<MapObject> object) const
{
    EventObject* obj = static_cast<EventObject*>(object.data());
    return obj->name;
}

QString EventObjectAccessor::getDesc(QSharedPointer<MapObject> object) const
{
    EventObject* obj = static_cast<EventObject*>(object.data());
    QString desc = QString("Chance: %1%, Order: %2").arg(obj->chance).arg(obj->order);
    if (obj->enabled) {
        desc += ", Enabled";
    }
    if (obj->occurOnce) {
        desc += ", Occurs Once";
    }
    return desc;
}

QSharedPointer<MapObject> EventObjectAccessor::cloneObject(QSharedPointer<MapObject> object) const
{
    // Implement cloning logic here
    return nullptr; // Placeholder
}

QMenu *EventObjectAccessor::requestMenu(QSharedPointer<MapObject> object) const
{
    return nullptr;
}

QList<FrameData> EventObjectAccessor::frameData(QSharedPointer<MapObject> object) const
{
    return QList<FrameData>();
}

IObjectEditor *EventObjectAccessor::createEditor() const
{
    // Implement editor creation logic here
    return nullptr; // Placeholder
}

IObjectEditor *MapStatisticsAccessor::createEditor() const
{
    return new MapStatisticView(); // Placeholder
}

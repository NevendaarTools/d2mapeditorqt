#ifndef TOMBOBJECT_H
#define TOMBOBJECT_H
#include "MapObject.h"
#include "toolsqt/DBFModel/GameClases.h"
#include "toolsqt/MapUtils/DataBlocks/D2Tomb.h"
#include "Diplomacy.h"

class TombObject : public MapObject
{
public:
    TombObject()
    {
        MapObject::uid.first = MapObject::Tomb;
    }
    TombObject(const D2Tomb & tomb_) : tomb(tomb_)
    {
        uid.first = MapObject::Tomb;
        uid.second = tomb.uid.second;
        x = tomb.posX;
        y = tomb.posY;
    }
    D2Tomb tomb;
};
template <> int getTypeId<TombObject>() {return MapObject::Tomb;}
Q_DECLARE_METATYPE(TombObject)


#endif // TOMBOBJECT_H

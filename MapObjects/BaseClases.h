#pragma once
#include <QString>
#include <QList>

#include "MapObjects/UnitObject.h"
#define LEADER_INC "G000UM9031"
#define LEADER_DEC "G000UM9032"

struct Inventory
{
    struct Item
    {
        QString type;
        int count = 1;
        int charges = 0;
    };

    QList<Item> items;

    void addItem(const QString& id, int count, int charges = 0)
    {
        for(int i = 0; i < items.count(); ++i)
        {
            if (items[i].type == id && items[i].charges == charges)
            {
                items[i].count += count;
                return;
            }
        }
        items.append(Item{id, count, charges});
    }

    void removeItem(const QString& id, int count, int charges = 0)
    {
        for(int i = 0; i < items.count(); ++i)
        {
            if (items[i].type == id && items[i].charges == charges)
            {
                items[i].count -= count;
                if (items[i].count <= 0)
                    items.removeAt(i);
                return;
            }
        }
    }

    void addItem(const QString& id)
    {
        addItem(id, 1);
    }
    void removeItem(const QString& id)
    {
        removeItem(id, 1);
    }

    void removeAll(const QString& id)
    {
        for(int i = 0; i < items.count(); ++i)
        {
            if (items[i].type == id)
            {
                items.removeAt(i);
                return;
            }
        }
    }

    QStringList toList() const
    {
        QStringList result;
        for(int i = 0; i < items.count(); ++i)
        {
            for (int k = 0; k < items[i].count; ++k)
            {
                result << items[i].type;
            }
        }
        return result;
    }
};

struct GroupData
{
    QList<QPair<QPair<int,int>, QPoint>> units;
    QString name;
    Inventory inventory;
    QString toString() const;

    GroupData();

    void addUnit(UnitObject unit, const QPoint& pos);
    void addUnit(QPair<int,int> uid, const QPoint& pos);

    void moveUnit(const QPoint& from, const QPoint& newPos);
    int unitIndexByPos(const QPoint & pos) const;
    bool hasUnit(const QPoint &pos) const;
    void remove(int x, int y);
    UnitObject unitByPos(const QPoint &pos) const;

    bool isLeader(int x, int y);
    QString leaderId() const;
    int leaderIndex() const;

    int leadership() const;
    int requiredLeadership() const;
};

int groupExpForKill(const GroupData& group);
QString groupDataDesc(const GroupData& group);

int calcKilledExp(const QString& unit, int level);

int calcConsumedExp(const QString& unit, int level, bool withUpd = false);

@echo off

set current_dir=%CD%
set parent_dir=%CD%\..
set toolsqt_dir=%parent_dir%\toolsqt

if not exist %toolsqt_dir% (
    cd %parent_dir%
    git clone https://NevendaarTools@bitbucket.org/NevendaarTools/toolsqt.git
	cd %current_dir%
)

mklink /D %CD%\toolsqt %toolsqt_dir%

md build\build_32\Release
md build\build_32\Debug
md build\build_64\Release
md build\build_64\Debug

mklink /D %current_dir%\build\build_32\Release\Translations %current_dir%\Translations
mklink /D %current_dir%\build\build_32\Release\Images %current_dir%\Images
mklink /D %current_dir%\build\build_32\Release\QML %current_dir%\QML
mklink /D %current_dir%\build\build_32\Release\Presets %current_dir%\Presets
mklink /D %current_dir%\build\build_32\Release\Generators %current_dir%\Generators
mklink /D %current_dir%\build\build_32\Release\ExtraDeps %current_dir%\ExtraDeps

mklink /D %current_dir%\build\build_32\Debug\Translations %current_dir%\Translations
mklink /D %current_dir%\build\build_32\Debug\Images %current_dir%\Images
mklink /D %current_dir%\build\build_32\Debug\QML %current_dir%\QML
mklink /D %current_dir%\build\build_32\Debug\Presets %current_dir%\Presets
mklink /D %current_dir%\build\build_32\Debug\Generators %current_dir%\Generators
mklink /D %current_dir%\build\build_32\Debug\ExtraDeps %current_dir%\ExtraDeps

mklink /D %current_dir%\build\build_64\Release\Translations %current_dir%\Translations
mklink /D %current_dir%\build\build_64\Release\Images %current_dir%\Images
mklink /D %current_dir%\build\build_64\Release\QML %current_dir%\QML
mklink /D %current_dir%\build\build_64\Release\Presets %current_dir%\Presets
mklink /D %current_dir%\build\build_64\Release\Generators %current_dir%\Generators
mklink /D %current_dir%\build\build_64\Release\ExtraDeps %current_dir%\ExtraDeps

mklink /D %current_dir%\build\build_64\Debug\Translations %current_dir%\Translations
mklink /D %current_dir%\build\build_64\Debug\Images %current_dir%\Images
mklink /D %current_dir%\build\build_64\Debug\QML %current_dir%\QML
mklink /D %current_dir%\build\build_64\Debug\Presets %current_dir%\Presets
mklink /D %current_dir%\build\build_64\Debug\Generators %current_dir%\Generators
mklink /D %current_dir%\build\build_64\Debug\ExtraDeps %current_dir%\ExtraDeps

echo Done.

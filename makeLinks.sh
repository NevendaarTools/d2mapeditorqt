#!/bin/bash

# Define base paths
SOURCE_DIR=~/d2/d2mapeditorqt
DEBUG_DIR=~/d2/build-D2MapEditor-Desktop_Qt_6_7_2-Debug/D2MapEditor.app/Contents/MacOS/
RELEASE_DIR=~/d2/build-D2MapEditor-Desktop_Qt_6_7_2-Release/D2MapEditor.app/Contents/MacOS/

# Create symlinks for Debug
ln -s $SOURCE_DIR/Translations $DEBUG_DIR
ln -s $SOURCE_DIR/Images $DEBUG_DIR
ln -s $SOURCE_DIR/QML $DEBUG_DIR
ln -s $SOURCE_DIR/Presets $DEBUG_DIR
ln -s $SOURCE_DIR/Generators $DEBUG_DIR
ln -s $SOURCE_DIR/ExtraDeps $DEBUG_DIR

# Create symlinks for Release
ln -s $SOURCE_DIR/Translations $RELEASE_DIR
ln -s $SOURCE_DIR/Images $RELEASE_DIR
ln -s $SOURCE_DIR/QML $RELEASE_DIR
ln -s $SOURCE_DIR/Presets $RELEASE_DIR
ln -s $SOURCE_DIR/Generators $RELEASE_DIR
ln -s $SOURCE_DIR/ExtraDeps $RELEASE_DIR

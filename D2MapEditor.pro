
HEADERS += mainwindow.h \
    BaseEditorTool.h \
    Commands/Command.h \
    Commands/CommandBus.h \
    Commands/EditCommands.h \
    Commands/MenuCommands.h \
    Commands/OverlayCommands.h \
    Commands/UIEvents.h \
    Common/AutoCompleteModel.h \
    Common/DataStorageVIew/DataFragmentDisplayWidget.h \
    Common/DataStorageVIew/DataSearchWidget.h \
    Common/DataStorageVIew/DataSelectWidget.h \
    Common/DataStorageVIew/DataStorage.h \
    Common/DataStorageVIew/DataStorageWidget.h \
    Common/DataStorageVIew/DataViewCommands.h \
    Common/DataStorageVIew/FileViewerWidget.h \
    Common/DataStorageVIew/MappedDataFragment.h \
    Common/HunspellChecker.h \
    Common/HunspellDLL.h \
    Common/Profiling.h \
    Common/SpellCheckHighlighter.h \
    Dialogs/SelectMapInSagaDialog.h \
    Dialogs/settingseditordialog.h \
    Engine/BaseEditorTool.h \
    Engine/Components/AccessorHolder.h \
    Engine/Components/DisplaySettingsManager.h \
    Engine/Components/ResourceManager.h \
    Engine/Components/TranslationHelper.h \
    Engine/DIContainer.h \
    Engine/GameInstance.h \
    Engine/Map/Map.h \
    Engine/Map/MapStateHolder.h \
    Engine/MapConverter.h \
    Engine/MapView/CustomMapObject.h \
    Engine/MapView/HoverHighlight.h \
    Engine/MapView/LandscapeObject.h \
    Engine/MapView/MapGraphicsObject.h \
    Engine/MapView/MapTileHelper.h \
    Engine/MapView/MapView.h \
    Engine/MapView/MinimapHelper.h \
    Engine/Option.h \
    Engine/PresetManager.h \
    Engine/SettingsManager.h \
    Engine/version.h \
    Events/EventBus.h \
    Events/MapEvents.h \
    Generation/GeneratorHandle.h \
    Generation/GeneratorWrapper.h \
    Generation/Generators.h \
    Generation/VilgeforcLootGenerator.h \
    Generation/VilgeforcStackGenerator.h \
    Generation/WaveFunctionCollapse.h \
    MapObjects/BaseClases.h \
    MapObjects/CrystalObject.h \
    MapObjects/Diplomacy.h \
    MapObjects/EventObject.h \
    MapObjects/FortObject.h \
    MapObjects/GameMap.h \
    MapObjects/GridObject.h \
    MapObjects/LocationObject.h \
    MapObjects/MapLink.h \
    MapObjects/MerchantObject.h \
    MapObjects/MountainObject.h \
    MapObjects/ObjectAccessors.h \
    MapObjects/QuestLine.h \
    MapObjects/RodObject.h \
    MapObjects/RuinObject.h \
    MapObjects/ScenVariableObject.h \
    MapObjects/StackTemplateObject.h \
    MapObjects/TombObject.h \
    MapObjects/TreasureObject.h \
    MapObjects/UnitObject.h \
    Common/MapJsonIO.h \
    Common/MathHelper.h \
    Common/StringHelper.h \
    Common/TemplateFunctions.h \
    OverlayWidget.h \
    QMLHelpers/Common/DynamicImageItem.h \
    QMLHelpers/Common/IStringDataProvider.h \
    QMLHelpers/Common/ITagProvider.h \
    QMLHelpers/Common/ImageProvider.h \
    QMLHelpers/Common/OverlayWrapper.h \
    QMLHelpers/Common/PresetController.h \
    QMLHelpers/Common/StyleManager.h \
    QMLHelpers/Components/BaseObjectPreview.h \
    QMLHelpers/Components/CostEditor.h \
    QMLHelpers/Components/EditorComponents.h \
    QMLHelpers/Components/EventConditionEditor.h \
    QMLHelpers/Components/EventConvertionHelper.h \
    QMLHelpers/Components/EventEditor.h \
    QMLHelpers/Components/EventEffectEditor.h \
    QMLHelpers/Components/GroupUnitsEditor.h \
    QMLHelpers/Components/ItemView.h \
    QMLHelpers/Components/QuestStageEditor.h \
    QMLHelpers/Components/SceneVariablesEditor.h \
    QMLHelpers/Components/SpellView.h \
    QMLHelpers/Components/StackOrderEditor.h \
    QMLHelpers/Components/UnitEditor.h \
    QMLHelpers/Components/StackTemplateEditor.h \
    QMLHelpers/Editors/EditorWidget.h \
    QMLHelpers/Editors.h \
    QMLHelpers/Editors/MapInfoEditor.h \
    QMLHelpers/Editors/MapStackEditor.h \
    QMLHelpers/Editors/MapStatisticView.h \
    QMLHelpers/Editors/MerchantEditor.h \
    QMLHelpers/Editors/QuestLineEditor.h \
    QMLHelpers/Editors/RuinEditor.h \
    QMLHelpers/Editors/TreasureEditor.h \
    QMLHelpers/Editors/VillageEditor.h \
    QMLHelpers/MainMenuController.h \
    QMLHelpers/MainToolBarController.h \
    QMLHelpers/MapCreationController.h \
    QMLHelpers/Models/EventsListModel.h \
    QMLHelpers/Models/FilterSettingsModel.h \
    QMLHelpers/Models/InventoryModel.h \
    QMLHelpers/Models/ItemSelectModel.h \
    QMLHelpers/Models/LogModel.h \
    QMLHelpers/Models/Models.h \
    QMLHelpers/Models/MountainSelectModel.h \
    QMLHelpers/Models/OptionsModel.h \
    QMLHelpers/Models/OwnerSelectModel.h \
    QMLHelpers/Models/SelectGameObjectModel.h \
    QMLHelpers/Models/SelectMapObjectModel.h \
    QMLHelpers/Models/SelectObjectModel.h \
    QMLHelpers/Models/SpellsListModel.h \
    QMLHelpers/Models/SpellsSelectModel.h \
    QMLHelpers/Models/StringModel.h \
    QMLHelpers/Models/TagSelectModel.h \
    QMLHelpers/Models/TreeSelectModel.h \
    QMLHelpers/Models/UnitHireModel.h \
    QMLHelpers/Models/UnitSelectModel.h \
    QMLHelpers/Tools/EditorTool.h \
    QMLHelpers/Tools/LayersTool.h \
    QMLHelpers/Tools/MapEditTool.h \
    QMLHelpers/Tools/MapGenerationTool.h \
    QMLHelpers/Tools/ObjectCloneTool.h \
    QMLHelpers/Tools/ObjectMoveTool.h \
    QMLHelpers/Tools/ObjectRemoveTool.h \
    QMLHelpers/Tools/PlaceCapitalTool.h \
    QMLHelpers/Tools/PlaceCrystalTool.h \
    QMLHelpers/Tools/PlaceLMarkTool.h \
    QMLHelpers/Tools/PlaceMerchantTool.h \
    QMLHelpers/Tools/PlaceObjectTool.h \
    QMLHelpers/Tools/PlaceRuinTool.h \
    QMLHelpers/Tools/PlaceStackTool.h \
    QMLHelpers/Tools/PlaceTombTool.h \
    QMLHelpers/Tools/PlaceTreasureTool.h \
    QMLHelpers/Tools/PlaceVillageTool.h \
    QMLHelpers/Tools/SearchObjectTool.h \
    QMLHelpers/Tools/Tools.h \
    QMLHelpers/Tools/ToolsProvider.h \
    Dialogs/ValidationResultYesNoDialog.h \
    toolsqt/Common/ByteEncoder.h \
    toolsqt/Common/INotifier.h \
    toolsqt/Common/Logger.h \
    toolsqt/MapUtils/D2MapEditor.h \
    toolsqt/MapUtils/D2MapModel.h \
    toolsqt/MapUtils/D2SagaModel.h \
    toolsqt/MapUtils/DataBlocks/D2Bag.h \
    toolsqt/MapUtils/DataBlocks/D2Capital.h \
    toolsqt/MapUtils/DataBlocks/D2Crystal.h \
    toolsqt/MapUtils/DataBlocks/D2Diplomacy.h \
    toolsqt/MapUtils/DataBlocks/D2Event.h \
    toolsqt/MapUtils/DataBlocks/D2Item.h \
    toolsqt/MapUtils/DataBlocks/D2Location.h \
    toolsqt/MapUtils/DataBlocks/D2Mage.h \
    toolsqt/MapUtils/DataBlocks/D2Map.h \
    toolsqt/MapUtils/DataBlocks/D2MapFog.h \
    toolsqt/MapUtils/DataBlocks/D2Merchant.h \
    toolsqt/MapUtils/DataBlocks/D2Mercs.h \
    toolsqt/MapUtils/DataBlocks/D2Mountains.h \
    toolsqt/MapUtils/DataBlocks/D2Plan.h \
    toolsqt/MapUtils/DataBlocks/D2Player.h \
    toolsqt/MapUtils/DataBlocks/D2PlayerBuildings.h \
    toolsqt/MapUtils/DataBlocks/D2PlayerSpells.h \
    toolsqt/MapUtils/DataBlocks/D2QuestLog.h \
    toolsqt/MapUtils/DataBlocks/D2Road.h \
    toolsqt/MapUtils/DataBlocks/D2Rod.h \
    toolsqt/MapUtils/DataBlocks/D2Ruin.h \
    toolsqt/MapUtils/DataBlocks/D2ScenarioInfo.h \
    toolsqt/MapUtils/DataBlocks/D2SceneVariables.h \
    toolsqt/MapUtils/DataBlocks/D2SpellCast.h \
    toolsqt/MapUtils/DataBlocks/D2SpellEffects.h \
    toolsqt/MapUtils/DataBlocks/D2StackDestroyed.h \
    toolsqt/MapUtils/DataBlocks/D2StackTemplate.h \
    toolsqt/MapUtils/DataBlocks/D2SubRace.h \
    toolsqt/MapUtils/DataBlocks/D2TalismanCharges.h \
    toolsqt/MapUtils/DataBlocks/D2Tomb.h \
    toolsqt/MapUtils/DataBlocks/D2Trainer.h \
    toolsqt/MapUtils/DataBlocks/D2TurnSummary.h \
    toolsqt/MapUtils/DataBlocks/D2Unit.h \
    toolsqt/MapUtils/DataBlock.h \
    toolsqt/MapUtils/DataBlocks/D2LandMark.h \
    toolsqt/MapUtils/DataBlocks/D2MapBlock.h \
    toolsqt/MapUtils/DataBlocks/D2Stack.h \
    toolsqt/MapUtils/DataBlocks/D2Village.h \
    MapObjects/LandmarkObject.h \
    MapObjects/MapObject.h \
    MapObjects/StackObject.h \
    toolsqt/DBFModel/BaseDBClases.h \
    toolsqt/DBFModel/ByteHelper.h \
    toolsqt/DBFModel/DBFModel.h \
    toolsqt/DBFModel/GameClases.h \
    toolsqt/DBFModel/SimpleDBFAdapter.h \
    toolsqt/MapUtils/DataBlocks/DataBlocks.h \
    toolsqt/MapUtils/IdsHelper.h \
    toolsqt/ResourceModel/ByteUtills.h \
    toolsqt/ResourceModel/GameResource.h \
    toolsqt/ResourceModel/ResourceDescription.h \
    toolsqt/ResourceModel/ResourceModel.h

SOURCES += main.cpp \
    Commands/CommandBus.cpp \
    Common/AutoCompleteModel.cpp \
    Common/DataStorageVIew/DataFragmentDisplayWidget.cpp \
    Common/DataStorageVIew/DataSearchWidget.cpp \
    Common/DataStorageVIew/DataSelectWidget.cpp \
    Common/DataStorageVIew/DataStorage.cpp \
    Common/DataStorageVIew/DataStorageWidget.cpp \
    Common/DataStorageVIew/FileViewerWidget.cpp \
    Common/DataStorageVIew/MappedDataFragment.cpp \
    Common/HunspellChecker.cpp \
    Common/HunspellDLL.cpp \
    Common/SpellCheckHighlighter.cpp \
    Dialogs/SelectMapInSagaDialog.cpp \
    Dialogs/settingseditordialog.cpp \
    Engine/Components/AccessorHolder.cpp \
    Engine/Components/DisplaySettingsManager.cpp \
    Engine/Components/ResourceManager.cpp \
    Engine/Components/TranslationHelper.cpp \
    Engine/GameInstance.cpp \
    Engine/Map/MapStateHolder.cpp \
    Engine/MapConverter.cpp \
    Engine/MapView/CustomMapObject.cpp \
    Engine/MapView/HoverHighlight.cpp \
    Engine/MapView/LandscapeObject.cpp \
    Engine/MapView/MapTileHelper.cpp \
    Engine/MapView/MapView.cpp \
    Engine/MapView/MinimapHelper.cpp \
    Engine/Option.cpp \
    Engine/PresetManager.cpp \
    Engine/SettingsManager.cpp \
    Events/EventBus.cpp \
    Generation/GeneratorHandle.cpp \
    Generation/GeneratorWrapper.cpp \
    Generation/VilgeforcLootGenerator.cpp \
    Generation/VilgeforcStackGenerator.cpp \
    Generation/WaveFunctionCollapse.cpp \
    MapObjects/BaseClases.cpp \
    MapObjects/Diplomacy.cpp \
    MapObjects/EventObject.cpp \
    MapObjects/GameMap.cpp \
    MapObjects/GridObject.cpp \
    MapObjects/MapLink.cpp \
    MapObjects/ObjectAccessors.cpp \
    Common/MapJsonIO.cpp \
    Common/MathHelper.cpp \
    OverlayWidget.cpp \
    QMLHelpers/Common/DynamicImageItem.cpp \
    QMLHelpers/Common/ImageProvider.cpp \
    QMLHelpers/Common/OverlayWrapper.cpp \
    QMLHelpers/Common/PresetController.cpp \
    QMLHelpers/Common/StyleManager.cpp \
    QMLHelpers/Components/BaseObjectPreview.cpp \
    QMLHelpers/Components/CostEditor.cpp \
    QMLHelpers/Components/EventConditionEditor.cpp \
    QMLHelpers/Components/EventEditor.cpp \
    QMLHelpers/Components/EventEffectEditor.cpp \
    QMLHelpers/Components/GroupUnitsEditor.cpp \
    QMLHelpers/Components/ItemView.cpp \
    QMLHelpers/Components/QuestStageEditor.cpp \
    QMLHelpers/Components/SceneVariablesEditor.cpp \
    QMLHelpers/Components/SpellView.cpp \
    QMLHelpers/Components/StackOrderEditor.cpp \
    QMLHelpers/Components/UnitEditor.cpp \
    QMLHelpers/Components/StackTemplateEditor.cpp \
    QMLHelpers/Editors/EditorWidget.cpp \
    QMLHelpers/Editors/MapInfoEditor.cpp \
    QMLHelpers/Editors/MapStackEditor.cpp \
    QMLHelpers/Editors/MapStatisticView.cpp \
    QMLHelpers/Editors/MerchantEditor.cpp \
    QMLHelpers/Editors/QuestLineEditor.cpp \
    QMLHelpers/Editors/RuinEditor.cpp \
    QMLHelpers/Editors/TreasureEditor.cpp \
    QMLHelpers/Editors/VillageEditor.cpp \
    QMLHelpers/MainMenuController.cpp \
    QMLHelpers/MainToolBarController.cpp \
    QMLHelpers/MapCreationController.cpp \
    QMLHelpers/Models/EventsListModel.cpp \
    QMLHelpers/Models/FilterSettingsModel.cpp \
    QMLHelpers/Models/InventoryModel.cpp \
    QMLHelpers/Models/ItemSelectModel.cpp \
    QMLHelpers/Models/LogModel.cpp \
    QMLHelpers/Models/MountainSelectModel.cpp \
    QMLHelpers/Models/OptionsModel.cpp \
    QMLHelpers/Models/OwnerSelectModel.cpp \
    QMLHelpers/Models/SelectGameObjectModel.cpp \
    QMLHelpers/Models/SelectMapObjectModel.cpp \
    QMLHelpers/Models/SelectObjectModel.cpp \
    QMLHelpers/Models/SpellsListModel.cpp \
    QMLHelpers/Models/SpellsSelectModel.cpp \
    QMLHelpers/Models/StringModel.cpp \
    QMLHelpers/Models/TagSelectModel.cpp \
    QMLHelpers/Models/TreeSelectModel.cpp \
    QMLHelpers/Models/UnitHireModel.cpp \
    QMLHelpers/Models/UnitSelectModel.cpp \
    QMLHelpers/Tools/LayersTool.cpp \
    QMLHelpers/Tools/MapEditTool.cpp \
    QMLHelpers/Tools/MapGenerationTool.cpp \
    QMLHelpers/Tools/ObjectCloneTool.cpp \
    QMLHelpers/Tools/ObjectMoveTool.cpp \
    QMLHelpers/Tools/ObjectRemoveTool.cpp \
    QMLHelpers/Tools/PlaceCapitalTool.cpp \
    QMLHelpers/Tools/PlaceCrystalTool.cpp \
    QMLHelpers/Tools/PlaceLMarkTool.cpp \
    QMLHelpers/Tools/PlaceMerchantTool.cpp \
    QMLHelpers/Tools/PlaceObjectTool.cpp \
    QMLHelpers/Tools/PlaceRuinTool.cpp \
    QMLHelpers/Tools/PlaceStackTool.cpp \
    QMLHelpers/Tools/PlaceTombTool.cpp \
    QMLHelpers/Tools/PlaceTreasureTool.cpp \
    QMLHelpers/Tools/PlaceVillageTool.cpp \
    QMLHelpers/Tools/SearchObjectTool.cpp \
    QMLHelpers/Tools/ToolsProvider.cpp \
    Dialogs/ValidationResultYesNoDialog.cpp \
    toolsqt/Common/ByteEncoder.cpp \
    toolsqt/Common/Logger.cpp \
    toolsqt/DBFModel/SimpleDBFAdapter.cpp \
    toolsqt/MapUtils/D2MapEditor.cpp \
    toolsqt/MapUtils/D2MapModel.cpp \
    toolsqt/MapUtils/D2SagaModel.cpp \
    toolsqt/ResourceModel/GameResource.cpp \
    toolsqt/ResourceModel/ResourceModel.cpp

SOURCES += mainwindow.cpp
RC_ICONS = Images/Icon.ico
QT += widgets quickwidgets quick
#QT += multimedia
#greaterThan(QT_MAJOR_VERSION,5): QT += core5compat
#qtHaveModule(printsupport): QT += printsupport
qtHaveModule(opengl): QT += opengl
CONFIG += c++17
CONFIG += sdk_no_version_check
CONFIG *= debug_and_release debug_and_release_target
# CONFIG += force_debug_info

win32 {
    QMAKE_CXXFLAGS += /bigobj
}
{
    contains(QT_ARCH, i386) {
        CONFIG += x86
#        DESTDIR = build/build_32
#        OBJECTS_DIR = build/obj_32

        release:DESTDIR = build/build_32/release
        release:OBJECTS_DIR = build/build_32/release/.obj
        release:MOC_DIR = build/build_32/release/.moc
        release:RCC_DIR = build/build_32/release/.rcc
        release:UI_DIR = build/build_32/release/.ui

        debug:DESTDIR = build/build_32/debug
        debug:OBJECTS_DIR = build/build_32/debug/.obj
        debug:MOC_DIR = build/build_32/debug/.moc
        debug:RCC_DIR = build/build_32/debug/.rcc
        debug:UI_DIR = build/build_32/debug/.ui
    } else {
        CONFIG += x64
#        DESTDIR = build/build_64
#        OBJECTS_DIR = build/obj_64
        release:DESTDIR = build/build_64/release
        release:OBJECTS_DIR = build/build_64/release/.obj
        release:MOC_DIR = build/build_64/release/.moc
        release:RCC_DIR = build/build_64/release/.rcc
        release:UI_DIR = build/build_64/release/.ui

        debug:DESTDIR = build/build_64/debug
        debug:OBJECTS_DIR = build/build_64/debug/.obj
        debug:MOC_DIR = build/build_64/debug/.moc
        debug:RCC_DIR = build/build_64/debug/.rcc
        debug:UI_DIR = build/build_64/debug/.ui
    }
}

build_all:!build_pass {
    CONFIG -= build_all
    CONFIG += release
}
INCLUDEPATH += hunspell

DISTFILES += \
    QML/Preview/BasePreviewItem.qml \
    QML/Component_EventsView.qml \
    QML/CreateMapDialog.qml \
    QML/FilterAreaWidget.qml \
    QML/GroupView.qml \
    QML/ImageButton.qml \
    QML/InventoryView.qml \
    QML/InventoryWidget.qml \
    QML/ItemWidget.qml \
    QML/LoggerWidget.qml \
    QML/MainMenu.qml \
    QML/MainToolbar.qml \
    QML/NTAutocompleteTextEdit.qml \
    QML/NTCheckBox.qml \
    QML/NTCostView.qml \
    QML/NTDropDownButton.qml \
    QML/NTEditorWindow.qml \
    QML/NTHeroPuppet.qml \
    QML/NTImageButton.qml \
    QML/NTInfoDialog.qml \
    QML/NTLeftPanel.qml \
    QML/NTMapObjectSelectView.qml \
    QML/NTRectangle.qml \
    QML/NTSettingsGrid.qml \
    QML/NTSpellCheckedTextEdit.qml \
    QML/NTSpinBoxFloat.qml \
    QML/NTSpinBoxInt.qml \
    QML/NTTextButton.qml \
    QML/NTTextInputDialog.qml \
    QML/NTTooltipItem.qml \
    QML/NTTwoButtonsDialog.qml \
    QML/NTTwoStateButton.qml \
    QML/NTVerticalTabBar.qml \
    QML/OptionsList.qml \
    QML/OrderEditor.qml \
    QML/OverlayWidget.qml \
    QML/PresetPanel.qml \
    QML/SelectItemWidget.qml \
    QML/SelectOrderTargetWidget.qml \
    QML/SelectSpellWidget.qml \
    QML/SelectUnitWidget.qml \
    QML/SpellWidget.qml \
    QML/SpellsListWidget.qml \
    QML/TextFilterItem.qml \
    QML/Tool_CloneObject.qml \
    QML/Tool_ConfigureLayers.qml \
    QML/Tool_GenerateMap.qml \
    QML/Tool_MapEdit.qml \
    QML/Tool_MoveObject.qml \
    QML/Tool_PlaceCapital.qml \
    QML/Tool_PlaceCrystal.qml \
    QML/Tool_PlaceLMark.qml \
    QML/Tool_PlaceMerchant.qml \
    QML/Tool_PlaceRuin.qml \
    QML/Tool_PlaceStack.qml \
    QML/Tool_PlaceTomb.qml \
    QML/Tool_PlaceTreasure.qml \
    QML/Tool_PlaceVillage.qml \
    QML/Tool_RemoveObject.qml \
    QML/Tool_SearchObject.qml \
    QML/UnitEditorWidget.qml \
    QML/UnitHireWidget.qml \
    QML/UnitView.qml \
    QML/edit_Capital.qml \
    QML/edit_Event.qml \
    QML/edit_EventCondition.qml \
    QML/edit_EventEffect.qml \
    QML/edit_Info.qml \
    QML/edit_MapStatistics.qml \
    QML/edit_Merchant.qml \
    QML/edit_QuestLines.qml \
    QML/edit_Ruin.qml \
    QML/edit_Stack.qml \
    QML/edit_StackTemplates.qml \
    QML/edit_Treasure.qml \
    QML/edit_Variables.qml \
    QML/edit_Village.qml \
    QML/Preview/Crystal.qml \
    QML/Preview/Fort.qml \
    QML/Preview/LandMark.qml \
    QML/Preview/Location.qml \
    QML/Preview/MapInfo.qml \
    QML/Preview/Merchant.qml \
    QML/Preview/Mountain.qml \
    QML/Preview/Ruin.qml \
    QML/Preview/Stack.qml \
    QML/Preview/StackTemplate.qml \
    QML/Preview/Tomb.qml \
    QML/Preview/Treasure.qml \
    QML/Preview/Unit.qml \
    toolsqt/.gitignore \
    toolsqt/README.md

RESOURCES +=


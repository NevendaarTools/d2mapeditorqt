#include "EventBus.h"
#include "Engine/GameInstance.h"

EventSubscriber::~EventSubscriber()
{
    unsub();
}

void EventSubscriber::unsub()
{
    RESOLVE(EventBus)->unsubscribe(this);
}

void EventSubscriber::subscribe(const QString &name, std::function<void (const QVariant &)> function)
{
    m_subs.insert(name, function);
    RESOLVE(EventBus)->subscribe(name, this);
}

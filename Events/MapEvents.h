#pragma once
#include <QString>
#include <QMetaType>
#include <QPoint>
#include <MapObjects/MapObject.h>


struct HoverChangedEvent
{
    HoverChangedEvent(const QPoint& pos_ = QPoint(), bool mousePressed_ = false) : pos(pos_), mousePressed(mousePressed_){}
    QPoint pos;
    bool mousePressed = false;
};
Q_DECLARE_METATYPE(HoverChangedEvent)

struct HoveredObjectChangedEvent
{
    QPair<int, int> hover_in;
    QPair<int, int> hover_out;
};
Q_DECLARE_METATYPE(HoveredObjectChangedEvent)


struct ObjectChangedEvent
{
    ObjectChangedEvent(const QPair<int, int>& uid_ = QPair<int, int>()) : uid(uid_){}

    QPair<int, int> uid;
};
Q_DECLARE_METATYPE(ObjectChangedEvent)

struct ObjectRemovedEvent
{
    ObjectRemovedEvent(const QPair<int, int>& uid_ = QPair<int, int>()) : uid(uid_){}

    QPair<int, int> uid;
};
Q_DECLARE_METATYPE(ObjectRemovedEvent)

struct GameLoadedEvent
{
};
Q_DECLARE_METATYPE(GameLoadedEvent)

struct MapCreatedEvent
{
    MapCreatedEvent() {}
};
Q_DECLARE_METATYPE(MapCreatedEvent)

struct MapLoadedEvent
{
};
Q_DECLARE_METATYPE(MapLoadedEvent)

struct GridChangedEvent
{
};
Q_DECLARE_METATYPE(GridChangedEvent)

struct ObjectAddedEvent
{
    ObjectAddedEvent(const QSharedPointer<MapObject>& object_ = QSharedPointer<MapObject>()) : object(object_){}
    QSharedPointer<MapObject> object;
};
Q_DECLARE_METATYPE(ObjectAddedEvent)


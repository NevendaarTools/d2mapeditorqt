#ifndef EVENTBUS_H
#define EVENTBUS_H

#include <QObject>
#include <QVariant>
#include <QDebug>
#include "Common/StringHelper.h"
#include "toolsqt/Common/INotifier.h"

class IEventSubscriber
{
public:
    virtual ~IEventSubscriber(){}
    virtual void onNotify(const QVariant& variant) = 0;
};

class EventSubscriber: public IEventSubscriber
{
public:
    virtual ~EventSubscriber();
    void unsub();

    void onNotify(const QVariant& command)
    {
        const QString typeName = command.typeName();
        return m_subs[typeName](command);
    }

    void subscribe(const QString& name, std::function<void(const QVariant& command)> function);
    template <typename eventT>
    void subscribe(std::function<void(const QVariant& command)> function)
    {
        const QString& name = QMetaType::typeName(qMetaTypeId<eventT>());
        subscribe(name, function);
    }
private:
    QHash<QString, std::function<void(const QVariant& command)>> m_subs;
};

class EventBus : public INotifier
{
public:
    explicit EventBus(){}

    void subscribe(const QString& name, IEventSubscriber * sub)
    {
        // LOG_MESSAGE("Subscribe for : " + name);
        m_subscribers[name].append(sub);
    }

    void unsubscribe(IEventSubscriber * sub)
    {
        QStringList keys = m_subscribers.keys();
        for (int i = 0; i < keys.count(); ++i)
        {
            for (int k = 0; k < m_subscribers[keys[i]].count(); ++k)
            {
                if (m_subscribers[keys[i]][k] == sub)
                {
                    // LOG_MESSAGE("Unsubscribe for : " + keys[i]);
                    m_subscribers[keys[i]].removeAt(k);
                }
            }
        }
    }

    virtual void notify(const QVariant& event) override
    {
        const QString typeName = event.typeName();
        for (int i = 0; i < m_subscribers[typeName].count(); ++i)
        {
            m_subscribers[typeName][i]->onNotify(event);
        }
    }

    template <typename eventT>
    void notify(const eventT& event)
    {
        return notify(QVariant::fromValue(event));
    }
private:
    QHash<QString, QList<IEventSubscriber*>> m_subscribers;
};

#endif // EVENTBUS_H

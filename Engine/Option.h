#ifndef OPTION_H
#define OPTION_H
#include <QString>
#include <QList>

class Option
{
public:
    enum Type
    {
        Bool = 0,
        Float,
        Int,
        String,
        Enum,
        Dir,

        Title = 32,
        Spacer
    };
    enum Status
    {
        Normal = 0,
        Hiden,
        Disabled
    };

    QString name;
    QString desc;
    QString value;
    QString min = "0";
    QString defValue = "0";
    QString max = "10000";
    QString group = "";

    QList<QString> variants;
    QList<QString> variantValues;
    Type type;
    Status status = Normal;

    static Option title(QString name, QString desc = QString())
    {
        Option result;
        result.name = name;
        result.desc = desc;
        if (desc.isEmpty())
            desc = name + "_desc";
        result.type = Type::Title;
        return result;
    }

    static Option spacer()
    {
        Option result;
        result.type = Type::Spacer;
        return result;
    }

    static Option enumOption(QString name, QString value, QString desc, QList<QString> variants)
    {
        Option result;
        result.name = name;
        result.desc = desc;
        result.type = Type::Enum;
        result.variants = variants;
        return result;
    }

    static Option enumOption(QString name, QString value, QString desc,QList<QString> variants,QList<QString> variantValues)
    {
        Option result;
        result.name = name;
        result.desc = desc;
        result.type = Type::Enum;
        result.variants = variants;
        result.variantValues = variantValues;
        return result;
    }

    static Option stringOption(QString name, QString value, QString desc)
    {
        Option result;
        result.name = name;
        result.desc = desc;
        result.type = Type::String;
        result.value = value;
        return result;
    }

    static Option dirOption(QString name, QString value, QString desc)
    {
        Option result;
        result.name = name;
        result.desc = desc;
        result.type = Type::Dir;
        result.value = value;
        return result;
    }

    static Option boolOption(QString name, bool value, QString desc)
    {
        Option result;
        result.name = name;
        result.desc = desc;
        result.type = Type::Bool;
        result.value = value ? "True" : "False";
        return result;
    }
    static Option BoolOption(QString name, bool value)
    {
        return boolOption(name, value, name + "_desc");
    }

    static Option floatOption(QString name, float value, QString desc, float min = 0.01f, float max = 10000)
    {
        Option result;
        result.name = name;
        result.desc = desc;
        result.type = Type::Float;
        result.min = QString::number(min);
        result.value = QString::number(value);
        result.max = QString::number(max);
        return result;
    }

    static Option floatOption(QString name, float value, float min = 0.01f, float max = 10000)
    {
        return floatOption(name, value, name + "_desc", min, max);
    }

    static Option intOption(QString name, int value, QString desc, int min = 0, int max = 10000)
    {
        Option result;
        result.name = name;
        result.desc = desc;
        result.type = Type::Int;
        result.min = QString::number(min);
        result.value = QString::number(value);
        result.max = QString::number(max);
        return result;
    }

    static Option intOption(QString name, int value, int min = 0, int max = 10000)
    {
        return intOption(name, value, name + "_desc", min, max);
    }

    static Option floatOption(QString name, QString value, QString desc, QString min, QString max)
    {
        return floatOption(name, value.toFloat(), desc, min.toFloat(), max.toFloat());
    }

    static Option intOption(QString name, QString value, QString desc, QString min, QString max)
    {
        return intOption(name, value.toInt(), desc, min.toInt(), max.toInt());
    }

    static bool setValueByName(QList<Option> & options, const QString & name, const QString & value)
    {
        for (int i = 0; i < options.count(); i++)
        {
            if (options[i].name == name)
            {
                options[i].value = value;
                return true;
            }
        }
        return false;
    }

    static Option getOption(const QList<Option> & options, const QString & name)
    {
        foreach (Option option, options)
        {
            if (option.name == name)
                return option;
        }
        return Option();
    }

    static QString getOptionValue(const QList<Option> & options, const QString & name)
    {
        foreach (Option option, options)
        {
            if (option.name == name)
                return option.value;
        }
        return QString();
    }

    static bool getBoolOption(const Option & option)
    {
        return option.value.trimmed() == "True";
    }

    static bool hasOption(const QList<Option> & options, const QString & name)
    {
        foreach (const Option & option, options)
        {
            if (option.name == name)
                return true;
        }
        return false;
    }

    static bool getBoolOption(const QList<Option> & options, const QString & name)
    {
        return getBoolOption(getOption(options, name));
    }

    static float getFloatOption(const QList<Option> & options, const QString & name)
    {
        QString value = getOptionValue(options, name);
        value = value.replace(",", ".");
        return value.toFloat();
    }

    static int getIntOption(const QList<Option> & options, const QString & name)
    {
        QString value = getOptionValue(options, name);
        return value.toInt();
    }

    //
    int index = 0;
};
#endif // OPTION_H

#include "MapView.h"
#include <QtMath>
#include <QQmlContext>
#include <QQmlEngine>
#include <QVBoxLayout>
#include <QStyle>
#include "CustomMapObject.h"
#include <QScrollBar>
#include <QApplication>
#include <QQuickItem>
#include "QMLHelpers/Common/OverlayWrapper.h"
#include "Engine/SettingsManager.h"
#include "Engine/Map/MapStateHolder.h"
#include "Engine/GameInstance.h"
#include "Engine/Components/DisplaySettingsManager.h"

#if QT_CONFIG(wheelevent)
void GraphicsView::wheelEvent(QWheelEvent *e)
{
    if (e->modifiers() & Qt::ControlModifier) {
        if (e->angleDelta().y() > 0)
            view->zoomIn(6);
        else
            view->zoomOut(6);
        e->accept();
    } else {
        //QGraphicsView::wheelEvent(e);
    }
}
#endif

View::View(QWidget *parent)
    : QFrame(parent)
{
    QHBoxLayout * layout = new QHBoxLayout();
    layout->setContentsMargins(0,0,0,0);
    layout->setSpacing(0);
    setLayout(layout);

    m_graphicsView = new GraphicsView(this);
    m_graphicsView->setInteractive(true);
//    m_graphicsView->setRenderHints(QPainter::Antialiasing | QPainter::SmoothPixmapTransform);
    m_graphicsView->setDragMode(QGraphicsView::ScrollHandDrag);
    m_graphicsView->setViewportUpdateMode(QGraphicsView::BoundingRectViewportUpdate);
    m_graphicsView->setTransformationAnchor(QGraphicsView::AnchorUnderMouse);
    m_style = new StyleManager(this);

    QGridLayout *topLayout = new QGridLayout;
    topLayout->setContentsMargins(0,0,0,0);
    topLayout->addWidget(m_graphicsView, 1, 0);
    setLayout(topLayout);
    setupMatrix();
    m_landscape  = new LandscapeObject();
    m_grid  = new GridView();
    m_hoverItem = new HoverHighlightItem();
    m_scene = new CustomScene(this);
    m_graphicsView->setScene(m_scene);
    m_scene->addItem(m_landscape);
    m_scene->addItem(m_grid);
    m_scene->addItem(m_hoverItem);

    m_scene->setBackgroundBrush(QBrush(Qt::gray));
    // m_infoProvider = new InfoPanelProvider(this);
    initToolbar();
    m_graphicsView->setHorizontalScrollBarPolicy(Qt::ScrollBarAlwaysOff);
    m_graphicsView->setVerticalScrollBarPolicy(Qt::ScrollBarAlwaysOff);
    m_editor = new EditorWidget(this, this);
    // m_editor->setInfoProvider(m_infoProvider);
    // m_editor->setOverlayWidget(m_overlayWidget);

    QObject::connect(&m_timer, SIGNAL(timeout()), this, SLOT(onTimer()));
    m_timer.setSingleShot(false);
    m_timer.start(42);
    m_edgeMove = RESOLVE(SettingsManager)->getBool("edgeMove");

    subscribe<ObjectChangedEvent>([this](const QVariant &event){onObjectUpdated(event);});
    subscribe<ObjectRemovedEvent>([this](const QVariant &event){onObjectRemoved(event);});
    subscribe<DisplaySettingsChangedEvent>([this](const QVariant &event){onLayersSettingsUpdate(event);});
    subscribe<ObjectAddedEvent>([this](const QVariant &event){onObjectAdded(event);});
    subscribe<SettingsChangedEvent>([this](const QVariant &event){onSettingsUpdate(event);});

    registerExecutor<RemoveObjectCommand>
            ([this](const QVariant &command){return executeRemoveObjectCommand(command);});
    registerExecutor<FocusOnCommand>
            ([this](const QVariant &command){return executeFocusOnCommand(command);});
    layout->addWidget(m_graphicsView);
    m_toolbar->setFixedWidth(m_style->toolBarPanelWidth());
    layout->addWidget(m_toolbar);
}

CommandResult View::executeRemoveObjectCommand(const QVariant &commandVar)
{
    RemoveObjectCommand command = commandVar.value<RemoveObjectCommand>();
    RESOLVE(MapStateHolder)->removeObject(command.uid);
    return CommandResult::success();
}

CommandResult View::executeFocusOnCommand(const QVariant &commandVar)
{
    FocusOnCommand command = commandVar.value<FocusOnCommand>();
    QPointF point = cartesianToIsometric(command.pos.x() * TILE_SIZE, command.pos.y() * TILE_SIZE);
    m_graphicsView->centerOn(point);
    return CommandResult::success();
}

void View::onObjectUpdated(const QVariant &event)
{
    ObjectChangedEvent command = event.value<ObjectChangedEvent>();
    foreach(QSharedPointer<MapGraphicsObject> obj, m_objects)
    {
        if (obj->getObject()->uid == command.uid)
            obj->reload();
    }
}

void View::onObjectRemoved(const QVariant &event)
{
    ObjectRemovedEvent command = event.value<ObjectRemovedEvent>();
    for (int i = 0; i < m_objects.count(); ++i)
    {
        if (m_objects[i]->getObject()->uid == command.uid)
        {
            m_objects.removeAt(i);
            break;
        }
    }
}

void View::onLayersSettingsUpdate(const QVariant &eventVar)
{
    Q_UNUSED(eventVar)
    foreach(QSharedPointer<MapGraphicsObject> obj, m_objects)
    {
        if (RESOLVE(DisplaySettingsManager)->settings.hidenObjects.contains(obj->getObject()->uid.first))
            obj->item()->setVisible(false);
        else
            obj->item()->setVisible(true);
    }
}

void View::onSettingsUpdate(const QVariant &eventVar)
{
    SettingsChangedEvent event = eventVar.value<SettingsChangedEvent>();
    QString value =  RESOLVE(SettingsManager)->get(event.name);
    if (event.name == "enable_object_menu")
    {
        foreach(QSharedPointer<MapGraphicsObject> obj, m_objects)
        {
            obj->setOption("enable_object_menu", value);
        }
    }
    m_edgeMove = RESOLVE(SettingsManager)->getBool("edgeMove");
    qDebug()<<Q_FUNC_INFO<<event.name<<value;
    if (event.name == "enable_map_drag")
    {
//        if (value == "True")
//        {
//            m_graphicsView->setDragMode(QGraphicsView::DragMode::ScrollHandDrag);
//        }
//        else
//        {
//            m_graphicsView->setDragMode(QGraphicsView::DragMode::NoDrag);
//        }
    }
}

QGraphicsView *View::view() const
{
    return static_cast<QGraphicsView *>(m_graphicsView);
}

void View::resetView()
{
    setupMatrix();
    m_graphicsView->ensureVisible(QRectF(0, 0, 0, 0));
}

void View::setupMatrix()
{
    qreal scale = qPow(qreal(2), (m_zoomValue - 250) / qreal(50));
    QTransform matrix;
    matrix.scale(scale, scale);
    m_graphicsView->setTransform(matrix);
}

void View::initToolbar()
{
    m_toolbarProvider = new MainToolBarController(this);
    m_toolbar = new QQuickWidget(m_graphicsView);
    m_toolbar->setAttribute(Qt::WA_TranslucentBackground, true);
    m_toolbar->setAttribute(Qt::WA_AlwaysStackOnTop, true);
    m_toolbar->setAttribute(Qt::WA_TransparentForMouseEvents, false);
    m_toolbar->setClearColor(Qt::transparent);
    m_toolbar->rootContext()->setContextProperty("toolbar", m_toolbarProvider);
    auto overlay = new OverlayWrapper(this);
    m_toolbar->rootContext()->setContextProperty("overlay", overlay);
    m_toolbar->rootContext()->setContextProperty("translate", overlay);
    ToolsProvider *tools = m_toolbarProvider->tools();
    m_toolbar->rootContext()->setContextProperty("tools", tools);
    m_grid->setTools(tools);
    m_scene->setTools(tools);
    // m_toolbar->rootContext()->setContextProperty("overlay", m_infoProvider);
    //m_toolbar->rootContext()->setContextProperty("toolsProvider", m_grid);
    m_toolbar->engine()->addImageProvider(QLatin1String("provider"), new ImageProvider());
    m_toolbar->setSource(QUrl::fromLocalFile("QML/MainToolbar.qml"));
    m_toolbar->setResizeMode(QQuickWidget::SizeRootObjectToView);
}

void View::closeMap()
{
    m_objects.clear();
}

void View::onObjectAdded(const QVariant &eventVar)
{
    ObjectAddedEvent event = eventVar.value<ObjectAddedEvent>();
    if (event.object->populate)
    {
        CustomMapObject * object = new  CustomMapObject(event.object);
        m_scene->addItem(object->item());
        object->reload();
        m_objects.append(QSharedPointer<MapGraphicsObject>(object));
    }
}

void View::setMap(const GameMap &map)
{
    m_map = map;
    m_objects.clear();
    QElapsedTimer timer;
    timer.start();
    QList<QSharedPointer<MapObject> > objects = map.objects();
    foreach(QSharedPointer<MapObject> mapObj, objects)
    {
        if (!mapObj->populate)
            continue;
        CustomMapObject * object = new  CustomMapObject(mapObj);
        m_scene->addItem(object->item());
        object->reload();
        m_objects.append(QSharedPointer<MapGraphicsObject>(object));
        if (RESOLVE(DisplaySettingsManager)->settings.hidenObjects.contains(mapObj->uid.first))
            object->item()->setVisible(false);
    }
    qDebug()<<"GameMap::object count = "<<objects.count();
    int mapSize = m_map.mapSize();
    int w = mapSize * TILE_SIZE * 2;

    m_landscape->setup(m_map.getGrid());
    m_grid->reloadMaps();
    m_hoverItem->init();
    m_landscape->setPos(QPointF(-w/2 - 50,0 - 50));
    m_grid->setPos(QPointF(-w/2,0));
    qDebug()<<"Creating mapitems takes"<<timer.elapsed();
}

void View::resizeEvent(QResizeEvent *event)
{
    // int wPart = m_style->toolBarPanelWidth();
    // m_toolbar->setGeometry(m_graphicsView->width() - wPart,0, wPart, m_graphicsView->height());
    QWidget::resizeEvent(event);
}

void View::zoomIn(int level)
{
    m_zoomValue += level;
    if (m_zoomValue > 500) m_zoomValue = 500;
    setupMatrix();
}

void View::zoomOut(int level)
{
    m_zoomValue -= level;
    if (m_zoomValue < 0) m_zoomValue = 0;
    setupMatrix();
}

void View::onTimer()
{
    QRectF view = m_graphicsView->mapToScene(m_graphicsView->viewport()->geometry() ).boundingRect();
    foreach(QSharedPointer<MapGraphicsObject> obj, m_objects)
    {
        obj->nextFrame(view);
    }
    m_hoverItem->nextFrame(view);
    m_grid->onTimer();
    if (!m_edgeMove)
        return;
    int xMove = 0;
    int yMove = 0;
    QPoint cursorPoint = mapFromGlobal(QCursor::pos());
//    if (m_toolbar->geometry().contains(cursorPoint))
//        return;
    if (QApplication::mouseButtons() != Qt::NoButton)
        return;
    int w = 5;
    float step = 75;
    if (cursorPoint.x() < w)
    {
        xMove = -step;
    }
    if (cursorPoint.x() > this->width() - w)
    {
        xMove = step;
    }
    if (cursorPoint.y() < w)
    {
        yMove = -step;
    }
    if (cursorPoint.y() > this->height() - w)
    {
        yMove = step;
    }

    if (xMove != 0 || yMove != 0)
    {
        QPointF point = m_graphicsView->mapToScene(m_graphicsView->viewport()->rect()).boundingRect().center();
        point.setY(point.y() + yMove);
        point.setX(point.x() + xMove);
        m_graphicsView->centerOn(point);
    }
}

View *GraphicsView::getView() const
{
    return view;
}

void GraphicsView::keyPressEvent(QKeyEvent *event)
{
    if (event->key() == Qt::Key_G)
    {
        auto & settings = RESOLVE(DisplaySettingsManager)->settings;
        settings.gridEnabled = !settings.gridEnabled;
        RESOLVE(EventBus)->notify(QVariant::fromValue(DisplaySettingsChangedEvent()));
    }
    if (event->key() == Qt::Key_P)
    {
        auto & settings = RESOLVE(DisplaySettingsManager)->settings;
        settings.passableEnabled = !settings.passableEnabled;
        RESOLVE(EventBus)->notify(QVariant::fromValue(DisplaySettingsChangedEvent()));
    }

    QGraphicsView::keyPressEvent(event);
}

QPoint CustomScene::mapPos(const QPointF &pos) const
{
    MapGrid grid = RESOLVE(MapStateHolder)->getGrid();
    auto m_mapSize = grid.cells.count();
    QPointF pos2 = isometricToCartesian(pos.x() + m_mapSize * TILE_SIZE, pos.y());
    QPoint result;
    result.setX(ceil(pos2.x() / TILE_SIZE ) - m_mapSize / 2 - 1);
    result.setY(ceil(pos2.y() / TILE_SIZE ) + m_mapSize / 2 - 1);
    if (result.x() < 0)
        result.setX(0);
    if (result.y() < 0)
        result.setY(0);
    if (result.x() >= m_mapSize)
        result.setX(m_mapSize - 1);
    if (result.y() >= m_mapSize)
        result.setY(m_mapSize - 1);
    return result;
}

void CustomScene::mousePressEvent(QGraphicsSceneMouseEvent *event) {
    QGraphicsScene::mousePressEvent(event);
    m_cursorePos = mapPos(event->scenePos());
    _lastMousePos = event->screenPos();
    if (m_tools != nullptr)
    {
        if (event->modifiers() == Qt::KeyboardModifier(MOUSE_EMULATED_LEFT))
            m_tools->mousePressed(Qt::MiddleButton);
        // else

    }

}

void CustomScene::mouseMoveEvent(QGraphicsSceneMouseEvent *event) {
    QGraphicsScene::mouseMoveEvent(event);
    QPoint old = m_cursorePos;
    m_cursorePos = mapPos(event->scenePos());
    if (old !=m_cursorePos)
    {
        RESOLVE(EventBus)->notify(HoverChangedEvent{m_cursorePos, true});
        const MapObjBinding & binding = RESOLVE(MapStateHolder)->getGrid().objBinging;
        if (!binding.binding[m_cursorePos.x()][m_cursorePos.y()].items.isEmpty())
        {
            HoveredObjectChangedEvent event;
            event.hover_in = binding.binding[m_cursorePos.x()][m_cursorePos.y()].items.first();
            event.hover_out = EMPTY_ID;
            RESOLVE(EventBus)->notify(event);
        }
        else
        {
            HoveredObjectChangedEvent event;
            event.hover_in = EMPTY_ID;
            event.hover_out = EMPTY_ID;
            RESOLVE(EventBus)->notify(event);
        }
    }
    if (_mousePressed) {
    }
}

void CustomScene::mouseReleaseEvent(QGraphicsSceneMouseEvent *event) {
    QGraphicsScene::mouseReleaseEvent(event);
    if (event->button() == Qt::RightButton) {
        _mousePressed = false;
    }
    if (m_tools != nullptr)
    {
        m_tools->mouseReleased();
    }
    if ((qAbs(event->screenPos().x() - _lastMousePos.x()) < 8) &&
        (qAbs(event->screenPos().y() - _lastMousePos.y()) < 8))
    {
        qDebug()<<"Pressed";
        m_tools->mousePressed(event->button());
    }
}

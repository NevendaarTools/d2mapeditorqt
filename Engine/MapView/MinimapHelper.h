#ifndef MINIMAPHELPER_H
#define MINIMAPHELPER_H
#include "toolsqt/MapUtils/D2MapModel.h"
#include "toolsqt/DBFModel/DBFModel.h"
#include "MapObjects/GameMap.h"

QString shortRaceByPlayerId(D2MapModel map, QSharedPointer<DBFModel> model, const QPair<int, int> &id);

QString shortRaceByPlayerId2(const GameMap & map, QSharedPointer<DBFModel> model, const QPair<int, int> &id);

class MinimapHelper
{
public:
    MinimapHelper();
    static void init();
    static QImage miniMap(D2MapModel &map, QSharedPointer<DBFModel> model);
    static QImage miniMap(GameMap &map, QSharedPointer<DBFModel> model);
};

#endif // MINIMAPHELPER_H

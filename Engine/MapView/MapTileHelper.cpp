#include "MapTileHelper.h"
#include "Engine/Components/ResourceManager.h"
#include "toolsqt/MapUtils/MapGrid.h"
#include "toolsqt/Common/Logger.h"
#include "Engine/GameInstance.h"
#include "toolsqt/DBFModel/DBFModel.h"

const QImage & MapTileHelper::getEmpty() const
{
    return m_empty;
}

QImage MapTileHelper::createDiamond()
{
    QImage resultImage = QImage(TILE_SIZE * 2, TILE_SIZE, QImage::Format_ARGB32);
    resultImage.fill(Qt::transparent);
    // clang-format off
    static const int tileIndices[] = {
        1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,0,0,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,
        1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,0,0,0,0,0,0,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,
        1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,0,0,0,0,0,0,0,0,0,0,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,
        1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,
        1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,
        1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,
        1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,
        1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,
        1,1,1,1,1,1,1,1,1,1,1,1,1,1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,
        1,1,1,1,1,1,1,1,1,1,1,1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,2,2,2,2,2,2,2,2,2,2,2,2,2,2,
        1,1,1,1,1,1,1,1,1,1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,2,2,2,2,2,2,2,2,2,2,2,2,
        1,1,1,1,1,1,1,1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,2,2,2,2,2,2,2,2,2,2,
        1,1,1,1,1,1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,2,2,2,2,2,2,2,2,
        1,1,1,1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,2,2,2,2,2,2,
        1,1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,2,2,2,2,
        0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,2,2,
        0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,4,4,
        3,3,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,4,4,4,4,
        3,3,3,3,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,4,4,4,4,4,4,
        3,3,3,3,3,3,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,4,4,4,4,4,4,4,4,
        3,3,3,3,3,3,3,3,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,4,4,4,4,4,4,4,4,4,4,
        3,3,3,3,3,3,3,3,3,3,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,4,4,4,4,4,4,4,4,4,4,4,4,
        3,3,3,3,3,3,3,3,3,3,3,3,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,4,4,4,4,4,4,4,4,4,4,4,4,4,4,
        3,3,3,3,3,3,3,3,3,3,3,3,3,3,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,
        3,3,3,3,3,3,3,3,3,3,3,3,3,3,3,3,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,
        3,3,3,3,3,3,3,3,3,3,3,3,3,3,3,3,3,3,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,
        3,3,3,3,3,3,3,3,3,3,3,3,3,3,3,3,3,3,3,3,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,
        3,3,3,3,3,3,3,3,3,3,3,3,3,3,3,3,3,3,3,3,3,3,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,
        3,3,3,3,3,3,3,3,3,3,3,3,3,3,3,3,3,3,3,3,3,3,3,3,0,0,0,0,0,0,0,0,0,0,0,0,0,0,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,
        3,3,3,3,3,3,3,3,3,3,3,3,3,3,3,3,3,3,3,3,3,3,3,3,3,3,0,0,0,0,0,0,0,0,0,0,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,
        3,3,3,3,3,3,3,3,3,3,3,3,3,3,3,3,3,3,3,3,3,3,3,3,3,3,3,3,0,0,0,0,0,0,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,
        3,3,3,3,3,3,3,3,3,3,3,3,3,3,3,3,3,3,3,3,3,3,3,3,3,3,3,3,3,3,0,0,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4
    };
    // clang-format on
    for (int x = 0; x < TILE_SIZE * 2; ++x)
    {
        for (int y = 0; y < TILE_SIZE; ++y)
        {
            int index = y * TILE_SIZE * 2 + x;
            if (tileIndices[index] == 0)
                resultImage.setPixelColor(x,y, QColor(255,255,255,255));
        }
    }
    return resultImage;
}

QImage MapTileHelper::mainImage(int value, int x, int y, int cellsCount)
{
    int newX = getXOffset(x * TILE_SIZE, y * TILE_SIZE, cellsCount * TILE_SIZE * 2, TILE_SIZE);
    int newY = getYOffset(x * TILE_SIZE, y * TILE_SIZE);

    const int ground = tileGround(value);
    if (ground == 3)
    {
        return instance()->waterExtractor.extractRegion(newX, newY);
    }
    value = tileTerrain(value);
    return instance()->extractors[value].extractRegion(newX, newY);
}

QImage MapTileHelper::waterBorder(int x, int y, int type)
{
    auto variants = instance()->m_borderImages.values(type);
    if (variants.count() == 0)
        return instance()->getEmpty();
    int index = (x * y + x + y) % variants.count();
    return variants[index];
}

QImage MapTileHelper::createTile(const LayerHelper &layers)
{
    QImage result = QImage(TILE_SIZE * 2, TILE_SIZE, QImage::Format_ARGB32);
    result.fill(Qt::transparent);
    uchar* resultBits = result.bits();
    foreach (const auto & layer, layers.layers)
    {
        const uchar* imBits = layer.image.bits();
        const uchar* maskBits = layer.mask.bits();
        const uchar* diamondBits = instance()->m_diamond.bits();
        for (int x = 0; x < TILE_SIZE * 2; ++x)
        {
            for (int y = 0; y < TILE_SIZE; ++y)
            {
                int index = (y * TILE_SIZE * 2 + x) * 4;
                if (diamondBits[index + 3] && maskBits[index + 3] && imBits[index + 3])
                {
                    resultBits[index] = imBits[index];
                    resultBits[index + 1] = imBits[index + 1];
                    resultBits[index + 2] = imBits[index + 2];
                    resultBits[index + 3] = imBits[index + 3];
                }
            }
        }
    }

    return result;
}

void MapTileHelper::drawTile(const LayerHelper &layers, uchar *resultBits, uint startIndex, uint w)
{
    foreach (const auto & layer, layers.layers)
    {
        const uchar* imBits = layer.image.bits();
        const uchar* maskBits = layer.mask.bits();
        const uchar* diamondBits = instance()->m_diamond.bits();
        for (int x = 0; x < TILE_SIZE * 2; ++x)
        {
            for (int y = 0; y < TILE_SIZE; ++y)
            {
                int index = (y * TILE_SIZE * 2 + x) * 4;
                uint resultIndex = startIndex + x * 4 + w * y;
                if (diamondBits[index + 3] && maskBits[index + 3] && imBits[index + 3])
                {
                    resultBits[resultIndex] = imBits[index];
                    resultBits[resultIndex + 1] = imBits[index + 1];
                    resultBits[resultIndex + 2] = imBits[index + 2];
                    resultBits[resultIndex + 3] = imBits[index + 3];
                }
            }
        }
    }
}

void MapTileHelper::drawImage(const QImage &image, uchar *resultBits, uint startIndex, uint w)
{
    const uchar* imBits = image.bits();
    for (int x = 0; x < image.width(); ++x)
    {
        for (int y = 0; y < image.height(); ++y)
        {
            int index = (y * image.width() + x) * 4;
            uint resultIndex = startIndex + x * 4 + w * y;
            if (imBits[index + 3])
            {
                resultBits[resultIndex] = imBits[index];
                resultBits[resultIndex + 1] = imBits[index + 1];
                resultBits[resultIndex + 2] = imBits[index + 2];
                resultBits[resultIndex + 3] = imBits[index + 3];
            }
        }
    }
}

const QImage & MapTileHelper::tileByValue(int value)
{
    const int ground = tileGround(value);
    if (ground == 3)
    {
        //            if (instance()->tilesMap.contains(value))
        //                return instance()->tilesMap[value];
        if (instance()->groundMap.contains(ground))
            return instance()->groundMap[ground];
        return instance()->getEmpty();
    }
    value = tileTerrain(value);
    if (instance()->tilesMap.contains(value))
        return instance()->tilesMap[value];
    return instance()->getEmpty();
}

const QImage &MapTileHelper::groundByValue(int value)
{
    value = tileGround(value);
    if (instance()->groundMap.contains(value))
        return instance()->groundMap[value];
    return instance()->getEmpty();
}

QImage &MapTileHelper::roadImage(int value)
{
    return instance()->m_roadImages[value];
}

const QImage &MapTileHelper::forestByValue(int value)
{
    int forest = tileForestImage(value);
    int raceCode = tileTerrain(value);
    int keyInt = (forest << 26) | raceCode;
    if (instance()->treeMap.contains(keyInt))
        return instance()->treeMap[keyInt];
    QString key = instance()->raceMap[raceCode] + "F" + QString::number(forest).rightJustified(4, '0');

    QList<QImage> res = RESOLVE(ResourceModel)->getFramesById("IsoTerrn", key.trimmed());
    if (res.count() > 0)
    {
        return instance()->treeMap[keyInt] = res [0];
        return res[0];
    }
    return instance()->getEmpty();
}

const QImage &MapTileHelper::diamond() const
{
    return m_diamond;
}

void MapTileHelper::init()
{
    if (instance()->m_initialised)
        return;
    Logger::disable();
    QVector<QSharedPointer<Lterrain>> terrains = RESOLVE(DBFModel)->getList<Lterrain>();
    foreach (QSharedPointer<Lterrain> terr, terrains)
    {
        QString key = terr->text;
        key = key.replace("L_", "").left(2);
        instance()->raceMap.insert(terr->id, key);
        key = "ISO" + key;
        QList<QImage> res = RESOLVE(ResourceModel)->getFramesById("PalMap", key.trimmed());
        //            QList<QImage> res = rModel->getFramesById("Ground", key.trimmed());
        if (res.count() > 0){
            instance()->tilesMap.insert(terr->id, res[0]);
            //qDebug()<<"terrain = "<<terr->id<<" ("<<key<<")";
        }
    }
    //    {
    //        QString key = "" + key;
    //        QList<QImage> res = RESOLVE(ResourceModel)->getFramesById("PalMap", "ISOWA");
    //        //            QList<QImage> res = rModel->getFramesById("Ground", key.trimmed());
    //        if (res.count() > 0)
    //            instance()->tilesMap.insert(3, res[0]);
    //    }
    QVector<QSharedPointer<Lground>> grounds = RESOLVE(DBFModel)->getList<Lground>();
    /*
    0	L_PLAIN
    1	L_FOREST
    4	L_MOUNTAIN
    3	L_WATER
    */
    foreach (QSharedPointer<Lground> ground, grounds)
    {
        QString key = ground->text;
        key = key.replace("L_", "").left(2);
        for(int k = 0; k < 3; ++k)
        {
            QString resKey = key.trimmed() + "_" + QString::number(k).rightJustified(2,'0');
            QList<QImage> res = RESOLVE(ResourceModel)->getFramesById("Ground", resKey);
            if (res.count() > 0){
                instance()->groundMap.insert(ground->id, res[0]);
                if (ground->id == 3)
                    instance()->waterExtractor.addImage(res[0]);
            }
        }
    }
    QVector<QSharedPointer<Lterrain>> races = RESOLVE(DBFModel)->getList<Lterrain>();
    foreach (QSharedPointer<Lterrain> race, races)
    {
        instance()->extractors.insert(race->id, MapRegionExtractor(0));
        QString key = race->text;
        key = key.replace("L_", "");
        for(int k = 0; k < 3; ++k)
        {
            QString resKey = key.trimmed() + "_" + QString::number(k).rightJustified(2,'0');
            QList<QImage> res = RESOLVE(ResourceModel)->getFramesById("Ground", resKey);
            if (res.count() > 0){
                instance()->extractors[race->id].addImage(res[0]);
            }
        }
    }


    for(int i = 0; i < 17; i++)
    {
        QString key = "ROAD" + QString::number(i).rightJustified(2, '0') + "00";
        QSharedPointer<ImagesData> image = RESOLVE(ResourceManager)->getImagesData("IsoTerrn", key, false);
        if (image.isNull() || image.data()->data.count() == 0)
        {
            QImage image(40,40, QImage::Format_ARGB32);
            image.fill(Qt::red);
            instance()->m_roadImages.insert(i, image);
        }
        else
        {
            QImage im = image.data()->data[0];
            //            im.convertTo(QImage::Format_ARGB32);
            instance()->m_roadImages.insert(i, im);
        }
    }

    for(int i = 1; i < 32; i++)
    {
        for(int k = 0; k < 3; ++k)
        {
            auto borderImageName = QString("WA_%1_%2")
                    .arg(QString::number(i).rightJustified(2,'0'))
                    .arg(QString::number(k).rightJustified(2,'0'));
            QSharedPointer<ImagesData> image = RESOLVE(ResourceManager)->getImagesData("GrBorder", borderImageName, false, PreprocessingShaderType::Border);
            if (image.isNull() || image.data()->data.count() == 0)
            {
                continue;
            }
            else
            {
                QImage im = image.data()->data[0];
                im.convertTo(QImage::Format_ARGB32);
                im = im.scaled(TILE_SIZE * 2, TILE_SIZE);
                instance()->m_borderImages.insert(i, im);
            }
        }
    }

    for(int i = 1; i < 32; i++)
    {
        for(int k = 0; k < 3; ++k)
        {
            auto borderImageName = QString("NE_%1_%2")
                    .arg(QString::number(i).rightJustified(2,'0'))
                    .arg(QString::number(k).rightJustified(2,'0'));
            QSharedPointer<ImagesData> image = RESOLVE(ResourceManager)->getImagesData("GrBorder", borderImageName, false, PreprocessingShaderType::TransparentBlack);
            if (image.isNull() || image.data()->data.count() == 0)
            {
                continue;
            }
            else
            {
                QImage im = image->data[0];
                im.convertTo(QImage::Format_ARGB32);
                im = im.scaled(TILE_SIZE * 2, TILE_SIZE);
                instance()->m_borderMasks.insert(i, im);
            }
        }
    }

    Logger::enable();
    instance()->m_initialised = true;
}

const QImage &MapTileHelper::getBlendMask(int x, int y, int type)
{
    auto variants = instance()->m_borderMasks.values(type);
    if (variants.count() == 0)
    {
        return instance()->getEmpty();
    }
    int index = (x * y + x + y) % variants.count();
    int key = type * 1000000 + index;
    if (!instance()->m_tileHash.contains(key))
    {
        instance()->m_tileHash[key] = variants[index];
    }
    return instance()->m_tileHash[key];
}

MapTileHelper::MapTileHelper()
{
    m_diamond = createDiamond();
    m_empty = QImage(TILE_SIZE * 2, TILE_SIZE, QImage::Format_ARGB32);
    m_empty.fill(Qt::red);
}

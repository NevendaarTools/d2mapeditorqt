#include "MinimapHelper.h"
#include "MapTileHelper.h"
#include "toolsqt/MapUtils/DataBlocks/DataBlocks.h"
#include "toolsqt/MapUtils/D2MapModel.h"
#include "Common/MapJsonIO.h"
#include "MapObjects/CrystalObject.h"
#include "MapObjects/Diplomacy.h"
#include "Engine/Components/ResourceManager.h"
#include "Engine/GameInstance.h"
#include <QPainter>
#include "MapObjects/FortObject.h"
#include "MapObjects/MountainObject.h"
#include "MapObjects/LandmarkObject.h"
#include "MapObjects/StackObject.h"
#include "MapObjects/RuinObject.h"

MinimapHelper::MinimapHelper()
{

}

void MinimapHelper::init()
{
    MapTileHelper::init();
}

QColor colorForRace(int value)
{
    switch(value)
    {
    case 5: return QColor("#666633");//neutrals
        case 1: return QColor("#006633");//empire
        case 3: return QColor("#990000");//demons
        case 2: return Qt::white;//gnoms
        case 4: return QColor("#444444");//undead
        case 6: return Qt::darkYellow;//elves
        case 29: return QColor("#336699");
    }
    return Qt::red;
}

QImage MinimapHelper::miniMap(D2MapModel &map, QSharedPointer<DBFModel> model)
{
    int mapSize = map.header.mapSize;
    int tileSize = 4;
    QImage result (mapSize * tileSize, mapSize * tileSize, QImage::Format_ARGB32);
    result.fill(Qt::gray);
    QList<D2MapBlock> blocks = map.getAll<D2MapBlock>();
    QList<D2Village> villages = map.getAll<D2Village>();
    QList<D2Capital> capitals = map.getAll<D2Capital>();
    QList<D2Ruin> ruins = map.getAll<D2Ruin>();
    QList<D2Stack> stacks = map.getAll<D2Stack>();
    QList<D2Crystal> crystals = map.getAll<D2Crystal>();
    QList<D2LandMark> lmarks = map.getAll<D2LandMark>();
    D2Mountains mountains = map.getAll<D2Mountains>()[0];
    QPainter g(&result);
    {
        foreach (D2MapBlock block , blocks)
        {
            int blockX = block.x();
            int blockY = block.y();
            for (int i = 0; i < 32; ++i)
            {
                int x = blockX + i % 8;
                int y = blockY + (int)(i / 8);
                auto value = block.blockData[i];
                const int ground = tileGround(value);
                QColor color;
                if (ground == 3)
                {
                    color = QColor("#336699");
                } else {
                    value = tileTerrain(value);
                    color = colorForRace(value);
                }
                g.fillRect(QRect(tileSize * x, tileSize * y, tileSize, tileSize), color);
            }
        }
        foreach (D2Capital capital , capitals)
        {
            int x = capital.posX;
            int y = capital.posY;

            auto player = map.get<D2Player>(QPair<int, int>(IDataBlock::Player, capital.owner));
            auto race = RESOLVE(DBFModel)->get<Grace>(player.raceId);
            int raceId = shieldByRaceID(race->race_type.key);
            QString key = QString("G000RR%1SHLV8").arg(QString::number(raceId).rightJustified(4,'0'));
            QSharedPointer<ImagesData> images =
                RESOLVE(ResourceManager)->getImagesData("IsoCmon", key, false);
            if (images)
            {
                if (images->data.count() > 0)
                {
                    QRect capitalRect(tileSize * (x), tileSize * (y), tileSize * 5, tileSize * 5);
                    g.drawImage(capitalRect, images->data[0], QRect(0, images->data[0].height() - images->data[0].width(),
                                                                    images->data[0].width(), images->data[0].width()));
                    g.setPen(QPen(QBrush(Qt::black), 2));
                    g.drawEllipse(capitalRect.center(), tileSize * 4, tileSize * 4);
                    g.setPen(QPen(QBrush(Qt::black), 1));
                }
            }
        }
        foreach (D2Village village , villages)
        {
            int x = village.posX;
            int y = village.posY;
            auto player = map.get<D2Player>(village.owner);
            auto race = RESOLVE(DBFModel)->get<Grace>(player.raceId);
            QRect villageRect(tileSize * (x), tileSize * (y), tileSize * 3, tileSize * 3);
            g.fillRect(villageRect, colorForRace(race->race_type.key));
            g.drawEllipse(villageRect.center(), tileSize * 3, tileSize * 3);
        }
        foreach (D2Mountains::Mountain mountain , mountains.mountains)
        {
            int x = mountain.posX;
            int y = mountain.posY;
            QString key = "MOUNTNE" + QString::number(mountain.sizeX);
            QImage raceCap = RESOLVE(ResourceManager)->getImageNoCash(key);
            g.drawImage(QRect(tileSize * x, tileSize * y, tileSize * mountain.sizeX, tileSize * mountain.sizeY), raceCap);
        }
        for (const D2LandMark & lmark : lmarks)
        {
            int x = lmark.posX;
            int y = lmark.posY;
            QSharedPointer<GLmark> mark = RESOLVE(DBFModel)->get<GLmark>(lmark.baseType);
            QRect lmarkRect(tileSize * (x), tileSize * (y), tileSize * mark->cx, tileSize * mark->cy);
            g.fillRect(lmarkRect, QColor("#663300"));
        }
        foreach (D2Ruin ruin , ruins)
        {
            int x = ruin.posX;
            int y = ruin.posY;
            QRect ruinRect(tileSize * x, tileSize * y, tileSize * 3, tileSize * 3);
            g.fillRect(ruinRect, QColor("#FF9933"));
        }
        foreach (D2Stack stack , stacks)
        {
            if (stack.inside.first != -1)
                continue;
            QRect stackRect(tileSize * stack.posX, tileSize * stack.posY, tileSize * 2, tileSize * 2);
            g.fillRect(stackRect, QColor("#990000"));
        }
        // foreach (D2Crystal crystal, crystals)
        // {
        //     int x = crystal.posX;
        //     int y = crystal.posY;
        //     QString key = CrystalObject::CrystalIdByResource((CrystalObject::ResourceType)crystal.resource);
        //     QImage raceCap = RESOLVE(ResourceManager)->getImageNoCash(QStringList()<<"IsoCmon"<<"IsoAnim",
        //                                                              key,
        //                                                              PreprocessingShaderType::TransparentBlackWithSizeOptimisation);
        //     g.drawImage(QRect(tileSize * (x - 3), tileSize * (y - 2), tileSize * 3, tileSize * 6), raceCap);
        // }
    }
    g.end();
    return result;
}

QImage MinimapHelper::miniMap(GameMap &map, QSharedPointer<DBFModel> model)
{
    auto grid = map.getGrid();
    int mapSize = grid.cells.size();
    int tileSize = 4;
    QImage result (mapSize * tileSize, mapSize * tileSize, QImage::Format_ARGB32);
    result.fill(Qt::gray);
    QPainter g(&result);
    for(int x = 0; x < mapSize; ++x)
    {
        for (int y = 0; y < mapSize; ++y)
        {
            auto & cell = grid.cells[x][y];
            auto value = cell.value;
            const int ground = tileGround(value);
            QColor color;
            if (ground == 3)
            {
                color = QColor("#336699");
            } else {
                value = tileTerrain(value);
                color = colorForRace(value);
            }
            g.fillRect(QRect(tileSize * x, tileSize * y, tileSize, tileSize), color);
        }
    }
    auto forts = map.objectsByType<FortObject>();
    for (const auto & fort: forts) {
        if (fort.fortType == FortObject::Capital) {
            QSharedPointer<Grace> race = RESOLVE(DBFModel)->get<Grace>(fort.raceId);
            int raceId = shieldByRaceID(race->race_type.key);
            QString key = QString("G000RR%1SHLV8").arg(QString::number(raceId).rightJustified(4,'0'));
            QSharedPointer<ImagesData> images =
                RESOLVE(ResourceManager)->getImagesData("IsoCmon", key, false);
            if (images)
            {
                if (images->data.count() > 0)
                {
                    QRect capitalRect(tileSize * (fort.x), tileSize * (fort.y), tileSize * 5, tileSize * 5);
                    g.drawImage(capitalRect, images->data[0], QRect(0, images->data[0].height() - images->data[0].width(),
                                                                    images->data[0].width(), images->data[0].width()));
                    g.setPen(QPen(QBrush(Qt::black), 2));
                    g.drawEllipse(capitalRect.center(), tileSize * 4, tileSize * 4);
                    g.setPen(QPen(QBrush(Qt::black), 1));
                }
            }
        }
        else {
            PlayerObject player = map.objectByIdT<PlayerObject>(fort.owner.second);
            QSharedPointer<Grace> race = RESOLVE(DBFModel)->get<Grace>(player.raceId);
            QRect villageRect(tileSize * (fort.x), tileSize * (fort.y), tileSize * 3, tileSize * 3);
            g.fillRect(villageRect, colorForRace(race->race_type.key));
            g.drawEllipse(villageRect.center(), tileSize * 3, tileSize * 3);
        }
    }
    auto mountains = map.objectsByType<MountainObject>();
    for (const MountainObject & mountain : mountains)
    {
        int x = mountain.x;
        int y = mountain.y;
        QString key = "MOUNTNE" + QString::number(mountain.w);
        QImage raceCap = RESOLVE(ResourceManager)->getImageNoCash(key);
        g.drawImage(QRect(tileSize * x, tileSize * y, tileSize * mountain.w, tileSize * mountain.h), raceCap);
    }
    auto lmarks = map.objectsByType<LandmarkObject>();
    for (const LandmarkObject & lmark : lmarks)
    {
        int x = lmark.x;
        int y = lmark.y;
        QSharedPointer<GLmark> mark = RESOLVE(DBFModel)->get<GLmark>(lmark.lmarkId);
        QRect lmarkRect(tileSize * (x), tileSize * (y), tileSize * mark->cx, tileSize * mark->cy);
        g.fillRect(lmarkRect, QColor("#663300"));
    }
    auto stacks = map.objectsByType<StackObject>();
    for (const StackObject & stack : stacks)
    {
        if (stack.inside.first != -1)
            continue;
        QRect stackRect(tileSize * stack.x, tileSize * stack.y, tileSize * 2, tileSize * 2);
        g.fillRect(stackRect, QColor("#990000"));
    }
    auto ruins = map.objectsByType<RuinObject>();
    for (const RuinObject & ruin : ruins)
    {
        QRect ruinRect(tileSize * ruin.x, tileSize * ruin.y, tileSize * 3, tileSize * 3);
        g.fillRect(ruinRect, QColor("#FF9933"));
    }
    g.end();

    return result;
}



QString shortRaceByPlayerId(D2MapModel map, QSharedPointer<DBFModel> model, const QPair<int, int> &id)
{
    auto players = map.getAll<D2Player>();
    foreach (auto player, players)
    {
        if (player.uid == id)
        {
            auto race = model->get<Grace>(player.raceId);
            QString key = race->race_type.value->text;
            return key.replace("L_", "").mid(0, 2);
        }
    }
    return "";
}

QString shortRaceByPlayerId2(const GameMap &map, QSharedPointer<DBFModel> model, const QPair<int, int> &id)
{
    auto player = map.objectByIdT<PlayerObject>(id);
    auto race = model->get<Grace>(player.raceId);
    QString key = race->race_type.value->text;
    return key.replace("L_", "").mid(0, 2);
}

#ifndef MAPGRAPHICSOBJECT_H
#define MAPGRAPHICSOBJECT_H
#include <QGraphicsItem>
#include <QSharedPointer>

class MapGraphicsObject
{
public:
    virtual ~MapGraphicsObject(){}
    virtual void nextFrame(const QRectF &view) = 0;
    virtual void reload() = 0;
    virtual QGraphicsItem * item() = 0;

    virtual QSharedPointer<MapObject> getObject() const = 0;
    virtual void setOption(const QString& name, const QString& value) = 0;
};

#endif // MAPGRAPHICSOBJECT_H

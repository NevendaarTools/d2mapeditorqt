#include "CustomMapObject.h"
#include "Engine/GameInstance.h"
#include "Commands/MenuCommands.h"
#include "Commands/EditCommands.h"
#include <QMenu>
#include "Events/MapEvents.h"
#include "Engine/Map/MapStateHolder.h"
#include <QPainter>

CustomMapObject::CustomMapObject(QSharedPointer<MapObject> object)
    : m_object(object)
{
    setAcceptHoverEvents(false);
    m_hovered = false;
    m_menuOpened = false;
    m_object = object;
    m_initialised = false;
    m_rect = QRect(0,0,0,0);
}

QRectF CustomMapObject::boundingRect() const
{
    return m_rect;
}

QPainterPath CustomMapObject::shape() const
{
    QPainterPath path;
    path.addPolygon(m_polygon);
    return path;
}

QRect findBoundingRect(const QList<QRect>& rects)
{
    if (rects.isEmpty())
        return QRect();

    int left = rects[0].left();
    int top = rects[0].top();
    int right = rects[0].right();
    int bottom = rects[0].bottom();

    for (const QRect& rect : rects)
    {
        left = qMin(left, rect.left());
        top = qMin(top, rect.top());
        right = qMax(right, rect.right());
        bottom = qMax(bottom, rect.bottom());
    }

    return QRect(QPoint(left, top), QPoint(right, bottom));
}

void CustomMapObject::paint(QPainter *painter, const QStyleOptionGraphicsItem *item, QWidget *widget)
{
    if (!m_initialised)
        initImages();
    for(int i = 0; i < m_frameData.count(); ++i)
    {
        FrameData & data = m_frameData[i];
        if (data.images->status != ImagesData::Ready)
            continue;
        if (data.rects.count() < data.images->data.count())
            continue;
        if (data.images->data.count() > data.currentFrame)
            painter->drawImage(data.rects[data.currentFrame],
                                data.images->data[data.currentFrame],
                                data.images->data[data.currentFrame].rect());
    }
    if (m_hovered || m_menuOpened)
    {
        painter->setPen(QPen(Qt::yellow, 3));
        painter->drawPolygon(m_polygon);
        painter->setPen(QPen(Qt::red, 1));
        painter->drawPolygon(m_polygon);

        //painter->drawRect(imRect);
    }
}

void CustomMapObject::nextFrame(const QRectF &view)
{
    bool neadUpdate = false;
    for(int i = 0; i < m_frameData.count(); ++i)
    {
        if (m_frameData[i].nextFrame())
            neadUpdate = true;
    }
    if (m_rect.width() == 0)
    {
        prepareGeometryChange();
        QPointF centerPos = cartesianToIsometric(m_accessor->getW(m_object) / 2.0 * TILE_SIZE, m_accessor->getH(m_object) / 2.0 * TILE_SIZE);
        QList<QRect> total;
        for(int i = 0; i < m_frameData.count(); ++i)
        {
            FrameData & data = m_frameData[i];
            if (data.images->status != ImagesData::Ready){
                total.clear();
                break;
            }
            if (data.rects.count() < data.images->data.count())
            {
                data.rects.clear();
                for (int k = 0; k < data.images->data.count(); ++k)
                {
                    const QImage pixmap = data.images->data[k];
                    int dx = centerPos.x() - pixmap.width() / 2;
                    int dy = centerPos.y() - pixmap.height() / 2;
                    data.rects << QRect(dx, dy, pixmap.width(), pixmap.height());
                }
                total << findBoundingRect(data.rects);
            }
        }
        if (total.count() > 0)
            m_rect = findBoundingRect(total);
    }
    if (neadUpdate)
        update(boundingRect());
}

void CustomMapObject::reload()
{
    auto map = GameInstance::instance()->get<MapStateHolder>();
    m_object = map->objectById(m_object->uid);
    if (m_object.isNull())
    {
        qDebug()<<"Null object";
        m_object = map->objectById(m_object->uid);
        return;
    }
    m_accessor = RESOLVE(AccessorHolder)->objectAccessor(m_object->uid.first);
    setZValue(m_accessor->getZ(m_object) + m_object->y + m_object->x +
                                          m_accessor->getH(m_object));
    m_polygon.clear();
    m_polygon<<QPointF(0, 0);
    m_polygon<< cartesianToIsometric((m_accessor->getW(m_object)) * TILE_SIZE, 0);
    m_polygon<< cartesianToIsometric(( m_accessor->getW(m_object)) * TILE_SIZE, (m_accessor->getH(m_object)) * TILE_SIZE);
    m_polygon<< cartesianToIsometric(0, (m_accessor->getH(m_object)) * TILE_SIZE);
    m_rect = m_polygon.boundingRect().toRect();
    QPointF pos = cartesianToIsometric(m_object->x * TILE_SIZE, m_object->y * TILE_SIZE);

    int x = pos.x();
    int y = pos.y();//TILE_SIZE * object->getH() / 2 + pos.y();
    setPos(QPointF(x, y));
    m_initialised = false;
    m_rect = QRect(0,0,0,0);
    initImages();
    update();
}

void CustomMapObject::hoverEnterEvent(QGraphicsSceneHoverEvent *event)
{
    m_hovered = true;
    update(boundingRect());
    event->ignore();
    HoveredObjectChangedEvent command;
    command.hover_in = m_object->uid;
    command.hover_out = EMPTY_ID;
    RESOLVE(EventBus)->notify(command);
}

void CustomMapObject::hoverMoveEvent(QGraphicsSceneHoverEvent *event)
{
    QPoint old = cursorePos;
    cursorePos = mapPos(event->pos());
    if (old !=cursorePos)
    {
        RESOLVE(EventBus)->notify(HoverChangedEvent(cursorePos));
    }
    event->ignore();
    //QGraphicsItem::hoverMoveEvent(event);
}

void CustomMapObject::mouseDoubleClickEvent(QGraphicsSceneMouseEvent *event)
{
    RESOLVE(CommandBus)->execute(EditCommand(m_object->uid));
}

void CustomMapObject::hoverLeaveEvent(QGraphicsSceneHoverEvent *event)
{
    m_hovered = false;
    HoveredObjectChangedEvent command;
    command.hover_in = EMPTY_ID;
    command.hover_out = m_object->uid;
    RESOLVE(EventBus)->notify(command);
    update(boundingRect());
    event->ignore();

    //QGraphicsItem::hoverLeaveEvent(event);
}

void CustomMapObject::mousePressEvent(QGraphicsSceneMouseEvent *event)
{
    QGraphicsItem::mousePressEvent(event);
    //update();
}

void CustomMapObject::contextMenuEvent(QGraphicsSceneContextMenuEvent *event)
{
    if (!m_menuEnabled)
        return;
    QMenu * menu = m_accessor->requestMenu(m_object);
    if (menu!= nullptr)
    {
    }
    else
        menu = new QMenu();

    QAction * editAction = menu->addAction("Edit");
    editAction->setData(QVariant::fromValue(EditCommand(m_object->uid)));
    m_menuOpened = true;
    QAction *selectedAction = menu->exec(event->screenPos());
    if ( selectedAction != nullptr)
    {
        RESOLVE(CommandBus)->execute(selectedAction->data());
    }
    else
    {
        qDebug()<<"else";
    }
    m_menuOpened = false;
}

QPoint CustomMapObject::mapPos(const QPointF &pos) const
{
    QPointF pos2 = isometricToCartesian(pos.x(), pos.y());
    QPoint result;
    result.setX(ceil(pos2.x() / TILE_SIZE ) + m_object->x - 1);
    result.setY(ceil(pos2.y() / TILE_SIZE ) + m_object->y - 1);
    if (result.x() < 0)
        result.setX(0);
    if (result.y() < 0)
        result.setY(0);
    return result;
}

void CustomMapObject::initImages()
{
    if (m_initialised)
        return;
    m_frameData = m_accessor->frameData(m_object);
    m_initialised = true;
}

bool CustomMapObject::getMenuEnabled() const
{
    return m_menuEnabled;
}

void CustomMapObject::setMenuEnabled(bool menuEnabled)
{
    m_menuEnabled = menuEnabled;
}

void CustomMapObject::setOption(const QString &name, const QString &value)
{
    if (name == "enable_object_menu")
    {
        setMenuEnabled(value == "True");
    }
}

#ifndef LANDSCAPEOBJECT_H
#define LANDSCAPEOBJECT_H
#include <QColor>
#include <QGraphicsItem>
#include "toolsqt/ResourceModel/ResourceModel.h"
#include "Engine/Components/ResourceManager.h"
#include "toolsqt/DBFModel/DBFModel.h"
#include "toolsqt/MapUtils/MapGrid.h"
#include "Commands/CommandBus.h"
#include "Events/EventBus.h"
#define EXTRA_OFFSET TILE_SIZE * 2
#define DISPLAY_TILE_SIZE 256

class TileItem: public QGraphicsPixmapItem
{
public:
    void init(int tileX, int tileY, int w)
    {
        m_tileX = tileX;
        m_tileY = tileY;
        setPos(tileX * DISPLAY_TILE_SIZE - EXTRA_OFFSET -w/2, tileY * DISPLAY_TILE_SIZE - EXTRA_OFFSET);
    }
    void reload(QImage & image)
    {
        QRect tileRect(m_tileX * DISPLAY_TILE_SIZE, m_tileY * DISPLAY_TILE_SIZE,
                       DISPLAY_TILE_SIZE, DISPLAY_TILE_SIZE);
        QPixmap tilePixmap = QPixmap::fromImage(image.copy(tileRect));
        setPixmap(tilePixmap);
        m_loaded = true;
        update();
    }

    bool loaded() const
    {
        return m_loaded;
    }

private:
    int m_tileX;
    int m_tileY;
    bool m_loaded = false;
};

class LandscapeObject : public QGraphicsItem, public EventSubscriber
{
public:
    LandscapeObject();

    void reloadGrid();
    void setup(MapGrid grid);
    QRectF boundingRect() const override;
    QPainterPath shape() const override;
    void paint(QPainter *painter, const QStyleOptionGraphicsItem *item, QWidget *widget) override;
    void onGridUpdate(const QVariant &eventVar);
    void onLayersSettingsUpdate(const QVariant &eventVar);
private:
    QPoint mapPos(const QPointF &pos) const;
    int evaluateBorder(int x, int y) const;
    int evaluateExtraBorder(int x, int y, int border) const;
    void drawCell(int x, int y);
    void drawRoad(int x, int y);
    void drawRoads();
    void drawTrees();
    void drawTree(int x, int y);
    void clearItemsGrid();
    void addTilesFromCell(int x, int y, QVector<TileItem*> & tiles);
    TileItem* tileByPos(int x, int y);
private:
    QRect rect;
    QPainterPath m_shape;
    MapGrid m_grid;
    QImage img;
    bool m_simpleView = false;
    QList<QGraphicsPixmapItem*> m_items;
    QVector<QVector<TileItem*>> m_itemsGrid;
};

#endif // LANDSCAPEOBJECT_H

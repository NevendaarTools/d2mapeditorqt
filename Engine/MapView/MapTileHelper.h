#ifndef MAPTILEHELPER_H
#define MAPTILEHELPER_H
#include <QVector>
#include <QPolygon>
#include <QHash>
#include <QMap>
#include <QImage>
#include <random>
#include <qdebug.h>

class MapRegionExtractor {
public:
    MapRegionExtractor(int seed = 0)
        : m_seed(seed), m_rng(seed)
    {

    }

    QImage extractRegion(int x, int y)
    {
        int resKey = 1000000 * x + y;
        if (hashMap.contains(resKey))
            return hashMap[resKey];
        const QSize regionSize(64, 32);
        int roundX = x % m_tileSize;
        int roundY = y % m_tileSize;
        QPoint position = QPoint(roundX, roundY);

        QImage region(regionSize, QImage::Format_ARGB32);
        QPoint topLeftTile = QPoint(position.x() / m_tileSize, position.y() / m_tileSize);
        QPoint bottomRightTile = QPoint((position.x() + regionSize.width() - 1) / m_tileSize,
                                        (position.y() + regionSize.height() - 1) / m_tileSize);

        for (int tileY = topLeftTile.y(); tileY <= bottomRightTile.y(); ++tileY) {
            for (int tileX = topLeftTile.x(); tileX <= bottomRightTile.x(); ++tileX) {
                QPoint tileOrigin(tileX * m_tileSize, tileY * m_tileSize);
                QRect tileRect = getTileRectInRegion(tileOrigin, position, regionSize);

                QImage tile = getTileAt(tileX, tileY);
                QPoint copyStartPoint = tileRect.topLeft() - tileOrigin;

                copyTileToRegion(region, tile, tileRect, copyStartPoint, position);
            }
        }
        hashMap[resKey] = region;
        return region;
    }
    void addImage(const QImage & im)
    {
        m_tiles << im;
        m_tileSize = im.width();
    }
private:
    QRect getTileRectInRegion(const QPoint& tileOrigin, const QPoint& regionTopLeft, const QSize& regionSize) const {
        int left = std::max(tileOrigin.x(), regionTopLeft.x());
        int top = std::max(tileOrigin.y(), regionTopLeft.y());
        int right = std::min(tileOrigin.x() + m_tileSize, regionTopLeft.x() + regionSize.width());
        int bottom = std::min(tileOrigin.y() + m_tileSize, regionTopLeft.y() + regionSize.height());

        return QRect(left, top, right - left, bottom - top);
    }

    void copyTileToRegion(QImage& region, const QImage& tile, const QRect& tileRect, const QPoint& copyStartPoint, const QPoint& regionTopLeft) const {
        QPoint regionStartPoint = tileRect.topLeft() - regionTopLeft;
        for (int y = 0; y < tileRect.height(); ++y) {
            memcpy(region.scanLine(regionStartPoint.y() + y) + (regionStartPoint.x() * 4),
                   tile.scanLine(copyStartPoint.y() + y) + (copyStartPoint.x() * 4),
                   tileRect.width() * 4);
        }
    }

    QImage getTileAt(int tileX, int tileY) const {
        int tileIndex = calculateTileIndex(tileX, tileY);
        return m_tiles[tileIndex];
    }

    int calculateTileIndex(int tileX, int tileY) const {
        if (m_tiles.count() == 1)
            return 0;
        if (m_tiles.count() == 0)
        {
            qDebug()<<"empty tile type";
            return 0;
        }
        std::mt19937 rng(m_seed + tileX * 73856093 + tileY * 19349663);
        std::uniform_int_distribution<int> dist(0, m_tiles.size() - 1);
        return dist(rng);
    }

    QList<QImage> m_tiles;
    int m_seed = 0;
    int m_tileSize = 192;
    mutable std::mt19937 m_rng;
    QMap<int, QImage> hashMap;
};

class MapTileHelper
{
public:
    struct ImageLayer
    {
        QImage image;
        QImage mask;
    };
    struct LayerHelper
    {
        void addLayer(ImageLayer layer)
        {
            layers.prepend(layer);
        }
        void addLayer(QImage image)
        {
            ImageLayer layer;
            layer.image = image;
            layer.mask = image;
            layers.append(layer);
        }
        void addLayer(QImage image, QImage mask)
        {
            ImageLayer layer;
            layer.image = image;
            layer.mask = mask;
            layers.append(layer);
        }
        QList<ImageLayer> layers;
    };
    static QImage mainImage(int value, int x, int y, int cellsCount);
    static QImage waterBorder(int x, int y, int type);
    static QImage createTile(const LayerHelper &layers);

    static void drawTile(const LayerHelper &layers, uchar* resultBits, uint startIndex, uint w);
    static void drawImage(const QImage &image, uchar* resultBits, uint startIndex, uint w);
    static const QImage &tileByValue(int value);
    static QImage & roadImage(int value);
    static const QImage &forestByValue(int value);
    static void init();

    static const QImage &getBlendMask(int x, int y, int type);

    const QImage &diamond() const;
private:
    static MapTileHelper* instance() {
        static MapTileHelper inst;
        return &inst;
    }
    const QImage &getEmpty() const;
    MapTileHelper();
    QImage createDiamond();
    static const QImage &groundByValue(int value);
private:
    QHash<int, QImage> m_blendedBorderHash;
    QImage m_diamond;
    QImage m_empty;

    QHash<int, QImage> m_roadImages;
    QHash<int, QImage> m_tileHash;
    QMultiHash<int, QImage> m_borderImages;
    QMultiHash<int, QImage> m_borderMasks;

    QMap<int, QImage> tilesMap;
    QMap<int, QImage> groundMap;
    QMap<int, QString> raceMap;
    QMap<int, QImage> treeMap;

    QMap<int, MapRegionExtractor> extractors;
    MapRegionExtractor waterExtractor;

    bool m_initialised = false;
};

#endif // MAPTILEHELPER_H

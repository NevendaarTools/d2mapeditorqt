#ifndef MAPVIEW_H
#define MAPVIEW_H

#include <QFrame>
#include <QGraphicsView>
#include <QSlider>
#include "QMLHelpers/MainToolBarController.h"
#include <QQuickWidget>
#include <QFrame>
#include "Commands/EditCommands.h"
#include "Events/MapEvents.h"
#include "MapObjects/GameMap.h"
#include "MapObjects/GridObject.h"
#include <QTimer>
#include "QMLHelpers/Editors/EditorWidget.h"
#include "QMLHelpers/Common/StyleManager.h"
#include "Engine/MapView/LandscapeObject.h"
#include "Engine/MapView/MapGraphicsObject.h"
#include "Events/EventBus.h"
#include "Events/MapEvents.h"
#include "Engine/GameInstance.h"
#include "HoverHighlight.h"

#define MOUSE_EMULATED_LEFT 0xfe000001

class View;
class CustomScene : public QGraphicsScene {
    Q_OBJECT

public:
    CustomScene(QObject *parent = nullptr) : QGraphicsScene(parent) {
    }

    ToolsProvider *tools() const
    {
        return m_tools;
    }
    void setTools(ToolsProvider *newTools)
    {
        m_tools = newTools;
    }

protected:
    QPoint mapPos(const QPointF &pos) const;
    void mousePressEvent(QGraphicsSceneMouseEvent *event) override;
    void mouseMoveEvent(QGraphicsSceneMouseEvent *event) override;

    void mouseReleaseEvent(QGraphicsSceneMouseEvent *event) override;
signals:
    void keyPressed(QKeyEvent *event);
private:
    bool _mousePressed = false;
    QPointF _lastMousePos;
    ToolsProvider * m_tools = nullptr;
    QPoint  m_cursorePos;
};

class GraphicsView : public QGraphicsView
{
    Q_OBJECT
public:
    GraphicsView(View *v) : QGraphicsView(), view(v)
    {
        //installEventFilter(this);
    }

    void mouseMoveEvent(QMouseEvent *event) override
    {
        //neaded to catch mouse move event in SceneItems while mouse pressed
        QMouseEvent customEvent(event->type(), event->pos(), Qt::NoButton, Qt::NoButton, event->modifiers());
        QGraphicsView::mouseMoveEvent(&customEvent);
    }
    View *getView() const;
    void keyPressEvent(QKeyEvent *event) override;
protected:
#if QT_CONFIG(wheelevent)
    void wheelEvent(QWheelEvent *) override;
#endif

private:
    View *view;
    QPointF m_mapMove;
};

class View : public QFrame,
        public CommandExecutor, public EventSubscriber
{
    Q_OBJECT
public:
    explicit View (QWidget *parent = nullptr);

    CommandResult executeRemoveObjectCommand(const QVariant &commandVar);
    CommandResult executeFocusOnCommand(const QVariant &commandVar);

    QGraphicsView *view() const;

    void resizeEvent(QResizeEvent *event) override;
    void setMap(const GameMap &map);
    void closeMap();

    void onObjectAdded(const QVariant &eventVar);
    void onObjectUpdated(const QVariant &event);
    void onObjectRemoved(const QVariant &event);
    void onLayersSettingsUpdate(const QVariant &eventVar);
    void onSettingsUpdate(const QVariant &eventVar);
public slots:
    void zoomIn(int level = 1);
    void zoomOut(int level = 1);
    void onTimer();
private slots:
    void resetView();
    void setupMatrix();
private:
    void initToolbar();
private:
    CustomScene *m_scene;
    GraphicsView *m_graphicsView;

    QQuickWidget* m_toolbar;
    MainToolBarController * m_toolbarProvider;
    GameMap m_map;
    EditorWidget * m_editor;

    QList<QSharedPointer<MapGraphicsObject>> m_objects;
    LandscapeObject * m_landscape;
    HoverHighlightItem *m_hoverItem;
    GridView * m_grid;
    QTimer m_timer;
    int m_zoomValue = 250;
    bool m_edgeMove = true;
    ToolsProvider * m_tools = nullptr;
    StyleManager * m_style;
};

#endif // MAPVIEW_H

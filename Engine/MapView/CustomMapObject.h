#ifndef CUSTOMMAPOBJECT_H
#define CUSTOMMAPOBJECT_H
#include <QColor>
#include <QGraphicsItem>
#include "toolsqt/ResourceModel/ResourceModel.h"
#include "toolsqt/DBFModel/DBFModel.h"
#include "MapObjects/MapObject.h"
#include <QGraphicsSceneMouseEvent>
#include "Commands/CommandBus.h"
#include "Events/EventBus.h"
#include "MapGraphicsObject.h"
#include "Engine/Components/ResourceManager.h"
#include "Engine/Components/AccessorHolder.h"

class CustomMapObject : public QObject, public QGraphicsItem, public MapGraphicsObject
{
     Q_OBJECT
public:
    CustomMapObject(QSharedPointer<MapObject> object);
    QRectF boundingRect() const override;
    QPainterPath shape() const override;
    void paint(QPainter *painter, const QStyleOptionGraphicsItem *item, QWidget *widget) override;
    virtual void nextFrame(const QRectF &view) override;
    virtual void reload() override;
    virtual void mousePressEvent(QGraphicsSceneMouseEvent *event) override;
    virtual void hoverEnterEvent(QGraphicsSceneHoverEvent *event) override;
    virtual void hoverLeaveEvent(QGraphicsSceneHoverEvent *event) override;
    virtual void hoverMoveEvent(QGraphicsSceneHoverEvent *event) override;
    virtual void mouseDoubleClickEvent(QGraphicsSceneMouseEvent *event) override;

    virtual QGraphicsItem * item() {return this;}
    virtual QSharedPointer<MapObject> getObject() const {return m_object;}

    void contextMenuEvent(QGraphicsSceneContextMenuEvent *event) override;
    bool getMenuEnabled() const;
    void setMenuEnabled(bool menuEnabled);
    void setOption(const QString &name, const QString &value);

private:
    QPoint mapPos(const QPointF& pos) const;
    void initImages();
private:
    QList<FrameData> m_frameData;
    QSharedPointer<MapObject> m_object;
    QSharedPointer<IMapObjectAccessor> m_accessor;

    QRect m_rect;
    QPolygonF m_polygon;
    bool m_hovered;
    bool m_menuOpened;
    QPoint cursorePos;
    bool m_menuEnabled = false;

    bool m_initialised = false;
};

#endif // CUSTOMMAPOBJECT_H

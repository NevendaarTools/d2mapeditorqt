#include "HoverHighlight.h"
#include "Engine/GameInstance.h"
#include "Events/MapEvents.h"
#include <QPen>
#include <QBrush>

void HoverHighlightItem::drawImage(QPainter *painter, FrameData &data, const QPointF &pos)
{
    if (data.images->status != ImagesData::Ready)
        return;
    if (data.rects.count() < data.images->data.count())
    {
        data.rects.clear();
        for (int k = 0; k < data.images->data.count(); ++k)
        {
            const QImage pixmap = data.images->data[k];
            int dx =  - pixmap.width();
            int dy =  - pixmap.height() + TILE_SIZE;
            data.rects << QRect(dx/2, dy/2, pixmap.width(), pixmap.height());
        }
    }
    QRect resRect = data.rects[data.currentFrame];
    resRect.translate(pos.toPoint());
    if (data.images->data.count() > data.currentFrame)
        painter->drawImage(resRect,
                           data.images->data[data.currentFrame],
                           data.images->data[data.currentFrame].rect());
}

HoverHighlightItem::HoverHighlightItem(QGraphicsItem *parent)
    : QGraphicsItem(parent)
{
    subscribe<HoverChangedEvent>([this](const QVariant &event){onHoverChanged(event);});
    setZValue(9999);
    m_bounds = QRectF(-64, -16, 128, 64);
}

void HoverHighlightItem::init()
{
    QStringList ffsource = QStringList()<<"IsoCmon"<<"IsoAnim";
    m_selectedTile.images = RESOLVE(ResourceManager)->getImagesData(ffsource, "TILE_HIGHLIGHT");
    m_loaded = true;
}

QRectF HoverHighlightItem::boundingRect() const {
    return m_bounds;
}


void HoverHighlightItem::paint(QPainter *painter, const QStyleOptionGraphicsItem *option, QWidget *widget)
{
    drawImage(painter, m_selectedTile, QPoint(0, 0));
    // painter->drawRect(boundingRect());
}

void HoverHighlightItem::nextFrame(const QRectF &view)
{
    bool neadUpdate = false;
    if (m_selectedTile.nextFrame())
        neadUpdate = true;
    if (neadUpdate)
        update(boundingRect());
}

void HoverHighlightItem::onHoverChanged(const QVariant &eventVar)
{
    HoverChangedEvent event = eventVar.value<HoverChangedEvent>();
    QPointF pos = cartesianToIsometric(event.pos.x() * TILE_SIZE, event.pos.y() * TILE_SIZE);
    setPos(pos);
    update();
}

#include "LandscapeObject.h"
#include <QGraphicsSceneMouseEvent>
#include <QElapsedTimer>
#include "Events/MapEvents.h"
#include "Commands/UIEvents.h"
#include "MapTileHelper.h"
#include <QtConcurrent/QtConcurrent>
#include <QGraphicsScene>
#include <QPolygonF>
#include "Common/Profiling.h"
#include "Engine/GameInstance.h"
#include "Engine/Map/MapStateHolder.h"
#include "Engine/Components/DisplaySettingsManager.h"

LandscapeObject::LandscapeObject()
{
    setFlags(flags() & ~ItemIsSelectable & ~ItemIsMovable);
    setAcceptHoverEvents(true);
    setAcceptedMouseButtons(Qt::AllButtons);
    subscribe<GridChangedEvent>([this](const QVariant &event){onGridUpdate(event);});
    subscribe<DisplaySettingsChangedEvent>([this](const QVariant &event){onLayersSettingsUpdate(event);});
}

void LandscapeObject::reloadGrid()
{
    QElapsedTimer timer;
    timer.start();
    setZValue(-10);
    int h = m_grid.cells.count() * TILE_SIZE;
    int w = m_grid.cells.count() * TILE_SIZE * 2;
    img = QImage(w + EXTRA_OFFSET * 2, h + EXTRA_OFFSET * 2, QImage::Format_ARGB32);
    img.fill(Qt::gray);
    rect = img.rect();

    QPolygonF mapShape;
    mapShape.append(QPointF(w/2, 0));
    mapShape.append(QPointF(0, h /2));
    mapShape.append(QPointF(w/2,h));
    mapShape.append(QPointF(w, h / 2));
    m_shape.clear();
    m_shape.addPolygon(mapShape);

    qDebug()<<w<<" - "<<h;
    qDebug()<<"Map shape"<<m_shape;
    for (int k = 0; k < m_grid.cells.count(); ++k) {
            for (int i = 0; i < m_grid.cells.count(); i++)
                this->drawCell(i, k);
    }

    drawRoads();
    drawTrees();
    update();
    clearItemsGrid();
    QVector<TileItem*> tiles;
    for (int k = 0; k < m_grid.cells.count(); ++k) {
        for (int i = 0; i < m_grid.cells.count(); i++)
        {
            addTilesFromCell(k,i, tiles);
        }
    }
    for(auto tile:tiles)
    {
        if (!tile->loaded())
            tile->reload(img);
    }
    qDebug()<<"Image creation tooks:"<<timer.elapsed();
}

void LandscapeObject::setup(MapGrid grid)
{
    m_grid = grid;
    MapTileHelper::init();
    reloadGrid();
}

QRectF LandscapeObject::boundingRect() const
{
    return rect;
}

QPainterPath LandscapeObject::shape() const
{
    return m_shape;
}

void LandscapeObject::paint(QPainter *painter, const QStyleOptionGraphicsItem *item, QWidget *widget)
{
    // painter->drawImage(rect, img, img.rect());

}

void LandscapeObject::onGridUpdate(const QVariant &eventVar)
{
    Q_UNUSED(eventVar)
    MapGrid newGrid = RESOLVE(MapStateHolder)->getGrid();
    MapGrid old = m_grid;
    m_grid = newGrid;
    QList<QPoint> points;

    if (newGrid.cells.count() != old.cells.count())
    {
        reloadGrid();
        return;
    }
    int minX = old.cells.count(), minY = old.cells.count(),
        maxX = 0, maxY = 0;
    for (int k = 0; k < m_grid.cells.count(); ++k)
    {
        for (int i = 0; i < m_grid.cells.count(); i++)
        {
            if (newGrid.cells[i][k].value != old.cells[i][k].value ||
                newGrid.cells[i][k].roadType != old.cells[i][k].roadType ||
                newGrid.cells[i][k].roadVar != old.cells[i][k].roadVar)
            {
                if (i < minX)
                    minX = i;
                if (i > maxX)
                    maxX = i;
                if (k < minY)
                    minY = k;
                if (k > maxY)
                    maxY = k;
                points << QPoint (i, k);
            }
        }
    }

    minX = qMax(minX - 3, 0);
    minY = qMax(minY - 3, 0);
    maxX = qMin(maxX + 3, old.cells.count() - 3);
    maxY = qMin(maxY + 3, old.cells.count() - 3);

    QVector<TileItem*> tiles;

    for (int i = minX; i < maxX; ++i)
    {
        for (int k = minY; k < maxY; ++k)
        {
            drawCell(i, k);
            drawRoad(i, k);
            addTilesFromCell(i,k, tiles);
        }
    }

//    for (int i = minX; i < maxX; ++i)
//    {
//        for (int k = minY; k < maxY; ++k)
//        {
//            drawTree(i, k);
//        }
//    }
    drawTrees();
    for(auto tile:tiles)
    {
        tile->reload(img);
    }
}

void LandscapeObject::onLayersSettingsUpdate(const QVariant &eventVar)
{
    Q_UNUSED(eventVar)
    if (m_simpleView != RESOLVE(DisplaySettingsManager)->settings.simpleView)
    {
        m_simpleView = RESOLVE(DisplaySettingsManager)->settings.simpleView;
        reloadGrid();
    }
}

int LandscapeObject::evaluateBorder(int x, int y) const
{
    uint8_t border = 0;
    if (x > 0 && m_grid.cells[x][y].value != m_grid.cells[x - 1][y].value)
        border |= 1 << 0;
    if (y > 0 && m_grid.cells[x][y].value != m_grid.cells[x][y - 1].value)
        border |= 1 << 1;
    if (x < m_grid.cells.count() -1 && m_grid.cells[x][y].value != m_grid.cells[x + 1][y].value)
        border |= 1 << 2;
    if (y < m_grid.cells[0].count() -1 && m_grid.cells[x][y].value != m_grid.cells[x][y + 1].value)
        border |= 1 << 3;
    return border;
}

int LandscapeObject::evaluateExtraBorder(int x, int y, int border) const
{
    uint8_t extraBorder = 0;
    if ((x > 0) && (y > 0) &&
        m_grid.cells[x][y].value != m_grid.cells[x - 1][y - 1].value &&
        ((border & 0b11) == 0))
        extraBorder |= 1 << 0;
    if ((x < m_grid.cells.count() -1) && (y > 0)  &&
        m_grid.cells[x][y].value != m_grid.cells[x + 1][y - 1].value &&
        ((border & 0b110) == 0))
        extraBorder |= 1 << 1;
    if ((x < m_grid.cells.count() -1) && (y < m_grid.cells[0].count() -1) &&
        m_grid.cells[x][y].value != m_grid.cells[x + 1][y + 1].value &&
        ((border & 0b1100) == 0))
        extraBorder |= 1 << 2;
    if ((x > 0) && (y < m_grid.cells[0].count() -1) &&
        m_grid.cells[x][y].value != m_grid.cells[x - 1][y + 1].value &&
        ((border & 0b1001) == 0))
        extraBorder |= 1 << 3;
    return extraBorder;
}

inline bool test(const MapCell & val1, const MapCell & val2)
{
    if (val2.isWater())
        return false;
    if (val1.isWater())
        return true;

    const int terr1 = tileTerrain(val1.value);
    const int terr2 = tileTerrain(val2.value);
    //dwarfs and empire ignore terrain
    if (terr1 == 2 && terr2 == 1)
        return false;
    if (terr1 == 1 && terr2 == 2)
        return true;
    return terr1 > terr2;
}

void LandscapeObject::drawCell(int x, int y)
{
    MapTileHelper::LayerHelper helper;
    helper.addLayer(MapTileHelper::mainImage(m_grid.cells[x][y].value, x, y, m_grid.cells.count()));
    int border = evaluateBorder(x, y);
    int extraBorder = evaluateExtraBorder(x, y, border);

    if (border != 0 && !m_simpleView)
    {
        if (m_grid.cells[x][y].isWater())
        {
            helper.addLayer(MapTileHelper::waterBorder(x, y, border));
        }

        if (x > 0 &&
            test(m_grid.cells[x][y], m_grid.cells[x - 1][y]))
        {
            helper.addLayer(MapTileHelper::mainImage(m_grid.cells[x - 1][y].value, x - 1, y, m_grid.cells.count()),
                            MapTileHelper::getBlendMask(x, y, 1));
        }
        if (y > 0 &&
            test(m_grid.cells[x][y], m_grid.cells[x][y - 1]))
        {
            helper.addLayer(MapTileHelper::mainImage(m_grid.cells[x][y - 1].value, x, y - 1, m_grid.cells.count()),
                            MapTileHelper::getBlendMask(x, y, 2));
        }
        if (x < m_grid.cells.count() - 1 &&
            test(m_grid.cells[x][y], m_grid.cells[x + 1][y]))
        {
            helper.addLayer(MapTileHelper::mainImage(m_grid.cells[x + 1][y].value, x + 1, y, m_grid.cells.count()),
                            MapTileHelper::getBlendMask(x, y, 4));
        }
        if (y < m_grid.cells[0].count() - 1 &&
            test(m_grid.cells[x][y], m_grid.cells[x][y + 1]))
        {
            helper.addLayer(MapTileHelper::mainImage(m_grid.cells[x][y + 1].value, x, y + 1, m_grid.cells.count()),
                            MapTileHelper::getBlendMask(x, y, 8));
        }
    }
    if (extraBorder != 0 && !m_simpleView)
    {
        if (m_grid.cells[x][y].isWater())
        {
            helper.addLayer(MapTileHelper::waterBorder(x, y, extraBorder + 16));
        }
        if (x > 0 && y > 0 &&
            test(m_grid.cells[x][y], m_grid.cells[x - 1][y - 1]))
        {
            helper.addLayer(MapTileHelper::mainImage(m_grid.cells[x - 1][y - 1].value, x - 1, y - 1, m_grid.cells.count()),
                            MapTileHelper::getBlendMask(x, y, 17));
        }
        if (x < m_grid.cells.count() - 1 && y > 0 &&
            test(m_grid.cells[x][y], m_grid.cells[x + 1][y - 1]))
        {
            helper.addLayer(MapTileHelper::mainImage(m_grid.cells[x + 1][y - 1].value, x + 1, y - 1, m_grid.cells.count()),
                            MapTileHelper::getBlendMask(x, y, 18));
        }
        if (x < m_grid.cells.count() - 1 && y < m_grid.cells[0].count() - 1 &&
            test(m_grid.cells[x][y], m_grid.cells[x + 1][y + 1]))
        {
            helper.addLayer(MapTileHelper::mainImage(m_grid.cells[x + 1][y + 1].value, x + 1, y + 1, m_grid.cells.count()),
                            MapTileHelper::getBlendMask(x, y, 20));
        }
        if (x > 0 && y < m_grid.cells[0].count() - 1 &&
            test(m_grid.cells[x][y], m_grid.cells[x - 1][y + 1]))
        {
            helper.addLayer(MapTileHelper::mainImage(m_grid.cells[x - 1][y + 1].value, x - 1, y + 1, m_grid.cells.count()),
                            MapTileHelper::getBlendMask(x, y, 24));
        }
    }
    int newX = getXOffset(x * TILE_SIZE, y * TILE_SIZE, m_grid.cells.count() * TILE_SIZE * 2, TILE_SIZE);
    int newY = getYOffset(x * TILE_SIZE, y * TILE_SIZE);
    newX += EXTRA_OFFSET;
    newY += EXTRA_OFFSET;
    int imW = img.width() * 4;
    MapTileHelper::drawTile(helper, img.bits(), newX * 4 + newY * imW, imW);
}


void LandscapeObject::drawRoad(int x, int y)
{
    if (m_grid.cells[x][y].roadType != -1)
    {
        int w = m_grid.cells.count() * TILE_SIZE * 2;
        int imW = img.width() * 4;

        int newX = getXOffset(x * TILE_SIZE, y * TILE_SIZE, w, TILE_SIZE);
        int newY = getYOffset(x * TILE_SIZE, y * TILE_SIZE);
        newX += EXTRA_OFFSET;
        newY += EXTRA_OFFSET;

        auto road = MapTileHelper::roadImage(m_grid.cells[x][y].roadType);
        int dx =  road.width() / 2;
        int dy =  road.height() / 2;
        int imX = newX + TILE_SIZE - dx;
        int imY = newY + TILE_SIZE / 2 - dy;
        MapTileHelper::drawImage(road, img.bits(), imX * 4 + imY * imW , imW);
    }
}

void LandscapeObject::drawRoads()
{
//    QList<QFuture<void>> futures;

//    int w = m_grid.cells.count() * TILE_SIZE * 2;
//    int imW = img.width() * 4;

    for (int  x = 0; x < m_grid.cells.count(); ++x)
    {
//        futures.append(QtConcurrent::run([this, x, w, imW]() {
            for (int y = 0; y < m_grid.cells.count(); ++y)
            {
                drawRoad(x, y);
            }
//        }));
    }
//    foreach (auto future, futures) {
//        future.waitForFinished();
//    }
}

void LandscapeObject::drawTrees()
{
    int w = m_grid.cells.count() * TILE_SIZE * 2;
    int imW = img.width() * 4;

    for (int  x = 0; x < m_grid.cells.count(); ++x)
    {
        for (int y = 0; y < m_grid.cells.count(); ++y)
        {
            drawTree(x, y);
        }
    }
}

void LandscapeObject::drawTree(int x, int y)
{
    int w = m_grid.cells.count() * TILE_SIZE * 2;
    int imW = img.width() * 4;
    if (tileGround(m_grid.cells[x][y].value)  == 1)
    {
        int newX = getXOffset(x * TILE_SIZE, y * TILE_SIZE, w, TILE_SIZE);
        int newY = getYOffset(x * TILE_SIZE, y * TILE_SIZE);
        newX += EXTRA_OFFSET;
        newY += EXTRA_OFFSET;

        QImage tree = MapTileHelper::forestByValue(m_grid.cells[x][y].value);
        int dx =  tree.width() / 2;
        int dy =  tree.height() / 2;
        int imX = newX + TILE_SIZE - dx;
        int imY = newY + TILE_SIZE / 2 - dy;
        MapTileHelper::drawImage(tree, img.bits(), imX * 4 + imY * imW , imW);
    }
}

void LandscapeObject::clearItemsGrid()
{
    for (int i = 0; i < m_itemsGrid.count(); ++i)
    {
        for (int k = 0; k < m_itemsGrid[i].count(); ++k)
        {
            if (m_itemsGrid[i][k] != nullptr)
            {
                scene()->removeItem(m_itemsGrid[i][k]);
                delete m_itemsGrid[i][k];
                m_itemsGrid[i][k] = nullptr;
            }
        }
    }
    int w = m_grid.cells.count() * TILE_SIZE * 2;
    int cols = (img.width() + DISPLAY_TILE_SIZE - 1) / DISPLAY_TILE_SIZE;
    m_itemsGrid.resize(cols);
    for (int i = 0; i < m_itemsGrid.count(); ++i)
    {
        m_itemsGrid[i].resize(cols);
    }
    for (int i = 0; i < m_itemsGrid.count(); ++i)
    {
        for (int k = 0; k < m_itemsGrid[i].count(); ++k)
        {
            m_itemsGrid[i][k] = new TileItem();
            m_itemsGrid[i][k]->init(i, k, w);
            scene()->addItem(m_itemsGrid[i][k]);
        }
    }
}

void LandscapeObject::addTilesFromCell(int x, int y, QVector<TileItem *> &tiles)
{
    int w = m_grid.cells.count() * TILE_SIZE * 2;
    int newX = getXOffset(x * TILE_SIZE, y * TILE_SIZE, w, TILE_SIZE) + EXTRA_OFFSET;
    int newY = getYOffset(x * TILE_SIZE, y * TILE_SIZE) + EXTRA_OFFSET;

    TileItem* tile;
    tile = tileByPos(newX, newY);
    if (!tiles.contains(tile))
        tiles<<tile;

    tile = tileByPos(newX + TILE_SIZE * 2, newY);
    if (!tiles.contains(tile))
        tiles<<tile;

    tile = tileByPos(newX  + TILE_SIZE * 2, newY + TILE_SIZE / 2);
    if (!tiles.contains(tile))
        tiles<<tile;

    tile = tileByPos(newX, newY + TILE_SIZE / 2);
    if (!tiles.contains(tile))
        tiles<<tile;
}

TileItem *LandscapeObject::tileByPos(int x, int y)
{
    int tileX = x / DISPLAY_TILE_SIZE;
    int tileY = y / DISPLAY_TILE_SIZE;
    return m_itemsGrid[tileX][tileY];
}

QPoint LandscapeObject::mapPos(const QPointF &pos) const
{
    QPointF pos2 = isometricToCartesian(pos.x(), pos.y());
    QPoint result;
    result.setX(ceil(pos2.x() / TILE_SIZE - m_grid.cells.count() / 2 - 1));
    result.setY(ceil(pos2.y() / TILE_SIZE + m_grid.cells.count() / 2 - 1));
    if (result.x() < 0)
        result.setX(0);
    if (result.y() < 0)
        result.setY(0);
    return result;
}

#ifndef HOVERHIGHLIGHT_H
#define HOVERHIGHLIGHT_H
#include <QGraphicsItem>
#include <QPainter>
#include <QPixmap>
#include <QList>
#include <QPoint>
#include "Engine/Map/MapStateHolder.h"
#include "Engine/Components/ResourceManager.h"
#include "Events//EventBus.h"

class HoverHighlightItem : public QGraphicsItem , public EventSubscriber{
public:
    HoverHighlightItem(QGraphicsItem* parent = nullptr);
    void init();
    void nextFrame(const QRectF &view);

    void onHoverChanged(const QVariant &eventVar);

    QRectF boundingRect() const override;
    void paint(QPainter* painter, const QStyleOptionGraphicsItem* option, QWidget* widget) override;
private:
    void drawImage(QPainter* painter, FrameData & data, const QPointF & pos);
private:
    MapObject m_obj;
    QRectF m_bounds;

    FrameData m_availableImage;
    FrameData m_notAvailableImage;
    FrameData m_nextImage;
    FrameData m_battleImage;
    FrameData m_initialImage;
    //MOVENEGO
    FrameData m_selectedTile;
    //MOVEOUT
    bool m_loaded = false;
};

#endif // HOVERHIGHLIGHT_H

#include "MapConverter.h"
#include "MapObjects/LandmarkObject.h"
#include "MapObjects/StackObject.h"
#include "MapObjects/StackTemplateObject.h"
#include "MapObjects/FortObject.h"
#include "MapObjects/MountainObject.h"
#include "MapObjects/CrystalObject.h"
#include "MapObjects/RuinObject.h"
#include "MapObjects/ScenVariableObject.h"
#include "MapObjects/TreasureObject.h"
#include "MapObjects/MerchantObject.h"
#include "MapObjects/LocationObject.h"
#include "MapObjects/EventObject.h"
#include "MapObjects/RodObject.h"
#include "MapObjects/TombObject.h"
#include "MapObjects/Diplomacy.h"
#include "Engine/GameInstance.h"
#include "Components/AccessorHolder.h"
#include "toolsqt/DBFModel/DBFModel.h"
#include "Engine/Map/MapStateHolder.h"

GroupData importStack(const D2MapModel &map, const StackData& stack, const int &leader, GameMap * mapOut);
GroupData importTemplate(const D2StackTemplate &stack, const QString &leader, GameMap *map);


ValidationResult validateStack(const D2MapModel &map, const QPair<int, int> & uid, const StackData& stack, const int &leader)
{
    ValidationResult res;
    res.valid = true;
    for(int i = 0; i < 6; ++i)
    {
        if (stack.pos[i] != -1)
        {
            const int id = stack.unit[stack.pos[i]];
            D2Unit unit = map.get<D2Unit>(id);
            auto gunit = RESOLVE(DBFModel)->get<Gunit>(unit.baseUnit);
            if (gunit == nullptr)
                res.errors<<QString("%1:%2").arg(uid.first).arg(uid.second) + " Gunit not found(" + unit.baseUnit + ")";
        }
    }
    if (leader != -1)
    {
        D2Unit unit = map.get<D2Unit>(leader);
        auto gunit = RESOLVE(DBFModel)->get<Gunit>(unit.baseUnit);
        if (gunit == nullptr)
            res.errors << QString("%1:%2").arg(uid.first).arg(uid.second) + "Gunit not found(" + unit.baseUnit + ")";
        else
        {
            if (gunit->unit_cat->text != "L_LEADER" &&
                gunit->unit_cat->text != "L_NOBLE")
                res.errors<<QString("%1:%2").arg(uid.first).arg(uid.second) + "Leader unit with not leader category(" + unit.baseUnit + ")";
        }
    }
    res.valid = res.errors.isEmpty();
    return res;
}

ValidationResult MapConverter::validateMap(const D2MapModel &map)
{
    ValidationResult res;
    res.valid = true;
    QList<D2LandMark> marks = map.getAll<D2LandMark>();
    foreach(const D2LandMark & mark, marks)
    {
        auto base = RESOLVE(DBFModel)->get<GLmark>(mark.baseType);
        if (base == nullptr)
            res.errors<<QString("%1:%2").arg(mark.uid.first).arg(mark.uid.second) + " LMark not found(" + mark.baseType + ")";
    }
    QList<D2Village> villages = map.getAll<D2Village>();
    foreach(const D2Village & village, villages)
    {
        foreach(const int& item, village.items)
        {
            D2Item itemBase = map.get<D2Item>(item);
            auto base = RESOLVE(DBFModel)->get<GItem>(itemBase.itemType);
            if (base == nullptr)
                res.errors<<QString("%1:%2").arg(village.uid.first).arg(village.uid.second) +" GItem not found(" + itemBase.itemType + ")";
        }
        ValidationResult stackRes = validateStack(map, village.uid, village.stack, -1);
        res.errors += stackRes.errors;
    }
    QList<D2Stack> stacks = map.getAll<D2Stack>();

    foreach(const D2Stack & stack, stacks)
    {
        ValidationResult stackRes = validateStack(map, stack.uid, stack.stack, stack.leaderId);
        res.errors += stackRes.errors;
        foreach(const int& item, stack.items)
        {
            D2Item itemBase = map.get<D2Item>(item);
            auto base = RESOLVE(DBFModel)->get<GItem>(itemBase.itemType);
            if (base == nullptr)
                res.errors<<QString("%1:%2").arg(stack.uid.first).arg(stack.uid.second) + " GItem not found(" + itemBase.itemType + ")";
        }
    }
    QList<D2Capital> capitals = map.getAll<D2Capital>();
    foreach(const D2Capital & capital, capitals)
    {
        ValidationResult stackRes = validateStack(map, capital.uid, capital.stack, -1);
        res.errors += stackRes.errors;
        foreach(const int& item, capital.items)
        {
            D2Item itemBase = map.get<D2Item>(item);
            auto base = RESOLVE(DBFModel)->get<GItem>(itemBase.itemType);
            if (base == nullptr)
                res.errors<<QString("%1:%2").arg(capital.uid.first).arg(capital.uid.second) + " GItem not found(" + itemBase.itemType + ")";
        }
    }
    QList<D2Ruin > ruins = map.getAll<D2Ruin>();
    foreach(const D2Ruin & ruin, ruins)
    {
//        ValidationResult stackRes = validateStack(map, ruin.uid, ruin.stack, -1);
//        res.errors += stackRes.errors;
        if (ruin.item == "G000000000")
            continue;
        auto base = RESOLVE(DBFModel)->get<GItem>(ruin.item);
        if (base == nullptr)
            res.errors<<QString("%1:%2").arg(ruin.uid.first).arg(ruin.uid.second) + " GItem not found(" + ruin.item + ")";
    }
    QList<D2Bag > bags = map.getAll<D2Bag>();
    foreach(const D2Bag & bag, bags)
    {
        foreach(const int& item, bag.items)
        {
            D2Item itemBase = map.get<D2Item>(item);
            auto base = RESOLVE(DBFModel)->get<GItem>(itemBase.itemType);
            if (base == nullptr)
                res.errors<<QString("%1:%2").arg(bag.uid.first).arg(bag.uid.second) + " GItem not found(" + itemBase.itemType + ")";
        }
    }
    QList<D2Merchant> traders = map.getAll<D2Merchant>();
    foreach(const D2Merchant & trader, traders)
    {
        foreach(const D2Merchant::MerchantEntry& item, trader.items)
        {
            auto base = RESOLVE(DBFModel)->get<GItem>(item.itemId);
            if (base == nullptr)
                res.errors<<QString("%1:%2").arg(trader.uid.first).arg(trader.uid.second) + " GItem not found(" + item.itemId + ")";
        }
    }

    QList<D2Mercs> mercs = map.getAll<D2Mercs>();
    foreach(const D2Mercs & merc, mercs)
    {
        foreach(const D2Mercs::MercsUnitEntry& entry, merc.units)
        {
            auto base = RESOLVE(DBFModel)->get<Gunit>(entry.id);
            if (base == nullptr)
                res.errors<<QString("%1:%2").arg(merc.uid.first).arg(merc.uid.second) + " GUnit not found(" + entry.id + ")";
        }
    }

    QList<D2Mage> mages = map.getAll<D2Mage>();
    foreach(const D2Mage & mage, mages)
    {
        foreach(const QString& entry, mage.spells)
        {
            auto base = RESOLVE(DBFModel)->get<GSpell>(entry);
            if (base == nullptr)
                res.errors<<QString("%1:%2").arg(mage.uid.first).arg(mage.uid.second) + " GSpell not found(" + entry + ")";
        }
    }
    res.valid = res.errors.isEmpty();
    return res;
}

bool MapConverter::importMap(GameMap * output, const D2MapModel &map)
{
    QElapsedTimer timer;
    timer.start();

    output->clear();
    output->setMapName(map.header.name);
    output->setMapDesc(map.header.description);
    MapGrid grid;
    int mapSize = map.header.mapSize;
    grid.init(mapSize);
    QList<D2MapBlock> blocks = map.getAll<D2MapBlock>();
    qDebug()<<"BlocksCount = "<<blocks.count();
    foreach (const D2MapBlock & block, blocks)
    {
        int blockX = block.x();
        int blockY = block.y();
        for (int i = 0; i < 32; ++i)
        {
            int x = blockX + i % 8;
            int y = blockY + (int)(i / 8);
            int32_t value = block.blockData[i];
            grid.cells[x][y].value = value;
        }
    }
    qDebug()<<"Filling grid takes"<<timer.elapsed();

    D2ScenarioInfo info = map.get<D2ScenarioInfo>(0);
    output->replaceObject<MapInfo>(MapInfo(info));

    D2TalismanCharges charges = map.getAll<D2TalismanCharges>()[0];

    QList<D2Road> roads = map.getAll<D2Road>();
    qDebug()<<"D2Road Count = "<<roads.count();
    foreach (const D2Road & road, roads)
    {
        grid.cells[road.posX][road.posY].roadType = road.roadIndex;
        grid.cells[road.posX][road.posY].roadVar = road.var;
    }

    QList<D2Player> players = map.getAll<D2Player>();
    foreach(const D2Player & player, players)
    {
        PlayerObject playerObj;

        playerObj.bank = Currency::fromString(player.bank);
        playerObj.name = player.name;
        playerObj.desc = player.desc;
        playerObj.face = player.face;
        auto lord = RESOLVE(DBFModel)->get<Glord>(player.lordId);
        playerObj.lordCat = (Glord::Category)lord->category.key;
        playerObj.raceId = player.raceId;
        playerObj.attitude = player.attitude;
        //fog
        //playerObj.spellbook = player.spellBank;
        //playerObj.buildings = player.buildingsId;
        playerObj.isHuman = player.isHuman;
        playerObj.alwaysAI = player.alwaysai;
        playerObj.researchThisTurn = player.researchOnThisTurn;
        playerObj.buildThisTurn = player.constructOnTHisTurn;

        output->addObject(QSharedPointer<MapObject>(new PlayerObject(playerObj)), player.uid);
    }

    QList<D2SubRace> subRaces = map.getAll<D2SubRace>();
    foreach(const D2SubRace & subRace, subRaces)
    {
        SubRaceObject subRaceObj;
        subRaceObj.banner = subRace.banner;
        subRaceObj.name = subRace.name;
        subRaceObj.number = subRace.number;
        subRaceObj.subrace = subRace.subrace;
        subRaceObj.playerUid = MapLink<PlayerObject>(subRace.playerId);
        output->addObject(QSharedPointer<MapObject>(new SubRaceObject(subRaceObj)), subRace.uid);
    }

    timer.start();
    QList<D2LandMark> marks = map.getAll<D2LandMark>();
    qDebug()<<"Marks : "<<marks.count();
    qDebug()<<"Fetching marks takes"<<timer.elapsed();
    timer.start();
    foreach(const D2LandMark & mark, marks)
    {
        auto base = RESOLVE(DBFModel)->get<GLmark>(mark.baseType);
        if (base == nullptr)
        {
            qDebug()<<"LMark not found(" + mark.baseType + ")";
            continue;
        }
        LandmarkObject landObj(base->lmark_id, mark.posX,mark.posY);
        landObj.desc = mark.desc;
        output->addObject(QSharedPointer<MapObject>(new LandmarkObject(landObj)), mark.uid);
    }
    qDebug()<<"creating marks takes"<<timer.elapsed();

    timer.start();
    QList<D2Village> villages = map.getAll<D2Village>();

    qDebug()<<"Villages : "<<villages.count();
    qDebug()<<"Fetching villages takes"<<timer.elapsed();
    timer.start();
    foreach(const D2Village & village, villages)
    {
        FortObject  villageObj;
        villageObj.fortType = FortObject::Village;
        villageObj.x = village.posX;
        villageObj.y = village.posY;
        villageObj.level = village.size;
        villageObj.desc = village.desc;
        villageObj.name = village.name;
        villageObj.garrison = importStack(map, village.stack, -1, output);
        villageObj.owner = MapLink<PlayerObject>(village.owner.second);
        villageObj.subrace = MapLink<SubRaceObject>(village.subRace.second);
        villageObj.priority = village.AIpriority;
        foreach(const int& item, village.items)
        {
            D2Item itemBase = map.get<D2Item>(item);
            auto base = RESOLVE(DBFModel)->get<GItem>(itemBase.itemType);
            int chargesCount = 0;
            if (base->item_cat == GItem::Talisman)
            {
                for (int i = 0; i < charges.charges.count(); ++i)
                {
                    if (charges.charges[i].talismanId == item)
                    {
                        chargesCount = charges.charges.at(i).count;
                        break;
                    }
                }
            }
            villageObj.garrison.inventory.addItem(itemBase.itemType,1, chargesCount);
        }
        if (village.stackId != -1)
        {
            villageObj.visiter = MapLink<StackObject>(village.stackId);
        }
        villageObj.uid.second = village.uid.second;
        output->replaceObject(villageObj);
    }
    qDebug()<<"creating villages takes"<<timer.elapsed();

    timer.start();
    QList<D2Stack> stacks = map.getAll<D2Stack>();

    qDebug()<<"Stacks : "<<stacks.count();
    qDebug()<<"Fetching stacks takes"<<timer.elapsed();
    timer.start();

    foreach(const D2Stack & stack, stacks)
    {
        StackObject temp;
        temp.stack = importStack(map, stack.stack, stack.leaderId, output);
        temp.order = stack.order;
        temp.aiorder = stack.aiorder;
        temp.rotation = stack.facing;
        temp.x = stack.posX;
        temp.y = stack.posY;
        temp.orderTarget = stack.orderTarget;
        temp.aiOrderTarget = stack.aiorderTarget;
        temp.inside = stack.inside;
        temp.creatLvl = stack.creatLvl;
        temp.nbbattle = stack.nbbattle;
        temp.invisible = stack.invisible;
        temp.aiIgnore = stack.aiIgnore;
        temp.priority = stack.aipriority;
        temp.owner = MapLink<PlayerObject>(stack.owner.second);
        temp.subrace = MapLink<SubRaceObject>(stack.subRace.second);
        temp.uid.second = stack.uid.second;
        temp.move = stack.move;

        foreach(const int& item, stack.items)
        {
            D2Item itemBase = map.get<D2Item>(item);
            auto base = RESOLVE(DBFModel)->get<GItem>(itemBase.itemType);
            int chargesCount = 0;
            if (base->item_cat == GItem::Talisman)
            {
                for (int i = 0; i < charges.charges.count(); ++i)
                {
                    if (charges.charges[i].talismanId == item)
                    {
                        chargesCount = charges.charges.at(i).count;
                        break;
                    }
                }
            }
            temp.stack.inventory.addItem(itemBase.itemType,1, chargesCount);
        }
        output->replaceObject(temp);
    }
    qDebug()<<"creating stacks takes"<<timer.elapsed();

    timer.start();


    QList<D2Capital> capitals = map.getAll<D2Capital>();

    qDebug()<<"Capitals : "<<capitals.count();
    qDebug()<<"Fetching capitals takes"<<timer.elapsed();
    timer.start();

    foreach(const D2Capital & capital, capitals)
    {
        D2Player player = map.get<D2Player>(capital.owner);
        //auto race = m_model->get<Grace>(player._raceId);
        //QString key = race->race_type.m_value->text;
        FortObject object;
        object.name = capital.name;
        object.desc = capital.desc;
        object.raceId = player.raceId;
        object.x = capital.posX;
        object.y = capital.posY;
        object.fortType = FortObject::Capital;
        object.garrison = importStack(map, capital.stack, -1, output);
        foreach(const int& item, capital.items)
        {
            D2Item itemBase = map.get<D2Item>(item);
            auto base = RESOLVE(DBFModel)->get<GItem>(itemBase.itemType);
            int chargesCount = 0;
            if (base->item_cat == GItem::Talisman)
            {
                for (int i = 0; i < charges.charges.count(); ++i)
                {
                    if (charges.charges[i].talismanId == item)
                    {
                        chargesCount = charges.charges.at(i).count;
                        break;
                    }
                }
            }
            object.garrison.inventory.addItem(itemBase.itemType,1, chargesCount);
        }
        if (capital.stackId != -1)
        {
            object.visiter = MapLink<StackObject>(capital.stackId);
        }
        output->addObject(QSharedPointer<MapObject>
                         (new FortObject(object)));
    }
    qDebug()<<"creating capitals takes"<<timer.elapsed();

    timer.start();
    QList<D2Mountains > mountains = map.getAll<D2Mountains>();

    qDebug()<<"D2Mountains : "<<mountains.count();
    qDebug()<<"Fetching D2Mountains takes"<<timer.elapsed();
    timer.start();

    foreach(const D2Mountains::Mountain & mountain, mountains[0].mountains)
    {
        MountainObject object;
        object.x = mountain.posX;
        object.y = mountain.posY;
        object.w = mountain.sizeX;
        object.h = mountain.sizeY;
        object.race = mountain.race;
        object.image = mountain.image;
        object.uid.second = mountain.id;
        output->replaceObject(object);
    }
    qDebug()<<"creating D2Mountains takes"<<timer.elapsed();

    timer.start();
    QList<D2Crystal > crystals = map.getAll<D2Crystal>();

    qDebug()<<"D2Crystal : "<<crystals.count();
    qDebug()<<"Fetching D2Crystal takes"<<timer.elapsed();
    timer.start();

    foreach(const D2Crystal & crystal, crystals)
    {
        output->replaceObject(CrystalObject(crystal));
    }
    qDebug()<<"creating D2Crystal takes"<<timer.elapsed();

    timer.start();
    QList<D2Ruin > ruins = map.getAll<D2Ruin>();

    qDebug()<<"D2Ruin : "<<ruins.count();
    qDebug()<<"Fetching D2Ruin takes"<<timer.elapsed();
    timer.start();

    foreach(const D2Ruin & ruin, ruins)
    {
        RuinObject object;
        object.x = ruin.posX;
        object.y = ruin.posY;
        object.uid.second = ruin.uid.second;
        object.name = ruin.name;
        object.desc = ruin.desc;
        object.image = ruin.image;
        object.guards = importStack(map, ruin.stack, -1, output);
        object.reward.read(ruin.cash);
        object.item = ruin.item;
        object.priority = ruin.AIPriority;
        output->replaceObject(object);
    }
    qDebug()<<"creating D2Ruin takes"<<timer.elapsed();
    timer.start();
    QList<D2Bag > bags = map.getAll<D2Bag>();

    qDebug()<<"D2Bag : "<<ruins.count();
    qDebug()<<"Fetching D2Bag takes"<<timer.elapsed();
    timer.start();

    foreach(const D2Bag & bag, bags)
    {
        TreasureObject object;
        object.x = bag.posX;
        object.y = bag.posY;
        object.image = bag.image;
        object.AIpriority = bag.AIpriority;
        foreach(const int& item, bag.items)
        {
            D2Item itemBase = map.get<D2Item>(item);
            auto base = RESOLVE(DBFModel)->get<GItem>(itemBase.itemType);
            int chargesCount = 0;
            if (base->item_cat == GItem::Talisman)
            {
                for (int i = 0; i < charges.charges.count(); ++i)
                {
                    if (charges.charges[i].talismanId == item)
                    {
                        chargesCount = charges.charges.at(i).count;
                        break;
                    }
                }
            }
            object.inventory.addItem(itemBase.itemType,1, chargesCount);
        }
        output->addObject(QSharedPointer<MapObject>
                         (new TreasureObject(object)), bag.uid);
    }
    qDebug()<<"creating D2Bag takes"<<timer.elapsed();

    QList<D2Merchant> traders = map.getAll<D2Merchant>();
    foreach(const D2Merchant & trader, traders)
    {
        MerchantObject object;
        object.x = trader.posX;
        object.y = trader.posY;
        object.image = trader.image;
        object.name = trader.name;
        object.desc = trader.desc;
        object.type = MerchantObject::Items;
        foreach(const D2Merchant::MerchantEntry& item, trader.items)
        {
            object.inventory.addItem(item.itemId, item.count);

        }
        object.priority = trader.AIpriority;
        output->addObject(QSharedPointer<MapObject>
                         (new MerchantObject(object)), trader.uid);
    }

    QList<D2Mercs> mercs = map.getAll<D2Mercs>();
    foreach(const D2Mercs & merc, mercs)
    {
        MerchantObject object;
        object.x = merc.posX;
        object.y = merc.posY;
        object.image = merc.image;
        object.name = merc.name;
        object.desc = merc.desc;
        object.type = MerchantObject::Units;
        foreach(const D2Mercs::MercsUnitEntry& entry, merc.units)
        {
            UnitObject unit;
            unit.id = entry.id;
            unit.level = entry.level;
            object.units.addItem(unit);
        }
        object.priority = merc.AIpriority;
        output->addObject(QSharedPointer<MapObject>
                          (new MerchantObject(object)), QPair<int, int>(MapObject::Merchant, merc.uid.second));
    }

    QList<D2Trainer> trainers = map.getAll<D2Trainer>();
    foreach(const D2Trainer & trainer, trainers)
    {
        MerchantObject object;
        object.x = trainer.posX;
        object.y = trainer.posY;
        object.image = trainer.image;
        object.name = trainer.name;
        object.desc = trainer.desc;
        object.type = MerchantObject::Trainer;
        object.priority = trainer.AIpriority;
        output->addObject(QSharedPointer<MapObject>
                         (new MerchantObject(object)), QPair<int, int>(MapObject::Merchant, trainer.uid.second));
    }

    QList<D2Mage> mages = map.getAll<D2Mage>();
    foreach(const D2Mage & mage, mages)
    {
        MerchantObject object;
        object.x = mage.posX;
        object.y = mage.posY;
        object.image = mage.image;
        object.name = mage.name;
        object.desc = mage.desc;
        object.type = MerchantObject::Spells;
        foreach(const QString& entry, mage.spells)
        {
            object.spells.append(entry);
        }
        object.priority = mage.AIpriority;
        output->addObject(QSharedPointer<MapObject>
                         (new MerchantObject(object)), QPair<int, int>(MapObject::Merchant, mage.uid.second));
    }
    QList<D2Rod> rodes = map.getAll<D2Rod>();
    foreach(const D2Rod & rod, rodes)
    {
        RodObject object(rod);
        output->addObject(QSharedPointer<MapObject>
                         (new RodObject(object)), QPair<int, int>(MapObject::Rod, rod.uid.second));
    }

    QList<D2Tomb> tombs = map.getAll<D2Tomb>();
    foreach(const D2Tomb & tomb, tombs)
    {
        qDebug()<<"###TOMB"<<tomb.posX<<tomb.posY;
        TombObject object(tomb);
        output->addObject(QSharedPointer<MapObject>
                         (new TombObject(object)), QPair<int, int>(MapObject::Tomb, tomb.uid.second));
    }


    QList<D2Location > locs = map.getAll<D2Location>();
    foreach(const D2Location & loc, locs)
    {
        LocationObject object(loc.name, loc.posX, loc.posY, 1 + loc.radius * 2);
        object.uid.second = loc.uid.second;
        output->replaceObject(object);
    }
//TODO: fill earlier
    QList<QSharedPointer<MapObject> > objects = output->objects();
    foreach(QSharedPointer<MapObject> mapObj, objects)
    {
        if (!mapObj->populate)
            continue;
        QSharedPointer<IMapObjectAccessor> accessor =
                RESOLVE(AccessorHolder)->objectAccessor(mapObj->uid.first);
        if (accessor.isNull())
            continue;
        int w = accessor->getW(mapObj);
        int h = accessor->getH(mapObj);
        int x = mapObj->x;
        int y = mapObj->y;
        for(int i = 0; i < w; ++i)
        {
            for(int k = 0; k < h; ++k)
            {
                if (x + i >= mapSize || y + k >= mapSize)
                    continue;
                if (x + i < 0 || y + k < 0)
                    continue;
                if (mapObj->uid.first == MapObject::Location)
                    grid.locationsBinging.binding[x + i][y + k].items << mapObj->uid;
                else
                    grid.objBinging.binding[x + i][y + k].items << mapObj->uid;
            }
        }
    }
    output->setGrid(grid);

    qDebug()<<"D2Event : "<<ruins.count();
    QList<D2Event> events = map.getAll<D2Event>();
    qDebug()<<"Fetching D2Event takes"<<timer.elapsed();
    timer.start();

    QuestLine defaultQuests;
    defaultQuests.uid.second = 0;
    defaultQuests.name = "undefined";
    foreach(const D2Event & event, events)
    {
        EventObject object;
        object.uid.second = event.uid.second;
        object.name = event.name_txt;
        object.occurOnce = event.occurOnce;
        object.order = event.order;
        object.chance = event.chance;
        object.activatedBy[EventObject::HUMAN] = event.human;
        object.activatedBy[EventObject::DWARF] = event.dwarf;
        object.activatedBy[EventObject::UNDEAD] = event.undead;
        object.activatedBy[EventObject::HERETIC] = event.heretic;
        object.activatedBy[EventObject::NEUTRAL] = event.neutral;
        object.activatedBy[EventObject::ELF] = event.elf;

        object.effectsTo[EventObject::HUMAN] = event.verhuman;
        object.effectsTo[EventObject::DWARF] = event.verdwarf;
        object.effectsTo[EventObject::UNDEAD] = event.verundead;
        object.effectsTo[EventObject::HERETIC] = event.verheretic;
        object.effectsTo[EventObject::NEUTRAL] = event.verneutral;
        object.effectsTo[EventObject::ELF] = event.verelf;

        foreach (const D2Event::EventCondition & cond, event.conditions)
        {
            EventObject::EventCondition eventCondition;
            eventCondition.condition = cond;
            fillCondition(eventCondition);
            object.conditions << eventCondition;
        }
        foreach (const D2Event::EventEffect & effect, event.effects)
        {
            EventObject::EventEffect eventEffect;
            eventEffect.effect = effect;
            fillEffectTargets(eventEffect);
            object.effects << eventEffect;
        }
        output->replaceObject(object);
        defaultQuests.stages[0].events << object.uid;
        defaultQuests.stages[0].name = "default";
    }
    output->replaceObject(defaultQuests);
    {
        QuestLine utilQuests;
        utilQuests.uid.second = 1;
        utilQuests.name = "Util: stacks respawn";
        utilQuests.utility = true;
        output->replaceObject(utilQuests);
    }
    {
        QuestLine utilQuests;
        utilQuests.uid.second = 2;
        utilQuests.name = "Util: village reward";
        utilQuests.utility = true;
        output->replaceObject(utilQuests);
    }
    qDebug()<<"creating D2Event takes"<<timer.elapsed();

    importDiplomacy(output, map);
    importTemplates(output, map);
    importVariables(output, map);
    return true;
}

void MapConverter::importVariables(GameMap *output, const D2MapModel &map)
{
    auto variables = map.get<D2SceneVariables>(0);
    for (int i = 0; i < variables.variables.count(); ++i)
    {
        ScenVariableObject variable;
        variable.uid.second = variables.variables[i].id;
        variable.name = variables.variables[i].name;
        variable.value = variables.variables[i].value;
        output->replaceObject(variable);
    }
}

void MapConverter::importTemplates(GameMap *output, const D2MapModel &map)
{
    QList<D2StackTemplate> stacksTmp = map.getAll<D2StackTemplate>();
    foreach(const D2StackTemplate & stack, stacksTmp)
    {
        StackTemplateObject temp;
        temp.uid.second = stack.uid.second;
        temp.stack = importTemplate(stack, stack._Leader, output);
        temp.order = stack._Order;
        temp.orderTarget = stack.orderTarget;
        temp.rotation = stack._Facing;
        temp.priority = stack._AIPriority;
        temp.useFacing = stack.useFacing;
        temp.subrace = MapLink<SubRaceObject>(stack.subRace.second);
        output->replaceObject(temp);
    }
}

void MapConverter::importDiplomacy(GameMap *output, const D2MapModel &map)
{
    auto diplomacy = map.get<D2Diplomacy>(0);
    Relations relations;
    relations.uid.second = 0;

    const auto & players = map.getAll<D2Player>();
    QHash<int, int> mapping;
    for(const auto & player: players)
    {
        QSharedPointer<Grace> race1 = RESOLVE(DBFModel)->get<Grace>(player.raceId);
        mapping.insert(race1->race_type.key, player.uid.second);
    }
    for(auto & entry: diplomacy.entries)
    {
        Relations::RelationEntry newEntry;
        newEntry.race1 = mapping[entry.race1];
        newEntry.race2 = mapping[entry.race2];//TODO: fill spy
        newEntry.relation = entry.relation;
        relations.entries<<newEntry;
    }
    output->replaceObject(relations);
}

QString toString(D2MapModel *output, const StackData & data)
{
    QString result = "";
    for(int i = 0; i < 6; ++i)
    {
        if (data.unit[i] == -1)
            continue;
        D2Unit unit = output->get<D2Unit>(data.unit[i]);
        auto gunit = RESOLVE(DBFModel)->get<Gunit>(unit.baseUnit);
        result += QString("[%1] ").arg(i) + gunit->name_txt->text + "   ";
    }
    for(int i = 0; i < 6; ++i)
    {
        result += "   " + QString::number(data.pos[i]);
    }
    return result;
}

D2MapModel MapConverter::exportMap(GameMap  map)
{
    D2MapEditor editor(RESOLVE(DBFModel));
    editor.createMap(map.mapSize());
    int mapSize = map.mapSize();
    auto players = map.objectsByType<PlayerObject>();
    auto capitalsTmp = map.objectsByType<FortObject>();
    QList<FortObject> capitals;
    for(auto & village: capitalsTmp)
    {
        if (village.fortType == FortObject::Capital)
            capitals << village;
    }
    QList<int> capitalIndexes;
    for (int i = 0; i < players.count(); ++i)
    {
        const auto & player = players[i];
        capitalIndexes << 0;
        for (int k = 0; k < capitals.count(); ++k)
        {
            if (player.raceId == capitals[k].raceId)
            {
                capitalIndexes.last() = k;
                break;
            }
        }
    }

    editor.replace(map.objectByIdT<MapInfo>(0).info);

    for (int i = 0; i < players.count(); ++i)
    {
        const auto & player = players[i];
        QSharedPointer<Grace> race = RESOLVE(DBFModel)->get<Grace>(player.raceId);
        //player
        D2Player d2player;
        {
            d2player.name = player.name;
            d2player.desc = player.desc;
            d2player.bank = player.bank.toString(":");
            d2player.face = player.face;
            d2player.attitude = player.attitude;
            QVector<QSharedPointer<Glord>> lords = RESOLVE(DBFModel)->getList<Glord>();
            for (int i = 0; i < lords.count(); ++i)
            {
                if (lords[i]->race_id.key.toLower() == player.raceId.toLower() &&
                    lords[i]->category.key == player.lordCat)
                {
                    d2player.lordId = lords[i]->lord_id.toUpper();
                    break;
                }
            }
            d2player.raceId = player.raceId;
            d2player.uid.second = player.uid.second;
        }
        //fog
        {
            D2MapFog fog;
            fog.uid.second = editor.nextId<D2MapFog>();
            for (int y = 0; y < mapSize; ++y)
            {
                D2MapFog::FogEntry entry;
                entry.posY = y;
                entry.fog.resize(mapSize / 8);
                entry.fog.fill('\0');
                fog.entries << entry;
            }
            editor.replace(fog);
        }
        //buildings
        {
            D2PlayerBuildings buildings;
            buildings.uid.second = player.uid.second;
            buildings.buildings = player.buildings;
            editor.replace(buildings);
            d2player.buildingsId = buildings.uid.second;
        }
        //spells
        {
            D2PlayerSpells spells;
            spells.uid.second = player.uid.second;
            spells.spells = player.spellbook;
            editor.replace(spells);
            d2player.knownId = spells.uid.second;
        }
        {
            D2MapFog fog;
            fog.uid.second = player.uid.second;
            for (int y = 0; y < mapSize; ++y)
            {
                D2MapFog::FogEntry entry;
                entry.posY = y;
                entry.fog.resize(mapSize / 8);
                entry.fog.fill('\0');
                fog.entries << entry;
            }
            d2player.fogId = fog.uid.second;
            editor.replace(fog);
        }
        editor.replace(d2player);

        if (race->playable)
        {
            const auto & capital = capitals[capitalIndexes[i]];

            D2Capital d2capital;
            d2capital.posX = capital.x;
            d2capital.posY = capital.y;
            d2capital.uid.second = capital.uid.second;
            d2capital.owner = player.uid.second;
            d2capital.subRace.first = IDataBlock::SubRace;
            d2capital.subRace.second = player.uid.second;
            d2capital.name = capital.name;
            d2capital.items = exportItems(&editor, capital.garrison.inventory);
            int tmp;
            updateGarrison(&editor, capital.garrison, tmp, d2capital.stack);

            if (capital.visiter.second != -1)
            {
                d2capital.stackId = capital.visiter.second;
            }

            editor.replace(d2capital);
        }
    }
    exportVillages(&editor, map);
    exportStacks(&editor, map);
    exportMerchants(&editor, map);
    exportTemplates(&editor, map);
    exportCrystals(&editor, map);
    exportLandMarks(&editor, map);
    exportMountains(&editor, map);
    exportLocations(&editor, map);
    exportVariables(&editor, map);
    exportRuins(&editor, map);
    exportBags(&editor, map);
    exportEvents(&editor, map);
    exportRods(&editor, map);
    exportTombs(&editor, map);
    exportDiplomacy(&editor, map);
    editor.setGrid(map.getGrid());
    editor.commitGrid();
    editor.updateSubraces();
    editor.updateInfo();
    return editor.map();
}

void MapConverter::updateGarrison(D2MapEditor *editor, const GroupData &garrison, int &leaderId, StackData &stack)
{
    for (int i = 0; i < 6; ++i)
    {
        if (stack.unit[i] != -1)
        {
            editor->removeObject(QPair<int, int>((int)IDataBlock::Unit, stack.unit[i]));
        }
    }
    stack.clear();
    for (int i = 0; i < garrison.units.count(); ++i)
    {

        auto unit = RESOLVE(MapStateHolder)->objectByIdT<UnitObject>(garrison.units[i].first);
        D2Unit d2Unit = editor->addUnit(unit.id, unit.level, garrison.units[i].first.second);
        d2Unit.mods = unit.modifiers;
        d2Unit.name = unit.name;
        editor->replace(d2Unit);
        stack.unit[i] = d2Unit.uid.second;

        if (garrison.leaderIndex() == i)
        {
            leaderId = d2Unit.uid.second;
        }

        auto gunit = editor->getDbfModel()->get<Gunit>(d2Unit.baseUnit);
        int index = baseIndexByPos(garrison.units[i].second);
        if (gunit->size_small)
        {
            stack.pos[index] = i;
        }
        else
        {
            int index2 = baseIndexByPos(QPoint((garrison.units[i].second.x() ==  0)?1:0, garrison.units[i].second.y()));
            stack.pos[index] = i;
            stack.pos[index2] = i;
        }
    }
}

void MapConverter::updateTemplate(D2MapEditor *editor, const GroupData &garrison, D2StackTemplate &stack)
{
    stack.stack.clear();
    stack.mods.clear();
    for (int i = 0; i < garrison.units.count(); ++i)
    {
        auto unit = RESOLVE(MapStateHolder)->objectByIdT<UnitObject>(garrison.units[i].first);
        stack.stack.unit[i] = unit.id;
        stack.stack.level[i] = unit.level;

        if (garrison.leaderIndex() == i)
        {
            stack._Leader = unit.id;
            stack._LeaderLvl = unit.level;
        }

        auto gunit = editor->getDbfModel()->get<Gunit>(unit.id);
        int index = baseIndexByPos(garrison.units[i].second);
        if (gunit->size_small)
        {
            stack.stack.pos[index] = i;
        }
        else
        {
            int index2 = baseIndexByPos(QPoint((garrison.units[i].second.x() ==  0)?1:0, garrison.units[i].second.y()));
            stack.stack.pos[index] = i;
            stack.stack.pos[index2] = i;
        }
        foreach (auto mod, unit.modifiers)
        {
            stack.mods << D2StackTemplate::UnitTemplateMod{index, mod};
        }
    }
    std::sort(stack.mods.begin(), stack.mods.end(), []
          (const D2StackTemplate::UnitTemplateMod &obj1,
           const D2StackTemplate::UnitTemplateMod &obj2) {
        return obj1.index < obj2.index;
    });
}

QList<int> MapConverter::exportItems(D2MapEditor *editor, const Inventory &inventory)
{
    QList<int> result;
    foreach (const auto & item, inventory.items)
    {
        for (int k = 0; k < item.count; ++k)
        {
            result << editor->addItem(item.type, item.charges).uid.second;
        }
    }
    return result;
}

void MapConverter::exportMerchants(D2MapEditor *editor, GameMap &map)
{
    auto merchants = map.objectsByType<MerchantObject>();
    foreach (auto merchant, merchants)
    {
        if (merchant.type == MerchantObject::Items)
        {
            D2Merchant d2merchant;
            d2merchant.uid = merchant.uid;
            d2merchant.image = merchant.image;
            d2merchant.name = merchant.name;
            d2merchant.desc = merchant.desc;
            d2merchant.posX = merchant.x;
            d2merchant.posY = merchant.y;
            d2merchant.AIpriority = merchant.priority;
            d2merchant.items = convert(merchant.inventory);
            editor->replace(d2merchant);
        }
        if (merchant.type == MerchantObject::Units)
        {
            D2Mercs d2merchant;
            d2merchant.uid = merchant.uid;
            d2merchant.image = merchant.image;
            d2merchant.name = merchant.name;
            d2merchant.desc = merchant.desc;
            d2merchant.posX = merchant.x;
            d2merchant.posY = merchant.y;
            d2merchant.AIpriority = merchant.priority;
            d2merchant.units = convert(merchant.units);
            editor->replace(d2merchant);
        }
        if (merchant.type == MerchantObject::Spells)
        {
            D2Mage d2merchant;
            d2merchant.uid = merchant.uid;
            d2merchant.image = merchant.image;
            d2merchant.name = merchant.name;
            d2merchant.desc = merchant.desc;
            d2merchant.posX = merchant.x;
            d2merchant.posY = merchant.y;
            d2merchant.AIpriority = merchant.priority;
            d2merchant.spells = merchant.spells;
            editor->replace(d2merchant);
        }
        if (merchant.type == MerchantObject::Trainer)
        {
            D2Trainer d2merchant;
            d2merchant.uid = merchant.uid;
            d2merchant.image = merchant.image;
            d2merchant.name = merchant.name;
            d2merchant.desc = merchant.desc;
            d2merchant.posX = merchant.x;
            d2merchant.posY = merchant.y;
            d2merchant.AIpriority = merchant.priority;
            editor->replace(d2merchant);
        }
    }
}

void MapConverter::exportTemplates(D2MapEditor *editor, GameMap &map)
{
    auto templates = map.objectsByType<StackTemplateObject>();
    foreach (auto stackTemplate, templates)
    {
        D2StackTemplate d2stack;
        d2stack.uid = stackTemplate.uid;
        d2stack.owner = stackTemplate.owner;
        d2stack.subRace = stackTemplate.subrace;
        d2stack.useFacing = stackTemplate.useFacing;
        d2stack._Order = stackTemplate.order;
        d2stack.orderTarget = stackTemplate.orderTarget;
        d2stack._Name = stackTemplate.stack.name;
        d2stack._AIPriority = stackTemplate.priority;
        d2stack._Facing = stackTemplate.rotation;
        updateTemplate(editor, stackTemplate.stack, d2stack);
        editor->replace(d2stack);
    }
}

void MapConverter::exportCrystals(D2MapEditor *editor, GameMap &map)
{
    auto crystals = map.objectsByType<CrystalObject>();
    foreach (const auto & crystal, crystals)
    {
        editor->replace(crystal.toD2());
    }
}

void MapConverter::exportLandMarks(D2MapEditor *editor, GameMap &map)
{
    auto landmarks = map.objectsByType<LandmarkObject>();
    std::sort(landmarks.begin(), landmarks.end(), [](const LandmarkObject &obj1, const LandmarkObject &obj2) {
        return obj1.uid.second < obj2.uid.second;
    });
    foreach (const auto & landmark, landmarks)
    {
        D2LandMark lmark;
        lmark.baseType = landmark.lmarkId.toUpper();
        lmark.posX = landmark.x;
        lmark.posY = landmark.y;
        lmark.desc = landmark.desc;
        lmark.uid.second = landmark.uid.second;
        editor->replace(lmark);
    }
}

void MapConverter::exportMountains(D2MapEditor * editor, GameMap &map)
{
    auto mountains = map.objectsByType<MountainObject>();
    std::sort(mountains.begin(), mountains.end(), [](const MountainObject &obj1, const MountainObject &obj2) {
        return obj1.uid.second < obj2.uid.second;
    });
    auto d2mountains = editor->map().get<D2Mountains>(0);
    d2mountains.mountains.clear();
    d2mountains.mountains.reserve(mountains.count());
    foreach (const auto & mountain, mountains)
    {
        D2Mountains::Mountain mount;
        mount.posX = mountain.x;
        mount.posY = mountain.y;
        mount.sizeX = mountain.w;
        mount.sizeY = mountain.h;
        mount.image = mountain.image;
        mount.race = mountain.race;
        mount.id = mountain.uid.second;
        d2mountains.mountains << mount;
    }
    editor->replace(d2mountains);
}

void MapConverter::exportBags(D2MapEditor *editor, GameMap & map)
{
    auto treasures = map.objectsByType<TreasureObject>();
    foreach (const auto & treasure, treasures)
    {
        D2Bag bag;
        bag.image = treasure.image;
        bag.posX = treasure.x;
        bag.posY = treasure.y;
        updateInventory(editor, treasure.inventory, bag.items);
        bag.AIpriority = treasure.AIpriority;
        editor->replace(bag);
    }
}

void MapConverter::exportVariables(D2MapEditor *editor, GameMap &map)
{
    auto variables = map.objectsByType<ScenVariableObject>();
    std::sort(variables.begin(), variables.end(), [](const ScenVariableObject &obj1, const ScenVariableObject &obj2) {
        return obj1.uid.second < obj2.uid.second;
    });
    auto d2vars = editor->map().get<D2SceneVariables>(0);
    d2vars.variables.clear();
    foreach (const auto & variable, variables)
    {
        d2vars.variables <<
            D2SceneVariables::SceneVariable{variable.uid.second,
                                              variable.name,
                                              variable.value};
    }
    editor->replace(d2vars);
}

void MapConverter::exportLocations(D2MapEditor *editor, GameMap &map)
{
    auto locations = map.objectsByType<LocationObject>();
    foreach (const auto & location, locations)
    {
        D2Location d2loc;
        d2loc.uid.second = location.uid.second;
        d2loc.radius =  (location.r - 1) / 2;
        d2loc.posX = location.x + (location.r - 1) / 2;
        d2loc.posY = location.y + (location.r - 1) / 2;
        d2loc.name = location.name;
        editor->replace(d2loc);
    }
}

void MapConverter::exportRuins(D2MapEditor *editor, GameMap &map)
{
    auto ruins = map.objectsByType<RuinObject>();
    foreach (const auto & ruin, ruins)
    {
        D2Ruin d2ruin;
        d2ruin.uid.second = ruin.uid.second;
        d2ruin.name =  ruin.name;
        d2ruin.posX = ruin.x;
        d2ruin.posY = ruin.y;
        d2ruin.desc = ruin.desc;
        d2ruin.item = ruin.item;
        if (d2ruin.item.isEmpty())
            d2ruin.item = "G000000000";
        d2ruin.cash = ruin.reward.toString(":");
        d2ruin.AIPriority = ruin.priority;
        d2ruin.image = ruin.image;
        int tmp;
        updateGarrison(editor, ruin.guards, tmp, d2ruin.stack);
        editor->replace(d2ruin);
    }
}

void MapConverter::exportVillages(D2MapEditor *editor, GameMap &map)
{
    auto villages = map.objectsByType<FortObject>();
    for (int i = 0; i < villages.count(); ++i)
    {
        const auto & village = villages[i];
        if (village.fortType == FortObject::Capital)
            continue;
        D2Village d2village;
        d2village.posX = village.x;
        d2village.posY = village.y;
        d2village.uid.second = village.uid.second;
        d2village.owner = village.owner;
        d2village.subRace = village.subrace;
        d2village.name = village.name;
        d2village.size = village.level;
        d2village.AIpriority = village.priority;
        int tmp;
        updateGarrison(editor, village.garrison, tmp, d2village.stack);
        d2village.items = exportItems(editor, village.garrison.inventory);

        if (village.visiter.second != -1)
        {
            d2village.stackId = village.visiter.second;
        }
        editor->replace(d2village);
    }
}

void MapConverter::exportStacks(D2MapEditor *editor, GameMap &map)
{
    auto stacks = map.objectsByType<StackObject>();
    for (int i = 0; i < stacks.count(); ++i)
    {
        const auto & stack = stacks[i];
        D2Stack heroStack;
        heroStack.uid.second = stack.uid.second;
        heroStack.inside = stack.inside;
        updateGarrison(editor, stack.stack, heroStack.leaderId, heroStack.stack);
        heroStack.posX = stack.x;
        heroStack.posY = stack.y;
        heroStack.items = exportItems(editor, stack.stack.inventory);
        heroStack.aiorder = stack.aiorder;
        heroStack.aipriority = stack.priority;
        heroStack.order = stack.order;
        heroStack.orderTarget = stack.orderTarget;
        heroStack.aiorderTarget = stack.aiOrderTarget;
        heroStack.facing = stack.rotation;
        heroStack.move = stack.move;
        heroStack.owner = stack.owner;
        heroStack.creatLvl = stack.creatLvl;
        heroStack.nbbattle = stack.nbbattle;
        heroStack.invisible = stack.invisible;
        heroStack.aiIgnore = stack.aiIgnore;
        heroStack.subRace = QPair<int, int>(IDataBlock::SubRace, stack.subrace.second);
        editor->replace(heroStack);
    }
}

void MapConverter::exportEvents(D2MapEditor *editor, GameMap &map)
{
    auto events = map.objectsByType<EventObject>();
    foreach (const auto & event, events)
    {
        D2Event object;
        object.uid.second = event.uid.second;
        object.name_txt = event.name;
        object.occurOnce = event.occurOnce;
        object.order = event.order;
        object.chance = event.chance;
        object.human = event.activatedBy[EventObject::HUMAN];
        object.dwarf = event.activatedBy[EventObject::DWARF];
        object.undead = event.activatedBy[EventObject::UNDEAD];
        object.heretic = event.activatedBy[EventObject::HERETIC];
        object.neutral = event.activatedBy[EventObject::NEUTRAL];
        object.elf = event.activatedBy[EventObject::ELF];

        object.verhuman = event.effectsTo[EventObject::HUMAN];
        object.verdwarf = event.effectsTo[EventObject::DWARF];
        object.verundead = event.effectsTo[EventObject::UNDEAD];
        object.verheretic = event.effectsTo[EventObject::HERETIC];
        object.verneutral = event.effectsTo[EventObject::NEUTRAL];
        object.verelf = event.effectsTo[EventObject::ELF];

        object.conditions.clear();
        foreach (auto cond, event.conditions) {
            object.conditions << cond.condition;
        };
        object.effects.clear();
        foreach (auto cond, event.effects) {
            object.effects << cond.effect;
        };
        editor->replace(object);
    }
}

void MapConverter::exportRods(D2MapEditor *editor, GameMap &map)
{
    auto rods = map.objectsByType<RodObject>();
    foreach (const RodObject & rod, rods)
    {
        D2Rod rodObject;
        rodObject.owner = rod.owner;
        rodObject.uid = rod.uid;
        rodObject.posX = rod.x;
        rodObject.posY = rod.y;
        editor->replace(rodObject);
    }
}

void MapConverter::exportTombs(D2MapEditor *editor, GameMap &map)
{
    auto tombs = map.objectsByType<TombObject>();
    foreach (const TombObject & tomb, tombs)
    {
        D2Tomb object = tomb.tomb;
        object.uid = tomb.uid;
        object.posX = tomb.x;
        object.posY = tomb.y;
        editor->replace(object);
    }
}

void MapConverter::exportDiplomacy(D2MapEditor *editor, GameMap &map)
{
    auto diplomacy = editor->map().get<D2Diplomacy>(0);
    diplomacy.entries.clear();
    Relations relations = map.objectByIdT<Relations>(QPair<int, int>(MapObject::Relation, 0));
    for(auto & entry: relations.entries)
    {
        D2Diplomacy::DiplomacyEntry newEntry;
        QSharedPointer<Grace> race1 = RESOLVE(DBFModel)->get<Grace>(entry.race1.get().raceId);
        newEntry.race1 = race1->race_type.key;
        QSharedPointer<Grace> race2 = RESOLVE(DBFModel)->get<Grace>(entry.race2.get().raceId);
        newEntry.race2 = race2->race_type.key;
        newEntry.relation = entry.relation;
        diplomacy.entries.append(newEntry);
    }
    //TODO: fill spy
    editor->replace(diplomacy);
}

void MapConverter::updateInventory(D2MapEditor *editor, const Inventory & inventory,
                                   QList<int> & items)
{
    foreach(const auto & item, items)
        editor->removeObject(QPair<int,int>((int)IDataBlock::Item,item));
    items.clear();
    for (int i = 0; i < inventory.items.count(); ++i)
    {
        auto item = inventory.items[i];
        for (int k = 0; k < item.count; ++k)
        {
            D2Item d2item = editor->addItem(item.type);
            items.append(d2item.uid.second);//TODO: charges
        }
    }
}
//0 1
//2 3
//4 5
int MapConverter::baseIndexByPos(const QPoint &pos)
{
    return pos.y() * 2 + (1 - pos.x());
}

QList<D2Merchant::MerchantEntry> MapConverter::convert(const Inventory &inventory)
{
    QList<D2Merchant::MerchantEntry> result;
    for(int i = 0; i < inventory.items.count(); ++i)
    {
        D2Merchant::MerchantEntry entry;
        entry.itemId = inventory.items[i].type;
        entry.count = inventory.items[i].count;//TODO: charges
        result << entry;
    }
    return result;
}

QList<D2Mercs::MercsUnitEntry> MapConverter::convert(const MerchantObject::UnitHireList &units)
{
    QList<D2Mercs::MercsUnitEntry> result;
    for(int i = 0; i < units.items.count(); ++i)
    {
        D2Mercs::MercsUnitEntry entry;
        entry.id = units.items[i].first.id;
        entry.level = units.items[i].first.level;

        if (units.items[i].second == 0)
        {
            entry.isUnique = false;
            result << entry;
        }
        else
        {
            entry.isUnique = true;
            for (int k = 0; k < units.items[i].second; ++k)
            {
                result << entry;
            }
        }
    }
    return result;
}

void MapConverter::fillCondition(EventObject::EventCondition &eventCondition)
{
    auto cond = eventCondition.condition;
    switch (cond.category)
    {
    case D2Event::EventCondition::FREQUENCY:
        break;
    case D2Event::EventCondition::ENTERING_A_PREDEFINED_ZONE:
    {
        D2Event::EventCondition::ConditionEnterZone condition =
                std::get<D2Event::EventCondition::ConditionEnterZone>(cond.condition);
        eventCondition.targets<<TypeHolder::idFromString(condition.locId);
        break;
    }
    case D2Event::EventCondition::ENTERING_A_CITY:
    {
        D2Event::EventCondition::ConditionEnterCity condition =
                std::get<D2Event::EventCondition::ConditionEnterCity>(cond.condition);
        eventCondition.targets<<QPair<int, int>(IDataBlock::Fort, IDataBlock::getIntId(condition.villageId));
        break;
    }
    case D2Event::EventCondition::OWNING_A_CITY:
    {
        D2Event::EventCondition::ConditionOwningCity condition =
                std::get<D2Event::EventCondition::ConditionOwningCity>(cond.condition);
        eventCondition.targets<<QPair<int, int>(IDataBlock::Fort, IDataBlock::getIntId(condition.villageId));
        break;
    }
    case D2Event::EventCondition::DESTROY_STACK:
    {
        D2Event::EventCondition::ConditionDestroyStack condition =
                std::get<D2Event::EventCondition::ConditionDestroyStack>(cond.condition);
        eventCondition.targets<<TypeHolder::idFromString(condition.stackId);
        break;
    }
    case D2Event::EventCondition::OWNING_AN_ITEM:
        break;
    case D2Event::EventCondition::SPECIFIC_LEADER_OWNING_AN_ITEM:
    {
        D2Event::EventCondition::ConditionLeaderOwningItem condition =
                std::get<D2Event::EventCondition::ConditionLeaderOwningItem>(cond.condition);
        eventCondition.targets<<TypeHolder::idFromString(condition.stackId);
        break;
    }
    case D2Event::EventCondition::DIPLOMACY_RELATIONS:
        break;
    case D2Event::EventCondition::ALLIANCE:
        break;
    case D2Event::EventCondition::LOOTING_A_RUIN:
    {
        D2Event::EventCondition::ConditionLootingRuin condition =
                std::get<D2Event::EventCondition::ConditionLootingRuin>(cond.condition);
        eventCondition.targets<<TypeHolder::idFromString(condition.ruinId);
        break;
    }
    case D2Event::EventCondition::TRANSFORMING_LAND:
        break;
    case D2Event::EventCondition::VISITING_A_SITE:
    {
        D2Event::EventCondition::ConditionVisitingSite condition =
                std::get<D2Event::EventCondition::ConditionVisitingSite>(cond.condition);
        eventCondition.targets<<TypeHolder::idFromString(condition.siteId);
        break;
    }
    case D2Event::EventCondition::STACK_IN_LOCATION:
    {
        D2Event::EventCondition::ConditionStackInLocation condition =
                std::get<D2Event::EventCondition::ConditionStackInLocation>(cond.condition);
        eventCondition.targets<<TypeHolder::idFromString(condition.stackId);
        eventCondition.targets<<TypeHolder::idFromString(condition.locId);
        break;
    }
    case D2Event::EventCondition::STACK_IN_CITY:
    {
        D2Event::EventCondition::ConditionStackInCity condition =
                std::get<D2Event::EventCondition::ConditionStackInCity>(cond.condition);
        eventCondition.targets<<TypeHolder::idFromString(condition.stackId);
        eventCondition.targets<<TypeHolder::idFromString(condition.villageId);
        break;
    }
    case D2Event::EventCondition::ITEM_TO_LOCATION:
    {
        D2Event::EventCondition::ConditionItemToLocation condition =
                std::get<D2Event::EventCondition::ConditionItemToLocation>(cond.condition);
        eventCondition.targets<<TypeHolder::idFromString(condition.locId);
        break;
    }
    case D2Event::EventCondition::STACK_EXISTANCE:
    {
        D2Event::EventCondition::ConditionStackExist condition =
                std::get<D2Event::EventCondition::ConditionStackExist>(cond.condition);
        eventCondition.targets<<TypeHolder::idFromString(condition.stackId);
        break;
    }
    case D2Event::EventCondition::VARIABLE_IS_IN_RANGE:
        break;

    }
}

void MapConverter::fillEffectTargets(EventObject::EventEffect & eventEffect)
{
    auto effect = eventEffect.effect.effect;
    switch (eventEffect.effect.category)
    {
    case D2Event::EventEffect::WIN_OR_LOSE_SCENARIO:
    {
        D2Event::EventEffect::WinLooseEffect condition =
                std::get<D2Event::EventEffect::WinLooseEffect>(effect);
        eventEffect.targets<<TypeHolder::idFromString(condition.playerId);
        break;
    }
    case D2Event::EventEffect::CREATE_NEW_STACK:
    {
        D2Event::EventEffect::CreateStackEffect condition =
                std::get<D2Event::EventEffect::CreateStackEffect>(effect);
        eventEffect.targets<<TypeHolder::idFromString(condition.templateId);
        eventEffect.targets<<TypeHolder::idFromString(condition.locId);
        break;
    }
    case D2Event::EventEffect::CAST_SPELL_ON_TRIGGERER:
        break;
    case D2Event::EventEffect::CAST_SPELL_AT_SPECIFIC_LOCATION:
    {
        D2Event::EventEffect::CastSpellOnLocationEffect condition =
                std::get<D2Event::EventEffect::CastSpellOnLocationEffect>(effect);
        eventEffect.targets<<TypeHolder::idFromString(condition.locId);
        break;
    }
    case D2Event::EventEffect::CHANGE_STACK_OWNER:
    {
        D2Event::EventEffect::ChangeStackOwnerEffect condition =
                std::get<D2Event::EventEffect::ChangeStackOwnerEffect>(effect);
        eventEffect.targets<<TypeHolder::idFromString(condition.stackId);
        break;
    }
    case D2Event::EventEffect::MOVE_STACK_NEXT_TO_TRIGGERER:
    {
        D2Event::EventEffect::MoveStackToTriggererEffect condition =
                std::get<D2Event::EventEffect::MoveStackToTriggererEffect>(effect);
        eventEffect.targets<<TypeHolder::idFromString(condition.stackId);
        break;
    }
    case D2Event::EventEffect::GO_INTO_BATTLE:
    {
        D2Event::EventEffect::GoBattleEffect condition =
                std::get<D2Event::EventEffect::GoBattleEffect>(effect);
        eventEffect.targets<<TypeHolder::idFromString(condition.stackId);
        break;
    }
    case D2Event::EventEffect::ENABLE_DISABLE_ANOTHER_EVENT:
    {
        D2Event::EventEffect::DisableEventEffect condition =
                std::get<D2Event::EventEffect::DisableEventEffect>(effect);
        eventEffect.targets<<TypeHolder::idFromString(condition.eventId);
        break;
    }
    case D2Event::EventEffect::GIVE_SPELL:
        break;
    case D2Event::EventEffect::GIVE_ITEM:
        break;
    case D2Event::EventEffect::MOVE_STACK_TO_SPECIFIC_LOCATION:
    {
        D2Event::EventEffect::MoveStackToLocationEffect condition =
                std::get<D2Event::EventEffect::MoveStackToLocationEffect>(effect);
        eventEffect.targets<<TypeHolder::idFromString(condition.stackTmpId);
        eventEffect.targets<<TypeHolder::idFromString(condition.locId);
        break;
    }
    case D2Event::EventEffect::ALLY_TWO_AI_PLAYERS:
        break;
    case D2Event::EventEffect::CHANGE_PLAYER_DIPLOMACY_METER:
        break;
    case D2Event::EventEffect::UNFOG_OR_FOG_AN_AREA_ON_THE_MAP:
    {
        D2Event::EventEffect::ChangeFogEffect condition =
                std::get<D2Event::EventEffect::ChangeFogEffect>(effect);
        eventEffect.targets<<TypeHolder::idFromString(condition.locId);
        break;
    }
    case D2Event::EventEffect::REMOVE_MOUNTAINS_AROUND_A_LOCATION:
    {
        D2Event::EventEffect::RemoveMountainsEffect condition =
                std::get<D2Event::EventEffect::RemoveMountainsEffect>(effect);
        eventEffect.targets<<TypeHolder::idFromString(condition.locId);
        break;
    }
    case D2Event::EventEffect::REMOVE_LANDMARK:
    {
        D2Event::EventEffect::RemoveLandMarkEffect condition =
                std::get<D2Event::EventEffect::RemoveLandMarkEffect>(effect);
        eventEffect.targets<<TypeHolder::idFromString(condition.lmarkId);
        break;
    }
    case D2Event::EventEffect::CHANGE_SCENARIO_OBJECTIVE_TEXT:
        break;
    case D2Event::EventEffect::DISPLAY_POPUP_MESSAGE:
        break;
    case D2Event::EventEffect::CHANGE_STACK_ORDER:
    {
        D2Event::EventEffect::ChangeStackOrderEffect condition =
                std::get<D2Event::EventEffect::ChangeStackOrderEffect>(effect);
        eventEffect.targets<<TypeHolder::idFromString(condition.stackId);
        break;
    }
    case D2Event::EventEffect::DESTROY_ITEM:
        break;
    case D2Event::EventEffect::REMOVE_STACK:
        break;
    case D2Event::EventEffect::CHANGE_LANDMARK:
    {
        D2Event::EventEffect::ChangeLandmarkEffect condition =
                std::get<D2Event::EventEffect::ChangeLandmarkEffect>(effect);
        eventEffect.targets<<TypeHolder::idFromString(condition.lmarkId);
        break;
    }
    case D2Event::EventEffect::CHANGE_TERRAIN:
    {
        D2Event::EventEffect::ChangeTerrainEffect condition =
                std::get<D2Event::EventEffect::ChangeTerrainEffect>(effect);
        eventEffect.targets<<TypeHolder::idFromString(condition.locId);
        break;
    }
    case D2Event::EventEffect::MODIFY_VARIABLE:
    {
        D2Event::EventEffect::ModifyVarEffect condition =
                std::get<D2Event::EventEffect::ModifyVarEffect>(effect);
        eventEffect.targets<<QPair<int, int>((int)MapObject::ScenVariable, condition.val2);
        break;
    }
    }
}

GroupData importStack(const D2MapModel &map, const StackData& stack, const int &leader, GameMap *mapOut)
{
    GroupData result;
    QList<int> usedIds;
    for(int i = 0; i < 6; ++i)
    {
        if(stack.unit[i] == -1)
            continue;

        const int & id = stack.unit[i];
        D2Unit unit = map.get<D2Unit>(id);
        UnitObject resultUnit;
        resultUnit.id = unit.baseUnit;
        resultUnit.level = unit.level;
        resultUnit.modifiers = unit.mods;
        resultUnit.name = unit.name;
        resultUnit.currentHp = unit.hp;
        resultUnit.currentExp = unit.exp;

        if (id == leader && id != -1)
        {
            resultUnit.leader = true;
            result.name = unit.name;
        }
        int index = 0;
        for (int k = 0; k < 6; ++k)
        {
            if (stack.pos[k] == i)
            {
                index = k;
                break;
            }
        }

        auto gunit = RESOLVE(DBFModel)->get<Gunit>(resultUnit.id);
        if (!gunit->size_small)
        {
            if (usedIds.contains(id))
                continue;
            usedIds << id;
            resultUnit.uid.second = unit.uid.second;
            mapOut->replaceObject(resultUnit);
            result.addUnit(resultUnit.uid, QPoint(0, index / 2));
            continue;
        }

        resultUnit.uid.second = unit.uid.second;
        mapOut->replaceObject(resultUnit);
        result.addUnit(resultUnit.uid, QPoint((index + 1) % 2, index / 2));
    }
    return result;
}

GroupData importTemplate(const D2StackTemplate & stack, const QString &leader, GameMap * mapOut)
{
    D2StackTemplate::StackTemplateData stackData = stack.stack;
    GroupData result;
    QList<int> used;
    for(int i = 0; i < 6; ++i)
    {
        if(stackData.unit[i] == "G000000000")
            continue;
        const QString & id = stackData.unit[i];
        UnitObject *resultUnit = new UnitObject;
        resultUnit->id = id;
        resultUnit->level = stackData.level[i];
        int index = 0;
        for (int k = 0; k < 6; ++k)
        {
            if (used.contains(k))
                continue;
            if (stackData.unit[stackData.pos[k]] == id &&
                stackData.level[stackData.pos[k]] == resultUnit->level)
            {
                index = k;
                break;
            }
        }
        used << index;
        foreach (auto mod, stack.mods)
        {
            if (mod.index == index)
                resultUnit->modifiers<<mod.modifier;
        }

        auto gunit = RESOLVE(DBFModel)->get<Gunit>(resultUnit->id);
        resultUnit->name = gunit->name_txt->text;
        resultUnit->currentHp = calcLeveledValue(gunit->hit_point, gunit->level, resultUnit->level,
                                                 gunit->dyn_upg_lv, gunit->dyn_upg1->hit_point,
                                                 gunit->dyn_upg2->hit_point);

        if (!gunit->size_small)
        {
            if (id == leader && id != QString())
            {
                resultUnit->leader = true;
                result.name = stack._Name;
            }
            auto uid = mapOut->addObject(QSharedPointer<UnitObject>(resultUnit));
            result.addUnit(uid, QPoint(0, index / 2));
            continue;
        }

        if (id == leader && id != QString())
        {
            resultUnit->leader = true;
            result.name = stack._Name;
        }
        auto uid = mapOut->addObject(QSharedPointer<UnitObject>(resultUnit));
        result.addUnit(uid, QPoint((index + 1) % 2, index / 2));
    }
    return result;
}

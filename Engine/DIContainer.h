#ifndef DICONTAINER_H
#define DICONTAINER_H
#include <QHash>
#include <QSharedPointer>
#include <QDebug>
#include <typeinfo>

class IComponentFactory
{
public:
    ~IComponentFactory    (){}
};

template<class classT>
QString className()
{
    return typeid(classT).name();
}
class DiContainer;

template<class resultT>
class ComponentFactory : public IComponentFactory
{
public:
    typedef resultT* (*creationFunction)(const DiContainer& container);
    ComponentFactory(creationFunction f, bool isSingle = false): m_function(f), m_isSingleInstance(isSingle){}

    QSharedPointer<resultT> create(const DiContainer& container)
    {
        if (!m_isSingleInstance)
            return QSharedPointer<resultT>(m_function(container));
        if (m_data.isNull())
            m_data = QSharedPointer<resultT>(QSharedPointer<resultT>(m_function(container)));
        return m_data;
    }

    ~ComponentFactory() {qDebug()<<QString("Factory fo type %1 destroyed.").arg(className<resultT>());}
private:
    creationFunction m_function;
    bool m_isSingleInstance;
    QSharedPointer<resultT> m_data;
};


class DiContainer
{
public:

    enum RegistrationType
    {
        SingleInstance,
        UnmanadgesMultyInstance
    };

    template<class classT, RegistrationType type>
    void registerFactory(typename ComponentFactory<classT>::creationFunction f)
    {
        m_factories.insert(className<classT>(), QSharedPointer<IComponentFactory>(new ComponentFactory<classT>(f, type == SingleInstance)));
    }

    template<class classT>
    void registerFactory(typename ComponentFactory<classT>::creationFunction f)
    {
        m_factories.insert(className<classT>(), QSharedPointer<IComponentFactory>(new ComponentFactory<classT>(f)));
    }

    template<class classT>
    QSharedPointer<classT> resolve() const
    {
        ComponentFactory<classT> * factory = static_cast<ComponentFactory<classT>* >(m_factories[className<classT>()].data());
        return factory->create(*this);
    }


private:
    QHash<QString, QSharedPointer<IComponentFactory> > m_factories;
};


template<class classT>
classT* createFunction(const DiContainer& c)
{
    Q_UNUSED(c);
    return new classT();
}

template<class depT, class classT>
classT* createFunction(const DiContainer& c)
{
    return new classT(c.resolve<depT>());
}

template<class depT1, class depT2, class classT>
classT* createFunction(const DiContainer& c)
{
    return new classT(c.resolve<depT1>(), c.resolve<depT2>());
}


#endif // DICONTAINER_H

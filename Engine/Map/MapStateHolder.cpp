#include "MapStateHolder.h"
#include "Engine/GameInstance.h"
#include "Events/MapEvents.h"
#include "Events/EventBus.h"
#include "Engine/Components/AccessorHolder.h"
#include "Engine/SettingsManager.h"

MapStateHolder::MapStateHolder()
{

}

GameMap & MapStateHolder::map()
{
    return m_mapStates.last();
}

const GameMap &MapStateHolder::map() const
{
    return m_mapStates.last();
}

void MapStateHolder::setMap(const GameMap &newMap)
{
    m_mapStates.clear();
    m_mapStates.push(newMap);
    RESOLVE(EventBus)->notify(MapLoadedEvent());
}

bool MapStateHolder::hasObject(QPair<int, int> uid)
{
    return map().hasObject(uid);
}

QSharedPointer<MapObject> MapStateHolder::objectById(QPair<int, int> uid) const
{
    return map().objectById(uid);
}

void MapStateHolder::replaceObject(QSharedPointer<MapObject> obj)
{
    map().replaceObject(obj);
    if (obj->populate)
    {
        MapGrid grid = map().getGrid();
        for (int i = 0; i < map().mapSize(); ++i)
        {
            for (int k = 0; k < map().mapSize(); ++k) {
                grid.objBinging.binding[i][k].items.removeAll(obj->uid);
            }
        }
        QSharedPointer<IMapObjectAccessor> accessor =
            RESOLVE(AccessorHolder)->objectAccessor(obj->uid.first);
        int w = accessor->getW(obj);
        int h = accessor->getH(obj);
        int x = obj->x;
        int y = obj->y;
        for(int i = 0; i < w; ++i)
        {
            for(int k = 0; k < h; ++k)
            {
                grid.objBinging.binding[x + i][y + k].items << obj->uid;
            }
        }
        setGrid(grid);
    }
    m_hasChanges = true;
    RESOLVE(EventBus)->notify(ObjectChangedEvent(obj->uid));
}

void MapStateHolder::removeObject(QPair<int, int> uid)
{
    MapGrid grid = map().getGrid();
    for (int i = 0; i < map().mapSize(); ++i)
    {
        for (int k = 0; k < map().mapSize(); ++k)
        {
            if (uid.first == MapObject::Mountain && grid.objBinging.binding[i][k].items.contains(uid))
                grid.cells[i][k].value = 5;
            grid.objBinging.binding[i][k].items.removeAll(uid);
        }
    }
    setGrid(grid);
    map().removeObject(uid);
    m_hasChanges = true;
    RESOLVE(EventBus)->notify(ObjectRemovedEvent(uid));
}

MapGrid MapStateHolder::getGrid() const
{
    return map().getGrid();
}

void MapStateHolder::setGrid(const MapGrid &value)
{
    map().setGrid(value);
    m_hasChanges = true;
    RESOLVE(EventBus)->notify<GridChangedEvent>(GridChangedEvent());
}

size_t MapStateHolder::mapSize() const
{
    return map().mapSize();
}

QList<QSharedPointer<MapObject> > MapStateHolder::objects() const
{
    return map().objects();
}

ObjectsCollection MapStateHolder::collectionByType(size_t type) const
{
    return map().collectionByType(type);
}

const QString &MapStateHolder::getMapName() const
{
    return map().getMapName();
}

void MapStateHolder::setMapName(const QString &newMapName)
{
    map().setMapName(newMapName);
    m_hasChanges = true;
}

const QString &MapStateHolder::getMapDesc() const
{
    return map().getMapDesc();
}

void MapStateHolder::setMapDesc(const QString &newMapDesc)
{
    map().setMapDesc(newMapDesc);
    m_hasChanges = true;
}

QPair<int, int> MapStateHolder::addObject(QSharedPointer<MapObject> obj)
{
    auto result =  map().addObject(obj);
    obj->uid = result;
    if (obj->populate)
    {
        QSharedPointer<IMapObjectAccessor> accessor =
            RESOLVE(AccessorHolder)->objectAccessor(result.first);
        int w = accessor->getW(obj);
        int h = accessor->getH(obj);
        int mapSize = map().mapSize();
        MapGrid grid = map().getGrid();
        int x = obj->x;
        int y = obj->y;
        for(int i = 0; i < w; ++i)
        {
            for(int k = 0; k < h; ++k)
            {
                if (x + i >= mapSize || y + k >= mapSize)
                    continue;
                if (x + i < 0 || y + k < 0)
                    continue;
                grid.objBinging.binding[x + i][y + k].items << result;
                if (result.first == MapObject::Mountain)
                    grid.cells[i][k].value = 37;
            }
        }
        setGrid(grid);
    }
    RESOLVE(EventBus)->notify<ObjectAddedEvent>(ObjectAddedEvent(obj));
    m_hasChanges = true;
    return result;
}

void MapStateHolder::init()
{
    m_editsCount = RESOLVE(SettingsManager)->getInt("undoLimit", 10);
}

bool MapStateHolder::hasChanges() const
{
    return m_hasChanges;
}

#pragma once

#include <QPair>
#include <toolsqt/MapUtils/DataBlock.h>



static constexpr inline std::uint8_t tileTerrain(std::uint32_t tile)
{
    return static_cast<std::uint8_t>(tile & 7);
}

static constexpr inline std::uint8_t tileGround(std::uint32_t tile)
{
    return static_cast<std::uint8_t>((tile >> 3) & 7);
}

static constexpr inline std::uint8_t tileForestImage(std::uint32_t tile)
{
    return tile >> 26;
}



#ifndef MAPSTATEHOLDER_H
#define MAPSTATEHOLDER_H
#include "MapObjects/GameMap.h"
#include <QStack>

class MapChange
{

};

class MapStateHolder
{
public:
    enum MapIndex
    {
        Initial = 0,
        Working,
        CurrentState
    };

    MapStateHolder();
    GameMap &map();
    const GameMap &map() const;
    void setMap(const GameMap & newMap);

    bool hasObject(QPair<int, int> uid);
    QSharedPointer<MapObject> objectById(QPair<int, int> uid) const;
    template <class classT>
    classT objectByIdT(QPair<int, int> uid) const
    {
        auto object = objectById(uid);
        if (object.isNull())
        {
            qDebug()<<"Failed to get object by id:"<<uid;
            return classT();
        }
        return classT(*static_cast<classT *>(object.data()));

    }
    void replaceObject(QSharedPointer<MapObject> obj);
    template <class classT>
    void replaceObject(const classT & obj)
    {
        replaceObject(QSharedPointer<MapObject>(new classT(obj)));
    }
    void removeObject(QPair<int, int> uid);

    MapGrid getGrid() const;
    void setGrid(const MapGrid &value);

    size_t mapSize() const;

    QList<QSharedPointer<MapObject> > objects() const;
    ObjectsCollection collectionByType(size_t type) const;
    template <class classT>
    QList<classT> objectsByType()
    {
        return map().objectsByType<classT>();
    }
    const QString &getMapName() const;
    void setMapName(const QString &newMapName);

    const QString &getMapDesc() const;
    void setMapDesc(const QString &newMapDesc);
    QPair<int, int> addObject(QSharedPointer<MapObject> obj);
    template <class classT>
    QPair<int, int> addObject(classT & obj)
    {
        obj.uid = addObject(QSharedPointer<MapObject>(new classT(obj)));
        return obj.uid;
    }

    void startEdit()
    {
        m_mapStates.push(m_mapStates.last());
        if (m_mapStates.count() > m_editsCount)
            m_mapStates.removeFirst();
        m_hasChanges = false;
    }

    void acceptEdit()
    {
        auto state = m_mapStates.pop();
        m_mapStates.pop();
        m_mapStates.push(state);
        m_hasChanges = false;
    }

    void rejectEdit()
    {
        m_mapStates.pop();
        m_hasChanges = false;
    }

    void init();
    bool hasChanges() const;

private:
    QStack<GameMap> m_mapStates;
    int m_editsCount = 10;
    bool m_hasChanges = false;
};

#endif // MAPSTATEHOLDER_H

#ifndef PRESETMANAGER_H
#define PRESETMANAGER_H

#include <QObject>
#include <QHash>

class PresetManager
{
public:
    struct Preset
    {
        QString name;
        QStringList data;
    };
    explicit PresetManager();
    ~PresetManager();
    QList<Preset> presetList(const QString& type);

    bool load(const QString & type);
    bool save(const QString & type);
    void save();
    void addPreset(const QString& type, const QString& name, const QStringList& data);
signals:
    void presetsChanged();
private:
    QHash<QString, QList<Preset>> m_presets;
    int m_currentPreset;

    QString m_type;

};

#endif // PRESETMANAGER_H

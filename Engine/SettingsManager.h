#ifndef SETTINGSMANAGER_H
#define SETTINGSMANAGER_H
#include <QHash>
#include <QString>
#include <QList>
#include "Option.h"

#include <QJsonObject>
#include <QJsonArray>
#include <QJsonDocument>
#include <QFile>

class SettingsManager
{
public:
    SettingsManager();
    void addOption(const Option& option);

    bool setValue(const QString &key, const bool &value);
    bool setValue(const QString &key, const QString &value);
    QString get(const QString &key, const QString & defaultValue = QString()) const;
    int getInt(const QString &key, const int & defaultValue = 0) const;
    float getFloat(const QString &key, const float & defaultValue = 0) const;
    bool getBool(const QString &key, const bool & defaultValue = 0) const;
    bool hasOption(const QString& key) const;
    bool removeOption(const QString& key);

    bool load(const QString& path);
    bool loadSimple(const QString& path);
    bool save(const QString& path);
    bool saveSimple(const QString& path);
    bool loaded() const;
    void setLoaded(bool newLoaded);

    void setSettings(const QList<Option> &newSettings);
    const QList<Option> &settings() const;

private:
    QList<Option> m_settings;
    bool m_loaded = false;
};

#endif // SETTINGSMANAGER_H

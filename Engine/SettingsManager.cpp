#include "SettingsManager.h"
#include "Commands/UIEvents.h"
#include "Engine/GameInstance.h"
#include "Events/EventBus.h"

SettingsManager::SettingsManager()
{

}

void SettingsManager::addOption(const Option &option)
{
    m_settings.append(option);
}

bool SettingsManager::setValue(const QString &key, const bool &value)
{
    return setValue(key, QString(value?"True":"False"));
}

bool SettingsManager::setValue(const QString &key, const QString &value)
{
    bool result = Option::setValueByName(m_settings, key, value);
    RESOLVE(EventBus)->notify(SettingsChangedEvent{"", key});
    return result;
}

QString SettingsManager::get(const QString &key, const QString &defaultValue) const
{
    if (hasOption(key))
        return Option::getOptionValue(m_settings, key);
    return defaultValue;
}

int SettingsManager::getInt(const QString &key, const int &defaultValue) const
{
    if (hasOption(key))
        return Option::getIntOption(m_settings, key);
    return defaultValue;
}

float SettingsManager::getFloat(const QString &key, const float &defaultValue) const
{
    if (hasOption(key))
        return Option::getFloatOption(m_settings, key);
    return defaultValue;
}

bool SettingsManager::getBool(const QString &key, const bool &defaultValue) const
{
    if (hasOption(key))
        return Option::getBoolOption(m_settings, key);
    return defaultValue;
}

bool SettingsManager::hasOption(const QString &key) const
{
    return Option::hasOption(m_settings, key);
}

bool SettingsManager::removeOption(const QString &key)
{
    for(int i = 0; i < m_settings.count(); ++i)
    {
        if (m_settings[i].name == key)
        {
            m_settings.removeAt(i);
            return true;
        }
    }
    return false;
}

bool SettingsManager::load(const QString &path)
{
    QFile jsonFile(path);
    if (!jsonFile.open(QIODevice::ReadOnly))
    {
        qDebug()<<"Error opening file: " + path;
        return false;
    }
    QByteArray saveData = jsonFile.readAll();
    jsonFile.close();
    QJsonDocument jsonDocument(QJsonDocument::fromJson(saveData));
    QJsonArray array = jsonDocument.array();

    for (int i = 0; i < array.count(); ++i)
    {
        const QJsonObject& object = array.at(i).toObject();
        Option option;
        if (object.contains("name")) option.name = object["name"].toString();
        if (object.contains("desc")) option.desc = object["desc"].toString();
        if (object.contains("value")) option.value = object["value"].toString();
        if (object.contains("min")) option.min = object["min"].toString();
        if (object.contains("defValue")) option.defValue = object["defValue"].toString();
        if (object.contains("max")) option.max = object["max"].toString();
        if (object.contains("group")) option.group = object["group"].toString();
        if (object.contains("type")) option.type = (Option::Type)object["type"].toInt();
        if (object.contains("status")) option.status = (Option::Status)object["status"].toInt();
        if (object.contains("variants"))
        {
            QJsonArray variants = object["variants"].toArray();
            for (int i = 0; i < variants.count(); ++i)
            {
                option.variants << variants.at(i).toString();
            }
        }
        if (object.contains("variantValues"))
        {
            QJsonArray variants = object["variantValues"].toArray();
            for (int i = 0; i < variants.count(); ++i)
            {
                option.variantValues << variants.at(i).toString();
            }
        }
        m_settings.append(option);
    }
    m_loaded = true;
    return true;
}

bool SettingsManager::loadSimple(const QString &path)
{
    QFile jsonFile(path);
    if (!jsonFile.open(QIODevice::ReadOnly))
    {
        qDebug()<<"Error opening file: " + path;
        return false;
    }
    QByteArray jsonData = jsonFile.readAll();
    QJsonDocument jsonDocument = QJsonDocument::fromJson(jsonData);
    if (jsonDocument.isNull())
    {
        qDebug() << "Invalid JSON in file: " + path;
        return false;
    }

    QJsonObject jsonObject = jsonDocument.object();
    foreach(const QString &key, jsonObject.keys())
    {
        Option option;
        option.name = key;
        option.value = jsonObject.value(key).toString();
        m_settings.append(option);
    }
    jsonFile.close();
    m_loaded = true;
    qDebug()<<"Load simple:";
    foreach(const Option & option, m_settings)
    {
        qDebug()<<option.name << option.value;
    }

    return true;
}

bool SettingsManager::save(const QString &path)
{
    QFile jsonFile(path);
    if (!jsonFile.open(QIODevice::WriteOnly))
    {
        qDebug()<<"Error opening file: " + path;
        return false;
    }

    QJsonArray array;
    foreach(const Option & option, m_settings)
    {
        QJsonObject result;
        result["name"] = option.name;
        result["desc"] = option.desc;
        result["group"] = option.group;
        result["value"] = option.value;
        result["defValue"] = option.defValue;
        if (option.type == Option::Float || option.type == Option::Int)
        {
            result["min"] = option.min;
            result["max"] = option.max;
        }
        result["type"] = (int)option.type;
        result["status"] = (int)option.status;
        if (option.variants.count() > 0)
        {
            QJsonArray variants;
            foreach(const QString& str, option.variants)
                variants << str;
            result["variants"] = variants;
        }
        if (option.variants.count() > 0)
        {
            QJsonArray variants;
            foreach(const QString& str, option.variantValues)
                variants << str;
            result["variantValues"] = variants;
        }
        array << result;
    }

    QJsonDocument jsonDocument(array);
    jsonFile.write(jsonDocument.toJson(QJsonDocument::Indented));
    jsonFile.close();
    return true;
}

bool SettingsManager::saveSimple(const QString &path)
{
    QFile jsonFile(path);
    if (!jsonFile.open(QIODevice::WriteOnly))
    {
        qDebug()<<"Error opening file: " + path;
        return false;
    }
    QJsonObject result;
    foreach(const Option & option, m_settings)
    {
        result[option.name] = option.value;
    }

    QJsonDocument jsonDocument(result);
    jsonFile.write(jsonDocument.toJson(QJsonDocument::Indented));
    jsonFile.close();
    return true;
}

bool SettingsManager::loaded() const
{
    return m_loaded;
}

void SettingsManager::setLoaded(bool newLoaded)
{
    m_loaded = newLoaded;
}

void SettingsManager::setSettings(const QList<Option> &newSettings)
{
    m_settings = newSettings;
}

const QList<Option> &SettingsManager::settings() const
{
    return m_settings;
}

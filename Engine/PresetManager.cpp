#include "PresetManager.h"
#include <QHash>
#include <QFile>
#include "toolsqt/Common/Logger.h"
#include <QJsonDocument>
#include <QJsonArray>
#include <QJsonObject>

PresetManager::PresetManager()
{

}

PresetManager::~PresetManager()
{
    save();
}

QList<PresetManager::Preset> PresetManager::presetList(const QString &type)
{
    if (!m_presets.contains(type))
        load(type);
    return m_presets[type];
}

bool PresetManager::load(const QString &type)
{
    if (type.isEmpty())
    {
        return false;
    }

    m_presets[type].clear();
    QString filename = "Presets/" + type + ".json";

    QFile file(filename);
    if (!file.exists())
    {
        return false;
    }

    if (!file.open(QIODevice::ReadOnly))
    {
        LOG_ERROR("Error opening file: " + filename);
        return false;
    }

    QByteArray saveData = file.readAll();
    file.close();

    QJsonDocument jsonDocument(QJsonDocument::fromJson(saveData));
    QJsonArray array = jsonDocument.array();

    for (int i = 0; i < array.count(); ++i)
    {
        const QJsonObject& object = array.at(i).toObject();
        Preset preset;
        if (object.contains("name")) preset.name = object["name"].toString();

        if (object.contains("data"))
        {
            QJsonArray variants = object["data"].toArray();
            for (int i = 0; i < variants.count(); ++i)
            {
                preset.data << variants.at(i).toString();
            }
        }

        m_presets[type].append(preset);
    }
    return true;
}

bool PresetManager::save(const QString &type)
{
    QString filename = "Presets/" + type + ".json";
    QFile jsonFile(filename);
    if (!jsonFile.open(QIODevice::WriteOnly))
    {
        LOG_ERROR("Error opening file: " + filename);
        return false;
    }

    QJsonArray array;
    foreach(const Preset & preset, m_presets[type])
    {
        QJsonObject result;
        result["name"] = preset.name;
        if (preset.data.count() > 0)
        {
            QJsonArray variants;
            foreach(const QString& str, preset.data)
                variants << str;
            result["data"] = variants;
        }
        array << result;
    }

    QJsonDocument jsonDocument(array);
    jsonFile.write(jsonDocument.toJson(QJsonDocument::Indented));
    jsonFile.close();
    return true;
}

void PresetManager::save()
{
    foreach (const QString& key, m_presets.keys())
    {
        save(key);
    }
}

void PresetManager::addPreset(const QString &type, const QString &name, const QStringList &data)
{
    for(int i = 0; i < m_presets[type].count(); ++i)
    {
        if (m_presets[type][i].name == name)
        {
            m_presets[type][i].data = data;
            return;
        }
    }
    Preset preset;
    preset.name = name;
    preset.data = data;
    m_presets[type].append(preset);
}

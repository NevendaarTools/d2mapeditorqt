#include "TranslationHelper.h"
#include <QDir>
#include <QDebug>
#include "Engine/GameInstance.h"
#include "toolsqt/DBFModel/DBFModel.h"

bool TranslationHelper::loadTranslation(const QString &path)
{
    QFile inputFile(path);
    if (inputFile.open(QIODevice::ReadOnly))
    {
        QFileInfo info(inputFile);
        Translation translation;
        translation.lang = info.baseName();
        QTextStream in(&inputFile);
        while (!in.atEnd())
        {
            QString line = in.readLine();
            QStringList data = line.split("==>");
            if (data.count() < 2)
                continue;
            translation.data.insert(data[0], data[1]);
        }
        m_translations.insert(translation.lang, translation);
        inputFile.close();
        return true;
    }
    return false;
}

void TranslationHelper::init(const QString &path)
{
    QFileInfoList dirContent = QDir(path).entryInfoList(QStringList()<< "*.tr",QDir::Files);
    foreach(const QFileInfo & info, dirContent)
    {
        loadTranslation(info.absoluteFilePath());
    }
    m_tApp.loadFile(path + "/mappingTApp.txt");
    m_tAppEdit.loadFile(path + "/mappingTAppEdit.txt");
}

const QString &TranslationHelper::current() const
{
    return m_current;
}

void TranslationHelper::setCurrent(const QString &newCurrent)
{
    m_current = newCurrent;
}

QString TranslationHelper::tr(const QString &text)
{
    return instance()->tr_(text);
}

void TranslationHelper::loadHardcode()
{
    m_tApp.fillFromTApp();
    m_tAppEdit.fillFromTAppEdit();
}

QString TranslationHelper::tr_(const QString &text)
{
    if (m_current.isEmpty())
    {
        //AddMismatchedTranslation(text, "");
        return text;
    }
    if (text == "")
        return text;
    if (m_tApp.hasKey(text))
        return m_tApp.tr(text);
    if (m_tAppEdit.hasKey(text))
        return m_tAppEdit.tr(text);
    if (m_translations.contains(m_current))
        return m_translations[m_current].tr(text);

    //qDebug()<<"Translation not found: ["<<m_current<<"] "<<text;
    return text;
}

QString TranslationHelper::Translation::tr(const QString &text)
{
    if (data.contains(text))
        return data[text];
    //qDebug()<<"Translation not found: "<<QString("%1==>%2").arg(text).arg(lang);
    return text;
}

bool HardcodeHelper::loadFile(const QString &path)
{
    QFile inputFile(path);
    if (inputFile.open(QIODevice::ReadOnly))
    {
        QTextStream in(&inputFile);
        while (!in.atEnd())
        {
            QString line = in.readLine();
            QStringList data = line.split("==>");
            if (data.count() < 2)
                continue;
            m_binding.insert(data[0], data[1]);
        }
        inputFile.close();
        return true;
    }
    return false;
}

void HardcodeHelper::fillFromTApp()
{
    m_loadedData.clear();
    auto dbf = GameInstance::instance()->get<DBFModel>();
    QList<QString> keys = m_binding.keys();
    foreach (const QString& key, keys) {
        QString subKey = m_binding[key];
        QList<QString> data = subKey.split(" ");
        QString result;
        foreach (const QString & tmp, data) {
            QSharedPointer<TApp> value = dbf->get<TApp>(tmp);
            if (value.isNull())
            {
                qDebug()<<("HardcodeHelper: Failed to load TApp for key - " + key);
                continue;
            }
            result += value->text + " ";
        }

        m_loadedData.insert(key, result);
    }
}

void HardcodeHelper::fillFromTAppEdit()
{
    m_loadedData.clear();
    auto dbf = GameInstance::instance()->get<DBFModel>();
    QList<QString> keys = m_binding.keys();
    foreach (const QString& key, keys) {
        QString subKey = m_binding[key];
        QList<QString> data = subKey.split(" ");
        QString result;
        foreach (const QString & tmp, data) {
            QSharedPointer<TAppEdit> value = dbf->get<TAppEdit>(tmp);
            if (value.isNull())
            {
                qDebug()<<("HardcodeHelper: Failed to load TAppEdit for key - " + key);
                continue;
            }
            result += value->text + " ";
        }

        m_loadedData.insert(key, result);
    }
}

bool HardcodeHelper::hasKey(const QString &key) const
{
    return m_loadedData.contains(key);
}

QString HardcodeHelper::tr(const QString &key) const
{
    return m_loadedData.value(key);
}

QString getTAppText(const QString &key)
{
    auto value = RESOLVE(DBFModel)->get<TApp>(key);
    if (value.isNull())
        return QString();
    return value->text;
}

QString getTAppEditText(const QString &key)
{
    auto value = RESOLVE(DBFModel)->get<TAppEdit>(key);
    if (value.isNull())
        return QString();
    return value->text;
}

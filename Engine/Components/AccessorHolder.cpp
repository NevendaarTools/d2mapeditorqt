#include "AccessorHolder.h"

AccessorHolder::AccessorHolder() {}

QSharedPointer<IMapObjectAccessor> AccessorHolder::objectAccessor(size_t type)
{
    return m_accessors[type];
}

void AccessorHolder::registerAccessor(size_t type, QSharedPointer<IMapObjectAccessor> accessor)
{
    m_accessors.insert(type, accessor);
}

#ifndef DISPLAYSETTINGSMANAGER_H
#define DISPLAYSETTINGSMANAGER_H
#include <QString>
#include <QList>
#include "MapObjects/MapObject.h"

struct LayersSettings
{
    LayersSettings() {hidenObjects<<MapObject::Location;}
    bool gridEnabled = false;
    bool passableEnabled = false;
    bool terraformingEnabled = false;
    bool dangerEnabled = false;
    bool forestEnabled = false;
    bool roadsEnabled = false;
    bool simpleView = false;
    QList<int> hidenObjects;
};

class DisplaySettingsManager
{
public:
    DisplaySettingsManager();
    LayersSettings settings;
};

#endif // DISPLAYSETTINGSMANAGER_H

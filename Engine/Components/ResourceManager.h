#ifndef RESOURCEMANAGER_H
#define RESOURCEMANAGER_H
#include <QString>
#include "toolsqt/ResourceModel/GameResource.h"
#include "toolsqt/ResourceModel/ResourceModel.h"
#define TILE_SIZE 32//96/2


class ImagesData
{
public:
    enum Status
    {
        Ready = 0,
        Loadind,
        NotFound,
        None
    };
    Status status = None;
    QList<QImage> data;
};

struct FrameData
{
    QSharedPointer<ImagesData> images;
    int currentFrame = 0;
    QList<QRect> rects;

    bool nextFrame()
    {
        if (!images)
            return true;
        if (images->data.count() == 1)
            return false;
        if (currentFrame + 1 >= images->data.count())
            currentFrame = 0;
        else
            currentFrame ++;
        return true;
    }
};

class ResourceManager
{
public:
    ResourceManager(QSharedPointer<ResourceModel> resourceModel);
    void init(const QString & path);
    bool hasImagesData(const QString &id);
    bool hasLoadedImagesData(const QString &id);
    QSharedPointer<ImagesData> getImagesData(const QStringList &resources, const QString &id, bool async = true,
                                             PreprocessingShaderType shader = PreprocessingShaderType::DefaultWithSizeOptimisation);
    QSharedPointer<ImagesData> getImagesData(const QString &resource, const QString &id, bool async = true,
                                             PreprocessingShaderType shader = PreprocessingShaderType::DefaultWithSizeOptimisation);
    QSharedPointer<ImagesData> getImagesData(const QString &id);


    QImage getImageNoCash(const QStringList &resources, const QString &id,
                          PreprocessingShaderType shader = PreprocessingShaderType::DefaultWithSizeOptimisation);
    QImage getImageNoCash(const QString &id);
    QImage getImageNoCash(const QString &resource, const QString &id,
                          PreprocessingShaderType shader = PreprocessingShaderType::DefaultWithSizeOptimisation);


    void addImage(const QImage &pix, const QString &key);
    void addImageList(const QList<QImage> &pixmaps, const QString &key);
    void removeImage(const QString &key);
private:
    QSharedPointer<ResourceModel> m_resourceModel;
    QMap<QString, QSharedPointer<ImagesData>> m_imagesMap;
};

#endif // RESOURCEMANAGER_H

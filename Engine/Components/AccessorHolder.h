#ifndef ACCESSORHOLDER_H
#define ACCESSORHOLDER_H
#include <QHash>
#include <QSharedPointer>
#include "MapObjects/MapObject.h"
#include "ResourceManager.h"

class IMapObjectAccessor
{
public:
    virtual ~IMapObjectAccessor(){}
    virtual qreal getZ(QSharedPointer<MapObject> object) const  = 0;
    virtual int getW(QSharedPointer<MapObject> object) const  = 0;
    virtual int getH(QSharedPointer<MapObject> object) const  = 0;

    virtual QString getName(QSharedPointer<MapObject> object) const  = 0;
    virtual QString getDesc(QSharedPointer<MapObject> object) const  = 0;

    virtual QMenu * requestMenu(QSharedPointer<MapObject> object) const  = 0;
    virtual QList<FrameData> frameData(QSharedPointer<MapObject> object) const  = 0;
    virtual QSharedPointer<MapObject> cloneObject(QSharedPointer<MapObject> object) const = 0;
    virtual IObjectEditor * createEditor() const  = 0;
};

class MapObjectAccessor : public IMapObjectAccessor
{
public:
    qreal getZ(QSharedPointer<MapObject> object) const override{return 15;}
    int getW(QSharedPointer<MapObject> object) const override{return 1;}
    int getH(QSharedPointer<MapObject> object) const override{return 1;}

    QString getName(QSharedPointer<MapObject> object) const override{return "";}
    QString getDesc(QSharedPointer<MapObject> object) const override{return "";}

    QMenu * requestMenu(QSharedPointer<MapObject> object) const override
    {
        return nullptr;
    }

    QList<FrameData> frameData(QSharedPointer<MapObject> object) const override
    {
        return QList<FrameData>();
    }

    virtual IObjectEditor * createEditor() const override
    {
        return nullptr;
    }
    virtual QSharedPointer<MapObject> cloneObject(QSharedPointer<MapObject> object) const
    {
        return QSharedPointer<MapObject>();
    }
};

class AccessorHolder
{
public:
    AccessorHolder();
    QSharedPointer<IMapObjectAccessor> objectAccessor(size_t type);
    void registerAccessor(size_t type, QSharedPointer<IMapObjectAccessor> accessor);
private:
    QHash<size_t, QSharedPointer<IMapObjectAccessor>> m_accessors;
};

#endif // ACCESSORHOLDER_H

#include "ResourceManager.h"
#include <QDir>
#include <QDebug>
#include <QMovie>
#include <QFuture>
#include <QtConcurrent/QtConcurrent>

ResourceManager::ResourceManager(QSharedPointer<ResourceModel> resourceModel) : m_resourceModel(resourceModel)
{

}

void ResourceManager::init(const QString &path)
{
    m_imagesMap.clear();
    m_resourceModel->init(path);
    QDir dataDir;
    dataDir.setPath("Images");
    QStringList files = dataDir.entryList(QStringList()<<"*.png"<<"*.jpg", QDir::Files);
    foreach (QString fileName, files) {
        QString tmpName = fileName;
        tmpName.remove(tmpName.lastIndexOf("."),4);
        addImage(QImage(dataDir.path() + "/" + fileName), tmpName);
    }

    files = dataDir.entryList(QStringList()<<"*.gif", QDir::Files);
    foreach (QString fileName, files) {
        QString tmpName = fileName;
        tmpName.remove(tmpName.lastIndexOf("."),4);
        QMovie* movie = new QMovie(dataDir.path() + "/" + fileName);
        if (movie->isValid())
        {
            QList <QImage> pixmaps;
            for (int i = 0; i < movie->frameCount(); ++i)
            {
                if (!movie->jumpToFrame(i))
                    continue;
                pixmaps << movie->currentImage();
            }
            addImageList(pixmaps, tmpName);
        }
        else
            qDebug()<< movie->lastErrorString();

    }
}

QSharedPointer<ImagesData> ResourceManager::getImagesData(const QStringList &resources, const QString &id, bool async, PreprocessingShaderType shader)
{
    QString resultId = resources.join("-") + "-" + id;
    if (m_imagesMap.contains(resultId))
        return m_imagesMap[resultId];
    QSharedPointer<ImagesData> result = QSharedPointer<ImagesData>(new ImagesData());
    result->status = ImagesData::Loadind;
    if (!async)
    {
        result->data = m_resourceModel->getFramesById(resources, id, shader);
        if (result->data.count() == 0)
            qDebug()<<("Failed to get images: " + id);
        result->status = ImagesData::Ready;
        m_imagesMap.insert(resultId, result);
        return result;
    }
    m_imagesMap.insert(resultId, result);
    //loadingData << result;
    QFuture<void> future = QtConcurrent::run([this, result, resources, id, shader]()
                                             {
                                                 result->data = m_resourceModel->getFramesById(resources, id, shader);
                                                 if (result->data.count() == 0)
                                                     qDebug()<<("Failed to get images: " + id);
                                                 result->status = ImagesData::Ready;
                                             });

    return result;
}

QSharedPointer<ImagesData> ResourceManager::getImagesData(const QString &resource, const QString &id, bool async, PreprocessingShaderType shader)
{
    QString resultId = resource + "-" + id;
    if (m_imagesMap.contains(resultId))
        return m_imagesMap[resultId];
    QSharedPointer<ImagesData> result = QSharedPointer<ImagesData>(new ImagesData());
    result->status = ImagesData::Loadind;
    if (!async)
    {
        result->data = m_resourceModel->getFramesById(resource, id, shader);
        if (result->data.count() == 0)
            qDebug()<<("Failed to get images: " + id);
        result->status = ImagesData::Ready;
        m_imagesMap.insert(resultId, result);
        return result;
    }
    m_imagesMap.insert(resultId, result);
    QFuture<void> future = QtConcurrent::run([this, result, resource, id, shader]()
                                             {
                                                 result->data = m_resourceModel->getFramesById(resource, id, shader);
                                                 if (result->data.count() == 0)
                                                     qDebug()<<("Failed to get images: " + id);
                                                 result->status = ImagesData::Ready;
                                             });

    return result;
}

QSharedPointer<ImagesData> ResourceManager::getImagesData(const QString &id)
{
    if (m_imagesMap.contains(id))
        return m_imagesMap[id];
    return QSharedPointer<ImagesData>(new ImagesData());
}

bool ResourceManager::hasImagesData(const QString &id)
{
    return m_imagesMap.contains(id);
}

bool ResourceManager::hasLoadedImagesData(const QString &id)
{
    if (!hasImagesData(id))
        return false;
    return m_imagesMap[id]->status == ImagesData::Ready;
}

QImage ResourceManager::getImageNoCash(const QStringList &resources, const QString &id, PreprocessingShaderType shader)
{
    QString mergedId = resources.join("-") + "-" + id;
    if (m_imagesMap.contains(mergedId))
    {
        QSharedPointer<ImagesData> data = m_imagesMap[mergedId];
        if (data->data.count() > 0)
            return data->data[0];
    }
    QImage res = m_resourceModel->getFrameById(resources, id, shader);
    if (!res.isNull())
        return res;
    QImage im(64,64, QImage::Format_ARGB32);
    im.fill(Qt::red);
    return im;
}

QImage ResourceManager::getImageNoCash(const QString &id)
{
    if (m_imagesMap.contains(id))
    {
        QSharedPointer<ImagesData> data = m_imagesMap[id];
        if (data->data.count() > 0)
            return data->data[0];
    }
    qDebug()<<"Failed to get image:" <<id;
    QImage im(64,64, QImage::Format_ARGB32);
    im.fill(Qt::red);
    return im;
}

QImage ResourceManager::getImageNoCash(const QString &resource, const QString &id, PreprocessingShaderType shader)
{
    QImage res = m_resourceModel->getFrameById(resource, id, shader);
    if (!res.isNull())
        return res;
    qDebug()<<"Failed to get image:" <<resource<<"_"+id;
    QImage im(64,64, QImage::Format_ARGB32);
    im.fill(Qt::red);
    return im;
}

void ResourceManager::addImage(const QImage &pix, const QString &key)
{
    m_imagesMap.insert(key, QSharedPointer<ImagesData>(new ImagesData()));
    m_imagesMap[key]->data.append(pix);
    m_imagesMap[key]->status = ImagesData::Ready;
}

void ResourceManager::addImageList(const QList<QImage> &pixmaps, const QString &key)
{
    m_imagesMap.insert(key, QSharedPointer<ImagesData>(new ImagesData()));
    for (int i = 0; i < pixmaps.count(); ++i)
    {
        m_imagesMap[key]->data.append(pixmaps[i]);
    }
    m_imagesMap[key]->status = ImagesData::Ready;
}

void ResourceManager::removeImage(const QString &key)
{
    if (m_imagesMap.contains(key))
        m_imagesMap.remove(key);
}

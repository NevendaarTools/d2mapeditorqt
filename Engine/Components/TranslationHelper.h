#ifndef TRANSLATIONHELPER_H
#define TRANSLATIONHELPER_H

#include <QObject>
#include <QHash>
#include <QDebug>
#include <QFile>
#include <QFileInfo>

class HardcodeHelper
{
public:
    bool loadFile(const QString &path);
    void fillFromTApp();
    void fillFromTAppEdit();
    bool hasKey(const QString &key) const;
    QString tr(const QString& key) const;
private:
    QHash<QString, QString> m_binding;
    QHash<QString, QString> m_loadedData;
};

class TranslationHelper
{
    struct Translation
    {
        QString lang = "";
        QHash<QString, QString> data;
        QString tr(const QString & text);
    };
public:
    static TranslationHelper* instance() {
        static TranslationHelper inst;
        return &inst;
    }

    bool loadTranslation(const QString & path);
    void init(const QString& path);

    const QString &current() const;
    void setCurrent(const QString &newCurrent);
    static QString tr(const QString & text);

    void loadHardcode();
signals:
    void translationChanged();
private:
    explicit TranslationHelper(){}
    QString tr_(const QString & text);
private:
    QHash<QString, Translation> m_translations;
    QString m_current;
    HardcodeHelper m_tApp;
    HardcodeHelper m_tAppEdit;
};

QString getTAppText(const QString & key);
QString getTAppEditText(const QString & key);

#endif // TRANSLATIONHELPER_H

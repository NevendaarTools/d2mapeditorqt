#ifndef GAMEINSTANCE_H
#define GAMEINSTANCE_H
#include "DIContainer.h"

class GameInstance
{
public:
    static GameInstance* instance() {
        static GameInstance inst;
        return &inst;
    }

    template<class classT>
    QSharedPointer<classT> get() const
    {
        return m_container.resolve<classT>();
    }
    static void init(const QString &path);
    static void initInternal();
private:
    GameInstance();
private:
    DiContainer m_container;
};

#define RESOLVE(type) GameInstance::instance()->get<type>()

#endif // GAMEINSTANCE_H

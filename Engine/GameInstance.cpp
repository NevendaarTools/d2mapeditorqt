#include "GameInstance.h"
#include "Engine/Components/ResourceManager.h"
#include "Engine/Components/TranslationHelper.h"
#include "Engine/Components/AccessorHolder.h"
#include "toolsqt/DBFModel/DBFModel.h"
#include "Engine/Map/MapStateHolder.h"
#include "Engine/SettingsManager.h"
#include "Engine/PresetManager.h"
#include "Commands/CommandBus.h"
#include "Events/EventBus.h"
#include "Engine/GameInstance.h"
#include "toolsqt/Common/Logger.h"
#include "Events/MapEvents.h"
#include <QGuiApplication>
#include "Engine/Components/DisplaySettingsManager.h"

void initLocImages()
{
    {
        QImage pix(TILE_SIZE * 2, TILE_SIZE, QImage::Format_ARGB32);
        pix.fill(Qt::transparent);
        QPainter p(&pix);
        p.setPen(QPen(Qt::blue, 2));
        QPolygon pol;
        pol << QPoint(0, pix.height() / 2);
        pol << QPoint(pix.width() / 2, 0);
        pol << QPoint(pix.width(), pix.height() / 2);
        pol << QPoint(pix.width() / 2, pix.height());
        p.drawPolygon(pol);
        p.end();
        QString key = "loc_1";
        RESOLVE(ResourceManager)->addImage(pix, key);
    }
    {
        QImage pix(TILE_SIZE * 6, TILE_SIZE * 3, QImage::Format_ARGB32);
        pix.fill(Qt::transparent);
        QPainter p(&pix);
        p.setPen(QPen(Qt::blue, 2));
        QPolygon pol;
        pol << QPoint(0, pix.height() / 2);
        pol << QPoint(pix.width() / 2, 0);
        pol << QPoint(pix.width(), pix.height() / 2);
        pol << QPoint(pix.width() / 2, pix.height());
        p.drawPolygon(pol);
        p.end();
        QString key = "loc_3";
        RESOLVE(ResourceManager)->addImage(pix, key);
    }
    {
        QImage pix(TILE_SIZE * 10, TILE_SIZE * 5, QImage::Format_ARGB32);
        pix.fill(Qt::transparent);
        QPainter p(&pix);
        p.setPen(QPen(Qt::blue, 2));
        QPolygon pol;
        pol << QPoint(0, pix.height() / 2);
        pol << QPoint(pix.width() / 2, 0);
        pol << QPoint(pix.width(), pix.height() / 2);
        pol << QPoint(pix.width() / 2, pix.height());
        p.drawPolygon(pol);
        p.end();
        QString key = "loc_5";
        RESOLVE(ResourceManager)->addImage(pix, key);
    }
    {
        QImage pix(TILE_SIZE * 14, TILE_SIZE * 7, QImage::Format_ARGB32);
        pix.fill(Qt::transparent);
        QPainter p(&pix);
        p.setPen(QPen(Qt::blue, 2));
        QPolygon pol;
        pol << QPoint(0, pix.height() / 2);
        pol << QPoint(pix.width() / 2, 0);
        pol << QPoint(pix.width(), pix.height() / 2);
        pol << QPoint(pix.width() / 2, pix.height());
        p.drawPolygon(pol);
        p.end();
        QString key = "loc_7";
        RESOLVE(ResourceManager)->addImage(pix, key);
    }
}

void GameInstance::init(const QString &path)
{
    RESOLVE(DBFModel)->load(path);
    RESOLVE(ResourceModel)->init(path);
    TranslationHelper::instance()->loadHardcode();
    GameLoadedEvent event;
    initLocImages();
    RESOLVE(EventBus)->notify(event);
    QImage im = (RESOLVE(ResourceManager)->getImageNoCash("IsoCursr","MOVE"));
    QPixmap pixmap = QPixmap::fromImage(im);
    QCursor cursor = QCursor(pixmap, im.width() / 2, im.height() / 2);
    QGuiApplication::setOverrideCursor(cursor);
}

Option createStyleOption()
{
    const QString presetsDirPath = "Presets";
    QDir presetsDir(presetsDirPath);
    if (!presetsDir.exists())
    {
        return Option::stringOption("Presets dir not found", presetsDir.absolutePath(), "");
    }

    QStringList filters;
    filters << "style_*.json";
    presetsDir.setNameFilters(filters);
    QFileInfoList jsonFiles = presetsDir.entryInfoList(QDir::Files | QDir::Readable);

    QStringList styleNames;
    for (const QFileInfo &fileInfo : jsonFiles)
    {
        QString fileName = fileInfo.fileName();
        if (fileName.startsWith("style_") && fileName.endsWith(".json"))
        {
            QString styleName = fileName.mid(6); // style_
            styleName.chop(5);                  // .json
            styleNames << styleName;
        }
    }

    if (styleNames.isEmpty())
    {
        return Option::stringOption("Styles in presets dir not found", presetsDir.absolutePath(), "");
    }

    Option styleOption = Option::enumOption("style", "style_1280_720.json", "need restart", styleNames);
    styleOption.group = "style";
    return styleOption;
}

void GameInstance::initInternal()
{
    auto settings  = RESOLVE(SettingsManager);
    if (!settings->load("settings.json"))
    {
        //TODO: default init
        settings->addOption(
            Option::dirOption("LatestMapFolder", "", "LatestMapFolder"));
        settings->addOption(
            Option::dirOption("GamePath", "", "GamePath"));
        settings->addOption(
            Option::boolOption("logPanel", false, "logPanel"));

        settings->addOption(Option::enumOption("language", "ru", "language", QStringList()<<"ru"<<"en"));

        settings->addOption(
            Option::boolOption("logToFile", true, "logToFile"));
        settings->addOption(
            Option::boolOption("edgeMove", true, "edgeMove"));
        settings->addOption(
            Option::intOption("undoLimit", 10, "undoLimit_desc", 3, 9999));//The maximum number of actions that can be undone.
        // Лимит Отмены
        // Максимальное количество действий, которые можно отменить.
    }
    if (!settings->hasOption("edgeMove"))
        settings->addOption(Option::boolOption("edgeMove", true, "edgeMove"));
    if (!settings->hasOption("enable_map_drag"))
        settings->addOption(Option::boolOption("enable_map_drag", true, "enable_map_drag"));
    if (!settings->hasOption("version"))
        settings->addOption(Option::floatOption("version", EDITOR_VERSION));
    else
        settings->setValue("version", QString::number(EDITOR_VERSION));

    QString style;
    if (settings->hasOption("style"))
    {
        style = settings->get("style");
        settings->removeOption("style");
    }
    auto updatedStyle = createStyleOption();
    if (updatedStyle.variants.contains(style))
        updatedStyle.value = style;
    settings->addOption(updatedStyle);

    TranslationHelper::instance()->init("Translations");
    TranslationHelper::instance()->setCurrent(RESOLVE(SettingsManager)->get("language"));
    Logger::setWriteToFile(RESOLVE(SettingsManager)->getBool("logToFile"));
    Logger::setNotifier(RESOLVE(EventBus).data());
}


GameInstance::GameInstance()
{
    m_container.registerFactory<ResourceModel,
                                DiContainer::SingleInstance>(&createFunction);
    m_container.registerFactory<ResourceManager,
                                DiContainer::SingleInstance>(&createFunction<ResourceModel>);
    m_container.registerFactory<DBFModel,
                                DiContainer::SingleInstance>(&createFunction);
    m_container.registerFactory<MapStateHolder,
                                DiContainer::SingleInstance>(&createFunction);
    m_container.registerFactory<CommandBus,
                                DiContainer::SingleInstance>(&createFunction);
    m_container.registerFactory<EventBus,
                                DiContainer::SingleInstance>(&createFunction);
    m_container.registerFactory<SettingsManager,
                                DiContainer::SingleInstance>(&createFunction);
    m_container.registerFactory<PresetManager,
                                DiContainer::SingleInstance>(&createFunction);
    m_container.registerFactory<AccessorHolder,
                                DiContainer::SingleInstance>(&createFunction);
    m_container.registerFactory<DisplaySettingsManager,
                                DiContainer::SingleInstance>(&createFunction);

}

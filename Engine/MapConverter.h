#ifndef MAPCONVERTER_H
#define MAPCONVERTER_H
#include "toolsqt/MapUtils/D2MapModel.h"
#include "MapObjects/GameMap.h"
#include <QSharedPointer>
#include "MapObjects/StackObject.h"
#include "MapObjects/StackObject.h"
#include "MapObjects/MerchantObject.h"
#include "toolsqt/MapUtils/D2MapEditor.h"
#include "toolsqt/MapUtils/DataBlocks/DataBlocks.h"
#include "MapObjects/EventObject.h"

struct ValidationResult
{
    bool valid;
    QStringList errors;
};

class MapConverter
{
public:
    static ValidationResult validateMap(const D2MapModel &map);
    static bool importMap(GameMap *output, const D2MapModel &map);
    static void importVariables(GameMap *output, const D2MapModel &map);
    static void importTemplates(GameMap *output, const D2MapModel &map);
    static void importDiplomacy(GameMap *output, const D2MapModel &map);

    static D2MapModel exportMap(GameMap map);
    static void updateGarrison(D2MapEditor * editor,
                           const GroupData & garrison, int &leaderId,
                           StackData & stack);
    static void updateTemplate(D2MapEditor * editor,
                               const GroupData & garrison,
                               D2StackTemplate &stack);

    //
    static QList<int> exportItems(D2MapEditor * editor,const Inventory & inventory);

    static void exportMerchants(D2MapEditor * editor, GameMap &map);
    static void exportTemplates(D2MapEditor * editor, GameMap &map);
    static void exportCrystals(D2MapEditor * editor, GameMap &map);
    static void exportLandMarks(D2MapEditor * editor, GameMap &map);
    static void exportMountains(D2MapEditor *editor, GameMap &map);
    static void exportBags(D2MapEditor * editor, GameMap &map);
    static void exportVariables(D2MapEditor * editor, GameMap &map);
    static void exportLocations(D2MapEditor * editor, GameMap &map);
    static void exportRuins(D2MapEditor * editor, GameMap &map);
    static void exportVillages(D2MapEditor * editor, GameMap &map);
    static void exportStacks(D2MapEditor * editor, GameMap &map);
    static void exportEvents(D2MapEditor * editor, GameMap &map);
    static void exportRods(D2MapEditor * editor, GameMap &map);
    static void exportTombs(D2MapEditor * editor, GameMap &map);
    static void exportDiplomacy(D2MapEditor * editor, GameMap &map);
    //
    static void updateInventory(D2MapEditor * editor,
                                const Inventory & inventory,
                           QList<int> &items);
    static int baseIndexByPos(const QPoint& pos);
    static QList<D2Merchant::MerchantEntry> convert(const Inventory & inventory);
    static QList<D2Mercs::MercsUnitEntry> convert(const MerchantObject::UnitHireList & units);
    static void fillCondition(EventObject::EventCondition &eventCondition);
    static void fillEffectTargets(EventObject::EventEffect &eventEffect);
};

#endif // MAPCONVERTER_H

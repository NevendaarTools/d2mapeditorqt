#!/bin/bash

current_dir=$(pwd)
parent_dir=$(dirname "$current_dir")
toolsqt_dir="$parent_dir/toolsqt"

if [ ! -d "$toolsqt_dir" ]; then
    cd "$parent_dir"
    git clone https://NevendaarTools@bitbucket.org/NevendaarTools/toolsqt.git
    cd "$current_dir"
fi

ln -s "$toolsqt_dir" "$current_dir/toolsqt"

mkdir -p build/build_32
mkdir -p build/build_64

ln -s "$current_dir/Translations" "$current_dir/build/build_32/Translations"
ln -s "$current_dir/Images" "$current_dir/build/build_32/Images"
ln -s "$current_dir/QML" "$current_dir/build/build_32/QML"
ln -s "$current_dir/Presets" "$current_dir/build/build_32/Presets"
ln -s "$current_dir/Generators" "$current_dir/build/build_32/Generators"
ln -s "$current_dir/ExtraDeps" "$current_dir/build/build_32/ExtraDeps"

ln -s "$current_dir/Translations" "$current_dir/build/build_64/Translations"
ln -s "$current_dir/Images" "$current_dir/build/build_64/Images"
ln -s "$current_dir/QML" "$current_dir/build/build_64/QML"
ln -s "$current_dir/Presets" "$current_dir/build/build_64/Presets"
ln -s "$current_dir/Generators" "$current_dir/build/build_64/Generators"
ln -s "$current_dir/ExtraDeps" "$current_dir/build/build_64/ExtraDeps"

echo "Done."


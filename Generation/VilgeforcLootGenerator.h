#ifndef VILGEFORCLOOTGENERATOR_H
#define VILGEFORCLOOTGENERATOR_H
#include "Generators.h"

struct ItemGenerationInfo
{
    int totalExistCount = 0;
    int chance = 100;
    int perGenLimit = 6;
    bool limitedGen = false;
    int currentExistCount = 0;
    QVector<QSharedPointer<GItem>> items;
    QHash<QString, int> existed;
    int availableIndex = 0;

    void findLimit(int maxValue)
    {
        for (int i = 0; i < items.count(); ++i)
        {
            int value = Currency::fromString(items[i]->value).totalCount();
            if (value > maxValue)
            {
                availableIndex = i - 1;
                return;
            }
        }
        availableIndex = items.count() - 1;
    }
};

class VilgeforcLootGenerator: public ILootGenerator
{
    QSharedPointer<DBFModel> m_DBFModel;
    QSharedPointer<MapStateHolder> m_map;
    QSharedPointer<OptionsModel> m_options;

    QVector<QSharedPointer<GItem>> m_items;

    ItemGenerationInfo genInfo[GItem::CategoryCount];

    int m_lootValue = 50;
    int m_chancesSum = 0;
    GroupEditor m_editor;

public:
    VilgeforcLootGenerator();
    virtual QString name() const override;
    virtual void init(QSharedPointer<DBFModel> model,
                      QSharedPointer<MapStateHolder> map,
                      QSharedPointer<OptionsModel> options) override;
    virtual void updateContext(int x, int y, const MapLocation &location, const GroupData & group) override;
    virtual Inventory generate() override;
};

#endif // VILGEFORCLOOTGENERATOR_H

#ifndef WAVEFUNCTIONCOLLAPSE_H
#define WAVEFUNCTIONCOLLAPSE_H
#include <list>
#include <vector>
#include <string>
#include <stdio.h>
#include <cstdint>
typedef unsigned long   ulong;

#define UnknownTile 0


class WaveFunctionCollapse
{
public:

    struct CellVariant
    {
        int8_t value;
        int8_t top;
        int8_t right;
        int8_t bottom;
        int8_t left;
        int32_t count = 0;
    };

    WaveFunctionCollapse();

    void setDefault(const int8_t &defaultValue);
    void train(const std::vector<std::vector<int8_t>> & data);
    void fill(std::vector<std::vector<int8_t>> & data, bool makeKnownTileCollapsed = true);
    bool loadDataFromFile(const std::string& filename);
    bool saveDataToFile(const std::string& filename);

    //not for regular use
    int8_t getDefault() const;
    const std::list<CellVariant> & getVariants() const{return m_variants;}
    void pushVariant(int8_t value,
                     int8_t top,
                     int8_t right,
                     int8_t bottom,
                     int8_t left);
private:
    int8_t getTileType(const std::vector<std::vector<int8_t>> & data, ulong x, ulong y);
    std::list<CellVariant> getVariants( int8_t top,
                                    int8_t right,
                                    int8_t bottom,
                                    int8_t left);
    int8_t getCellValue(ulong x, ulong y);
    void updateCellVariants(ulong x, ulong y);
private:
    struct CellData
    {
        std::list<CellVariant> variants;
        bool isCollapsed() const {return variants.size() == 1;}

        void collapse()
        {
            int64_t totalProbability = 0;
            for (const auto & prob : variants) {
                totalProbability += prob.count;
            }

            int64_t randomValue = rand() % totalProbability;
            int64_t cumulativeProbability = 0;
            for (const auto & prob : variants) {
                cumulativeProbability += prob.count;
                if (randomValue <= cumulativeProbability)
                {
                    auto result = prob;
                    variants.clear();
                    variants.push_back(result);
                    return;
                }
            }

            variants.clear();
            variants.push_back(CellVariant{UnknownTile, UnknownTile, UnknownTile, UnknownTile, UnknownTile, 1});
        }
    };

    int8_t m_default = 0;
    std::list<CellVariant> m_variants;
    std::vector<std::vector<CellData>> m_data;
};

#endif // WAVEFUNCTIONCOLLAPSE_H

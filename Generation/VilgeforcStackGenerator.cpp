#include "VilgeforcStackGenerator.h"
#include "Engine/GameInstance.h"
#include "toolsqt/DBFModel/DBFModel.h"
#include "toolsqt/Common/Logger.h"

QString VilgeforcStackGenerator::name() const
{
    return "Vilgeforc";
}

void VilgeforcStackGenerator::init(QSharedPointer<DBFModel> model, QSharedPointer<MapStateHolder> holder, QSharedPointer<OptionsModel> options)
{
    m_DBFModel = model;
    m_map = holder;
    m_options = options;
    options->clear();
    options->addOption(Option::intOption("Exp Killed", 125, "Exp Killed_gen_desc", 25, 99999));
    options->addOption(Option::boolOption("Increase with disstance", true, "Increase with disstance_desc"));
    Option result = Option::intOption("Exp result", 125, "Exp result_gen_desc", 25, 99999);
    result.status = Option::Disabled;
    options->addOption(result);

    auto races = RESOLVE(DBFModel)->getList<GSubRace>();
    foreach (const QSharedPointer<GSubRace> & race, races)
    {
        Option opt = Option::boolOption(race->name_txt->text, false, "");
        //        opt.variants = new List<string>();
        //        opt.variants.Add(race.race_type.key.ToString());
        opt.group = "Available races";
        options->addOption(opt);
    }
    options->addOption(Option::boolOption("Zone override", true, "Zone override_desc"));
    m_units = RESOLVE(DBFModel)->getList<Gunit>();
    m_banned << "g001uu7599";
    m_banned << "g000uu7626";
    m_banned << "g000uu7599";
    foreach (const QSharedPointer<Gunit> & unit, m_units)
    {
        if (unit->unit_cat->text == "L_NOBLE")
            continue;
        if (m_banned.contains(unit->unit_id))
            continue;
        if (unit->unit_cat->text == "L_LEADER")
        {
            m_leaders << unit;
            continue;
        }
        if (getRange(unit) == Range::Melee)
            m_melee << unit;
        else
            m_range << unit;
    }
    leaders = filtered(m_leaders);
    melee = filtered(m_melee);
    range = filtered(m_range);
}

void VilgeforcStackGenerator::updateContext(int x, int y, const MapLocation &location, bool withLeader)
{
    m_withLeader = withLeader;
}

QList<QSharedPointer<Gunit>> VilgeforcStackGenerator::filtered(QList<QSharedPointer<Gunit>> base) const
{
    QList<QSharedPointer<Gunit>> result;
    if (m_subraces.isEmpty())
    {
        result =  base;
    }
    else
    {
        foreach (const QSharedPointer<Gunit> & unit, base)
        {
            if (!m_subraces.contains(unit->subrace.key))
                continue;
            result << unit;
        }
    }
    std::sort(result.begin(), result.end(),
              [](const QSharedPointer<Gunit> a, const QSharedPointer<Gunit> b) -> bool
              { return a->xp_killed < b->xp_killed; });

    return result;
}

GroupData VilgeforcStackGenerator::generate()
{
    m_editor.clearGroup();

    int expKillBase = Option::getIntOption(m_options->options(), "Exp result");
    int expKill = expKillBase;
    static int chances [] = { 100, 97, 90, 90, 80, 80 };
    //                        1    2   3   4   5   6
    availablePos.reset();
    //create leader
    while (true)
    {
        int index = maxIndex(leaders, expKill);
        if (index == -1)
        {
            expKill += 30;
            continue;
        }
        QSharedPointer<Gunit> unit = getRandUnit(leaders, index);
        placeUnit(unit);
        expKill-=unit->xp_killed;
        break;
    }
    //create units
    Range unitRange = nextRange();
    bool switched = false;
    while (true)
    {
        if (availablePos.unitsCount > 5)
            break;
        int rand = QRandomGenerator::global()->bounded(0, 100);
        if (rand >= chances[availablePos.unitsCount])
            break;
        QSharedPointer<Gunit> unit;
        if (unitRange == Range::Melee)
        {
            int index = maxIndex(melee, expKill);
            if (index == -1 || availablePos.meleeCount == 0)
            {
                if (switched)
                {
                    break;
                }
                unitRange = switchRange(unitRange);
                switched = true;
                continue;
            }
            unit = getRandUnit(melee, index);
            if (!placeUnit(unit))
                continue;
        }
        else
        {
            int index = maxIndex(range, expKill);
            if (index == -1 || availablePos.rangeCount == 0)
            {
                if (switched)
                {
                    break;
                }
                unitRange = switchRange(unitRange);
                switched = true;
                continue;
            }
            unit = getRandUnit(range, index);
            if (!placeUnit(unit))
                continue;
        }
        expKill-=unit->xp_killed;
        unitRange = nextRange();
    }
    if (expKill > 1)
    {
        struct UnitInfo
        {
            UnitObject unit;
            QSharedPointer<Gunit> unitBase;
            int unitLvl;
        };
        QVector<UnitInfo> units;
        for (int i = 0; i < m_editor.group().units.count(); ++i)
        {
            auto unit = RESOLVE(MapStateHolder)->objectByIdT<UnitObject>(m_editor.group().units[i].first);
            auto gunit = RESOLVE(DBFModel)->get<Gunit>(unit.id);
            units << UnitInfo{unit, gunit, unit.level};
        }
        std::sort(units.begin(), units.end(),
                  [](const UnitInfo & a, const UnitInfo & b) -> bool
                  { return a.unitBase->xp_killed < b.unitBase->xp_killed; });

        while(true)
        {
            Logger::log("exp kill = " + QString::number(expKill));
            bool added = false;
            for (int i = 0; i < units.count(); ++i)
            {
                int toAdd = ((units[i].unitLvl + 1) >= units[i].unitBase->dyn_upg_lv)?
                                (units[i].unitBase->dyn_upg2->xp_killed):
                                units[i].unitBase->dyn_upg1->xp_killed;
                if (toAdd < expKill)
                {
                    expKill-=toAdd;
                    added = true;
                    units[i].unitLvl++;
                }
            }

            if (!added)
                break;
        }
        for (int i = 0; i < units.count(); ++i)
        {
            if (units[i].unitLvl != units[i].unit.level)
            {
                units[i].unit.level = units[i].unitLvl;
                RESOLVE(MapStateHolder)->replaceObject(QSharedPointer<MapObject>(new UnitObject(units[i].unit)));
            }
        }
    }

    return m_editor.group();
}

void VilgeforcStackGenerator::propertyChanged(const QString &name)
{
    Q_UNUSED(name)
    m_subraces.clear();
    auto races = RESOLVE(DBFModel)->getList<GSubRace>();
    foreach (const QSharedPointer<GSubRace> & race, races)
    {
        if (Option::getBoolOption(m_options->options(), race->name_txt->text))
            m_subraces << race->race_type.key;
    }
    leaders = filtered(m_leaders);
    melee = filtered(m_melee);
    range = filtered(m_range);
}

void VilgeforcStackGenerator::acceptGeneration()
{
    m_editor.setGroup(GroupData());
}

int VilgeforcStackGenerator::maxIndex(const QList<QSharedPointer<Gunit> > &total, int exp) const
{
    if (total.count() == 0)
        return -1;
    for (int i = 0; i < total.count(); ++i)
    {
        if (total[i]->xp_killed > exp)
            return i - 1;
    }
    return total.count() - 1;
}

QSharedPointer<Gunit> VilgeforcStackGenerator::getRandUnit(const QList<QSharedPointer<Gunit> > &total, int maxIndex) const
{
    if (maxIndex == 0)
        return total[0];
    int index = QRandomGenerator::global()->bounded(0, maxIndex);
    return total[index];
}

bool VilgeforcStackGenerator::placeUnit(const QSharedPointer<Gunit> unit)
{
    int posY = 0;
    int posX = 0;
    if (!unit->size_small)
    {
        if (availablePos.bigCount == 0)
            return false;
        posY = availablePos.randBigPos();
        posX = 1;
        availablePos.setBig(posY);
    }
    else
    {
        Range range = getRange(unit);
        if (range == Range::Melee)
        {
            posY = availablePos.randMeleePos();
            posX = 1;
            availablePos.setMelee(posY);
        }
        else
        {
            posY = availablePos.randRangePos();
            posX = 0;
            availablePos.setRange(posY);
        }
    }
    return m_editor.tryHire(posX, posY, unit->unit_id, unit->level);
}

VilgeforcStackGenerator::Range VilgeforcStackGenerator::getRange(const QSharedPointer<Gunit> &unit)
{
    auto reach = unit->attack_id->reach;
    if (reach->att_script.trimmed() != "")
    {
        if (reach->melee)
            return Range::Melee;
        if (reach->max_targts == 1)
            return Range::Archer;
        return Range::Mage;
    }
    if (reach->text == "L_ALL")
        return Range::Mage;
    if(reach->text == "L_ANY")
        return Range::Archer;
    return Range::Melee;
}

VilgeforcStackGenerator::Range VilgeforcStackGenerator::nextRange() const
{
    if (availablePos.meleeCount == 0 && availablePos.rangeCount == 0)
        return Range::None;
    if (availablePos.meleeCount == 3)
        return Range::Melee;
    return (QRandomGenerator::global()->generate() %2 == 0) ? Range::Melee : Range::Archer;
}

VilgeforcStackGenerator::Range VilgeforcStackGenerator::switchRange(Range range) const
{
    return (range == Range::Melee)? Range::Archer : Range::Melee;
}

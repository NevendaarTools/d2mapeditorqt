#include "WaveFunctionCollapse.h"
#include <fstream>

WaveFunctionCollapse::WaveFunctionCollapse()
{

}

void WaveFunctionCollapse::pushVariant(int8_t value, int8_t top, int8_t right, int8_t bottom, int8_t left)
{
    for(auto & tileVariant: m_variants)
    {
        if (value == tileVariant.value &&
                top == tileVariant.top &&
                right == tileVariant.right &&
                bottom == tileVariant.bottom &&
                left == tileVariant.left)
        {
            tileVariant.count++;
            return;
        }
    }
    m_variants.push_back(WaveFunctionCollapse::CellVariant{value, top, right, bottom, left, 1});
}

std::list<WaveFunctionCollapse::CellVariant> WaveFunctionCollapse::getVariants(int8_t top, int8_t right, int8_t bottom, int8_t left)
{
    std::list<CellVariant> result;
    for (const auto & tileVariant: m_variants)
    {
        if ((top == tileVariant.top || top == UnknownTile) &&
                (right == tileVariant.right  || right == UnknownTile)&&
                (bottom == tileVariant.bottom  || bottom == UnknownTile)&&
                (left == tileVariant.left || left == UnknownTile))
        {
            result.push_back(tileVariant);
        }
    }
//    std::copy_if(
//        m_variants.begin(), m_variants.end(),
//        std::back_inserter(result),
//        [&](const CellVariant& tileVariant) {
//            return (top == tileVariant.top || top == UnknownTile) &&
//                   (right == tileVariant.right || right == UnknownTile) &&
//                   (bottom == tileVariant.bottom || bottom == UnknownTile) &&
//                   (left == tileVariant.left || left == UnknownTile);
//        }
//    );

    if (result.size() == 0)
        result.push_back(CellVariant{m_default, UnknownTile, UnknownTile, UnknownTile, UnknownTile, 1});
    return result;
}

int8_t WaveFunctionCollapse::getDefault() const
{
    return m_default;
}

void WaveFunctionCollapse::setDefault(const int8_t &defaultValue)
{
    m_default = defaultValue;
}

int8_t WaveFunctionCollapse::getCellValue(ulong x, ulong y)
{
    if (x < 0 || y < 0)
        return UnknownTile;
    if (x > m_data.size() - 1 || y  > m_data[x].size() - 1)
        return UnknownTile;
    if (!m_data[x][y].isCollapsed())
        return UnknownTile;

    return m_data[x][y].variants.back().value;
}

void WaveFunctionCollapse::updateCellVariants(ulong x, ulong y)
{
    if (m_data[x][y].isCollapsed())
        return;
    m_data[x][y].variants = getVariants(getCellValue(x, y - 1),
                                        getCellValue(x + 1, y),
                                        getCellValue(x, y + 1),
                                        getCellValue(x - 1, y));

}

int8_t WaveFunctionCollapse::getTileType(const std::vector<std::vector<int8_t> > &data, ulong x, ulong y)
{
    if (x < 0 || y < 0)
        return UnknownTile;
    if (x > data.size() - 1 || y  > data.size() - 1)
        return UnknownTile;
    return data[x][y];
}

void WaveFunctionCollapse::train(const std::vector<std::vector<int8_t> > &data)
{
    for (ulong x = 0; x < data.size(); ++x)
    {
        for (ulong y = 0; y < data.size(); ++y)
        {
            pushVariant(getTileType(data, x, y),
                        getTileType(data, x, y - 1),
                        getTileType(data, x + 1, y),
                        getTileType(data, x, y + 1),
                        getTileType(data, x - 1, y));
        }
    }
}

void WaveFunctionCollapse::fill(std::vector<std::vector<int8_t> > &data, bool makeKnownTileCollapsed)
{
    //initialise data
    m_data.clear();
    m_data.resize(data.size());
    for(ulong i = 0; i < data.size(); ++i)
    {
        m_data[i].resize(data[i].size());
        for(ulong k = 0; k < data[i].size(); ++k)
        {
            if (makeKnownTileCollapsed && data[i][k] != UnknownTile)
            {
                WaveFunctionCollapse::CellVariant tmp{data[i][k], UnknownTile, UnknownTile, UnknownTile, UnknownTile, 1};
                m_data[i][k].variants = std::list<WaveFunctionCollapse::CellVariant>{tmp};
            }
            else
                m_data[i][k].variants = getVariants();
        }
    }
    //calculate posible variants
    for(ulong i = 0; i < data.size(); ++i)
    {
        for(ulong k = 0; k < data[i].size(); ++k)
        {
            updateCellVariants(i, k);
        }
    }
    //collapse data
    while (true)
    {
        bool finished = true;

        ulong minCount = m_variants.size();
        ulong minX = 0, minY = 0;
        for (ulong i = 0; i < m_data.size(); ++i)
        {
            for (ulong k = 0; k < m_data[i].size(); ++k)
            {
                if (m_data[i][k].isCollapsed())
                    continue;
                if (m_data[i][k].variants.size() <= minCount)
                {
                    minCount = m_data[i][k].variants.size() ;
                    minX = i; minY = k;
                    finished = false;
                }
            }
        }
        if (m_data[minX][minY].isCollapsed())
            break;
        m_data[minX][minY].collapse();
        if (minX > 1)
            updateCellVariants(minX - 1, minY);
        if (minY > 1)
            updateCellVariants(minX, minY - 1);
        if (minX < m_data.size() - 1)
            updateCellVariants(minX + 1, minY);
        if (minY < m_data[minX].size() - 1)
            updateCellVariants(minX, minY + 1);
        if (finished)
            break;
    }
    //write data
    for (ulong i = 0; i < data.size(); ++i)
    {
        for (ulong k = 0; k < data.size(); ++k)
        {
            data[i][k] = m_data[i][k].variants.front().value;
        }
    }
}

bool WaveFunctionCollapse::loadDataFromFile(const std::string &filename)
{
    std::ifstream file(filename, std::ios::binary);
    if (!file.is_open())
    {
        return false;
    }

    m_variants.clear();

    CellVariant variant;
    while (file.read(reinterpret_cast<char*>(&variant), sizeof(CellVariant)))
    {
        m_variants.push_back(variant);
    }

    file.close();
    return true;
}

bool WaveFunctionCollapse::saveDataToFile(const std::string &filename)
{
    std::ofstream file(filename, std::ios::binary);
    if (!file.is_open())
    {
        return false;
    }

    for (const CellVariant& variant : m_variants)
    {
        file.write(reinterpret_cast<const char*>(&variant), sizeof(CellVariant));
    }

    file.close();
    return true;
}

#include "GeneratorWrapper.h"
#include <QStringList>
#ifdef WINDOWS
GeneratorWrapper::GeneratorWrapper(GeneratorHandle *handle)
    : m_handle(handle)
{

}

GeneratorWrapper::~GeneratorWrapper()
{

}

int GeneratorWrapper::optionsCount() const
{
    return m_QtOptions.count();
}

Option GeneratorWrapper::getOptionAt(int index)
{
    return m_QtOptions[index];
}

bool GeneratorWrapper::setOptionValue(int index, const QString &value)
{
    int realIndex = resolveRealIndex(index);
    int result = m_handle->setOptionAt(realIndex, QString(value + "\0").toStdString().c_str());
    qDebug()<<Q_FUNC_INFO<<value<<index<<realIndex<<result;
    fillOptions();
    return result != 0;
}
QString safeValue(char * value)
{
    if (!!value)
    {
        return QString::fromLocal8Bit(value);
    }
    return QString();
}

#include <QDir>
QString GeneratorWrapper::generate()
{
    QString cur = QDir::currentPath();
    qDebug()<<cur;
    QDir::setCurrent(cur + "/Generators");
    qDebug()<<QDir::currentPath();
    QString result = safeValue(m_handle->generateMap());
    QDir::setCurrent(cur);
    return result;
}

void GeneratorWrapper::init(const QString &path, const QString &lang)
{
    m_handle->initLib(QString(path + "\0").toStdString().c_str(),
                      QString(lang + "\0").toStdString().c_str());
    fillOptions();
}

QString GeneratorWrapper::name() const
{
    return m_handle->name;
}

QString GeneratorWrapper::desc() const
{
    return m_handle->desc;
}

void GeneratorWrapper::fillOptions()
{
    m_options.clear();
    m_QtOptions.clear();
    int count = m_handle->getOptionsCount();
    for(int i = 0; i < count; ++i)
    {
        OptionWrap * opt = m_handle->getOptionAt(i);
        m_options.append(opt);
        //if (opt != 0 && opt->status == 1)
        {
            Option result;
            result.desc = safeValue(opt->desc);
            result.name = safeValue(opt->name);
            result.min = safeValue(opt->min);
            result.max = safeValue(opt->max);
            result.value = safeValue(opt->value);
            result.type = (Option::Type)opt->type;
            QString variantsString = safeValue(opt->variants);
            if (variantsString.count() > 0)
                result.variants = variantsString.split(";");
            result.index = i;
            m_QtOptions.append(result);
        }
    }
}

int GeneratorWrapper::resolveRealIndex(int index)
{
    return m_QtOptions[index].index;
}
#endif

#ifndef GENERATORWRAPPER_H
#define GENERATORWRAPPER_H
#include "GeneratorHandle.h"
#include <QVector>

#ifdef WINDOWS
class GeneratorWrapper : public IConfigurable
{
public:
    GeneratorWrapper(GeneratorHandle * handle);
~GeneratorWrapper();
    // IConfigurable interface
public:
    virtual int optionsCount() const override;
    virtual Option getOptionAt(int index) override;
    virtual bool setOptionValue(int index, const QString &value) override;//return neadUpdate
    QString generate();
    void init(const QString &path, const QString& lang);

    QString name() const;
    QString desc() const;
private:
    void fillOptions();
    int resolveRealIndex(int index);
private:
    GeneratorHandle * m_handle;
    QVector<OptionWrap*> m_options;
    QVector<Option> m_QtOptions;
};
#endif
#endif // GENERATORWRAPPER_H

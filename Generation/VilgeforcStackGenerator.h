#ifndef VILGEFORCSTACKGENERATOR_H
#define VILGEFORCSTACKGENERATOR_H
#include "Generators.h"

struct AvailablePositions
{
    void reset()
    {
        availableMelee.resize(3);
        availableRange.resize(3);
        availableBig.resize(3);
        for (int i = 0; i < 3; ++i)
        {
            availableMelee[i].second = true;
            availableRange[i].second = true;
            availableBig[i].second = true;
        }
        meleeCount=3;
        rangeCount=3;
        bigCount=3;

        unitsCount=0;
    }
    int randMeleePos() const
    {
        int index = 0;
        if (meleeCount != 1)
        {
            index = QRandomGenerator::global()->bounded(0, meleeCount);
        }
        for (int i = 0; i < 3; ++i)
        {
            if (availableMelee[i].second)
            {
                if (index == 0)
                    return i;
                index--;
            }
        }
        return 0;
    }
    int randRangePos() const
    {
        int index = 0;
        if (rangeCount != 1)
        {
            index = QRandomGenerator::global()->bounded(0, rangeCount);
        }
        for (int i = 0; i < 3; ++i)
        {
            if (availableRange[i].second)
            {
                if (index == 0)
                    return i;
                index--;
            }
        }
        return 0;
    }
    int randBigPos() const
    {
        int index = 0;
        if (bigCount != 1)
        {
            index = QRandomGenerator::global()->bounded(0, bigCount);
        }
        for (int i = 0; i < 3; ++i)
        {
            if (availableBig[i].second)
            {
                if (index == 0)
                    return i;
                index--;
            }
        }
        return 0;
    }

    void setMelee(int index)
    {
        unitsCount++;
        meleeCount-=1;
        availableMelee[index].second = false;
        availableBig[index].second = false;
    }
    void setRange(int index)
    {
        unitsCount++;
        rangeCount-=1;
        availableRange[index].second = false;
        availableBig[index].second = false;
        if (availableMelee[index].second)
            bigCount--;
    }
    void setBig(int index)
    {
        unitsCount+=2;
        meleeCount-=1;
        rangeCount-=1;
        bigCount--;

        availableMelee[index].second = false;
        availableRange[index].second = false;
        availableBig[index].second = false;
    }
    int meleeCount = 3;
    int rangeCount = 3;
    int bigCount = 3;

    int unitsCount = 0;

    QVector<QPair<int, bool>> availableMelee;
    QVector<QPair<int, bool>> availableRange;
    QVector<QPair<int, bool>> availableBig;
};


class VilgeforcStackGenerator  : public IStackGenerator
{
    QSharedPointer<DBFModel> m_DBFModel;
    QSharedPointer<MapStateHolder> m_map;
    QSharedPointer<OptionsModel> m_options;
    GroupEditor m_editor;
    QVector<QSharedPointer<Gunit>> m_units;
    QList<QSharedPointer<Gunit>> m_leaders;
    QList<QSharedPointer<Gunit>> m_melee;
    QList<QSharedPointer<Gunit>> m_range;
    QSet<QString> m_banned;
    QSet<int> m_subraces;
    bool m_withLeader = true;
    //tmp data
    QList<QSharedPointer<Gunit>> leaders;
    QList<QSharedPointer<Gunit>> melee;
    QList<QSharedPointer<Gunit>> range;

    AvailablePositions availablePos;
public:
    enum Range
    {
        Melee = 0,
        Mage = 1,
        Archer = 2,
        None
    };
    virtual QString name() const override;
    virtual void init(QSharedPointer<DBFModel> model,
                      QSharedPointer<MapStateHolder> holder,
                      QSharedPointer<OptionsModel> options) override;
    virtual void updateContext(int x, int y, const MapLocation &location, bool withLeader) override;
    virtual GroupData generate() override;
    virtual void propertyChanged(const QString &name) override;
    virtual void acceptGeneration() override;
private:
    int maxIndex(const QList<QSharedPointer<Gunit> > &total, int exp) const;
    QSharedPointer<Gunit> getRandUnit(const QList<QSharedPointer<Gunit> > &total, int maxIndex) const;
    bool placeUnit(const QSharedPointer<Gunit> unit);

    Range getRange(const QSharedPointer<Gunit> & unit);
    Range nextRange() const;
    Range switchRange(Range range) const;
    QList<QSharedPointer<Gunit>> filtered(QList<QSharedPointer<Gunit>> base) const;
};


#endif // VILGEFORCSTACKGENERATOR_H

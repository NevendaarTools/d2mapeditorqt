#ifndef GENERATORS_H
#define GENERATORS_H
#include <QString>
#include "MapObjects/BaseClases.h"
#include "QMLHelpers/Models/OptionsModel.h"
#include "QMLHelpers/Components/GroupUnitsEditor.h"
#include <QRandomGenerator>
#include "toolsqt/DBFModel/DBFModel.h"
#include "Engine/Map/MapStateHolder.h"

struct MapLocation
{
    bool defined = false;//no currentLocation
    QList<QString> races;//gsubrace.dbf
    int expPerStackValue = 25;
};

class IStackGenerator
{
public:
    virtual ~ IStackGenerator(){}
    virtual QString name() const = 0;
    virtual void init(QSharedPointer<DBFModel> model,
                      QSharedPointer<MapStateHolder> holder,
                      QSharedPointer<OptionsModel> options) = 0;
    virtual void updateContext(int x, int y, const MapLocation & location, bool withLeader) = 0;
    virtual GroupData generate() = 0;
    virtual void acceptGeneration() = 0;
    virtual void propertyChanged(const QString & name) = 0;
};

class ILootGenerator
{
public:
    virtual ~ ILootGenerator(){}
    virtual QString name() const = 0;
    virtual void init(QSharedPointer<DBFModel> model,
                      QSharedPointer<MapStateHolder> holder,
                      QSharedPointer<OptionsModel> options) = 0;
    virtual void updateContext(int x, int y, const MapLocation & location, const GroupData & group) = 0;
    virtual Inventory generate() = 0;
};

#endif // GENERATORS_H

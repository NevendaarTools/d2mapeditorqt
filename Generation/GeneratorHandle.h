#ifndef GENERATORHANDLE_H
#define GENERATORHANDLE_H
#ifdef  WINDOWS
#include <Windows.h>
#include <QDebug>

struct OptionWrap
{
    enum Type
    {
        Bool = 0,
        Float,
        Int,
        String,
        Enum,
        Title = 32,
        Spacer
    };

    char *  name;
    char *  desc;
    char *  value;
    char *  min;
    char *  max;
    char *  variants;
    int type;
    int status;
};


typedef int32_t ErrorCode;

struct GeneratorHandle
{
    GeneratorHandle();

    bool load(const wchar_t * filename);
    void initLib(const char* path, const char* lang);
    bool close();

    void        (*init)             (const char*, const char*);//gamepath, lang
    char*       (*getName)          (void);
    char*       (*getDescription)   (void);
    int         (*getOptionsCount)  (void);
    OptionWrap* (*getOptionAt)      (int);
    int         (*setOptionAt)      (int, const char*);//index, value
    char*       (*generateMap)      (void);
    void        (*cleanup)          (void);

    char * name;
    char * desc;
    char * map;
    HMODULE libHandle;
};
#endif
#endif // GENERATORHANDLE_H

#include "GeneratorHandle.h"
#ifdef WINDOWS
GeneratorHandle::GeneratorHandle()
    :name(nullptr), cleanup(nullptr)
{}

bool GeneratorHandle::load(const wchar_t *filename)
{
    SetDefaultDllDirectories(LOAD_LIBRARY_SEARCH_DEFAULT_DIRS | LOAD_LIBRARY_SEARCH_USER_DIRS);

    libHandle = LoadLibraryExW(filename, NULL,
                               LOAD_LIBRARY_SEARCH_SYSTEM32 |
                               LOAD_LIBRARY_SEARCH_USER_DIRS |
                               LOAD_LIBRARY_SEARCH_DLL_LOAD_DIR);
    //libHandle = LoadLibrary(filename);
    if (!libHandle)
    {
        printf("Failed to load dll.");
        int error = GetLastError();
        printf("%d", error);
        return false;
    }
    init = (void (*)(const char *, const char *))GetProcAddress(libHandle, "init");
    if (!init)
    {
        printf("Failed to export \"init\" from DLL.");
        close();
        return false;
    }

    getName = (char* (*)(void))GetProcAddress(libHandle, "getName");
    if (!getName)
    {
        printf("Failed to export \"getName\" from DLL.");
        close();
        return false;
    }

    getDescription = (char* (*)(void))GetProcAddress(libHandle, "getDescription");
    if (!getDescription)
    {
        printf("Failed to export \"getDescription\" from DLL.");
        close();
        return false;
    }

    getOptionsCount = (int (*)(void))GetProcAddress(libHandle, "getOptionsCount");

    if (!getOptionsCount)
    {
        printf("Failed to export \"getOptionsCount\" from DLL.");
        close();
        return false;
    }

    getOptionAt = (OptionWrap* (*)(int))GetProcAddress(libHandle, "getOptionAt");
    if (!getOptionAt)
    {
        printf("Failed to export \"getOptionAt\" from DLL.");
        close();
        return false;
    }
    setOptionAt = (int (*)(int, const char *))GetProcAddress(libHandle, "setOptionAt");
    if (!setOptionAt)
    {
        printf("Failed to export \"setOptionAt\" from DLL.");
        close();
        return false;
    }

    generateMap = (char* (*)(void))GetProcAddress(libHandle, "generateMap");
   // generateMap = (char* (*)(void))GetProcAddress(libHandle, "getDescription");

    if (!generateMap)
    {
        printf("Failed to export \"generateMap\" from DLL.");
        close();
        return false;
    }

    return true;
}

void GeneratorHandle::initLib(const char *path, const char *lang)
{
    init(path, lang);
    desc = getDescription();
    name = getName();
}

bool GeneratorHandle::close()
{
    if (cleanup)
        cleanup();
    FreeLibrary(libHandle);
    return true;
}
#endif

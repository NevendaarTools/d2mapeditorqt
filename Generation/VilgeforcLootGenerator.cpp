#include "VilgeforcLootGenerator.h"

VilgeforcLootGenerator::VilgeforcLootGenerator()
{
    genInfo[GItem::Armor]           = ItemGenerationInfo{0, 20, 1, true};
    genInfo[GItem::Relict]          = ItemGenerationInfo{0, 20, 1, true};
    genInfo[GItem::Weapon]          = ItemGenerationInfo{0, 20, 1, true};
    genInfo[GItem::Banner]          = ItemGenerationInfo{0, 20, 1, true};
    genInfo[GItem::OneTurnBuff]     = ItemGenerationInfo{0, 70, 4, false};
    genInfo[GItem::Heal]            = ItemGenerationInfo{0, 90, 4, false};
    genInfo[GItem::Resurrect]       = ItemGenerationInfo{0, 40, 1, false};
    genInfo[GItem::EternalBuf]      = ItemGenerationInfo{0, 5, 1, true};
    genInfo[GItem::Scroll]          = ItemGenerationInfo{0, 60, 3, false};
    genInfo[GItem::Staff]           = ItemGenerationInfo{0, 10, 1, true};
    genInfo[GItem::Valuable]        = ItemGenerationInfo{0, 150, 3, false};
    genInfo[GItem::Sphere]          = ItemGenerationInfo{0, 70, 3, false};
    genInfo[GItem::Talisman]        = ItemGenerationInfo{0, 30, 1, true};
    genInfo[GItem::Boots]           = ItemGenerationInfo{0, 30, 1, true};
    genInfo[GItem::Quest]           = ItemGenerationInfo{0, 0, 0, true};
}

QString VilgeforcLootGenerator::name() const
{
    return "Vilgeforc";
}

void VilgeforcLootGenerator::init(QSharedPointer<DBFModel> model,
                                  QSharedPointer<MapStateHolder> map,
                                  QSharedPointer<OptionsModel> options)
{
    m_DBFModel = model;
    m_map = map;
    m_options = options;
    options->clear();
    options->addOption(Option::intOption("Scale, %", 100, "Scale_gen_desc", 0, 100000));
    options->addOption(Option::boolOption("Use loot tables", false, "Use loot tables_gen_desc"));
    options->addOption(Option::boolOption("Zone override", true, "Zone override_desc"));
    m_chancesSum = 0;
    for (int i = 0; i < GItem::CategoryCount; ++i)
    {
        genInfo[i].currentExistCount = 0;
        genInfo[i].items.clear();
        m_chancesSum += genInfo[i].chance;
    }
    m_items = model->getList<GItem>();
    std::sort(m_items.begin(), m_items.end(),
              [](const QSharedPointer<GItem> a, const QSharedPointer<GItem> b) -> bool
              {
                  return Currency::fromString(a->value).totalCount() < Currency::fromString(b->value).totalCount();
              });
    foreach (const QSharedPointer<GItem> & item, m_items)
    {
        genInfo[item->item_cat].items << item;
    }
    //TODO: fill totalExistCount
}

void VilgeforcLootGenerator::updateContext(int x, int y, const MapLocation &location, const GroupData &group)
{
    m_editor.setGroup(group);
    m_lootValue = m_editor.expForKill();
}
Inventory VilgeforcLootGenerator::generate()
{
    Inventory res;
    int lootValue = m_lootValue * Option::getIntOption(m_options->options(), "Scale, %") / 100.0;

    int limit = lootValue * 0.6;
    for (int i = 0; i < GItem::CategoryCount; ++i)
    {
        genInfo[i].currentExistCount = 0;
    }
    while(true)
    {
        m_chancesSum = 0;
        for (int i = 0; i < GItem::CategoryCount; ++i)
        {
            genInfo[i].findLimit(limit);
            if (genInfo[i].availableIndex == 0)
                continue;
            if (genInfo[i].currentExistCount > genInfo[i].perGenLimit)
                continue;
            m_chancesSum += genInfo[i].chance;
        }
        if (m_chancesSum == 0 || lootValue < 1)
            break;
        int catIndex = -1;
        int catValue =  QRandomGenerator::global()->bounded(0, m_chancesSum);
        for (int i = 0; i < GItem::CategoryCount; ++i)
        {
            if (genInfo[i].availableIndex <= 0)
                continue;
            if (genInfo[i].currentExistCount > genInfo[i].perGenLimit)
                continue;
            catValue -= genInfo[i].chance;
            if (catValue <=0)
            {
                catIndex = i;
                break;
            }
        }
        if (catIndex < 0)
            break;
        int itemIndex = 0;
        if (genInfo[catIndex].availableIndex != 1)
        {
            itemIndex = QRandomGenerator::global()->bounded(0, genInfo[catIndex].availableIndex + 1);
        }
        genInfo[catIndex].currentExistCount++;
        genInfo[catIndex].totalExistCount++;
        res.addItem(genInfo[catIndex].items[itemIndex]->item_id, 1,
                    (catIndex == GItem::Talisman)? 5 : 0);
        lootValue-= Currency::fromString(genInfo[catIndex].items[itemIndex]->value).totalCount();
        limit = lootValue;
    }
    //res.addItem("G000IG9126", 1, 7); // Добавьте его
    return res;
}

echo on
cls
set VSDIR="e:/Program Files (x86)/Microsoft Visual Studio/2019/Community/VC/Auxiliary/Build/vcvars64.bat"

if not exist %VSDIR% (
   set VSDIR="e:/Program Files (x86)/Microsoft Visual Studio/2017/Professional/VC/Auxiliary/Build/vcvars64.bat"
)

set CUR_DIR=%cd%
call %VSDIR%
cd bin
PATH=C:/Qt/6.5.3/msvc2019_64/bin;%PATH%
call "C:/Qt/6.5.3/msvc2019_64/bin/qmake.exe" D2MapEditor.pro -spec win32-msvc "CONFIG+=release"

cd %CUR_DIR%
del /f /s /q bin 1>nul
rmdir bin \s \q
mkdir bin
xcopy %CUR_DIR%\build\build_64\release\D2MapEditor.exe %CUR_DIR%\bin\ /R /I /Y /S
xcopy %CUR_DIR%\Images\* bin\Images /R /I /Y /S
xcopy %CUR_DIR%\QML\* bin\QML /R /I /Y /S
xcopy %CUR_DIR%\Presets\* bin\Presets /R /I /Y /S
xcopy %CUR_DIR%\Generators\* bin\Generators /R /I /Y /S
xcopy %CUR_DIR%\Translations\* bin\Translations /R /I /Y /S
xcopy %CUR_DIR%\ExtraDeps\* bin\ExtraDeps /R /I /Y /S

cd "C:/Qt/6.5.3/msvc2019_64/bin/"
call windeployqt.exe  -multimedia -no-opengl-sw %CUR_DIR%/bin
cd %CUR_DIR%
del bin\vc_redist.x64.exe

call "C:\Program Files\7-Zip\7z.exe" a ./bin/D2MapEditor.zip ./bin/*
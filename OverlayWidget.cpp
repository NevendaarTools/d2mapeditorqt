#include "OverlayWidget.h"
#include <QQmlContext>
#include <QQmlEngine>
#include "QMLHelpers/Common/ImageProvider.h"
#include "Events/MapEvents.h"
#include "Commands/OverlayCommands.h"
#include "Engine/Components/TranslationHelper.h"

OverlayWidget::OverlayWidget(QWidget *parent): QQuickWidget(parent)
{
    setAttribute(Qt::WA_TransparentForMouseEvents);
    setAttribute(Qt::WA_NoSystemBackground);
    setAttribute(Qt::WA_TranslucentBackground);
    setAttribute(Qt::WA_TransparentForMouseEvents, true);
    setAttribute(Qt::WA_AlwaysStackOnTop);
    setClearColor(Qt::transparent);
    setResizeMode(QQuickWidget::SizeRootObjectToView);
    m_rootItem = nullptr;

    subscribe<GameLoadedEvent>([this](const QVariant &event){onGameLoaded(event);});
    subscribe<HoveredObjectChangedEvent>([this](const QVariant &event){onHoverObjectChanged(event);});
    subscribe<HoverChangedEvent>([this](const QVariant &event){onHoverChanged(event);});
    subscribe<ObjectRemovedEvent>([this](const QVariant &event){onObjectRemoved(event);});
    subscribe<MapOverlaySettingsChanged>([this](const QVariant &event){onMapOverlaySettingsChanged(event);});

    registerExecutor<ShowToolTipCommand>
        ([this](const QVariant &commandVar){
        ShowToolTipCommand command = commandVar.value<ShowToolTipCommand>();
        emit tooltipRequested(command.x, command.y, command.text);
        return CommandResult::success();
    });
    registerExecutor<HideToolTipCommand>
        ([this](const QVariant &command){
        emit tooltipHideRequested();
        return CommandResult::success();
    });
    registerExecutor<ShowPreviewCommand>
        ([this](const QVariant &commandVar){
            ShowPreviewCommand command = commandVar.value<ShowPreviewCommand>();
            emit previewRequested(command.x, command.y, command.source, command.id);
            return CommandResult::success();
        });
    registerExecutor<HidePreviewCommand>
        ([this](const QVariant &commandVar){
            HidePreviewCommand command = commandVar.value<HidePreviewCommand>();
            emit previewHideRequested();
            return CommandResult::success();
        });
}

void OverlayWidget::init()
{
    rootContext()->setContextProperty("overlayWidget", this);
    rootContext()->setContextProperty("translate", this);
    engine()->addImageProvider(QLatin1String("provider"), new ImageProvider());
    setSource(QUrl::fromLocalFile("QML/OverlayWidget.qml"));
}

void OverlayWidget::onMapOverlaySettingsChanged(const QVariant &event)
{
    MapOverlaySettingsChanged command = event.value<MapOverlaySettingsChanged>();
    m_versionVisible = command.gameVVisible;
    m_posVisible = command.positionVisible;
    qDebug()<<Q_FUNC_INFO<<m_versionVisible<<" "<<m_posVisible;
    emit changed();
}

void OverlayWidget::onGameLoaded(const QVariant &event)
{
    Q_UNUSED(event)
    m_gameV = TranslationHelper::tr("GAME_VERSION");
    m_gameV = convertToHtml(m_gameV);//.replace("\\n", " ");
    emit changed();
}

void OverlayWidget::onHoverObjectChanged(const QVariant &event)
{
    HoveredObjectChangedEvent command = event.value<HoveredObjectChangedEvent>();
    if (command.hover_in != EMPTY_ID)
    {
        if (!m_rootItem)
            return;
        auto screanCoordinates = m_rootItem->mapToGlobal(QPoint(0, 15));
        emit previewRequested(screanCoordinates.x(), screanCoordinates.y(),
                              "Preview/" + MapObject::typeName((MapObject::Type)command.hover_in.first) + ".qml",
                              IdToString(command.hover_in));
    }
    else
    {
        emit previewHideRequested();
    }
}

void OverlayWidget::onHoverChanged(const QVariant &event)
{
    HoverChangedEvent hoverEvent = event.value<HoverChangedEvent>();
    showPos(hoverEvent.pos);
    if (!m_rootItem)
        emit requestRoot();
}

void OverlayWidget::onObjectRemoved(const QVariant &event)
{
    emit previewHideRequested();
    emit changed();
}

QString OverlayWidget::tr(const QString &text)
{
    return TranslationHelper::tr(text);
}

QString OverlayWidget::getPosition() const
{
    if (!m_posVisible)
        return "";
    return m_position;
}

QString OverlayWidget::getGameV() const
{
    if (!m_versionVisible)
        return "";
    return m_gameV;
}

void OverlayWidget::showPos(const QPoint &pos)
{
    m_position = QString("%1 : %2").arg(pos.x()).arg(pos.y());
    emit changed();
}

void OverlayWidget::setRootItem(QQuickItem *newRootItem)
{
    m_rootItem = newRootItem;
}
